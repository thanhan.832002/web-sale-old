import { CacheModule, Module } from '@nestjs/common';
import * as redisStore from 'cache-manager-redis-store';
import { CachePageService } from './cache-page.service';

@Module({
  imports: [
    CacheModule.registerAsync({
      useFactory: async () => ({
        store: redisStore,
        // host: '198.199.73.118',
        host: 'localhost',
        port: 6379,
        db: 1,
        auth_pass: 'bestbuyredispassword',
      })
    }),
  ],
  providers: [CachePageService],
  controllers: [],
  exports: [CachePageService]
})
export class CachePageModule { }
