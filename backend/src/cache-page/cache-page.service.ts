import { Injectable, Inject, CACHE_MANAGER } from '@nestjs/common';
import { Cache } from 'cache-manager';
@Injectable()
export class CachePageService {
    constructor(
        @Inject(CACHE_MANAGER) private cacheManager: Cache
    ) {
    }

    async removePageCache(slug: string, domain: string): Promise<any> {
        const keySubDotDomain = `p:${slug}.${domain}`
        const keyDomainAndSub = `p:${domain}/${slug}`
        //@ts-ignore
        await this.cacheManager.del([keySubDotDomain, keyDomainAndSub])
    }
}