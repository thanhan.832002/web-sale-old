import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class ChangePositionDto {
    @ApiProperty()
    @IsNotEmpty()
    status: number
}

export class AddToBestsellerDto {
    @ApiProperty({ type: [String] })
    @IsNotEmpty()
    listProductId: string[]
}