import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { BestSeller } from './best-seller.model';
var momentTz = require("moment-timezone");
import * as moment from 'moment'
import { OrderService } from 'src/order/order.service';
import { ProductService } from 'src/product/product.service';
import { ModuleRef } from '@nestjs/core';
import { OrderStatusEnum } from 'src/order/order.enum';
import { Timeout } from '@nestjs/schedule';
import * as Promise from 'bluebird'
import { ChangePositionDto } from './best-seller.dto';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';
import { ProductStatus } from 'src/product/product.enum';
import { Request } from 'express';
import { DomainService } from 'src/domain/domain.service';

@Injectable()
export class BestSellerService implements OnModuleInit {
    private productService: ProductService
    constructor(
        @InjectModel(BestSeller.name) private readonly bestSellerModel: Model<BestSeller>,
        private readonly orderService: OrderService,
        // private readonly settingService: SettingService,
        private readonly domainService: DomainService,
        private moduleRef: ModuleRef

    ) { }

    onModuleInit() {
        this.productService = this.moduleRef.get(ProductService, { strict: false });
    }

    async getBestSeller(domain: string, slug: string) {
        try {
            let main = domain
            if (!slug) {
                const indexSlice = domain.indexOf('.')
                main = domain.slice(indexSlice + 1)
            }
            const foundDomain = await this.domainService.findOne({ filter: { domain } })
            if (!foundDomain) {
                throw new BaseApiException({
                    message: BaseNoti.DOMAIN.DOMAIN_NOT_EXISTS
                })
            }
            const listSeller = await this.getListBestSeller(foundDomain.adminId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listSeller
            }
        } catch (error) {
            throw error
        }
    }

    async addToBestSeller(adminId: string, listProductId: string[]) {
        try {
            for (let productId of listProductId) {
                const foundProduct = await this.productService.findOne({ filter: { _id: productId } })
                if (!foundProduct) continue
                const isExist = await this.bestSellerModel.exists({ productId, adminId })
                if (isExist) continue
                const lastBestSeller = await this.bestSellerModel.find({ adminId }).sort({ serial: -1 }).limit(1).lean()
                const newBestSeller = new this.bestSellerModel({
                    adminId,
                    productId,
                    serial: lastBestSeller && lastBestSeller.length && lastBestSeller[0] ? lastBestSeller[0].serial + 1 : 1
                })
                await newBestSeller.save()
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async changePosition(adminId: string, bestSellerId: string, changePositionDto: ChangePositionDto) {
        try {
            const { status } = changePositionDto
            const foundBestSeller = await this.bestSellerModel.findOne({ _id: bestSellerId, adminId })
            if (!foundBestSeller) {
                throw new BaseApiException({
                    message: BaseNoti.BESTSELLER.BESTSELLER_NOT_EXISTS
                })
            }
            await this.productService.findOneProduct({ _id: foundBestSeller?.productId })
            let changeField
            let changeFieldTogether
            let changeTogether
            if (status == 1) {
                changeField = { $inc: { serial: -1 } }
                changeFieldTogether = { $inc: { serial: 1 } }
                changeTogether = await this.bestSellerModel.findOne({ serial: foundBestSeller?.serial - 1 })
            } else if (status == 2) {
                changeField = { $inc: { serial: 1 } }
                changeFieldTogether = { $inc: { serial: -1 } }
                changeTogether = await this.bestSellerModel.findOne({ serial: foundBestSeller?.serial + 1 })
            }
            if (!changeTogether) {
                throw new BaseApiException({
                    message: BaseNoti.BESTSELLER.STATUS_ERROR
                })
            }
            await Promise.all([
                this.bestSellerModel.findByIdAndUpdate(bestSellerId, changeField),
                this.bestSellerModel.findByIdAndUpdate(changeTogether._id, changeFieldTogether)
            ])
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            console.log('error :>> ', error);
            throw error
        }
    }

    async deleteBestseller(adminId: string, bestSellerId: string) {
        try {
            const foundBestSeller = await this.bestSellerModel.findOne({ _id: bestSellerId, adminId })
            if (!foundBestSeller) {
                throw new BaseApiException({
                    message: BaseNoti.BESTSELLER.BESTSELLER_NOT_EXISTS
                })
            }
            const { serial } = foundBestSeller
            const list = await this.bestSellerModel.find({ serial: { $gt: serial }, _id: { $ne: bestSellerId } }).select('id')
            const listId = list.map(best => best._id)
            await Promise.all([
                this.bestSellerModel.updateMany({ _id: { $in: listId } }, { $inc: { serial: -1 } }),
                this.bestSellerModel.findByIdAndDelete(bestSellerId)
            ])
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }


    async getListBestSeller(adminId: string) {
        try {
            const listSeller = await this.bestSellerModel.find({ adminId }).sort({ serial: 1 })
                .populate([{
                    path: 'productId', select: 'productName slug listImageId categoryId listVariantOptionId listVariant',
                    populate: [
                        { path: 'listVariant', populate: [{ path: 'imageId' }] },
                        { path: 'categoryId' },
                        { path: 'listImageId' },
                    ]
                }])
            return listSeller
        } catch (error) {
            throw error
        }
    }

}
