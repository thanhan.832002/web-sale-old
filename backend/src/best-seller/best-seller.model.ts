import { ApiProperty } from "@nestjs/swagger"
import { Transform } from "class-transformer"
import { IsNotEmpty, IsNumber, Validate } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsObjectId } from "src/base/custom-validator"

export const BestSellerSchema: Schema = new Schema({
    serial: {
        type: Number
    },
    productId: {
        type: Types.ObjectId,
        ref: 'Product'
    },
    adminId: {
        type: Types.ObjectId,
        ref: 'User'
    }
}, { ...schemaOptions, collection: 'BestSeller' })


export class BestSeller extends Document {
    @ApiProperty()
    @IsNotEmpty()
    serial: number
    @ApiProperty()
    @IsNotEmpty()
    productId: string

    @ApiProperty()
    @IsNotEmpty()
    @Validate(IsObjectId)
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    adminId: string
}