import { Body, Controller, Get, Put, Post, Delete, Param, UseGuards, Req, Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AddToBestsellerDto, ChangePositionDto } from './best-seller.dto';
import { BestSellerService } from './best-seller.service';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { Request } from 'express';
import { GetDomain } from 'src/base/decorator/get-domain.decorator';
import { SlugQuery } from 'src/base/interfaces/base-query.interface';

@Controller('best-seller')
@ApiTags('Best Seller')
export class BestSellerController {
    constructor(
        private readonly bestSellerService: BestSellerService
    ) { }

    @Get('list')
    getListBestSeller(
        @GetDomain() domain: string,
        @Query() { slug }: SlugQuery
    ) {
        return this.bestSellerService.getBestSeller(domain, slug)
    }

    @Post('add')
    @ApiBearerAuth()
    @UseGuards(AdminGuard)
    @UseGuards(AuthGuard('jwt'))
    addToBestSeller(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() { listProductId }: AddToBestsellerDto) {
        return this.bestSellerService.addToBestSeller(userId, listProductId)
    }

    @Put('change-position/:bestSellerId')
    @ApiBearerAuth()
    @UseGuards(AdminGuard)
    @UseGuards(AuthGuard('jwt'))
    changePosition(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('bestSellerId') bestSellerId: string,
        @Body() changePositionDto: ChangePositionDto) {
        return this.bestSellerService.changePosition(userId, bestSellerId, changePositionDto)
    }

    @Delete('delete/:bestSellerId')
    @ApiBearerAuth()
    @UseGuards(AdminGuard)
    @UseGuards(AuthGuard('jwt'))
    deleteBestseller(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('bestSellerId') bestSellerId: string) {
        return this.bestSellerService.deleteBestseller(userId, bestSellerId)
    }

}
