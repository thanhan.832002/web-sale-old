import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { OrderModule } from 'src/order/order.module';
import { BestSeller, BestSellerSchema } from './best-seller.model';
import { BestSellerService } from './best-seller.service';
import { ProductModule } from 'src/product/product.module';
import { BestSellerController } from './best-seller.controller';
import { DomainModule } from 'src/domain/domain.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: BestSeller.name, schema: BestSellerSchema }]),
    OrderModule,
    ProductModule,
    DomainModule
  ],
  providers: [BestSellerService],
  exports: [BestSellerService],
  controllers: [BestSellerController],
})
export class BestSellerModule {

}
