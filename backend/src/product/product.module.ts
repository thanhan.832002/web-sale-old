import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GalleryModule } from 'src/gallery/gallery.module';
import { ReviewModule } from 'src/review/review.module';
import { VariantOptionModule } from 'src/variant-option/variant-option.module';
import { VariantModule } from 'src/variant/variant.module';
import { ProductVariantOptionController } from './product-variant-option/product-variant-option.controller';
import { ProductVariantController } from './product-variant/product-variant.controller';
import { ProductAdminController } from './product-admin.controller';
import { Product, ProductSchema } from './product.model';
import { ProductService } from './product.service';
import { ProductSellerController } from './product-seller.controller';
import { VariantSellerModule } from 'src/variant-seller/variant-seller.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Product.name, schema: ProductSchema }]),
    GalleryModule,
    VariantOptionModule,
    VariantModule,
    VariantSellerModule
  ],
  controllers: [ProductAdminController, ProductSellerController, ProductVariantOptionController, ProductVariantController],
  providers: [ProductService],
  exports: [ProductService]
})
export class ProductModule { }
