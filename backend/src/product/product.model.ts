import { Schema, Types, Document } from "mongoose";
import { schemaOptions } from "src/base/base.shema";
import { Variant } from "src/variant/variant.model";
import { ProductStatus } from "./product.enum";

export const ProductSchema: Schema = new Schema({
    categoryId: {
        type: Types.ObjectId,
        ref: 'Category'
    },
    productName: {
        type: String,
        defaut: true,
        index: true
    },
    // slug: {
    //     type: String,
    //     index: true
    // },
    productStatus: {
        type: Number,
        enum: ProductStatus,
        index: true,
        default: ProductStatus.editing
    },
    listVariant: {
        type: [Types.ObjectId],
        ref: 'Variant'
    },
    listVariantOptionId: {
        type: [Types.ObjectId],
        ref: 'VariantOption'
    },
    description: {
        type: String
    },
    listImageId: {
        type: [Types.ObjectId],
        ref: 'Gallery'
    },
    sku: {
        type: String
    },
    language: {
        type: String,
        default: 'EN'
    },
    adminId: {
        type: Types.ObjectId,
        index: true,
        ref: 'User'
    }
}, { ...schemaOptions, Category: 'Product' })

export class Product extends Document {
    categoryId: string
    productName: string
    // slug: string
    productStatus: number
    listVariant: string[] | Variant[]
    listVariantOptionId: string[]
    description: string
    listImageId: string[]
    sku: string
    language: string
    adminId: string
}