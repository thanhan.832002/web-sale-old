import { Body, Controller, Param, Post, Put, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { UpdateListVariantOptionDto } from 'src/variant-option/variant-option.dto';
import { ProductService } from '../product.service';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';

@Controller('variant-option')
@ApiTags('Variant Option')
@ApiBearerAuth()
@UseGuards(AdminGuard)
@UseGuards(AuthGuard('jwt'))
export class ProductVariantOptionController {
    constructor(
        private readonly productService: ProductService
    ) { }

    @Put('update')
    updateListVariantOption(
        @GetUser() { userId, role }: JwtTokenDecrypted,
        @Body() updateListVariantOptionDto: UpdateListVariantOptionDto) {
        return this.productService.updateListVariantOption(userId, role, updateListVariantOptionDto)
    }
}
