import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { ChangeCategoryDto, EditProductDto, ListProductQuery, NewProductDto, } from './product.dto';
import { ProductService } from './product.service';


@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('product')
@ApiTags('Product Admin')

export class ProductAdminController {
    constructor(private readonly productService: ProductService) { }

    @UseGuards(AdminGuard)
    @Post('create-product')
    newProduct(@GetUser() { userId }: JwtTokenDecrypted, @Body() newProductDto: NewProductDto) {
        return this.productService.addProduct(userId, newProductDto)
    }

    @UseGuards(AdminGuard)
    @Post('edit-product/:productId')
    editProduct(@GetUser() { userId }: JwtTokenDecrypted, @Param('productId') productId: string, @Body() editProductDto: EditProductDto) {
        return this.productService.editProduct(userId, productId, editProductDto)
    }

    @Get('product-detail/:productId')
    adminGetProductDetail(@GetUser() { userId, role }: JwtTokenDecrypted, @Param('productId') productId: string) {
        return this.productService.adminGetProductDetail(userId, role, productId)
    }

    @UseGuards(AdminGuard)
    @Get('admin/list-product')
    adminGetListProduct(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: ListProductQuery) {
        return this.productService.adminGetListProduct(userId, query)
    }

    @UseGuards(AdminGuard)
    @Delete('delete-product/:productId')
    deleteproduct(@GetUser() { userId }: JwtTokenDecrypted, @Param('productId') productId: string) {
        return this.productService.deleteproduct(userId, productId)
    }

}
