import { ChangeCategoryDto, EditProductDto, ListProductQuery, ListProductSellerQuery, NewProductDto } from './product.dto';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Product } from './product.model';
import { GalleryService } from 'src/gallery/gallery.service';
import { Model, Types } from 'mongoose';
import * as Promise from "bluebird";
import { VariantOptionService } from 'src/variant-option/variant-option.service';
import { VariantService } from 'src/variant/variant.service';
import { UpdateListVariantOptionDto } from 'src/variant-option/variant-option.dto';
import { UpdateListVariantDto } from 'src/variant/variant.dto';
import { DataFilter } from 'src/base/interfaces/data.interface';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { BaseService } from 'src/base/base.service';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';
import { ProductStatus } from './product.enum';
import { VariantSellerService } from 'src/variant-seller/variant-seller.service';
import { StoreService } from 'src/store/store.service';
import { ModuleRef } from '@nestjs/core';
import { Role } from 'src/base/enum/roles.enum';
import { UserService } from 'src/user/user.service';
import { Timeout } from '@nestjs/schedule';
@Injectable()
export class ProductService extends BaseService<Product> implements OnModuleInit {
    private storeService: StoreService
    private userService: UserService
    constructor(
        @InjectModel(Product.name) private readonly productModel: Model<Product>,
        private readonly galleryService: GalleryService,
        private readonly variantOptionService: VariantOptionService,
        private readonly variantService: VariantService,
        private readonly variantSellerService: VariantSellerService,
        private moduleRef: ModuleRef

    ) { super(productModel) }

    onModuleInit() {
        this.storeService = this.moduleRef.get(StoreService, { strict: false });
        this.userService = this.moduleRef.get(UserService, { strict: false });
    }

    async addProduct(userId: string, newProductDto: NewProductDto) {
        try {
            const { categoryId, listVariant, listVariantOption, sku } = newProductDto
            if (!categoryId) delete newProductDto.categoryId
            if (!sku) delete newProductDto.sku
            let newProductFields: any = { ...newProductDto, adminId: userId }
            const newProduct = await new this.productModel(newProductFields)
            if (listVariant?.length && listVariantOption?.length) {
                const now = new Date().getTime()
                let count = 1
                const listVariantDto = listVariant.map(variant => {
                    let sku = variant.sku
                    if (!variant.sku) sku = `${now}-${count}`
                    count++
                    return {
                        ...variant,
                        sku
                    }
                })
                const listcreatedVariantOption = await this.variantOptionService.createListVariantOption(listVariantOption, newProduct._id)
                const listCreatedVariant = await this.variantService.createListVariant(listVariantDto, listcreatedVariantOption, newProduct)
                newProduct.listVariant = listCreatedVariant.map(variant => variant._id)
                newProduct.listVariantOptionId = listcreatedVariantOption.map(option => option._id)
            }
            await newProduct.save()
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: newProduct,
            };

        } catch (error) {
            throw error
        }
    }

    async editProduct(adminId: string, productId: string, editProductDto: EditProductDto) {
        try {
            const { categoryId, productName,/*  slug, */ productStatus, description, listImageId, sku, language } = editProductDto
            const foundProduct = await this.productModel.findOne({ _id: productId, adminId })
            if (!foundProduct) {
                throw new BaseApiException({ message: BaseNoti.PRODUCT.PRODUCT_NOT_EXISTS })
            }
            // if (foundProduct.productStatus != ProductStatus.editing) {
            //     throw new BaseApiException({ message: BaseNoti.PRODUCT.PRODUCT_NOT_EDITING })
            // }
            // await this.cachePageService.removePageCache({ slug: foundProduct.slug })
            const updateFields: any = {}
            if (categoryId) updateFields.categoryId = categoryId
            if (productName) updateFields.productName = productName
            if (language) updateFields.language = language
            if (sku) updateFields.sku = sku
            if (description) updateFields.description = description
            if (productStatus) {
                updateFields.productStatus = productStatus
            }
            if (listImageId && listImageId.length && listImageId[0]) updateFields.listImageId = listImageId
            const updateProduct = await this.productModel.findByIdAndUpdate(foundProduct._id, updateFields, { new: true })
            await this.storeService.updateProductAt(foundProduct._id)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: updateProduct
            };

        } catch (error) {
            throw error
        }
    }

    async adminGetListProduct(adminId: string, query: ListProductQuery) {
        try {
            let { page, limit, search, filter, categoryId, productStatus } = query;
            let filterObject: DataFilter<Partial<Product>> = {
                limit,
                page,
                condition: { adminId },
                population: [
                    {
                        path: 'categoryId',
                    },
                    {
                        path: 'listImageId',
                        select: 'imageUrl'
                    },
                    {
                        path: 'listVariant',
                        select: 'price productCost'//'imageId listVariantOptionId listValue stock price comparePrice productCost fullfillmentCost sku',
                    }
                ],
                selectCols: '-description -listVariantOptionId'
            };
            if (categoryId) {
                filterObject.condition = { ...filterObject.condition, categoryId }
            }
            if (productStatus) {
                filterObject.condition = { ...filterObject.condition, productStatus }
            }
            if (filter) {
                let { sort } = filter
                if (sort) {
                    if (sort == 1) filterObject.sort = { _id: -1, productStatus: '-1' }
                    if (sort == 2) filterObject.sort = { createdAt: 1, productStatus: '-1' }
                    if (sort == 3) filterObject.sort = { productName: -1, productStatus: '-1' }
                    if (sort == 4) filterObject.sort = { productName: 1, productStatus: '-1' }
                    if (sort == 5) filterObject.sort = { price: 1, productStatus: '-1' }
                    if (sort == 6) filterObject.sort = { price: -1, productStatus: '-1' }
                }
                delete filter.sort
                filterObject.condition = { ...filterObject.condition, ...filter };
            }
            if (search) {
                filterObject.condition = { ...filterObject.condition, productName: { $regex: search, $options: 'i' } }
            }
            const listProduct = await this.getListDocument(filterObject);
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listProduct,
            };
        } catch (error) {
            throw error
        }
    }

    async adminGetProductDetail(userId: string, role: Role, productId: string) {
        try {
            let adminId = userId
            if (role == Role.seller) {
                const foundUser = await this.userService.findUserById(userId)
                adminId = foundUser.adminId
            }
            const foundProduct = await this.productModel.findOne({ _id: productId, adminId }).populate(
                [
                    {
                        path: 'categoryId',
                    },
                    {
                        path: 'listVariantOptionId',
                    },
                    {
                        path: 'listImageId',
                        select: 'imageUrl'
                    },
                    {
                        path: 'listVariant',
                        select: 'imageId listVariantOptionId listValue stock price comparePrice productCost fullfillmentCost sku isSellable',
                        populate: [{ path: 'imageId', select: 'imageUrl' }, { path: 'listVariantOptionId', select: 'name values' }]
                    }
                ]
            )
            if (!foundProduct) {
                throw new BaseApiException({ message: BaseNoti.PRODUCT.PRODUCT_NOT_EXISTS })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: foundProduct
            };
        } catch (error) {
            throw error
        }
    }

    async findProductCanCreateProductSeller(productId: string) {
        try {
            const foundProduct: Product = await this.productModel.findById(productId).populate({ path: 'listVariant listVariantOptionId' })
            if (!foundProduct) {
                throw new BaseApiException({ message: BaseNoti.PRODUCT.PRODUCT_NOT_EXISTS })
            }
            if (foundProduct.productStatus !== ProductStatus.publishing) {
                if (!foundProduct) {
                    throw new BaseApiException({ message: BaseNoti.PRODUCT.PRODUCT_NOT_PUBLISH })
                }
            }
            return foundProduct
        } catch (error) {
            throw error
        }
    }

    async findOneProduct(filter: any) {
        try {
            const foundProduct = await this.productModel.findOne(filter).populate([
                {
                    path: 'listImageId',
                    select: 'imageUrl'
                },
                {
                    path: 'listVariant',
                    select: 'imageId listVariantOptionId listValue stock price comparePrice productCost fullfillmentCost sku',
                    populate: [{ path: 'imageId', select: 'imageUrl' }, { path: 'listVariantOptionId', select: 'name values' }]
                },])
            if (!foundProduct) {
                throw new BaseApiException({ message: BaseNoti.PRODUCT.PRODUCT_NOT_EXISTS })
            }
            return foundProduct
        } catch (error) {
            throw error
        }
    }

    async deleteproduct(adminId: string, productId: string) {
        try {
            const foundProduct = await this.productModel.findOne({ _id: productId, adminId })
            if (!foundProduct) {
                throw new BaseApiException({ message: BaseNoti.PRODUCT.PRODUCT_NOT_EXISTS })
            }
            await Promise.all([
                this.productModel.findByIdAndDelete(foundProduct._id),
                this.variantService.deleteAllVariantProduct({ productId }),
                // this.reviewService.deleteReviewProduct(productId)
            ])

            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async updateListVariantOption(userId: string, role: Role, { listVariantOption, productId, listVariant }: UpdateListVariantOptionDto) {
        try {
            let adminId = userId
            if (role == Role.seller) {
                const foundUser = await this.userService.findUserById(userId)
                adminId = foundUser.adminId
            }
            const foundProduct = await this.productModel.findOne({ _id: productId, adminId })
            if (!foundProduct) {
                throw new BaseApiException({
                    message: BaseNoti.PRODUCT.PRODUCT_NOT_EXISTS
                })
            }
            this.storeService.removeCacheProductId(productId)
            const { listCreatedVariantId, listcreatedVariantOption } = await this.variantOptionService.updateListVariantOption({ listVariantOption, product: foundProduct, listVariant })
            await this.productModel.findByIdAndUpdate(productId, { listVariant: listCreatedVariantId, listVariantOptionId: listcreatedVariantOption.map(option => option._id) })
            await this.storeService.updateProductAt(foundProduct._id)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async getListVariantProduct(userId: string, role: Role, productId: string) {
        try {
            let adminId = userId
            if (role == Role.seller) {
                const foundUser = await this.userService.findUserById(userId)
                adminId = foundUser.adminId
            }
            const foundProduct = await this.productModel.findOne({ _id: productId, adminId })
            if (!foundProduct) {
                throw new BaseApiException({
                    message: BaseNoti.PRODUCT.PRODUCT_NOT_EXISTS
                })
            }
            const listVariant = await this.variantService.getListVariantProduct(productId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listVariant
            }
        } catch (error) {
            throw error
        }
    }

    async updateListVariant(userId: string, role: Role, updateListVariantDto: UpdateListVariantDto) {
        try {
            let adminId = userId
            if (role == Role.seller) {
                const foundUser = await this.userService.findUserById(userId)
                adminId = foundUser.adminId
            }
            const { listVariant, productId } = updateListVariantDto
            const listVariantId = listVariant.map(variant => variant.variantId)
            const foundProduct = await this.productModel.findOne({ _id: productId, adminId, listVariant: { $all: listVariantId } })
            if (!foundProduct) {
                throw new BaseApiException({
                    message: BaseNoti.PRODUCT.PRODUCT_NOT_EXISTS
                })
            }
            this.storeService.removeCacheProductId(productId)
            await Promise.map(listVariant, async (variant) => {
                const { variantId, isSellable } = variant
                await Promise.all([
                    this.variantService.updateVariant(variantId, { ...variant }),
                    this.variantSellerService.updateMany({ variantId }, { isSellable })
                ])
            }, { concurrency: 25 })
            await this.storeService.updateProductAt(foundProduct._id)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async sellerGetListProduct(userId: string, query: ListProductSellerQuery) {
        try {
            const foundUser = await this.userService.findUserById(userId)
            const adminId = foundUser.adminId
            let { page, limit, search, filter, priceFrom, priceTo, productCostFrom, productCostTo } = query;
            let filterObject: DataFilter<Partial<Product>> = {
                limit,
                page,
                condition: { productStatus: { $nin: [ProductStatus.editing, ProductStatus.close] }, adminId },
                population: [
                    {
                        path: 'categoryId',
                    },
                    {
                        path: 'listVariantOptionId',
                    },
                    {
                        path: 'listImageId',
                        select: 'imageUrl'
                    },
                    {
                        path: 'listVariant',
                        select: 'imageId listVariantOptionId listValue stock price comparePrice productCost fullfillmentCost sku',
                        populate: [{ path: 'imageId', select: 'imageUrl' }, { path: 'listVariantOptionId', select: 'name values' }]
                    }
                ],
                selectCols: '-description'
            };
            if (filter) {
                let { sort } = filter
                if (sort) {
                    if (sort == 1) filterObject.sort = { _id: -1, productStatus: '-1' }
                    if (sort == 2) filterObject.sort = { createdAt: 1, productStatus: '-1' }
                    if (sort == 3) filterObject.sort = { name: -1, productStatus: '-1' }
                    if (sort == 4) filterObject.sort = { name: 1, productStatus: '-1' }
                    if (sort == 5) filterObject.sort = { price: 1, productStatus: '-1' }
                    if (sort == 6) filterObject.sort = { price: -1, productStatus: '-1' }
                }
                delete filter.sort
                filterObject.condition = { ...filterObject.condition, ...filter };
            }
            let price
            if (priceFrom) {
                price = { $gte: priceFrom }
            }
            if (priceTo) {
                price = { ...price, $lte: priceTo }
            }
            let productCost
            if (productCostFrom) {
                productCost = { $gte: productCostFrom }
            }
            if (productCostTo) {
                productCost = { ...productCost, $lte: productCostTo }
            }
            if (price || productCost) {
                let listVariant = []
                if (price && productCost) listVariant = await this.variantService.findListVariant({ price, productCost })
                else if (!price) listVariant = await this.variantService.findListVariant({ productCost })
                else if (!productCost) listVariant = await this.variantService.findListVariant({ price })
                filterObject.condition = {
                    ...filterObject.condition,
                    listVariant: { $in: listVariant.map(variant => variant._id) },
                }
            }
            if (search) {
                filterObject.condition = { ...filterObject.condition, productName: { $regex: search, $options: 'i' } }
            }
            const listProduct = await this.getListDocument(filterObject);
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listProduct,
            };
        } catch (error) {
            throw error
        }
    }

    async getImageUrlFromProducId(productId: string) {
        try {
            const product = await this.productModel.findById(productId).select('listImageId').populate({ path: 'listImageId', select: 'imageUrl' }).lean()
            if (product && product.listImageId && product.listImageId.length) {
                //@ts-ignore
                return product.listImageId[0].imageUrl
            }
        } catch (error) {
            throw error
        }
    }

    async changeCategory({ listProductId, categoryId }: ChangeCategoryDto) {
        try {
            await this.productModel.updateMany({ _id: { $in: listProductId } }, { categoryId })
        } catch (error) {
            throw error
        }
    }

    async findImageUrlById(imageId) {
        try {
            const url = await this.galleryService.getImageUrl(imageId)
            return url
        } catch (error) {
            throw error
        }
    }

    async findVariantById(productId: string, variantId: string) {
        try {
            const foundVariant = await this.variantService.findVariantId(productId, variantId)
            return foundVariant
        } catch (error) {
            throw error
        }
    }

    async countProduct(condition: any) {
        try {
            const count = await this.productModel.countDocuments(condition)
            return count || 0
        } catch (error) {
            throw error
        }
    }

    async updateOne(filter: any, updateDto: Partial<Product>) {
        try {
            await this.productModel.findOneAndUpdate(filter, updateDto)
        } catch (error) {
            throw error
        }
    }

    async updateMany(filter: any, updateDto: Partial<Product>) {
        try {
            await this.productModel.updateMany(filter, updateDto)
        } catch (error) {
            throw error
        }
    }

    async findMany(filter: any) {
        try {
            const find = await this.productModel.find(filter)
            return find
        } catch (error) {
            throw error
        }
    }

    async aggregate(filter: any) {
        try {
            const find = await this.productModel.aggregate(filter)
            return find
        } catch (error) {
            throw error
        }
    }

    async listProductId() {
        try {
            const list = await this.productModel.find().select('id')
            return list.map(product => product._id) || []
        } catch (error) {
            throw error
        }
    }

    async findTestMail() {
        try {
            const found = await this.productModel.find({}).limit(1).select('productName listVariant').populate([
                { path: 'listVariant', select: 'price comparePrice imageId listValue', populate: 'imageId' }
            ])
            return found
        } catch (error) {
            throw error
        }
    }

    async findProductId(productId: string) {
        try {
            const found = await this.productModel.findById(productId)
            if (!found) throw new BaseApiException({
                message: BaseNoti.PRODUCT.PRODUCT_NOT_EXISTS
            })
            return found
        } catch (error) {
            throw error
        }
    }

}
