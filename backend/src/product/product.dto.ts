import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger"
import { Transform, Type } from "class-transformer"
import { IsArray, IsEnum, IsNotEmpty, IsNumber, IsOptional, Validate, ValidateNested } from "class-validator"
import { IsListObjectId, IsObjectId } from "src/base/custom-validator"
import { BaseQuery } from "src/base/interfaces/base-query.interface"
import { OneVariantOptionDto } from "src/variant-option/variant-option.dto"
import { OneVariantDto } from "src/variant/variant.dto"
import { ProductStatus } from "./product.enum"
export class NewProductDto {
    @ApiPropertyOptional()
    @IsOptional()
    categoryId?: string

    @ApiProperty()
    @IsNotEmpty()
    productName: string

    // @ApiProperty()
    // @IsNotEmpty()
    // slug: string

    @ApiPropertyOptional()
    @IsEnum(ProductStatus)
    @Type(() => Number)
    @IsOptional()
    productStatus: number

    @ApiProperty({ type: [OneVariantOptionDto] })
    @ValidateNested()
    @Type(() => OneVariantOptionDto)
    @IsArray()
    @IsOptional()
    listVariantOption: OneVariantOptionDto[]

    @ApiProperty({ type: [OneVariantDto] })
    @ValidateNested()
    @Type(() => OneVariantDto)
    @IsArray()
    @IsOptional()
    listVariant: OneVariantDto[]

    @ApiProperty()
    @IsNotEmpty()
    description: string

    @ApiPropertyOptional()
    @IsOptional()
    sku: string

    @ApiPropertyOptional()
    @Validate(IsListObjectId)
    @IsOptional()
    listImageId: string[]

    listVariantOptionId: string[]

    @ApiPropertyOptional()
    @IsOptional()
    language: string
}

export class EditProductDto {
    @ApiPropertyOptional()
    @IsOptional()
    categoryId?: string

    @ApiPropertyOptional()
    @IsOptional()
    productName: string

    // @ApiPropertyOptional()
    // @IsOptional()
    // slug: string

    @ApiPropertyOptional()
    @IsEnum(ProductStatus)
    @IsOptional()
    productStatus: number

    @ApiPropertyOptional()
    @IsOptional()
    sku: string

    @ApiPropertyOptional()
    @IsOptional()
    description: string

    @ApiPropertyOptional()
    @Validate(IsListObjectId)
    @IsOptional()
    listImageId: string[]

    @ApiPropertyOptional()
    @IsOptional()
    language: string
}

export class ListProductQuery extends BaseQuery {
    @ApiPropertyOptional()
    @Validate(IsObjectId)
    @IsOptional()
    categoryId: string

    @ApiPropertyOptional()
    @IsOptional()
    productStatus: ProductStatus
}

export class ListProductId {
    @ApiProperty({
        type: [String]
    })
    @IsOptional()
    listProductId: string[]
}

export class ListProductSellerQuery extends BaseQuery {
    @ApiPropertyOptional()
    @Type(() => Number)
    @IsOptional()
    priceFrom: number

    @ApiPropertyOptional()
    @Type(() => Number)
    @IsOptional()
    priceTo: number

    @ApiPropertyOptional()
    @Type(() => Number)
    @IsOptional()
    productCostFrom: number

    @ApiPropertyOptional()
    @Type(() => Number)
    @IsOptional()
    productCostTo: number
}

export class ChangeCategoryDto extends ListProductId {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    categoryId: string
}