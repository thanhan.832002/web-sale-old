import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SellerGuard } from 'src/base/guard/seller.guard';
import { ListProductSellerQuery } from './product.dto';
import { ProductService } from './product.service';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';

@ApiBearerAuth()
@UseGuards(SellerGuard)
@UseGuards(AuthGuard('jwt'))
@Controller('product-seller')
@ApiTags('Product Seller')
export class ProductSellerController {
    constructor(
        private readonly productService: ProductService,
    ) { }

    @Get('list')
    sellerGetListProduct(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: ListProductSellerQuery) {
        return this.productService.sellerGetListProduct(userId, query)
    }

}
