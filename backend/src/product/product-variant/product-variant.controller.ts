import { Body, Controller, Put, Get, Param, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { UpdateListVariantDto } from 'src/variant/variant.dto';
import { ProductService } from '../product.service';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';
@ApiBearerAuth()
@UseGuards(AdminGuard)
@UseGuards(AuthGuard('jwt'))
@Controller('variant')
@ApiTags('Variant')
export class ProductVariantController {
    constructor(
        private readonly productService: ProductService,
    ) { }

    @Get('list/:productId')
    getListVariantProduct(
        @GetUser() { userId, role }: JwtTokenDecrypted,
        @Param('productId') productId: string) {
        return this.productService.getListVariantProduct(userId, role, productId)
    }

    @Put('update-list')
    updateListVariant(
        @GetUser() { userId, role }: JwtTokenDecrypted,
        @Body() updateListVariantDto: UpdateListVariantDto) {
        return this.productService.updateListVariant(userId, role, updateListVariantDto)
    }

}
