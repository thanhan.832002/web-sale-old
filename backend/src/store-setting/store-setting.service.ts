import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { StoreSetting } from './store-setting.model';
import { Request } from 'express';

@Injectable()
export class StoreSettingService {
    constructor(
        @InjectModel(StoreSetting.name) private readonly storeSettingModel: Model<StoreSetting>
    ) {
    }

    async findStoreSetting(storeId: string) {
        try {
            const foundStoreSetting = await this.storeSettingModel.findOne({ storeId }).populate('logo')
            return foundStoreSetting
        } catch (error) {
            throw error
        }
    }

    async createDefaultStoreSetting(storeId: string) {
        try {
            const storeSetting = new this.storeSettingModel({
                storeId,
                phoneNumber: '',
                companyInfo: '',
                // logo: ''
            })
            await storeSetting.save()
        } catch (error) {
            throw error
        }
    }

    async updateStoreSetting(storeId: string, updateStoreField: Partial<StoreSetting>) {
        try {
            await this.storeSettingModel.findOneAndUpdate({ storeId }, updateStoreField)
        } catch (error) {
            throw error
        }
    }
}
