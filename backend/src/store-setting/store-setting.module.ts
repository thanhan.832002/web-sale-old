import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StoreSettingController } from './store-setting.controller';
import { StoreSetting, StoreSettingSchema } from './store-setting.model';
import { StoreSettingService } from './store-setting.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: StoreSetting.name, schema: StoreSettingSchema }])
  ],
  controllers: [StoreSettingController],
  providers: [StoreSettingService],
  exports: [StoreSettingService]
})
export class StoreSettingModule { }
