import { ApiProperty } from "@nestjs/swagger"
import { Type } from "class-transformer"
import { IsNotEmpty, IsNumber, Validate, IsOptional } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsObjectId } from "src/base/custom-validator"

export const StoreSettingSchema: Schema = new Schema({
    storeId: {
        type: Types.ObjectId,
        index: true,
        ref: 'Store'
    },
    phoneNumber: String,
    companyInfo: String,
    logo: {
        type: Types.ObjectId,
        ref: 'Gallery'
    }
}, { ...schemaOptions, collection: 'StoreSetting' })


export class StoreSetting extends Document {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    storeId: string

    @ApiProperty()
    @IsOptional()
    phoneNumber: string

    @ApiProperty()
    @IsOptional()
    companyInfo: string

    @ApiProperty()
    @IsOptional()
    logo: string
}
