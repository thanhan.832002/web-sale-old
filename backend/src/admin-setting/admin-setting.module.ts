import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AdminSettingController } from './admin-setting.controller';
import { AdminSettingService } from './admin-setting.service';
import { AdminSetting, AdminSettingSchema } from './admin-setting.model';
import { SharedModule } from 'src/base/shared.module';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: AdminSetting.name, schema: AdminSettingSchema }]),
        SharedModule
    ],
    providers: [AdminSettingService],
    controllers: [AdminSettingController],
    exports: [AdminSettingService]
})
export class AdminSettingModule { }