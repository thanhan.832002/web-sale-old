import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsNotEmpty, IsOptional, Validate } from "class-validator";
import { Schema, Document, Types } from "mongoose";
import { schemaOptions } from "src/base/base.shema";
import { IsObjectId } from "src/base/custom-validator";

export const AdminSettingSchema: Schema = new Schema({
    adminId: {
        type: Types.ObjectId,
        ref: 'User'
    },
    timezone: {
        type: String,
        default: '-7'
    },
    sendgridApikey: String,
    ipServer: String,
    zerosslKey: String,
    seventeenTrackKey: String
}, { ...schemaOptions, ... { collection: 'AdminSetting' } })

export class AdminSetting extends Document {
    @ApiProperty()
    @IsNotEmpty()
    @Validate(IsObjectId)
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    adminId: string

    @ApiPropertyOptional()
    @IsOptional()
    timezone: string

    @ApiPropertyOptional()
    @IsOptional()
    sendgridApikey: string

    @ApiPropertyOptional()
    @IsOptional()
    ipServer: string

    @ApiPropertyOptional()
    @IsOptional()
    zerosslKey: string

    @ApiPropertyOptional()
    @IsOptional()
    seventeenTrackKey: string
}

