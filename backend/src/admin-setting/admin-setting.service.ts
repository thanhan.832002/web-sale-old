import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AdminSetting } from './admin-setting.model';
import BaseNoti from 'src/base/notify';
import { UpdateSettingDto } from './admin-setting.dto';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import { Role } from 'src/base/enum/roles.enum';
import { UserService } from 'src/user/user.service';
import { ModuleRef } from '@nestjs/core';
import { Timeout } from '@nestjs/schedule';
import { CacheService } from 'src/base/caching/cache.service';
import { KeyRedisEnum } from 'src/view/view.enum';

@Injectable()
export class AdminSettingService implements OnModuleInit {
  private userService: UserService;
  constructor(
    @InjectModel(AdminSetting.name)
    private readonly adminSettingModel: Model<AdminSetting>,
    private readonly cacheService: CacheService,
    private moduleRef: ModuleRef,
  ) {}

  onModuleInit() {
    this.userService = this.moduleRef.get(UserService, { strict: false });
  }

  async getSetting(userId: string, role: Role) {
    try {
      let setting: any = {
        timezone: '-7',
        ipServer: '',
        sendgridApikey: '',
        zerosslKey: '',
        seventeenTrackKey: '',
      };
      let adminId = userId;
      if (role == Role.seller) {
        const founduser = await this.userService.findUserById(userId);
        adminId = founduser.adminId;
      }
      const [foundSetting, all] = await Promise.all([
        this.adminSettingModel.findOne({ adminId }),
        this.adminSettingModel.findOne({}),
      ]);
      if (foundSetting) {
        const { timezone, sendgridApikey, seventeenTrackKey } = foundSetting;
        if (timezone) setting.timezone = timezone;
        if (sendgridApikey) setting.sendgridApikey = sendgridApikey;
        if (seventeenTrackKey) setting.seventeenTrackKey = seventeenTrackKey;
      }
      if (all) {
        const { ipServer, zerosslKey } = all;
        if (ipServer) setting.ipServer = ipServer;
        if (zerosslKey) setting.zerosslKey = zerosslKey;
      }
      return {
        message: BaseNoti.GLOBAL.SUCCESS,
        data: setting,
      };
    } catch (error) {
      throw error;
    }
  }

  async updateSetting(userId: string, updateSettingDto: UpdateSettingDto) {
    try {
      const settingFound = await this.adminSettingModel.findOne({
        adminId: userId,
      });
      let setting: AdminSetting;
      if (!settingFound) {
        //ADD NEW CONTACT
        setting = await new this.adminSettingModel({
          ...updateSettingDto,
          adminId: userId,
        });
        await setting.save();
      } else {
        let listKeyDel: string[] = [];
        const {
          timezone,
          sendgridApikey,
          /*  ipServer, zerosslKey, */ seventeenTrackKey,
        } = updateSettingDto;
        if (timezone && timezone != settingFound.timezone) {
          listKeyDel.push(`${KeyRedisEnum.timezone}-${userId}`);
        } else {
          delete updateSettingDto.timezone;
        }
        if (sendgridApikey) {
          listKeyDel.push(`${KeyRedisEnum.sendgrid}-${userId}`);
        } else {
          updateSettingDto.sendgridApikey = '';
        }
        // if (!ipServer) updateSettingDto.ipServer = ''
        // if (!zerosslKey) updateSettingDto.zerosslKey = ''
        if (!seventeenTrackKey) updateSettingDto.seventeenTrackKey = '';
        setting = await this.adminSettingModel.findByIdAndUpdate(
          settingFound._id,
          updateSettingDto,
          { new: true },
        );
        listKeyDel.length &&
          (await this.cacheService.remove({ key: listKeyDel }));
      }
      return {
        message: BaseNoti.GLOBAL.SUCCESS,
        data: setting,
      };
    } catch (error) {
      throw error;
    }
  }

  async getApikeySendgrid(adminId: string) {
    try {
      const foundKeyRedis = await this.cacheService.get({
        key: `${KeyRedisEnum.sendgrid}-${adminId}`,
      });
      if (foundKeyRedis) {
        return foundKeyRedis;
      }
      const setting = await this.adminSettingModel.findOne({ adminId });
      const apikeySendgrod = setting?.sendgridApikey;
      apikeySendgrod &&
        (await this.cacheService.set({
          key: `${KeyRedisEnum.sendgrid}-${adminId}`,
          value: apikeySendgrod,
          expiresIn: 30 * 60,
        }));
      if (!apikeySendgrod) {
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.APIKEY_NOT_EXISTS,
        });
      }
      return apikeySendgrod;
    } catch (error) {
      throw error;
    }
  }

  async getIpServer(adminId: string) {
    try {
      const setting = await this.adminSettingModel.findOne({});
      const ip = setting?.ipServer;
      if (!ip) {
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.IPSERVER_NOT_EXISTS,
        });
      }
      return ip;
    } catch (error) {
      throw error;
    }
  }

  async getZerosslKey(adminId: string) {
    try {
      const setting = await this.adminSettingModel.findOne({});
      const zerosslKey = setting?.zerosslKey;
      if (!zerosslKey) {
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.ZEROSSL_KEY_NOT_EXISTS,
        });
      }
      return zerosslKey;
    } catch (error) {
      throw error;
    }
  }

  async getSeventeenTrackKey(adminId: string) {
    try {
      const setting = await this.adminSettingModel.findOne({ adminId });
      const seventeenTrackKey = setting?.seventeenTrackKey;
      if (!seventeenTrackKey) {
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.ZEROSSL_KEY_NOT_EXISTS,
        });
      }
      return seventeenTrackKey;
    } catch (error) {
      return '';
    }
  }

  async getTimezone(adminId: string) {
    try {
      let timezone = '-7';
      const setting = await this.adminSettingModel.findOne({ adminId });
      if (setting?.timezone) timezone = setting.timezone;
      if (isNaN(Number(timezone))) return -7;
      return Number(timezone);
    } catch (error) {
      throw error;
    }
  }

  async getStringTimezone(adminId: string) {
    try {
      let timezone = '-7';
      let stringTimezone = 'US/Arizona';
      const setting = await this.adminSettingModel.findOne({ adminId });
      if (setting?.timezone) timezone = setting.timezone;
      if (timezone == '+1') stringTimezone = 'Atlantic/Cape_Verde';
      if (timezone == '-9') stringTimezone = 'America/Anchorage';
      if (timezone == '-8') stringTimezone = 'America/Chicago';
      if (timezone == '+0') stringTimezone = 'GB';
      if (timezone == '+7') stringTimezone = 'Asia/Ho_Chi_Minh';
      if (timezone == '+8') stringTimezone = 'Asia/Singapore';
      return stringTimezone;
    } catch (error) {
      throw error;
    }
  }
}
