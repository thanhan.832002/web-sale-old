import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsNotEmpty, IsOptional } from "class-validator";

export enum TimezoneEnum {
    'US/Arizona' = '-7',
    'Atlantic/Cape_Verde' = '+1',
    'America/Adak' = '-9',
    'America/Anchorage' = '-8',
    'GB' = '+0',
    'Asia/Ho_Chi_Minh' = '+7',
    'Asia/Singapore' = '+8',
}
export class UpdateSettingDto {
    @ApiProperty()
    @IsOptional()
    timezone: TimezoneEnum

    @ApiPropertyOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsOptional()
    sendgridApikey: string

    // @ApiPropertyOptional()
    // @Transform(({ value }) => {
    //     if (value) return value.trim()
    // })
    // @IsOptional()
    // ipServer: string


    // @ApiPropertyOptional()
    // @Transform(({ value }) => {
    //     if (value) return value.trim()
    // })
    // @IsOptional()
    // zerosslKey: string

    @ApiPropertyOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsOptional()
    seventeenTrackKey: string
}