import { Body, Controller, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { AdminSettingService } from './admin-setting.service';
import { UpdateSettingDto } from './admin-setting.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';

@Controller('admin-setting')
@ApiTags('AdminSetting')
export class AdminSettingController {
    constructor(
        private readonly adminSettingService: AdminSettingService
    ) { }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @Get('get')
    getPackedSettings(
        @GetUser() { userId, role }: JwtTokenDecrypted
    ) {
        return this.adminSettingService.getSetting(userId, role)
    }

    @ApiBearerAuth()
    @UseGuards(AdminGuard)
    @UseGuards(AuthGuard('jwt'))
    @Put('update')
    updateSetting(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() updateSettingDto: UpdateSettingDto
    ) {
        return this.adminSettingService.updateSetting(userId, updateSettingDto)
    }
}
