import { Injectable } from '@nestjs/common';
import { StoreUpsale } from './store-upsale.model';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import BaseNoti from 'src/base/notify';
import { UpdateUpsaleDto } from './store-upsale.dto';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import { CacheService } from 'src/base/caching/cache.service';
import { KeyRedisEnum } from 'src/view/view.enum';

@Injectable()
export class StoreUpsaleService {
    constructor(
        @InjectModel(StoreUpsale.name) private storeUpsaleModel: Model<StoreUpsale>,
        private readonly cacheService: CacheService
    ) { }

    async buyerGetListUpsale(domain: string) {
        try {

        } catch (error) {
            throw error
        }
    }

    async getStoreUpsale(storeId: string) {
        try {
            const found = await this.storeUpsaleModel.findOne({ storeId }).populate([
                { path: 'sellerDiscountId', select: 'name getForMore exp button status' },
                {
                    path: 'listStoreId', select: 'productSellerId storeName status domain id', populate: [{
                        path: 'productSellerId', select: 'productName listImageId listVariantSellerId'
                    }]
                }
            ])
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: found
            }
        } catch (error) {
            throw error
        }
    }

    async buyerGetStoreUpsale(storeId: string) {
        try {
            const found = await this.storeUpsaleModel.findOne({ storeId, status: true }).populate([
                { path: 'sellerDiscountId', select: 'name getForMore exp button status' },
                {
                    path: 'listStoreId', select: 'productSellerId storeName status domain id', populate: [{
                        path: 'productSellerId', select: 'productName listImageId listVariantSellerId', populate: [
                            { path: 'listVariantSellerId', select: 'price comparePrice' },
                            { path: 'listImageId', select: 'imageUrl' }]
                    }]
                }
            ])
            return found
        } catch (error) {
            throw error
        }
    }

    async updateUpsale(storeId: string, updateUpsaleDto: UpdateUpsaleDto) {
        try {
            const found: any = await this.storeUpsaleModel.findOne({ storeId }).populate([
                { path: 'sellerDiscountId', select: 'status' },
                { path: 'storeId', select: 'domain' }
            ])
            if (found) {
                const { status, sellerDiscountId } = updateUpsaleDto
                if (!sellerDiscountId || (sellerDiscountId && found?.sellerDiscountId?.id && sellerDiscountId == found.sellerDiscountId.id.toString() && !found.sellerDiscountId.status)) {
                    updateUpsaleDto.status = false
                } else {
                    updateUpsaleDto.status = status
                }
                await this.storeUpsaleModel.findByIdAndUpdate(found._id, updateUpsaleDto)
                await this.cacheService.remove({ key: `${KeyRedisEnum.upsale}-${found.storeId?.domain}` })
            } else {
                const upsaleIns = new this.storeUpsaleModel({
                    storeId,
                    ...updateUpsaleDto
                })
                await upsaleIns.save()
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async sellerDeleteUpsale(storeId: string, upsaleId: string) {
        try {
            const found = await this.storeUpsaleModel.findOne({ storeId, _id: upsaleId })
            if (!found) {
                throw new BaseApiException({ message: BaseNoti.UPSALE.UPSALE_NOT_EXISTS })
            }
            await this.deleteManyUpsale({ _id: upsaleId })
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async updateUpsaleWhenStoreClose(storeId: string) {
        try {
            const found = await this.storeUpsaleModel.findOne({ storeId })
            if (!found) return
            await this.storeUpsaleModel.findByIdAndUpdate(found._id, { $pull: { listStoreId: new Types.ObjectId(storeId) } })
        } catch (error) {
            throw error
        }
    }

    async deleteManyUpsale(filter: any) {
        try {
            await this.storeUpsaleModel.deleteMany(filter)
        } catch (error) {
            throw error
        }
    }

    async updateMany(filter: any, updateDto: any) {
        try {
            await this.storeUpsaleModel.updateMany(filter, updateDto)
        } catch (error) {
            throw error
        }
    }
}
