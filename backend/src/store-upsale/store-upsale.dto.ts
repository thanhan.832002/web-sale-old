import { ApiProperty, PickType } from "@nestjs/swagger";
import { StoreUpsale } from "./store-upsale.model";
import { IsNotEmpty, IsOptional, Validate } from "class-validator";
import { Transform } from "class-transformer";
import { IsListObjectId, IsObjectId } from "src/base/custom-validator";

export class UpdateUpsaleDto {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsOptional()
    sellerDiscountId: string

    @ApiProperty({ type: [String] })
    @Validate(IsListObjectId)
    @IsOptional()
    listStoreId: string[]

    @ApiProperty()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsOptional()
    status: boolean
}