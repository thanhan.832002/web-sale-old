import { Module } from '@nestjs/common';
import { StoreUpsaleService } from './store-upsale.service';
import { StoreUpsaleController } from './store-upsale.controller';
import { StoreUpsale, StoreUpsaleSchema } from './store-upsale.model';
import { MongooseModule } from '@nestjs/mongoose';
import { SharedModule } from 'src/base/shared.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: StoreUpsale.name, schema: StoreUpsaleSchema }]),
    SharedModule
  ],
  providers: [StoreUpsaleService],
  exports: [StoreUpsaleService],
  controllers: [StoreUpsaleController]
})
export class StoreUpsaleModule { }
