import { ApiProperty } from "@nestjs/swagger"
import { Transform, Type } from "class-transformer"
import { IsNotEmpty, IsNumber, Validate } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsListObjectId, IsObjectId } from "src/base/custom-validator"

export const StoreUpsaleSchema: Schema = new Schema({
    storeId: {
        type: Types.ObjectId,
        ref: 'SellerDiscount'
    },
    sellerDiscountId: {
        type: Types.ObjectId,
        ref: 'SellerDiscount'
    },
    listStoreId: {
        type: [Types.ObjectId],
        ref: 'Store'
    },
    status: {
        type: Boolean,
        default: false
    }
}, { ...schemaOptions, collection: 'StoreUpsale' })


export class StoreUpsale extends Document {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    storeId: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    sellerDiscountId: string

    @ApiProperty({ type: [String] })
    @Validate(IsListObjectId)
    @IsNotEmpty()
    listStoreId: string[]

    @ApiProperty()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsNotEmpty()
    status: boolean
}
