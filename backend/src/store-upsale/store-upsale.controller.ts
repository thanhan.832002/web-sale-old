import { Controller, Get, Param, UseGuards, Put, Body, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { SellerGuard } from 'src/base/guard/seller.guard';
import { StoreUpsaleService } from './store-upsale.service';
import { UpdateUpsaleDto } from './store-upsale.dto';

@Controller('store-upsale')
@ApiTags('Store Upsale')
@UseGuards(SellerGuard)
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
export class StoreUpsaleController {
    constructor(
        private readonly storeUpsaleService: StoreUpsaleService
    ) { }

    @Get('/:storeId')
    getListStoreUpsale(
        @Param('storeId') storeId: string) {
        return this.storeUpsaleService.getStoreUpsale(storeId)
    }

    @Put('update/:storeId')
    updateUpsale(
        @Param('storeId') storeId: string,
        @Body() updateUpsaleDto: UpdateUpsaleDto) {
        return this.storeUpsaleService.updateUpsale(storeId, updateUpsaleDto)
    }

    @Delete('delete/:storeId/:upsaleId')
    sellerDeleteUpsale(
        @Param('storeId') storeId: string,
        @Param('upsaleId') upsaleId: string) {
        return this.storeUpsaleService.sellerDeleteUpsale(storeId, upsaleId)
    }
}
