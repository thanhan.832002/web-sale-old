import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger"
import { Type } from "class-transformer"
import { IsArray, IsNotEmpty, IsNumber, IsOptional, Min, Validate, ValidateNested } from "class-validator"
import { IsObjectId } from "src/base/custom-validator"

export class UpdateSellerVariantDto {
    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    price: number

    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    comparePrice: number

    @ApiProperty()
    @Validate(IsObjectId)
    @IsOptional()
    imageId: string
}

export class OneVariantDto {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    variantId: string

    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    price: number

    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    comparePrice: number

    @ApiProperty()
    @Validate(IsObjectId)
    @IsOptional()
    imageId: string
}

export class UpdateManyVariantSellerDto {
    @ApiProperty({ type: [OneVariantDto] })
    @ValidateNested()
    @Type(() => OneVariantDto)
    @IsArray()
    @IsOptional()
    listVariant: OneVariantDto[]
}