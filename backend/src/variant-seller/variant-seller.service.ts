import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Variant } from 'src/variant/variant.model';
import { UpdateManyVariantSellerDto } from './variant-seller.dto';
import { VariantSeller } from './variant-seller.model';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';
import { VariantOption } from 'src/variant-option/variant-option.model';

@Injectable()
export class VariantSellerService {
    constructor(
        @InjectModel(VariantSeller.name) private readonly variantSellerModel: Model<VariantSeller>,
    ) { }

    async createListVariantSeller(productSellerId: string, listVariant: Variant[], listOption: VariantOption[]) {
        try {
            const listVariantSeller = listVariant.map(variant => {
                const variantSeller: Partial<VariantSeller> = {
                    ...variant.toObject(),
                    productSellerId,
                    listOption,
                    variantId: variant._id
                }
                delete variantSeller._id
                const variantSellerIns: VariantSeller = new this.variantSellerModel(variantSeller)
                return variantSellerIns
            })
            await this.variantSellerModel.insertMany(listVariantSeller)
            const listVariantSellerId = listVariantSeller.map(variantSeller => variantSeller.id as string)
            return listVariantSellerId
        } catch (error) {
            throw error
        }
    }

    async updateManyVariantSeller({ listVariant }: UpdateManyVariantSellerDto) {
        try {
            for (const variant of listVariant) {
                await this.variantSellerModel.findOneAndUpdate({ _id: variant.variantId }, { ...variant })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async updateVariantSellerById(variantSellerId: string, updatedVariantSeller: Partial<VariantSeller>) {
        try {
            await this.variantSellerModel.findByIdAndUpdate(variantSellerId, updatedVariantSeller)
        } catch (error) {
            throw error
        }
    }

    async updateVariantSeller(filter: any, updatedVariantSeller: Partial<VariantSeller>) {
        try {
            await this.variantSellerModel.findOneAndUpdate(filter, updatedVariantSeller)
        } catch (error) {
            throw error
        }
    }

    async updateMany(filter: any, updatedVariantSeller: Partial<VariantSeller>) {
        try {
            await this.variantSellerModel.updateMany(filter, updatedVariantSeller)
        } catch (error) {
            throw error
        }
    }

    async findVariantCheckout(variantSellerId: string) {
        try {
            const foundVariant = await this.variantSellerModel.findById(variantSellerId).populate('imageId')
            if (!foundVariant) {
                throw new BaseApiException({
                    message: BaseNoti.VARIANT.VARIANT_NOT_EXISTS
                })
            }
            return foundVariant
        } catch (error) {
            throw error
        }
    }

    async findVariantById(variantId?: string) {
        try {
            if (!variantId) return
            const found = await this.variantSellerModel.findById(variantId).populate({ path: 'imageId', select: 'imageUrl' })
            return found
        } catch (error) {
            throw error
        }
    }

    async deleteMany(filter: any) {
        try {
            await this.variantSellerModel.deleteMany(filter)
        } catch (error) {
            throw error
        }
    }
}
