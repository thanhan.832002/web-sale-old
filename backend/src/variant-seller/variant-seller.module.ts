import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { VariantSellerController } from './variant-seller.controller';
import { VariantSeller, VariantSellerSchema } from './variant-seller.model';
import { VariantSellerService } from './variant-seller.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: VariantSeller.name, schema: VariantSellerSchema }])
  ],
  controllers: [VariantSellerController],
  providers: [VariantSellerService],
  exports: [VariantSellerService]
})
export class VariantSellerModule { }
