import { ApiProperty } from "@nestjs/swagger"
import { Type } from "class-transformer"
import { IsNotEmpty, IsNumber, Validate, IsOptional } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsObjectId } from "src/base/custom-validator"
import { VariantOption, VariantOptionSchema } from "src/variant-option/variant-option.model"

export const VariantSellerSchema: Schema = new Schema({
    productSellerId: {
        type: Types.ObjectId,
        index: true,
        ref: 'ProductSeller'
    },
    variantId: {
        type: Types.ObjectId,
        index: true,
        ref: 'Variant'
    },
    price: Number,
    comparePrice: Number,
    productCost: Number,
    fullfillmentCost: Number,
    sku: String,
    imageId: {
        type: Types.ObjectId,
        ref: 'Gallery'
    },
    listValue: [String],
    listOption: [VariantOptionSchema],
    isSellable: {
        type: Boolean,
        default: true
    }
}, { ...schemaOptions, collection: 'VariantSeller' })


export class VariantSeller extends Document {
    productSellerId: string
    variantId: string
    price: number
    comparePrice: number
    productCost: number
    fullfillmentCost: number
    imageId: string
    sku: string
    listValue: string[]
    listOption: VariantOption[]
    isSellable: boolean
}
