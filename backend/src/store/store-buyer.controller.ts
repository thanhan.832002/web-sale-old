import { Controller, Get, Param, Query, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { StoreService } from './store.service';
import { GetDomain } from 'src/base/decorator/get-domain.decorator';
import { UtmQuery } from 'src/product-buyer/product-buyer.query';

@Controller('store-buyer')
@ApiTags('Store Buyer')
export class StoreBuyerController {
    constructor(
        private readonly storeService: StoreService
    ) { }

    @Get('setting/:domain')
    buyerGetStoreSetting(@Param('domain') domain: string) {
        try {
            return this.storeService.buyerGetStoreSetting(domain)
        } catch (error) {
            throw error
        }
    }

    @Get('upsale/:domain')
    buyerGetStoreUpsale(@Param('domain') domain: string) {
        return this.storeService.buyerGetStoreUpsale(domain)
    }
}
