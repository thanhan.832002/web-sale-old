import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { BaseService } from 'src/base/base.service';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { DataFilter } from 'src/base/interfaces/data.interface';
import BaseNoti from 'src/base/notify';
import { Gallery } from 'src/gallery/gallery.model';
import { GalleryService } from 'src/gallery/gallery.service';
import { DeleteMediaImageDto, SortMediaImageDto, UpdateMediaImageDto } from 'src/product-seller/product-seller.dto';
import { ProductSeller } from 'src/product-seller/product-seller.model';
import { ProductSellerService } from 'src/product-seller/product-seller.service';
import { StoreSetting } from 'src/store-setting/store-setting.model';
import { StoreSettingService } from 'src/store-setting/store-setting.service';
import { OneVariantDto, UpdateManyVariantSellerDto, UpdateSellerVariantDto } from 'src/variant-seller/variant-seller.dto';
import { VariantSeller } from 'src/variant-seller/variant-seller.model';
import { VariantSellerService } from 'src/variant-seller/variant-seller.service';
import { Variant } from 'src/variant/variant.model';
import { ChangeStatusStoreDto, CheckDomainQuery, CreateStoreDto, ProductDisplayDto, StoreQuery, UpdateOverviewDto } from './store.dto';
import { StoreStatus } from './store.enum';
import { Store } from './store.model';
import { UserPaymentService } from 'src/seller-payment/seller-payment.service';
import { StorePaymentService } from 'src/store-payment/store-payment.service';
import { StoreUpsaleService } from 'src/store-upsale/store-upsale.service';
import { DomainService } from 'src/domain/domain.service';
import { CachePageService } from 'src/cache-page/cache-page.service';
import * as Promise from 'bluebird'
import { CacheService } from 'src/base/caching/cache.service';
import { KeyRedisEnum } from 'src/view/view.enum';
import { ReviewSellerService } from 'src/review/review-seller.service';
import { ModuleRef } from '@nestjs/core';
@Injectable()
export class StoreService extends BaseService<Store> implements OnModuleInit {
    private reviewService: ReviewSellerService
    constructor(
        @InjectModel(Store.name) private readonly storeModel: Model<Store>,
        private readonly productSellerService: ProductSellerService,
        private readonly storeSettingService: StoreSettingService,
        private readonly variantSellerService: VariantSellerService,
        private readonly galleryService: GalleryService,
        private readonly userPaymentService: UserPaymentService,
        private readonly storePaymentService: StorePaymentService,
        private readonly storeUpsaleService: StoreUpsaleService,
        private readonly domainService: DomainService,
        private readonly cachePageService: CachePageService,
        private readonly cacheService: CacheService,
        private moduleRef: ModuleRef
    ) {
        super(storeModel)
    }

    onModuleInit() {
        this.reviewService = this.moduleRef.get(ReviewSellerService, { strict: false });
    }

    async createStore(sellerId: string, createStoreDto: CreateStoreDto) {
        try {
            let { productId, storeName, supportEmail, productDisplay, domainId, slug, templateType } = createStoreDto
            slug = this.createSlug(slug)
            if (!slug) {
                throw new BaseApiException({
                    message: BaseNoti.PRODUCT.SLUG_NOT_VALID
                })
            }
            const foundDomain = await this.domainService.findDomainById(domainId)
            const domain = slug && foundDomain ? `${slug}.${foundDomain.domain}` : createStoreDto.domain
            createStoreDto.domain = domain.toLowerCase()
            await this.checkDomainStoreExists(domain)
            if (!productDisplay || !Object.keys(productDisplay).length) {
                productDisplay = {
                    peopleViewing: 3500,
                    purchased: Math.floor(1800 + (3000 - 1800) * Math.random()),
                    lastMinute: Math.floor(700 + (1000 - 700) * Math.random())
                }
            } else {
                if (!productDisplay?.peopleViewing) {
                    productDisplay.peopleViewing = 3500
                }
                if (!productDisplay?.purchased) {
                    productDisplay.purchased = Math.floor(1800 + (3000 - 1800) * Math.random())
                }
                if (!productDisplay?.lastMinute) {
                    productDisplay.lastMinute = Math.floor(700 + (1000 - 700) * Math.random())
                }
            }
            const productSeller = await this.productSellerService.createProductSeller(productId)
            const storeIns = new this.storeModel({
                sellerId,
                productSellerId: productSeller.id,
                storeName,
                domainId: foundDomain._id,
                status: StoreStatus.pending,
                domain,
                slug,
                supportEmail,
                isOpenAbandoned: true,
                productDisplay: productDisplay,
                adminId: foundDomain.adminId,
                templateType: templateType ? templateType : 1
            })
            await storeIns.save()
            await Promise.all([
                this.storeSettingService.createDefaultStoreSetting(storeIns.id),
                this.userPaymentService.createDefaultUserPayment(sellerId, storeIns._id),
                this.reviewService.createDefaultReview(storeIns._id, productId)

            ])
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            console.log('error :>> ', error);
            throw error
        }
    }

    async createStoreGallery(storeId: string, user: JwtTokenDecrypted, images: Array<Express.Multer.File>) {
        try {
            const { userId } = user
            const foundStore = await this.findStore({ sellerId: userId, _id: storeId })
            const listImage = await this.galleryService.createStoreGallery(storeId, user, images)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listImage
            }
        } catch (error) {
            throw error
        }
    }

    async listStoreGallery(storeId: string, userId: string) {
        try {
            const foundStore = await this.findStore({ sellerId: userId, _id: storeId })
            const foundStoreSetting = await this.storeSettingService.findStoreSetting(storeId)
            const foundProductSeller = await this.productSellerService.findProductSeller(foundStore.productSellerId)
            const listImageInGallery = await this.galleryService.getStoreGallery(storeId)
            let listImageId = []
            if (foundStoreSetting?.logo) listImageId.push(foundStoreSetting.logo)
            if (foundProductSeller?.listImageId) listImageId = [...listImageId, ...foundProductSeller.listImageId]
            if (foundProductSeller?.listVariantSellerId.length) {
                for (let variant of foundProductSeller.listVariantSellerId as Variant[]) {
                    const { imageId } = variant
                    if (imageId) listImageId.push(imageId)
                }
            }
            if (listImageInGallery?.length) listImageId = [...listImageId, ...listImageInGallery]
            let listUniqueImage = []
            for (let image of listImageId) {
                const foundInListUnique = listUniqueImage.find(imageInArray => imageInArray._id.toString() == image._id.toString())
                if (!foundInListUnique) listUniqueImage.push(image)
            }
            const sortListImage = listUniqueImage.sort(this.compare)
            return sortListImage
        } catch (error) {
            throw error
        }
    }

    compare(a, b) {
        if (a.createdAt < b.createdAt) {
            return -1;
        }
        if (a.createdAt > b.createdAt) {
            return 1;
        }
        return 0;
    }

    async listStore(sellerId: string, query: StoreQuery) {
        try {
            let { page, limit, search, filter, status } = query
            let filterObject: DataFilter<Partial<Store>> = {
                limit,
                page,
                condition: { sellerId },
                selectCols: 'storeName domain status productSellerId',
                population: [
                    //@ts-ignore
                    { path: 'productSellerId', select: 'listImageId', populate: { path: 'listImageId', select: 'imageUrl' } }
                ],
                sort: {
                    productUpdatedAt: -1,
                    _id: -1
                }
            }
            if (filter) {
                filterObject.condition = { ...filterObject.condition, ...filter }
            }
            if (status) {
                filterObject.condition = { ...filterObject.condition, status }
            }
            if (search) {
                filterObject.condition['$or'] = [
                    { domain: { '$regex': search, '$options': 'i' } },
                    { slug: { '$regex': search, '$options': 'i' } },
                ]
            }
            const store = await this.getListDocument(filterObject)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: store
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetListStore(adminId: string, query: StoreQuery) {
        try {
            let { page, limit, search, filter, sort, sellerId } = query
            let filterObject: DataFilter<Partial<Store>> = {
                limit,
                page,
                condition: { adminId },
                population: []
            }
            if (filter) {
                filterObject.condition = { ...filterObject.condition, ...filter }
            }
            if (sellerId) {
                filterObject.condition = { ...filterObject.condition, sellerId }
            }
            if (search) {
                filterObject.condition['$or'] = [
                    { email: { '$regex': search, '$options': 'i' } },
                    { supportEmail: { '$regex': search, '$options': 'i' } },
                ]
            }
            if (sort) {
                filterObject.sort = sort
            }
            const store = await this.getListDocument(filterObject)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: store
            }
        } catch (error) {
            throw error
        }
    }


    async sellerChangeStatus(userId: string, { storeId, status }: ChangeStatusStoreDto) {
        try {
            const foundStore = await this.findStore({ storeId, sellerId: userId })
            if (foundStore.status == StoreStatus.active && status == StoreStatus.pending) {
                return {
                    message: BaseNoti.GLOBAL.SUCCESS,
                    data: foundStore
                }
            }
            const storeUpdated = await this.storeModel.findByIdAndUpdate(storeId, { status }, { new: true })
            if (status == StoreStatus.pending) {
                await this.storeUpsaleService.updateUpsaleWhenStoreClose(storeId)
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: storeUpdated
            }
        } catch (error) {
            throw error
        }
    }

    async updateOverview(sellerId: string, storeId: string, updateOverviewDto: UpdateOverviewDto) {
        try {
            const foundStore = await this.findStore({ sellerId, _id: storeId })
            let { storeName, supportEmail, logo, isOpenAbandoned, saveButton, colorVariant, phoneNumber, companyInfo, productName, description, isOnHomePage, buyMore, productDisplay, listVariant, announcement, templateType } = updateOverviewDto
            let updateStoreField: Partial<Store> = {}
            let domainId = updateOverviewDto.domainId || foundStore.domainId
            const foundMain = await this.domainService.findDomainById(domainId)
            let slug = this.createSlug(updateOverviewDto.slug) || foundStore.slug
            updateStoreField.slug = slug
            const domain = `${slug}.${foundMain.domain}`.toLowerCase()
            if (storeName) updateStoreField.storeName = storeName
            if (announcement != null || announcement != undefined) updateStoreField.announcement = announcement
            if (buyMore) updateStoreField.buyMore = buyMore
            if (saveButton != undefined || saveButton != null) updateStoreField.saveButton = saveButton
            if (colorVariant) updateStoreField.colorVariant = colorVariant
            if (domain != foundStore.domain) {
                await this.checkDomainStoreExists(domain, storeId)
                updateStoreField.domain = domain
            }
            if (supportEmail) updateStoreField.supportEmail = supportEmail
            if (templateType) updateStoreField.templateType = templateType
            if (isOnHomePage != undefined) updateStoreField.isOnHomePage! = isOnHomePage!
            if (isOpenAbandoned != undefined) updateStoreField.isOpenAbandoned! = isOpenAbandoned!
            if (Object.keys(productDisplay).length) {
                updateStoreField.productDisplay = productDisplay
            }
            if (Object.keys(updateStoreField).length) {
                await this.storeModel.findByIdAndUpdate(storeId, updateStoreField)
            }
            let updateStoreSettingField: Partial<StoreSetting> = {}
            if (phoneNumber != undefined) updateStoreSettingField.phoneNumber = phoneNumber
            if (companyInfo != undefined) updateStoreSettingField.companyInfo = companyInfo
            if (logo != undefined) updateStoreSettingField.logo = logo
            if (Object.keys(updateStoreSettingField).length) {
                await this.storeSettingService.updateStoreSetting(storeId, updateStoreSettingField)
            }
            if (listVariant && listVariant.length) {
                await this.updateListVariantSeller(foundStore, listVariant)
            }
            let updateProductSellerField: Partial<ProductSeller> = {}
            if (productName) updateProductSellerField.productName = productName
            if (description) updateProductSellerField.description = description
            if (Object.keys(updateProductSellerField).length) {
                await this.productSellerService.updateProductSeller(foundStore.productSellerId, updateProductSellerField)
            }
            await this.removeCacheStore(domain)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async removeCachePageBySlugAndDomainId(slug: string, domainId: string) {
        try {
            const foundDomain = await this.domainService.findDomainById(domainId)
            await this.cachePageService.removePageCache(slug, foundDomain.domain)
        } catch (error) {
            throw error
        }
    }

    async updateManyVariantSeller(storeId: string, userId: string, updateManySellerVariantDto: UpdateManyVariantSellerDto) {
        try {
            const { listVariant } = updateManySellerVariantDto
            const listVariantId = listVariant.map(variant => variant.variantId)
            const foundStore = await this.findStore({ sellerId: userId, _id: storeId })
            const listValid = await this.productSellerService.checkListVariantSellerIsValid(foundStore.productSellerId, listVariantId)
            const lidValidVariant = listVariant.filter(variant => listValid.includes(variant.variantId))
            await this.variantSellerService.updateManyVariantSeller({ listVariant: lidValidVariant })
            // this.removeCachePageBySlugAndDomainId(foundStore.slug, foundStore.domainId)
            await this.removeCacheStore(foundStore.domain)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async updateListVariantSeller(store: Store, listVariant: OneVariantDto[]) {
        try {
            const listVariantId = listVariant.map(variant => variant.variantId)
            const listValid = await this.productSellerService.checkListVariantSellerIsValid(store.productSellerId, listVariantId)
            const lidValidVariant = listVariant.filter(variant => listValid.includes(variant.variantId))
            await this.variantSellerService.updateManyVariantSeller({ listVariant: lidValidVariant })
            // this.removeCachePageBySlugAndDomainId(store.slug, store.domainId)
            await this.removeCacheStore(store.domain)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async updateProductDisplay(storeId: string, userId: string, productDisplayDto: ProductDisplayDto) {
        try {
            const foundStore = await this.findStore({ sellerId: userId, _id: storeId })
            await this.storeModel.findByIdAndUpdate(storeId, { productDisplay: productDisplayDto })
            await this.removeCacheStore(foundStore.domain)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async addMediaImage(storeId: string, userId: string, updateMediaImageDto: UpdateMediaImageDto) {
        try {
            const { listNewImageId } = updateMediaImageDto
            const foundStore = await this.findStore({ sellerId: userId, _id: storeId })
            const foundProductSeller = await this.productSellerService.findProductSeller(foundStore.productSellerId)
            const listImageId = (foundProductSeller.listImageId as Gallery[]).map(gallery => gallery._id)
            const listImageValid = await this.listStoreGallery(storeId, userId)
            const listImageValidId = listImageValid.map(image => image._id.toString())
            const listNewValidImageId = listNewImageId.filter(imageId => listImageValidId.includes(imageId.toString()))
            const listImageIdNotInProductSeller = listNewValidImageId.filter(imageId => !listImageId.includes(imageId.toString()))
            const updatedProductSeller = await this.productSellerService.updateProductSeller(foundStore.productSellerId, {
                $push: { listImageId: { $each: listImageIdNotInProductSeller } }
            })
            await this.removeCacheStore(foundStore.domain)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: updatedProductSeller.listImageId
            }
        } catch (error) {
            console.log('error :>> ', error);
            throw error
        }
    }

    async sortMediaImage(storeId: string, userId: string, sortMediaImageDto: SortMediaImageDto) {
        try {
            let { listNewImageId } = sortMediaImageDto
            listNewImageId = [...new Set(listNewImageId)]
            const foundStore = await this.findStore({ sellerId: userId, _id: storeId })
            const foundProductSeller = await this.productSellerService.findProductSeller(foundStore.productSellerId)
            let listImageId = (foundProductSeller.listImageId as Gallery[]).map(gallery => gallery._id.toString())
            listImageId = [...new Set(listImageId)]
            const isSameElemArray = listNewImageId.every(imageId => listImageId.includes(imageId))
            if (isSameElemArray) {
                await this.productSellerService.updateProductSeller(foundStore.productSellerId, {
                    listImageId: listNewImageId
                })
            }
            await this.removeCacheStore(foundStore.domain)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async deleteMediaImage(storeId: string, userId: string, deleteMediaImageDto: DeleteMediaImageDto) {
        try {
            let { listDeletedImageId } = deleteMediaImageDto
            listDeletedImageId = [...new Set(listDeletedImageId)]
            const foundStore = await this.findStore({ sellerId: userId, _id: storeId })
            const foundProductSeller = await this.productSellerService.findProductSeller(foundStore.productSellerId)
            let listImage = (foundProductSeller.listImageId as Gallery[])
            let listDeleteImage = listImage.filter(image => listDeletedImageId.includes(image._id.toString()))
            let listDeleteImageId = listDeleteImage.map(image => image._id)
            const updatedProductSeller = await this.productSellerService.updateProductSeller(foundStore.productSellerId, {
                $pullAll: { listImageId: listDeleteImageId }
            })
            const listStoreImageId = listDeleteImage.filter(image => image.storeId == storeId).map(image => image.id)
            await this.galleryService.deleteListImage(listStoreImageId)
            await this.removeCacheStore(foundStore.domain)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async getStoreDetail(userId: string, storeId: string) {
        try {
            const foundStore = await this.findStore({ sellerId: userId, _id: storeId })
            if (!foundStore) {
                throw new BaseApiException({
                    message: BaseNoti.STORE.STORE_NOT_EXISTS
                })
            }
            const foundStoreSetting = await this.storeSettingService.findStoreSetting(storeId)
            const foundProductSeller = await this.productSellerService.findProductSeller(foundStore.productSellerId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: {
                    store: foundStore,
                    storeSetting: foundStoreSetting,
                    product: foundProductSeller
                }
            }
        } catch (error) {
            throw error
        }
    }

    async getListStorePayment(userId: string, storeId: string) {
        try {
            const foundStore = await this.findStore({ sellerId: userId, _id: storeId })
            if (!foundStore) {
                throw new BaseApiException({
                    message: BaseNoti.STORE.STORE_NOT_EXISTS
                })
            }
            let listPayment = await this.storePaymentService.findListStorePayment(storeId)
            if (!listPayment || !listPayment.length) {
                listPayment = await this.userPaymentService.createDefaultUserPayment(userId, storeId)
                await this.cacheService.remove({ key: [`${KeyRedisEnum.policy}-${foundStore.domain}`, `${KeyRedisEnum.listPayment}-${foundStore.domain}`] })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listPayment
            }
        } catch (error) {
            throw error
        }
    }


    async buyerGetStoreSetting(domain: string) {
        try {
            const cachedResponse = await this.cacheService.get({ key: `${KeyRedisEnum.storeSetting}-${domain}` })
            if (cachedResponse) return JSON.parse(cachedResponse)
            const foundStore = await this.findStoreBuyer(domain)
            const { supportEmail } = foundStore
            const [foundStoreSetting, productName] = await Promise.all([
                this.storeSettingService.findStoreSetting(foundStore._id),
                this.productSellerService.getProductSellername(foundStore.productSellerId)
            ])
            const response = {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: { ...foundStoreSetting.toObject(), ...foundStore.toObject(), supportEmail, productName }
            }
            await this.cacheService.set({ key: `${KeyRedisEnum.storeSetting}-${domain}`, value: JSON.stringify(response), expiresIn: 30 * 24 * 60 * 60 })
            return response
        } catch (error) {
            throw error
        }
    }

    async sellerCheckDomain({ domain, storeId }: CheckDomainQuery) {
        try {
            await this.checkDomainStoreExists(domain, storeId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async buyerGetStoreUpsale(domain: string) {
        try {
            const cachedResponse = await this.cacheService.get({ key: `${KeyRedisEnum.upsale}-${domain}` })
            if (cachedResponse) return JSON.parse(cachedResponse)
            const foundStore = await this.findStoreBuyer(domain)
            const upsale = await this.storeUpsaleService.buyerGetStoreUpsale(foundStore._id)
            const response = {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: upsale
            }
            await this.cacheService.set({ key: `${KeyRedisEnum.upsale}-${domain}`, value: JSON.stringify(response), expiresIn: 30 * 24 * 60 * 60 })
            return response
        } catch (error) {
            throw error
        }
    }

    async findStore(filter: any = {}) {
        try {
            const { domain } = filter
            if (domain && domain == 'localhost:2002') {
                filter.domain = "tummy-pants-ztech-test.toptrend1day.com"
            }
            // return await this.storeModel.findOne({ status: 'active' })
            const foundStore = await this.storeModel.findOne(filter)
            if (!foundStore) {
                throw new BaseApiException({
                    message: BaseNoti.STORE.STORE_NOT_EXISTS
                })
            }
            return foundStore
        } catch (error) {
            throw error
        }
    }

    async findStoreBuyer(domain: string) {
        try {
            if (domain && domain == 'localhost:2002') {
                domain = "tummy-pants-ztech-test.toptrend1day.com"
            }
            const foundStore = await this.storeModel.findOne({ domain, status: StoreStatus.active }).select('-sellerId -createdAt -updatedAt').populate({ path: 'domainId', select: 'enabled' })
            //@ts-ignore
            if (!foundStore || !foundStore.domainId?.enabled) {
                throw new BaseApiException({
                    message: BaseNoti.STORE.STORE_NOT_EXISTS
                })
            }
            return foundStore
        } catch (error) {
            throw error
        }
    }

    async findListStoreHomepage(filter: any) {
        try {
            const listStore = await this.storeModel.find(filter).populate([
                {
                    path: 'productSellerId', populate: [
                        {
                            path: 'listVariantSellerId',
                            populate: [{ path: 'imageId' }]
                        },
                        { path: 'listImageId' },
                        { path: 'productId', select: 'slug' }
                    ]
                }
            ]).lean()
            return listStore
        } catch (error) {
            throw error
        }
    }

    async updateProductAt(productId: string) {
        try {
            const listProductSellerId = await this.productSellerService.findListProductSellerIdByProductId(productId)
            await this.storeModel.updateMany({ productSellerId: { $in: listProductSellerId } }, { productUpdatedAt: new Date() })
        } catch (error) {
            throw error
        }
    }

    async findListStore(filter: any) {
        try {
            const foundStore = await this.storeModel.find(filter)
            return foundStore
        } catch (error) {
            throw error
        }
    }

    async findListStoreView(filter: any) {
        try {
            const foundStore = await this.storeModel.find(filter).populate({ path: 'productSellerId', select: 'productId' }).lean()
            return foundStore
        } catch (error) {
            throw error
        }
    }

    async updateStore(filter: any, updateField: any) {
        try {
            await this.storeModel.findOneAndUpdate(filter, updateField)
        } catch (error) {
            throw error
        }
    }

    async checkDomainStoreExists(domain: string, storeId?: string) {
        try {
            domain = domain.toLowerCase()
            const condition = storeId ? { domain, _id: { $ne: storeId } } : { domain }
            const isExists = await this.storeModel.exists(condition)
            if (isExists) {
                throw new BaseApiException({
                    message: BaseNoti.DOMAIN.DOMAIN_ALREADY_EXISTS
                })
            }
            return isExists
        } catch (error) {
            throw error
        }
    }

    async getDomainFromProductSellerId(productSellerId: string): Promise<string> {
        try {
            const foundStore = await this.storeModel.findOne({ productSellerId }).select('domain')

            return foundStore?.domain || ''
        } catch (error) {
            throw error
        }
    }

    async getDomainFromId(storeId: string): Promise<string> {
        try {
            const foundStore = await this.storeModel.findById(storeId).select('slug domainId domain')
            return foundStore?.domain || ''
        } catch (error) {
            throw error
        }
    }

    async removeCacheStore(domain: string) {
        try {
            const listKey = [
                `${KeyRedisEnum.storeSetting}-${domain}`,
                `${KeyRedisEnum.productBuyer}-${domain}`
            ]
            await this.cacheService.remove({ key: listKey })
        } catch (error) {

        }
    }

    async removeCacheStoreById(storeId: string) {
        try {
            const foundStore = await this.storeModel.findById(storeId)
            const listKey = [
                `${KeyRedisEnum.storeSetting}-${foundStore?.domain}`,
                `${KeyRedisEnum.productBuyer}-${foundStore?.domain}`
            ]
            foundStore && foundStore.domain && await this.cacheService.remove({ key: listKey })
        } catch (error) {

        }
    }

    async removeCacheProductId(productId: string) {
        try {
            const listProductSellerId = await this.productSellerService.findListProductSellerIdByProductId(productId)
            await Promise.map(listProductSellerId, async (productSellerId) => {
                const domain = await this.getDomainFromProductSellerId(productSellerId)
                if (domain) {
                    await this.removeCacheStore(domain)
                }
            }, { concurrency: 25 })
        } catch (error) {

        }
    }

    createSlug(name: string) {
        try {
            if (!name) return
            name = name.length < 40 ? name : name.substring(0, 40)
            let slug = name.trim().toLowerCase();
            slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
            slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
            slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
            slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
            slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
            slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
            slug = slug.replace(/đ/gi, 'd');
            slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
            slug = slug.split(' ').filter(char => char != '' && char != undefined && char != null).join('-')
            slug = slug.split('-').filter(char => char != '' && char != undefined && char != null).join('-')
            let lastChar = slug.substring(slug.length - 1)
            let firstChar = slug.substring(0, 1)
            if (firstChar == '-') slug = slug.substring(1)
            if (lastChar && lastChar == '-') slug = slug.slice(0, slug.length - 1)
            return slug
        } catch (error) {
            throw error
        }
    }
}

