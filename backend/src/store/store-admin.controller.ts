import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { StoreQuery } from './store.dto';
import { StoreService } from './store.service';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';

@Controller('store-admin')
@ApiTags('Store Admin')
@UseGuards(AdminGuard)
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
export class StoreAdminController {
    constructor(
        private readonly storeService: StoreService
    ) { }

    @Get('list')
    adminGetListStore(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: StoreQuery) {
        return this.storeService.adminGetListStore(userId, query)
    }

}
