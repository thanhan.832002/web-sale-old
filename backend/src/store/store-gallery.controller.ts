import { Body, Controller, Delete, Get, Param, Post, Put, Query, UploadedFiles, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FilesInterceptor } from '@nestjs/platform-express';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { extname } from 'path';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { SellerGuard } from 'src/base/guard/seller.guard';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import BaseNoti from 'src/base/notify';
import { GalleryService } from 'src/gallery/gallery.service';
import { CreateStoreDto, UpdateOverviewDto } from './store.dto';
import { StoreService } from './store.service';

const imageFileFilter = (req: any, file: any, callback: Function) => {
    const ext = extname(file.originalname);
    if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.webp') {
        return callback(null, false);
    }
    callback(null, true);
};

const multerOptionsImage: MulterOptions = {
    fileFilter: imageFileFilter,
    limits: {
        fileSize: 10485760 //10MB
    }
};

const generateFilename = (file: any) => {
    return `${Date.now()}-${file.originalname}`;
};

@Controller('store-gallery')
@ApiTags('Store Gallery')
@UseGuards(SellerGuard)
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
export class StoreGalleryController {
    constructor(
        private readonly storeService: StoreService,
        private readonly galleryService: GalleryService,
    ) { }

    @Post('create/:storeId')
    @UseInterceptors(FilesInterceptor('images', Number.POSITIVE_INFINITY, multerOptionsImage))
    createStoreGallery(
        @Param('storeId') storeId: string,
        @GetUser() user: JwtTokenDecrypted,
        @UploadedFiles() images: Array<Express.Multer.File>
    ) {
        return this.storeService.createStoreGallery(storeId, user, images)
    }

    @Get('list/:storeId')
    async listStoreGallery(
        @Param('storeId') storeId: string,
        @GetUser() { userId }: JwtTokenDecrypted
    ) {
        try {
            const listImage = await this.storeService.listStoreGallery(storeId, userId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listImage
            }
        } catch (error) {
            throw error
        }
    }

    @Delete('delete/:storeId/:galleryId')
    async deleteImage(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('storeId') storeId: string,
        @Param('galleryId') galleryId: string
    ) {
        return this.storeService.deleteMediaImage(storeId, userId, { listDeletedImageId: [galleryId] })
    }
}
