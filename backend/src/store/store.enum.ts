export enum StoreStatus {
    pending = 'pending',
    active = 'active'
}