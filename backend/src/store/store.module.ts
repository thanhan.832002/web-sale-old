import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GalleryModule } from 'src/gallery/gallery.module';
import { ProductSellerModule } from 'src/product-seller/product-seller.module';
import { StoreSettingModule } from 'src/store-setting/store-setting.module';
import { VariantSellerModule } from 'src/variant-seller/variant-seller.module';
import { StoreAdminController } from './store-admin.controller';
import { StoreGalleryController } from './store-gallery.controller';
import { StoreController } from './store.controller';
import { Store, StoreSchema } from './store.model';
import { StoreService } from './store.service';
import { StoreBuyerController } from './store-buyer.controller';
import { UserPaymentModule } from 'src/seller-payment/seller-payment.module';
import { StorePaymentModule } from 'src/store-payment/store-payment.module';
import { StoreUpsaleModule } from 'src/store-upsale/store-upsale.module';
import { DomainModule } from 'src/domain/domain.module';
import { CachePageModule } from 'src/cache-page/cache-page.module';
import { SharedModule } from 'src/base/shared.module';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: Store.name, schema: StoreSchema }]),
    ProductSellerModule,
    StoreSettingModule,
    GalleryModule,
    VariantSellerModule,
    UserPaymentModule,
    StorePaymentModule,
    StoreUpsaleModule,
    DomainModule,
    CachePageModule,
    SharedModule
  ],
  controllers: [StoreAdminController, StoreController, StoreGalleryController, StoreBuyerController],
  providers: [StoreService],
  exports: [StoreService]
})
export class StoreModule { }
