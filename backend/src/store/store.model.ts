import { ApiProperty } from "@nestjs/swagger"
import { Transform, Type } from "class-transformer"
import { IsBoolean, IsNotEmpty, IsNumber, IsOptional, Validate } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsObjectId } from "src/base/custom-validator"
import { StoreStatus } from "./store.enum"
import { BuyMoreDto, ProductDisplayDto } from "./store.dto"
import { DiscountEnum } from "src/coupon/coupon.model"

export const ProductPageDisplaySchema: Schema = new Schema({
    peopleViewing: Number,
    purchased: Number,
    lastMinute: Number
})

const DiscountBuyMore: Schema = new Schema({
    quantity: Number,
    content: {
        type: Number,
        enum: DiscountEnum
    },
    discount: Number
})

const BuyMoreSchema: Schema = new Schema({
    text: String,
    listBuyMore: [DiscountBuyMore]
})

export const StoreSchema: Schema = new Schema({
    adminId: {
        type: Types.ObjectId,
        index: true,
        ref: 'User'
    },
    sellerId: {
        type: Types.ObjectId,
        index: true,
        ref: 'User'
    },
    productSellerId: {
        type: Types.ObjectId,
        index: true,
        ref: 'ProductSeller'
    },
    domainId: {
        type: Types.ObjectId,
        ref: 'Domain',
        index: true
    },
    slug: { type: String, index: true },
    storeName: String,
    status: {
        type: String,
        enum: StoreStatus
    },
    domain: String,
    supportEmail: String,
    isOpenAbandoned: Boolean,
    isOnHomePage: { type: Boolean, default: false },
    productDisplay: ProductPageDisplaySchema,
    announcement: String,
    buyMore: BuyMoreSchema,
    saveButton: { type: Boolean, default: true },
    colorVariant: {
        type: String,
        default: '#fb5607'
    },
    templateType: {
        type: String,
        default: '1'
    },
    productUpdatedAt: {
        type: Date,
        index: true
    }
}, { ...schemaOptions, collection: 'Store' })
export class Store extends Document {
    @ApiProperty()
    @IsNotEmpty()
    @Validate(IsObjectId)
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    adminId: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    sellerId: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    productSellerId: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    domainId: string

    @ApiProperty()
    @IsNotEmpty()
    slug: string

    @ApiProperty()
    @IsNotEmpty()
    storeName: string

    @ApiProperty()
    @IsNotEmpty()
    status: StoreStatus

    @ApiProperty()
    @IsNotEmpty()
    domain: string

    @ApiProperty()
    @IsOptional()
    announcement: string

    @ApiProperty()
    @IsNotEmpty()
    supportEmail: string

    @ApiProperty()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsNotEmpty()
    isOpenAbandoned: boolean


    @ApiProperty()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsNotEmpty()
    isOnHomePage: boolean

    productDisplay: ProductDisplayDto

    buyMore: BuyMoreDto

    @ApiProperty()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsNotEmpty()
    saveButton: boolean

    @ApiProperty()
    @IsOptional()
    colorVariant: string

    @ApiProperty()
    @IsNotEmpty()
    templateType: string

    productUpdatedAt: Date
}
