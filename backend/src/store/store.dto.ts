import { ApiProperty, ApiPropertyOptional, PickType } from "@nestjs/swagger";
import { Transform, Type } from "class-transformer";
import { IsArray, IsBoolean, IsInt, IsNotEmpty, IsOptional, Matches, Min, Validate, ValidateNested } from "class-validator";
import { IsObjectId } from "src/base/custom-validator";
import { BaseQuery } from "src/base/interfaces/base-query.interface";
import { StoreStatus } from "./store.enum";
import { Store } from "./store.model";
import { OneVariantDto } from "src/variant-seller/variant-seller.dto";
import { DiscountEnum } from "src/coupon/coupon.model";
export class ProductDisplayDto {
    @ApiProperty()
    @Min(0)
    @IsInt()
    @IsOptional()
    peopleViewing: number

    @ApiProperty()
    @Min(0)
    @IsInt()
    @IsOptional()
    purchased: number

    @ApiProperty()
    @Min(0)
    @IsInt()
    @IsOptional()
    lastMinute: number
}
export class BuyMoreDiscount {
    @ApiProperty()
    @Min(0)
    @IsInt()
    @IsNotEmpty()
    quantity: number

    @ApiProperty()
    @IsNotEmpty()
    content: DiscountEnum

    @ApiProperty()
    @Min(0)
    @IsInt()
    @IsNotEmpty()
    discount: number
}
export class BuyMoreDto {
    @ApiProperty()
    @IsNotEmpty()
    text: string

    @ApiProperty({ type: BuyMoreDiscount })
    @ValidateNested({ each: true })
    @Type(() => BuyMoreDiscount)
    @IsOptional()
    listBuyMore: BuyMoreDiscount[]
}

export class CreateStoreDto extends PickType(Store, ['storeName', 'domain', 'announcement']) {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    productId: string

    @ApiProperty()
    @Matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
    @Transform(({ value }) => {
        if (value) return value.toLowerCase().trim()
    })
    @IsNotEmpty()
    supportEmail: string

    @ApiProperty({ type: ProductDisplayDto })
    @IsOptional()
    productDisplay: ProductDisplayDto

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    domainId: string

    @ApiProperty()
    @IsNotEmpty()
    slug: string

    @ApiProperty()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsOptional()
    isOnHomePage: boolean

    @ApiProperty()
    @IsOptional()
    templateType: string
}

export class UpdateOverviewDto {
    @ApiProperty()
    @IsOptional()
    storeName: string

    @ApiProperty()
    @IsOptional()
    domain: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsOptional()
    domainId: string

    @ApiProperty()
    @IsOptional()
    slug: string

    @ApiProperty()
    @IsOptional()
    supportEmail: string

    @ApiProperty()
    @IsOptional()
    productName: string

    @ApiProperty()
    @IsOptional()
    phoneNumber: string

    @ApiProperty()
    @IsOptional()
    companyInfo: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsOptional()
    logo: string

    @ApiProperty()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsOptional()
    isOpenAbandoned: boolean

    @ApiProperty()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsOptional()
    isOnHomePage: boolean

    @ApiProperty()
    @IsOptional()
    description: string

    @ApiProperty()
    @IsOptional()
    announcement: string

    @ApiProperty({ type: [OneVariantDto] })
    @ValidateNested()
    @Type(() => OneVariantDto)
    @IsArray()
    @IsOptional()
    listVariant: OneVariantDto[]

    @ApiProperty({ type: ProductDisplayDto })
    @IsOptional()
    productDisplay: ProductDisplayDto

    @ApiProperty({ type: BuyMoreDto })
    @IsOptional()
    buyMore: BuyMoreDto

    @ApiProperty()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsOptional()
    saveButton: boolean

    @ApiProperty()
    @IsOptional()
    colorVariant: string

    @ApiProperty()
    @IsOptional()
    templateType: string
}

export class StoreQuery extends BaseQuery {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsOptional()
    sellerId: string

    @ApiProperty()
    @IsOptional()
    status: StoreStatus
}

export class ChangeStatusStoreDto {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    storeId: string

    @ApiProperty()
    @IsNotEmpty()
    status: StoreStatus
}

export class CheckDomainQuery {
    @ApiProperty()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsNotEmpty()
    domain: string

    @ApiPropertyOptional()
    @Validate(IsObjectId)
    @IsOptional()
    storeId?: string
}