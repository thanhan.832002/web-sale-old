import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { SellerGuard } from 'src/base/guard/seller.guard';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { DeleteMediaImageDto, SortMediaImageDto, UpdateMediaImageDto } from 'src/product-seller/product-seller.dto';
import { UpdateManyVariantSellerDto, UpdateSellerVariantDto } from 'src/variant-seller/variant-seller.dto';
import { ChangeStatusStoreDto, CheckDomainQuery, CreateStoreDto, ProductDisplayDto, StoreQuery, UpdateOverviewDto } from './store.dto';
import { StoreService } from './store.service';

@Controller('store')
@ApiTags('Store')
@UseGuards(SellerGuard)
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
export class StoreController {
    constructor(
        private readonly storeService: StoreService
    ) { }

    @Post('create')
    createStore(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() createStoreDto: CreateStoreDto
    ) {
        return this.storeService.createStore(userId, createStoreDto)
    }

    @Get('detail/:storeId')
    getStoreDetail(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('storeId') storeId: string,
    ) {
        return this.storeService.getStoreDetail(userId, storeId)
    }

    @Put('update-overview/:storeId')
    updateOverview(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('storeId') storeId: string,
        @Body() updateOverviewDto: UpdateOverviewDto
    ) {
        return this.storeService.updateOverview(userId, storeId, updateOverviewDto)
    }

    @Get('list')
    listStore(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: StoreQuery
    ) {
        return this.storeService.listStore(userId, query)
    }

    @Post('media/:storeId/add-image')
    addMediaImage(
        @Param('storeId') storeId: string,
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() updateMediaImageDto: UpdateMediaImageDto
    ) {
        return this.storeService.addMediaImage(storeId, userId, updateMediaImageDto)
    }

    @Put('media/:storeId/sort-image')
    sortMediaImage(
        @Param('storeId') storeId: string,
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() sortMediaImageDto: SortMediaImageDto
    ) {
        return this.storeService.sortMediaImage(storeId, userId, sortMediaImageDto)
    }

    @Put('media/:storeId/delete-image')
    deleteMediaImage(
        @Param('storeId') storeId: string,
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() deleteMediaImageDto: DeleteMediaImageDto
    ) {
        return this.storeService.deleteMediaImage(storeId, userId, deleteMediaImageDto)
    }

    @Put('change-status')
    changeStatusStore(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() changeStatusStoreDto: ChangeStatusStoreDto) {
        return this.storeService.sellerChangeStatus(userId, changeStatusStoreDto)
    }

    @Get('list-payment/:storeId')
    getListStorePayment(
        @Param('storeId') storeId: string,
        @GetUser() { userId }: JwtTokenDecrypted,) {
        return this.storeService.getListStorePayment(userId, storeId)
    }

    @Get('check-domain')
    checkDomain(
        @Query() checkDomainQuery: CheckDomainQuery
    ) {
        return this.storeService.sellerCheckDomain(checkDomainQuery)
    }
}
