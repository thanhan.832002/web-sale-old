import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Review, ReviewSchema } from './review.model';
import { ReviewService } from './review.service';
import { ReviewCustomerController } from './review-customer.controller';
import { ReviewSellerController } from './review-seller.controller';
import { ReviewSellerService } from './review-seller.service';
import { StoreModule } from 'src/store/store.module';
import { SharedModule } from 'src/base/shared.module';
import { ProductReviewModule } from 'src/product-review/product-review.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Review.name, schema: ReviewSchema }]),
    StoreModule,
    SharedModule,
    ProductReviewModule
  ],
  providers: [ReviewService, ReviewSellerService],
  exports: [ReviewService],
  controllers: [ReviewCustomerController, ReviewSellerController]
})
export class ReviewModule { }
