import { ApiProperty, ApiPropertyOptional, PickType } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsBoolean, IsNotEmpty, IsOptional, Matches } from "class-validator";
import { BaseQuery } from "src/base/interfaces/base-query.interface";
import { Review } from "./review.model";

export class CreateReviewDto extends PickType(Review, ['email', 'phoneNumber', 'name', 'images', 'rating', 'content', 'title'/* , 'replyTo' */]) { }

export class ApprovedReviewDto {
    @ApiProperty()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsNotEmpty()
    approved: boolean
}

export class AdminListReviewQuery extends BaseQuery {
    @ApiPropertyOptional()
    @IsOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    productId: string

    @ApiPropertyOptional()
    @IsOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    approved: string
}

export class UpdateReviewDto {

    @ApiPropertyOptional()
    @IsOptional()
    @Matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
    email: string

    @ApiPropertyOptional()
    @IsOptional()
    name: string

    @ApiPropertyOptional()
    @IsOptional()
    phoneNumber: string

    @ApiProperty({
        type: [String]
    })
    @IsOptional()
    images: string[]

    @ApiPropertyOptional()
    @IsOptional()
    rating: number

    @ApiPropertyOptional()
    @IsOptional()
    content: string

    @ApiPropertyOptional()
    @IsOptional()
    title: string

}