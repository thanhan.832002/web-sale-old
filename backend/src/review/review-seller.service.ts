import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AdminListReviewQuery, ApprovedReviewDto, CreateReviewDto, UpdateReviewDto } from './review.dto';
import { Review } from './review.model';
import * as path from 'path';
import * as fs from 'fs';
import * as Promise from 'bluebird'
import { BaseService } from 'src/base/base.service';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { DataFilter } from 'src/base/interfaces/data.interface';
import BaseNoti from 'src/base/notify';
import { StoreService } from 'src/store/store.service';
import { CacheService } from 'src/base/caching/cache.service';
import { KeyRedisEnum } from 'src/view/view.enum';
import { ProductReviewService } from 'src/product-review/product-review.service';
import { ModuleRef } from '@nestjs/core';
const sharp = require('sharp');

@Injectable()
export class ReviewSellerService extends BaseService<Review> implements OnModuleInit {
    private storeService: StoreService
    constructor(
        @InjectModel(Review.name) private reviewModel: Model<Review>,
        private readonly productReviewService: ProductReviewService,
        private readonly cacheService: CacheService,
        private moduleRef: ModuleRef
    ) { super(reviewModel) }

    onModuleInit() {
        this.storeService = this.moduleRef.get(StoreService, { strict: false });
    }

    async sellerApprovedReview(storeId: string, reviewId: string, approvedReviewDto: ApprovedReviewDto) {
        try {
            const foundStore = await this.storeService.findStore({ _id: storeId })
            await this.findReview(reviewId, foundStore.productSellerId)
            await this.reviewModel.findByIdAndUpdate(reviewId, approvedReviewDto)
            await this.rmCacheReview(foundStore.domain)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async sellerDeleteReview(storeId: string, reviewId: string) {
        try {
            const foundStore = await this.storeService.findStore({ _id: storeId })
            const { images, productReviewId } = await this.findReview(reviewId, foundStore.productSellerId)
            await this.rmCacheReview(foundStore.domain)
            if (images && images.length && productReviewId) {
                for (const image of images) {
                    const publicPath = path.join(__dirname, '../../public')
                    try {
                        const filePath = path.join(publicPath, image)
                        fs.unlinkSync(filePath)
                    } catch (error) { }
                }
            }
            await this.reviewModel.findByIdAndDelete(reviewId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async sellerListReview(storeId: string, query: BaseQuery) {
        try {
            let { page, limit, search, filter } = query;
            const foundStore = await this.storeService.findStore({ _id: storeId })
            let filterObject: DataFilter<Partial<Review>> = {
                limit,
                page,
                condition: { productSellerId: foundStore.productSellerId },
                population: [
                    {
                        path: 'productSellerId',
                        select: 'listImageId',
                        populate: [{ path: 'listImageId', select: 'imageUrl' }]
                    }
                ]
            };
            if (filter) {
                filterObject.condition = { ...filterObject.condition, ...filter };
            }
            if (search) {
                filterObject.condition['$or'] = [
                    { name: { $regex: search, $options: 'i' } },
                    { email: { $regex: search, $options: 'i' } },
                    { content: { $regex: search, $options: 'i' } },
                ];
            }
            const listProduct = await this.getListDocument(filterObject);
            return listProduct
        } catch (error) {
            throw error
        }
    }

    generateFilename = (file: any) => {
        if (file.originalname.includes('.png')) file.originalname = file.originalname.replace('.png', '.webp')
        if (file.originalname.includes('.jpg')) file.originalname = file.originalname.replace('.jpg', '.webp')
        if (file.originalname.includes('.jpeg')) file.originalname = file.originalname.replace('.jpeg', '.webp')
        let filename = `${Date.now()}-${file.originalname}`
        filename = filename.replaceAll(/[%#]/g, "");
        return filename
    };

    async sellerCreateReview(storeId: string, createReviewDto: CreateReviewDto, images?: Array<Express.Multer.File>) {
        try {
            const foundStore = await this.storeService.findStore({ _id: storeId })
            let newReview: Partial<Review> = {
                ...createReviewDto,
                productSellerId: foundStore.productSellerId,
                approved: true
            }
            if (images && images.length) {
                for (let image of images) {
                    const fileName = this.generateFilename(image)
                    image.filename = fileName
                    sharp(image.buffer)
                        .resize(800)
                        .toFile(`./public/reviews/${fileName}`, (err, info) => { });
                }
                newReview.images = images.map(image => `reviews/${image.filename}`)
            }
            const newReviewIns = new this.reviewModel(newReview)
            await newReviewIns.save()
            await this.rmCacheReview(foundStore.domain)
            return newReviewIns
        } catch (error) {
            throw error
        }
    }

    async editReview(storeId: string, reviewId: string, updateReviewDto: UpdateReviewDto, images?: Array<Express.Multer.File>) {
        try {
            const foundReview = await this.reviewModel.findOne({ storeId, _id: reviewId })
            if (!foundReview) {
                throw new BaseApiException({
                    message: BaseNoti.REVIEW.REVIEW_NOT_EXISTS
                })
            }
            let update: any = { ...updateReviewDto }
            if (images?.length) {
                for (let image of images) {
                    const fileName = this.generateFilename(image)
                    image.filename = fileName
                    sharp(image.buffer)
                        .resize(800)
                        .toFile(`./public/reviews/${fileName}`, (err, info) => { });
                }
                update.images = images.map(image => `reviews/${image.filename}`)
                if (foundReview.productReviewId) update['$unset'] = { productReviewId: 1 }
            } else {
                update.images = updateReviewDto.images || []
            }
            const updatedReview = await this.reviewModel.findByIdAndUpdate(reviewId, update, { new: true }).lean()
            if (!foundReview.productReviewId) {
                const listImageNew = updatedReview.images
                const listOld = foundReview.images
                const listRm = listOld.filter(image => !listImageNew.includes(image))
                console.log('listRm :>> ', listRm);
                for (const image of listRm) {
                    const publicPath = path.join(__dirname, '../../public')
                    try {
                        const filePath = path.join(publicPath, image)
                        fs.unlinkSync(filePath)
                    } catch (error) { }
                }
            }
            await this.storeService.removeCacheStoreById(storeId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }


    async createDefaultReview(storeId: string, productId: string) {
        try {
            const { productSellerId } = await this.storeService.findStore({ _id: storeId })
            const listReviewFound = await this.productReviewService.findListReview(productId)
            if (listReviewFound && listReviewFound.length) {
                const listIns = listReviewFound.map(review => {
                    const { email, name, images, rating, content, title, productId } = review
                    return { email, name, images, rating, content, title, productId, approved: true, productSellerId }
                })
                await this.reviewModel.insertMany(listIns)
                return listIns
            }
        } catch (error) {
            throw error
        }
    }

    async findReview(reviewId: string, productSellerId: string) {
        try {
            const foundReview = await this.reviewModel.findOne({ _id: reviewId, productSellerId })
            if (!foundReview) {
                throw new BaseApiException({
                    message: BaseNoti.REVIEW.REVIEW_NOT_EXISTS
                })
            }
            return foundReview
        } catch (error) {
            throw error
        }
    }

    async rmCacheReview(domain: string) {
        try {
            await this.cacheService.remove({ key: `${KeyRedisEnum.productBuyer}-${domain}` })
        } catch (error) {

        }
    }
}
