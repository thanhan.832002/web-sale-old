import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AdminListReviewQuery, CreateReviewDto } from './review.dto';
import { Review } from './review.model';
import * as path from 'path';
import * as fs from 'fs';
import * as Promise from 'bluebird'
import { BaseService } from 'src/base/base.service';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { DataFilter } from 'src/base/interfaces/data.interface';
import BaseNoti from 'src/base/notify';

@Injectable()
export class ReviewService extends BaseService<Review> {
    constructor(
        @InjectModel(Review.name) private reviewModel: Model<Review>
    ) { super(reviewModel) }

    // async customerListReview(productId: string, query: BaseQuery) {
    //     try {
    //         let { page, limit, search, filter } = query;
    //         let filterObject: DataFilter<Partial<Review>> = {
    //             limit,
    //             page,
    //             condition: { productId, approved: true, replyTo: { $exists: false } },
    //             population: [
    //                 {
    //                     path: 'productId',
    //                     select: 'name coverId imageIds slug',
    //                     populate: [{ path: 'coverId', select: 'imageUrl' }, { path: 'imageIds', select: 'imageUrl' }]
    //                 },
    //                 // {
    //                 //     path: 'replyTo',
    //                 // }
    //             ],
    //         };
    //         if (filter) {
    //             filterObject.condition = { ...filterObject.condition, ...filter };
    //         }
    //         if (search) {
    //             filterObject.condition['$or'] = [
    //                 { name: { $regex: search, $options: 'i' } },
    //                 { email: { $regex: search, $options: 'i' } },
    //                 { content: { $regex: search, $options: 'i' } },
    //             ];
    //         }
    //         let listProduct: any = await this.getListDocument(filterObject);
    //         listProduct.data = await Promise.map(listProduct.data, async (review) => {
    //             const answerReview = await this.reviewModel.find({ replyTo: review._id, approved: true }).populate([
    //                 {
    //                     path: 'productId',
    //                     select: 'name coverId imageIds slug',
    //                     populate: [{ path: 'coverId', select: 'imageUrl' }, { path: 'imageIds', select: 'imageUrl' }]
    //                 },
    //                 // {
    //                 //     path: 'replyTo',
    //                 // }
    //             ]).lean()
    //             return { ...review, answerReview }
    //         }, { concurrency: 25 })
    //         return listProduct
    //     } catch (error) {
    //         throw error
    //     }
    // }

    // async adminListReview(query: AdminListReviewQuery) {
    //     try {
    //         let { productId, approved, page, limit, search, filter } = query;
    //         let filterObject: DataFilter<Partial<Review>> = {
    //             limit,
    //             page,
    //             condition: {},
    //             population: [
    //                 {
    //                     path: 'productId',
    //                     select: 'name coverId imageIds slug',
    //                     populate: [{ path: 'coverId', select: 'imageUrl' }, { path: 'imageIds', select: 'imageUrl' }]
    //                 },
    //                 {
    //                     path: 'replyTo',
    //                 }
    //             ],
    //         };
    //         if (productId) {
    //             filterObject.condition = { ...filterObject.condition, productId };
    //         }
    //         if (approved) {
    //             filterObject.condition = { ...filterObject.condition, approved };
    //         }
    //         if (filter) {
    //             filterObject.condition = { ...filterObject.condition, ...filter };
    //         }
    //         if (search) {
    //             filterObject.condition['$or'] = [
    //                 { name: { $regex: search, $options: 'i' } },
    //                 { email: { $regex: search, $options: 'i' } },
    //                 { content: { $regex: search, $options: 'i' } },
    //             ];
    //         }
    //         const listProduct = await this.getListDocument(filterObject);
    //         return listProduct
    //     } catch (error) {
    //         throw error
    //     }
    // }

    // async createReview(createReviewDto: CreateReviewDto, images?: Array<Express.Multer.File>) {
    //     try {
    //         if (images && images.length && images[0]) {
    //             createReviewDto.images = images.map(image => `reviews/${image.filename}`)
    //         }
    //         const newReview = new this.reviewModel(createReviewDto)
    //         await newReview.save()
    //         return newReview
    //     } catch (error) {
    //         throw error
    //     }
    // }

    // async findReviewById(reviewId: string) {
    //     try {
    //         const foundReview = await this.reviewModel.findById(reviewId)
    //         if (!foundReview) {
    //             throw new BaseApiException({
    //                 message: BaseNoti.REVIEW.REVIEW_NOT_EXISTS
    //             })
    //         }
    //         return foundReview
    //     } catch (error) {
    //         throw error
    //     }
    // }

    // async updateReview(reviewId: string, updateDto: Partial<Review>) {
    //     try {
    //         const review = await this.reviewModel.findByIdAndUpdate(reviewId, updateDto, { new: true })
    //         return review || null
    //     } catch (error) {
    //         throw error
    //     }
    // }

    // async deleteReview(reviewId: string) {
    //     try {
    //         const foundReview = await this.findReviewById(reviewId)
    //         const { images } = foundReview
    //         if (images && images.length && images[0]) {
    //             const publicPath = path.join(__dirname, '../../public')
    //             for (const imageUrl of images) {
    //                 try {
    //                     const filePath = path.join(publicPath, imageUrl)
    //                     fs.unlinkSync(filePath)
    //                 } catch (error) { }
    //             }
    //         }
    //         await this.reviewModel.findByIdAndDelete(reviewId)
    //     } catch (error) {
    //         throw error
    //     }
    // }

    // async deleteReviewProduct(productId: string) {
    //     try {
    //         const listReview = this.reviewModel.find({ productId })
    //         await Promise.all(listReview, async (review) => {
    //             const foundReview = await this.findReviewById(review._id)
    //             const { images } = foundReview
    //             if (images && images.length && images[0]) {
    //                 const publicPath = path.join(__dirname, '../../public')
    //                 for (const imageUrl of images) {
    //                     try {
    //                         const filePath = path.join(publicPath, imageUrl)
    //                         fs.unlinkSync(filePath)
    //                     } catch (error) { }
    //                 }
    //             }
    //             await this.reviewModel.findByIdAndDelete(review._id)
    //         })
    //     } catch (error) {
    //         throw error
    //     }
    // }

    async findListReviewByProductSellerId(productSellerId: string) {
        try {
            let foundReview: any = await this.reviewModel.find({ productSellerId, approved: true })
            foundReview = await Promise.map(foundReview, async (review) => {
                const answerReview = await this.reviewModel.find({ replyTo: review._id, approved: true })
                review.answerReview = answerReview
                return review
            })
            return foundReview
        } catch (error) {
            return []
        }
    }
}
