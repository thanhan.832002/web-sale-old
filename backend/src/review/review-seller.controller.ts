import { Body, Controller, Delete, Get, Param, Post, Put, Query, UploadedFiles, UseGuards, UseInterceptors } from '@nestjs/common';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { ReviewService } from './review.service';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { ApprovedReviewDto, CreateReviewDto, UpdateReviewDto } from './review.dto';
import { AuthGuard } from '@nestjs/passport';
import { ReviewSellerService } from './review-seller.service';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';

const storageOptionsImages = diskStorage({
    // destination: './public/reviews',
    filename: (req: any, file: any, callback: Function) => {
        callback(null, generateFilename(file));
    },
});

const imageFileFilter = (req: any, file: any, callback: Function) => {
    const ext = extname(file.originalname);
    if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.webp') {
        return callback(null, false);
    }
    callback(null, true);
};

const multerOptionsImage: MulterOptions = {
    // storage: storageOptionsImages,
    fileFilter: imageFileFilter,
    limits: {
        fileSize: 10485760 //10MB
    }
};

const generateFilename = (file: any) => {
    let filename = `${Date.now()}-${file.originalname}`
    filename = filename.replaceAll(/[%#]/g, "");
    return filename
};

@Controller('review-seller')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@ApiTags('Product Seller Review')
export class ReviewSellerController {
    constructor(
        private readonly reviewSellerService: ReviewSellerService
    ) { }

    @Post('create/:storeId')
    @UseInterceptors(AnyFilesInterceptor(multerOptionsImage))
    sellerCreateReview(
        @Param('storeId') storeId: string,
        @Body() createReviewDto: CreateReviewDto,
        @UploadedFiles() images: Array<Express.Multer.File>
    ) {
        return this.reviewSellerService.sellerCreateReview(storeId, createReviewDto, images)
    }

    @Put('edit/:storeId/:reviewId')
    @UseInterceptors(AnyFilesInterceptor(multerOptionsImage))
    sellerEditReview(
        @Param('storeId') storeId: string,
        @Param('reviewId') reviewId: string,
        @Body() updateReviewDto: UpdateReviewDto,
        @UploadedFiles() images: Array<Express.Multer.File>
    ) {
        return this.reviewSellerService.editReview(storeId, reviewId, updateReviewDto, images)
    }

    @Put('approved/:storeId/:reviewId')
    adminApprovedReview(
        @Param('storeId') storeId: string,
        @Param('reviewId') reviewId: string,
        @Body() approvedReviewDto: ApprovedReviewDto
    ) {
        return this.reviewSellerService.sellerApprovedReview(storeId, reviewId, approvedReviewDto)
    }

    @Delete('delete/:storeId/:reviewId')
    sellerDeleteReview(
        @Param('storeId') storeId: string,
        @Param('reviewId') reviewId: string
    ) {
        return this.reviewSellerService.sellerDeleteReview(storeId, reviewId)
    }

    @Get("list/:storeId")
    sellerListReview(@Param('storeId') storeId: string, @Query() query: BaseQuery) {
        return this.reviewSellerService.sellerListReview(storeId, query)
    }

}
