import { Body, Controller, Get, Post, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { ApiTags } from '@nestjs/swagger';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { ReviewService } from './review.service';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { CreateReviewDto } from './review.dto';

const storageOptionsImages = diskStorage({
    destination: './public/reviews',
    filename: (req: any, file: any, callback: Function) => {
        callback(null, generateFilename(file));
    },
});

const imageFileFilter = (req: any, file: any, callback: Function) => {
    const ext = extname(file.originalname);
    if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.webp') {
        return callback(null, false);
    }
    callback(null, true);
};

const multerOptionsImage: MulterOptions = {
    storage: storageOptionsImages,
    fileFilter: imageFileFilter,
};

const generateFilename = (file: any) => {
    return `${Date.now()}-${file.originalname}`;
};

@Controller('review-customer')
@ApiTags('Product Seller Review')
export class ReviewCustomerController {
    constructor(
        private readonly reviewService: ReviewService
    ) { }

    // @Post('create')
    // @UseInterceptors(AnyFilesInterceptor(multerOptionsImage))
    // createReview(@Body() createReviewDto: CreateReviewDto, @UploadedFiles() images: Array<Express.Multer.File>) {
    //     return this.reviewService.createReview(createReviewDto, images)
    // }
}
