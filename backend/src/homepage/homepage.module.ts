import { Module } from '@nestjs/common';
import { HomepageController } from './homepage.controller';
import { HomepageService } from './homepage.service';
import { DomainModule } from 'src/domain/domain.module';
import { StoreModule } from 'src/store/store.module';
import { HomepageSettingModule } from 'src/homepage-setting/homepage-setting.module';
import { ProductSellerModule } from 'src/product-seller/product-seller.module';

@Module({
    imports: [
        DomainModule,
        StoreModule,
        HomepageSettingModule,
        ProductSellerModule
    ],
    providers: [HomepageService],
    exports: [HomepageService],
    controllers: [HomepageController]
})
export class HomepageModule { }
