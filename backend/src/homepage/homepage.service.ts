import { Injectable, OnModuleInit } from '@nestjs/common';
import { async } from 'rxjs';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';
import { DomainService } from 'src/domain/domain.service';
import { HomepageSettingService } from 'src/homepage-setting/homepage-setting.service';
import { StoreStatus } from 'src/store/store.enum';
import { StoreService } from 'src/store/store.service';
import * as Promise from 'bluebird'
import { ProductSellerService } from 'src/product-seller/product-seller.service';
import { ModuleRef } from '@nestjs/core';
import { StorePaymentService } from 'src/store-payment/store-payment.service';

@Injectable()
export class HomepageService implements OnModuleInit {
    private storePaymentService: StorePaymentService
    private storeService: StoreService
    constructor(
        private readonly hompageSettingService: HomepageSettingService,
        private readonly productSellerService: ProductSellerService,
        private readonly domainService: DomainService,
        private moduleRef: ModuleRef,
    ) { }

    onModuleInit() {
        this.storePaymentService = this.moduleRef.get(StorePaymentService, { strict: false });
        this.storeService = this.moduleRef.get(StoreService, { strict: false });
    }

    async buyerCheckDomain(domain: string) {
        try {
            const listDomain = await this.domainService.findListDomainBuyer()
            let check = false
            if (listDomain.includes(domain)) {
                check = true
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: check
            }
        } catch (error) {
            throw error
        }
    }

    async getHomepage(domain: string) {
        try {
            if (domain == 'localhost:2002') domain = 'bestsell2day.com'
            const foundDomain = await this.domainService.findOne({
                filter: {
                    isAdmin: false,
                    enabled: true,
                    domain
                }
            })
            if (!foundDomain) {
                throw new BaseApiException({
                    message: BaseNoti.DOMAIN.DOMAIN_NOT_EXISTS
                })
            }
            const foundHomepageSetting = await this.hompageSettingService.buyerGetHomepageSetting(foundDomain.adminId, foundDomain._id)
            const listFoundStore = await this.storeService.findListStoreHomepage({
                domainId: foundDomain._id,
                status: StoreStatus.active,
                isOnHomePage: true
            })
            let listStore = listFoundStore.map((store: any) => {
                const { domain, slug, productSellerId } = store
                return {
                    _id: store._id,
                    domain,
                    slug,
                    ...productSellerId
                }
            })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: {
                    ...foundHomepageSetting.toObject(),
                    listStore
                }
            }
        } catch (error) {
            throw error
        }
    }

    async getHomepagePolicy(domain: string) {
        try {
            const foundDomain = await this.domainService.findOne({
                filter: {
                    isAdmin: false,
                    enabled: true,
                    domain
                }
            })
            if (!foundDomain) {
                throw new BaseApiException({
                    message: BaseNoti.DOMAIN.DOMAIN_NOT_EXISTS
                })
            }
            const foundHomepageSetting: any = await this.hompageSettingService.buyerGetHomepageSettingAndPolicy(foundDomain.adminId, foundDomain._id)
            const { homepageName, supportEmail } = foundHomepageSetting
            if (foundHomepageSetting?.paymentId?.policyId) {
                for (let policy in foundHomepageSetting.paymentId.policyId) {
                    try {
                        if (policy !== '_id' && policy !== 'createdAt' && policy !== 'updatedAt' && policy !== '__v') {
                            foundHomepageSetting.paymentId.policyId[policy] = foundHomepageSetting.paymentId.policyId[policy].replace(/{{shop}}/g, homepageName)
                            foundHomepageSetting.paymentId.policyId[policy] = foundHomepageSetting.paymentId.policyId[policy].replace(/{{email_support}}/g, supportEmail)
                        }
                    } catch (error) { }
                }
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: foundHomepageSetting
            }
        } catch (error) {
            throw error
        }
    }

    async getPolicy(domain: string) {
        try {
            const foundDomain = await this.domainService.findOne({
                filter: {
                    isAdmin: false,
                    enabled: true,
                    domain
                }
            })
            if (!foundDomain) {
                throw new BaseApiException({
                    message: BaseNoti.DOMAIN.DOMAIN_NOT_EXISTS
                })
            }
            const foundHomepageSetting: any = await this.hompageSettingService.buyerGetPolicy(foundDomain.adminId, foundDomain._id)
            const { homepageName, supportEmail } = foundHomepageSetting
            if (foundHomepageSetting?.paymentId?.policyId) {
                for (let policy in foundHomepageSetting.paymentId.policyId) {
                    try {
                        if (policy !== '_id' && policy !== 'createdAt' && policy !== 'updatedAt' && policy !== '__v') {
                            foundHomepageSetting.paymentId.policyId[policy] = foundHomepageSetting.paymentId.policyId[policy].replace(/{{shop}}/g, homepageName)
                            foundHomepageSetting.paymentId.policyId[policy] = foundHomepageSetting.paymentId.policyId[policy].replace(/{{email_support}}/g, supportEmail)
                        }
                    } catch (error) { }
                }
                return foundHomepageSetting.paymentId.policyId
            }
            return null
        } catch (error) {
            throw error
        }
    }
}
