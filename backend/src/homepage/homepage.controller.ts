import { Controller, Get, Query, Param, Put, Body } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { HomepageService } from './homepage.service';

@Controller('homepage')
@ApiTags('Home Page')

export class HomepageController {
    constructor(
        private readonly homepageService: HomepageService
    ) { }

    @Get('check-domain/:domain')
    async buyerGetListDomain(
        @Param('domain') domain: string
    ) {
        return this.homepageService.buyerCheckDomain(domain)

    }

    @Get('/:domain')
    async getHomepage(
        @Param('domain') domain: string) {
        return this.homepageService.getHomepage(domain)
    }

    @Get('policy/:domain')
    async getPolicy(
        @Param('domain') domain: string
    ) {
        return this.homepageService.getHomepagePolicy(domain)
    }

}
