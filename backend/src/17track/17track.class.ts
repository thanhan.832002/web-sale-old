const axios = require('axios')
export class SeventeenTrack {
    secretkey: string
    constructor(
        secretkey: string
    ) {
        this.secretkey = secretkey
    }
    async registerTracking(listNumber) {
        try {
            let failed = 0
            const url = 'https://api.17track.net/track/v2/register'
            for (let i = 1; (i - 1) * 40 < listNumber.length; i++) {
                const listCallApi = listNumber.slice((i - 1) * 40, 40)
                const configAxios = {
                    method: 'post',
                    url,
                    headers: {
                        '17token': this.secretkey,
                        'Content-Type': 'application/json'
                    },
                    data: listCallApi
                }
                let stateRejected = 0
                try {
                    const { data } = await axios(configAxios)
                    if (data?.data?.rejected) {
                        stateRejected = data.data.rejected.length
                    }
                    return data
                } catch (error) {
                    stateRejected = listCallApi.length
                }
                failed += stateRejected
            }
            return { failed }
        } catch (error) {
            throw error
        }
    }

}