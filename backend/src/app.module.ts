import { CacheModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { CategoryModule } from './category/category.module';
import { ScheduleModule } from '@nestjs/schedule';
import { GalleryModule } from './gallery/gallery.module';
import { CronModule } from './cron/cron.module';
import { EmailModule } from './email/email.module';
import { ProductModule } from './product/product.module';
import { VariantModule } from './variant/variant.module';
import { VariantOptionModule } from './variant-option/variant-option.module';
import { ReviewModule } from './review/review.module';
import { PaymentModule } from './payment-method/payment.module';
import { CouponModule } from './coupon/coupon.module';
import { PolicyModule } from './policy/policy.module';
import { UserPaymentModule } from './seller-payment/seller-payment.module';
import { ShippingModule } from './shipping/shipping.module';
import { ViewModule } from './view/view.module';
import { StoreModule } from './store/store.module';
import { ProductSellerModule } from './product-seller/product-seller.module';
import { PixelModule } from './pixel/pixel.module';
import { VariantSellerModule } from './variant-seller/variant-seller.module';
import { StoreSettingModule } from './store-setting/store-setting.module';
import { OrderModule } from './order/order.module';
import { SelectedProductSellerModule } from './selected-product-seller/selected-product-seller.module';
import { TrackingHistoryModule } from './tracking-history/tracking-history.module';
import { AbandonedCheckoutModule } from './abandoned-checkout/abandoned-checkout.module';
import { StatsModule } from './stats/stats.module';
import { BestSellerModule } from './best-seller/best-seller.module';
import { AdminSettingModule } from './admin-setting/admin-setting.module';
import { SendAbandonedModule } from './do-send-abandoned/send-abandoned.module';
import { DomainModule } from './domain/domain.module';
import { ProductBuyerModule } from './product-buyer/product-buyer.module';
import { SendgridApiModule } from './sendgrid-api/sendgrid-api.module';
import { ZerosslModule } from './zerossl/zerossl.module';
import { StorePaymentModule } from './store-payment/store-payment.module';
import { StoreUpsaleModule } from './store-upsale/store-upsale.module';
import { SellerDiscountModule } from './seller-discount/seller-discount.module';
import { HomepageSettingModule } from './homepage-setting/homepage-setting.module';
import { HomepageModule } from './homepage/homepage.module';
import { CachePageModule } from './cache-page/cache-page.module';
import { TrackingModule } from './tracking/tracking.module';
import { UtmModule } from './utm/utm.module';
import { ProductReviewModule } from './product-review/product-review.module';
import config from './base/config/config';
import { StoreShippingModule } from './store-shipping/store-shipping.module';

@Module({
  imports: [
    // MongooseModule.forRoot('mongodb://websalebig:websalebigztechpassword@51.79.229.71:27017/websalebig'),//old
    MongooseModule.forRoot(
      'mongodb://lanhdao:mongopasshcom@194.233.71.80:27017/websaleopensiteold',
    ), // production
    // MongooseModule.forRoot('mongodb://127.0.0.1/websale'),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
    }),
    ScheduleModule.forRoot(),
    AuthModule,
    UserModule,
    CouponModule,
    ProductModule,
    VariantModule,
    VariantOptionModule,
    ReviewModule,
    PaymentModule,
    CategoryModule,
    GalleryModule,
    CronModule,
    EmailModule,
    PolicyModule,
    UserPaymentModule,
    ShippingModule,
    ViewModule,
    StoreModule,
    ProductSellerModule,
    PixelModule,
    VariantSellerModule,
    StoreSettingModule,
    OrderModule,
    SelectedProductSellerModule,
    TrackingHistoryModule,
    AbandonedCheckoutModule,
    StatsModule,
    BestSellerModule,
    AdminSettingModule,
    SendAbandonedModule,
    DomainModule,
    ProductBuyerModule,
    SendgridApiModule,
    ZerosslModule,
    StorePaymentModule,
    StoreUpsaleModule,
    SellerDiscountModule,
    HomepageSettingModule,
    HomepageModule,
    CachePageModule,
    TrackingModule,
    UtmModule,
    ProductReviewModule,
    StoreShippingModule,
  ],
})
export class AppModule {}
