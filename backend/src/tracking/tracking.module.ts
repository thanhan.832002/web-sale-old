import { Module } from '@nestjs/common';
import { TrackingController } from './tracking.controller';
import { TrackingService } from './tracking.service';
import { OrderModule } from 'src/order/order.module';
import { EmailModule } from 'src/email/email.module';

@Module({
  imports: [
    OrderModule,
    EmailModule
  ],
  controllers: [TrackingController],
  providers: [TrackingService]
})
export class TrackingModule { }
