import { Controller } from '@nestjs/common';
import { TrackingService } from './tracking.service';
import { Post } from '@nestjs/common';
import { Body } from '@nestjs/common';

@Controller('tracking')
export class TrackingController {
    constructor(
        private readonly trackingService: TrackingService
    ) { }

    @Post('/webhook')
    async webhookTracking(@Body() body: any) {
        console.log('JSON.stringify(body) :>> ', JSON.stringify(body));
        return this.trackingService.webhookTracking(body)
    }
}
