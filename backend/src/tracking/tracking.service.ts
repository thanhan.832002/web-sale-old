import { Injectable } from '@nestjs/common';
import { EmailService } from 'src/email/email.service';
import { OrderService } from 'src/order/order.service';
import * as moment from 'moment';

@Injectable()
export class TrackingService {
    constructor(
        private readonly orderService: OrderService,
        private readonly emailService: EmailService
    ) { }

    async webhookTracking(body) {
        try {
            const { event, data } = body
            if (!data || !event) return
            if (event == 'TRACKING_UPDATED') {
                const { number, carrier, track_info } = data
                if (!number || !track_info) return
                const { latest_status, latest_event } = track_info
                if (!latest_status || !latest_event) return
                const { time_iso, description } = latest_event
                const foundOrder = await this.orderService.findOne({ filter: { trackingNumber: number } })
                if (!foundOrder) return
                await Promise.all([
                    // this.orderService.adminAddTimeline(foundOrder._id, {
                    //     content: `Tracking email was sent to: ${foundOrder.shippingAddress.email}`,
                    //     time: moment(time_iso).unix()
                    // }),
                    // this.emailService.emailTrackingUpdated(adminId, foundOrder)
                ])
            }
        } catch (error) {
            throw error
        }
    }
}
