import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsNotEmpty, IsOptional, Validate } from "class-validator";
import { Schema, Types, Document } from "mongoose";
import { schemaOptions } from "src/base/base.shema";
import { IsObjectId } from "src/base/custom-validator";
import { AddTracking } from "src/order/order.dto";

export class Tracking {
    @ApiProperty()
    @IsNotEmpty()
    orderCode: string

    @ApiProperty()
    @IsNotEmpty()
    oldTrackingNumber?: string

    @ApiProperty()
    @IsNotEmpty()
    trackingNumber: string

    @ApiProperty()
    @IsNotEmpty()
    carrierCode: string
}

const TrackingSchema = new Schema({
    orderCode: String,
    oldTrackingNumber: String,
    trackingNumber: String,
    carrierCode: String
});

export const TrackingSchemaHistory: Schema = new Schema({
    serial: {
        type: Number,
        default: 1
    },
    trackings: {
        type: [TrackingSchema]
    },
    success: Number,
    failed: Number,
    listFailed: {
        type: [TrackingSchema]
    },
    thirdPartyError: Number,
    total: Number,
    type: String,
    adminId: {
        type: Types.ObjectId,
        index: true,
        ref: 'User'
    },
    listErrPaypal: String,
    listErrStripe: String,
    duplicate: Number,
    listFailedMail: [String]
}, { ...schemaOptions, collection: 'TrackingHistory' })

export class TrackingHistory extends Document {
    serial: number
    trackings: Tracking[]
    success: number
    failed: number
    thirdPartyError: number
    total: number
    type: string
    duplicate: number
    listFailedMail: string[]
    adminId: string
}

export class UploadTrackingHistoryDto {
    trackings: Tracking[]
    listFailed: Tracking[]
    success: number
    failed: number
    thirdPartyError: number
    total: number
    type: string
    @ApiProperty()
    @IsNotEmpty()
    @Validate(IsObjectId)
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    adminId: string
    listErrPaypal: string
    listErrStripe: string
    listFailedMail: string[]
    duplicate: number
}
