import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TrackingHistory, TrackingSchemaHistory } from './tracking-history.model';
import { TrackingHistoryService } from './tracking-history.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: TrackingHistory.name, schema: TrackingSchemaHistory }]),
  ],
  providers: [TrackingHistoryService],
  exports: [TrackingHistoryService]
})
export class TrackingHistoryModule { }
