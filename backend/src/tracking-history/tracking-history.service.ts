import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BaseService } from 'src/base/base.service';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { DataFilter } from 'src/base/interfaces/data.interface';
import BaseNoti from 'src/base/notify';
import { TrackingHistory, UploadTrackingHistoryDto } from './tracking-history.model';
import { Timeout } from '@nestjs/schedule';

@Injectable()
export class TrackingHistoryService extends BaseService<TrackingHistory>{
    constructor(
        @InjectModel(TrackingHistory.name) public uploadTrackingModel: Model<TrackingHistory>,
    ) { super(uploadTrackingModel) }

    async saveHistoryUploadTracking({ trackings, success, failed, thirdPartyError, total, type, adminId, listFailed, duplicate, listErrPaypal, listErrStripe, listFailedMail }: UploadTrackingHistoryDto) {
        try {
            let serial = 1
            const lastUploadArray = await this.uploadTrackingModel.find().sort({ _id: -1 }).limit(1)
            if (lastUploadArray && lastUploadArray.length && lastUploadArray[0] && lastUploadArray[0].serial) {
                serial = lastUploadArray[0].serial + 1
            }
            const newHistory = new this.uploadTrackingModel({ listFailed, listErrPaypal, listErrStripe, serial, trackings, success, failed, thirdPartyError, total, type, adminId, duplicate, listFailedMail })
            await newHistory.save()
            return newHistory
        } catch (error) {
            throw error
        }
    }

    async getHistoryUploadTracking(adminId: string, query: BaseQuery) {
        try {
            let { page, limit, search, filter, sort } = query;
            let filterObject: DataFilter<Partial<TrackingHistory>> = {
                limit,
                page,
                sort: { serial: -1 },
                condition: { adminId },
                population: [],
            };
            if (filter) {
                filterObject.condition = { ...filterObject.condition, ...filter };
            }
            if (search) {
                filterObject.condition['$or'] = [
                    { email: { $regex: search, $options: 'i' } },

                ];
            }
            const uploadTrackingHistory = await this.getListDocument(filterObject);
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: uploadTrackingHistory,
            };
        } catch (error) {
            throw error
        }
    }

    async findByIdAndUpdate(historyId: string, updateField: any) {
        try {
            await this.uploadTrackingModel.findByIdAndUpdate(historyId, updateField)
        } catch (error) {
            throw error
        }
    }

    async deleteUpload(uploadId: string) {
        try {
            await this.uploadTrackingModel.findByIdAndDelete(uploadId)
        } catch (error) {
            throw error
        }
    }
}
