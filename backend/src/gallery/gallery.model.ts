import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsNotEmpty, IsOptional, Validate } from "class-validator";
import { Schema, Types, Document } from "mongoose";
import { schemaOptions } from "src/base/base.shema";
import { IsObjectId } from "src/base/custom-validator";
import { Role } from "src/base/enum/roles.enum";

export const GallerySchema: Schema = new Schema({
    userId: {
        type: Types.ObjectId,
        index: true,
        ref: 'User'
    },
    adminId: {
        type: Types.ObjectId,
        index: true,
        ref: 'User'
    },
    storeId: {
        type: Types.ObjectId,
        index: true,
        ref: 'Store'
    },
    imageUrl: {
        type: String,
        required: true
    },
    role: {
        type: Number,
        enum: Role
    }
}, { ...schemaOptions, collection: 'Gallery' })

export class Gallery extends Document {
    @ApiProperty()
    @IsNotEmpty()
    @Validate(IsObjectId)
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    adminId: string

    @ApiProperty()
    @IsNotEmpty()
    @Validate(IsObjectId)
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    userId: string

    @ApiProperty()
    @IsOptional()
    @Validate(IsObjectId)
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    storeId: string

    @ApiProperty()
    @IsNotEmpty()
    imageUrl: string

    @ApiProperty()
    @IsNotEmpty()
    role: Role
}   