import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as path from 'path';
import * as fs from 'fs';
import { Gallery } from './gallery.model';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';
const webp = require('webp-converter');
import { Readable } from "stream"
import { Role } from 'src/base/enum/roles.enum';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { StoreService } from 'src/store/store.service';
import { ModuleRef } from '@nestjs/core';
import { UserService } from 'src/user/user.service';
import { Timeout } from '@nestjs/schedule';
var ffmpeg = require('fluent-ffmpeg');
const sharp = require('sharp');


@Injectable()
export class GalleryService implements OnModuleInit {
    private userService: UserService
    constructor(
        @InjectModel(Gallery.name) private readonly galleryModel: Model<Gallery>,
        private moduleRef: ModuleRef
    ) { }

    convertImage(image: Readable, outputName: string) {
        ffmpeg().input(image).saveToFile(outputName)
    }

    onModuleInit() {
        this.userService = this.moduleRef.get(UserService, { strict: false });
    }


    bufferToStream(buffer: Buffer): Readable {
        const readable = new Readable()
        readable._read = () => { }
        readable.push(buffer)
        readable.push(null)
        return readable
    }

    generateFilename = (file: any) => {
        if (file.originalname.includes('.png')) file.originalname = file.originalname.replace('.png', '.webp')
        if (file.originalname.includes('.jpg')) file.originalname = file.originalname.replace('.jpg', '.webp')
        if (file.originalname.includes('.jpeg')) file.originalname = file.originalname.replace('.jpeg', '.webp')
        let filename = `${Date.now()}-${file.originalname}`
        filename = filename.replaceAll(/[%#]/g, "");
        return filename
    };

    async createGallery(userId: string, role: Role, images: Array<Express.Multer.File>) {
        try {
            let adminId = userId
            if (role == Role.seller) {
                const foundUser = await this.userService.findUserById(userId)
                adminId = foundUser.adminId
            }
            images = images.reverse()
            let listGallery: Partial<Gallery>[] = []
            for (let image of images) {
                const fileName = this.generateFilename(image)
                sharp(image.buffer, { animated: true })
                    // .resize(800)
                    .toFile(`./public/images/${fileName}`, (err, info) => { });
                const newGallery = new this.galleryModel({
                    imageUrl: `images/${fileName}`,
                    userId,
                    role,
                    adminId
                })
                listGallery.push(newGallery)
            }
            await this.galleryModel.insertMany(listGallery)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listGallery
            }
        } catch (error) {
            throw error
        }
    }

    async createStoreGallery(storeId: string, { userId, role }: JwtTokenDecrypted, images: Array<Express.Multer.File> = []) {
        try {
            let adminId = userId
            if (role == Role.seller) {
                const foundUser = await this.userService.findUserById(userId)
                adminId = foundUser.adminId
            }
            let listGallery = []
            for (let image of images) {
                const fileName = this.generateFilename(image)
                sharp(image.buffer, { animated: true })
                    // .resize(800)
                    .toFile(`./public/images/${fileName}`, (err, info) => { });
                listGallery.push({
                    imageUrl: `images/${fileName}`,
                    userId,
                    role,
                    storeId,
                    adminId
                })
            }
            listGallery?.length && await this.galleryModel.insertMany(listGallery)
            return listGallery
        } catch (error) {
            throw error
        }
    }

    async getStoreGallery(storeId: string) {
        try {
            const listImage = await this.galleryModel.find({ storeId }).sort({ _id: -1 })
            return listImage || []
        } catch (error) {

        }
    }

    async storeCreateGallery(storeId: string, { role, userId }: JwtTokenDecrypted, images: Array<Express.Multer.File>) {
        try {
            let adminId = userId
            if (role == Role.seller) {
                const foundUser = await this.userService.findUserById(userId)
                adminId = foundUser.adminId
            }
            images = images.reverse()
            let listGallery = []
            for (let image of images) {
                const fileName = this.generateFilename(image)
                sharp(image.buffer)
                    .resize(800)
                    .toFile(`./public/images/${fileName}`, (err, info) => { });
                listGallery.push({
                    imageUrl: `images/${fileName}`,
                    userId,
                    storeId,
                    role,
                    adminId
                })
            }
            await this.galleryModel.insertMany(listGallery)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listGallery
            }
        } catch (error) {
            throw error
        }
    }

    async getListImage(userId: string, role: Role) {
        try {
            let adminId = userId
            if (role == Role.seller) {
                const foundUser = await this.userService.findUserById(userId)
                adminId = foundUser.adminId
            }
            const filterImage: Partial<Gallery> = { userId, adminId }
            //@ts-ignore
            const listImage = await this.galleryModel.find(filterImage).sort({ _id: -1 })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listImage
            }
        } catch (error) {
            throw error
        }
    }

    async deleteGallery(userId: string, listImageId: string[]) {
        try {
            for (let galleryId of listImageId) {
                const deletedGallery = await this.galleryModel.findOneAndDelete({ _id: galleryId, userId })
                if (deletedGallery && deletedGallery.imageUrl) {
                    const url = deletedGallery.imageUrl
                    const publicPath = path.join(__dirname, '../../public')
                    try {
                        const filePath = path.join(publicPath, url)
                        fs.unlinkSync(filePath)
                    } catch (error) { }
                }
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async deleteStoreGallery(userId: string, galleryId: string) {
        try {
            const deletedGallery = await this.galleryModel.findOneAndDelete({ userId, _id: galleryId })
            if (deletedGallery && deletedGallery.role == Role.seller && deletedGallery.imageUrl) {
                const url = deletedGallery.imageUrl
                const publicPath = path.join(__dirname, '../../public')
                try {
                    const filePath = path.join(publicPath, url)
                    fs.unlinkSync(filePath)
                } catch (error) { }
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async getImageUrl(imageId: string) {
        try {
            if (!imageId) return
            const image = await this.galleryModel.findById(imageId)
            if (image) {
                return image.imageUrl
            } else return null
        } catch (error) {
            throw error
        }
    }

    async checkImageExists(userId: string, imageId: string) {
        try {
            const foundImage = await this.galleryModel.findOne({ userId, imageId })
            if (!foundImage) {
                throw new BaseApiException({
                    message: BaseNoti.GALLERY.IMAGE_NOT_EXISTS
                })
            }
            return foundImage
        } catch (error) {
            throw error
        }
    }

    async deleteListImage(listImageId: string[]) {
        try {
            await this.galleryModel.deleteMany({ _id: { $in: listImageId } })
        } catch (error) {
            throw error
        }
    }

}
