import { ApiProperty, ApiPropertyOptional, PickType } from "@nestjs/swagger";
import { IsArray, IsNotEmpty, IsOptional } from "class-validator";
import { Gallery } from './gallery.model';

export class CreateGalleryDto { }

export class ChangeGalleryTagDto {
    @ApiProperty()
    @IsArray()
    @IsNotEmpty()
    listGalleryId: string[]

    @ApiProperty()
    @IsNotEmpty()
    galleryTagId: string
}

export class DeleteListImageDto {
    @ApiProperty({ type: [String] })
    @IsArray()
    @IsNotEmpty()
    listImageId: string[]
}