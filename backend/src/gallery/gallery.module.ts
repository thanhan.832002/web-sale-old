import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GalleryAdminController } from './gallery-admin.controller';
import { GalleryController } from './gallery.controller';
import { Gallery, GallerySchema } from './gallery.model';
import { GalleryService } from './gallery.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Gallery.name, schema: GallerySchema }]),
  ],
  controllers: [GalleryController, GalleryAdminController],
  providers: [GalleryService],
  exports: [GalleryService]
})
export class GalleryModule { }
