import { Body, Controller, Delete, Get, HttpStatus, Param, ParseFilePipeBuilder, Post, Put, Query, UploadedFiles, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FilesInterceptor } from '@nestjs/platform-express';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { Roles } from 'src/base/decorator/roles.decorator';
import { Role } from 'src/base/enum/roles.enum';
import { RolesGuard } from 'src/base/guard/roles.guard';
import { GalleryService } from './gallery.service';


const storageOptionsImages = diskStorage({
    // destination: './public/images',
    filename: (req: any, file: any, callback: Function) => {
        callback(null, generateFilename(file));
    },
});

const imageFileFilter = (req: any, file: any, callback: Function) => {
    const ext = extname(file.originalname);
    if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.webp') {
        return callback(null, false);
    }
    callback(null, true);
};

const multerOptionsImage: MulterOptions = {
    // storage: storageOptionsImages,
    fileFilter: imageFileFilter,
    limits: {
        fileSize: 52428800 //20MB
        // fileSize: 1000
    }
};

const generateFilename = (file: any) => {
    return `${Date.now()}-${file.originalname}`;
};

@Controller('gallery')
@ApiTags('Gallery')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
export class GalleryController {
    constructor(
        private readonly galleryService: GalleryService
    ) { }

    @Post('create')
    @UseInterceptors(FilesInterceptor('images', Number.POSITIVE_INFINITY, multerOptionsImage))
    createGallery(@GetUser() { userId, role }: JwtTokenDecrypted, @UploadedFiles() images: Array<Express.Multer.File>) {
        return this.galleryService.createGallery(userId, role, images)
    }
}
