import { ApiProperty, PickType } from "@nestjs/swagger";
import { AbandonedCheckout } from "./abandoned-checkout.model";
import { IsInt, IsNotEmpty, IsNumber, ValidateNested } from "class-validator";
import { Type } from "class-transformer";

export class AbandonedCoupon {
    @ApiProperty()
    @IsNotEmpty()
    code: string

    // @ApiProperty()
    // @IsNotEmpty()
    // @IsNumber()
    // @Type(() => Number)
    // discount: number
}

export class EditAbandonedDto extends PickType(AbandonedCheckout, ['time', 'timestamp', 'discount', 'emailSubject', 'emailTemplate']) {
    @ApiProperty()
    @ApiProperty({ type: AbandonedCoupon })
    @ValidateNested({ each: true })
    @Type(() => AbandonedCoupon)
    @IsNotEmpty()
    coupon: AbandonedCoupon
}