import { Module } from '@nestjs/common';
import { AbandonedCheckoutService } from './abandoned-checkout.service';
import { AbandonedCheckoutController } from './abandoned-checkout.controller';
import { AbandonedCheckout, AbandonedCheckoutSchema } from './abandoned-checkout.model';
import { MongooseModule } from '@nestjs/mongoose';
import { CouponModule } from 'src/coupon/coupon.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: AbandonedCheckout.name, schema: AbandonedCheckoutSchema }]),
    CouponModule
  ],
  providers: [AbandonedCheckoutService],
  controllers: [AbandonedCheckoutController],
  exports: [AbandonedCheckoutService]
})
export class AbandonedCheckoutModule { }
