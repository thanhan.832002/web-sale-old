import { Body, Controller, Get, Put, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { EditAbandonedDto } from './abandoned-checkout.dto';
import { AbandonedCheckoutService } from './abandoned-checkout.service';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
@ApiBearerAuth()
@UseGuards(AdminGuard)
@UseGuards(AuthGuard('jwt'))
@Controller('abandoned-checkout')
@ApiTags('Abandoned Checkouts')
export class AbandonedCheckoutController {
    constructor(
        private readonly abandonedCheckoutService: AbandonedCheckoutService
    ) { }

    @Get('list')
    getListAbandon(
        @GetUser() { userId }: JwtTokenDecrypted
    ) {
        return this.abandonedCheckoutService.getListAbandon(userId)
    }

    @Put('edit')
    editAbandoned(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() editAbandonedDto: EditAbandonedDto
    ) {
        return this.abandonedCheckoutService.editAbandoned(userId, editAbandonedDto)
    }
}
