import { ApiProperty, } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsEnum, IsNotEmpty, IsNumber, IsOptional, Min, Validate } from "class-validator";
import { Schema, Document, Types } from "mongoose";
import { schemaOptions } from "src/base/base.shema";
import { IsObjectId } from "src/base/custom-validator";

export enum AbandonedTimeEnum {
    first = 1,
    second,
    third
}

export const AbandonedCheckoutSchema: Schema = new Schema({
    time: {
        type: Number,
        index: true,
        enum: [1, 2, 3]
    },
    timestamp: Number,
    discount: Number,
    adminId: {
        type: Types.ObjectId,
        ref: 'User'
    },
    emailTemplate: {
        type: String,
        default: ''
    },
    emailSubject: {
        type: String,
        default: ''
    },
    couponId: {
        type: Types.ObjectId,
        ref: 'Coupon'
    }
}, { ...schemaOptions, collection: 'AbandonedCheckout' })

export class AbandonedCheckout extends Document {
    @ApiProperty()
    @IsEnum(AbandonedTimeEnum)
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsNotEmpty()
    time: number

    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsNotEmpty()
    @IsNotEmpty()
    timestamp: number

    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsNotEmpty()
    discount: number

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    adminId: string

    @ApiProperty()
    @IsNotEmpty()
    emailTemplate: string

    @ApiProperty()
    @IsNotEmpty()
    emailSubject: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsOptional()
    couponId: string
}   