import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import BaseNoti from 'src/base/notify';
import { EditAbandonedDto } from './abandoned-checkout.dto';
import { AbandonedCheckout } from './abandoned-checkout.model';
import { CouponService } from 'src/coupon/coupon.service';
import { Coupon, CouponTypeEnum, DiscountEnum } from 'src/coupon/coupon.model';

@Injectable()
export class AbandonedCheckoutService {
    constructor(
        @InjectModel(AbandonedCheckout.name) private readonly abandonedCheckoutModel: Model<AbandonedCheckout>,
        private readonly couponService: CouponService

    ) { }

    async editAbandoned(userId: string, editAbandonedDto: EditAbandonedDto) {
        try {
            const { time, timestamp, discount, emailSubject, emailTemplate, coupon } = editAbandonedDto
            const { code } = coupon
            const foundAbando = await this.abandonedCheckoutModel.findOne({ time, adminId: userId })
            let abandoReturn: AbandonedCheckout = null
            if (!foundAbando) {
                const abandoCouponIns: Partial<Coupon> = {
                    adminId: userId,
                    type: CouponTypeEnum.abandoned,
                    code,
                    discount,
                    status: true,
                    discountType: DiscountEnum.percent
                }
                const abandoCoupon = await this.couponService.createCouponAbandoned(abandoCouponIns)
                const abandoIns = new this.abandonedCheckoutModel({
                    time,
                    timestamp,
                    discount,
                    adminId: userId,
                    emailSubject,
                    emailTemplate,
                    couponId: abandoCoupon.id
                })
                abandoReturn = abandoIns
                await abandoIns.save()
            } else {
                const abandoUpdate: Partial<AbandonedCheckout> = {
                    time,
                    timestamp,
                    discount,
                    adminId: userId,
                    emailSubject,
                    emailTemplate,
                }
                if (foundAbando.couponId) {
                    await this.couponService.updateCouponAbando(foundAbando.couponId, { code, adminId: userId })
                } else {
                    const abandoCouponIns: Partial<Coupon> = {
                        adminId: userId,
                        type: CouponTypeEnum.abandoned,
                        code,
                        discount,
                        status: true,
                        discountType: DiscountEnum.percent
                    }
                    const abandoCoupon = await this.couponService.createCouponAbandoned(abandoCouponIns)
                    abandoUpdate.couponId = abandoCoupon.id
                }
                abandoReturn = await this.abandonedCheckoutModel.findByIdAndUpdate(foundAbando.id, abandoUpdate, { new: true })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: abandoReturn
            }
        } catch (error) {
            throw error
        }
    }

    async getListAbandon(adminId: string) {
        try {
            let list = await this.abandonedCheckoutModel.find({ adminId }).sort({ time: 1, discount: 1 }).populate('couponId').lean()
            if (!list || !list.length) {
                list = await this.createDefaultAbandon(adminId)
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: list
            }
        } catch (error) {
            throw error
        }
    }

    async createDefaultAbandon(adminId: string) {
        try {
            const list = await this.abandonedCheckoutModel.insertMany([
                {
                    timestamp: 60 * 5,
                    time: 1,
                    discount: 0,
                    adminId,
                    emailTemplate: '',
                    emailSubject: ''
                },
                {
                    timestamp: 60 * 30,
                    time: 2,
                    discount: 0,
                    adminId,
                    emailTemplate: '',
                    emailSubject: ''
                },
                {
                    timestamp: 60 * 60 * 12,
                    time: 3,
                    discount: 0,
                    adminId,
                    emailTemplate: '',
                    emailSubject: ''
                }
            ])
            return list
        } catch (error) {
            throw error
        }
    }

    async findOne(filter: any) {
        try {
            const found = await this.abandonedCheckoutModel.findOne(filter)
            return found
        } catch (error) {
            throw error
        }
    }

    async findOneTestMail(filter) {
        try {
            const found = await this.abandonedCheckoutModel.findOne(filter).populate('couponId')
            return found
        } catch (error) {
            throw error
        }
    }
}
