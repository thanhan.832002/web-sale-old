import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SharedModule } from 'src/base/shared.module';
import { ViewController } from './view.controller';
import { View, ViewSchema } from './view.model';
import { ViewService } from './view.service';
import { AdminSettingModule } from 'src/admin-setting/admin-setting.module';
import { StoreModule } from 'src/store/store.module';
import { UtmModule } from 'src/utm/utm.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: View.name, schema: ViewSchema }]),
    SharedModule,
    AdminSettingModule,
    StoreModule,
    UtmModule
  ],
  controllers: [ViewController],
  providers: [ViewService],
  exports: [ViewService]
})
export class ViewModule { }
