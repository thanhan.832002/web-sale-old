export enum ViewType {
    view = 'view',
    addToCart = 'addToCart',
    checkout = 'checkout',
    gocheckout = 'gocheckout',
}

export enum KeyRedisEnum {
    timezone = 'timezone',
    listUtm = 'listutm',
    sendgrid = 'sendgrid',
    listDomain = "listdomain",
    productBuyer = 'productbuyer',
    storeSetting = 'storesetting',
    pixel = 'pixel',
    upsale = 'upsale',
    paymentlist = 'paymentlist',
    skstripe = 'skstripe',
    policy = 'policy',
    listPayment = 'listpayment',
    shippingdetail = 'shippingdetail',
    listshipping = 'listshipping',
    orderprefix = 'orderprefix',
    listUtmTraffic = 'listutmtraffic',
    listStoreTraffic = 'liststore'
}