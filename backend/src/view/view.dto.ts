import { Validate } from "class-validator";
import { Types } from "mongoose";
import { IsListObjectId, IsObjectId } from "src/base/custom-validator";
import { BaseQuery } from "src/base/interfaces/base-query.interface";

export class ViewQuery {
    @Validate(IsListObjectId)
    listProductSellerId?: string[] | Types.ObjectId[]

    dateFrom: number

    dateTo: number
}