import { Controller, Ip, Param, Post, Query, UseGuards } from '@nestjs/common';
import { KeyGuard } from 'src/base/guard/key.guard';
import { ViewService } from './view.service';
import { GetDomain } from 'src/base/decorator/get-domain.decorator';
import { StoreService } from 'src/store/store.service';
import { DomainQuery, SlugQuery } from 'src/base/interfaces/base-query.interface';

@Controller('view')
export class ViewController {
    constructor(
        private readonly viewService: ViewService,
        private readonly storeService: StoreService
    ) { }

    @UseGuards(KeyGuard)
    @Post('count-add-to-cart')
    async countAddToCart(
        @Query() { domain, utm_campaign, utm_content, utm_medium, utm_source, sessionId }: DomainQuery,
        @Ip() ip: string
    ) {
        try {
            const foundStore = await this.storeService.findStoreBuyer(domain)
            const { adminId } = foundStore
            return this.viewService.countAddToCart(adminId, foundStore._id, 1, { utm_campaign, utm_content, utm_medium, utm_source, sessionId }, ip)
        } catch (error) {
            throw error
        }
    }
}
