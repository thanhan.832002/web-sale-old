import { ApiProperty } from "@nestjs/swagger"
import { IsNotEmpty, IsNumber, Validate } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsObjectId } from "src/base/custom-validator"

export const ViewSchema: Schema = new Schema({
    day: {
        type: Number,
        default: (new Date()).getDate()
    },
    month: {
        type: Number,
        default: (new Date()).getMonth() + 1
    },
    year: {
        type: Number,
        default: (new Date()).getFullYear()
    },
    view: Number,
    addToCart: Number,
    checkout: Number,
    goCheckout: Number,
    totalCancel: Number,
    total: Number,
    productCost: Number,
    quantity: Number,
    orders: Number,
    tip: Number,
    totalAbandon: Number,
    productCostAbandon: Number,
    quantityAbandon: Number,
    ordersAbandon: Number,
    tipAbandon: Number,
    adminId: {
        type: Types.ObjectId,
        ref: 'User',
        index: true
    },
    sellerId: {
        type: Types.ObjectId,
        ref: 'User',
        index: true
    },
    productId: {
        type: Types.ObjectId,
        ref: 'Product',
        index: true
    },
    productSellerId: {
        type: Types.ObjectId,
        ref: 'ProductSeller',
        index: true
    },
    storeId: {
        type: Types.ObjectId,
        ref: 'Store',
        index: true
    },
    utmId: {
        type: Types.ObjectId,
        ref: 'Utm'
    }
}, { ...schemaOptions, collection: 'View' })


export class View extends Document {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    adminId: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    sellerId: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    productSellerId: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    productId: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    storeId: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    utmId: string

    @ApiProperty()
    @IsNotEmpty()
    createdAt: Date

    day: number
    month: number
    year: number
    view: number
    addToCart: number
    checkout: number
    goCheckout: number
    total: number
    tip: number
    productCost: number
    quantity: number
    orders: number
    totalAbandon: number
    tipAbandon: number
    productCostAbandon: number
    quantityAbandon: number
    ordersAbandon: number
    totalCancel: number
}
