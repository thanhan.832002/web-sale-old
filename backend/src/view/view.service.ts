import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CacheService } from 'src/base/caching/cache.service';
import { View } from './view.model';
var momentTz = require("moment-timezone");
import { KeyRedisEnum, ViewType } from './view.enum';
import { AdminSettingService } from 'src/admin-setting/admin-setting.service';
import { SellerUtmQuery } from 'src/stats/stats.dto';
import { UtmService } from 'src/utm/utm.service';
import { UtmQuery } from 'src/product-buyer/product-buyer.query';
import * as Promise from 'bluebird'
import { CountViewDto } from 'src/product-buyer/product-buyer.dto';
import * as moment from 'moment'
import { ViewUtmQuery } from 'src/base/interfaces/base-query.interface';
import BaseNoti from 'src/base/notify';
@Injectable()
export class ViewService {
    constructor(
        @InjectModel(View.name) public viewModel: Model<View>,
        private readonly adminSettingService: AdminSettingService,
        private readonly cacheService: CacheService,
        private readonly utmService: UtmService
    ) { }

    async countView({ utm_campaign, utm_content, utm_medium, utm_source, storeId, sessionId, adminId }: CountViewDto, ip: string) {
        try {
            const utmCampaign = utm_campaign ? utm_campaign : ''
            const utmContent = utm_content ? utm_content : ''
            const utmMedium = utm_medium ? utm_medium : ''
            const utmSource = utm_source ? utm_source : ''

            // lay timezone va utm ( neu ko co utm => create utm => luu len redis | neu ko co timezone => vo db lay => luu len redis)
            const timezone = await this.getTimezoneRedis(adminId)
            const utm = await this.findUtmByUtmInfo({ utmCampaign, utmContent, utmMedium, utmSource })
            //Save utm and storeId in redis:
            await this.checkTraffic(storeId, utm._id.toString())

            // tao key: storeId-sessionId-utmId
            const keySession = `${storeId}-${ip}_${sessionId}-${utm._id.toString()}`
            // tim key tren redis: neu co => return | ko co: +1 view, luu key voi expiresIn: cuoi ngay cua timezone
            const foundKey = await this.cacheService.get({ key: keySession })
            if (foundKey) return
            // +1 view: key view: `daymonthyear_${date}_${month}_${year}-storeId_${storeId.toString()}-type_${ViewType.view}_${utmId}`
            const nowTz = momentTz.tz(timezone)
            const endDay = nowTz.endOf('days').unix()
            const now = moment().unix()
            //luu key:
            await this.cacheService.set({ key: keySession, value: 1, expiresIn: (endDay - now) })
            //cong view:
            const date = nowTz.date()
            const month = nowTz.month() + 1
            const year = nowTz.year()
            const view = await this.cacheService.get({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId.toString()}-type_${ViewType.view}_utm_${utm._id.toString()}` })
            const countView = view ? Number(view) + 1 : 1
            await this.cacheService.set({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId.toString()}-type_${ViewType.view}_utm_${utm._id.toString()}`, value: countView, expiresIn: 10 * 60 * 2 })

        } catch (error) {
            throw error
        }
    }

    async countAddToCart(adminId: string, storeId: string, count: number, { utm_campaign, utm_content, utm_medium, utm_source, sessionId }: ViewUtmQuery, ip: string) {
        try {
            const utmCampaign = utm_campaign ? utm_campaign : ''
            const utmContent = utm_content ? utm_content : ''
            const utmMedium = utm_medium ? utm_medium : ''
            const utmSource = utm_source ? utm_source : ''
            let [timezoneStr, utm] = await Promise.all([
                this.getTimezoneRedis(adminId),
                this.findUtmByUtmInfo({ utmCampaign, utmContent, utmMedium, utmSource })
            ])
            const keySession = `addtocart-${storeId}-${ip}_${sessionId}-${utm._id.toString()}`
            const foundKey = await this.cacheService.get({ key: keySession })
            if (foundKey) return
            const nowTz = momentTz.tz(timezoneStr)
            const endDay = nowTz.endOf('days').unix()
            const now = moment().unix()
            //luu key:
            await this.cacheService.set({ key: keySession, value: 1, expiresIn: (endDay - now) })
            await this.checkTraffic(storeId, utm._id.toString())
            const date = nowTz.date()
            const month = nowTz.month() + 1
            const year = nowTz.year()
            const view = await this.cacheService.get({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId.toString()}-type_${ViewType.addToCart}_utm_${utm._id.toString()}` })
            const countView = view ? Number(view) + count : count
            await this.cacheService.set({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId.toString()}-type_${ViewType.addToCart}_utm_${utm._id.toString()}`, value: countView, expiresIn: 10 * 60 * 2 })
        } catch (error) {
            throw error
        }
    }

    async countCheckout(adminId: string, storeId: string, count: number, utmId: string, sessionId: string, ip: string) {
        try {
            let timezoneStr = await this.getTimezoneRedis(adminId)
            const keySession = `checkout-${storeId}-${ip}_${sessionId}-${utmId}`
            const foundKey = await this.cacheService.get({ key: keySession })
            if (foundKey) return
            const nowTz = momentTz.tz(timezoneStr)
            const endDay = nowTz.endOf('days').unix()
            const now = moment().unix()
            //luu key:
            await this.cacheService.set({ key: keySession, value: 1, expiresIn: (endDay - now) })
            const date = nowTz.date()
            const month = nowTz.month() + 1
            const year = nowTz.year()
            await this.checkTraffic(storeId, utmId.toString())
            const view = await this.cacheService.get({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId.toString()}-type_${ViewType.checkout}_utm_${utmId.toString()}` })
            const countView = view ? Number(view) + count : count
            await this.cacheService.set({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId.toString()}-type_${ViewType.checkout}_utm_${utmId.toString()}`, value: countView, expiresIn: 10 * 60 * 2 })
        } catch (error) {
            throw error
        }
    }

    async countGoCheckout(adminId: string, storeId: string, count: number, { utm_campaign, utm_content, utm_medium, utm_source, sessionId }: ViewUtmQuery, ip: string) {
        try {
            const utmCampaign = utm_campaign ? utm_campaign : ''
            const utmContent = utm_content ? utm_content : ''
            const utmMedium = utm_medium ? utm_medium : ''
            const utmSource = utm_source ? utm_source : ''
            let [timezoneStr, utm] = await Promise.all([
                this.getTimezoneRedis(adminId),
                this.findUtmByUtmInfo({ utmCampaign, utmContent, utmMedium, utmSource })
            ])
            const keySession = `gocheckout-${storeId}-${ip}_${sessionId}-${utm._id.toString()}`
            const foundKey = await this.cacheService.get({ key: keySession })
            if (foundKey) return
            const nowTz = momentTz.tz(timezoneStr)
            const endDay = nowTz.endOf('days').unix()
            const now = moment().unix()
            //luu key:
            await this.cacheService.set({ key: keySession, value: 1, expiresIn: (endDay - now) })
            const date = nowTz.date()
            const month = nowTz.month() + 1
            const year = nowTz.year()
            await this.checkTraffic(storeId, utm._id.toString())
            const view = await this.cacheService.get({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId.toString()}-type_${ViewType.gocheckout}_utm_${utm._id.toString()}` })
            const countView = view ? Number(view) + count : count
            await this.cacheService.set({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId.toString()}-type_${ViewType.gocheckout}_utm_${utm._id.toString()}`, value: countView, expiresIn: 10 * 60 * 2 })
        } catch (error) {
            throw error
        }
    }

    async updateViewFromRedis(adminId: string, sellerId: string, productSellerId: string, storeId: string, utmId: string, productId: string) {
        try {
            const timezoneStr = await this.getTimezoneRedis(adminId)
            const timeNow = momentTz.tz(timezoneStr)
            const { date, month, year } = { date: timeNow.date(), month: timeNow.month() + 1, year: timeNow.year() }
            const [totalViewRedis, totalAddToCartRedis, totalCheckoutRedis, totalGoCheckoutRedis] = await Promise.all([
                this.cacheService.get({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId}-type_${ViewType.view}_utm_${utmId}` }),
                this.cacheService.get({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId}-type_${ViewType.addToCart}_utm_${utmId}` }),
                this.cacheService.get({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId}-type_${ViewType.checkout}_utm_${utmId}` }),
                this.cacheService.get({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId}-type_${ViewType.gocheckout}_utm_${utmId}` })
            ])
            if (totalViewRedis || totalAddToCartRedis || totalCheckoutRedis || totalGoCheckoutRedis) {
                const foundView = await this.viewModel.findOneAndUpdate(
                    { day: date, month, year, storeId, utmId },
                    { $inc: { view: Number(totalViewRedis || 0), addToCart: Number(totalAddToCartRedis || 0), checkout: Number(totalCheckoutRedis || 0), goCheckout: Number(totalGoCheckoutRedis || 0) }, productSellerId, sellerId, productId, adminId })
                if (!foundView) {
                    const newView = new this.viewModel({
                        day: date, month, year, storeId, utmId,
                        view: Number(totalViewRedis || 0), addToCart: Number(totalAddToCartRedis || 0), checkout: Number(totalCheckoutRedis || 0), goCheckout: Number(totalGoCheckoutRedis || 0),
                        productSellerId,
                        adminId,
                        sellerId,
                        productId
                    })
                    await newView.save()
                }
            }
            await Promise.all([
                totalViewRedis ? this.cacheService.set({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId}-type_${ViewType.view}_utm_${utmId}`, value: 0, expiresIn: 10 * 60 }) : "",
                totalAddToCartRedis ? this.cacheService.set({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId}-type_${ViewType.addToCart}_utm_${utmId}`, value: 0, expiresIn: 10 * 60 }) : "",
                totalCheckoutRedis ? this.cacheService.set({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId}-type_${ViewType.checkout}_utm_${utmId}`, value: 0, expiresIn: 10 * 60 }) : "",
                totalGoCheckoutRedis ? this.cacheService.set({ key: `daymonthyear_${date}_${month}_${year}-storeId_${storeId}-type_${ViewType.gocheckout}_utm_${utmId}`, value: 0, expiresIn: 10 * 60 }) : ""
            ])
        } catch (error) {
            throw error
        }
    }

    async masterGetTotalStatsByTime(from, to) {
        try {
            let revenue = 0
            let orders = 0
            let productCost = 0
            let totalView = 0
            let tip = 0
            const info = await this.viewModel.aggregate([
                {
                    $match: { createdAt: { $gte: from, $lte: to } }
                },
                {
                    $group: {
                        _id: 'a',
                        total: { $sum: "$total" },
                        orders: { $sum: "$orders" },
                        productCost: { $sum: "$productCost" },
                        view: { $sum: "$view" },
                        tip: { $sum: "$tip" },
                    }
                }
            ])
            if (info && info.length && info[0] && info[0].total) {
                revenue = info[0].total
            }
            if (info && info.length && info[0] && info[0].orders) {
                orders = info[0].orders
            }
            if (info && info.length && info[0] && info[0].productCost) {
                productCost = info[0].productCost
            }
            if (info && info.length && info[0] && info[0].view) {
                totalView = info[0].view
            }
            if (info && info.length && info[0] && info[0].tip) {
                tip = info[0].tip
            }
            const conversionRate = totalView == 0 ? 0 : orders / totalView * 100
            return {
                revenue: Math.round((revenue - tip) * 100) / 100,
                orders: Math.round(orders * 100) / 100,
                productCost: Math.round(productCost * 100) / 100,
                conversionRate: Math.round(conversionRate * 100) / 100,
                tip: Math.round(tip * 100) / 100,
            }
        } catch (error) {
            throw error
        }
    }

    async analyticUtm({ from, to, timezone, utmCampaign, utmMedium }: SellerUtmQuery, sellerId?: string) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
            let match: any = { createdAt: { $gte: timeFrom, $lte: timeTo } }
            if (sellerId) {
                match.sellerId = sellerId
            }
            if (utmCampaign) {
                const listUtmId = await this.utmService.getListUtmId({
                    utmCampaign: { '$regex': utmCampaign, '$options': 'i' }
                })
                match.utmId = { $in: listUtmId }
            }
            if (utmMedium) {
                const listUtmId = await this.utmService.getListUtmId({
                    utmMedium: { '$regex': utmMedium, '$options': 'i' }
                })
                match.utmId = { $in: listUtmId }
            }
            const listView: any = await this.viewModel.find(match).select('addToCart tip goCheckout checkout quantity total orders view utmId').populate('utmId').lean()
            let resultArray = []
            for (let view of listView) {
                let { addToCart, checkout, tip, total, orders, goCheckout } = view
                if (!tip) tip = 0
                if (!goCheckout) goCheckout = 0
                if (!view.utmId) continue
                const utmId = view.utmId._id.toString()
                const indexKey = resultArray.findIndex(result => result.utmId == utmId)
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        utmId,
                        utm: view.utmId,
                        view: view.view ? resultArray[indexKey].view + view.view : resultArray[indexKey].view,
                        addToCart: addToCart ? resultArray[indexKey].addToCart + addToCart : resultArray[indexKey].addToCart,
                        checkout: checkout ? resultArray[indexKey].checkout + checkout : resultArray[indexKey].checkout,
                        goCheckout: goCheckout ? resultArray[indexKey].goCheckout + goCheckout : resultArray[indexKey].goCheckout,
                        revenue: total ? resultArray[indexKey].revenue + total - tip : resultArray[indexKey].revenue - tip,
                        purchase: orders ? resultArray[indexKey].purchase + orders : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        utmId,
                        utm: view.utmId,
                        view: view.view ? view.view : 0,
                        addToCart: addToCart ? addToCart : 0,
                        purchase: orders ? orders : 0,
                        revenue: total ? total - tip : 0,
                        checkout: checkout ? checkout : 0,
                        goCheckout
                    })
                }
            }
            resultArray = resultArray.sort((a, b) => {
                return b.utm.utmCampaign.length - a.utm.utmCampaign.length
            })
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async analyticRecentAdmin(adminId: string, from, to) {
        try {
            // const listView: any = await this.viewModel.find({ createdAt: { $gte: from, $lte: to }, orders: { $exists: true, $ne: 0 }, adminId }).select('productSellerId  view addToCart checkout storeId total productCost quantity orders tip').lean()
            const listProductSellerId: any = await this.viewModel.distinct('productSellerId', { createdAt: { $gte: from, $lte: to }, orders: { $exists: true, $ne: 0 }, adminId })
            const result = await Promise.map(listProductSellerId, async (productSellerId) => {
                let revenue = 0
                let purchase = 0
                let productCost = 0
                let quantity = 0
                let viewContent = 0
                let tip = 0
                let addToCart = 0
                let checkout = 0
                let goCheckout = 0
                const info = await this.viewModel.aggregate([
                    {
                        $match: { createdAt: { $gte: from, $lte: to }, orders: { $exists: true, $ne: 0 }, productSellerId: new Types.ObjectId(productSellerId) }
                    },
                    {
                        $group: {
                            _id: '$storeId',
                            revenue: { $sum: "$total" },
                            purchase: { $sum: "$orders" },
                            productCost: { $sum: "$productCost" },
                            quantity: { $sum: "$quantity" },
                            goCheckout: { $sum: "$goCheckout" },
                            viewContent: { $sum: "$view" },
                            addToCart: { $sum: "$addToCart" },
                            checkout: { $sum: "$checkout" },
                            tip: { $sum: "$tip" },
                        },

                    }
                ])
                if (info && info.length && info[0] && info[0].revenue) {
                    revenue = info[0].revenue
                }
                if (info && info.length && info[0] && info[0].purchase) {
                    purchase = info[0].purchase
                }
                if (info && info.length && info[0] && info[0].productCost) {
                    productCost = info[0].productCost
                }
                if (info && info.length && info[0] && info[0].quantity) {
                    quantity = info[0].quantity
                }
                if (info && info.length && info[0] && info[0].viewContent) {
                    viewContent = info[0].viewContent
                }
                if (info && info.length && info[0] && info[0].tip) {
                    tip = info[0].tip
                }
                if (info && info.length && info[0] && info[0].addToCart) {
                    addToCart = info[0].addToCart
                }
                if (info && info.length && info[0] && info[0].checkout) {
                    checkout = info[0].checkout
                }
                if (info && info.length && info[0] && info[0].goCheckout) {
                    goCheckout = info[0].goCheckout
                }
                revenue = revenue - tip
                let storeId = info && info.length && info[0] && info[0]._id || ""
                return {
                    storeId,
                    productSellerId,
                    viewContent,
                    revenue,
                    productCost,
                    tip,
                    addToCart,
                    checkout,
                    goCheckout,
                    quantity,
                    purchase,
                }
            }, { concurrency: 25 })
            return result

        } catch (error) {
            throw error
        }
    }

    async analyticDomain(sellerId: string, { from, to }) {
        try {
            const listView: any = await this.viewModel.find({ sellerId, createdAt: { $gte: from, $lte: to } }).select('view tip addToCart goCheckout checkout total productCost quantity orders productSellerId storeId').lean()
            let resultArray = []
            for (let view of listView) {
                let { addToCart, checkout, storeId, total, productCost, quantity, orders, tip, goCheckout } = view
                if (!total) total = 0
                if (!productCost) productCost = 0
                if (!tip) tip = 0
                if (!goCheckout) goCheckout = 0
                if (!view.view && !addToCart && !checkout && !total && !productCost && !quantity || !view.productSellerId) continue
                const productSellerId = view.productSellerId.toString()
                const indexKey = resultArray.findIndex(result => result.productSellerId == productSellerId);
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        storeId,
                        productSellerId,
                        viewContent: view.view ? resultArray[indexKey].viewContent + view.view : resultArray[indexKey].viewContent,
                        addToCart: addToCart ? resultArray[indexKey].addToCart + addToCart : resultArray[indexKey].addToCart,
                        goCheckout: goCheckout ? resultArray[indexKey].goCheckout + goCheckout : resultArray[indexKey].goCheckout,
                        initiateCheckout: checkout ? resultArray[indexKey].initiateCheckout + checkout : resultArray[indexKey].initiateCheckout,
                        revenue: total ? resultArray[indexKey].revenue + total - tip : resultArray[indexKey].revenue - tip,
                        productCost: productCost ? resultArray[indexKey].productCost + productCost : resultArray[indexKey].productCost,
                        quantity: quantity ? resultArray[indexKey].quantity + quantity : resultArray[indexKey].quantity,
                        purchase: orders ? resultArray[indexKey].purchase + orders : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        storeId,
                        productSellerId,
                        viewContent: view.view ? view.view : 0,
                        addToCart: addToCart ? addToCart : 0,
                        initiateCheckout: checkout ? checkout : 0,
                        goCheckout,
                        revenue: total - tip,
                        productCost: productCost,
                        quantity: quantity | 0,
                        purchase: orders | 0,
                    })
                }
            }
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async sellerAnalyticRecent(sellerId: string, { from, to }) {
        try {
            const listView: any = await this.viewModel.find({ sellerId, createdAt: { $gte: from, $lte: to } }).select('view tip addToCart goCheckout checkout total productCost quantity orders productSellerId storeId tip').lean()
            let resultArray = []
            for (let view of listView) {
                let { addToCart, checkout, storeId, total, productCost, quantity, orders, tip, goCheckout } = view
                if (!view.view && !addToCart && !checkout && !total && !productCost && !quantity || !view.productSellerId) continue
                if (!total) total = 0
                if (!productCost) productCost = 0
                if (!tip) tip = 0
                if (!goCheckout) goCheckout = 0

                const productSellerId = view.productSellerId.toString()
                const indexKey = resultArray.findIndex(result => result.productSellerId == productSellerId);
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        storeId,
                        productSellerId,
                        viewContent: view.view ? resultArray[indexKey].viewContent + view.view : resultArray[indexKey].viewContent,
                        addToCart: addToCart ? resultArray[indexKey].addToCart + addToCart : resultArray[indexKey].addToCart,
                        goCheckout: goCheckout ? resultArray[indexKey].goCheckout + goCheckout : resultArray[indexKey].goCheckout,
                        initiateCheckout: checkout ? resultArray[indexKey].initiateCheckout + checkout : resultArray[indexKey].initiateCheckout,
                        revenue: total ? resultArray[indexKey].revenue + total - tip : resultArray[indexKey].revenue - tip,
                        productCost: productCost ? resultArray[indexKey].productCost + productCost : resultArray[indexKey].productCost,
                        quantity: quantity ? resultArray[indexKey].quantity + quantity : resultArray[indexKey].quantity,
                        purchase: orders ? resultArray[indexKey].purchase + orders : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        storeId,
                        productSellerId,
                        viewContent: view.view ? view.view : 0,
                        addToCart: addToCart ? addToCart : 0,
                        initiateCheckout: checkout ? checkout : 0,
                        goCheckout,
                        revenue: total - tip,
                        productCost: productCost,
                        quantity: quantity | 0,
                        purchase: orders | 0,
                    })
                }
            }
            resultArray = resultArray.filter(result => result.purchase)
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async anlyticByTime(from, to) {
        try {
            const info = await this.viewModel.aggregate(
                [
                    {
                        $match: {
                            'createdAt': {
                                '$gte': from,
                                '$lte': to
                            }
                        }
                    },
                    {
                        $group:
                        {
                            _id: 'a',
                            revenue: { $sum: "$total" },
                            productCost: { $sum: "$productCost" },
                            purchase: { $sum: "$orders" },
                            viewContent: { $sum: "$view" },
                            addToCart: { $sum: "$addToCart" },
                            initiateCheckout: { $sum: "$checkout" },
                        },
                    }
                ]
            )
            const productCost = info && info.length && info[0] && info[0].productCost ? info[0].productCost : 0
            const revenue = info && info.length && info[0] && info[0].revenue ? info[0].revenue : 0
            const purchase = info && info.length && info[0] && info[0].purchase ? info[0].purchase : 0
            const viewContent = info && info.length && info[0] && info[0].viewContent ? info[0].viewContent : 0
            const addToCart = info && info.length && info[0] && info[0].addToCart ? info[0].addToCart : 0
            const initiateCheckout = info && info.length && info[0] && info[0].initiateCheckout ? info[0].initiateCheckout : 0
            const conversionRate = viewContent == 0 ? 0 : purchase / viewContent * 100
            return { productCost, revenue, purchase, viewContent, addToCart, initiateCheckout, conversionRate }
        } catch (error) {
            throw error
        }
    }

    async adminGetTotalStatsByTime(adminId: string, from, to) {
        try {
            let revenue = 0
            let orders = 0
            let productCost = 0
            let itemSold = 0
            let totalView = 0
            let totalTip = 0
            let goCheckout = 0

            const info = await this.viewModel.aggregate([
                {
                    $match: { createdAt: { $gte: from, $lte: to }, adminId: new Types.ObjectId(adminId) }
                },
                {
                    $group: {
                        _id: 'a',
                        total: { $sum: "$total" },
                        orders: { $sum: "$orders" },
                        goCheckout: { $sum: "$goCheckout" },
                        productCost: { $sum: "$productCost" },
                        quantity: { $sum: "$quantity" },
                        view: { $sum: "$view" },
                        tip: { $sum: "$tip" },
                    }
                }
            ])
            if (info && info.length && info[0] && info[0].total) {
                revenue = info[0].total
            }
            if (info && info.length && info[0] && info[0].orders) {
                orders = info[0].orders
            }
            if (info && info.length && info[0] && info[0].productCost) {
                productCost = info[0].productCost
            }
            if (info && info.length && info[0] && info[0].quantity) {
                itemSold = info[0].quantity
            }
            if (info && info.length && info[0] && info[0].view) {
                totalView = info[0].view
            }
            if (info && info.length && info[0] && info[0].tip) {
                totalTip = info[0].tip
            }
            if (info && info.length && info[0] && info[0].goCheckout) {
                goCheckout = info[0].goCheckout
            }
            const conversionRate = totalView == 0 ? 0 : orders / totalView * 100
            const aov = orders == 0 ? 0 : (revenue / orders)
            return {
                revenue: Math.round((revenue - totalTip) * 100) / 100,
                orders: Math.round(orders * 100) / 100,
                tip: totalTip,
                productCost: Math.round(productCost * 100) / 100,
                itemSold: Math.round(itemSold * 100) / 100,
                conversionRate: Math.round(conversionRate * 100) / 100,
                aov: Math.round(aov * 100) / 100,
                viewContent: totalView,
                goCheckout
            }
        } catch (error) {
            throw error
        }
    }

    async sellerGetTotalStatsByTime(sellerId: string, from, to, storeId?: string | Types.ObjectId) {
        try {
            const condition = storeId ? { createdAt: { $gte: from, $lte: to }, sellerId, storeId } : { createdAt: { $gte: from, $lte: to }, sellerId }
            const listView = await this.viewModel.find(condition)
            let revenue = 0
            let orders = 0
            let productCost = 0
            let itemSold = 0
            let totalView = 0
            let totalTip = 0
            for (let view of listView) {
                const { quantity, total, tip } = view
                revenue += (total || 0)
                orders += (view.orders || 0)
                productCost += (view.productCost || 0)
                itemSold += (quantity || 0)
                totalView += (view.view || 0)
                totalTip += (tip || 0)
            }
            const conversionRate = totalView == 0 ? 0 : orders / totalView * 100
            const aov = orders == 0 ? 0 : (revenue / orders)
            return {
                revenue: Math.round((revenue - totalTip) * 100) / 100,
                orders: Math.round(orders * 100) / 100,
                productCost: Math.round(productCost * 100) / 100,
                itemSold: Math.round(itemSold * 100) / 100,
                conversionRate: Math.round(conversionRate * 100) / 100,
                aov: Math.round(aov * 100) / 100,
            }
        } catch (error) {
            throw error
        }
    }

    async sellerRevenueProduct(from, to, sellerId: string) {
        try {
            const condition = { createdAt: { $gte: from, $lte: to }, sellerId }
            const listView: any = await this.viewModel.find(condition).select('view addToCart goCheckout checkout total tip totalCancel productCost quantity orders productId storeId').lean()
            let resultArray = []
            for (let view of listView) {
                let { total, productCost, quantity, orders, storeId, totalCancel, addToCart, checkout, tip, goCheckout } = view
                if (!view.view && !total && !productCost && !quantity || !view.productId) continue
                const productId = view.productId.toString()
                if (!totalCancel) totalCancel = 0
                if (!total) total = 0
                if (!productCost) productCost = 0
                if (!tip) tip = 0
                if (!tip) tip = 0
                const indexKey = resultArray.findIndex(result => result.productId == productId);
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        productId,
                        storeId,
                        revenue: total ? resultArray[indexKey].revenue + total - totalCancel - tip : resultArray[indexKey].revenue - totalCancel - tip,
                        revenueCancel: totalCancel ? resultArray[indexKey].revenueCancel + tip + totalCancel : resultArray[indexKey].revenueCancel,
                        view: view.view ? resultArray[indexKey].view + view.view : resultArray[indexKey].view,
                        productCost: productCost ? resultArray[indexKey].productCost + productCost : resultArray[indexKey].productCost,
                        addToCart: addToCart ? resultArray[indexKey].addToCart + addToCart : resultArray[indexKey].addToCart,
                        checkout: checkout ? resultArray[indexKey].checkout + checkout : resultArray[indexKey].checkout,
                        goCheckout: goCheckout ? resultArray[indexKey].goCheckout + goCheckout : resultArray[indexKey].goCheckout,
                        quantity: quantity ? resultArray[indexKey].quantity + quantity : resultArray[indexKey].quantity,
                        purchase: orders ? resultArray[indexKey].purchase + orders : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        productId,
                        storeId,
                        revenue: (total - totalCancel - tip),
                        revenueCancel: totalCancel,
                        productCost: productCost,
                        view: view.view | 0,
                        addToCart: addToCart,
                        checkout: checkout,
                        goCheckout,
                        quantity: quantity | 0,
                        purchase: orders | 0,
                    })
                }
            }
            resultArray = resultArray.filter(product => product.purchase)
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async sellerRevenueDomain(sellerId: string, { from, to }) {
        try {
            const listView: any = await this.viewModel.find({ sellerId, createdAt: { $gte: from, $lte: to } }).select('view total tip addToCart goCheckout checkout totalCancel productCost quantity orders storeId listImageId').lean()
            let resultArray = []
            for (let view of listView) {
                let { total, productCost, quantity, orders, totalCancel, addToCart, checkout, goCheckout, tip } = view
                if (!totalCancel) totalCancel = 0
                if (!total) total = 0
                if (!productCost) productCost = 0
                if (!tip) tip = 0
                if (!goCheckout) goCheckout = 0
                if (!total && !productCost && !quantity || !view.storeId) continue
                const storeId = view.storeId.toString()
                const indexKey = resultArray.findIndex(result => result.storeId == storeId);
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        storeId,
                        revenue: total ? resultArray[indexKey].revenue + total - totalCancel - tip : resultArray[indexKey].revenue - totalCancel - tip,
                        revenueCancel: totalCancel ? resultArray[indexKey].revenueCancel + totalCancel : resultArray[indexKey].revenueCancel,
                        productCost: productCost ? resultArray[indexKey].productCost + productCost : resultArray[indexKey].productCost,
                        view: view.view ? resultArray[indexKey].view + view.view : resultArray[indexKey].view,
                        addToCart: addToCart ? resultArray[indexKey].addToCart + addToCart : resultArray[indexKey].addToCart,
                        checkout: checkout ? resultArray[indexKey].checkout + checkout : resultArray[indexKey].checkout,
                        goCheckout: goCheckout ? resultArray[indexKey].goCheckout + goCheckout : resultArray[indexKey].goCheckout,
                        quantity: quantity ? resultArray[indexKey].quantity + quantity : resultArray[indexKey].quantity,
                        purchase: orders ? resultArray[indexKey].purchase + orders : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        storeId,
                        revenue: (total - totalCancel - tip),
                        revenueCancel: totalCancel,
                        view: view.view | 0,
                        productCost: productCost,
                        addToCart: addToCart,
                        checkout: checkout,
                        goCheckout,
                        quantity: quantity | 0,
                        purchase: orders | 0,
                    })
                }
            }
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async adminRevenueDomain(adminId: string, { from, to }) {
        try {
            const listView: any = await this.viewModel.find({ adminId, createdAt: { $gte: from, $lte: to } }).select('tip view addToCart goCheckout checkout total productCost quantity orders storeId totalCancel').lean()
            let resultArray = []
            for (let view of listView) {
                let { total, productCost, quantity, orders, totalCancel, checkout, addToCart, tip, goCheckout } = view
                if (!totalCancel) totalCancel = 0
                if (!total && !productCost && !quantity || !view.storeId) continue
                if (!total) total = 0
                if (!totalCancel) totalCancel = 0
                if (!productCost) productCost = 0
                if (!checkout) checkout = 0
                if (!tip) tip = 0
                if (!goCheckout) goCheckout = 0
                if (!addToCart) addToCart = 0
                const storeId = view.storeId.toString()
                const indexKey = resultArray.findIndex(result => result.storeId == storeId);
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        storeId,
                        revenue: total ? resultArray[indexKey].revenue + total - totalCancel - tip : resultArray[indexKey].revenue - totalCancel - tip,
                        revenueCancel: totalCancel ? resultArray[indexKey].revenueCancel + totalCancel : resultArray[indexKey].revenueCancel,
                        productCost: productCost ? resultArray[indexKey].productCost + productCost : resultArray[indexKey].productCost,
                        addToCart: addToCart ? resultArray[indexKey].addToCart + addToCart : resultArray[indexKey].addToCart,
                        checkout: checkout ? resultArray[indexKey].checkout + checkout : resultArray[indexKey].checkout,
                        goCheckout: goCheckout ? resultArray[indexKey].goCheckout + goCheckout : resultArray[indexKey].goCheckout,
                        viewContent: view.view ? resultArray[indexKey].viewContent + view.view : resultArray[indexKey].viewContent,
                        quantity: quantity ? resultArray[indexKey].quantity + quantity : resultArray[indexKey].quantity,
                        tip: tip ? resultArray[indexKey].tip + tip : resultArray[indexKey].tip,
                        purchase: orders ? resultArray[indexKey].purchase + orders : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        storeId,
                        revenue: (total - totalCancel - tip),
                        revenueCancel: totalCancel,
                        productCost: productCost,
                        tip,
                        addToCart: addToCart,
                        checkout: checkout,
                        goCheckout,
                        quantity: quantity | 0,
                        purchase: orders | 0,
                        viewContent: view.view | 0,
                    })
                }
            }
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async adminGetAbandonDomain(adminId: string, { from, to }) {
        try {
            const listView: any = await this.viewModel.find({ adminId, createdAt: { $gte: from, $lte: to } }).select('totalAbandon productCostAbandon quantityAbandon ordersAbandon tipAbandon storeId totalCancel storeId').lean()
            let resultArray = []
            for (let view of listView) {
                let { totalAbandon, productCostAbandon, quantityAbandon, ordersAbandon, tipAbandon } = view
                if (!totalAbandon || !view.storeId) continue
                if (!totalAbandon) totalAbandon = 0
                if (!productCostAbandon) productCostAbandon = 0
                if (!tipAbandon) tipAbandon = 0
                const storeId = view.storeId.toString()
                const indexKey = resultArray.findIndex(result => result.storeId == storeId);
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        storeId,
                        revenue: totalAbandon ? resultArray[indexKey].revenue + totalAbandon - tipAbandon : resultArray[indexKey].revenue - tipAbandon,
                        productCost: productCostAbandon ? resultArray[indexKey].productCost + productCostAbandon : resultArray[indexKey].productCost,
                        quantity: quantityAbandon ? resultArray[indexKey].quantity + quantityAbandon : resultArray[indexKey].quantity,
                        tip: tipAbandon ? resultArray[indexKey].tip + tipAbandon : resultArray[indexKey].tip,
                        purchase: ordersAbandon ? resultArray[indexKey].purchase + ordersAbandon : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        storeId,
                        revenue: totalAbandon - tipAbandon,
                        productCost: productCostAbandon,
                        tip: tipAbandon,
                        quantity: quantityAbandon | 0,
                        purchase: ordersAbandon | 0,
                    })
                }
            }
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async adminRevenueSeller(adminId: string, { from, to }) {
        try {
            const listView: any = await this.viewModel.find({ adminId, createdAt: { $gte: from, $lte: to } }).select('tip view addToCart goCheckout checkout total productCost quantity orders sellerId totalCancel').lean()
            let resultArray = []
            for (let view of listView) {
                let { total, productCost, quantity, orders, totalCancel, addToCart, checkout, tip, goCheckout } = view
                if (!totalCancel) totalCancel = 0
                if (!total) total = 0
                if (!tip) tip = 0
                if (!productCost) productCost = 0
                if (!addToCart) addToCart = 0
                if (!checkout) checkout = 0
                if (!goCheckout) goCheckout = 0
                if (!total && !productCost && !quantity || !view.sellerId) continue
                const sellerId = view.sellerId.toString()
                const indexKey = resultArray.findIndex(result => result.sellerId == sellerId);
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        sellerId,
                        revenue: total ? resultArray[indexKey].revenue + total - totalCancel - tip : resultArray[indexKey].revenue - totalCancel - tip,
                        revenueCancel: totalCancel ? resultArray[indexKey].revenueCancel + totalCancel : resultArray[indexKey].revenueCancel,
                        productCost: productCost ? resultArray[indexKey].productCost + productCost : resultArray[indexKey].productCost,
                        addToCart: addToCart ? resultArray[indexKey].addToCart + addToCart : resultArray[indexKey].addToCart,
                        checkout: checkout ? resultArray[indexKey].checkout + checkout : resultArray[indexKey].checkout,
                        goCheckout: goCheckout ? resultArray[indexKey].goCheckout + goCheckout : resultArray[indexKey].goCheckout,
                        viewContent: view.view ? resultArray[indexKey].viewContent + view.view : resultArray[indexKey].viewContent,
                        quantity: quantity ? resultArray[indexKey].quantity + quantity : resultArray[indexKey].quantity,
                        tip: tip ? resultArray[indexKey].tip + tip : resultArray[indexKey].tip,
                        purchase: orders ? resultArray[indexKey].purchase + orders : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        sellerId,
                        revenue: (total - totalCancel - tip),
                        tip,
                        revenueCancel: totalCancel,
                        productCost: productCost,
                        addToCart: addToCart,
                        checkout: checkout,
                        goCheckout,
                        quantity: quantity | 0,
                        purchase: orders | 0,
                        viewContent: view.view | 0,
                    })
                }
            }
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async adminGetAbandonSeller(adminId: string, { from, to }) {
        try {
            const listView: any = await this.viewModel.find({ adminId, createdAt: { $gte: from, $lte: to } }).select('totalAbandon productCostAbandon quantityAbandon ordersAbandon tipAbandon sellerId').lean()
            let resultArray = []
            for (let view of listView) {
                let { totalAbandon, productCostAbandon, quantityAbandon, ordersAbandon, tipAbandon } = view
                if (!totalAbandon || !view.sellerId) continue
                if (!totalAbandon) totalAbandon = 0
                if (!tipAbandon) tipAbandon = 0
                if (!productCostAbandon) productCostAbandon = 0
                const sellerId = view.sellerId.toString()
                const indexKey = resultArray.findIndex(result => result.sellerId == sellerId);
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        sellerId,
                        revenue: totalAbandon ? resultArray[indexKey].revenue + totalAbandon - tipAbandon : resultArray[indexKey].revenue - tipAbandon,
                        productCost: productCostAbandon ? resultArray[indexKey].productCost + productCostAbandon : resultArray[indexKey].productCost,
                        quantity: quantityAbandon ? resultArray[indexKey].quantity + quantityAbandon : resultArray[indexKey].quantity,
                        tip: tipAbandon ? resultArray[indexKey].tip + tipAbandon : resultArray[indexKey].tip,
                        purchase: ordersAbandon ? resultArray[indexKey].purchase + ordersAbandon : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        sellerId,
                        revenue: totalAbandon - tipAbandon,
                        tip: tipAbandon,
                        productCost: productCostAbandon,
                        quantity: quantityAbandon | 0,
                        purchase: ordersAbandon | 0,
                    })
                }
            }
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async sellerGetRevenueSeller(sellerId: string, from?, to?) {
        try {
            const match: { createdAt?: object, sellerId: Types.ObjectId } = {
                sellerId: new Types.ObjectId(sellerId)
            }
            if (from) {
                match.createdAt = { $gte: from }
            }
            if (to) {
                match.createdAt = { ...match.createdAt, $lte: to }
            }
            const info = await this.viewModel.aggregate(
                [
                    {
                        $match: match
                    },
                    {
                        $group:
                        {
                            _id: 'a',
                            total: { $sum: "$total" },
                            view: { $sum: "$view" },
                            goCheckout: { $sum: "$goCheckout" },
                            tip: { $sum: "$tip" },
                            totalCancel: { $sum: "$totalCancel" },
                            productCost: { $sum: "$productCost" },
                            addToCart: { $sum: "$addToCart" },
                            checkout: { $sum: "$checkout" },
                            orders: { $sum: "$orders" },
                        },
                    }
                ]
            )
            const total = info && info.length && info[0] && info[0].total ? info[0].total : 0
            const view = info && info.length && info[0] && info[0].view ? info[0].view : 0
            const tip = info && info.length && info[0] && info[0].tip ? info[0].tip : 0
            const totalCancel = info && info.length && info[0] && info[0].totalCancel ? info[0].totalCancel : 0
            const purchase = info && info.length && info[0] && info[0].orders ? info[0].orders : 0
            const productCost = info && info.length && info[0] && info[0].productCost ? info[0].productCost : 0
            const addToCart = info && info.length && info[0] && info[0].addToCart ? info[0].addToCart : 0
            const checkout = info && info.length && info[0] && info[0].checkout ? info[0].checkout : 0
            const goCheckout = info && info.length && info[0] && info[0].goCheckout ? info[0].goCheckout : 0
            return { total: total - tip, purchase, productCost, totalCancel, addToCart, checkout, view, goCheckout }
        } catch (error) {
            throw error
        }
    }


    async adminRevenueProduct(adminId: string, from, to) {
        try {
            const condition = { adminId, createdAt: { $gte: from, $lte: to } }
            const listView: any = await this.viewModel.find(condition).select('view tip addToCart goCheckout checkout total totalCancel productCost quantity orders productId').lean()
            let resultArray = []
            for (let view of listView) {
                let { total, productCost, quantity, orders, totalCancel, addToCart, checkout, tip, goCheckout } = view
                if (!totalCancel) totalCancel = 0
                if (!total) total = 0
                if (!tip) tip = 0
                if (!productCost) productCost = 0
                if (!addToCart) addToCart = 0
                if (!checkout) checkout = 0
                if (!goCheckout) goCheckout = 0
                if (!total && !productCost && !quantity || !view.productId) continue
                const productId = view.productId.toString()
                const indexKey = resultArray.findIndex(result => result.productId == productId);
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        productId,
                        revenue: total ? resultArray[indexKey].revenue + total - totalCancel - tip : resultArray[indexKey].revenue - totalCancel - tip,
                        revenueCancel: totalCancel ? resultArray[indexKey].revenueCancel + totalCancel : resultArray[indexKey].revenueCancel,
                        productCost: productCost ? resultArray[indexKey].productCost + productCost : resultArray[indexKey].productCost,
                        addToCart: addToCart ? resultArray[indexKey].addToCart + addToCart : resultArray[indexKey].addToCart,
                        checkout: checkout ? resultArray[indexKey].checkout + checkout : resultArray[indexKey].checkout,
                        goCheckout: goCheckout ? resultArray[indexKey].goCheckout + goCheckout : resultArray[indexKey].goCheckout,
                        viewContent: view.view ? resultArray[indexKey].viewContent + view.view : resultArray[indexKey].viewContent,
                        quantity: quantity ? resultArray[indexKey].quantity + quantity : resultArray[indexKey].quantity,
                        tip: tip ? resultArray[indexKey].tip + tip : resultArray[indexKey].tip,
                        purchase: orders ? resultArray[indexKey].purchase + orders : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        productId,
                        revenue: (total - totalCancel - tip),
                        revenueCancel: totalCancel,
                        productCost: productCost,
                        addToCart: addToCart,
                        checkout: checkout,
                        goCheckout,
                        tip,
                        quantity: quantity | 0,
                        purchase: orders | 0,
                        viewContent: view.view | 0,
                    })
                }
            }
            resultArray = resultArray.filter(product => product.purchase)
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async adminGetAbandonProduct(adminId: string, from, to) {
        try {
            const condition = { adminId, createdAt: { $gte: from, $lte: to } }
            const listView: any = await this.viewModel.find(condition).select('totalAbandon productCostAbandon quantityAbandon ordersAbandon tipAbandon productId').lean()
            let resultArray = []
            for (let view of listView) {
                let { totalAbandon, productCostAbandon, quantityAbandon, ordersAbandon, tipAbandon } = view
                if (!totalAbandon) totalAbandon = 0
                if (!tipAbandon) tipAbandon = 0
                if (!productCostAbandon) productCostAbandon = 0
                const productId = view.productId.toString()
                const indexKey = resultArray.findIndex(result => result.productId == productId);
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        productId,
                        revenue: totalAbandon ? resultArray[indexKey].revenue + totalAbandon - tipAbandon : resultArray[indexKey].revenue - tipAbandon,
                        productCost: productCostAbandon ? resultArray[indexKey].productCost + productCostAbandon : resultArray[indexKey].productCost,
                        quantity: quantityAbandon ? resultArray[indexKey].quantity + quantityAbandon : resultArray[indexKey].quantity,
                        tip: tipAbandon ? resultArray[indexKey].tip + tipAbandon : resultArray[indexKey].tip,
                        purchase: ordersAbandon ? resultArray[indexKey].purchase + ordersAbandon : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        productId,
                        revenue: totalAbandon - tipAbandon,
                        productCost: productCostAbandon,
                        tip: tipAbandon,
                        quantity: quantityAbandon | 0,
                        purchase: ordersAbandon | 0,
                    })
                }
            }
            resultArray = resultArray.filter(product => product.purchase)
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async sellerRevenueDate(sellerId: string, { from, to }) {
        try {
            const listView: any = await this.viewModel.find({ sellerId, createdAt: { $gte: from, $lte: to } }).select('view total tip addToCart goCheckout checkout totalCancel productCost quantity orders day month year createdAt').lean()
            let resultArray = []
            const arrayTime = this.calculateArrayTime(from, to)
            for (let view of listView) {
                let { total, productCost, quantity, orders, day, month, year, totalCancel, createdAt, addToCart, checkout, goCheckout, tip } = view
                if (!totalCancel) totalCancel = 0
                if (!total) total = 0
                if (!productCost) productCost = 0
                if (!tip) tip = 0
                if (!goCheckout) goCheckout = 0
                if (!total && !productCost && !quantity || !day) continue
                const date = `${year}-${month}-${day}`
                const indexKey = resultArray.findIndex(result => result.date == date);
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        date,
                        createdAt,
                        revenue: total ? resultArray[indexKey].revenue + total - totalCancel - tip : resultArray[indexKey].revenue - totalCancel - tip,
                        revenueCancel: totalCancel ? resultArray[indexKey].revenueCancel + totalCancel : resultArray[indexKey].revenueCancel,
                        productCost: productCost ? resultArray[indexKey].productCost + productCost : resultArray[indexKey].productCost,
                        view: view.view ? resultArray[indexKey].view + view.view : resultArray[indexKey].view,
                        addToCart: addToCart ? resultArray[indexKey].addToCart + addToCart : resultArray[indexKey].addToCart,
                        checkout: checkout ? resultArray[indexKey].checkout + checkout : resultArray[indexKey].checkout,
                        goCheckout: goCheckout ? resultArray[indexKey].goCheckout + goCheckout : resultArray[indexKey].goCheckout,
                        quantity: quantity ? resultArray[indexKey].quantity + quantity : resultArray[indexKey].quantity,
                        purchase: orders ? resultArray[indexKey].purchase + orders : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        date,
                        createdAt,
                        revenue: (total - totalCancel - tip),
                        revenueCancel: totalCancel,
                        view: view.view,
                        productCost: productCost,
                        addToCart: addToCart,
                        checkout: checkout,
                        goCheckout,
                        quantity: quantity | 0,
                        purchase: orders | 0,
                    })
                }
            }
            for (let time of arrayTime) {
                const { year, month, day, from } = time
                const date = `${year}-${month}-${day}`
                if (!resultArray.map(result => result.date).includes(date)) {
                    resultArray.push({
                        date,
                        createdAt: from,
                        revenue: 0,
                        revenueCancel: 0,
                        productCost: 0,
                        quantity: 0,
                        purchase: 0,
                        addToCart: 0,
                        checkout: 0,
                        goCheckout: 0
                    })
                }
            }
            resultArray = resultArray.sort((a, b) => { return a.createdAt - b.createdAt })
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async adminRevenueDate(adminId: string, { from, to }: { from: Date, to: Date }) {
        try {
            const listView: any = await this.viewModel.find({ adminId, createdAt: { $gte: from, $lte: to } }).select('view goCheckout tip addToCart checkout total totalCancel productCost quantity orders day month year createdAt').lean()
            let resultArray = []
            const arrayTime = this.calculateArrayTime(from, to)
            for (let view of listView) {
                let { total, tip, productCost, quantity, orders, day, month, year, totalCancel, createdAt, addToCart, checkout, goCheckout } = view
                if (!orders) continue
                if (!totalCancel) totalCancel = 0
                if (!total) total = 0
                if (!productCost) productCost = 0
                if (!tip) tip = 0
                if (!goCheckout) goCheckout = 0
                const group = `${year}-${month}-${day}`
                const indexKey = resultArray.findIndex(result => result.group == group);
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        group,
                        createdAt,
                        revenue: total ? resultArray[indexKey].revenue + total - totalCancel - tip : resultArray[indexKey].revenue - totalCancel - tip,
                        revenueCancel: totalCancel ? resultArray[indexKey].revenueCancel + totalCancel : resultArray[indexKey].revenueCancel,
                        productCost: productCost ? resultArray[indexKey].productCost + productCost : resultArray[indexKey].productCost,
                        addToCart: addToCart ? resultArray[indexKey].addToCart + addToCart : resultArray[indexKey].addToCart,
                        checkout: checkout ? resultArray[indexKey].checkout + checkout : resultArray[indexKey].checkout,
                        goCheckout: goCheckout ? resultArray[indexKey].goCheckout + goCheckout : resultArray[indexKey].goCheckout,
                        viewContent: view.view ? resultArray[indexKey].viewContent + view.view : resultArray[indexKey].viewContent,
                        quantity: quantity ? resultArray[indexKey].quantity + quantity : resultArray[indexKey].quantity,
                        tip: tip ? resultArray[indexKey].tip + tip : resultArray[indexKey].tip,
                        purchase: orders ? resultArray[indexKey].purchase + orders : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        group,
                        createdAt,
                        revenue: (total - totalCancel - tip),
                        revenueCancel: totalCancel,
                        productCost: productCost,
                        tip,
                        addToCart: addToCart | 0,
                        checkout: checkout | 0,
                        goCheckout,
                        quantity: quantity | 0,
                        purchase: orders | 0,
                        viewContent: view.view | 0
                    })
                }
            }
            for (let time of arrayTime) {
                const { year, month, day, from } = time
                const group = `${year}-${month}-${day}`
                if (!resultArray.map(result => result.group).includes(group)) {
                    resultArray.push({
                        group,
                        createdAt: from,
                        revenue: 0,
                        revenueCancel: 0,
                        tip: 0,
                        productCost: 0,
                        addToCart: 0,
                        checkout: 0,
                        goCheckout: 0,
                        quantity: 0,
                        purchase: 0,
                        viewContent: 0
                    })
                }
            }
            resultArray = resultArray.sort((a, b) => { return a.createdAt - b.createdAt })
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async adminRevenueDateAbandoned(adminId: string, { from, to }: { from: Date, to: Date }) {
        try {
            const listView: any = await this.viewModel.find({ adminId, createdAt: { $gte: from, $lte: to } }).select('totalAbandon productCostAbandon quantityAbandon ordersAbandon tipAbandon day month year createdAt').lean()
            let resultArray = []
            const arrayTime = this.calculateArrayTime(from, to)
            for (let view of listView) {
                let { totalAbandon, tipAbandon, productCostAbandon, quantityAbandon, ordersAbandon, day, month, year, totalCancel, createdAt } = view
                if (!ordersAbandon) continue
                if (!totalAbandon) totalAbandon = 0
                if (!productCostAbandon) productCostAbandon = 0
                if (!tipAbandon) tipAbandon = 0
                const group = `${year}-${month}-${day}`
                const indexKey = resultArray.findIndex(result => result.group == group);
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        group,
                        createdAt,
                        revenue: totalAbandon ? resultArray[indexKey].revenue + totalAbandon - tipAbandon : resultArray[indexKey].revenue - tipAbandon,
                        productCost: productCostAbandon ? resultArray[indexKey].productCost + productCostAbandon : resultArray[indexKey].productCost,
                        quantity: quantityAbandon ? resultArray[indexKey].quantity + quantityAbandon : resultArray[indexKey].quantity,
                        tip: tipAbandon ? resultArray[indexKey].tip + tipAbandon : resultArray[indexKey].tip,
                        purchase: ordersAbandon ? resultArray[indexKey].purchase + ordersAbandon : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        group,
                        createdAt,
                        revenue: (totalAbandon - tipAbandon),
                        productCost: productCostAbandon,
                        tip: tipAbandon,
                        quantity: quantityAbandon | 0,
                        purchase: ordersAbandon | 0,
                    })
                }
            }
            for (let time of arrayTime) {
                const { year, month, day, from } = time
                const group = `${year}-${month}-${day}`
                if (!resultArray.map(result => result.group).includes(group)) {
                    resultArray.push({
                        group,
                        createdAt: from,
                        revenue: 0,
                        revenueCancel: 0,
                        tip: 0,
                        productCost: 0,
                        quantity: 0,
                        purchase: 0,
                    })
                }
            }
            resultArray = resultArray.sort((a, b) => { return a.createdAt - b.createdAt })
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async sellerRevenueMonth(sellerId: string, { from, to }) {
        try {
            const listView: any = await this.viewModel.find({ sellerId, createdAt: { $gte: from, $lte: to } }).select('view total tip addToCart goCheckout checkout totalCancel productCost quantity orders day month year createdAt').lean()
            const arrayTime = this.calculateArrayTimeMonth(from, to)
            let resultArray = []
            for (let view of listView) {
                let { total, productCost, quantity, orders, day, year, totalCancel, createdAt, addToCart, checkout, goCheckout, tip } = view
                if (!totalCancel) totalCancel = 0
                if (!total) total = 0
                if (!productCost) productCost = 0
                if (!tip) tip = 0
                if (!goCheckout) goCheckout = 0
                if (!total && !productCost && !quantity || !day) continue
                const month = `${year}-${view.month}`
                const indexKey = resultArray.findIndex(result => result.month == month);
                if (indexKey > -1) {
                    resultArray[indexKey] = {
                        month,
                        createdAt,
                        revenue: total ? resultArray[indexKey].revenue + total - totalCancel - tip : resultArray[indexKey].revenue - totalCancel - tip,
                        revenueCancel: totalCancel ? resultArray[indexKey].revenueCancel + totalCancel : resultArray[indexKey].revenueCancel,
                        productCost: productCost ? resultArray[indexKey].productCost + productCost : resultArray[indexKey].productCost,
                        view: view.view ? resultArray[indexKey].view + view.view : resultArray[indexKey].view,
                        addToCart: addToCart ? resultArray[indexKey].addToCart + addToCart : resultArray[indexKey].addToCart,
                        checkout: checkout ? resultArray[indexKey].checkout + checkout : resultArray[indexKey].checkout,
                        goCheckout: goCheckout ? resultArray[indexKey].goCheckout + goCheckout : resultArray[indexKey].goCheckout,
                        quantity: quantity ? resultArray[indexKey].quantity + quantity : resultArray[indexKey].quantity,
                        purchase: orders ? resultArray[indexKey].purchase + orders : resultArray[indexKey].purchase,
                    }
                } else {
                    resultArray.push({
                        month,
                        createdAt,
                        revenue: (total - totalCancel - tip),
                        revenueCancel: totalCancel,
                        productCost: productCost,
                        view: view.view,
                        addToCart: addToCart,
                        checkout: checkout,
                        quantity: quantity | 0,
                        purchase: orders | 0,
                        goCheckout
                    })
                }
            }
            for (let time of arrayTime) {
                const { year, from } = time
                const month = `${year}-${time.month}`
                if (!resultArray.map(result => result.month).includes(month)) {
                    resultArray.push({
                        month,
                        createdAt: from,
                        revenue: 0,
                        revenueCancel: 0,
                        productCost: 0,
                        quantity: 0,
                        purchase: 0,
                        goCheckout: 0
                    })
                }
            }
            resultArray = resultArray.sort((a, b) => { return a.createdAt - b.createdAt })
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async updateAnalyticOrder(isAbandon: boolean, createdAt: Date, sellerId: string, adminId: string, storeId: string, { total, cost, quantity, tip }: { total: number, cost: number, quantity: number, tip: number }, utmId: string) {
        try {
            let objInc: any = { total, productCost: cost, quantity, orders: 1, tip }
            if (isAbandon) objInc = { ...objInc, totalAbandon: total, productCostAbandon: cost, quantityAbandon: quantity, tipAbandon: tip, ordersAbandon: 1 }
            let timezoneStr = await this.cacheService.get({ key: `${KeyRedisEnum.timezone}-${adminId}` })
            if (!timezoneStr) timezoneStr = 'US/Arizona'
            const nowTz = momentTz(createdAt).tz(timezoneStr)
            const start = nowTz.startOf('days').toDate()
            const end = nowTz.endOf('days').toDate()
            const found = await this.viewModel.findOneAndUpdate({ storeId, utmId, createdAt: { $exists: true, $gte: start, $lte: end } }, { $inc: objInc })
            if (!found) {
                const { date, month, year } = { date: nowTz.date(), month: nowTz.month() + 1, year: nowTz.year() }
                const newView = new this.viewModel({
                    day: date, month, year, storeId, utmId,
                    sellerId,
                    total, productCost: cost, quantity, orders: 1
                })
                await newView.save()
                return newView
            }

        } catch (error) {
            throw error
        }
    }

    async findByIdAndUpdate(id: string, updateField) {
        try {
            await this.viewModel.findByIdAndUpdate(id, updateField)
        } catch (error) {
            throw error
        }
    }

    calculateArrayTime(from: Date, to: Date, timezone?: string) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            let arrayTime = []
            let pavot = from
            while (pavot < to) {
                arrayTime.push({
                    from: momentTz(pavot).tz(timezone).startOf('day').toDate(),
                    // to: momentTz(pavot).tz(timezone).endOf('day').toDate(),
                    day: momentTz(pavot).tz(timezone).date(),
                    month: momentTz(pavot).tz(timezone).month() + 1,
                    year: momentTz(pavot).tz(timezone).year()
                })
                pavot = momentTz(pavot).tz(timezone).add(1, 'days').toDate()
            }
            return arrayTime
        } catch (error) {
            throw error
        }
    }

    calculateArrayTimeMonth(from: Date, to: Date, timezone?: string) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            let arrayTime = []
            let pavot = from
            while (pavot < to) {
                const from = momentTz(pavot).tz(timezone).startOf('month').toDate()
                arrayTime.push({
                    from,
                    to: momentTz(pavot).tz(timezone).endOf('month').toDate(),
                    day: momentTz(pavot).tz(timezone).date(),
                    month: momentTz(pavot).tz(timezone).month() + 1,
                    year: momentTz(pavot).tz(timezone).year()
                })
                pavot = momentTz(from).tz(timezone).add(1, 'months').toDate()
            }
            return arrayTime
        } catch (error) {
            throw error
        }
    }

    async getTimezoneRedis(userId: string) {
        try {
            let timezone = await this.cacheService.get({ key: `${KeyRedisEnum.timezone}-${userId}` })
            if (!timezone) {
                timezone = await this.adminSettingService.getStringTimezone(userId)
                await this.cacheService.set({ key: `${KeyRedisEnum.timezone}-${userId}`, value: timezone, expiresIn: 30 * 60 })
            }
            return timezone
        } catch (error) {
            throw error
        }
    }

    async getListUtm() {
        try {

            let listUtm = await this.cacheService.get({ key: KeyRedisEnum.listUtm })
            if (listUtm) {
                try {
                    return JSON.parse(listUtm)
                } catch (error) {
                    await this.cacheService.remove({ key: KeyRedisEnum.listUtm })
                    return []
                }
            } else {
                if (listUtm == '') return []
                const listFoundUtm = await this.utmService.getListUtm({})
                if (listFoundUtm && listFoundUtm.length) {
                    await this.cacheService.set({ key: KeyRedisEnum.listUtm, value: JSON.stringify(listFoundUtm), expiresIn: 30 * 60 })
                } else {
                    await this.cacheService.set({ key: KeyRedisEnum.listUtm, value: '', expiresIn: 30 * 60 })
                }
                return listFoundUtm
            }
        } catch (error) {
            throw error
        }
    }

    async findUtmByUtmInfo({ utmCampaign, utmContent, utmMedium, utmSource }) {
        try {
            const listUtm = await this.getListUtm()
            const foundUtm = listUtm.find(utm => utm.utmCampaign === utmCampaign && utm.utmContent === utmContent && utm.utmMedium === utmMedium && utm.utmSource === utmSource)
            if (foundUtm) {
                return foundUtm
            } else {
                const newUtm = new this.utmService.utmModel({
                    utmCampaign,
                    utmContent,
                    utmMedium,
                    utmSource
                })
                await this.cacheService.remove({ key: KeyRedisEnum.listUtm })
                await newUtm.save()
                return newUtm
            }
        } catch (error) {
            throw error
        }
    }

    async checkTraffic(storeId: string, utmId: string) {
        try {
            let listUtmParse
            let listStoreTrafficParse
            let [listUtm, listStoreTraffic] = await Promise.all([
                this.cacheService.get({ key: `${KeyRedisEnum.listUtmTraffic}` }),
                this.cacheService.get({ key: `${KeyRedisEnum.listStoreTraffic}` })
            ])
            try {
                listUtmParse = JSON.parse(listUtm)
                listUtmParse.push(utmId)
                listUtmParse = [...new Set(listUtmParse)]
            } catch (error) {
                listUtmParse = [utmId]
            }
            try {
                listStoreTrafficParse = JSON.parse(listStoreTraffic)
                listStoreTrafficParse.push(storeId)
                listStoreTrafficParse = [...new Set(listStoreTrafficParse)]
            } catch (error) {
                listStoreTrafficParse = [storeId]
            }
            listUtmParse && listUtmParse.length && await this.cacheService.set({ key: `${KeyRedisEnum.listUtmTraffic}`, value: JSON.stringify(listUtmParse), expiresIn: 10 * 60 })
            listStoreTrafficParse && listStoreTrafficParse.length && await this.cacheService.set({ key: `${KeyRedisEnum.listStoreTraffic}`, value: JSON.stringify(listStoreTrafficParse), expiresIn: 10 * 60 })
        } catch (error) {
            throw error
        }
    }

    async getListTraffic() {
        try {
            let [listUtm, listStore] = await Promise.all([
                this.cacheService.get({ key: `${KeyRedisEnum.listUtmTraffic}` }),
                this.cacheService.get({ key: `${KeyRedisEnum.listStoreTraffic}` })
            ])
            const listUtmIds = listUtm && JSON.parse(listUtm) && JSON.parse(listUtm).length ? JSON.parse(listUtm) : []
            const listStoreIds = listStore && JSON.parse(listStore) && JSON.parse(listStore).length ? JSON.parse(listStore) : []
            return { listUtmIds, listStoreIds }
        } catch (error) {

        }
    }

    async clearListTraffic() {
        try {
            await this.cacheService.remove({ key: [`${KeyRedisEnum.listUtmTraffic}`, `${KeyRedisEnum.listStoreTraffic}`] })
        } catch (error) {

        }
    }


}
