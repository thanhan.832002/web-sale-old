import { Controller, Get, Put, Post, Param, Body } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { StoreShippingService } from "./store-shipping.service";
import { EditStoreShipingDto } from "./store-shipping.dto";
@ApiTags('Store Shipping')
@Controller('store-shipping')
export class StoreShippingController {
    constructor(
        private readonly storeShippingService: StoreShippingService
    ) { }

    @Get('detail/:storeId')
    async getDetailShipping(
        @Param('storeId') storeId: string
    ) {
        return this.storeShippingService.shippingDetail(storeId)
    }

    @Put('edit/:storeId')
    async editStoreShipping(
        @Param('storeId') storeId: string,
        @Body() editStoreShippingDto: EditStoreShipingDto
    ) {
        return this.storeShippingService.editStoreShipping(storeId, editStoreShippingDto)
    }

}
