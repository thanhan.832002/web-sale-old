import { Module } from "@nestjs/common";
import { StoreShipping, StoreShippingSchema } from "./store-shipping.model";
import { MongooseModule } from "@nestjs/mongoose";
import { StoreShippingController } from "./store-shipping.controller";
import { StoreShippingService } from "./store-shipping.service";

@Module({
    imports: [
        MongooseModule.forFeature([{ name: StoreShipping.name, schema: StoreShippingSchema }]),
    ],
    controllers: [StoreShippingController],
    providers: [StoreShippingService],
    exports: [StoreShippingService]
})

export class StoreShippingModule { }