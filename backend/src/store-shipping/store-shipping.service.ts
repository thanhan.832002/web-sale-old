import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { StoreShipping } from "./store-shipping.model";
import { Model } from "mongoose";
import BaseNoti from "src/base/notify";
import { EditStoreShipingDto } from "./store-shipping.dto";
import { BaseApiException } from "src/base/exception/base-api.exception";

@Injectable()
export class StoreShippingService {
    constructor(
        @InjectModel(StoreShipping.name) public storeShippingModel: Model<StoreShipping>,
    ) { }

    async shippingDetail(storeId: string) {
        try {
            const shipping = await this.storeShippingModel.findOne({ storeId })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: shipping
            }
        } catch (error) {
            throw error
        }
    }

    async getShippingStore(storeId: string) {
        try {
            const shipping = await this.storeShippingModel.findOne({ storeId }).lean()
            if (shipping?.listPrice?.length) return shipping
        } catch (error) {
            throw error
        }
    }

    async editStoreShipping(storeId: string, { name, listPrice }: EditStoreShipingDto) {
        try {
            if (!name) name = ''
            if (!listPrice || !listPrice.length) listPrice = []
            const shipping = await this.storeShippingModel.findOneAndUpdate({ storeId }, { name, listPrice })
            if (!shipping) {
                const newShipping = new this.storeShippingModel({
                    storeId,
                    name,
                    listPrice
                })
                await newShipping.save()
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

}