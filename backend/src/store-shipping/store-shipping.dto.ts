import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger"
import { Type, Transform } from "class-transformer"
import { IsOptional, ValidateNested, ArrayMinSize, IsArray, Validate, IsNotEmpty } from "class-validator"
import { StoreShippingPriceDto } from "./store-shipping.model"

export class EditStoreShipingDto {
    @ApiPropertyOptional()
    @IsOptional()
    name: string

    @ApiPropertyOptional({ type: [StoreShippingPriceDto] })
    @ValidateNested()
    @Type(() => StoreShippingPriceDto)
    @ArrayMinSize(0)
    @IsArray()
    @IsOptional()
    listPrice: StoreShippingPriceDto[]
}