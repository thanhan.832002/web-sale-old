import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Type, Transform } from "class-transformer";
import { IsNotEmpty, ValidateNested, ArrayMinSize, IsArray, Validate, IsOptional, Min } from "class-validator";
import { Document, Schema, Types } from "mongoose";
import { schemaOptions } from "src/base/base.shema";
import { IsObjectId } from "src/base/custom-validator";

export const StoreShippingPriceSchema: Schema = new Schema({
    price: Number,
    quantity: Number,
    total: Number
})

export const StoreShippingSchema: Schema = new Schema({
    name: String,
    listPrice: [StoreShippingPriceSchema],
    storeId: {
        type: Types.ObjectId,
        index: true,
        ref: 'Store'
    }
}, { ...schemaOptions, collection: 'StoreShipping' })

export class StoreShippingPriceDto {
    @ApiProperty()
    @Min(0)
    @Type(() => Number)
    @IsNotEmpty()
    price: number

    @ApiProperty()
    @IsOptional()
    quantity: number

    @ApiProperty()
    @IsOptional()
    total: number
}

export class StoreShipping extends Document {
    @ApiPropertyOptional()
    @IsOptional()
    name: string

    @ApiProperty({ type: [StoreShippingPriceDto] })
    @ValidateNested()
    @Type(() => StoreShippingPriceDto)
    @ArrayMinSize(1)
    @IsArray()
    @IsNotEmpty()
    listPrice: StoreShippingPriceDto[]

    @ApiProperty()
    @IsNotEmpty()
    @Validate(IsObjectId)
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    storeId: string
}