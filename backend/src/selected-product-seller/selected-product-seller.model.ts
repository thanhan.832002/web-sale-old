import { ApiProperty } from "@nestjs/swagger"
import { Type } from "class-transformer"
import { IsNotEmpty, IsNumber, Validate, IsOptional } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsObjectId } from "src/base/custom-validator"
import { VariantOption, VariantOptionSchema } from "src/variant-option/variant-option.model"

export const SelectedProductSellerSchema: Schema = new Schema({
    productSellerId: {
        type: Types.ObjectId,
        index: true,
        ref: 'ProductSeller'
    },
    variantSellerId: {
        type: Types.ObjectId,
        ref: 'VariantSeller'
    },
    variantId: {
        type: Types.ObjectId,
        ref: 'Variant'
    },
    productName: String,
    domain: String,
    sku: String,
    imageUrl: String,
    price: Number,
    comparePrice: Number,
    productCost: Number,
    listValue: [String],
    listOption: [VariantOptionSchema],
    quantity: Number,
    totalCost: Number,
    orderId: {
        type: Types.ObjectId,
        index: true,
        ref: 'Order'
    }
}, { ...schemaOptions, collection: 'SelectedProductSeller' })


export class SelectedProductSeller extends Document {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    productSellerId: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    variantSellerId: string

    @ApiProperty()
    @IsNotEmpty()
    productName: string

    @ApiProperty()
    @IsNotEmpty()
    imageUrl: string

    @ApiProperty()
    @IsNotEmpty()
    domain: string

    @ApiProperty()
    @IsNotEmpty()
    price: number

    @ApiProperty()
    @IsNotEmpty()
    comparePrice: number

    @ApiProperty()
    @IsNotEmpty()
    productCost: number

    @ApiProperty()
    @IsNotEmpty()
    totalCost: number

    @ApiProperty()
    @IsNotEmpty()
    sku: string

    @ApiProperty()
    @IsNotEmpty()
    listValue: string[]

    @ApiProperty()
    @IsNotEmpty()
    quantity: number

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    orderId: string

    @ApiProperty()
    @IsNotEmpty()
    listOption: VariantOption[]
}
