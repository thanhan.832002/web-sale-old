import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SelectedProductSeller } from './selected-product-seller.model';


@Injectable()
export class SelectedProductSellerService {
    constructor(
        @InjectModel(SelectedProductSeller.name) private readonly selectedProductSellerModel: Model<SelectedProductSeller>
    ) { }

    async createListSelectedProduct(selectedProductDto: Partial<SelectedProductSeller>[]) {
        try {
            const listSelectedProduct = await this.selectedProductSellerModel.insertMany(selectedProductDto)
            return listSelectedProduct
        } catch (error) {
            throw error
        }
    }

    async updateMany(filter: any, updateField: any) {
        try {
            await this.selectedProductSellerModel.updateMany(filter, updateField)
        } catch (error) {
            throw error
        }
    }

    async deleteManySelectedProduct(listSelectedProductId: string[]) {
        try {
            await this.selectedProductSellerModel.deleteMany({ _id: { $in: listSelectedProductId } })
        } catch (error) {
            throw error
        }
    }

    async find(filter: any, populate?: any) {
        return populate ? this.selectedProductSellerModel.find(filter).populate(populate).lean() : this.selectedProductSellerModel.find(filter).lean()
    }

}
