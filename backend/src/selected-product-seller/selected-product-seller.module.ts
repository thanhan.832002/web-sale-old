import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SelectedProductSellerController } from './selected-product-seller.controller';
import { SelectedProductSeller, SelectedProductSellerSchema } from './selected-product-seller.model';
import { SelectedProductSellerService } from './selected-product-seller.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: SelectedProductSeller.name, schema: SelectedProductSellerSchema }])
  ],
  controllers: [SelectedProductSellerController],
  providers: [SelectedProductSellerService],
  exports: [SelectedProductSellerService]
})
export class SelectedProductSellerModule { }
