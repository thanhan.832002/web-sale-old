import { ApiProperty } from "@nestjs/swagger"
import { IsNotEmpty, IsOptional } from "class-validator"

export class CreateSenderDto {
    @ApiProperty()
    @IsNotEmpty()
    nickname: string

    @ApiProperty()
    @IsNotEmpty()
    from_email: string

    @ApiProperty()
    @IsNotEmpty()
    from_name: string

    @ApiProperty()
    @IsNotEmpty()
    reply_to: string

    // @ApiProperty()
    // @IsNotEmpty()
    // reply_to_name: string

    @ApiProperty()
    @IsNotEmpty()
    address: string

    // @ApiProperty()
    // @IsNotEmpty()
    // address2: string

    // @ApiProperty()
    // @IsOptional()
    // state: string

    @ApiProperty()
    @IsNotEmpty()
    city: string

    @ApiProperty()
    @IsNotEmpty()
    country: string

    // @ApiProperty()
    // @IsNotEmpty()
    // zip: string

}