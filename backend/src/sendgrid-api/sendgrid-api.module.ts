import { Module } from "@nestjs/common";
import { SendgridApiService } from "./sendgrid-api.service";
import { AdminSettingModule } from "src/admin-setting/admin-setting.module";
@Module({
    imports: [
        AdminSettingModule
    ],
    providers: [SendgridApiService],
    exports: [SendgridApiService]
})
export class SendgridApiModule { }
