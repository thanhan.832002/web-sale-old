import { Injectable } from '@nestjs/common';
import { Timeout } from '@nestjs/schedule';
import { AdminSettingService } from 'src/admin-setting/admin-setting.service';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';
import { CreateSenderDto } from './sendgrid-api.dto';
const client = require('@sendgrid/client');

@Injectable()
export class SendgridApiService {
  constructor(private readonly adminSettingService: AdminSettingService) {}

  // async createSender(createSenderDto: CreateSenderDto) {
  //     try {
  //         const { nickname, from_email } = createSenderDto
  //         console.log('nickname, from_email  :>> ', nickname, from_email);
  //         const listSender = await this.getListSender(adminId)
  //         const exists = listSender.find((sender) => sender.nickname == nickname || sender.from_email == from_email)
  //         if (exists) {
  //             throw new BaseApiException({
  //                 message: BaseNoti.SENDGRID.NICKNAME_OR_EMAIL_EXISTS
  //             })
  //         }
  //         const request = {
  //             url: `/v3/verified_senders`,
  //             method: 'POST',
  //             body: createSenderDto
  //         }
  //         const [response] = await client.request(request).catch(error => { });
  //         if (response?.body) {
  //             return response.body
  //         } else {
  //             throw new BaseApiException({
  //                 message: BaseNoti.SENDGRID.CREATE_SENDER_FAIL
  //             })
  //         }
  //     } catch (error) {
  //         throw error
  //     }
  // }

  // async getStatusSender(adminId: string, email: string) {
  //     try {
  //         const listSender = await this.getListSender(adminId)
  //         const foundSender = listSender.find((sender) => sender.from_email == email)
  //         if (!foundSender) {
  //             throw new BaseApiException({
  //                 message: BaseNoti.SENDGRID.SENDER_NOT_FOUND
  //             })
  //         }
  //         return foundSender
  //     } catch (error) {
  //         throw error
  //     }
  // }

  // async foundSender(email: string) {
  //     try {
  //         const listSender = await this.getListSender(adminId)
  //         const foundSender = listSender.find((sender) => sender.from_email == email)
  //         return foundSender
  //     } catch (error) {
  //         throw error
  //     }
  // }

  // async resendVerifyMail(email: string) {
  //     try {
  //         const foundSender = await this.getStatusSender(email)
  //         const { id } = foundSender
  //         const request = {
  //             url: `/v3/verified_senders/resend/${id}`,
  //             method: 'POST',
  //         }
  //         const [response] = await client.request(request).catch(error => { });
  //         if (response?.statusCode == 204) {
  //             return {
  //                 message: BaseNoti.GLOBAL.SUCCESS
  //             }
  //         } else {
  //             throw new BaseApiException({
  //                 message: BaseNoti.SENDGRID.RESEND_VERIFY_FAIL
  //             })
  //         }
  //     } catch (error) {
  //         throw error
  //     }
  // }

  async getListSender(adminId: string) {
    try {
      const apikey = await this.adminSettingService.getApikeySendgrid(adminId);
      client.setApiKey(apikey);
      const config = {
        url: `/v3/verified_senders`,
        method: 'GET',
      };
      const [response] = await client.request(config).catch(() => {});
      const listSender = response?.body?.results || [];
      return listSender;
    } catch (error) {
      throw error;
    }
  }

  async verifyMail(adminId: string, token: string) {
    try {
      const apikey = await this.adminSettingService.getApikeySendgrid(adminId);
      client.setApiKey(apikey);
      const config = {
        url: `/v3/verified_senders/verify/${token}`,
        method: 'GET',
      };
      const [response] = await client.request(config).catch(() => {});
      return response;
    } catch (error) {
      throw error;
    }
  }

  // async deleteSender(adminId: string, email: string) {
  //     try {
  //         const foundSender = await this.foundSender(email)
  //         if (foundSender && foundSender.id) {
  //             const request = {
  //                 url: `/v3/verified_senders/${foundSender.id}`,
  //                 method: 'DELETE',

  //             }
  //             await client.request(request)
  //         }
  //     } catch (error) {
  //         throw error
  //     }
  // }

  async createAuthenticateDomain(adminId: string, domain: string) {
    try {
      const apikey = await this.adminSettingService.getApikeySendgrid(adminId);
      client.setApiKey(apikey);
      const data = {
        domain,
        subdomain: 'support',
      };
      const request = {
        url: `/v3/whitelabel/domains`,
        method: 'POST',
        body: data,
      };
      const [response] = await client.request(request);
      return response.body;
    } catch (error) {
      throw error;
    }
  }

  async validateAuthenDomain(adminId: string, domainId: string) {
    try {
      const apikey = await this.adminSettingService.getApikeySendgrid(adminId);
      client.setApiKey(apikey);
      const request = {
        url: `/v3/whitelabel/domains/${domainId}/validate`,
        method: 'POST',
      };
      const [response] = await client.request(request);
      return response?.body?.valid;
    } catch (error) {
      console.log('error :>> ', error);
      throw error;
    }
  }

  async getAllAuthenticateDomain(adminId: string, domain: string) {
    try {
      const apikey = await this.adminSettingService.getApikeySendgrid(adminId);
      client.setApiKey(apikey);
      const request = {
        url: `/v3/whitelabel/domains?domain=${domain}`,
        method: 'GET',
      };
      const [response] = await client.request(request);
      const found = response?.body.find(
        (domainAuth) => domainAuth.subdomain == 'support',
      );
      return found;
    } catch (error) {
      console.log('error :>> ', error);
    }
  }
}
