import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateDomainDto, UpdateDomainDto } from './domain.dto';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';
import { BaseService } from 'src/base/base.service';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { DataFilter, PaginationData } from 'src/base/interfaces/data.interface';
import { AdminSettingService } from 'src/admin-setting/admin-setting.service';
import { SendgridApiService } from 'src/sendgrid-api/sendgrid-api.service';
import { Domain } from './domain.model';
import * as ping from 'domain-ping';
import { ZerosslService } from 'src/zerossl/zerossl.service';
import { CreateDefaultHomePageDto } from 'src/homepage-setting/homepage-setting.dto';
import { HomepageSettingService } from 'src/homepage-setting/homepage-setting.service';
import { Role } from 'src/base/enum/roles.enum';
import { ModuleRef } from '@nestjs/core';
import { UserService } from 'src/user/user.service';
import { Timeout } from '@nestjs/schedule';
import { CacheService } from 'src/base/caching/cache.service';
import { KeyRedisEnum } from 'src/view/view.enum';
const forge = require('node-forge');

@Injectable()
export class DomainService extends BaseService<Domain> {
  private userService: UserService;
  constructor(
    @InjectModel(Domain.name) private readonly domainModel: Model<Domain>,
    private readonly sendGridApiService: SendgridApiService,
    private readonly adminSettingService: AdminSettingService,
    private readonly zerosslService: ZerosslService,
    private readonly homepageSettingService: HomepageSettingService,
    private readonly cacheService: CacheService,
    private moduleRef: ModuleRef,
  ) {
    super(domainModel);
  }

  onModuleInit() {
    this.userService = this.moduleRef.get(UserService, { strict: false });
  }

  async createDomain(adminId: string, createDomainDto: CreateDomainDto) {
    try {
      const { domain } = createDomainDto;
      await this.checkDomainExists(domain);
      const supportEmail = `support@${domain}`;
      //@ts-ignore
      createDomainDto.supportEmail = supportEmail;
      const { csr, privateKey } = this.createCsrAndPrivateKey(domain);
      const newDomain = new this.domainModel({
        ...createDomainDto,
        adminId,
        privateKey,
      });
      const createdSsl = await this.zerosslService.createCertificate(
        adminId,
        domain,
        csr,
      );
      await newDomain.save();
      const shopname = this.getShopname(domain);
      const defaultHomepageDto: CreateDefaultHomePageDto = {
        domainId: newDomain._id,
        homepageName: shopname,
        homepageTitle: shopname,
        supportEmail: `support@${domain}`,
        adminId,
      };
      await Promise.all([
        this.homepageSettingService.createDefaultHomePage(defaultHomepageDto),
        this.domainModel.findByIdAndUpdate(newDomain.id, {
          sslId: createdSsl.id,
        }),
        this.cacheService.remove({ key: KeyRedisEnum.listDomain }),
      ]);
      return {
        message: BaseNoti.GLOBAL.SUCCESS,
        data: newDomain,
      };
    } catch (error) {
      console.log('error :>> ', error);
      throw error;
    }
  }

  createCsrAndPrivateKey(domain: string) {
    try {
      const keys = forge.pki.rsa.generateKeyPair(2048);
      const csr = forge.pki.createCertificationRequest();
      csr.publicKey = keys.publicKey;
      csr.setSubject([{ name: 'commonName', value: domain }]);
      csr.sign(keys.privateKey);
      return {
        csr: forge.pki.certificationRequestToPem(csr),
        privateKey: forge.pki.privateKeyToPem(keys.privateKey),
      };
    } catch (error) {
      throw error;
    }
  }

  async updateDomain(adminId: string, updateDomainDto: UpdateDomainDto) {
    try {
      const { domainId, domain, enabled } = updateDomainDto;
      let updateField: any = {
        enabled,
        domain,
      };
      const foundDomain = await this.findDomainAdmin(adminId, domainId);
      if (
        enabled != undefined &&
        enabled &&
        (!foundDomain.isPointDomain ||
          !foundDomain.isEnableSsl ||
          !foundDomain.isVerifiedMail)
      ) {
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.SSL_NOT_ENABLE,
        });
      }
      if (domain && domain != foundDomain.domain) {
        await this.checkDomainExists(domain, domainId);
        updateField.enabled = false;
        updateField.supportEmail = `support@${domain}`;
      }
      const domainUpdated = await this.domainModel.findByIdAndUpdate(
        domainId,
        updateField,
        { new: true },
      );
      await this.cacheService.remove({ key: KeyRedisEnum.listDomain });
      return {
        message: BaseNoti.GLOBAL.SUCCESS,
        data: domainUpdated,
      };
    } catch (error) {
      throw error;
    }
  }

  async checkPointDomain(adminId: string, domainId: string) {
    try {
      const [foundDomain, ipServer] = await Promise.all([
        this.findDomainAdmin(adminId, domainId),
        this.adminSettingService.getIpServer(adminId),
      ]);
      const { domain, isPointDomain } = foundDomain;
      if (isPointDomain) {
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.IP_ALREADY_POINTED,
        });
      }
      try {
        const ipConfig = await ping(domain);
        if (ipConfig?.ip && ipConfig.ping && ipConfig.ip == ipServer) {
          await this.domainModel.findByIdAndUpdate(domainId, {
            isPointDomain: true,
          });
          return {
            message: BaseNoti.GLOBAL.SUCCESS,
          };
        } else {
          throw new BaseApiException({
            message: BaseNoti.SENDGRID.VERIFY_FAIL,
          });
        }
      } catch (error) {
        if (error && error.ip == ipServer) {
          await this.domainModel.findByIdAndUpdate(domainId, {
            isPointDomain: true,
          });
          return {
            message: BaseNoti.GLOBAL.SUCCESS,
          };
        }
        throw new BaseApiException({
          message: BaseNoti.SENDGRID.VERIFY_FAIL,
        });
      }
    } catch (error) {
      await this.domainModel.findByIdAndUpdate(domainId, {
        isPointDomain: false,
      });
      throw error;
    }
  }

  // async enableSsl(domainId: string) {
  //     try {
  //         const foundDomain = await this.findDomainById(domainId)
  //         const { adminId } = foundDomain
  //         const zerosslKey = await this.adminSettingService.getZerosslKey(adminId)
  //         if (!foundDomain || !foundDomain.domain) {
  //             throw new BaseApiException({
  //                 message: BaseNoti.DOMAIN.DOMAIN_NOT_EXISTS
  //             })
  //         }
  //         const { domain } = foundDomain
  //         await this.zerosslService.createCertificate(domain)
  //     } catch (error) {
  //         throw error
  //     }
  // }

  async getPoinDomainInfo(adminId: string, domainId: string) {
    try {
      const [foundDomain, ipServer] = await Promise.all([
        this.findDomainAdmin(adminId, domainId),
        this.adminSettingService.getIpServer(adminId),
      ]);
      if (!foundDomain || !foundDomain.domain) {
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.DOMAIN_NOT_EXISTS,
        });
      }
      const { domain, isPointDomain } = foundDomain;
      let poinDomainInfo: any = { isPointDomain };
      if (!isPointDomain) {
        let dns: any = [
          {
            name: domain,
            content: ipServer,
            type: 'A',
          },
          {
            name: '*',
            content: ipServer,
            type: 'A',
          },
        ];
        poinDomainInfo.dns = dns;
      }
      return {
        message: BaseNoti.GLOBAL.SUCCESS,
        data: poinDomainInfo,
      };
    } catch (error) {
      throw error;
    }
  }

  async getSslInfo(adminId: string, domainId: string) {
    try {
      const foundDomain: Domain = await this.findDomainAdmin(adminId, domainId);
      if (!foundDomain || !foundDomain.domain) {
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.DOMAIN_NOT_EXISTS,
        });
      }
      const { domain, isEnableSsl, sslId } = foundDomain;
      let sslInfo: any = { isEnableSsl };
      if (!isEnableSsl) {
        let verification = await this.zerosslService.verificationDetail(
          adminId,
          domain,
          sslId,
        );
        // console.log('verification :>> ', verification.validation.other_methods[`*.${domain}`]);
        let dns: any = {};
        if (verification?.validation?.other_methods[`*.${domain}`]) {
          dns = { ...verification.validation.other_methods[`*.${domain}`] };
        }
        sslInfo.dns = dns;
      }
      return {
        message: BaseNoti.GLOBAL.SUCCESS,
        data: sslInfo,
      };
    } catch (error) {
      throw error;
    }
  }

  async verifySsl(adminId: string, domainId: string) {
    try {
      const foundDomain = await this.findDomainAdmin(adminId, domainId);
      if (!foundDomain || !foundDomain.domain) {
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.DOMAIN_NOT_EXISTS,
        });
      }
      const { sslId, isEnableSsl, domain } = foundDomain;
      if (isEnableSsl) {
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.SSL_ALREADY_ENABLE,
        });
      }
      let zerosslDetail = await this.zerosslService.verificationDetail(
        adminId,
        domain,
        sslId,
      );
      console.log('zerosslDetail :>> ', zerosslDetail);
      if (zerosslDetail.status == 'issued') {
        // download key, create key
        console.log('Issued');
        const key = await this.zerosslService.downloadCsr(adminId, sslId);
        const certificate = key['certificate.crt'];
        const ca_bundle = key['ca_bundle.crt'];
        console.log('certificate :>> ', certificate);
        console.log('ca_bundle :>> ', ca_bundle);
        await this.zerosslService.saveCsrFile(
          domain,
          certificate,
          ca_bundle,
          foundDomain.privateKey,
        );
        await this.zerosslService.createNginxCfgFile(domain);
        await this.domainModel.findByIdAndUpdate(domainId, {
          isEnableSsl: true,
        });
        return {
          message: BaseNoti.GLOBAL.SUCCESS,
        };
      } else {
        let verificationSsl = await this.zerosslService.verifySsl(
          adminId,
          sslId,
        );
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.SSL_NOT_ENABLE,
        });
        // console.log('verificationSsl :>> ', verificationSsl);
        // if (verificationSsl.success && verificationSsl.status == 'issued') {

        // }
        // let sslInfo: any = { ...verificationSsl }
        // return {
        //     message: BaseNoti.GLOBAL.SUCCESS,
        //     data: sslInfo
        // }
      }
    } catch (error) {
      throw error;
    }
  }

  async deleteDomain(adminId: string, domainId: string) {
    try {
      const found = await this.domainModel.findOne({ _id: domainId, adminId });
      if (!found) {
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.DOMAIN_NOT_EXISTS,
        });
      }
      if (found.isSold) {
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.DOMAIN_CANNOT_DELETE,
        });
      }
      await this.domainModel.findByIdAndDelete(domainId);
      await this.homepageSettingService.deleteHomepageByDomainId(domainId);
      await this.cacheService.remove({ key: KeyRedisEnum.listDomain });
      return {
        messgae: BaseNoti.GLOBAL.SUCCESS,
      };
    } catch (error) {
      throw error;
    }
  }

  async getDetailDomain(userId: string, role: Role, domainId: string) {
    try {
      let adminId = userId;
      if (role == Role.seller) {
        const foundUser = await this.userService.findUserById(userId);
        adminId = foundUser.adminId;
      }
      const domain = await this.domainModel.findOne({ _id: domainId, adminId });
      if (!domain)
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.DOMAIN_NOT_EXISTS,
        });
      return {
        message: BaseNoti.GLOBAL.SUCCESS,
        data: domain,
      };
    } catch (error) {
      throw error;
    }
  }

  async getListDomain(adminId: string, query: BaseQuery) {
    try {
      let { page, limit, search, filter } = query;
      const filterObject: DataFilter<Domain> = {
        limit,
        page,
        condition: {
          $or: [{ isAdmin: { $exists: false } }, { isAdmin: false }],
          adminId,
        },
        population: [],
      };
      if (filter) {
        filterObject.condition = { ...filterObject.condition, ...filter };
      }
      if (search) {
        filterObject.condition['$or'] = [
          { domain: { $regex: search, $options: 'i' } },
        ];
      }
      let list: PaginationData<Domain> = await this.getListDocument(
        filterObject,
      );
      return {
        message: BaseNoti.GLOBAL.SUCCESS,
        data: list,
      };
    } catch (error) {
      throw error;
    }
  }

  async sellerGetListDomain(userId: string, role: Role, query: BaseQuery) {
    try {
      let adminId = userId;
      if (role == Role.seller) {
        const foundUser = await this.userService.findUserById(userId);
        adminId = foundUser.adminId;
      }
      let { page, limit, search, filter } = query;
      const filterObject: DataFilter<Domain> = {
        limit,
        page,
        condition: {
          enabled: true,
          $or: [{ isAdmin: { $exists: false } }, { isAdmin: false }],
          adminId,
        },
        population: [],
      };
      if (filter) {
        filterObject.condition = { ...filterObject.condition, ...filter };
      }
      if (search) {
        filterObject.condition['$or'] = [
          { domain: { $regex: search, $options: 'i' } },
        ];
      }
      let list: PaginationData<Domain> = await this.getListDocument(
        filterObject,
      );
      return {
        message: BaseNoti.GLOBAL.SUCCESS,
        data: list,
      };
    } catch (error) {
      throw error;
    }
  }

  async checkDomainExists(domain: string, domainId?: string) {
    try {
      const condition = domainId
        ? { domain, _id: { $ne: domainId } }
        : { domain };
      const isExists = await this.domainModel.exists(condition);
      if (isExists) {
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.DOMAIN_ALREADY_EXISTS,
        });
      }
      return isExists;
    } catch (error) {
      throw error;
    }
  }

  async checkMailSupportExists(email: string, domainId: string) {
    try {
      const isExists = await this.domainModel.exists({
        supportEmail: email,
        _id: { $ne: domainId },
      });
      if (isExists) {
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.DOMAIN_ALREADY_EXISTS,
        });
      }
      return isExists;
    } catch (error) {
      throw error;
    }
  }

  async findDomainById(domainId: string) {
    try {
      const domain = domainId
        ? await this.domainModel.findById(domainId)
        : null;
      if (!domain)
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.DOMAIN_NOT_EXISTS,
        });
      return domain;
    } catch (error) {
      throw error;
    }
  }

  async findDomainAdmin(adminId: string, domainId: string) {
    try {
      const domain = await this.domainModel.findOne({ _id: domainId, adminId });
      if (!domain)
        throw new BaseApiException({
          message: BaseNoti.DOMAIN.DOMAIN_NOT_EXISTS,
        });
      return domain;
    } catch (error) {
      throw error;
    }
  }

  async verifyEmailDomain(adminId: string, domainId: string) {
    try {
      const foundDomain = await this.findDomainAdmin(adminId, domainId);
      const { domain } = foundDomain;
      const foundAuthenDomain =
        await this.sendGridApiService.getAllAuthenticateDomain(adminId, domain);
      if (!foundAuthenDomain) {
        const responseBody =
          await this.sendGridApiService.createAuthenticateDomain(
            adminId,
            domain,
          );
        const { dns } = responseBody;
        let verifyEmailInfo = [];
        if (dns) {
          const { mail_cname, dkim1, dkim2 } = dns;
          if (mail_cname && dkim1 && dkim2) {
            verifyEmailInfo = [
              {
                ...mail_cname,
                name: 'mail_cname',
                value: mail_cname?.data,
              },
              {
                ...dkim1,
                name: 'dkim1',
                value: dkim1?.data,
              },
              {
                ...dkim2,
                name: 'dkim2',
                value: dkim2?.data,
              },
            ];
          }
        }

        if (responseBody?.valid) {
          await this.domainModel.findByIdAndUpdate(domainId, {
            isVerifiedMail: true,
            domainId: responseBody.id,
            verifyEmailInfo,
          });
          return {
            message: BaseNoti.GLOBAL.SUCCESS,
          };
        } else {
          await this.domainModel.findByIdAndUpdate(domainId, {
            domainId: responseBody.id,
            verifyEmailInfo,
          });
          const dns = responseBody?.dns;
          throw new BaseApiException({
            message: BaseNoti.SENDGRID.NOT_VERIFIED_YET,
            data: dns,
          });
        }
      }
      const { dns } = foundAuthenDomain;
      let verifyEmailInfo = [];
      if (dns) {
        const { mail_cname, dkim1, dkim2 } = dns;
        if (mail_cname && dkim1 && dkim2) {
          verifyEmailInfo = [
            {
              ...mail_cname,
              name: 'mail_cname',
              value: mail_cname?.data,
            },
            {
              ...dkim1,
              name: 'dkim1',
              value: dkim1?.data,
            },
            {
              ...dkim2,
              name: 'dkim2',
              value: dkim2?.data,
            },
          ];
        }
      }
      if (foundAuthenDomain.valid) {
        await this.domainModel.findByIdAndUpdate(domainId, {
          isVerifiedMail: true,
          domainId: foundAuthenDomain.id,
          verifyEmailInfo,
        });
        return {
          message: BaseNoti.GLOBAL.SUCCESS,
        };
      }
      const validated = await this.sendGridApiService.validateAuthenDomain(
        adminId,
        foundAuthenDomain.id,
      );
      if (validated) {
        await this.domainModel.findByIdAndUpdate(domainId, {
          isVerifiedMail: true,
          domainId: foundAuthenDomain.id,
          verifyEmailInfo: verifyEmailInfo.map((verify) => ({
            ...verify,
            valid: true,
          })),
        });
        return {
          message: BaseNoti.GLOBAL.SUCCESS,
        };
      } else {
        const dns = foundAuthenDomain.dns;
        throw new BaseApiException({
          message: BaseNoti.SENDGRID.NOT_VERIFIED_YET,
          data: dns,
        });
      }
    } catch (error) {
      throw error;
    }
  }

  async findListDomain(filter) {
    try {
      const list = await this.domainModel.find(filter);
      return list;
    } catch (error) {
      throw error;
    }
  }

  async findListDomainBuyer() {
    try {
      const found = await this.cacheService.get({
        key: KeyRedisEnum.listDomain,
      });
      if (found) return JSON.parse(found);
      const list = await this.domainModel
        .find({
          isAdmin: false,
          enabled: true,
        })
        .lean();
      const listStrDomain = list.map((domain) => domain.domain);
      listStrDomain &&
        listStrDomain.length &&
        (await this.cacheService.set({
          key: KeyRedisEnum.listDomain,
          value: JSON.stringify(listStrDomain),
          expiresIn: 24 * 60 * 60,
        }));
      return listStrDomain;
    } catch (error) {
      throw error;
    }
  }

  getShopname(domain: string) {
    try {
      const name = domain.split('.')[0];
      return name.charAt(0).toUpperCase() + name.slice(1);
    } catch (error) {
      throw error;
    }
  }

  async checkDomainSold(domainId: string) {
    try {
      const foundDomain = await this.domainModel
        .findById(domainId)
        .select('isSold');
      if (!foundDomain?.isSold) {
        await this.domainModel.findByIdAndUpdate(domainId, { isSold: true });
      }
    } catch (error) {
      throw error;
    }
  }
}
