import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Domain, DomainSchema } from './domain.model';
import { DomainController } from './domain.controller';
import { DomainService } from './domain.service';
import { DomainSellerController } from './domain-seller.controller';
import { AdminSettingModule } from 'src/admin-setting/admin-setting.module';
import { SendgridApiModule } from 'src/sendgrid-api/sendgrid-api.module';
import { ZerosslModule } from 'src/zerossl/zerossl.module';
import { HomepageSettingModule } from 'src/homepage-setting/homepage-setting.module';
import { SharedModule } from 'src/base/shared.module';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Domain.name, schema: DomainSchema }]),
        SendgridApiModule,
        AdminSettingModule,
        ZerosslModule,
        HomepageSettingModule,
        SharedModule
    ],
    controllers: [DomainController, DomainSellerController],
    providers: [DomainService],
    exports: [DomainService]
})
export class DomainModule { }
