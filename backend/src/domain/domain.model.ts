import { ApiProperty } from "@nestjs/swagger"
import { Transform, Type } from "class-transformer"
import { IsNotEmpty, IsNumber, Validate, IsOptional, Matches, IsBoolean } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { VerifyEmailInfoDto } from "./domain.dto"
import { IsObjectId } from "src/base/custom-validator"

const VeifyMailInfoSchema: Schema = new Schema({
    name: String,
    type: String,
    host: String,
    value: String,
    valid: Boolean
})

export const DomainSchema: Schema = new Schema({
    domain: { type: String, index: true },
    domainId: String,
    supportEmail: String,
    isPointDomain: { type: Boolean, default: false },
    sslId: String,
    privateKey: String,
    isEnableSsl: { type: Boolean, default: false },
    enabled: { type: Boolean, default: false, index: true },
    isVerifiedMail: { type: Boolean, default: false },
    verifyEmailInfo: [VeifyMailInfoSchema],
    isAdmin: { type: Boolean, default: false },
    adminId: {
        type: Types.ObjectId,
        index: true,
        ref: 'User'
    },
    isSold: Boolean
}, { ...schemaOptions, collection: 'Domain' })


export class Domain extends Document {
    @ApiProperty()
    @Matches(/^[a-zA-Z0-9][a-zA-Z0-9-.]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/)
    @IsNotEmpty()
    domain: string

    @ApiProperty()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsNotEmpty()
    enabled: boolean

    @ApiProperty()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsNotEmpty()
    isVerifiedMail: boolean

    verifyEmailInfo: VerifyEmailInfoDto[]

    isPointDomain: boolean
    isEnableSsl: boolean
    isAdmin: boolean
    sslId: string
    privateKey: string

    @ApiProperty()
    @Matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
    @Transform(({ value }) => {
        if (value) return value.toLowerCase().trim()
    })
    @IsNotEmpty()
    supportEmail: string

    @ApiProperty()
    @IsNotEmpty()
    @Validate(IsObjectId)
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    adminId: string

    isSold: boolean
}
