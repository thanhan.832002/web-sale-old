import { ApiProperty, ApiPropertyOptional, PickType } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
  IsBoolean,
  IsNotEmpty,
  IsOptional,
  Matches,
  Validate,
} from 'class-validator';
import { IsObjectId } from 'src/base/custom-validator';

export class CreateDomainDto {
  @ApiProperty()
  @Matches(/^[a-zA-Z0-9][a-zA-Z0-9-.]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/)
  @Transform(({ value }) => {
    if (value) {
      return value.toLowerCase().trim();
    }
  })
  @IsNotEmpty()
  domain: string;

  // @ApiProperty()
  // @IsBoolean()
  // @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
  // @IsOptional()
  // enabled: boolean
}

export class UpdateDomainDto {
  @ApiProperty()
  @Validate(IsObjectId)
  @IsNotEmpty()
  domainId: string;

  @ApiPropertyOptional()
  @Matches(/^[a-zA-Z0-9][a-zA-Z0-9-.]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/)
  @Transform(({ value }) => {
    if (value) return value.toLowerCase().trim();
  })
  @IsOptional()
  domain: string;

  @ApiPropertyOptional()
  @IsBoolean()
  @Transform(
    ({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1,
  )
  @IsOptional()
  enabled: boolean;
}

export class UpdateEmailDomainDto {
  @ApiProperty()
  @Validate(IsObjectId)
  @IsNotEmpty()
  domainId: string;

  @ApiProperty()
  @Matches(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  )
  @Transform(({ value }) => {
    if (value) return value.toLowerCase().trim();
  })
  @IsOptional()
  supportEmail: string;
}

export class VerifyEmailDto {
  @ApiProperty()
  @IsNotEmpty()
  token: string;
}

export class VerifyEmailDomain {
  @ApiProperty()
  @Validate(IsObjectId)
  @IsNotEmpty()
  domainId: string;
}

export class VerifyEmailInfoDto {
  name: string;
  type: string;
  host: string;
  value: string;
  valid: boolean;
}
