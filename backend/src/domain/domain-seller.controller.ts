import { Controller, UseGuards, Get, Param, Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { DomainService } from './domain.service';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { SellerGuard } from 'src/base/guard/seller.guard';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';

@Controller('domain-seller')
@ApiTags('Domain Seller')
@ApiBearerAuth()
@UseGuards(SellerGuard)
@UseGuards(AuthGuard('jwt'))
export class DomainSellerController {
    constructor(
        private readonly domainService: DomainService
    ) { }

    @Get('detail/:domainId')
    getDetailDomain(
        @GetUser() { userId, role }: JwtTokenDecrypted,
        @Param('domainId') domainId: string) {
        return this.domainService.getDetailDomain(userId, role, domainId)
    }

    @Get('list')
    sellerGetListDomain(
        @GetUser() { userId, role }: JwtTokenDecrypted,
        @Query() query: BaseQuery) {
        return this.domainService.sellerGetListDomain(userId, role, query)
    }

}
