import {
  Controller,
  UseGuards,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Query,
  Body,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { DomainService } from './domain.service';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import {
  CreateDomainDto,
  UpdateDomainDto,
  UpdateEmailDomainDto,
  VerifyEmailDomain,
  VerifyEmailDto,
} from './domain.dto';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';

@Controller('domain')
@ApiTags('Domain')
@ApiBearerAuth()
@UseGuards(AdminGuard)
@UseGuards(AuthGuard('jwt'))
export class DomainController {
  constructor(private readonly domainService: DomainService) {}

  @Get('detail/:domainId')
  getDetailDomain(
    @GetUser() { userId, role }: JwtTokenDecrypted,
    @Param('domainId') domainId: string,
  ) {
    return this.domainService.getDetailDomain(userId, role, domainId);
  }

  @Get('list')
  getListDomain(
    @GetUser() { userId }: JwtTokenDecrypted,
    @Query() query: BaseQuery,
  ) {
    return this.domainService.getListDomain(userId, query);
  }

  @Post('create')
  createDomain(
    @GetUser() { userId }: JwtTokenDecrypted,
    @Body() createDomainDto: CreateDomainDto,
  ) {
    return this.domainService.createDomain(userId, createDomainDto);
  }

  @Put('update')
  updateDomain(
    @GetUser() { userId }: JwtTokenDecrypted,
    @Body() updateDomainDto: UpdateDomainDto,
  ) {
    return this.domainService.updateDomain(userId, updateDomainDto);
  }

  @Delete('delete/:domainId')
  deleteDomain(
    @GetUser() { userId }: JwtTokenDecrypted,
    @Param('domainId') domainId: string,
  ) {
    return this.domainService.deleteDomain(userId, domainId);
  }

  @Post('verify-email-domain')
  verifyEmailDomain(
    @GetUser() { userId }: JwtTokenDecrypted,
    @Body() { domainId }: VerifyEmailDomain,
  ) {
    return this.domainService.verifyEmailDomain(userId, domainId);
  }

  @Put('check-point-domain/:domainId')
  checkPointDomain(
    @GetUser() { userId }: JwtTokenDecrypted,
    @Param('domainId') domainId: string,
  ) {
    return this.domainService.checkPointDomain(userId, domainId);
  }

  @Get('point-domain-info/:domainId')
  getPoinDomainInfo(
    @GetUser() { userId }: JwtTokenDecrypted,
    @Param('domainId') domainId: string,
  ) {
    return this.domainService.getPoinDomainInfo(userId, domainId);
  }

  @Get('ssl-info/:domainId')
  getSslInfo(
    @GetUser() { userId }: JwtTokenDecrypted,
    @Param('domainId') domainId: string,
  ) {
    return this.domainService.getSslInfo(userId, domainId);
  }

  @Get('verify-ssl/:domainId')
  verifySsl(
    @GetUser() { userId }: JwtTokenDecrypted,
    @Param('domainId') domainId: string,
  ) {
    return this.domainService.verifySsl(userId, domainId);
  }
}
