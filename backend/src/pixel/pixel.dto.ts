import { ApiProperty, PickType } from "@nestjs/swagger"
import { IsOptional } from "class-validator"
import { Pixel } from "./pixel.model"

export class UpdatePixelDto extends PickType(Pixel, ['googleAds', 'facebookPixel', 'tiktokPixel', 'pinterestTagAds', 'twitterPixel', 'snapPixel']) { }