import { ApiProperty } from "@nestjs/swagger"
import { Type } from "class-transformer"
import { IsNotEmpty, IsNumber, Validate, IsOptional } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsObjectId } from "src/base/custom-validator"

export const GoogleAdsSchema: Schema = new Schema({
    googleAnalyticId: String,
    googleTagManager: String
})

export const FacebookPixelSchema: Schema = new Schema({
    contentId: String,
    facebookPixelId: String
})


export const TiktokPixelSchema: Schema = new Schema({
    tikTokPixelId: String,
    customScript: String,
    snapPixelId: String
})

export const PinterestTagSchema: Schema = new Schema({
    pinterestTagId: String,
    PinterestEmail: String
})

export const SnapSchema: Schema = new Schema({
    snapPixelId: String,
})

export const TwitterPixelSchema: Schema = new Schema({
    twitterPixelId: String,
    pageViewId: String,
    contentViewId: String,
    addToCartId: String,
    checkoutInitiatedId: String,
    addedPaymentInfoId: String,
    purchaseId: String,
})

export const PixelSchema: Schema = new Schema({
    storeId: {
        type: Types.ObjectId,
        ref: 'Store',
    },
    googleAds: GoogleAdsSchema,
    facebookPixel: FacebookPixelSchema,
    tiktokPixel: TiktokPixelSchema,
    pinterestTag: PinterestTagSchema,
    twitterPixel: TwitterPixelSchema,
    snapPixel: SnapSchema,
}, { ...schemaOptions, collection: 'Pixel' })

export class GoogleAds {
    @ApiProperty()
    @IsOptional()
    googleAnalyticId: string

    @ApiProperty()
    @IsOptional()
    googleTagManager: string
}

export class FacebookPixel {
    @ApiProperty()
    @IsOptional()
    contentId: string

    @ApiProperty()
    @IsOptional()
    facebookPixelId: string

}

export class TiktokPixel {
    @ApiProperty()
    @IsOptional()
    tikTokPixelId: string

    @ApiProperty()
    @IsOptional()
    customScript: string

    @ApiProperty()
    @IsOptional()
    snapPixelId: string

}

export class PinterestTagAds {
    @ApiProperty()
    @IsOptional()
    pinterestTagId: string

    @ApiProperty()
    @IsOptional()
    PinterestEmail: string

}

export class TwitterPixel {
    @ApiProperty()
    @IsOptional()
    twitterPixelId: string

    @ApiProperty()
    @IsOptional()
    pageViewId: string

    @ApiProperty()
    @IsOptional()
    contentViewId: string

    @ApiProperty()
    @IsOptional()
    addToCartId: string

    @ApiProperty()
    @IsOptional()
    checkoutInitiatedId: string

    @ApiProperty()
    @IsOptional()
    addedPaymentInfoId: string

    @ApiProperty()
    @IsOptional()
    purchaseId: string

}

export class SnapPixel {
    @ApiProperty()
    @IsOptional()
    snapPixelId: string
}
export class Pixel extends Document {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    storeId: string

    @ApiProperty()
    @IsOptional()
    googleAds: GoogleAds

    @ApiProperty()
    @IsOptional()
    facebookPixel: FacebookPixel

    @ApiProperty()
    @IsOptional()
    tiktokPixel: TiktokPixel

    @ApiProperty()
    @IsOptional()
    pinterestTagAds: PinterestTagAds

    @ApiProperty()
    @IsOptional()
    twitterPixel: TwitterPixel

    @ApiProperty()
    @IsOptional()
    snapPixel: SnapPixel
}

