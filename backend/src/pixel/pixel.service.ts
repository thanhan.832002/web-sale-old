import { Injectable } from '@nestjs/common';
import { UpdatePixelDto } from './pixel.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Pixel } from './pixel.model';
import { Model } from 'mongoose';
import { StoreService } from 'src/store/store.service';
import BaseNoti from 'src/base/notify';
import { StoreStatus } from 'src/store/store.enum';
import { CacheService } from 'src/base/caching/cache.service';
import { KeyRedisEnum } from 'src/view/view.enum';

@Injectable()
export class PixelService {
    constructor(
        @InjectModel(Pixel.name) private pixelModel: Model<Pixel>,
        private readonly cacheService: CacheService,
        private readonly storeService: StoreService
    ) { }

    async updatePixel(userId: string, storeId: string, updatePixelDto: UpdatePixelDto) {
        try {
            const foundStore = await this.storeService.findStore({ _id: storeId, sellerId: userId })
            const foundPixel = await this.findPixelByStoreId(storeId)
            if (foundPixel) {
                await this.pixelModel.findByIdAndUpdate(foundPixel.id, updatePixelDto)
            } else {
                const newPixel = new this.pixelModel({
                    storeId,
                    ...updatePixelDto
                })
                await newPixel.save()
            }
            await this.cacheService.remove({ key: `${KeyRedisEnum.pixel}-${foundStore.domain}` })
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async pixelDetail(userId: string, storeId: string) {
        try {
            await this.storeService.findStore({ _id: storeId, sellerId: userId })
            const foundPixel = await this.findPixelByStoreId(storeId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: foundPixel
            }
        } catch (error) {
            throw error
        }
    }

    async pixelDetailDomain(domain: string) {
        try {
            const cachedResponse = await this.cacheService.get({ key: `${KeyRedisEnum.pixel}-${domain}` })
            if (cachedResponse) return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: JSON.parse(cachedResponse)
            }
            const store = await this.storeService.findStoreBuyer(domain)
            const foundPixel = await this.findPixelByStoreId(store._id)
            if (foundPixel) {
                await this.cacheService.set({ key: `${KeyRedisEnum.pixel}-${domain}`, value: JSON.stringify(foundPixel), expiresIn: 30 * 24 * 60 * 60 })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: foundPixel
            }
        } catch (error) {
            throw error
        }
    }

    async findPixelByStoreId(storeId: string) {
        try {
            const foundPixel = await this.pixelModel.findOne({ storeId })
            return foundPixel || null
        } catch (error) {
            throw error
        }
    }
}
