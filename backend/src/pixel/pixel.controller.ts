import { Body, Controller, Get, Param, Put, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SellerGuard } from 'src/base/guard/seller.guard';
import { PixelService } from './pixel.service';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { UpdatePixelDto } from './pixel.dto';
import { GetDomain } from 'src/base/decorator/get-domain.decorator';

@Controller('pixel')
@ApiTags('Pixel')
export class PixelController {
    constructor(
        private readonly pixelService: PixelService
    ) { }

    @UseGuards(SellerGuard)
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Put('update/:storeId')
    updatePixel(
        @Param('storeId') storeId: string,
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() updatePixelDto: UpdatePixelDto
    ) {
        return this.pixelService.updatePixel(userId, storeId, updatePixelDto)
    }

    @UseGuards(SellerGuard)
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Get('detail/:storeId')
    pixelDetail(
        @Param('storeId') storeId: string,
        @GetUser() { userId }: JwtTokenDecrypted
    ) {
        return this.pixelService.pixelDetail(userId, storeId)
    }

    @Get('domain/:domain')
    pixelDetailDomain(
        @Param('domain') domain: string
    ) {
        return this.pixelService.pixelDetailDomain(domain)
    }
}