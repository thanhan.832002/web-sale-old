import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PixelController } from './pixel.controller';
import { Pixel, PixelSchema } from './pixel.model';
import { PixelService } from './pixel.service';
import { StoreModule } from 'src/store/store.module';
import { SharedModule } from 'src/base/shared.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Pixel.name, schema: PixelSchema }]),
    StoreModule,
    SharedModule
  ],
  controllers: [PixelController],
  providers: [PixelService],
  exports: [PixelService]
})
export class PixelModule { }
