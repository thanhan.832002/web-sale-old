import { Module } from '@nestjs/common';
import { PaymentService } from './payment.service';
import { PaymentController } from './payment.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Payment, PaymentSchema } from './payment.model';
import { PolicyModule } from 'src/policy/policy.module';
import { UserPaymentModule } from 'src/seller-payment/seller-payment.module';
import { OrderModule } from 'src/order/order.module';
import { PaymentBuyerController } from './payment-buyer.comtroller';
import { StoreModule } from 'src/store/store.module';
import { StorePaymentModule } from 'src/store-payment/store-payment.module';
import { SharedModule } from 'src/base/shared.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Payment.name, schema: PaymentSchema }]),
    PolicyModule,
    UserPaymentModule,
    StoreModule,
    StorePaymentModule,
    SharedModule
  ],
  providers: [PaymentService],
  exports: [PaymentService],
  controllers: [PaymentController, PaymentBuyerController]
})
export class PaymentModule { }
