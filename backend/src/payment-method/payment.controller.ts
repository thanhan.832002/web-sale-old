import { Controller, Get, Post, Put, Body, UseGuards, Param, Query, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { AttachPaymentDto } from 'src/seller-payment/seller-payment.dto';
import { UserPaymentService } from 'src/seller-payment/seller-payment.service';
import { CreatePaymentDto, EditPaymentDto, PaymentQuery } from './payment.dto';
import { PaymentService } from './payment.service';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';

@Controller('payment')
@ApiTags('Payment')
@UseGuards(AdminGuard)
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
export class PaymentController {
    constructor(
        private readonly userPaymentService: UserPaymentService,
        private readonly paymentService: PaymentService
    ) { }

    @Post('create')
    createPayment(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() createPaymentDto: CreatePaymentDto) {
        return this.paymentService.createPayment(userId, createPaymentDto)
    }

    @Put('edit/:paymentId')
    editPayment(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('paymentId') paymentId: string,
        @Body() editPaymentDto: EditPaymentDto) {
        return this.paymentService.editPayment(userId, paymentId, editPaymentDto)
    }

    @Get('list')
    getListPayment(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() paymentQuery: PaymentQuery) {
        return this.paymentService.getListPayment(userId, paymentQuery)
    }

    @Get('detail/:paymentId')
    getPaymentDetail(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('paymentId') paymentId: string) {
        return this.paymentService.getPaymentDetail(userId, paymentId)
    }

    @Delete('delete/:paymentId')
    deletePayment(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('paymentId') paymentId: string) {
        return this.paymentService.deletePayment(userId, paymentId)
    }

    @Get('list-seller/:paymentId')
    adminGetListSellerPayment(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('paymentId') paymentId: string,
        @Query() query: BaseQuery) {
        return this.paymentService.adminGetListSellerPayment(userId, paymentId, query)
    }

    @Put('attach')
    adminAttachPaymentSeller(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() attachPaymentDto: AttachPaymentDto) {
        return this.paymentService.adminAttachPaymentSeller(userId, attachPaymentDto)
    }

    @Delete('delete-seller-payment/:userPaymentId')
    adminDeletePaymentSeller(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('userPaymentId') userPaymentId: string) {
        return this.userPaymentService.adminDeletePaymentSeller(userId, userPaymentId)
    }

}
