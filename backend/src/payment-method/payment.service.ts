import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';
import { PolicyService } from 'src/policy/policy.service';
import { CreatePaymentDto, EditPaymentDto, PaymentQuery } from './payment.dto';
import { Payment } from './payment.model';
import * as _ from 'lodash'
import { UserPaymentService } from 'src/seller-payment/seller-payment.service';
import { BaseService } from 'src/base/base.service';
import { DataFilter } from 'src/base/interfaces/data.interface';
import * as Promise from 'bluebird'
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { AttachPaymentDto } from 'src/seller-payment/seller-payment.dto';
import { StoreService } from 'src/store/store.service';
import { StorePaymentService } from 'src/store-payment/store-payment.service';
import { CacheService } from 'src/base/caching/cache.service';
import { KeyRedisEnum } from 'src/view/view.enum';
import { Timeout } from '@nestjs/schedule';
@Injectable()
export class PaymentService extends BaseService<Payment> {
    constructor(
        @InjectModel(Payment.name) private readonly paymentModel: Model<Payment>,
        private readonly userPaymentService: UserPaymentService,
        private readonly policyService: PolicyService,
        private readonly storeService: StoreService,
        private readonly storePaymentService: StorePaymentService,
        private readonly cacheService: CacheService
    ) { super(paymentModel) }

    async createPayment(adminId: string, createPaymentDto: CreatePaymentDto) {
        try {
            const { policy, name } = createPaymentDto
            const isPayment = await this.paymentModel.exists({ name, adminId })
            if (isPayment) {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.PAYMENT_ALREADY_EXISTS
                })
            }
            const newPayment = new this.paymentModel({ ...createPaymentDto, adminId })
            if (policy) {
                const newPolicy = await this.policyService.createPolicy(policy)
                newPayment.policyId = newPolicy._id
            }
            await newPayment.save()
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: newPayment
            }
        } catch (error) {
            throw error
        }
    }

    async editPayment(adminId: string, paymentId: string, editPaymentDto: EditPaymentDto) {
        try {
            const foundPayment = await this.findPaymentByAdmin(adminId, paymentId)
            const { enabled, policy } = editPaymentDto
            const validField: any = _.pickBy(editPaymentDto)
            if (enabled != undefined) {
                validField.enabled = enabled
                //Turn off all payment method of seller:
                if (enabled == false) {
                    await this.userPaymentService.turnOffAllSellerPayment(paymentId)
                }
            }
            if (policy) {
                if (foundPayment.policyId) {
                    await this.policyService.updatePolicy(foundPayment.policyId, policy)
                } else {
                    const newPolicy = await this.policyService.createPolicy(policy)
                    validField.policyId = newPolicy._id
                }
            }
            const payment = await this.paymentModel.findByIdAndUpdate(paymentId, validField, { new: true }).populate('policyId')
            await this.removeCachePolicy(paymentId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: payment
            }
        } catch (error) {
            throw error
        }
    }

    async removeCachePolicy(paymentId: string) {
        try {
            const { data } = await this.userPaymentService.getListDocument({ condition: { paymentId }, selectCols: 'sellerId' })
            if (!data || !data.length) return
            const listStore = await this.storeService.findListStore({})
            let listChunk = _.chunk(listStore, 10);
            await Promise.map(listChunk, async (chunk) => {
                let listKey = []
                for (let store of chunk) {
                    const { domain } = store
                    listKey.push(`${KeyRedisEnum.policy}-${domain}`)
                    listKey.push(`${KeyRedisEnum.listPayment}-${domain}`)
                    listKey.push(`${KeyRedisEnum.skstripe}-${domain}`)
                }
                await this.cacheService.remove({ key: listKey })
            }, { concurrency: 25 })
        } catch (error) {

        }
    }

    async deletePayment(adminId: string, paymentId: string) {
        try {
            if (!paymentId) {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.PAYMENT_NOT_EXISTS
                })
            }
            const found = await this.paymentModel.findOne({ _id: paymentId, adminId })
            if (!found) {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.PAYMENT_NOT_EXISTS
                })
            }
            let promiseArray = [
                this.paymentModel.findByIdAndDelete(paymentId),
                this.userPaymentService.deleteManyPayment({ paymentId }),
                this.removeCachePolicy(paymentId)
            ]
            if (found.policyId) promiseArray.push(
                this.policyService.deletePolicyById(found.policyId),
            )
            await Promise.all(promiseArray)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async getListPayment(adminId: string, paymentQuery: PaymentQuery) {
        try {
            let { filter, limit, page, search, sort, type } = paymentQuery
            const filterObject: DataFilter<Payment> = {
                limit,
                page,
                condition: { adminId },
                population: [{ path: 'policyId' }],
                selectCols: '-__v -publicKey -secretKey'
            }
            if (sort) {
                filterObject.sort = sort
            }
            if (filter) {
                filterObject.condition = { ...filterObject.condition, ...filter }
            }
            if (type) {
                filterObject.condition = { ...filterObject.condition, type }
            }
            if (search) {
                filterObject.condition['$or'] = [
                    { username: { '$regex': search, '$options': 'i' } },
                    { email: { '$regex': search, '$options': 'i' } }
                ]
            }
            let listPayment: any = await this.getListDocument(filterObject)
            const list: any = await Promise.map(listPayment.data, async (payment) => {
                const [countSeller, amount] = await Promise.all([
                    this.userPaymentService.countPayment({ paymentId: payment._id }),
                    0
                ])
                return {
                    ...payment.toObject(),
                    countSeller,
                    amount
                }
            }, { concurrency: 25 })
            listPayment.data = list
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listPayment
            }
        } catch (error) {
            throw error
        }
    }

    async getPaymentDetail(adminId: string, paymentId: string) {
        try {
            if (!paymentId) {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.PAYMENT_NOT_EXISTS
                })
            }
            const found = await this.paymentModel.findOne({ _id: paymentId, adminId }).populate('policyId')
            if (!found) {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.PAYMENT_NOT_EXISTS
                })
            }
            const { publicKey, secretKey } = found
            let publicKeyHash = ''
            let secretKeyHash = ''
            if (publicKey) publicKeyHash = publicKey.substring(0, 7) + '*'.repeat(publicKey.length > 7 ? publicKey.length - 7 : 7)
            if (secretKey) secretKeyHash = secretKey.substring(0, 7) + '*'.repeat(secretKey.length > 7 ? secretKey.length - 7 : 7)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: {
                    ...found.toObject(),
                    secretKey: secretKeyHash,
                    publicKey: publicKeyHash
                }
            }
        } catch (error) {
            throw error
        }
    }

    async buyerGetListPaymentStore(domain: string) {
        try {
            const cachedResponse = await this.cacheService.get({ key: `${KeyRedisEnum.listPayment}-${domain}` })
            if (cachedResponse && JSON.parse(cachedResponse) && JSON.parse(cachedResponse).data && JSON.parse(cachedResponse).data.length) {
                return JSON.parse(cachedResponse)
            }
            const foundStore = await this.storeService.findStoreBuyer(domain)
            let listStorePayment: any = await this.storePaymentService.findListStorePaymentBuyer(foundStore._id)
            for (let payment of listStorePayment) {
                if (payment?.sellerPaymentId?.paymentId?.secretKey) delete payment.sellerPaymentId.paymentId.secretKey
            }
            const listPayment = listStorePayment.map(storePayment => ({ ...storePayment.sellerPaymentId, isOpenPaypalCard: storePayment.isOpenPaypalCard }))
            const response = {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listPayment
            }
            await this.cacheService.set({ key: `${KeyRedisEnum.listPayment}-${domain}`, value: JSON.stringify(response), expiresIn: 24 * 60 * 60 })
            return response
        } catch (error) {
            throw error
        }
    }

    async buyerGetPolicy(domain: string) {
        try {
            const cachedResponse = await this.cacheService.get({ key: `${KeyRedisEnum.policy}-${domain}` })
            if (cachedResponse && JSON.parse(cachedResponse) && JSON.parse(cachedResponse).data) return JSON.parse(cachedResponse)
            const foundStore = await this.storeService.findStoreBuyer(domain)
            const policy = await this.storePaymentService.getPaypalStorePolicy(foundStore)
            const response = {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: policy
            }
            await this.cacheService.set({ key: `${KeyRedisEnum.policy}-${domain}`, value: JSON.stringify(response), expiresIn: 24 * 60 * 60 })
            return response
        } catch (error) {
            throw error
        }
    }

    async buyerGetSecretKeyStripe(domain: string) {
        try {
            const cachedResponse = await this.cacheService.get({ key: `${KeyRedisEnum.skstripe}-${domain}` })
            if (cachedResponse) return JSON.parse(cachedResponse)
            const foundStore = await this.storeService.findStoreBuyer(domain)
            const payment = await this.storePaymentService.getStripeStore(foundStore._id)
            //@ts-ignore
            const secretKey = payment[0]?.sellerPaymentId?.paymentId?.secretKey || ''
            // const secretKey = "sk_test_51N65klFRqDMGudAjJfk6GsEqfpUqtHGYdH4Q4DeSzBqDQwv2GmEQNvWUcKuDAoNaXc1GrDvlAxyPGMDPax6qYNym00Xo314OrX"
            const response = {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: secretKey
            }
            await this.cacheService.set({ key: `${KeyRedisEnum.skstripe}-${domain}`, value: JSON.stringify(response), expiresIn: 24 * 60 * 60 })
            return response

        } catch (error) {
            throw error
        }
    }

    async adminGetListSellerPayment(adminId: string, paymentId: string, query: BaseQuery) {
        try {
            await this.findPaymentByAdmin(adminId, paymentId)
            return this.userPaymentService.adminGetListSellerPayment(paymentId, query)
        } catch (error) {
            throw error
        }
    }
    async adminAttachPaymentSeller(adminId: string, attachPaymentDto: AttachPaymentDto) {
        try {
            const { paymentId, listSellerId } = attachPaymentDto
            const foundPayment = await this.findPaymentByAdmin(adminId, paymentId)
            const { type } = foundPayment
            attachPaymentDto.type = type
            return this.userPaymentService.adminAttachPaymentSeller(attachPaymentDto)
        } catch (error) {
            throw error
        }
    }

    async findPaymentById(paymentId: string) {
        try {
            const found = await this.paymentModel.findById(paymentId)
            if (!found) {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.PAYMENT_NOT_EXISTS
                })
            }
            return found
        } catch (error) {
            throw error
        }
    }

    async findPaymentByAdmin(adminId: string, paymentId: string) {
        try {
            const found = await this.paymentModel.findOne({ _id: paymentId, adminId })
            if (!found) {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.PAYMENT_NOT_EXISTS
                })
            }
            return found
        } catch (error) {
            throw error
        }
    }

    async getConfigPayment(paymentId: string) {
        try {
            let mode, publicKey, secretKey = ""
            const pay = await this.paymentModel.findOne({ _id: paymentId/* , enabled: true  */ })
            if (pay.mode) mode = pay.mode
            if (pay.publicKey) publicKey = pay.publicKey
            if (pay.secretKey) secretKey = pay.secretKey
            return {
                mode, publicKey, secretKey
            }
        } catch (error) {
            throw error
        }
    }

}
