import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger"
import { IsBoolean, IsNotEmpty, IsNumber, IsOptional, Validate } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsObjectId } from "src/base/custom-validator"
import { PaymentMode, PaymentType } from "./payment.enum"
import { Transform } from "class-transformer"

export const PaymentSchema: Schema = new Schema({
    name: {
        type: String
    },
    type: {
        type: String,
        enum: PaymentType
    },
    mode: {
        type: String,
        enum: PaymentMode,
        default: 'live'
    },
    enabled: {
        type: Boolean,
        default: true
    },
    publicKey: {
        type: String
    },
    secretKey: {
        type: String
    },
    policyId: {
        type: Types.ObjectId,
        ref: 'Policy'
    },
    adminId: {
        type: Types.ObjectId,
        ref: 'User'
    }
}, { ...schemaOptions, collection: 'Payment' })


export class Payment extends Document {
    @ApiProperty()
    @IsNotEmpty()
    name: string

    @ApiProperty()
    @IsNotEmpty()
    type: PaymentType

    @ApiProperty()
    @IsNotEmpty()
    mode: PaymentMode

    @ApiProperty()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsNotEmpty()
    enabled: boolean

    @ApiProperty()
    @IsNotEmpty()
    publicKey: string

    @ApiProperty()
    @IsNotEmpty()
    secretKey: string

    @ApiPropertyOptional()
    @Validate(IsObjectId)
    @IsOptional()
    policyId: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsOptional()
    adminId: string
}
