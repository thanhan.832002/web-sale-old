export enum PaymentType {
    paypal = 'paypal',
    stripe = 'stripe'
}

export enum PaymentMode {
    live = 'live',
    sandbox = 'sandbox'
}