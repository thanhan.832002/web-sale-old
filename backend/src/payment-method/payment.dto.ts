import { ApiProperty, ApiPropertyOptional, PickType } from "@nestjs/swagger";
import { IsBoolean, IsNotEmpty, IsOptional, Validate } from "class-validator";
import { IsObjectId } from "src/base/custom-validator";
import { BaseQuery } from "src/base/interfaces/base-query.interface";
import { Policy } from "src/policy/policy.model";
import { PaymentMode, PaymentType } from "./payment.enum";
import { Payment } from "./payment.model";
import { Transform } from "class-transformer";
export class CreatePaymentDto extends PickType(Payment, ['name', 'type', 'publicKey', 'secretKey']) {
    @ApiPropertyOptional({ type: Policy })
    @IsOptional()
    policy: Policy

    // @Validate(IsObjectId)
    policyId: string
}

export class EditPaymentDto {
    @ApiPropertyOptional()
    @IsOptional()
    name: string

    @ApiPropertyOptional()
    @IsOptional()
    type: PaymentType

    @ApiPropertyOptional()
    @IsOptional()
    mode: PaymentMode

    @ApiPropertyOptional()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsOptional()
    enabled: boolean

    @ApiPropertyOptional()
    @IsOptional()
    publicKey: string

    @ApiPropertyOptional()
    @IsOptional()
    secretKey: string

    @ApiPropertyOptional({ type: Policy })
    @IsOptional()
    policy: Policy
}

export class PaymentQuery extends BaseQuery {
    @ApiPropertyOptional()
    @IsOptional()
    type: PaymentType
}