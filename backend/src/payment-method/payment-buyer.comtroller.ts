import { Controller, Get, Post, Put, Delete, Param, Body, Query, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { GetDomain } from 'src/base/decorator/get-domain.decorator';
import { PaymentService } from './payment.service';

@Controller('payment-buyer')
@ApiTags('Payment Buyer')
export class PaymentBuyerController {
    constructor(
        private readonly paymentService: PaymentService
    ) { }

    @Get('policy/:domain')
    buyerGetPoilcy(@Param('domain') domain: string) {
        return this.paymentService.buyerGetPolicy(domain)
    }

    @Get('list/:domain')
    buyerGetListPaymentStore(@Param('domain') domain: string) {
        return this.paymentService.buyerGetListPaymentStore(domain)
    }

    @Get('sk-stripe/:domain')
    buyerGetSecretKeyStripe(@Param('domain') domain: string) {
        return this.paymentService.buyerGetSecretKeyStripe(domain)
    }

}
