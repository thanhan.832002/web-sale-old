import { ApiProperty, ApiPropertyOptional, PickType } from "@nestjs/swagger";
import { IsArray, IsBoolean, IsInt, IsNotEmpty, IsNumber, IsOptional, Min, Validate, ValidateNested } from "class-validator";
import { IsObjectId } from "src/base/custom-validator";
import { Coupon, DiscountEnum, Requirement } from "./coupon.model";
import { Transform, Type } from "class-transformer";

export class CreateCouponDto extends PickType(Coupon, ['code', 'status', 'discount', 'minAmount', 'minQuantity', 'discountType', 'dateFrom', 'dateTo']) {
    sellerId: string
}

export class CreateCouponStoreDto extends PickType(Coupon, ['code', 'status', 'discount', 'minAmount', 'minQuantity', 'discountType', 'dateFrom', 'dateTo', 'multipleReq']) {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    storeId: string

    sellerId: string
}
export class DeleteListCouponDto {
    @ApiProperty(
        { type: [String] }
    )
    @IsNotEmpty()
    listCouponId: string[]
}

export interface CoutUseCoupon {
    userId: string
    couponId?: string
}

export class UpdateCouponDto {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    couponId: string

    @ApiPropertyOptional()
    @IsOptional()
    code: string
    @ApiPropertyOptional()
    @IsOptional()
    discount: number
    @ApiPropertyOptional()
    @IsOptional()
    discountType: DiscountEnum
    @ApiPropertyOptional()
    @Type(() => Number)
    @IsOptional()
    minAmount: number
    @ApiPropertyOptional()
    @IsInt()
    @Min(0)
    @Type(() => Number)
    @IsOptional()
    minQuantity: number
    @ApiPropertyOptional()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsOptional()
    status: boolean
    @ApiPropertyOptional()
    @IsOptional()
    dateFrom: number
    @ApiPropertyOptional()
    @IsOptional()
    dateTo: number
}

export class UpdateCouponStoreDto extends UpdateCouponDto {
    @ApiPropertyOptional({ type: [Requirement] })
    @ValidateNested()
    @Type(() => Requirement)
    @IsArray()
    @IsOptional()
    multipleReq: Requirement[]
}

export class GetAutoAppyDto {
    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsNotEmpty()
    total: number

    @ApiProperty()
    @Min(0)
    @IsInt()
    @Type(() => Number)
    @IsNotEmpty()
    quantity: number

    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsNotEmpty()
    shipping: number

    @ApiPropertyOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsOptional()
    slug?: string
}