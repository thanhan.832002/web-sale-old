import { Controller, Get, Param, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { GetAutoAppyDto } from './coupon.dto';
import { CouponService } from './coupon.service';
import { GetDomain } from 'src/base/decorator/get-domain.decorator';
import { CouponQuery, SlugQuery } from 'src/base/interfaces/base-query.interface';

@Controller('coupon')
@ApiTags('Coupon')

export class CouponController {
    constructor(
        private readonly couponService: CouponService
    ) { }

    @Get('code/:couponCode')
    getCouponCode(
        @GetDomain() domain: string,
        @Query() { slug }: CouponQuery,
        @Param('couponCode') couponCode: string) {
        if (slug) domain = slug + '.' + domain
        return this.couponService.findCouponByCode(domain, couponCode)
    }

    @Get('/auto-apply')
    getCouponAutoApply(
        @GetDomain() domain: string,
        @Query() getAutoAppyDto: GetAutoAppyDto
    ) {
        const { slug } = getAutoAppyDto
        if (slug) domain = slug + '.' + domain
        return this.couponService.getCouponAutoApply(domain, getAutoAppyDto)
    }
}