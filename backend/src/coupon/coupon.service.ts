import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Response } from 'express';
import { Model } from 'mongoose';
import { BaseService } from 'src/base/base.service';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { DataFilter } from 'src/base/interfaces/data.interface';
import BaseNoti from 'src/base/notify';
import { CoutUseCoupon, CreateCouponDto, CreateCouponStoreDto, DeleteListCouponDto, GetAutoAppyDto, UpdateCouponDto, UpdateCouponStoreDto } from './coupon.dto';
import { Coupon, CouponTypeEnum, DiscountEnum } from './coupon.model';
import * as moment from 'moment'
import { Timeout } from '@nestjs/schedule';
import { StoreService } from 'src/store/store.service';
import { Store } from 'src/store/store.model';

@Injectable()
export class CouponService extends BaseService<Coupon>{
    constructor(
        @InjectModel(Coupon.name) private readonly couponModel: Model<Coupon>,
        private readonly storeService: StoreService
    ) { super(couponModel) }

    async sellerGetListCoupon(sellerId: string, query: BaseQuery) {
        try {
            let { page, limit, search, filter, sort } = query
            let filterObject: DataFilter<Partial<Coupon>> = {
                limit,
                page,
                condition: {
                    type: CouponTypeEnum.nomal,
                    sellerId,
                    storeId: { $exists: false }
                },
                population: [],
                selectCols: '-__v -type'
            }
            if (filter) {
                filterObject.condition = { ...filterObject.condition, ...filter }
            }
            if (sort) {
                filterObject.sort = sort
            }
            const list = await this.getListDocument(filterObject)
            return {
                message: BaseNoti.SUCCESS,
                data: list
            }
        } catch (error) {
            throw error
        }
    }

    async createCoupon(sellerId: string, createCouponDto: CreateCouponDto) {
        try {
            const { code } = createCouponDto
            await this.checkCodeExists(code, { sellerId })
            createCouponDto.sellerId = sellerId
            const coupon = await this.genarateCoupon()
            const newCouponIns = new this.couponModel({
                code: coupon,
                ...createCouponDto,
                type: CouponTypeEnum.nomal
            })
            await newCouponIns.save()
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: newCouponIns
            }
        } catch (error) {
            throw error
        }
    }

    async createCouponStore(sellerId: string, createCouponDto: CreateCouponStoreDto) {
        try {
            const { code, storeId, multipleReq } = createCouponDto
            const existsCoupon = await this.couponModel.exists({ storeId, sellerId })
            if (existsCoupon) {
                throw new BaseApiException({
                    message: BaseNoti.COUPON.STORE_ALREADY_HAVE_DISCOUNTCODE
                })
            }
            await this.checkCodeExists(code, { sellerId })
            createCouponDto.sellerId = sellerId
            const coupon = await this.genarateCoupon()
            const newCouponIns = new this.couponModel({
                code: coupon,
                ...createCouponDto,
                type: CouponTypeEnum.nomal
            })
            await newCouponIns.save()
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: newCouponIns
            }
        } catch (error) {
            throw error
        }
    }

    async updateCoupon(sellerId: string, updateCouponDto: UpdateCouponDto) {
        try {
            const { code, couponId, dateFrom, dateTo, minAmount, minQuantity } = updateCouponDto
            let update: any = { ...updateCouponDto }
            const found = await this.couponModel.findOne({ _id: couponId, sellerId })
            if (!found) {
                throw new BaseApiException({
                    message: BaseNoti.COUPON.COUPON_NOT_EXISTS
                })
            }
            if (code) {
                await this.checkCodeExists(code, { couponId, sellerId })
            }
            if (dateFrom) {
                update.dateFrom = dateFrom
            }
            if (minAmount != undefined) {
                update.minAmount = minAmount
            }
            if (minQuantity != undefined) {
                update.minQuantity = minQuantity
            }
            if (!dateTo) {
                update.$unset = { ...update.$unset, dateTo: 1 }
            }
            const coupon = await this.couponModel.findByIdAndUpdate(couponId, update, { new: true }).select('-__v -type')
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: coupon
            }

        } catch (error) {
            throw error
        }
    }

    async updateCouponStore(sellerId: string, storeId: string, updateCouponDto: UpdateCouponStoreDto) {
        try {
            const { code, couponId, dateFrom, dateTo, minAmount, minQuantity, multipleReq } = updateCouponDto
            let update: any = { ...updateCouponDto }
            const found = await this.couponModel.findOne({ _id: couponId, sellerId, storeId })
            if (!found) {
                throw new BaseApiException({
                    message: BaseNoti.COUPON.COUPON_NOT_EXISTS
                })
            }
            if (code) {
                await this.checkCodeExists(code, { couponId, sellerId })
            }
            if (dateFrom) {
                update.dateFrom = dateFrom
            }
            if (minAmount != undefined) {
                update.minAmount = minAmount
            }
            if (minQuantity != undefined) {
                update.minQuantity = minQuantity
            }
            if (!dateTo) {
                update.$unset = { ...update.$unset, dateTo: 1 }
                delete update.dateTo
            }
            const coupon = await this.couponModel.findByIdAndUpdate(couponId, update, { new: true }).select('-__v -type')
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: coupon
            }

        } catch (error) {
            throw error
        }
    }

    async updateCouponAbando(couponId: string, coupon: Partial<Coupon>) {
        try {
            const { code } = coupon
            const exists = await this.couponModel.exists({ type: CouponTypeEnum.abandoned, code, _id: { $ne: couponId } })
            await this.couponModel.findByIdAndUpdate(couponId, coupon)
        } catch (error) {
            throw error
        }
    }

    async deleteListCoupon(sellerId: string, { listCouponId }: DeleteListCouponDto) {
        try {
            await this.couponModel.deleteMany({ sellerId, _id: { $in: listCouponId } })
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async getCouponByCode(sellerId: string, code: string, total: number, totalQuantity: number, shipping: number) {
        try {
            let found: any = await this.couponModel.findOne({
                code,
                status: true,
                $or: [
                    { type: CouponTypeEnum.abandoned, sellerId: { $exists: false } },
                    { type: CouponTypeEnum.nomal, sellerId }
                ]
            })
            if (!found) {
                throw new BaseApiException({
                    message: BaseNoti.COUPON.COUPON_NOT_EXISTS
                })
            }
            const { dateFrom, dateTo, minAmount, minQuantity, multipleReq } = found
            if (dateFrom || dateTo) {
                const now = moment().unix()
                if (dateFrom) {
                    if (now < dateFrom) {
                        return
                    }
                }
                if (dateTo) {
                    if (now > dateTo) {
                        return
                    }
                }
            }
            let maxDiscount: any = { discountPriceReq: 0 }
            if (multipleReq && multipleReq.length) {
                for (let requirement of multipleReq) {
                    if (!requirement.status) continue
                    if (requirement.minAmount && requirement.minAmount > total) continue
                    if (requirement.minQuantity && requirement.minQuantity > totalQuantity) continue
                    let discountPriceReq = 0
                    if (requirement.discountType == DiscountEnum.percent) {
                        discountPriceReq = total * requirement.discount / 100
                    } else if (requirement.discountType == DiscountEnum.value) {
                        discountPriceReq = total - requirement.discount < 0 ? total : requirement.discount
                    } else {
                        discountPriceReq = shipping
                    }
                    if (discountPriceReq > maxDiscount.discountPriceReq) {
                        maxDiscount = {
                            discount: requirement.discount,
                            discountType: requirement.discountType,
                            discountPriceReq
                        }
                    }
                }
            }
            if (minAmount && minAmount > total && !maxDiscount.discountPriceReq || minQuantity && minQuantity > totalQuantity && !maxDiscount.discountPriceReq) return
            // if (minQuantity && minQuantity > totalQuantity && !maxDiscount.discountPriceReq) return
            // if (minAmount && minAmount > total && minQuantity && minQuantity > totalQuantity && maxDiscount.discountPriceReq) return
            let discountPrice = 0
            if (found.discountType == DiscountEnum.percent) {
                discountPrice = total * found.discount / 100
            } else if (found.discountType == DiscountEnum.value) {
                discountPrice = total - found.discount < 0 ? total : found.discount
            } else if (found.discountType == DiscountEnum.freeshipping) {
                discountPrice = shipping
            }
            if (maxDiscount && maxDiscount.discountPriceReq && maxDiscount.discountPriceReq > discountPrice) {
                found = { ...found.toObject(), ...maxDiscount }
            }
            return found
        } catch (error) {
            throw error
        }
    }

    async sellerGetDetailCoupon(sellerId: string, couponId: string) {
        try {
            const found = await this.couponModel.findOne({ _id: couponId, sellerId }).populate([
            ])
            if (!found) {
                throw new BaseApiException({
                    message: BaseNoti.COUPON.COUPON_NOT_EXISTS
                })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: found
            }
        } catch (error) {
            throw error
        }
    }

    async getStoreCoupon(sellerId: string, storeId: string) {
        try {
            const found = await this.couponModel.findOne({ sellerId, storeId })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: found
            }
        } catch (error) {
            throw error
        }
    }

    async findCouponByCode(domain: string, code: string) {
        try {
            code = code.trim()
            const foundCoupon = await this.couponModel.findOne({ code, status: true })
            if (!foundCoupon) {
                throw new BaseApiException({
                    message: BaseNoti.COUPON.COUPON_NOT_EXISTS
                })
            }
            const { type, storeId } = foundCoupon
            const foundStore = await this.storeService.findStore({ domain })
            if (type == CouponTypeEnum.abandoned) {
                return {
                    message: BaseNoti.GLOBAL.SUCCESS,
                    data: foundCoupon
                }
            }
            if (storeId.toString() != foundStore._id.toString()) {
                throw new BaseApiException({
                    message: BaseNoti.COUPON.COUPON_NOT_EXISTS
                })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: foundCoupon
            }
        } catch (error) {
            throw error
        }
    }

    async getStoreCounpon(storeId: string) {
        try {
            const now = moment().unix()
            const foundCoupon = await this.couponModel.findOne({
                storeId, status: true, dateFrom: { $lte: now }, $or: [
                    { dateTo: { $exists: true, $gte: now } },
                    { dateTo: { $exists: false } }],
                type: CouponTypeEnum.nomal
            })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: foundCoupon
            }
        } catch (error) {
            throw error
        }
    }

    async getCouponAutoApply(domain: string, getAutoAppyDto: GetAutoAppyDto) {
        try {
            const foundStore = await this.storeService.findStore({ domain })
            const foundCoupon = await this.getAutoApply(foundStore, getAutoAppyDto)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: foundCoupon
            }
        } catch (error) {
            throw error
        }
    }

    async getAutoApply(store: Store, { total, quantity, shipping }: GetAutoAppyDto) {
        try {
            const { sellerId } = store
            const now = moment().unix()
            const { buyMore } = store
            let foundCoupon: any = await this.couponModel.findOne({
                status: true, storeId: store._id, sellerId, dateFrom: { $lte: now }, $or: [
                    { dateTo: { $exists: true, $gte: now } },
                    { dateTo: { $exists: false } },
                ],
                type: CouponTypeEnum.nomal
            }).lean()
            if (!foundCoupon && !buyMore?.listBuyMore?.length) return
            // const { discount, discountType, minAmount, minQuantity, multipleReq } = foundCoupon
            let maxDiscount: any = { discountPriceReq: 0 }
            // if (minAmount && minAmount > total || minQuantity && minQuantity > quantity) return
            if (foundCoupon && foundCoupon.multipleReq && foundCoupon.multipleReq.length) {
                for (let requirement of foundCoupon.multipleReq) {
                    if (!requirement.status) continue
                    if (requirement.minAmount && requirement.minAmount > total /* && requirement.minQuantity && requirement.minQuantity > quantity */) continue
                    if (requirement.minQuantity && requirement.minQuantity > quantity) continue
                    let discountPriceReq = 0
                    if (requirement.discountType == DiscountEnum.percent) {
                        discountPriceReq = total * requirement.discount / 100
                    } else if (requirement.discountType == DiscountEnum.value) {
                        discountPriceReq = total - requirement.discount < 0 ? total : requirement.discount
                    } else {
                        discountPriceReq = shipping
                    }
                    if (discountPriceReq > maxDiscount.discountPriceReq) {
                        maxDiscount = {
                            discount: requirement.discount,
                            discountType: requirement.discountType,
                            discountPriceReq
                        }
                    }
                }
            }
            if (buyMore?.listBuyMore?.length) {
                for (let requirement of buyMore?.listBuyMore) {
                    if (requirement.quantity && requirement.quantity > quantity) continue
                    let discountPriceReq = 0
                    if (requirement.content == DiscountEnum.percent) {
                        discountPriceReq = total * requirement.discount / 100
                    } else if (requirement.content == DiscountEnum.value) {
                        discountPriceReq = total - requirement.discount < 0 ? total : requirement.discount
                    } else {
                        discountPriceReq = shipping
                    }
                    if (discountPriceReq > maxDiscount.discountPriceReq) {
                        maxDiscount = {
                            discount: requirement.discount,
                            discountType: requirement.content,
                            discountPriceReq
                        }
                    }
                }
            }
            if (foundCoupon && foundCoupon.minAmount && foundCoupon.minAmount > total && !maxDiscount.discountPriceReq || foundCoupon && foundCoupon.minQuantity && foundCoupon.minQuantity > quantity && !maxDiscount.discountPriceReq) return
            let discountPrice = 0
            if (foundCoupon && foundCoupon.minAmount && foundCoupon.minAmount <= total || foundCoupon && foundCoupon.minQuantity && foundCoupon.minQuantity <= quantity) {
                if (foundCoupon.discountType == DiscountEnum.percent) {
                    discountPrice = total * foundCoupon.discount / 100
                } else if (foundCoupon.discountType == DiscountEnum.value) {
                    discountPrice = total - foundCoupon.discount < 0 ? total : foundCoupon.discount
                } else if (foundCoupon.discountType == DiscountEnum.freeshipping) {
                    discountPrice = shipping
                }
            }
            if (maxDiscount && maxDiscount.discountPriceReq && maxDiscount.discountPriceReq > discountPrice) {
                foundCoupon = { ...foundCoupon, ...maxDiscount }
            }
            return foundCoupon
        } catch (error) {
            throw error
        }
    }

    async checkCodeExists(code: string, { couponId, sellerId }: { couponId?: string, sellerId?: string }) {
        try {
            const condition: any = { code }
            if (couponId) {
                condition._id = { $ne: couponId }
            }
            if (sellerId) {
                condition.sellerId = sellerId
            }
            const coupon = await this.couponModel.findOne(condition)
            if (coupon) {
                throw new BaseApiException({
                    message: BaseNoti.COUPON.CODE_ALREADY_EXISTS
                })
            }
        } catch (error) {
            throw error
        }
    }

    async createCouponAbandoned(createCouponDto: Partial<Coupon>) {
        try {
            const { code } = createCouponDto
            const existsCode = await this.couponModel.exists({ type: CouponTypeEnum.abandoned, code })
            if (existsCode) {
                throw new BaseApiException({
                    message: BaseNoti.COUPON.CODE_ALREADY_EXISTS
                })
            }
            const newCouponIns = new this.couponModel({
                ...createCouponDto
            })
            await newCouponIns.save()
            return newCouponIns
        } catch (error) {
            throw error
        }
    }

    async findByIdAndUpdate(couponId: string, updateField: any) {
        try {
            const updatedCoupon = await this.couponModel.findByIdAndUpdate(couponId, updateField, { new: true })
            return updatedCoupon
        } catch (error) {
            throw error
        }
    }

    async updateMany(filter: any, updateField: any) {
        try {
            const updatedCoupon = await this.couponModel.updateMany(filter, updateField, { new: true })
            return updatedCoupon
        } catch (error) {
            throw error
        }
    }

    async findByIdAndDelete(couponId: string) {
        try {
            await this.couponModel.findByIdAndDelete(couponId)
        } catch (error) {
            throw error
        }
    }

    async deleteMany(listCouponId: string[]) {
        try {
            await this.couponModel.deleteMany({ _id: { $in: listCouponId } })
        } catch (error) {
            throw error
        }
    }

    genarateCoupon = async () => {
        while (true) {
            let code = this.craeteCode();
            const isExists = await this.couponModel.exists({ code: code })
            if (!isExists) {
                return code
            }
        }
    }

    craeteCode = () => {
        const alphabet = '0123456789ABCDEFGHIJKLMNOPKRSTUVWXYZ'
        let code = "";
        for (let i = 0; i < 10; i++) {
            let index = Math.floor(Math.random() * alphabet.length);
            let randomchar = alphabet[index]
            code = code + randomchar;
        }
        return code;
    };

}
