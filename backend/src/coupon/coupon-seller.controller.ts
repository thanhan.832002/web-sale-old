import { Body, Controller, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { CreateCouponDto, CreateCouponStoreDto, DeleteListCouponDto, UpdateCouponDto, UpdateCouponStoreDto } from './coupon.dto';
import { CouponService } from './coupon.service';
import { SellerGuard } from 'src/base/guard/seller.guard';

@Controller('discount')
@ApiTags('Discount')

export class CouponSellerController {
    constructor(
        private readonly couponService: CouponService
    ) { }

    @Get('detail/:discountId')
    @UseGuards(SellerGuard)
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    sellerGetDetailCoupon(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('discountId') discountId: string
    ) {
        return this.couponService.sellerGetDetailCoupon(userId, discountId)
    }

    @Get('list')
    @UseGuards(SellerGuard)
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    adminGetListCoupon(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: BaseQuery) {
        return this.couponService.sellerGetListCoupon(userId, query)
    }

    @Post('create')
    @UseGuards(SellerGuard)
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    adminCreateCoupon(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() createCouponDto: CreateCouponDto
    ) {
        return this.couponService.createCoupon(userId, createCouponDto)
    }

    @Put('update')
    @UseGuards(SellerGuard)
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    adminUpdateCoupon(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() updateCouponDto: UpdateCouponDto
    ) {
        return this.couponService.updateCoupon(userId, updateCouponDto)
    }

    @Post('delete-list')
    @UseGuards(SellerGuard)
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    adminDeleteListCounpon(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() deleteListCouponDto: DeleteListCouponDto
    ) {
        return this.couponService.deleteListCoupon(userId, deleteListCouponDto)
    }

    @Get('store/:storeId')
    @UseGuards(SellerGuard)
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    getCouponStore(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('storeId') storeId: string
    ) {
        return this.couponService.getStoreCoupon(userId, storeId)
    }

    @Post('store/create')
    @UseGuards(SellerGuard)
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    createCouponStore(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() createCouponDto: CreateCouponStoreDto
    ) {
        return this.couponService.createCouponStore(userId, createCouponDto)
    }

    @Put('store/update/:storeId')
    @UseGuards(SellerGuard)
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    updateCouponStore(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('storeId') storeId: string,
        @Body() updateCouponDto: UpdateCouponStoreDto
    ) {
        return this.couponService.updateCouponStore(userId, storeId, updateCouponDto)
    }
}
