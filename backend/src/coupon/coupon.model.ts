import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsArray, IsNotEmpty, IsOptional, Validate, ValidateNested } from "class-validator";
import { Schema, Document, Types } from "mongoose";
import { schemaOptions } from "src/base/base.shema";
import { IsObjectId } from "src/base/custom-validator";

export enum DiscountEnum {
    percent = 1,
    value,
    freeshipping
}

export enum CouponTypeEnum {
    abandoned = 'abandoned',
    nomal = 'normal'
}

export const RequirementSchema: Schema = new Schema({
    discount: Number,
    status: {
        type: Boolean,
        default: true
    },
    discountType: {
        type: Number,
        enum: DiscountEnum,
        default: 1
    },
    minAmount: Number,
    minQuantity: Number
})

export const CouponSchema: Schema = new Schema({
    sellerId: {
        type: Types.ObjectId,
        ref: 'User',
        index: true
    },
    storeId: {
        type: Types.ObjectId,
        ref: 'Store',
        index: true
    },
    adminId: {
        type: Types.ObjectId,
        ref: 'User',
        index: true
    },
    type: {
        type: String,
        default: CouponTypeEnum.nomal
    },
    code: {
        type: String,
        index: true
    },
    discount: Number,
    status: {
        type: Boolean,
        default: true
    },
    discountType: {
        type: Number,
        enum: DiscountEnum,
        default: 1
    },
    minAmount: Number,
    minQuantity: Number,
    dateFrom: {
        type: Number
    },
    dateTo: {
        type: Number
    },
    multipleReq: {
        type: [RequirementSchema]
    }
}, { ...schemaOptions, ... { collection: 'Coupon' } })

export class Requirement {
    @ApiPropertyOptional()
    @IsOptional()
    discount: number
    @ApiProperty()
    @IsNotEmpty()
    status: boolean
    @ApiProperty()
    @IsNotEmpty()
    discountType: number
    @ApiPropertyOptional()
    @IsOptional()
    minAmount: number
    @ApiPropertyOptional()
    @IsOptional()
    minQuantity: number
}

export class Coupon extends Document {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    sellerId: string
    @ApiPropertyOptional()
    @Validate(IsObjectId)
    @IsOptional()
    storeId: string
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    adminId: string
    @ApiProperty()
    @IsNotEmpty()
    type: CouponTypeEnum
    @ApiProperty()
    @IsNotEmpty()
    code: string
    @ApiProperty()
    @IsOptional()
    discount: number
    @ApiProperty()
    @IsNotEmpty()
    status: boolean
    @ApiProperty()
    @IsNotEmpty()
    discountType: number
    @ApiProperty()
    @IsNotEmpty()
    minAmount: number
    @ApiProperty()
    @IsNotEmpty()
    minQuantity: number
    @ApiPropertyOptional()
    @IsOptional()
    dateFrom: number
    @ApiPropertyOptional()
    @IsOptional()
    dateTo: number

    @ApiPropertyOptional({ type: [Requirement] })
    @ValidateNested()
    @Type(() => Requirement)
    @IsArray()
    @IsOptional()
    multipleReq: Requirement[]
}

