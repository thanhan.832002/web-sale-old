import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Coupon, CouponSchema } from './coupon.model';
import { CouponService } from './coupon.service';
import { CouponController } from './coupon.controller';
import { CouponSellerController } from './coupon-seller.controller';
import { StoreModule } from 'src/store/store.module';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Coupon.name, schema: CouponSchema }]),
        StoreModule
    ],
    providers: [CouponService],
    controllers: [CouponController, CouponSellerController],
    exports: [CouponService]
})
export class CouponModule { }