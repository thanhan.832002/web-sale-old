import { Controller, Get, Post, Put, Req, Param, Body, Query, Ip } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ProductBuyerService } from './product-buyer.service';
import { GetDomain } from 'src/base/decorator/get-domain.decorator';
import { UtmQuery } from './product-buyer.query';
import { CountViewDto } from './product-buyer.dto';

@Controller('product-buyer')
@ApiTags('Product Buyer')
export class ProductBuyerController {
    constructor(
        private readonly productBuyerService: ProductBuyerService) { }

    @Get('detail/:domain')
    getDetailProduct(
        @Param('domain') domain: string
    ) {
        return this.productBuyerService.getDetailProduct(domain)
    }

    @Get('detail-product')
    countViewProduct(
        @Query() countViewDto: CountViewDto,
        @Ip() ip: string,
    ) {
        return this.productBuyerService.countViewProduct(countViewDto, ip)
    }

    @Get('checkout')
    countGoCheckout(
        @Query() countViewDto: CountViewDto,
        @Ip() ip: string,
    ) {
        return this.productBuyerService.countGoCheckout(countViewDto, ip)
    }

    @Get('meta/:domain')
    getMeta(@Param('domain') domain: string) {
        return this.productBuyerService.getMeta(domain)
    }


}
