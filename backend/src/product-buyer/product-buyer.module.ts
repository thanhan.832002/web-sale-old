import { Module } from '@nestjs/common';
import { ProductBuyerController } from './product-buyer.controller';
import { ProductBuyerService } from './product-buyer.service';
import { ProductSellerModule } from 'src/product-seller/product-seller.module';
import { VariantSellerModule } from 'src/variant-seller/variant-seller.module';
import { StoreModule } from 'src/store/store.module';
import { ReviewModule } from 'src/review/review.module';
import { ViewModule } from 'src/view/view.module';
import { SharedModule } from 'src/base/shared.module';

@Module({
    imports: [
        ProductSellerModule,
        VariantSellerModule,
        StoreModule,
        ReviewModule,
        ViewModule,
        SharedModule
    ],
    controllers: [ProductBuyerController],
    providers: [ProductBuyerService],
    exports: [ProductBuyerService]
})
export class ProductBuyerModule { }
