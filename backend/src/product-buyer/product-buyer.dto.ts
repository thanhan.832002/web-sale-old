import { IsNotEmpty } from "class-validator";
import { UtmQuery } from "./product-buyer.query";
import { ApiProperty } from "@nestjs/swagger";
import { Transform } from "class-transformer";

export class CountViewDto extends UtmQuery {
    @ApiProperty()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsNotEmpty()
    storeId: string

    @ApiProperty()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsNotEmpty()
    sessionId: string

    @ApiProperty()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsNotEmpty()
    adminId: string
}