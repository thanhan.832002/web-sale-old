import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger"
import { Transform } from "class-transformer"
import { IsNotEmpty, IsOptional } from "class-validator"

export class UtmQuery {
    @ApiPropertyOptional()
    @IsOptional()
    utm_source: string

    @ApiPropertyOptional()
    @IsOptional()
    utm_medium: string

    @ApiPropertyOptional()
    @IsOptional()
    utm_content: string

    @ApiPropertyOptional()
    @IsOptional()
    utm_campaign: string
}