import { Injectable } from '@nestjs/common';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';
import { ProductSellerService } from 'src/product-seller/product-seller.service';
import { ReviewService } from 'src/review/review.service';
import { StoreStatus } from 'src/store/store.enum';
import { StoreService } from 'src/store/store.service';
import { VariantSellerService } from 'src/variant-seller/variant-seller.service';
import { ViewService } from 'src/view/view.service';
import { UtmQuery } from './product-buyer.query';
import { CountViewDto } from './product-buyer.dto';
import { CacheService } from 'src/base/caching/cache.service';
import { KeyRedisEnum } from 'src/view/view.enum';

@Injectable()
export class ProductBuyerService {
    constructor(
        private readonly productSellerService: ProductSellerService,
        private readonly variantSellerService: VariantSellerService,
        private readonly storeService: StoreService,
        private readonly reviewService: ReviewService,
        private readonly cacheService: CacheService,
        private readonly viewService: ViewService
    ) { }

    async getDetailProduct(domain: string) {
        try {
            const resCached = await this.cacheService.get({ key: `${KeyRedisEnum.productBuyer}-${domain}` })
            if (resCached) {
                return JSON.parse(resCached)
            }
            const foundStore = await this.storeService.findStoreBuyer(domain)
            const { productSellerId, adminId } = foundStore
            const foundProduct = await this.productSellerService.findProductSeller(productSellerId)
            if (!foundProduct) {
                throw new BaseApiException({
                    message: BaseNoti.PRODUCT.PRODUCT_NOT_EXISTS
                })
            }
            const reviews = await this.reviewService.findListReviewByProductSellerId(productSellerId)
            const res = {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: { ...foundProduct, reviews, adminId }
            }
            await this.cacheService.set({ key: `${KeyRedisEnum.productBuyer}-${domain}`, value: JSON.stringify(res), expiresIn: 30 * 24 * 60 * 60 })
            return res
        } catch (error) {
            throw error
        }
    }

    async getMeta(domain: string) {
        try {
            const foundStore = await this.storeService.findStoreBuyer(domain)
            const { productSellerId, adminId } = foundStore
            const { imageUrl, productName } = await this.productSellerService.getCoverAndProductName(productSellerId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: { imageUrl, productName }
            }
        } catch (error) {
            console.log('domain :>> ', domain);
            throw error
        }
    }

    async countViewProduct(countViewDto: CountViewDto, ip: string) {
        try {
            await this.viewService.countView(countViewDto, ip)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async countGoCheckout({ adminId, storeId, utm_campaign, utm_content, utm_medium, utm_source, sessionId }: CountViewDto, ip: string) {
        try {
            await this.viewService.countGoCheckout(adminId, storeId, 1, { utm_campaign, utm_content, utm_medium, utm_source, sessionId }, ip)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

}
