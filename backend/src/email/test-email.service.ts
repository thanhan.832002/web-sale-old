import { Injectable, OnModuleInit } from '@nestjs/common';
import { ISendMail } from './sendMail.interface';
import { join, dirname } from 'path'
import { readFileSync } from 'fs'
import * as moment from 'moment'
import { Order } from 'src/order/order.model';
import { Store } from 'src/store/store.model';
import { StoreSettingService } from 'src/store-setting/store-setting.service';
import { Timeout } from '@nestjs/schedule';
import { SelectedProductSellerService } from 'src/selected-product-seller/selected-product-seller.service';
import { StoreService } from 'src/store/store.service';
import { SendAbandoned } from 'src/do-send-abandoned/send-abandoned.model';
import { Coupon, DiscountEnum } from 'src/coupon/coupon.model';
import { orderTest } from './orderTest';
import BaseNoti from 'src/base/notify';
import { ProductService } from 'src/product/product.service';
import { AdminSettingService } from 'src/admin-setting/admin-setting.service';
import { DomainService } from 'src/domain/domain.service';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import { AbandonedCheckoutService } from 'src/abandoned-checkout/abandoned-checkout.service';
var momentTz = require("moment-timezone");
const client = require('@sendgrid/mail')


@Injectable()
export class TestEmailService {
    constructor(
        private readonly adminSettingService: AdminSettingService,
        private readonly storeService: StoreService,
        private readonly productService: ProductService,
        private readonly domainService: DomainService,
        private readonly abandonedCheckoutService: AbandonedCheckoutService
    ) { }

    url = 'https://api.opensitex.com/'
    trackingUrl = 'https://youtube.com'

    async emailConfirmOrder(adminId: string, email: string) {
        try {
            const order = orderTest
            const found = await this.domainService.findOne({ filter: { enabled: true } })
            if (!found) {
                throw new BaseApiException({
                    message: `NOT_FOUND_EMAIL_SENDER`
                })
            }
            const storeName = found.domain.charAt(0).toUpperCase() + found.domain.slice(1)
            const supportEmail = found.supportEmail
            const pathEmail = join(__dirname, '../../../backend/public/orderConfirm.html')
            let emailTemplate = readFileSync(pathEmail, 'utf-8')
            const { shippingAddress, /* total, subTotal, discount, shipping,  */storeId, orderCode, listSelectedProductId } = order
            const { firstName, lastName, address, city, state, country, phoneNumber } = shippingAddress
            const logo = '/logo/logozshop.png'
            emailTemplate = emailTemplate.replace('{{logo}}', this.url + logo)
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName || 'Shop')
            emailTemplate = emailTemplate.replace('{{supportEmail}}', supportEmail)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{firstName}}', firstName)
            emailTemplate = emailTemplate.replace('{{orderCode}}', orderCode)

            emailTemplate = emailTemplate.replace('{{shippingName}}', `${firstName} ${lastName}`)
            emailTemplate = emailTemplate.replace('{{shippingAddress}}', `${address},${city ? ' ' + city : ''},${state ? ' ' + state : ''},${country ? ' ' + country : ''} `)
            emailTemplate = emailTemplate.replace('{{billName}}', `${firstName} ${lastName}`)
            emailTemplate = emailTemplate.replace('{{billAddress}}', `${address},${city ? ' ' + city : ''},${state ? ' ' + state : ''},${country ? ' ' + country : ''} `)
            const listSelectedProduct: any = await this.productService.findTestMail()
            let listProduct = ''
            let total = 0
            let subTotal = 0
            const discount = 0
            const shipping = 5.99
            for (const product of listSelectedProduct) {
                const variant = product.listVariant[0]
                const { price, comparePrice, imageId, listValue } = variant
                total = price
                subTotal = price
                const quantity = 1
                let oneProduct = `
                <tr>
                <td style="vertical-align:middle" align="left" width="17%"><a
                        style="display:block" title="" href="#" target="_blank">
                        <img style="display:block;font-family:Arial;width:80px; margin-top:8px"
                            src="${this.url}${imageId.imageUrl}"
                            alt="" width="80" border="0">
                    </a>
                </td>
                <td style="padding:0 20px 0 20px;vertical-align:top" align="left"
                    width="83%">
                    <p
                        style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif; margin-top:8px">
                        <a style="text-decoration:none" title="ErgoStool" href=""
                            rel="noopener"
                            target="_blank">${product.productName}</a>
                    </p>
                    {{variant}}
                    <p
                        style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">
                        <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$${price}</span>
                        &nbsp;x&nbsp; <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">${quantity}</span>
                    </p>
                </td>
                <td
                    style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">
                    $${(price * quantity).toFixed(2)}
                </td> 
                `
                if (listValue) {
                    let oneVariant = ''
                    if (listValue.length) {
                        let variant = listValue.join('/ ')
                        oneVariant = `
                        <p
                                style="font-size:15px;line-height:26px;color:#555;margin:4px 0px;font-family:sans-serif;">
                                ${variant}
                            </p>
                        `
                    }
                    oneProduct = oneProduct.replace('{{variant}}', oneVariant)
                } else {
                    oneProduct = oneProduct.replace('{{variant}}', '')
                }
                listProduct += oneProduct
            }
            total = total + shipping
            emailTemplate = emailTemplate.replace('{{subTotal}}', subTotal.toFixed(2))
            emailTemplate = emailTemplate.replace('{{total}}', total.toFixed(2))
            emailTemplate = emailTemplate.replace('{{discount}}', discount.toFixed(2))
            emailTemplate = emailTemplate.replace('{{shipping}}', shipping.toFixed(2))
            emailTemplate = emailTemplate.replace('{{listProduct}}', listProduct)
            const subject = `${storeName}: Email confirm order`
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: subject
            }
            await this.sendEmail(adminId, paramSendMail)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async emailAddTrackingOrder(adminId: string, email: string) {
        try {
            const order = orderTest
            const found = await this.domainService.findOne({ filter: { enabled: true } })
            if (!found) {
                throw new BaseApiException({
                    message: `NOT_FOUND_EMAIL_SENDER`
                })
            }
            const storeName = found.domain.charAt(0).toUpperCase() + found.domain.slice(1)
            const supportEmail = found.supportEmail
            const { shippingAddress, /* total, subTotal, discount, shipping,  */storeId, orderCode, listSelectedProductId, trackingNumber } = order
            const { firstName, lastName, address, city, state, country, phoneNumber } = shippingAddress
            const logo = '/logo/logozshop.png'
            const pathEmail = join(__dirname, '../../../backend/public/orderTracking.html')
            let emailTemplate = readFileSync(pathEmail, 'utf-8')
            const subject = `${storeName}: Order was fulfilled`
            emailTemplate = emailTemplate.replace('{{logo}}', this.url + logo)
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replace('{{orderCode}}', orderCode)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{trackingUrl}}', this.trackingUrl)
            emailTemplate = emailTemplate.replace('{{firstName}}', firstName)
            emailTemplate = emailTemplate.replace('{{trackingNumber}}', '1231231231231')
            emailTemplate = emailTemplate.replace('supportEmail', supportEmail)
            emailTemplate = emailTemplate.replace('{{shippingName}}', `${firstName} ${lastName}`)
            emailTemplate = emailTemplate.replace('{{shippingAddress}}', `${address},${city ? ' ' + city : ''},${state ? ' ' + state : ''},${country ? ' ' + country : ''} `)
            emailTemplate = emailTemplate.replace('{{billName}}', `${firstName} ${lastName}`)
            emailTemplate = emailTemplate.replace('{{billAddress}}', `${address},${city ? ' ' + city : ''},${state ? ' ' + state : ''},${country ? ' ' + country : ''} `)
            const listSelectedProduct: any = await this.productService.findTestMail()
            let listProduct = ''
            let subTotal = 0
            let total = 0
            const discount = 0
            const shipping = 5.99
            for (const product of listSelectedProduct) {
                const variant = product.listVariant[0]
                const { price, comparePrice, imageId, listValue } = variant
                total = price
                subTotal = price
                const quantity = 1
                let oneProduct = `
                <tr>
                <td style="vertical-align:middle" align="left" width="17%"><a
                        style="display:block" title="" href="#" target="_blank">
                        <img style="display:block;font-family:Arial;width:80px; margin-top:8px"
                            src="${this.url}${imageId.imageUrl}"
                            alt="" width="80" border="0">
                    </a>
                </td>
                <td style="padding:0 20px 0 20px;vertical-align:top" align="left"
                    width="83%">
                    <p
                        style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif; margin-top:8px">
                        <a style="text-decoration:none" title="ErgoStool" href=""
                            rel="noopener"
                            target="_blank">${product.productName}</a>
                    </p>
                    {{variant}}
                    <p
                        style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">
                        <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$${price}</span>
                        &nbsp;x&nbsp; <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">${quantity}</span>
                    </p>
                </td>
                <td
                    style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">
                    $${(price * quantity).toFixed(2)}
                </td> 
                `
                if (listValue) {
                    let oneVariant = ''
                    if (listValue.length) {
                        let variant = listValue.join('/ ')
                        oneVariant = `
                        <p
                                style="font-size:15px;line-height:26px;color:#555;margin:4px 0px;font-family:sans-serif;">
                                ${variant}
                            </p>
                        `
                    }
                    oneProduct = oneProduct.replace('{{variant}}', oneVariant)
                } else {
                    oneProduct = oneProduct.replace('{{variant}}', '')
                }
                listProduct += oneProduct
            }
            total = total + shipping
            emailTemplate = emailTemplate.replace('{{subTotal}}', subTotal.toFixed(2))
            emailTemplate = emailTemplate.replace('{{total}}', total.toFixed(2))
            emailTemplate = emailTemplate.replace('{{discount}}', discount.toFixed(2))
            emailTemplate = emailTemplate.replace('{{shipping}}', shipping.toFixed(2))
            emailTemplate = emailTemplate.replace('{{listProduct}}', listProduct)
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: subject
            }
            await this.sendEmail(adminId, paramSendMail)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
            // console.log('error', error.body, email)
        }
    }

    async emailChangeShipping(adminId: string, email: string) {
        try {
            const order = orderTest
            const found = await this.domainService.findOne({ filter: { enabled: true } })
            if (!found) {
                throw new BaseApiException({
                    message: `NOT_FOUND_EMAIL_SENDER`
                })
            }
            const storeName = found.domain.charAt(0).toUpperCase() + found.domain.slice(1)
            const supportEmail = found.supportEmail
            const { shippingAddress, /* total, subTotal, discount, shipping,  */storeId, orderCode, listSelectedProductId, trackingNumber } = order
            const { firstName, lastName, address, city, state, country, phoneNumber } = shippingAddress
            const logo = '/logo/logozshop.png'
            const pathEmail = join(__dirname, '../../../backend/public/changeShipping.html')
            let emailTemplate = readFileSync(pathEmail, 'utf-8')
            const subject = `${storeName}: Change order's infomation`
            emailTemplate = emailTemplate.replace('{{logo}}', this.url + logo)
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replace('{{orderCode}}', orderCode)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{supportEmail}}', supportEmail)
            emailTemplate = emailTemplate.replace('{{firstName}}', firstName)
            emailTemplate = emailTemplate.replace('{{shippingName}}', `${firstName} ${lastName}`)
            emailTemplate = emailTemplate.replace('{{shippingAddress}}', `${address},${city ? ' ' + city : ''},${state ? ' ' + state : ''},${country ? ' ' + country : ''} `)
            emailTemplate = emailTemplate.replace('{{billName}}', `${firstName} ${lastName}`)
            emailTemplate = emailTemplate.replace('{{billAddress}}', `${address},${city ? ' ' + city : ''},${state ? ' ' + state : ''},${country ? ' ' + country : ''} `)
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: subject
            }
            await this.sendEmail(adminId, paramSendMail)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async emailCancelOrder(adminId: string, email: string) {
        try {
            const order = orderTest
            const found = await this.domainService.findOne({ filter: { enabled: true } })
            if (!found) {
                throw new BaseApiException({
                    message: `NOT_FOUND_EMAIL_SENDER`
                })
            }
            const storeName = found.domain.charAt(0).toUpperCase() + found.domain.slice(1)
            const supportEmail = found.supportEmail
            const shipping = 5.99
            const { shippingAddress, /* total, subTotal, discount, shipping,  */storeId, orderCode, listSelectedProductId, trackingNumber } = order
            const { firstName, lastName, address, city, state, country, phoneNumber } = shippingAddress
            const logo = '/logo/logozshop.png'
            const pathEmail = join(__dirname, '../../../backend/public/orderCancel.html')
            let emailTemplate = readFileSync(pathEmail, 'utf-8')
            const subject = `${storeName}: Order was canceled`
            emailTemplate = emailTemplate.replace('{{logo}}', this.url + logo)
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replace('{{orderCode}}', orderCode)
            emailTemplate = emailTemplate.replace('{{orderCode}}', orderCode)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{supportEmail}}', supportEmail)
            emailTemplate = emailTemplate.replace('{{firstName}}', firstName)

            const listSelectedProduct: any = await this.productService.findTestMail()
            let listProduct = ''
            let total = 0
            for (const product of listSelectedProduct) {
                const variant = product.listVariant[0]
                const { price, comparePrice, imageId, listValue } = variant
                total = price
                const quantity = 1
                let oneProduct = `
                <tr>
                <td style="vertical-align:middle" align="left" width="17%"><a
                        style="display:block" title="" href="#" target="_blank">
                        <img style="display:block;font-family:Arial;width:80px; margin-top:8px"
                            src="${this.url}${imageId.imageUrl}"
                            alt="" width="80" border="0">
                    </a>
                </td>
                <td style="padding:0 20px 0 20px;vertical-align:top" align="left"
                    width="83%">
                    <p
                        style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif; margin-top:8px">
                        <a style="text-decoration:none" title="ErgoStool" href=""
                            rel="noopener"
                            target="_blank">${product.productName}</a>
                    </p>
                    {{variant}}
                    <p
                        style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">
                        <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$${price}</span>
                        &nbsp;x&nbsp; <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">${quantity}</span>
                    </p>
                </td>
                <td
                    style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">
                    $${(price * quantity).toFixed(2)}
                </td> 
                `
                if (listValue) {
                    let oneVariant = ''
                    if (listValue.length) {
                        let variant = listValue.join('/ ')
                        oneVariant = `
                        <p
                                style="font-size:15px;line-height:26px;color:#555;margin:4px 0px;font-family:sans-serif;">
                                ${variant}
                            </p>
                        `
                    }
                    oneProduct = oneProduct.replace('{{variant}}', oneVariant)
                } else {
                    oneProduct = oneProduct.replace('{{variant}}', '')
                }
                listProduct += oneProduct
            }
            total = total + shipping
            emailTemplate = emailTemplate.replace('{{total}}', total.toFixed(2))
            emailTemplate = emailTemplate.replace('{{listProduct}}', listProduct)
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: subject
            }
            await this.sendEmail(adminId, paramSendMail)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async testMailAbandoned1(adminId: string, email: string) {
        try {
            const foundAbandone = await this.abandonedCheckoutService.findOneTestMail({ adminId, time: 1 })
            let { emailSubject, emailTemplate, discount, couponId } = foundAbandone
            const foundDomain = await this.domainService.findOne({ filter: { enabled: true } })
            if (!foundDomain) {
                throw new BaseApiException({
                    message: `NOT_FOUND_EMAIL_SENDER`
                })
            }
            const storeName = foundDomain.domain.charAt(0).toUpperCase() + foundDomain.domain.slice(1)
            const supportEmail = foundDomain.supportEmail
            const listSelectedProduct: any = await this.productService.findTestMail()
            let listProduct = ''
            let total = 0
            for (const product of listSelectedProduct) {
                const variant = product.listVariant[0]
                const { price, comparePrice, imageId, listValue } = variant
                total = price
                const quantity = 1
                let oneProduct = `
                <tr>
                <td style="vertical-align:middle" align="left" width="17%"><a
                        style="display:block" title="" href="#" target="_blank">
                        <img style="display:block;font-family:Arial;width:80px; margin-top:8px"
                            src="${this.url}${imageId.imageUrl}"
                            alt="" width="80" border="0">
                    </a>
                </td>
                <td style="padding:0 20px 0 20px;vertical-align:top" align="left"
                    width="83%">
                    <p
                        style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif; margin-top:8px">
                        <a style="text-decoration:none" title="ErgoStool" href=""
                            rel="noopener"
                            target="_blank">${product.productName}</a>
                    </p>
                    {{variant}}
                    <p
                        style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">
                        <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$${price}</span>
                        &nbsp;x&nbsp; <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">${quantity}</span>
                    </p>
                </td>
                <td
                    style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">
                    $${(price * quantity).toFixed(2)}
                </td> 
                `
                if (listValue) {
                    let oneVariant = ''
                    if (listValue.length) {
                        let variant = listValue.join('/ ')
                        oneVariant = `
                        <p
                                style="font-size:15px;line-height:26px;color:#555;margin:4px 0px;font-family:sans-serif;">
                                ${variant}
                            </p>
                        `
                    }
                    oneProduct = oneProduct.replaceAll('{{variant}}', oneVariant)
                } else {
                    oneProduct = oneProduct.replaceAll('{{variant}}', '')
                }
                listProduct += oneProduct
            }
            const logo = '/logo/logozshop.png'
            emailTemplate = emailTemplate.replaceAll('{{listProduct}}', listProduct)
            emailTemplate = emailTemplate.replaceAll('{{logo}}', this.url + logo)
            emailTemplate = emailTemplate.replaceAll('{{storeName}}', storeName)
            emailSubject = emailSubject.replaceAll('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replaceAll('{{email}}', email)
            //@ts-ignore
            emailTemplate = emailTemplate.replaceAll('{{coupon}}', couponId?.code || '05PERCENT')
            const discountValue = discount.toString() + '%'
            emailTemplate = emailTemplate.replaceAll('{{discount}}', discountValue || '05 %');
            emailTemplate = emailTemplate.replaceAll('{{supportEmail}}', supportEmail || "support@example.com")
            emailTemplate = emailTemplate.replaceAll('{{abd-image}}', 'https://api.proxyv6.net/images/abd5.png')
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: emailSubject
            }
            await this.sendEmail(adminId, paramSendMail)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async testMailAbandoned2(adminId: string, email: string) {
        try {
            const foundAbandone = await this.abandonedCheckoutService.findOneTestMail({ adminId, time: 2 })
            let { emailSubject, emailTemplate, discount, couponId } = foundAbandone
            const foundDomain = await this.domainService.findOne({ filter: { enabled: true } })
            if (!foundDomain) {
                throw new BaseApiException({
                    message: `NOT_FOUND_EMAIL_SENDER`
                })
            }
            const storeName = foundDomain.domain.charAt(0).toUpperCase() + foundDomain.domain.slice(1)
            const supportEmail = foundDomain.supportEmail
            const listSelectedProduct: any = await this.productService.findTestMail()
            let listProduct = ''
            let total = 0
            for (const product of listSelectedProduct) {
                const variant = product.listVariant[0]
                const { price, comparePrice, imageId, listValue } = variant
                total = price
                const quantity = 1
                let oneProduct = `
                <tr>
                <td style="vertical-align:middle" align="left" width="17%"><a
                        style="display:block" title="" href="#" target="_blank">
                        <img style="display:block;font-family:Arial;width:80px; margin-top:8px"
                            src="${this.url}${imageId.imageUrl}"
                            alt="" width="80" border="0">
                    </a>
                </td>
                <td style="padding:0 20px 0 20px;vertical-align:top" align="left"
                    width="83%">
                    <p
                        style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif; margin-top:8px">
                        <a style="text-decoration:none" title="ErgoStool" href=""
                            rel="noopener"
                            target="_blank">${product.productName}</a>
                    </p>
                    {{variant}}
                    <p
                        style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">
                        <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$${price}</span>
                        &nbsp;x&nbsp; <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">${quantity}</span>
                    </p>
                </td>
                <td
                    style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">
                    $${(price * quantity).toFixed(2)}
                </td> 
                `
                if (listValue) {
                    let oneVariant = ''
                    if (listValue.length) {
                        let variant = listValue.join('/ ')
                        oneVariant = `
                        <p
                                style="font-size:15px;line-height:26px;color:#555;margin:4px 0px;font-family:sans-serif;">
                                ${variant}
                            </p>
                        `
                    }
                    oneProduct = oneProduct.replaceAll('{{variant}}', oneVariant)
                } else {
                    oneProduct = oneProduct.replaceAll('{{variant}}', '')
                }
                listProduct += oneProduct
            }
            const logo = '/logo/logozshop.png'
            emailTemplate = emailTemplate.replaceAll('{{listProduct}}', listProduct)
            emailTemplate = emailTemplate.replaceAll('{{logo}}', this.url + logo)
            emailTemplate = emailTemplate.replaceAll('{{storeName}}', storeName)
            emailSubject = emailSubject.replaceAll('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replaceAll('{{email}}', email)
            //@ts-ignore
            emailTemplate = emailTemplate.replaceAll('{{coupon}}', couponId?.code || '10PERCENT')
            const discountValue = discount.toString() + '%'
            emailTemplate = emailTemplate.replaceAll('{{discount}}', discountValue || '10 %');
            emailTemplate = emailTemplate.replaceAll('{{supportEmail}}', supportEmail || "support@example.com")
            emailTemplate = emailTemplate.replaceAll('{{abd-image}}', 'https://api.proxyv6.net/images/abd10.png')
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: emailSubject
            }
            await this.sendEmail(adminId, paramSendMail)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async testMailAbandoned3(adminId: string, email: string) {
        try {
            const foundAbandone = await this.abandonedCheckoutService.findOneTestMail({ adminId, time: 3 })
            let { emailSubject, emailTemplate, discount, couponId } = foundAbandone
            const foundDomain = await this.domainService.findOne({ filter: { enabled: true } })
            if (!foundDomain) {
                throw new BaseApiException({
                    message: `NOT_FOUND_EMAIL_SENDER`
                })
            }
            const storeName = foundDomain.domain.charAt(0).toUpperCase() + foundDomain.domain.slice(1)
            const supportEmail = foundDomain.supportEmail
            const listSelectedProduct: any = await this.productService.findTestMail()
            let listProduct = ''
            let total = 0
            for (const product of listSelectedProduct) {
                const variant = product.listVariant[0]
                const { price, comparePrice, imageId, listValue } = variant
                total = price
                const quantity = 1
                let oneProduct = `
                <tr>
                <td style="vertical-align:middle" align="left" width="17%"><a
                        style="display:block" title="" href="#" target="_blank">
                        <img style="display:block;font-family:Arial;width:80px; margin-top:8px"
                            src="${this.url}${imageId.imageUrl}"
                            alt="" width="80" border="0">
                    </a>
                </td>
                <td style="padding:0 20px 0 20px;vertical-align:top" align="left"
                    width="83%">
                    <p
                        style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif; margin-top:8px">
                        <a style="text-decoration:none" title="ErgoStool" href=""
                            rel="noopener"
                            target="_blank">${product.productName}</a>
                    </p>
                    {{variant}}
                    <p
                        style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">
                        <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$${price}</span>
                        &nbsp;x&nbsp; <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">${quantity}</span>
                    </p>
                </td>
                <td
                    style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">
                    $${(price * quantity).toFixed(2)}
                </td> 
                `
                if (listValue) {
                    let oneVariant = ''
                    if (listValue.length) {
                        let variant = listValue.join('/ ')
                        oneVariant = `
                        <p
                                style="font-size:15px;line-height:26px;color:#555;margin:4px 0px;font-family:sans-serif;">
                                ${variant}
                            </p>
                        `
                    }
                    oneProduct = oneProduct.replaceAll('{{variant}}', oneVariant)
                } else {
                    oneProduct = oneProduct.replaceAll('{{variant}}', '')
                }
                listProduct += oneProduct
            }
            const logo = '/logo/logozshop.png'
            emailTemplate = emailTemplate.replaceAll('{{listProduct}}', listProduct)
            emailTemplate = emailTemplate.replaceAll('{{logo}}', this.url + logo)
            emailTemplate = emailTemplate.replaceAll('{{storeName}}', storeName)
            emailSubject = emailSubject.replaceAll('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replaceAll('{{email}}', email)
            //@ts-ignore
            emailTemplate = emailTemplate.replaceAll('{{coupon}}', couponId?.code || '15PERCENT')
            const discountValue = discount.toString() + '%'
            emailTemplate = emailTemplate.replaceAll('{{discount}}', discountValue || '15 %');
            emailTemplate = emailTemplate.replaceAll('{{supportEmail}}', supportEmail || "support@example.com")
            emailTemplate = emailTemplate.replaceAll('{{abd-image}}', 'https://api.proxyv6.net/images/abd15.png')
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: emailSubject
            }
            await this.sendEmail(adminId, paramSendMail)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async sendEmail(adminId: string, param: ISendMail): Promise<void> {
        try {
            // const apiKey = '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'
            const apiKey = await this.adminSettingService.getApikeySendgrid(adminId)
            client.setApiKey(apiKey)
            let { to, html, from, subject } = param
            await client.send({
                to: to,
                from,
                subject: subject,
                text: 'a',
                html: html
            })
        } catch (error) {
            console.log('response.body :>> ', error.response.body);
            throw error
        }
    }



}