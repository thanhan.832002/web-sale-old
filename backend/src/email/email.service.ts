import { Injectable, OnModuleInit } from '@nestjs/common';
import { ISendMail } from './sendMail.interface';
import { join, dirname } from 'path'
import { readFileSync } from 'fs'
import { Order } from 'src/order/order.model';
import { StoreSettingService } from 'src/store-setting/store-setting.service';
import { SelectedProductSellerService } from 'src/selected-product-seller/selected-product-seller.service';
import { StoreService } from 'src/store/store.service';
import { SendAbandoned } from 'src/do-send-abandoned/send-abandoned.model';
import { Coupon, DiscountEnum } from 'src/coupon/coupon.model';
import { AdminSettingService } from 'src/admin-setting/admin-setting.service';
import { VariantSellerService } from 'src/variant-seller/variant-seller.service';
import { ModuleRef } from '@nestjs/core';
import { UserService } from 'src/user/user.service';
import { AbandonedCheckout } from 'src/abandoned-checkout/abandoned-checkout.model';
var momentTz = require("moment-timezone");
const client = require('@sendgrid/mail')
const fs = require('fs');



@Injectable()
export class EmailService implements OnModuleInit {
    private userService: UserService
    constructor(
        private readonly adminSettingService: AdminSettingService,
        private readonly storeSettingService: StoreSettingService,
        private readonly storeService: StoreService,
        private readonly selectedProductSellerService: SelectedProductSellerService,
        private readonly variantSellerService: VariantSellerService,
        private moduleRef: ModuleRef
    ) { }

    onModuleInit() {
        this.userService = this.moduleRef.get(UserService, { strict: false });
    }

    url = 'https://api.opensitex.com/'

    async emailVerifyAccount(adminId: string, email: string, token: string) {
        try {
            const storeName = 'US House'
            const pathEmail = join(__dirname, '../../../backend/public/verifyMail.html')
            let emailTemplate = readFileSync(pathEmail, 'utf-8')
            const linkVerify = `https://zshop.cc?token=${token}`
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{token}}', token)
            emailTemplate = emailTemplate.replace('{{linkVerify}}', linkVerify)
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName)
            const subject = `${storeName}: Please verify your account`
            const paramSendMail: ISendMail = {
                to: email,
                from: 'support@zshop.cc',
                html: emailTemplate,
                subject: subject
            }
            await this.sendEmail(adminId, paramSendMail)
        } catch (error) {
            console.log('error :>> ', error);
        }
    }

    async emailForgotPassword(adminId: string, email: string, token: string, domain: string) {
        try {
            const storeName = 'US House'
            const pathEmail = join(__dirname, '../../../backend/public/forgotPasswordMail.html')
            let emailTemplate = readFileSync(pathEmail, 'utf-8')
            const linkResetPassword = `https://${domain}/auth/forgot-password?token=${token}`
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{token}}', token)
            emailTemplate = emailTemplate.replace('{{linkResetPassword}}', linkResetPassword)
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName)
            const subject = `${storeName}: Reset your password`
            const paramSendMail: ISendMail = {
                to: email,
                from: 'support@zshop.cc',
                html: emailTemplate,
                subject: subject
            }
            await this.sendEmail(adminId, paramSendMail)
        } catch (error) {
            console.log('error :>> ', error);
        }
    }

    async emailConfirmOrder(adminId: string, order: Partial<Order>) {
        try {
            const pathEmail = join(__dirname, '../../../backend/public/orderConfirm.html')
            let emailTemplate = readFileSync(pathEmail, 'utf-8')
            const { shippingAddress, total, subTotal, discount, shipping, storeId, orderCode, listSelectedProductId, sellerId } = order
            let { billingAddress } = order
            if (!billingAddress) billingAddress = shippingAddress
            const store = await this.storeService.findOne({ filter: { _id: storeId } })
            const { storeName, supportEmail, _id, domain } = store
            const { email, firstName, lastName, address, apartment, city, state, country, phoneNumber, companyName } = shippingAddress
            const storeSetting: any = await this.storeSettingService.findStoreSetting(_id)
            const pathLogoDefault = '/logo/logozshop.png'
            const logo = storeSetting?.logo?.imageUrl || pathLogoDefault
            emailTemplate = emailTemplate.replace('{{logo}}', (encodeURI(this.url + logo || "")))
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName || 'Shop')
            emailTemplate = emailTemplate.replace('{{supportEmail}}', supportEmail || "")
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{firstName}}', firstName)
            emailTemplate = emailTemplate.replace('{{orderCode}}', orderCode)
            emailTemplate = emailTemplate.replace('{{subTotal}}', subTotal.toFixed(2))
            emailTemplate = emailTemplate.replace('{{total}}', total.toFixed(2))
            emailTemplate = emailTemplate.replace('{{discount}}', discount.toFixed(2))
            emailTemplate = emailTemplate.replace('{{shipping}}', shipping.toFixed(2))
            emailTemplate = emailTemplate.replace('{{shippingName}}', `${firstName} ${lastName}`)
            emailTemplate = emailTemplate.replace('{{shippingAddress}}', `${address}${apartment ? ' ' + apartment : ''},${city ? ' ' + city : ''},${state ? ' ' + state : ''},${country ? ' ' + country : ''} `)
            emailTemplate = emailTemplate.replace('{{billName}}', `${billingAddress.firstName} ${billingAddress.lastName}`)
            emailTemplate = emailTemplate.replace('{{billAddress}}', `${billingAddress.address}${billingAddress.apartment ? ' ' + billingAddress.apartment : ''},${billingAddress.city ? ' ' + billingAddress.city : ''},${billingAddress.state ? ' ' + billingAddress.state : ''},${billingAddress.country ? ' ' + billingAddress.country : ''} `)
            const listSelectedProduct: any = await this.selectedProductSellerService.find({ _id: { $in: listSelectedProductId } }, [{ path: 'productSellerId', select: '-description' }])
            let listProduct = ''
            for (const product of listSelectedProduct) {
                const { imageUrl, price, productSellerId, quantity, listValue } = product
                let oneProduct = `
                <tr>
                <td style="vertical-align:middle" align="left" width="17%"><a
                        style="display:block" title="" href="#" target="_blank">
                        <img style="display:block;font-family:Arial;width:80px; margin-top:8px"
                            src="${encodeURI(this.url + imageUrl) || ""}"
                            alt="" width="80" border="0">
                    </a>
                </td>
                <td style="padding:0 20px 0 20px;vertical-align:top" align="left"
                    width="83%">
                    <p
                        style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif; margin-top:8px">
                        <a style="text-decoration:none" title="ErgoStool" href="${domain}"
                            rel="noopener"
                            target="_blank">${productSellerId.productName}</a>
                    </p>
                    {{variant}}
                    <p
                        style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">
                        <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$${price}</span>
                        &nbsp;x&nbsp; <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">${quantity}</span>
                    </p>
                </td>
                <td
                    style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">
                    $${(price * quantity).toFixed(2)}
                </td> 
                `
                if (listValue) {
                    let oneVariant = ''
                    if (listValue.length) {
                        let variant = listValue.join('/ ')
                        oneVariant = `
                        <p
                                style="font-size:15px;line-height:26px;color:#555;margin:4px 0px;font-family:sans-serif;">
                                ${variant}
                            </p>
                        `
                    }
                    oneProduct = oneProduct.replace('{{variant}}', oneVariant)
                } else {
                    oneProduct = oneProduct.replace('{{variant}}', '')
                }
                listProduct += oneProduct
            }

            emailTemplate = emailTemplate.replace('{{listProduct}}', listProduct)
            console.log('email :>> ', email);
            const subject = `${storeName}: Email confirm order`
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: subject
            }
            await this.sendEmail(adminId, paramSendMail)
        } catch (error) {
            throw error
        }
    }

    async emailAddTrackingOrder(adminId: string, order: Partial<Order>) {
        try {

            const { shippingAddress, total, subTotal, discount, shipping, storeId, orderCode, listSelectedProductId, trackingNumber } = order
            let { billingAddress } = order
            if (!billingAddress) billingAddress = shippingAddress
            const store = await this.storeService.findOne({ filter: { _id: storeId } })
            const { storeName, supportEmail, _id, domain } = store
            const { email, firstName, lastName, address, apartment, city, state, country, phoneNumber, companyName } = shippingAddress
            const storeSetting: any = await this.storeSettingService.findStoreSetting(_id)
            const pathLogoDefault = '/logo/logozshop.png'
            const logo = storeSetting?.logo?.imageUrl || pathLogoDefault
            const pathEmail = join(__dirname, '../../../backend/public/orderTracking.html')
            let emailTemplate = readFileSync(pathEmail, 'utf-8')
            const subject = `${storeName}: Order was fulfilled`
            emailTemplate = emailTemplate.replace('{{logo}}', (encodeURI(this.url + logo || "")))
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replace('{{orderCode}}', orderCode)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            // emailTemplate = emailTemplate.replace('{{trackingUrl}}', this.trackingUrl)
            emailTemplate = emailTemplate.replace('{{firstName}}', firstName)
            emailTemplate = emailTemplate.replace('{{trackingNumber}}', trackingNumber)
            emailTemplate = emailTemplate.replace('{{trackingNumber}}', trackingNumber)
            emailTemplate = emailTemplate.replace('supportEmail', supportEmail)
            emailTemplate = emailTemplate.replace('{{shippingName}}', `${firstName} ${lastName}`)
            emailTemplate = emailTemplate.replace('{{shippingAddress}}', `${address}${apartment ? ' ' + apartment : ''},${city ? ' ' + city : ''},${state ? ' ' + state : ''},${country ? ' ' + country : ''} `)
            emailTemplate = emailTemplate.replace('{{billName}}', `${billingAddress.firstName} ${billingAddress.lastName}`)
            emailTemplate = emailTemplate.replace('{{billAddress}}', `${billingAddress.address}${billingAddress.apartment ? ' ' + billingAddress.apartment : ''},${billingAddress.city ? ' ' + billingAddress.city : ''},${billingAddress.state ? ' ' + billingAddress.state : ''},${billingAddress.country ? ' ' + billingAddress.country : ''} `)
            emailTemplate = emailTemplate.replace('{{subTotal}}', subTotal.toFixed(2))
            emailTemplate = emailTemplate.replace('{{shipping}}', shipping.toFixed(2))
            emailTemplate = emailTemplate.replace('{{discount}}', discount.toFixed(2))
            emailTemplate = emailTemplate.replace('{{total}}', total.toFixed(2))
            const listSelectedProduct: any = await this.selectedProductSellerService.find({ _id: { $in: listSelectedProductId } }, [{ path: 'productSellerId', select: '-description' }])
            let listProduct = ''

            for (const product of listSelectedProduct) {
                const { imageUrl, price, productSellerId, quantity, listValue } = product
                let oneProduct = `
                <tr>
                <td style="vertical-align:middle" align="left" width="17%"><a
                        style="display:block" title="" href="#" target="_blank">
                        <img style="display:block;font-family:Arial;width:80px; margin-top:8px"
                            src="${encodeURI(this.url + imageUrl) || ""}"
                            alt="" width="80" border="0">
                    </a>
                </td>
                <td style="padding:0 20px 0 20px;vertical-align:top" align="left"
                    width="83%">
                    <p
                        style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif; margin-top:8px">
                        <a style="text-decoration:none" title="ErgoStool" href="${domain}"
                            rel="noopener"
                            target="_blank">${productSellerId.productName}</a>
                    </p>
                    {{variant}}
                    <p
                        style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">
                        <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$${price}</span>
                        &nbsp;x&nbsp; <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">${quantity}</span>
                    </p>
                </td>
                <td
                    style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">
                    $${(price * quantity).toFixed(2)}
                </td> 
                `
                if (listValue) {
                    let oneVariant = ''
                    if (listValue.length) {
                        let variant = listValue.join('/ ')
                        oneVariant = `
                        <p
                                style="font-size:15px;line-height:26px;color:#555;margin:4px 0px;font-family:sans-serif;">
                                ${variant}
                            </p>
                        `
                    }
                    oneProduct = oneProduct.replace('{{variant}}', oneVariant)
                } else {
                    oneProduct = oneProduct.replace('{{variant}}', '')
                }
                listProduct += oneProduct
            }

            emailTemplate = emailTemplate.replace('{{listProduct}}', listProduct)
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: subject
            }
            await this.sendEmail(adminId, paramSendMail)
        } catch (error) {
            throw error
        }
    }

    async emailTrackingUpdated(adminId: string, order: Partial<Order>) {
        try {

            const { shippingAddress, total, subTotal, discount, shipping, storeId, orderCode, listSelectedProductId, trackingNumber } = order
            let { billingAddress } = order
            if (!billingAddress) billingAddress = shippingAddress
            const store = await this.storeService.findOne({ filter: { _id: storeId } })
            const { storeName, supportEmail, _id, domain } = store
            const { email, firstName, lastName, address, apartment, city, state, country, phoneNumber, companyName } = shippingAddress
            const storeSetting: any = await this.storeSettingService.findStoreSetting(_id)
            const pathLogoDefault = '/logo/logozshop.png'
            const logo = storeSetting?.logo?.imageUrl || pathLogoDefault
            const pathEmail = join(__dirname, '../../../backend/public/trackingUpdated.html')
            let emailTemplate = readFileSync(pathEmail, 'utf-8')
            const subject = `${storeName}: Order was updated`
            emailTemplate = emailTemplate.replace('{{logo}}', (encodeURI(this.url + logo || "")))
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replace('{{orderCode}}', orderCode)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            // emailTemplate = emailTemplate.replace('{{trackingUrl}}', this.trackingUrl)
            emailTemplate = emailTemplate.replace('{{firstName}}', firstName)
            emailTemplate = emailTemplate.replace('{{trackingNumber}}', trackingNumber)
            emailTemplate = emailTemplate.replace('{{trackingNumber}}', trackingNumber)
            emailTemplate = emailTemplate.replace('supportEmail', supportEmail)
            emailTemplate = emailTemplate.replace('{{shippingName}}', `${firstName} ${lastName}`)
            emailTemplate = emailTemplate.replace('{{shippingAddress}}', `${address}${apartment ? ' ' + apartment : ''},${city ? ' ' + city : ''},${state ? ' ' + state : ''},${country ? ' ' + country : ''} `)
            emailTemplate = emailTemplate.replace('{{billName}}', `${billingAddress.firstName} ${billingAddress.lastName}`)
            emailTemplate = emailTemplate.replace('{{billAddress}}', `${billingAddress.address}${billingAddress.apartment ? ' ' + billingAddress.apartment : ''},${billingAddress.city ? ' ' + billingAddress.city : ''},${billingAddress.state ? ' ' + billingAddress.state : ''},${billingAddress.country ? ' ' + billingAddress.country : ''} `)
            emailTemplate = emailTemplate.replace('{{subTotal}}', subTotal.toFixed(2))
            emailTemplate = emailTemplate.replace('{{shipping}}', shipping.toFixed(2))
            emailTemplate = emailTemplate.replace('{{discount}}', discount.toFixed(2))
            emailTemplate = emailTemplate.replace('{{total}}', total.toFixed(2))
            const listSelectedProduct: any = await this.selectedProductSellerService.find({ _id: { $in: listSelectedProductId } }, [{ path: 'productSellerId', select: '-description' }])
            let listProduct = ''

            for (const product of listSelectedProduct) {
                const { imageUrl, price, productSellerId, quantity, listValue } = product
                let oneProduct = `
                <tr>
                <td style="vertical-align:middle" align="left" width="17%"><a
                        style="display:block" title="" href="#" target="_blank">
                        <img style="display:block;font-family:Arial;width:80px; margin-top:8px"
                            src="${encodeURI(this.url + imageUrl) || ""}"
                            alt="" width="80" border="0">
                    </a>
                </td>
                <td style="padding:0 20px 0 20px;vertical-align:top" align="left"
                    width="83%">
                    <p
                        style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif; margin-top:8px">
                        <a style="text-decoration:none" title="ErgoStool" href="${domain}"
                            rel="noopener"
                            target="_blank">${productSellerId.productName}</a>
                    </p>
                    {{variant}}
                    <p
                        style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">
                        <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$${price}</span>
                        &nbsp;x&nbsp; <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">${quantity}</span>
                    </p>
                </td>
                <td
                    style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">
                    $${(price * quantity).toFixed(2)}
                </td> 
                `
                if (listValue) {
                    let oneVariant = ''
                    if (listValue.length) {
                        let variant = listValue.join('/ ')
                        oneVariant = `
                        <p
                                style="font-size:15px;line-height:26px;color:#555;margin:4px 0px;font-family:sans-serif;">
                                ${variant}
                            </p>
                        `
                    }
                    oneProduct = oneProduct.replace('{{variant}}', oneVariant)
                } else {
                    oneProduct = oneProduct.replace('{{variant}}', '')
                }
                listProduct += oneProduct
            }

            emailTemplate = emailTemplate.replace('{{listProduct}}', listProduct)
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: subject
            }
            await this.sendEmail(adminId, paramSendMail)
        } catch (error) {
            console.log('error :>> ', error);
            // console.log('error', error.body, email)
        }
    }

    async emailChangeShipping(adminId: string, order: Partial<Order>) {
        try {
            const { shippingAddress, total, subTotal, discount, shipping, storeId, orderCode, listSelectedProductId, trackingNumber } = order
            let { billingAddress } = order
            if (!billingAddress) billingAddress = shippingAddress
            const store = await this.storeService.findOne({ filter: { _id: storeId } })
            const { storeName, supportEmail, _id, domain } = store
            const { email, firstName, lastName, address, apartment, city, state, country, phoneNumber, companyName } = shippingAddress
            const storeSetting: any = await this.storeSettingService.findStoreSetting(_id)
            const pathLogoDefault = '/logo/logozshop.png'
            const logo = storeSetting?.logo?.imageUrl || pathLogoDefault
            const pathEmail = join(__dirname, '../../../backend/public/changeShipping.html')
            let emailTemplate = readFileSync(pathEmail, 'utf-8')
            const subject = `${storeName}: Change order's infomation`
            emailTemplate = emailTemplate.replace('{{logo}}', (encodeURI(this.url + logo || "")))
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replace('{{orderCode}}', orderCode)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{supportEmail}}', supportEmail || "")
            emailTemplate = emailTemplate.replace('{{firstName}}', firstName)
            emailTemplate = emailTemplate.replace('{{shippingName}}', `${firstName} ${lastName}`)
            emailTemplate = emailTemplate.replace('{{shippingAddress}}', `${address}${apartment ? ' ' + apartment : ''},${city ? ' ' + city : ''},${state ? ' ' + state : ''},${country ? ' ' + country : ''} `)
            emailTemplate = emailTemplate.replace('{{billName}}', `${billingAddress.firstName} ${billingAddress.lastName}`)
            emailTemplate = emailTemplate.replace('{{billAddress}}', `${billingAddress.address}${billingAddress.apartment ? ' ' + billingAddress.apartment : ''},${billingAddress.city ? ' ' + billingAddress.city : ''},${billingAddress.state ? ' ' + billingAddress.state : ''},${billingAddress.country ? ' ' + billingAddress.country : ''} `)
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: subject
            }
            await this.sendEmail(adminId, paramSendMail)
        } catch (error) {

        }
    }

    async emailCancelOrder(adminId: string, order: Partial<Order>) {
        try {
            const { shippingAddress, total, subTotal, discount, shipping, storeId, orderCode, listSelectedProductId, trackingNumber } = order
            const store = await this.storeService.findOne({ filter: { _id: storeId } })
            const { storeName, supportEmail, _id, domain } = store
            const { email, firstName, lastName, address, apartment, city, state, country, phoneNumber, companyName } = shippingAddress
            const storeSetting: any = await this.storeSettingService.findStoreSetting(_id)
            const pathLogoDefault = '/logo/logozshop.png'
            const logo = storeSetting?.logo?.imageUrl || pathLogoDefault
            const pathEmail = join(__dirname, '../../../backend/public/orderCancel.html')
            let emailTemplate = readFileSync(pathEmail, 'utf-8')
            const subject = `${storeName}: Order was canceled`
            emailTemplate = emailTemplate.replace('{{logo}}', (encodeURI(this.url + logo || "")))
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replace('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replace('{{orderCode}}', orderCode)
            emailTemplate = emailTemplate.replace('{{orderCode}}', orderCode)
            emailTemplate = emailTemplate.replace('{{email}}', email)
            emailTemplate = emailTemplate.replace('{{supportEmail}}', supportEmail || "")
            emailTemplate = emailTemplate.replace('{{firstName}}', firstName)
            emailTemplate = emailTemplate.replace('{{total}}', total.toFixed(2))
            const listSelectedProduct: any = await this.selectedProductSellerService.find({ _id: { $in: listSelectedProductId } }, [{ path: 'productSellerId', select: '-description' }])
            let listProduct = ''

            for (const product of listSelectedProduct) {
                const { imageUrl, price, productSellerId, quantity, listValue } = product
                let oneProduct = `
                <tr>
                <td style="vertical-align:middle" align="left" width="17%"><a
                        style="display:block" title="" href="#" target="_blank">
                        <img style="display:block;font-family:Arial;width:80px; margin-top:8px"
                            src="${encodeURI(this.url + imageUrl) || ""}"
                            alt="" width="80" border="0">
                    </a>
                </td>
                <td style="padding:0 20px 0 20px;vertical-align:top" align="left"
                    width="83%">
                    <p
                        style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif; margin-top:8px">
                        <a style="text-decoration:none" title="ErgoStool" href="${domain}"
                            rel="noopener"
                            target="_blank">${productSellerId.productName}</a>
                    </p>
                    {{variant}}
                    <p
                        style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">
                        <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$${price}</span>
                        &nbsp;x&nbsp; <span
                            style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">${quantity}</span>
                    </p>
                </td>
                <td
                    style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">
                    $${(price * quantity).toFixed(2)}
                </td> 
                `
                if (listValue) {
                    let oneVariant = ''
                    if (listValue.length) {
                        let variant = listValue.join('/ ')
                        oneVariant = `
                        <p
                                style="font-size:15px;line-height:26px;color:#555;margin:4px 0px;font-family:sans-serif;">
                                ${variant}
                            </p>
                        `
                    }
                    oneProduct = oneProduct.replace('{{variant}}', oneVariant)
                } else {
                    oneProduct = oneProduct.replace('{{variant}}', '')
                }
                listProduct += oneProduct
            }

            emailTemplate = emailTemplate.replace('{{listProduct}}', listProduct)
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: subject
            }
            await this.sendEmail(adminId, paramSendMail)
        } catch (error) {
        }
    }

    async sendMailAbandoned1(adminId: string, abandoned: Partial<SendAbandoned>, { code, discount, discountType }: Partial<Coupon>, abandoSetting: AbandonedCheckout) {
        try {
            let { email, storeId, listSelectedProductId } = abandoned
            const [store, listSelectedProduct]: any = await Promise.all([
                this.storeService.findOne({
                    filter: { _id: storeId },
                    population: [
                        {
                            path: 'productSellerId',
                            select: 'productName listVariantSellerId',
                            populate: [{ path: 'listVariantSellerId', select: 'listValue imageId price', populate: [{ path: 'imageId' }] }]
                        }
                    ]
                }),
                this.selectedProductSellerService.find({ _id: { $in: listSelectedProductId } }, [{ path: 'productSellerId', select: '-description' }])

            ])
            const { storeName, supportEmail, _id, domain, productSellerId } = store
            const { productName, listVariantSellerId } = productSellerId
            const storeSetting: any = await this.storeSettingService.findStoreSetting(_id)
            const pathLogoDefault = '/logo/logozshop.png'
            const logo = storeSetting?.logo?.imageUrl || pathLogoDefault
            let emailTemplate = abandoSetting.emailTemplate
            let emailSubject = abandoSetting.emailSubject
            const subject = emailSubject.replaceAll('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replaceAll('{{logo}}', (encodeURI(this.url + logo || "")))
            emailTemplate = emailTemplate.replaceAll('{{supportEmail}}', supportEmail || "")
            emailTemplate = emailTemplate.replaceAll('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replaceAll('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replaceAll('{{email}}', email)
            emailTemplate = emailTemplate.replaceAll('{{coupon}}', code)
            emailTemplate = emailTemplate.replaceAll('{{linkProduct}}', `https://${domain}/checkout?coupon=${code}&abandonedId=${abandoned._id.toString()}`)
            emailTemplate = emailTemplate.replaceAll('{{productName}}', productName)
            emailTemplate = emailTemplate.replaceAll('{{abd-image}}', 'https://api.proxyv6.net/images/abd5.png')

            const discountValue = discountType == DiscountEnum.percent ? discount.toString() + '%' : '$ ' + discount.toString()
            emailTemplate = emailTemplate.replaceAll('{{discount}}', discountValue);
            let listProduct = ''
            for (const product of listSelectedProduct) {
                const { imageUrl, price, productSellerId, quantity, listValue } = product
                let variant = listValue.join('/ ')
                const varianta = `<tr>
                <td
                  style="vertical-align: middle"
                  align="left"
                  width="17%"
                >
                  <a
                    style="display: block"
                    title=""
                    target="_blank"
                    mcafee_aps="true"
                  >
                    <img
                      style="
                        display: block;
                        font-family: Arial;
                        width: 80px;
                      "
                     src="${encodeURI(this.url + imageUrl) || ""}" 
                      alt=""
                      width="80"
                      border="0"
                      class="CToWUd"
                      data-bit="iit"
                    />
                  </a>
                </td>
                <td
                  style="
                    padding: 0 20px 0 20px;
                    vertical-align: top;
                  "
                  align="left"
                  width="83%"
                >
                  <p
                    style="
                      font-size: 14px;
                      line-height: 26px;
                      color: #222;
                      font-weight: 600;
                      margin: 0;
                      font-family: sans-serif;
                    "
                  >
                    <a
                      style="text-decoration: none"
                      title="ErgoStool"
                      rel="noopener"
                      target="_blank"
                      mcafee_aps="true"
                      >${productSellerId.productName}</a
                    >
                  </p>
                  <p
                    style="
                      font-size: 14px;
                      line-height: 26px;
                      color: #555;
                      margin: 0;
                      font-family: sans-serif;
                    "
                  >${variant}</p>
                  <p
                    style="
                      font-size: 14px;
                      line-height: 26px;
                      color: #222;
                      margin: 0;
                      font-family: sans-serif;
                    "
                  >
                    <span
                      style="
                        font-size: 14px;
                        line-height: 26px;
                        color: #222;
                        margin: 0;
                        font-family: sans-serif;
                      "
                      >$${price}</span
                    >
                    &nbsp;x&nbsp;
                    <span
                      style="
                        font-size: 14px;
                        line-height: 26px;
                        color: #222;
                        margin: 0;
                        font-family: sans-serif;
                      "
                      >${quantity}</span
                    >
                  </p>
                </td>
                <td
                  style="
                    font-size: 14px;
                    line-height: 26px;
                    color: #222;
                    vertical-align: bottom;
                    font-weight: 600;
                    font-family: sans-serif;
                  "
                >
                $${(price * quantity).toFixed(2)}
                </td>
              </tr>`
                listProduct += varianta
            }
            emailTemplate = emailTemplate.replaceAll('{{listProduct}}', listProduct)

            // fs.writeFileSync('mail.html', emailTemplate);
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: subject
            }
            await this.sendEmail(adminId, paramSendMail)
        } catch (error) {
            console.log('error :>> ', error);
        }
    }

    async sendMailAbandoned2(adminId: string, abandoned: Partial<SendAbandoned>, { code, discount, discountType }: Partial<Coupon>, abandoSetting: AbandonedCheckout) {
        try {
            const { email, storeId, listSelectedProductId } = abandoned
            const [store, listSelectedProduct]: any = await Promise.all([
                this.storeService.findOne({
                    filter: { _id: storeId },
                    population: [
                        {
                            path: 'productSellerId',
                            select: 'productName listVariantSellerId',
                            populate: [{ path: 'listVariantSellerId', select: 'listValue imageId price', populate: [{ path: 'imageId' }] }]
                        }
                    ]
                }),
                this.selectedProductSellerService.find({ _id: { $in: listSelectedProductId } }, [{ path: 'productSellerId', select: '-description' }])

            ])
            const { storeName, supportEmail, _id, domain, productSellerId } = store
            const { productName, listVariantSellerId } = productSellerId
            const storeSetting: any = await this.storeSettingService.findStoreSetting(_id)
            const pathLogoDefault = '/logo/logozshop.png'
            const logo = storeSetting?.logo?.imageUrl || pathLogoDefault
            let emailTemplate = abandoSetting.emailTemplate
            let emailSubject = abandoSetting.emailSubject
            const subject = emailSubject.replaceAll('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replaceAll('{{logo}}', (encodeURI(this.url + logo || "")))
            emailTemplate = emailTemplate.replaceAll('{{supportEmail}}', supportEmail || "")
            emailTemplate = emailTemplate.replaceAll('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replaceAll('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replaceAll('{{email}}', email)
            emailTemplate = emailTemplate.replaceAll('{{coupon}}', code)
            emailTemplate = emailTemplate.replaceAll('{{linkProduct}}', `https://${domain}/checkout?coupon=${code}&abandonedId=${abandoned._id.toString()}`)
            emailTemplate = emailTemplate.replaceAll('{{productName}}', productName)
            const discountValue = discountType == DiscountEnum.percent ? discount.toString() + '%' : '$ ' + discount.toString()
            emailTemplate = emailTemplate.replaceAll('{{discount}}', discountValue)
            emailTemplate = emailTemplate.replaceAll('{{abd-image}}', 'https://api.proxyv6.net/images/abd10.png')
            let listProduct = ''
            for (const product of listSelectedProduct) {
                const { imageUrl, price, productSellerId, quantity, listValue } = product
                let variant = listValue.join('/ ')
                const varianta = `<tr>
                <td
                  style="vertical-align: middle"
                  align="left"
                  width="17%"
                >
                  <a
                    style="display: block"
                    title=""
                    target="_blank"
                    mcafee_aps="true"
                  >
                    <img
                      style="
                        display: block;
                        font-family: Arial;
                        width: 80px;
                      "
                     src="${encodeURI(this.url + imageUrl) || ""}" 
                      alt=""
                      width="80"
                      border="0"
                      class="CToWUd"
                      data-bit="iit"
                    />
                  </a>
                </td>
                <td
                  style="
                    padding: 0 20px 0 20px;
                    vertical-align: top;
                  "
                  align="left"
                  width="83%"
                >
                  <p
                    style="
                      font-size: 14px;
                      line-height: 26px;
                      color: #222;
                      font-weight: 600;
                      margin: 0;
                      font-family: sans-serif;
                    "
                  >
                    <a
                      style="text-decoration: none"
                      title="ErgoStool"
                      rel="noopener"
                      target="_blank"
                      mcafee_aps="true"
                      >${productSellerId.productName}</a
                    >
                  </p>
                  <p
                    style="
                      font-size: 14px;
                      line-height: 26px;
                      color: #555;
                      margin: 0;
                      font-family: sans-serif;
                    "
                  >${variant}</p>
                  <p
                    style="
                      font-size: 14px;
                      line-height: 26px;
                      color: #222;
                      margin: 0;
                      font-family: sans-serif;
                    "
                  >
                    <span
                      style="
                        font-size: 14px;
                        line-height: 26px;
                        color: #222;
                        margin: 0;
                        font-family: sans-serif;
                      "
                      >$${price}</span
                    >
                    &nbsp;x&nbsp;
                    <span
                      style="
                        font-size: 14px;
                        line-height: 26px;
                        color: #222;
                        margin: 0;
                        font-family: sans-serif;
                      "
                      >${quantity}</span
                    >
                  </p>
                </td>
                <td
                  style="
                    font-size: 14px;
                    line-height: 26px;
                    color: #222;
                    vertical-align: bottom;
                    font-weight: 600;
                    font-family: sans-serif;
                  "
                >
                $${(price * quantity).toFixed(2)}
                </td>
              </tr>`
                listProduct += varianta
            }
            emailTemplate = emailTemplate.replace('{{listProduct}}', listProduct)
            // fs.writeFileSync('mail2.html', emailTemplate);
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: subject
            }
            await this.sendEmail(adminId, paramSendMail)
        } catch (error) {
            console.log('error :>> ', error);
        }
    }

    async sendMailAbandoned3(adminId, abandoned: Partial<SendAbandoned>, { code, discount, discountType }: Partial<Coupon>, abandoSetting: AbandonedCheckout) {
        try {
            let { email, storeId, listSelectedProductId } = abandoned
            const [store, listSelectedProduct]: any = await Promise.all([
                this.storeService.findOne({
                    filter: { _id: storeId },
                    population: [
                        {
                            path: 'productSellerId',
                            select: 'productName listVariantSellerId',
                            populate: [{ path: 'listVariantSellerId', select: 'listValue imageId price', populate: [{ path: 'imageId' }] }]
                        }
                    ]
                }),
                this.selectedProductSellerService.find({ _id: { $in: listSelectedProductId } }, [{ path: 'productSellerId', select: '-description' }])
            ])
            const { storeName, supportEmail, _id, domain, productSellerId } = store
            const { productName, listVariantSellerId } = productSellerId
            const storeSetting: any = await this.storeSettingService.findStoreSetting(_id)
            const pathLogoDefault = '/logo/logozshop.png'
            const logo = storeSetting?.logo?.imageUrl || pathLogoDefault
            let emailTemplate = abandoSetting.emailTemplate
            let emailSubject = abandoSetting.emailSubject
            const subject = emailSubject.replaceAll('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replaceAll('{{logo}}', (encodeURI(this.url + logo || "")))
            emailTemplate = emailTemplate.replaceAll('{{supportEmail}}', supportEmail || "")
            emailTemplate = emailTemplate.replaceAll('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replaceAll('{{storeName}}', storeName)
            emailTemplate = emailTemplate.replaceAll('{{email}}', email)
            emailTemplate = emailTemplate.replaceAll('{{coupon}}', code)
            emailTemplate = emailTemplate.replaceAll('{{linkProduct}}', `https://${domain}/checkout?coupon=${code}&abandonedId=${abandoned._id.toString()}`)
            emailTemplate = emailTemplate.replaceAll('{{productName}}', productName)
            const discountValue = discountType == DiscountEnum.percent ? discount.toString() + '%' : '$ ' + discount.toString()
            emailTemplate = emailTemplate.replaceAll('{{discount}}', discountValue)
            emailTemplate = emailTemplate.replaceAll('{{abd-image}}', 'https://api.proxyv6.net/images/abd15.png')
            let listProduct = ''
            for (const product of listSelectedProduct) {
                const { imageUrl, price, productSellerId, quantity, listValue } = product
                let variant = listValue.join('/ ')
                const varianta = `<tr>
                <td
                  style="vertical-align: middle"
                  align="left"
                  width="17%"
                >
                  <a
                    style="display: block"
                    title=""
                    target="_blank"
                    mcafee_aps="true"
                  >
                    <img
                      style="
                        display: block;
                        font-family: Arial;
                        width: 80px;
                      "
                     src="${encodeURI(this.url + imageUrl) || ""}" 
                      alt=""
                      width="80"
                      border="0"
                      class="CToWUd"
                      data-bit="iit"
                    />
                  </a>
                </td>
                <td
                  style="
                    padding: 0 20px 0 20px;
                    vertical-align: top;
                  "
                  align="left"
                  width="83%"
                >
                  <p
                    style="
                      font-size: 14px;
                      line-height: 26px;
                      color: #222;
                      font-weight: 600;
                      margin: 0;
                      font-family: sans-serif;
                    "
                  >
                    <a
                      style="text-decoration: none"
                      title="ErgoStool"
                      rel="noopener"
                      target="_blank"
                      mcafee_aps="true"
                      >${productSellerId.productName}</a
                    >
                  </p>
                  <p
                    style="
                      font-size: 14px;
                      line-height: 26px;
                      color: #555;
                      margin: 0;
                      font-family: sans-serif;
                    "
                  >${variant}</p>
                  <p
                    style="
                      font-size: 14px;
                      line-height: 26px;
                      color: #222;
                      margin: 0;
                      font-family: sans-serif;
                    "
                  >
                    <span
                      style="
                        font-size: 14px;
                        line-height: 26px;
                        color: #222;
                        margin: 0;
                        font-family: sans-serif;
                      "
                      >$${price}</span
                    >
                    &nbsp;x&nbsp;
                    <span
                      style="
                        font-size: 14px;
                        line-height: 26px;
                        color: #222;
                        margin: 0;
                        font-family: sans-serif;
                      "
                      >${quantity}</span
                    >
                  </p>
                </td>
                <td
                  style="
                    font-size: 14px;
                    line-height: 26px;
                    color: #222;
                    vertical-align: bottom;
                    font-weight: 600;
                    font-family: sans-serif;
                  "
                >
                $${(price * quantity).toFixed(2)}
                </td>
              </tr>`
                listProduct += varianta
            }
            emailTemplate = emailTemplate.replace('{{listProduct}}', listProduct)
            const paramSendMail: ISendMail = {
                to: email,
                from: {
                    name: storeName,
                    email: supportEmail
                },
                html: emailTemplate,
                subject: subject
            }
            await this.sendEmail(adminId, paramSendMail)
        } catch (error) {
            console.log('error :>> ', error);
        }
    }


    async sendEmail(adminId: string, param: ISendMail): Promise<void> {
        try {
            const apiKey = await this.adminSettingService.getApikeySendgrid(adminId)
            client.setApiKey(apiKey)
            let { to, from, html, subject } = param
            console.log('from :>> ', from);
            await client.send({
                to: to,
                from,
                subject: subject,
                text: 'a',
                html: html
            })

        } catch (error) {
            // console.log(error.response.body, 'error')
            throw error

        }
    }


}