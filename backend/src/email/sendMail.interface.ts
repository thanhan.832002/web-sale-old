export interface ISendMail {
    to: string
    from: any
    subject: string
    html: string
}