import { Body, Controller, Get, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { TestEmailService } from './test-email.service';
import { EmailQuery } from './test-mail.dto';
import { AuthGuard } from '@nestjs/passport';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';

@Controller('test-email')
@ApiTags('Test Email')
@UseGuards(AdminGuard)
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
export class EmailController {
    constructor(
        private readonly testEmailService: TestEmailService
    ) { }
    @Post('order-confirm')
    testMailConfirm(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() { email }: EmailQuery) {
        return this.testEmailService.emailConfirmOrder(userId, email)
    }

    @Post('order-cancel')
    testMailCancel(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() { email }: EmailQuery) {
        return this.testEmailService.emailCancelOrder(userId, email)
    }

    @Post('order-tracking')
    testMailTracking(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() { email }: EmailQuery) {
        return this.testEmailService.emailAddTrackingOrder(userId, email)
    }

    @Post('change-shipping')
    testMailChangeShip(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() { email }: EmailQuery) {
        return this.testEmailService.emailChangeShipping(userId, email)
    }

    @Post('abandoned-first')
    testMailAbandoned1(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() { email }: EmailQuery) {
        return this.testEmailService.testMailAbandoned1(userId, email)
    }

    @Post('abandoned-second')
    testMailAbandoned2(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() { email }: EmailQuery) {
        return this.testEmailService.testMailAbandoned2(userId, email)
    }

    @Post('abandoned-third')
    testMailAbandoned3(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() { email }: EmailQuery) {
        return this.testEmailService.testMailAbandoned3(userId, email)
    }
}
