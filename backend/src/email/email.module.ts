import { Module } from '@nestjs/common';
import { EmailService } from './email.service';
import { StoreSettingModule } from 'src/store-setting/store-setting.module';
import { SelectedProductSellerModule } from 'src/selected-product-seller/selected-product-seller.module';
import { StoreModule } from 'src/store/store.module';
import { EmailController } from './email.controller';
import { TestEmailService } from './test-email.service';
import { AdminSettingService } from 'src/admin-setting/admin-setting.service';
import { AdminSettingModule } from 'src/admin-setting/admin-setting.module';
import { ProductModule } from 'src/product/product.module';
import { DomainModule } from 'src/domain/domain.module';
import { VariantSellerModule } from 'src/variant-seller/variant-seller.module';
import { AbandonedCheckoutModule } from 'src/abandoned-checkout/abandoned-checkout.module';

@Module({
  imports: [
    StoreSettingModule,
    SelectedProductSellerModule,
    StoreModule,
    ProductModule,
    AdminSettingModule,
    DomainModule,
    VariantSellerModule,
    AbandonedCheckoutModule
  ],
  providers: [EmailService, TestEmailService],
  exports: [EmailService, TestEmailService],
  controllers: [EmailController]
})
export class EmailModule { }
