export const orderTest = {
  "_id": {
    "$oid": "643a1db892d61a9870711c30"
  },
  "listSelectedProductId": [
    {
      "$oid": "643a1db892d61a9870711c2d"
    }
  ],
  "storeId": {
    "$oid": "643903f4cc2c6e9262e56d50"
  },
  "sellerId": {
    "$oid": "6438fbd11368e914a65b5031"
  },
  "paymentId": {
    "$oid": "64392fe550d0bdd71fa450d3"
  },
  "orderCode": "1_1681530296",
  "subTotal": 47.99,
  "discount": 10,
  "shipping": 10,
  "total": 47.99,
  "totalCost": 47.99,
  "totalQuantity": 1,
  "oldTrackingNumber": [],
  "orderStatus": "processing",
  "timeLine": [
    {
      "content": "Confirmation email was sent to: sb-zojce21535471@personal.example.com",
      "_id": {
        "$oid": "643a1dcc92d61a9870711c38"
      }
    }
  ],
  "createdAt": {
    "$date": "2023-04-15T03:44:56.299Z"
  },
  "updatedAt": {
    "$date": "2023-04-17T10:45:26.523Z"
  },
  "__v": 0,
  "paypal": {
    "paymentID": "PAYID-MQ5B3OQ4GV73579YE149952F",
    "payerID": "XL9TH2BR7K9EQ",
    "amount": 47.99,
    "fee": 2.16,
    "remain": 45.83,
    "_id": {
      "$oid": "643a1dcc92d61a9870711c3b"
    }
  },
  "searchName": "John DoeDoe John",
  "shippingAddress": {
    "email": "ngductai.0901.21@gmail.com",
    "firstName": "John",
    "lastName": "Doe",
    "address": "1 Main St",
    "zipCode": "95131",
    "city": "San Jose",
    "state": "CA",
    "country": "US",
    "phoneNumber": "",
    "_id": {
      "$oid": "643a1dcc92d61a9870711c3c"
    }
  },
  "trackingNumber": "YT1231321354"
}