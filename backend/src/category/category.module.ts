import { Module } from '@nestjs/common';
import { CategoryService } from './category.service';
import { CategoryAdminController } from './category-admin.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Category, CategorySchema } from './category.model';
import { ProductModule } from 'src/product/product.module';

@Module({
    providers: [CategoryService],
    controllers: [CategoryAdminController],
    imports: [
        MongooseModule.forFeature([{ name: Category.name, schema: CategorySchema }]),
        ProductModule
    ],
    exports: [CategoryService]
})
export class CategoryModule { }
