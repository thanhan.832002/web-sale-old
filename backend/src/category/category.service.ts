import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';
import { CreateCategoryDto, UpdateCategoryDto } from './category.dto';
import { Category } from './category.model';
import * as _ from 'lodash'
import * as Promise from 'bluebird'
import { BaseService } from 'src/base/base.service';
import { DataFilter, PaginationData } from 'src/base/interfaces/data.interface';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { ProductService } from 'src/product/product.service';

@Injectable()
export class CategoryService extends BaseService<Category> {
    constructor(
        @InjectModel(Category.name) private readonly categoryModel: Model<Category>,
        private readonly productService: ProductService
    ) { super(categoryModel) }

    async getCategoryDetail(categorySlug: string) {
        try {
            const found = await this.findCategoryBySlug(categorySlug)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: found
            }
        } catch (error) {
            throw error
        }
    }

    async createCategory(createCategoryDto: CreateCategoryDto) {
        try {
            const { slug, listProductId } = createCategoryDto
            const exists = await this.categoryModel.exists({ slug })
            if (exists) {
                throw new BaseApiException({
                    message: BaseNoti.CATEGORY.CATEGORY_ALREADY_EXISTS
                })
            }
            const categoryIns = new this.categoryModel(createCategoryDto)
            await categoryIns.save()
            if (listProductId && listProductId.length) {
                await this.productService.changeCategory({ listProductId, categoryId: categoryIns._id })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: categoryIns
            }
        } catch (error) {

            throw error
        }
    }

    async listCategoryAdmin({ search, page, limit, sort, filter }: BaseQuery) {
        try {
            const filterProduct: DataFilter<Category> = {
                limit,
                page,
                condition: {},
                population: [],
            }
            filterProduct.condition = { ...filterProduct.condition, parentId: { $exists: false } }
            if (filter) {
                filterProduct.condition = { ...filterProduct.condition, ...filter }
            }
            if (search) {
                filterProduct.condition['$or'] = [
                    { name: { '$regex': search, '$options': 'i' } },
                    { slug: { '$regex': search, '$options': 'i' } },
                ]
            }
            let list: PaginationData<Category> = await this.getListDocument(filterProduct)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: list
            }
        } catch (error) {
            throw error
        }
    }

    async updateCategory(categoryId: string, updateCategoryDto: UpdateCategoryDto) {
        try {
            const { slug, listProductId } = updateCategoryDto
            const foundCategory = await this.findCategoryById(categoryId)
            if (slug) {
                const exists = await this.categoryModel.exists({ slug, _id: { $ne: categoryId } })
                if (exists) {
                    throw new BaseApiException({
                        message: BaseNoti.PRODUCT.SLUG_ALREADY_EXISTS
                    })
                }
            }
            const validField = _.pickBy(updateCategoryDto)

            const updatedCat = await this.categoryModel.findByIdAndUpdate(categoryId, validField, { new: true })
            if (listProductId && listProductId.length) {
                await this.productService.changeCategory({ listProductId, categoryId: foundCategory._id })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: updatedCat
            }
        } catch (error) {
            throw error
        }
    }

    async deleteCategory(categoryId: string) {
        try {
            const foundCategory = await this.findCategoryById(categoryId.trim())
            await this.categoryModel.findByIdAndDelete(foundCategory._id)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async findCategoryById(categoryId: string) {
        try {
            const foundCategory = await this.categoryModel.findById(categoryId)
            if (!foundCategory) {
                throw new BaseApiException({
                    message: BaseNoti.CATEGORY.CATEGORY_NOT_EXISTS
                })
            }
            return foundCategory
        } catch (error) {
            throw error
        }
    }

    async findCategoryBySlug(categorySlug: string) {
        try {
            const foundCategory = await this.categoryModel.findOne({ slug: categorySlug })
            if (!foundCategory) {
                throw new BaseApiException({
                    message: BaseNoti.CATEGORY.CATEGORY_NOT_EXISTS
                })
            }
            return foundCategory
        } catch (error) {
            throw error
        }
    }

    async findSubCategoryById(categoryId: string) {
        try {
            const foundCategory = await this.categoryModel.findById(categoryId)
            if (!foundCategory) {
                throw new BaseApiException({
                    message: BaseNoti.CATEGORY.CATEGORY_NOT_EXISTS
                })
            }
            const listSubCat = await this.categoryModel.find({ parentId: foundCategory._id })
            const listSubCatId = listSubCat.map(cat => cat._id)
            return listSubCatId
        } catch (error) {
            throw error
        }
    }

    async findSubCategoryBySlug(categorySlug: string) {
        try {
            const foundCategory = await this.categoryModel.findOne({ slug: categorySlug })
            if (!foundCategory) {
                throw new BaseApiException({
                    message: BaseNoti.CATEGORY.CATEGORY_NOT_EXISTS
                })
            }
            const listSubCat = await this.categoryModel.find({ parentId: foundCategory._id })
            const listSubCatId = listSubCat.map(cat => cat._id)
            return listSubCatId
        } catch (error) {
            throw error
        }
    }

    async insertCategory(cateArray: any[]) {
        try {
            await this.categoryModel.insertMany(cateArray)
        } catch (error) {
            throw error
        }
    }
    async findCategoryByName(name: string) {
        try {
            const tag = await this.categoryModel.findOne({ name })
            return tag
        } catch (error) {
            throw error
        }
    }

    async updateCategoryCron(categoryId: string, updateField: any) {
        try {
            await this.categoryModel.findByIdAndUpdate(categoryId, updateField)
        } catch (error) {
            throw error
        }
    }

    async listCategoryId() {
        try {
            const list = await this.categoryModel.find().select('id')
            return list.map(category => category._id)
        } catch (error) {
            throw error
        }
    }

    createSlug(name: string) {
        try {
            name = name.length < 40 ? name : name.substr(0, 40)
            let slug = name.trim().toLowerCase();
            slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
            slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
            slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
            slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
            slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
            slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
            slug = slug.replace(/đ/gi, 'd');
            slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
            slug = slug.split(' ').filter(char => char != '' && char != undefined && char != null).join('-')
            return slug
        } catch (error) {
            throw error
        }
    }
}
