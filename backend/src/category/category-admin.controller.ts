import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { CreateCategoryDto, UpdateCategoryDto } from './category.dto';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { CategoryService } from './category.service';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
@Controller('category')
@ApiTags('Category Admin')
@ApiBearerAuth()
@UseGuards(AdminGuard)
@UseGuards(AuthGuard('jwt'))
export class CategoryAdminController {
    constructor(
        private readonly categoryService: CategoryService
    ) { }

    @Post('create')
    async createCategory(
        @Body() createCategoryDto: CreateCategoryDto,
    ) {
        return this.categoryService.createCategory(createCategoryDto)
    }

    @Get('detail/:categorySlug')
    async getCategoryDetail(@Param('categorySlug') categorySlug: string) {
        return this.categoryService.getCategoryDetail(categorySlug)
    }

    @Get('list')
    async listCategory(
        @Query() categoryQuery: BaseQuery
    ) {
        return this.categoryService.listCategoryAdmin(categoryQuery)
    }

    @Put('update/:categoryId')
    async updateCategory(
        @Body() updateCategoryDto: UpdateCategoryDto,
        @Param('categoryId') categoryId: string,
    ) {
        return this.categoryService.updateCategory(categoryId, updateCategoryDto)
    }

    @Delete('delete/:categoryId')
    async deleteCategory(
        @Param('categoryId') categoryId: string
    ) {
        return this.categoryService.deleteCategory(categoryId)
    }
}
