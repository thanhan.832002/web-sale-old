import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsOptional, Min, Validate } from "class-validator";
import { Schema, Document, Types } from "mongoose";
import { schemaOptions } from "src/base/base.shema";
import { IsObjectId } from "src/base/custom-validator";

export const CategorySchema: Schema = new Schema({
    name: String,
    slug: String,
    desc: String,
    status: {
        type: String,
        default: 'open'
    },

    // numberProduct: {
    //     type: Number,
    //     default: 0
    // },
}, { ...schemaOptions, ... { collection: 'Category' } })

export class Category extends Document {
    @ApiProperty()
    @IsNotEmpty()
    name: string

    @ApiProperty()
    @IsNotEmpty()
    slug: string

    @ApiProperty()
    @IsNotEmpty()
    desc: string

    @ApiProperty()
    @IsNotEmpty()
    status: string

    // @ApiProperty()
    // @IsOptional()
    // @IsNumber()
    // @Min(0)
    // numberProduct: number
}

