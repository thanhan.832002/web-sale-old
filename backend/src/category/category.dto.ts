import { ApiProperty, ApiPropertyOptional, PickType } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsEnum, IsNotEmpty, IsOptional, Validate } from "class-validator";
import { IsObjectId } from "src/base/custom-validator";
import { Category } from "./category.model";
export class CreateCategoryDto extends PickType(Category, ['name', 'slug', 'desc']) {
    @ApiProperty({
        type: [String]
    })
    @IsOptional()
    listProductId: string[]
}

export class UpdateCategoryDto {
    @ApiProperty()
    @IsOptional()
    name: string

    @ApiProperty()
    @IsOptional()
    slug: string

    @ApiProperty()
    @IsOptional()
    desc: string

    @ApiProperty({
        type: [String]
    })
    @IsOptional()
    listProductId: string[]
}
export class CategorySlugQuery {
    @ApiPropertyOptional()
    @IsOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    categorySlug: string

    @ApiPropertyOptional()
    @IsOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    search: string
}

export class CategoryIdQuery {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsOptional()
    categoryId: string
}