import { ValidationPipe, Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AllExceptionsFilter } from './base/exception/exception.filter';
import { ResponseInterceptors } from './base/interceptors/response.interceptor';
import { json, urlencoded } from 'express';
import * as Sentry from '@sentry/node';
async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule);

    app.set('trust proxy', 'loopback')
    const config = new DocumentBuilder()
        .setTitle('Websale Premium')
        .setDescription('Power By: ZTECH.ASIA')
        .addBearerAuth()
        .setVersion('1.0')
        .build();

    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);
    Sentry.init({
        dsn: 'https://9e1bfdd4da76aded4694265ad96e4389@o4506245734596608.ingest.sentry.io/4506257780703232'
    });
    app.enableCors();
    app.useGlobalFilters(new AllExceptionsFilter())
    app.useGlobalPipes(new ValidationPipe({
        whitelist: true,
        transform: true
    }));
    app.useGlobalInterceptors(new ResponseInterceptors());
    app.use(json({ limit: '100mb' }));//Limit request body: 100mb
    app.use(urlencoded({ extended: true, limit: '100mb' }));//Limit request body: 100mb
    await app.listen(2000);
}
bootstrap();
