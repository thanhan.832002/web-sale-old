import { ApiProperty, ApiPropertyOptional, PickType } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import {
  ValidationOptions,
  registerDecorator,
  ValidationArguments,
  IsNotEmpty,
  IsOptional,
  Validate,
  IsNumber,
  IsInt,
  IsEnum,
  Min,
  Max,
  MinLength,
  Matches,
  IsBoolean,
} from 'class-validator';
import { IsObjectId } from 'src/base/custom-validator';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { User } from './user.model';

function IsRef(property: string, validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'IsRef',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: string, args: ValidationArguments) {
          const [relatedPropertyName] = args.constraints;
          const relatedValue = (args.object as any)[relatedPropertyName];
          return value === relatedValue;
        },
      },
    });
  };
}

export class ChangePassDto {
  @ApiProperty()
  @IsNotEmpty()
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  password: string;

  @ApiProperty()
  @IsNotEmpty()
  newPassword: string;

  @ApiProperty()
  @IsNotEmpty()
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  @IsRef('newPassword', { message: 'PASSWORD_CONFIRM_NOT_CORRECT' })
  passwordConfirm: string;
}

export class ForgotPasswordDto extends PickType(User, ['email']) {
  @ApiProperty()
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  @IsNotEmpty()
  domain: string;
}

export class CreateNewPasswordDto extends PickType(User, ['password']) {
  @ApiProperty()
  @IsNotEmpty()
  token: string;
}

export class UpdateUserDto {
  @ApiPropertyOptional()
  @IsOptional()
  name: string;

  @ApiPropertyOptional()
  @IsOptional()
  phoneNumber: string;
}
export class AdminActiveUserDto {
  @ApiProperty()
  @Validate(IsObjectId)
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  @IsOptional()
  userId: string;
}

export class UserIdQuery extends BaseQuery {
  @ApiPropertyOptional()
  @Validate(IsObjectId)
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  @IsOptional()
  userId: string;
}

export class UserQuery extends BaseQuery {
  @ApiPropertyOptional()
  @IsBoolean()
  @Transform(
    ({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1,
  )
  @IsOptional()
  isActive: boolean;
}

export class AdminCreateSellerDto extends PickType(User, [
  'email',
  'password',
  'phoneNumber',
]) {
  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @ApiPropertyOptional()
  @Matches(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  )
  @Transform(({ value }) => {
    if (value) return value.toLowerCase().trim();
  })
  @IsOptional()
  supportEmail: string;
}

export class CreateAdminDto extends PickType(User, [
  'email',
  'password',
  'orderPrefix',
]) {
  @ApiPropertyOptional()
  @IsOptional()
  code: string;
}
export class ChangeAdminStatusDto {
  @ApiProperty()
  @Validate(IsObjectId)
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  @IsNotEmpty()
  adminId: string;

  @ApiProperty()
  @IsBoolean()
  @Transform(
    ({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1,
  )
  @IsNotEmpty()
  isActive: boolean;

  @ApiPropertyOptional()
  @IsOptional()
  code: string;
}

export class AddAdminPrefixDto {
  @ApiProperty()
  @Validate(IsObjectId)
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  @IsNotEmpty()
  adminId: string;

  @ApiProperty()
  @IsNotEmpty()
  orderPrefix: string;

  @ApiPropertyOptional()
  @IsOptional()
  code: string;
}

export class Verify2faDto {
  @ApiProperty()
  @IsNotEmpty()
  code: string;

  @ApiProperty()
  @IsNotEmpty()
  password: string;
}

export class TurnOn2faDto {
  @ApiProperty()
  @IsNotEmpty()
  password: string;
}
