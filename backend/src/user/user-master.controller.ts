import { Post, Put, Controller, Get, Query, UseGuards, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UserService } from './user.service';
import { MasterGuard } from 'src/base/guard/master.guard';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { AddAdminPrefixDto, ChangeAdminStatusDto, CreateAdminDto, UserQuery } from './user.dto';

@Controller('user-master')
@ApiTags('User Master')
@UseGuards(MasterGuard)
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
export class UserMasterController {
    constructor(
        private readonly userService: UserService
    ) { }

    @Get('list-admin')
    async masterGetListAdmin(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: UserQuery
    ) {
        return this.userService.listAdmin(userId, query)
    }

    @Post('create-admin')
    async createAdmin(
        @GetUser() user: JwtTokenDecrypted,
        @Body() createAdminDto: CreateAdminDto
    ) {
        return this.userService.createAdmin(user, createAdminDto)
    }

    @Put('change-status')
    async masterChangeStatusAdmin(
        @GetUser() user: JwtTokenDecrypted,
        @Body() changeAdminStatus: ChangeAdminStatusDto
    ) {
        return this.userService.masterChangeAdminStatus(user, changeAdminStatus)
    }

    @Put('add-prefix')
    async masterAddPrefix(
        @GetUser() user: JwtTokenDecrypted,
        @Body() addAdminPrefixDto: AddAdminPrefixDto
    ) {
        return await this.userService.masterAddAdminPrefix(user, addAdminPrefixDto)
    }

}