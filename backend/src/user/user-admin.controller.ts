import { Body, Controller, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { AdminActiveUserDto, AdminCreateSellerDto, UserQuery } from './user.dto';
import { UserService } from './user.service';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';

@Controller('user-admin')
@ApiTags('User Admin')
@UseGuards(AdminGuard)
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
export class UserAdminController {
    constructor(
        private readonly userService: UserService
    ) { }

    @Get('list-seller')
    adminGetListSeller(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: UserQuery
    ) {
        return this.userService.listSeller(userId, query)
    }

    @Get('detail-seller/:sellerId')
    adminGetDetailSeller(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('sellerId') sellerId: string) {
        return this.userService.adminGetDetailSeller(userId, sellerId)
    }

    @Put('active-user')
    adminActiveUser(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() blockSellerDto: AdminActiveUserDto) {
        return this.userService.adminActiveUser(userId, blockSellerDto)
    }

    @Put('deactive-user')
    adminDeactiveUser(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() blockSellerDto: AdminActiveUserDto) {
        return this.userService.adminDeactiveUser(userId, blockSellerDto)
    }

    @Post('create-seller')
    adminCreateSeller(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() adminCreateSellerDto: AdminCreateSellerDto
    ) {
        return this.userService.adminCreateSeller(userId, adminCreateSellerDto)
    }
}