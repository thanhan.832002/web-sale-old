import { Schema, Document, Types } from 'mongoose';
import { schemaOptions } from 'src/base/base.shema';
import { hashSync, genSaltSync } from 'bcrypt';
import {
  IsBoolean,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Transform } from 'class-transformer';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Role } from 'src/base/enum/roles.enum';
export const TwofaSchema: Schema = new Schema({
  enable: Boolean,
  secret: String,
});

export const UserSchema: Schema = new Schema(
  {
    email: {
      type: String,
    },
    password: {
      type: String,
    },
    role: {
      type: Number,
      default: Role.seller,
    },
    isActive: {
      type: Boolean,
      default: false,
    },
    verified: {
      type: Boolean,
      default: false,
    },
    twofa: {
      type: TwofaSchema,
    },
    phoneNumber: {
      type: String,
    },
    supportEmail: {
      type: String,
    },
    name: {
      type: String,
    },
    adminId: {
      type: Types.ObjectId,
      ref: 'User',
    },
    orderPrefix: {
      type: String,
    },
  },
  { ...schemaOptions, ...{ collection: 'User' } },
);

export class TwofaDto {
  enable: boolean;
  secret: string;
}

UserSchema.pre('save', function (this: User, next: any) {
  if (this.password) {
    const salt = genSaltSync(10);
    this.password = hashSync(this.password, salt);
  }
  next();
});

export class User extends Document {
  @ApiPropertyOptional()
  @IsOptional()
  adminId: string;

  @ApiProperty()
  @Matches(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  )
  @Transform(({ value }) => {
    if (value) return value.toLowerCase().trim();
  })
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  @MaxLength(30)
  @MinLength(6)
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  @IsNotEmpty()
  password: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(Role)
  role: Role;

  @ApiProperty()
  @Matches(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  )
  @Transform(({ value }) => {
    if (value) return value.toLowerCase().trim();
  })
  @IsNotEmpty()
  supportEmail: string;

  @ApiProperty()
  @Transform(({ value }) => {
    if (value) return value.toLowerCase().trim();
  })
  @IsNotEmpty()
  phoneNumber: string;

  @ApiProperty()
  @IsNotEmpty()
  isActive: boolean;

  @ApiProperty()
  @IsBoolean()
  @Transform(
    ({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1,
  )
  @IsNotEmpty()
  verified: boolean;

  @ApiProperty()
  @IsOptional()
  twofa: TwofaDto;

  @ApiPropertyOptional()
  @IsOptional()
  name: string;

  @ApiPropertyOptional()
  @IsOptional()
  orderPrefix: string;
}
