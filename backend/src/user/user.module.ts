import { forwardRef, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { SharedModule } from 'src/base/shared.module';
import { EmailModule } from 'src/email/email.module';
import { UserMasterController } from './user-master.controller';
import { UserAdminController } from './user-admin.controller';
import { UserController } from './user.controller';
import { User, UserSchema } from './user.model';
import { UserService } from './user.service';
import { ShippingModule } from 'src/shipping/shipping.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    forwardRef(() => EmailModule),
    SharedModule,
    ConfigModule,
    EmailModule,
    ShippingModule
  ],
  controllers: [UserController, UserAdminController, UserMasterController],
  providers: [UserService],
  exports: [UserService]
})
export class UserModule { }
