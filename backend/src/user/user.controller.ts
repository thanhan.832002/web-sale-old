import { Body, Controller, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { ChangePassDto, CreateNewPasswordDto, ForgotPasswordDto, TurnOn2faDto, UpdateUserDto, Verify2faDto } from './user.dto';
import { UserService } from './user.service';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { AuthGuard } from '@nestjs/passport';
import { AdminGuard } from 'src/base/guard/admin.guard';

@Controller('user')
@ApiTags('User')

export class UserController {
    constructor(
        private readonly userService: UserService,
    ) { }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @Get('me')
    getMe(@GetUser() { userId }: JwtTokenDecrypted) {
        return this.userService.getMe(userId)
    }

    @ApiBearerAuth()
    @UseGuards(AdminGuard)
    @UseGuards(AuthGuard('jwt'))
    @Put('change-password')
    changePassword(@GetUser() user: JwtTokenDecrypted, @Body() changePassDto: ChangePassDto) {
        return this.userService.changePassword(user, changePassDto)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @Put('update')
    sellerUpdateUser(@GetUser() { userId }: JwtTokenDecrypted, @Body() updateUserDto: UpdateUserDto) {
        return this.userService.sellerUpdateUser(userId, updateUserDto)
    }

    // @Put('forgot-password')
    // forgotPassword(@Body() forgotPasswordDto: ForgotPasswordDto, @Body() { domain }: ForgotPasswordDto) {
    //     return this.userService.forgotPassword(forgotPasswordDto, domain)
    // }

    // @Put('create-new-password')
    // createNewPassword(@Body() createNewPasswordDto: CreateNewPasswordDto) {
    //     return this.userService.createNewPassword(createNewPasswordDto)
    // }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @Post('turn-on-2fa')
    async turnOn2fa(@GetUser() user: JwtTokenDecrypted, @Body() turnOn2faDto: TurnOn2faDto
    ) {
        return this.userService.turnOnTwoFa(user, turnOn2faDto)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @Post('verify-2fa')
    async verify2fa(@GetUser() { userId }: JwtTokenDecrypted, @Body() verify2faDto: Verify2faDto
    ) {
        return this.userService.verify2fa(userId, verify2faDto)
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @Post('turn-off-2fa')
    async turnOff2fa(@GetUser() { userId }: JwtTokenDecrypted, @Body() turnOn2faDto: TurnOn2faDto) {
        return this.userService.turnOff2fa(userId, turnOn2faDto)
    }
}