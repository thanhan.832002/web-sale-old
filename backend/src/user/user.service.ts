import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';
import { AddAdminPrefixDto, AdminActiveUserDto, AdminCreateSellerDto, ChangeAdminStatusDto, ChangePassDto, CreateAdminDto, CreateNewPasswordDto, ForgotPasswordDto, TurnOn2faDto, UpdateUserDto, UserQuery, Verify2faDto } from './user.dto';
import { User } from './user.model';
import { compareSync, genSaltSync, hashSync } from 'bcrypt';
import { ModuleRef } from '@nestjs/core';
import { AuthService } from 'src/auth/auth.service';
import { BaseService } from 'src/base/base.service';
import { DataFilter } from 'src/base/interfaces/data.interface';
import { Promise } from "bluebird";
import * as moment from 'moment'
import { Role } from 'src/base/enum/roles.enum';
import { CacheService } from 'src/base/caching/cache.service';
import { JwtPayload, JwtTokenDecrypted } from 'src/auth/auth.dto';
import { EmailService } from 'src/email/email.service';
import { ConfigService } from '@nestjs/config';
import { verify } from 'jsonwebtoken';
import { TwofactorProcesser } from 'src/base/twofa/twofa.class';
import { ShippingService } from 'src/shipping/shipping.service';
import { authenticator } from 'otplib';
import { KeyRedisEnum } from 'src/view/view.enum';
import { Timeout } from '@nestjs/schedule';
@Injectable()
export class UserService extends BaseService<User> implements OnModuleInit {
    private authService: AuthService
    constructor(
        @InjectModel(User.name) private readonly userModel: Model<User>,
        private readonly cacheService: CacheService,
        private readonly configService: ConfigService,
        private readonly emailService: EmailService,
        private readonly shippingService: ShippingService,
        private moduleRef: ModuleRef
    ) { super(userModel) }

    onModuleInit() {
        this.authService = this.moduleRef.get(AuthService, { strict: false });
    }

    async createAdmin({ userId }: JwtTokenDecrypted, { email, password, orderPrefix, code }: CreateAdminDto) {
        try {
            const foundUser = await this.userModel.findById(userId)
            if (foundUser && foundUser.twofa && foundUser.twofa.enable) {
                if (!code) {
                    throw new BaseApiException({
                        message: BaseNoti.AUTH.MISSING_2FA
                    })
                }
                const toStrCode = code.toString()
                const isValid = authenticator.verify({ token: toStrCode, secret: foundUser.twofa.secret });
                if (!isValid) {
                    throw new BaseApiException({
                        message: BaseNoti.AUTH.INVALID_2FA
                    })
                }
            }
            const existedEmail = await this.userModel.exists({ email })
            if (existedEmail) {
                throw new BaseApiException({
                    message: BaseNoti.USER.USERNAME_EXISTS
                })
            }
            const existedOrderPrefix = await this.userModel.exists({ orderPrefix })
            if (existedOrderPrefix) {
                throw new BaseApiException({
                    message: BaseNoti.USER.ORDER_PREFIX_EXISTS
                })
            }
            const newAdmin = new this.userModel({
                email,
                password,
                orderPrefix,
                isActive: true,
                verified: true,
                role: Role.admin
            })
            await newAdmin.save()
            await this.shippingService.createDefaultROW(newAdmin._id)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async masterChangeAdminStatus({ userId }: JwtTokenDecrypted, { adminId, isActive, code }: ChangeAdminStatusDto) {
        try {
            const foundUser = await this.userModel.findById(userId)
            if (foundUser && foundUser.twofa && foundUser.twofa.enable) {
                if (!code) {
                    throw new BaseApiException({
                        message: BaseNoti.AUTH.MISSING_2FA
                    })
                }
                const toStrCode = code.toString()
                const isValid = authenticator.verify({ token: toStrCode, secret: foundUser.twofa.secret });
                if (!isValid) {
                    throw new BaseApiException({
                        message: BaseNoti.AUTH.INVALID_2FA
                    })
                }
            }
            const foundAdmin = await this.userModel.findByIdAndUpdate(adminId, { isActive })
            if (!isActive) {
                await this.authService.removeToken(adminId)
                const listSeller = await this.userModel.find({ role: Role.seller, adminId }).select('id')
                await Promise.map(listSeller, async (seller) => {
                    await Promise.all([
                        this.authService.removeToken(seller._id),
                        this.userModel.findByIdAndUpdate(seller._id, { isActive: false })
                    ])
                })
            }
            if (!foundAdmin) {
                throw new BaseApiException({
                    message: BaseNoti.USER.ADMIN_NOT_EXISTS
                })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async masterAddAdminPrefix({ userId }: JwtTokenDecrypted, { adminId, orderPrefix, code }: AddAdminPrefixDto) {
        try {
            const foundUser = await this.userModel.findById(userId)
            if (foundUser && foundUser.twofa && foundUser.twofa.enable) {
                if (!code) {
                    throw new BaseApiException({
                        message: BaseNoti.AUTH.MISSING_2FA
                    })
                }
                const toStrCode = code.toString()
                const isValid = authenticator.verify({ token: toStrCode, secret: foundUser.twofa.secret });
                if (!isValid) {
                    throw new BaseApiException({
                        message: BaseNoti.AUTH.INVALID_2FA
                    })
                }
            }
            const foundAdmin = await this.userModel.findById(adminId)
            if (!foundAdmin) {
                throw new BaseApiException({
                    message: BaseNoti.USER.ADMIN_NOT_EXISTS
                })
            }
            const existedOrderPrefix = await this.userModel.exists({ orderPrefix, _id: { $ne: foundAdmin._id } })
            if (existedOrderPrefix) {
                throw new BaseApiException({
                    message: BaseNoti.USER.ORDER_PREFIX_EXISTS
                })
            }
            await this.userModel.findByIdAndUpdate(adminId, { orderPrefix })
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async getMe(userId: string) {
        try {
            const foundMe = await this.userModel.findById(userId).select('-__v -password')
            if (!foundMe) {
                throw new BaseApiException({
                    message: BaseNoti.USER.USER_NOT_EXISTS
                })
            }
            return {
                message: BaseNoti.SUCCESS,
                data: foundMe
            }
        } catch (error) {
            throw error
        }
    }

    async changePassword({ userId }: JwtTokenDecrypted, changePassDto: ChangePassDto) {
        try {
            const user = await this.findUserById(userId);
            const { password, newPassword, passwordConfirm } = changePassDto;
            if (user.password) {
                const passwordValid = compareSync(password, user.password);
                if (!passwordValid) {
                    throw new BaseApiException({
                        message: BaseNoti.AUTH.WRONG_PASSWORD,
                    });
                }
            }
            const salt = genSaltSync(10);
            const encPassword = hashSync(newPassword, salt);
            await this.userModel.findByIdAndUpdate(userId, { password: encPassword });
            const refreshTokenPayload: JwtPayload = {
                userId: user._id,
                tokenType: 'refreshToken',
            };
            if (user.role != undefined) {
                refreshTokenPayload.role = user.role;
            }
            const accessTokenPayload: JwtPayload = {
                userId: user._id,
                tokenType: 'refreshToken',
            };
            if (user.role != undefined) {
                refreshTokenPayload.role = user.role;
                accessTokenPayload.role = user.role;
            }
            await this.authService.accessTokenModel.deleteMany({ userId })
            const [accessToken, refreshToken] = await Promise.all([
                this.authService.signPayload(accessTokenPayload),
                this.authService.signPayload(refreshTokenPayload),
            ]);
            await Promise.all([
                this.authService.saveAccessToken(accessToken),
                this.authService.saveRefreshToken(refreshToken),
            ]);
            return {
                message: BaseNoti.SUCCESS,
                tokens: {
                    accessToken: accessToken.token,
                    refreshToken: refreshToken.token,
                },
            };
        } catch (error) {
            throw error;
        }
    }


    async forgotPassword(forgotPasswordDto: ForgotPasswordDto, domain: string) {
        try {
            const { email } = forgotPasswordDto
            const foundUser = await this.userModel.findOne({ email })
            if (!foundUser) {
                throw new BaseApiException({
                    message: BaseNoti.USER.USER_NOT_EXISTS
                })
            }
            const tokenData = {
                email,
                id: foundUser._id
            }
            const token = await this.authService.signVerifyToken(tokenData)
            const adminId = foundUser.adminId ? foundUser.adminId : foundUser._id
            await this.emailService.emailForgotPassword(adminId, email, token, domain)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async createNewPassword(createNewPassword: CreateNewPasswordDto) {
        try {
            const secret = await this.configService.get<string>('JWT_SECRET')
            const { token, password } = createNewPassword
            const data = verify(token, secret)
            // @ts-ignore
            const { email, id, exp } = data
            const now = moment().unix()
            if (!email || !id) return
            if (exp < now) {
                throw new BaseApiException({
                    message: BaseNoti.AUTH.TOKEN_EXPIRED
                })
            }
            const salt = genSaltSync(10)
            const encPassword = hashSync(password, salt)
            await this.userModel.findByIdAndUpdate(id, { password: encPassword })
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async turnOnTwoFa({ userId }: JwtTokenDecrypted, twofaDto: TurnOn2faDto) {
        try {

            const user = await this.findUserById(userId);
            const { password } = twofaDto;
            if (!user.isActive) {
                throw new BaseApiException({
                    message: BaseNoti.AUTH.USERNAME_BANNED,
                });
            }
            if (!user.password) {
                throw new BaseApiException({
                    message: BaseNoti.AUTH.GOOGLE_LOGIN
                })
            }
            const passwordValid = compareSync(password, user.password);
            if (!passwordValid) {
                throw new BaseApiException({
                    message: BaseNoti.AUTH.WRONG_PASSWORD,
                });
            }
            const secret = authenticator.generateSecret();
            await this.userModel.findByIdAndUpdate(userId, {
                twofa: { enable: false, secret: secret },
            });
            const twofactorProcesser = new TwofactorProcesser(user.email, secret);
            const qrCode = await twofactorProcesser.qRCodeImage();
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: qrCode,
            };
        } catch (error) {
            throw error;
        }
    }

    async turnOff2fa(userId: string, turnOn2faDto: TurnOn2faDto) {
        try {
            const { password } = turnOn2faDto;
            const user = await this.findUserById(userId);
            if (!user) {
                throw new BaseApiException({
                    message: BaseNoti.AUTH.USERNAME_NOT_EXISTS,
                });
            }
            if (!user.isActive) {
                throw new BaseApiException({
                    message: BaseNoti.AUTH.USERNAME_BANNED,
                });
            }
            const passwordValid = compareSync(password, user.password);
            if (!passwordValid) {
                throw new BaseApiException({
                    message: BaseNoti.AUTH.WRONG_PASSWORD,
                });
            }
            const newUser = await this.userModel.findByIdAndUpdate(userId, {
                $set: { 'twofa.enable': false },
            }, { new: true }).select('-password -twofa.secret');
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: newUser,
            };
        } catch (error) {
            throw error;
        }
    }

    async verify2fa(userId: string, verify2faDto: Verify2faDto) {
        try {
            const { code, password } = verify2faDto;
            const foundUser = await this.userModel.findById(userId);
            if (!foundUser) return;
            if (!foundUser.twofa || !foundUser.twofa.secret) return;
            if (foundUser.twofa.enable) {
                throw new BaseApiException({
                    message: BaseNoti.AUTH.TWOFA_IS_ON,
                });
            }
            const passwordValid = compareSync(password, foundUser.password);
            if (!passwordValid) {
                throw new BaseApiException({
                    message: BaseNoti.AUTH.WRONG_PASSWORD,
                });
            }
            const secret = foundUser.twofa.secret;
            let codeString = code.toString();
            const isValid = authenticator.check(codeString, secret);
            if (!isValid) {
                throw new BaseApiException({
                    message: BaseNoti.AUTH.INVALID_2FA,
                });
            }
            const newUser = await this.userModel.findByIdAndUpdate(userId, {
                $set: { 'twofa.enable': true },
            }, { new: true }).select('-password -twofa.secret');
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: newUser,
            };
        } catch (error) {
            throw error;
        }
    }

    async listSeller(userId: string, query: UserQuery) {
        try {
            let { page, limit, search, filter, sort, isActive } = query
            let filterObject: DataFilter<Partial<User>> = {
                limit,
                page,
                condition: { role: { $ne: Role.admin }, verified: true || undefined, adminId: userId },
                population: []
            }
            if (isActive != undefined) {
                filterObject.condition = { ...filterObject.condition, isActive }
            }
            if (filter) {
                filterObject.condition = { ...filterObject.condition, ...filter }
            }
            if (search) {
                filterObject.condition['$or'] = [
                    { email: { '$regex': search, '$options': 'i' } },
                    { supportEmail: { '$regex': search, '$options': 'i' } },
                ]
            }
            if (sort) {
                filterObject.sort = sort
            }
            filterObject.selectCols = '-password -__v -adminId -role'
            const list = await this.getListDocument(filterObject)
            return {
                message: BaseNoti.SUCCESS,
                data: list
            }
        } catch (error) {
            throw error
        }
    }

    async listAdmin(userId: string, query: UserQuery) {
        try {
            let { page, limit, search, filter, sort, isActive } = query
            let filterObject: DataFilter<Partial<User>> = {
                limit,
                page,
                condition: { role: Role.admin, _id: { $ne: userId } },
                population: []
            }
            if (isActive != undefined) {
                filterObject.condition = { ...filterObject.condition, isActive }
            }
            if (filter) {
                filterObject.condition = { ...filterObject.condition, ...filter }
            }
            if (search) {
                filterObject.condition['$or'] = [
                    { email: { '$regex': search, '$options': 'i' } },
                    { supportEmail: { '$regex': search, '$options': 'i' } },
                ]
            }
            if (sort) {
                filterObject.sort = sort
            }
            filterObject.selectCols = '-password -__v'
            const list = await this.getListDocument(filterObject)
            return {
                message: BaseNoti.SUCCESS,
                data: list
            }
        } catch (error) {
            throw error
        }
    }

    async adminCreateSeller(userId: string, adminCreateSellerDto: AdminCreateSellerDto) {
        try {
            const { email } = adminCreateSellerDto
            const found = await this.userModel.findOne({ email })
            if (found) {
                throw new BaseApiException({
                    message: BaseNoti.USER.USER_EXISTS
                })
            }
            const newSeller = new this.userModel({
                ...adminCreateSellerDto,
                verified: true,
                isActive: true,
                role: Role.seller,
                adminId: userId
            })
            await newSeller.save()
            return {
                message: BaseNoti.SUCCESS,
                data: newSeller
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetDetailSeller(adminId: string, userId: string) {
        try {
            const foundUser = await this.findUserByAdmin(adminId, userId)
            return {
                message: BaseNoti.SUCCESS,
                data: foundUser
            }
        } catch (error) {
            throw error
        }
    }

    async adminActiveUser(adminId: string, { userId }: AdminActiveUserDto) {
        try {
            const foundUser = await this.findUserByAdmin(adminId, userId)
            const userUpdated = await this.userModel.findByIdAndUpdate(userId, { isActive: true }, { new: true })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: userUpdated
            }
        } catch (error) {
            throw error
        }
    }

    async adminDeactiveUser(adminId: string, { userId }: AdminActiveUserDto) {
        try {
            const foundUser = await this.findUserByAdmin(adminId, userId)
            const [userUpdated] = await Promise.all([
                this.userModel.findByIdAndUpdate(userId, { isActive: false }, { new: true }),
                this.authService.removeToken(userId)
            ])
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: userUpdated
            }
        } catch (error) {
            throw error
        }
    }

    async sellerUpdateUser(userId: string, updateUserDto: UpdateUserDto) {
        try {
            const foundUser = await this.userModel.findByIdAndUpdate(userId, updateUserDto, { new: true }).select('-__v -password')
            if (!foundUser) {
                throw new BaseApiException({
                    message: BaseNoti.USER.USER_NOT_EXISTS
                })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: foundUser
            }
        } catch (error) {
            throw error
        }
    }

    async createUser(userPayload: Partial<User>) {
        try {
            const newUser = await new this.userModel(userPayload)
            await newUser.save()
            return newUser
        } catch (error) {
            throw error
        }
    }

    async update(filter: any, userPayload: any) {
        try {
            const user = await this.userModel.findOneAndUpdate(filter, userPayload, { new: true })
            return user
        } catch (error) {
            throw error
        }
    }

    async findUserById(userId: string) {
        try {
            const found = userId ? await this.userModel.findById(userId) : null
            if (!found) {
                throw new BaseApiException({
                    message: BaseNoti.USER.USER_NOT_EXISTS
                })
            }
            return found
        } catch (error) {
            throw error
        }
    }

    async findUserByAdmin(adminId: string, userId: string) {
        try {
            const found = await this.userModel.findOne({ _id: userId, adminId })
            if (!found) {
                throw new BaseApiException({
                    message: BaseNoti.USER.USER_NOT_EXISTS
                })
            }
            return found
        } catch (error) {
            throw error
        }
    }
    async findUserByEmail(email: string) {
        try {
            const found = email ? await this.userModel.findOne({ email }) : null
            if (!found) {
                throw new BaseApiException({
                    message: BaseNoti.USER.USER_NOT_EXISTS
                })
            }
            return found
        } catch (error) {
            throw error
        }
    }
    async findUser(filter: any) {
        try {
            const found = await this.userModel.findOne(filter).select('-__v')
            return found || null
        } catch (error) {
            throw error
        }
    }

    async findListUser(filter?: any) {
        try {
            const listUser = await this.userModel.find(filter)//.select('id numberOfProduct')
            return listUser
        } catch (error) {
            throw error
        }
    }

    async getListSellerIdByAdminId(adminId: string) {
        try {
            const listSeller = await this.userModel.find({ adminId })
            return listSeller.map(seller => seller._id.toString())
        } catch (error) {
            throw error
        }
    }

    async getOrderPrefix(adminId: string) {
        try {
            let prefix = await this.cacheService.get({ key: `${KeyRedisEnum.orderprefix}-${adminId}` })
            if (!prefix) {
                const foundAdmin = await this.findUserById(adminId)
                prefix = foundAdmin?.orderPrefix || ""
                prefix && await this.cacheService.set({ key: `${KeyRedisEnum.orderprefix}-${adminId}`, value: prefix, expiresIn: 60 * 60 })
            }
            return prefix
        } catch (error) {
            return ""
        }
    }

}
