import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UpdatePolicyDto } from './policy.dto';
import { Policy } from './policy.model';
import * as _ from 'lodash'
@Injectable()
export class PolicyService {
    constructor(
        @InjectModel(Policy.name) private readonly policyModel: Model<Policy>,
    ) { }

    async createPolicy(createPolicyDto: Policy) {
        try {
            const newPolicy = new this.policyModel(createPolicyDto)
            await newPolicy.save()
            return newPolicy
        } catch (error) {
            throw error
        }
    }

    async updatePolicy(policyId: string, updatePolicyDto: UpdatePolicyDto) {
        try {
            const validField: any = _.pickBy(updatePolicyDto)
            const policy = await this.policyModel.findByIdAndUpdate(policyId, validField, { new: true })
            return policy
        } catch (error) {
            throw error
        }
    }

    async findPolicyById(policyId?: string) {
        try {
            if (!policyId) return
            const found = await this.policyModel.findById(policyId)
            return found || null
        } catch (error) {
            throw error
        }
    }

    async deletePolicyById(policyId: string) {
        try {
            await this.policyModel.findByIdAndDelete(policyId)
        } catch (error) {
            throw error
        }
    }

}
