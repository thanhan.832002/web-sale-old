import { ApiProperty } from "@nestjs/swagger"
import { Type } from "class-transformer"
import { IsNotEmpty, IsNumber, Validate } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsObjectId } from "src/base/custom-validator"

export const PolicySchema: Schema = new Schema({
    refund: String,
    privacy: String,
    shipping: String,
    terms: String,
    faqs: String,
    aboutUs: String,
    dmca: String,
    cancelAndOrderChange: String,
    paymentMethod: String,
}, { ...schemaOptions, collection: 'Policy' })


export class Policy extends Document {
    @ApiProperty()
    @IsNotEmpty()
    refund: string

    @ApiProperty()
    @IsNotEmpty()
    privacy: string

    @ApiProperty()
    @IsNotEmpty()
    shipping: string

    @ApiProperty()
    @IsNotEmpty()
    terms: string

    @ApiProperty()
    @IsNotEmpty()
    faqs: string

    @ApiProperty()
    @IsNotEmpty()
    aboutUs: string

    @ApiProperty()
    @IsNotEmpty()
    dmca: string

    @ApiProperty()
    @IsNotEmpty()
    cancelAndOrderChange: string

    @ApiProperty()
    @IsNotEmpty()
    paymentMethod: string
}
