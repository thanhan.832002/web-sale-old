import { ApiPropertyOptional } from "@nestjs/swagger"
import { IsOptional } from "class-validator"

export class UpdatePolicyDto {
    @ApiPropertyOptional()
    @IsOptional()
    refund: string

    @ApiPropertyOptional()
    @IsOptional()
    privacy: string

    @ApiPropertyOptional()
    @IsOptional()
    shipping: string

    @ApiPropertyOptional()
    @IsOptional()
    terms: string

    @ApiPropertyOptional()
    @IsOptional()
    faqs: string

    @ApiPropertyOptional()
    @IsOptional()
    aboutUs: string

    @ApiPropertyOptional()
    @IsOptional()
    dmca: string

    @ApiPropertyOptional()
    @IsOptional()
    cancelAndOrderChange: string

    @ApiPropertyOptional()
    @IsOptional()
    paymentMethod: string
}