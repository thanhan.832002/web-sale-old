import { Injectable } from '@nestjs/common';
import { Interval, Timeout } from '@nestjs/schedule';
import { Promise } from "bluebird";
import * as moment from 'moment'
import { ViewService } from 'src/view/view.service';
import { ProductSellerService } from 'src/product-seller/product-seller.service';
import { SendAbandonedService } from 'src/do-send-abandoned/send-abandoned.service';
import { AuthService } from 'src/auth/auth.service';
import { EmailService } from 'src/email/email.service';
import { OrderService } from 'src/order/order.service';
import { StoreService } from 'src/store/store.service';
import { CouponService } from 'src/coupon/coupon.service';
import { StoreStatus } from 'src/store/store.enum';
import { DomainService } from 'src/domain/domain.service';
import { UtmService } from 'src/utm/utm.service';
import { UserService } from 'src/user/user.service';
import { Role } from 'src/base/enum/roles.enum';
import { OrderStatusEnum, ShippingStatusEnum } from 'src/order/order.enum';
var momentTz = require("moment-timezone");
var axios = require("axios");
const openssl = require('openssl-nodejs')
import * as _ from 'lodash'
import { readFile, readFileSync, writeFileSync } from 'fs';
@Injectable()
export class CronService {
    constructor(
        private readonly productSellerService: ProductSellerService,
        private readonly sendAbandonedService: SendAbandonedService,
        private readonly viewService: ViewService,
        private readonly authService: AuthService,
        private readonly orderService: OrderService,
        private readonly storeService: StoreService,
        private emailService: EmailService,
        private readonly couponService: CouponService,
        private readonly domainService: DomainService,
        private readonly utmService: UtmService,
        private readonly userService: UserService
    ) { }

    // @Timeout(0) /
    // async csr() {
    //     openssl("openssl req -new -newkey rsa:2048 -nodes -keyout domain.key -out domain.csr", function (err, buffer) {
    //         console.log('err :>> ', err);
    //         console.log(err.toString(), buffer.toString());
    //     });
    // }


    // @Timeout(0)
    wait(ms) {
        return new Promise((resolve, reject) => {
            setTimeout(() => { resolve(true) }, ms)
        })
    }

    @Interval(10 * 1000)
    async updateView() {
        try {

            if (process.env.NODE_APP_INSTANCE === "0" && process.platform != "win32") {
                const { listStoreIds, listUtmIds } = await this.viewService.getListTraffic()
                await this.viewService.clearListTraffic()
                console.log("get list store");
                const [listStore, listUtm] = await Promise.all([
                    this.storeService.findListStoreView({ _id: { $in: listStoreIds } }),
                    this.utmService.getListUtmId({ _id: { $in: listUtmIds } })
                ])
                console.log('listStore.length :>> ', listStore.length);
                let listChunk = _.chunk(listStore, 20);
                for (let chunk of listChunk) {
                    console.log('chunk.length :>> ', chunk.length);
                    await Promise.map(chunk, async (store) => {
                        const { productSellerId, sellerId, adminId } = store
                        await Promise.map(listUtm, async (utmId) => {
                            try {
                                await this.viewService.updateViewFromRedis(adminId, sellerId, productSellerId, store._id.toString(), utmId.toString(), productSellerId.productId)
                            } catch (error) {
                                console.log('error :>> ', error);
                            }
                            await this.wait(500)
                        }, { concurrency: 20 })

                    }, { concurrency: 20 })
                    await this.wait(1000)
                }
                console.log("done update");
            }
        } catch (error) {
            console.log('error :>> ', error);
        }
    }

    @Interval(30 * 1000)
    // @Timeout(0)
    async sendFirstMailAbandoned() {
        try {
            if (process.env.NODE_APP_INSTANCE === "0" && process.platform != "win32") {
                const listAdmin = await this.userService.findListUser({ role: Role.admin })
                for (let admin of listAdmin) {
                    const adminId = admin._id
                    await this.sendAbandonedService.sendFirstMail(adminId);
                }
            }
        } catch (error) {

        }
    }

    @Interval(30 * 1000)
    // @Timeout(0)
    async sendSecondMailAbandoned() {
        try {
            if (process.env.NODE_APP_INSTANCE === "0" && process.platform != "win32") {
                const listAdmin = await this.userService.findListUser({ role: Role.admin })
                for (let admin of listAdmin) {
                    const adminId = admin._id
                    await this.sendAbandonedService.sendSecondMail(adminId)
                }
            }
        } catch (error) {

        }
    }
    @Interval(30 * 1000)
    // @Timeout(0)
    async sendThirdMailAbandoned() {
        try {
            if (process.env.NODE_APP_INSTANCE === "0" && process.platform != "win32") {
                const listAdmin = await this.userService.findListUser({ role: Role.admin })
                for (let admin of listAdmin) {
                    const adminId = admin._id
                    await this.sendAbandonedService.sendThirdMail(adminId)
                }
            }
        } catch (error) {
        }
    }

    @Interval(60 * 60 * 1000)
    // @Timeout(0)
    async deleteNotUseRefreshToken() {
        try {
            if (process.env.NODE_APP_INSTANCE === "0" && process.platform != "win32") {
                const last2month = moment().subtract(2, 'month').toDate()
                await this.authService.accessTokenModel.deleteMany({ updatedAt: { $lt: last2month } })
            }
        } catch (error) {
            // throw error
        }
    }

    @Interval(30 * 1000)
    async cronStripeMiss() {
        try {
            if (process.env.NODE_APP_INSTANCE === "0" && process.platform != "win32") {
                const stripe = require('stripe')('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');
                // const charges = await stripe.charges.search({
                //     query: `payment_method_details.card.fingerprint:"7y7laAub42eFVN0h"`,
                // });
                const time = moment().subtract(1, 'minutes').toDate()
                const timeLast = moment().subtract(20, 'minutes').toDate()
                const listOrder = await this.orderService.orderModel.find({ createdAt: { $lte: time, $gte: timeLast }, orderStatus: 'draft', recheck: false }).populate({ path: 'sellerId', select: 'adminId' })
                let listChunk = _.chunk(listOrder, 25);
                for (let chunk of listChunk) {
                    await Promise.map(chunk, async (order) => {
                        const { orderCode } = order
                        const charge = await stripe.charges.search({
                            query: `metadata[\'order_id\']:\'${orderCode}\'`,
                        });
                        if (!charge?.data?.length) return
                        // if (charge?.data[0]?.payment_method && charge?.data[0]?.payment_method != order.stripe.paymentMethodID) {
                        //     await this.orderService.orderModel.findByIdAndUpdate(order._id, { recheck: true })
                        //     return
                        // }
                        const pay = charge.data[0]
                        if (pay.status != 'succeeded') {
                            await this.orderService.orderModel.findByIdAndUpdate(order._id, { recheck: true })
                            return
                        }
                        const stripeObj = {
                            paymentMethodID: pay.payment_method,
                            id: pay.payment_intent,
                            cardHolderName: order.stripe.cardHolderName,
                            amount: pay.amount,
                            amount_captured: pay.amount_captured,
                            amount_refunded: 0,
                            application_fee: 0 || 0,
                            application_fee_amount: pay.application_fee_amount || 0,
                        }
                        let sendMail = true
                        try {
                            //@ts-ignore
                            await this.emailService.emailConfirmOrder(order.sellerId.adminId, order)
                            //@ts-ignore
                            await this.orderService.adminAddTimeline(order.sellerId.adminId, order._id, { content: `Confirmation email was sent to: ${shippingAddress.email}`, time: moment().unix() })
                            if (order.billingAddress) {
                                //@ts-ignore
                                await this.emailService.emailChangeShipping(order.sellerId.adminId, order)
                            }
                        } catch (error) {
                            sendMail = false
                        }
                        console.log('order.stripe.paymentMethodID :>> ', order.stripe.paymentMethodID);
                        await this.orderService.orderModel.findByIdAndUpdate(order._id, {
                            stripe: stripeObj,
                            shippingStatus: ShippingStatusEnum.paid,
                            orderStatus: OrderStatusEnum.processing,
                            processingDate: new Date(),
                            sendMailConfirmed: sendMail,
                            recheck: true
                        })
                        const { total, totalCost, totalQuantity, tip, utmId } = order
                        //@ts-ignore
                        const nothasView = await this.viewService.updateAnalyticOrder(order.isAbandon, order.createdAt, order.sellerId._id, order.sellerId.adminId, order.storeId, { total, cost: totalCost, quantity: totalQuantity, tip: tip | 0 }, utmId)
                        if (nothasView) {
                            try {
                                const foundStore = await this.storeService.findOne({ filter: { _id: order.storeId }, selectCols: ['productSellerId'] })
                                await this.viewService.findByIdAndUpdate(nothasView._id, {
                                    //@ts-ignore
                                    adminId: order.sellerId.adminId,
                                    //@ts-ignore
                                    productId: order.productId,
                                    productSellerId: foundStore.productSellerId
                                })
                            } catch (error) {
                            }
                        }
                    }, { concurrency: 25 })
                    await this.wait(500)
                }
            }
        } catch (error) {
            console.log('error :>> ', error);
        }
    }

}
