import { Module } from '@nestjs/common';
import { ProductSellerModule } from 'src/product-seller/product-seller.module';
import { ViewModule } from 'src/view/view.module';
import { CronService } from './cron.service';
import { SendAbandonedModule } from 'src/do-send-abandoned/send-abandoned.module';
import { AuthModule } from 'src/auth/auth.module';
import { EmailModule } from 'src/email/email.module';
import { OrderModule } from 'src/order/order.module';
import { StoreModule } from 'src/store/store.module';
import { CouponModule } from 'src/coupon/coupon.module';
import { DomainModule } from 'src/domain/domain.module';
import { UtmModule } from 'src/utm/utm.module';
import { UserModule } from 'src/user/user.module';

@Module({
  providers: [CronService],
  imports: [
    ViewModule,
    ProductSellerModule,
    SendAbandonedModule,
    AuthModule,
    EmailModule,
    OrderModule,
    StoreModule,
    CouponModule,
    DomainModule,
    UtmModule,
    UserModule
  ]
})
export class CronModule { }
