import { Injectable } from '@nestjs/common';
import { HomepageSetting } from './homepage-setting.model';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateDefaultHomePageDto, UpdateHomepageSettingDto } from './homepage-setting.dto';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { BaseService } from 'src/base/base.service';
import { DataFilter, PaginationData } from 'src/base/interfaces/data.interface';
import { Timeout } from '@nestjs/schedule';
import { CacheService } from 'src/base/caching/cache.service';

@Injectable()
export class HomepageSettingService extends BaseService<HomepageSetting>{
    constructor(
        @InjectModel(HomepageSetting.name) private readonly homepageSettingModel: Model<HomepageSetting>,
        private readonly cacheService: CacheService
    ) { super(homepageSettingModel) }

    async adminGetHomepageSetting(adminId: string, domainId: string) {
        try {
            const foundHomepage = await this.homepageSettingModel.findOne({ domainId, adminId }).populate([
                { path: 'domainId', select: 'domain verifyEmailInfo' },
                { path: 'paymentId', select: 'name type enabled policyId'/* , populate: 'policyId' */ },
                { path: 'bannerId', select: 'imageUrl' },
                { path: 'logoId', select: 'imageUrl' }
            ])
            if (!foundHomepage) {
                throw new BaseApiException({
                    message: BaseNoti.HOMEPAGE.HOMEPAGE_NOT_EXISTS
                })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: foundHomepage
            }
        } catch (error) {
            throw error
        }
    }

    async buyerGetHomepageSetting(adminId: string, domainId: string) {
        try {
            const foundHomepage = await this.homepageSettingModel.findOne({ domainId, adminId })
                .select('-__v -domainId -paymentId')
                .populate([
                    { path: 'bannerId', select: 'imageUrl' },
                    { path: 'logoId', select: 'imageUrl' }
                ])
            if (!foundHomepage) {
                throw new BaseApiException({
                    message: BaseNoti.HOMEPAGE.HOMEPAGE_NOT_EXISTS
                })
            }
            return foundHomepage
        } catch (error) {
            throw error
        }
    }

    async buyerGetPolicy(adminId: string, domainId: string) {
        try {
            const foundHomepage = await this.homepageSettingModel.findOne({ domainId, adminId })
                .select('-__v -domainId -isRandomProduct')
                .populate([
                    // { path: 'domainId', select: 'domain' },
                    { path: 'paymentId', select: 'policyId', populate: 'policyId' }
                ])
            if (!foundHomepage) {
                throw new BaseApiException({
                    message: BaseNoti.HOMEPAGE.HOMEPAGE_NOT_EXISTS
                })
            }
            return foundHomepage
        } catch (error) {
            throw error
        }
    }

    async buyerGetHomepageSettingAndPolicy(adminId: string, domainId: string) {
        try {
            const foundHomepage = await this.homepageSettingModel.findOne({ domainId, adminId })
                .select('-__v -domainId -isRandomProduct')
                .populate([
                    // { path: 'domainId', select: 'domain' },
                    { path: 'paymentId', select: 'policyId', populate: 'policyId' },
                    { path: 'bannerId', select: 'imageUrl' },
                    { path: 'logoId', select: 'imageUrl' }
                ])
            if (!foundHomepage) {
                throw new BaseApiException({
                    message: BaseNoti.HOMEPAGE.HOMEPAGE_NOT_EXISTS
                })
            }
            return foundHomepage
        } catch (error) {
            throw error
        }
    }

    async updateHomepageSetting(adminId: string, domainId: string, updateHomepageSettingDto: UpdateHomepageSettingDto) {
        try {
            let { logoId } = updateHomepageSettingDto
            if (!logoId) delete updateHomepageSettingDto.logoId
            const foundHomepage = await this.homepageSettingModel.findOneAndUpdate({ domainId, adminId }, updateHomepageSettingDto, { new: true }).populate([
                { path: 'domainId', select: 'domain verifyEmailInfo' },
                { path: 'paymentId', select: 'name type enabled policyId'/* , populate: 'policyId' */ }
            ])
            if (!foundHomepage) {
                throw new BaseApiException({
                    message: BaseNoti.HOMEPAGE.HOMEPAGE_NOT_EXISTS
                })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: foundHomepage
            }
        } catch (error) {
            throw error
        }
    }

    async createDefaultHomePage(createDefaultHomePageDto: CreateDefaultHomePageDto) {
        try {
            const newHomepage = new this.homepageSettingModel(createDefaultHomePageDto)
            await newHomepage.save()
            return newHomepage
        } catch (error) {
            throw error
        }
    }

    async deleteHomepageByDomainId(domainId: string) {
        try {
            await this.homepageSettingModel.findOneAndDelete({ domainId })
        } catch (error) {
            throw error
        }
    }

}
