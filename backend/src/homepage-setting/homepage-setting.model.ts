import { ApiProperty } from "@nestjs/swagger"
import { Transform, Type } from "class-transformer"
import { IsBoolean, IsNotEmpty, IsNumber, IsOptional, Matches, Min, Validate } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsListObjectId, IsObjectId } from "src/base/custom-validator"

export const HomepageSettingSchema: Schema = new Schema({
    domainId: {
        type: Types.ObjectId,
        index: true,
        ref: 'Domain'
    },
    // isRandomProduct: {
    //     type: Boolean,
    //     default: false
    // },
    limitProduct: {
        type: Number,
        default: 16
    },
    topDesc: {
        type: String,
        default: ''
    },
    middleDesc: {
        type: String,
        default: ''
    },
    metaDesc: {
        type: String,
        default: ''
    },
    bannerId: {
        type: [Types.ObjectId],
        ref: 'Gallery'
    },
    logoId: {
        type: Types.ObjectId,
        ref: 'Gallery'
    },
    paymentId: {
        type: Types.ObjectId,
        ref: 'Payment'
    },
    homepageName: String,
    homepageTitle: String,
    headerColor: {
        type: String,
        default: '#f7f7f7'
    },
    backgroundColor: {
        type: String,
        default: '#ffffff'
    },
    textColor: {
        type: String,
        default: '#000000'
    },
    address: {
        type: String,
        default: ''
    },
    phone: {
        type: String,
        default: ''
    },
    supportEmail: String,
    googleAnalyticId: {
        type: String,
        default: ''
    }
}, { ...schemaOptions, collection: 'HomepageSetting' })


export class HomepageSetting extends Document {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    domainId: string

    // @ApiProperty()
    // @IsBoolean()
    // @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    // @IsNotEmpty()
    // isRandomProduct: boolean

    @ApiProperty()
    @IsNotEmpty()
    topDesc: string

    @ApiProperty()
    @IsNotEmpty()
    middleDesc: string

    @ApiProperty()
    @IsNotEmpty()
    metaDesc: string

    @ApiProperty({ type: [String] })
    @Validate(IsListObjectId)
    @IsNotEmpty()
    bannerId: string[]

    @ApiProperty()
    @Validate(IsObjectId)
    @IsOptional()
    logoId: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    paymentId: string


    @ApiProperty()
    @IsNotEmpty()
    homepageName: string

    @ApiProperty()
    @IsNotEmpty()
    homepageTitle: string


    @ApiProperty()
    @IsNotEmpty()
    headerColor: string

    @ApiProperty()
    @IsNotEmpty()
    backgroundColor: string

    @ApiProperty()
    @IsNotEmpty()
    textColor: string

    @ApiProperty()
    @IsNotEmpty()
    address: string

    @ApiProperty()
    @IsNotEmpty()
    phone: string

    @ApiProperty()
    @Matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
    @Transform(({ value }) => {
        if (value) return value.toLowerCase().trim()
    })
    @IsNotEmpty()
    supportEmail: string

    @ApiProperty()
    @IsNotEmpty()
    googleAnalyticId: string
}
