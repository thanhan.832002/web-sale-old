import { Module } from '@nestjs/common';
import { HomepageSettingService } from './homepage-setting.service';
import { HomepageSettingController } from './homepage-setting.controller';
import { HomepageSetting, HomepageSettingSchema } from './homepage-setting.model';
import { MongooseModule } from '@nestjs/mongoose';
import { SharedModule } from 'src/base/shared.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: HomepageSetting.name, schema: HomepageSettingSchema }]),
    SharedModule
  ],
  providers: [HomepageSettingService],
  exports: [HomepageSettingService],
  controllers: [HomepageSettingController]
})
export class HomepageSettingModule { }
