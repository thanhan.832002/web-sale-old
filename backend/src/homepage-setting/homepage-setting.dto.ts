import { ApiPropertyOptional } from "@nestjs/swagger"
import { Transform, Type } from "class-transformer"
import { IsBoolean, IsNumber, IsOptional, Matches, Min, Validate } from "class-validator"
import { IsListObjectId, IsObjectId } from "src/base/custom-validator"

export interface CreateDefaultHomePageDto {
    domainId: string
    homepageName: string
    homepageTitle: string
    supportEmail: string
    adminId: string
}

export class UpdateHomepageSettingDto {
    @ApiPropertyOptional()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsOptional()
    isRandomProduct: boolean

    @ApiPropertyOptional()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    limitProduct: number

    @ApiPropertyOptional()
    @IsOptional()
    topDesc: string

    @ApiPropertyOptional()
    @IsOptional()
    middleDesc: string

    @ApiPropertyOptional()
    @IsOptional()
    metaDesc: string

    @ApiPropertyOptional({ type: [String] })
    @Validate(IsListObjectId)
    @IsOptional()
    bannerId: string[]

    @ApiPropertyOptional()
    // @Validate(IsObjectId)
    @IsOptional()
    logoId: string

    @ApiPropertyOptional()
    @Validate(IsObjectId)
    @IsOptional()
    paymentId: string


    @ApiPropertyOptional()
    @IsOptional()
    homepageName: string

    @ApiPropertyOptional()
    @IsOptional()
    homepageTitle: string

    @ApiPropertyOptional()
    @IsOptional()
    headerColor: string

    @ApiPropertyOptional()
    @IsOptional()
    backgroundColor: string

    @ApiPropertyOptional()
    @IsOptional()
    textColor: string

    @ApiPropertyOptional()
    @IsOptional()
    address: string

    @ApiPropertyOptional()
    @IsOptional()
    phone: string

    @ApiPropertyOptional()
    @Matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
    @Transform(({ value }) => {
        if (value) return value.toLowerCase().trim()
    })
    @IsOptional()
    supportEmail: string

    @ApiPropertyOptional()
    @IsOptional()
    googleAnalyticId: string
}