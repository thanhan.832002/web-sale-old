import { Controller, Get, Query, Param, Put, Body, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { HomepageSettingService } from './homepage-setting.service';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { UpdateHomepageSettingDto } from './homepage-setting.dto';
import { AuthGuard } from '@nestjs/passport';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';

@Controller('homepage-setting')
@ApiTags('Home Page Setting')
@UseGuards(AdminGuard)
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
export class HomepageSettingController {
    constructor(
        private readonly homepageSettingService: HomepageSettingService
    ) { }

    @Get('detail/:domainId')
    async getHomepageSetting(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('domainId') domainId: string) {
        return this.homepageSettingService.adminGetHomepageSetting(userId, domainId)
    }

    @Put('update/:domainId')
    async updateHomepageSetting(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('domainId') domainId: string,
        @Body() updateHomepageSettingDto: UpdateHomepageSettingDto) {
        return this.homepageSettingService.updateHomepageSetting(userId, domainId, updateHomepageSettingDto)
    }

}
