import { Injectable } from '@nestjs/common';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { AdminSettingService } from 'src/admin-setting/admin-setting.service';
const axios = require('axios')
import { Interval, Timeout } from '@nestjs/schedule';
import { trusted } from 'mongoose';
import { ConfigService } from '@nestjs/config';
const fs = require("fs");
import { exec } from 'child_process'
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';


@Injectable()
export class ZerosslService {
    constructor(
        private readonly adminSettingService: AdminSettingService,
        private readonly configService: ConfigService,
    ) { }

    async createCertificate(adminId: string, domain: string, certificate_csr: string) {
        try {
            let url = `https://api.zerossl.com/certificates`
            const zerourl = await this.zerosslUrl(adminId, url);
            console.log('certificate_csr :>> ', certificate_csr);
            const data = {
                certificate_domains: `*.${domain}`,
                certificate_csr,
                certificate_validity_days: 90
            }

            const axiosOption: Partial<AxiosRequestConfig> = {
                method: 'post',
                url: zerourl,
                data: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }
            const reponse: AxiosResponse = await axios(axiosOption)
            return reponse.data
        } catch (error) {
            if (error?.toJSON()?.message?.includes('401')) {
                throw new BaseApiException({
                    message: BaseNoti.DOMAIN.INVALID_SSL_KEY
                })
            }
            throw error
        }
    }

    async verificationDetail(adminId: string, domain: string, sslId: string) {
        try {
            let url = `https://api.zerossl.com/certificates`
            const zerourl = await this.zerosslUrl(adminId, url)
            console.log('zerourl :>> ', zerourl);
            const axiosOption: Partial<AxiosRequestConfig> = {
                method: 'get',
                url: `${zerourl}&search=${domain}`,
            }
            const reponse: AxiosResponse = await axios(axiosOption)
            let result = reponse?.data?.results ? reponse.data.results : []
            result = result.filter(r => r.id == sslId)
            console.log('result :>> ', result);
            return result?.length ? result[0] : null
        } catch (error) {
            throw error
        }
    }

    async downloadCsr(adminId, sslId: string) {
        try {
            const url = `https://api.zerossl.com/certificates/${sslId}/download/return`
            const zerourl = await this.zerosslUrl(adminId, url)
            const axiosOption: Partial<AxiosRequestConfig> = {
                method: 'get',
                url: zerourl,
            }
            const reponse: AxiosResponse = await axios(axiosOption)
            return reponse.data
        } catch (error) {
            throw error
        }
    }

    async verifySsl(adminId, sslId: string) {
        try {
            let url = `https://api.zerossl.com/certificates/${sslId}/challenges`
            const zerourl = await this.zerosslUrl(adminId, url)
            console.log('zerourl :>> ', zerourl);
            const axiosOption: Partial<AxiosRequestConfig> = {
                method: 'post',
                url: zerourl,
                data: {
                    validation_method: 'CNAME_CSR_HASH'
                },
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }
            const reponse: AxiosResponse = await axios(axiosOption)
            return reponse.data
        } catch (error) {
            throw error
        }
    }

    async zerosslUrl(adminId: string, url: string) {
        try {
            const zerosslKey = await this.adminSettingService.getZerosslKey(adminId)
            return `${url}?access_key=${zerosslKey}`
        } catch (error) {
            throw error
        }
    }

    async saveCsrFile(domain: string, certificate: string, ca_bundle: string, privateKeyContent: string) {
        try {
            const folderName = domain.split('.')[0]
            // let rootPath = 'C:\\Users\\Admin\\Desktop\\etc\\ssl'
            let rootPath = '/etc/ssl'
            const path = `${rootPath}/${folderName}`;
            await this.createCsrFolder(path)
            let certificatePath = `${path}/certificate.crt`
            let ca_bundlePath = `${path}/ca_bundle.crt`
            let privatePath = `${path}/private.key`
            await this.writeCsrFile(certificatePath, certificate + ca_bundle)
            await this.writeCsrFile(ca_bundlePath, ca_bundle)
            console.log('privatePath :>> ', privatePath);
            console.log('privateKeyContent :>> ', privateKeyContent);
            await this.writeCsrFile(privatePath, privateKeyContent)
            await this.createNginxCfgFile(domain)
            await this.execCmd('service nginx restart')
        } catch (error) {
            throw error
        }
    }

    async createNginxCfgFile(domain: string) {
        try {
            // const folderPath = `C:\\Users\\Admin\\Desktop\\etc\\nginx\\conf.d`;
            const folderPath = `/etc/nginx/conf.d`;
            const configName = domain.split('.')[0]
            const cfgFile = `${folderPath}/${configName}.conf`
            const content = `
server {
    listen               443 ssl;
    ssl_certificate      /etc/ssl/${configName}/certificate.crt; 
    ssl_certificate_key  /etc/ssl/${configName}/private.key;
    server_name ~(^(?!api.).+.${domain}|${domain})$;
    location / {
        proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   Host      $host;
        proxy_pass         http://feUser;
        proxy_read_timeout 3600;
    }
    error_page  404 /404.html;
    location = /404.html {
        root /404.html;
    }

    error_page   500 502 503 504 /50x.html;
    location = /50x.html {
        root /404.html;
    }
    # pass the PHP scripts to FastCGI server
    #
    location ~\..*/.*\.php$ {
        return 403;
    }
}`
            await this.writeCsrFile(cfgFile, content)
        } catch (error) {
            throw error
        }
    }

    execCmd(cmd: string) {
        return new Promise((resolve, reject) => {
            exec(cmd, (error, stdout, stderr) => {
                console.log('stdout :>> ', stdout);
                console.log('stderr :>> ', stderr);
                resolve(stdout)
            });
        });
    }


    createCsrFolder(path: string) {
        return new Promise((resolve, reject) => {
            fs.access(path, (error) => {
                if (error) {
                    fs.mkdir(path, (error) => {
                        if (error) {
                            resolve(true)
                        } else {
                            console.log("New Directory created successfully !!");
                            resolve(true)
                        }
                    });
                } else {
                    resolve(true)
                }
            });
        });
    }

    writeCsrFile(path: string, content: string) {
        return new Promise((resolve, reject) => {
            fs.writeFile(path, content, function (err) {
                if (err) {
                    resolve(true)
                    console.log(err);
                }
                console.log("The file was saved!");
                resolve(true)
            });

        });
    }
}
