import { Module } from '@nestjs/common';
import { ZerosslService } from './zerossl.service';
import { AdminSettingModule } from 'src/admin-setting/admin-setting.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [AdminSettingModule, ConfigModule],
  providers: [ZerosslService],
  exports: [ZerosslService]
})
export class ZerosslModule { }
