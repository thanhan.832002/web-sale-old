import { ApiProperty, PickType } from "@nestjs/swagger";
import { SellerDiscount } from "./seller-discount.model";
import { IsOptional, Validate } from "class-validator";
import { Transform } from "class-transformer";
import { IsObjectId } from "src/base/custom-validator";
import { BaseQuery } from "src/base/interfaces/base-query.interface";

export class CreateDiscountDto extends PickType(SellerDiscount, ['name', 'getForMore', 'button', 'exp', 'status']) { }

export class UpdateDiscountDto {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsOptional()
    discountId: string

    @ApiProperty()
    @IsOptional()
    name: string

    @ApiProperty()
    @IsOptional()
    getForMore: string

    @ApiProperty()
    @IsOptional()
    exp: number

    @ApiProperty()
    @IsOptional()
    button: string

    @ApiProperty()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsOptional()
    status: boolean
}

export class SellerDisountQuery extends BaseQuery {
    @ApiProperty()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsOptional()
    status: boolean
}