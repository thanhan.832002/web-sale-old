import { ApiProperty } from "@nestjs/swagger"
import { Transform, Type } from "class-transformer"
import { IsNotEmpty, IsNumber, Validate } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsObjectId } from "src/base/custom-validator"

export const SellerDiscountSchema: Schema = new Schema({
    sellerId: {
        type: Types.ObjectId,
        ref: 'User'
    },
    name: String,
    getForMore: String,
    exp: Number,
    button: String,
    status: { type: Boolean, default: false }
}, { ...schemaOptions, collection: 'SellerDiscount' })


export class SellerDiscount extends Document {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    sellerId: string

    @ApiProperty()
    @IsNotEmpty()
    name: string

    @ApiProperty()
    @IsNotEmpty()
    getForMore: string

    @ApiProperty()
    @IsNotEmpty()
    exp: number

    @ApiProperty()
    @IsNotEmpty()
    button: string

    @ApiProperty()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsNotEmpty()
    status: boolean
} 
