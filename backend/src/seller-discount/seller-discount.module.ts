import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SellerDiscountController } from './seller-discount.controller';
import { SellerDiscount, SellerDiscountSchema } from './seller-discount.model';
import { SellerDiscountService } from './seller-discount.service';
import { StoreUpsaleModule } from 'src/store-upsale/store-upsale.module';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: SellerDiscount.name, schema: SellerDiscountSchema }]),
        StoreUpsaleModule
    ],
    providers: [SellerDiscountService],
    exports: [SellerDiscountService],
    controllers: [SellerDiscountController]
})
export class SellerDiscountModule { }
