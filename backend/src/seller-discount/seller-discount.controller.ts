import { Controller, UseGuards, Get, Query, Body, Post, Put, Delete, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SellerGuard } from 'src/base/guard/seller.guard';
import { SellerDiscountService } from './seller-discount.service';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { CreateDiscountDto, SellerDisountQuery, UpdateDiscountDto } from './seller-discount.dto';

@Controller('seller-discount')
@ApiTags('Seller Discount')
@UseGuards(SellerGuard)
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
export class SellerDiscountController {
    constructor(
        private readonly sellerDiscountService: SellerDiscountService
    ) { }

    @Get('list')
    getListDiscount(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() sellerDisountQuery: SellerDisountQuery
    ) {
        return this.sellerDiscountService.getListDiscount(userId, sellerDisountQuery)
    }

    @Post('create')
    createDiscount(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() createDiscountDto: CreateDiscountDto
    ) {
        return this.sellerDiscountService.createDiscount(userId, createDiscountDto)
    }

    @Put('update')
    updateDiscount(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() updateDiscountDto: UpdateDiscountDto
    ) {
        return this.sellerDiscountService.updateDiscount(userId, updateDiscountDto)
    }

    @Delete('delete/:discountId')
    deleteDiscount(
        @Param('discountId') discountId: string,
        @GetUser() { userId }: JwtTokenDecrypted
    ) {
        return this.sellerDiscountService.deleteDiscount(userId, discountId)
    }
}
