import { Injectable } from '@nestjs/common';
import { SellerDiscount } from './seller-discount.model';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import BaseNoti from 'src/base/notify';
import { CreateDiscountDto, SellerDisountQuery, UpdateDiscountDto } from './seller-discount.dto';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import { StoreUpsaleService } from 'src/store-upsale/store-upsale.service';
import { BaseService } from 'src/base/base.service';
import { DataFilter } from 'src/base/interfaces/data.interface';

@Injectable()
export class SellerDiscountService extends BaseService<SellerDiscount>{
    constructor(
        @InjectModel(SellerDiscount.name) private readonly sellerDiscountModel: Model<SellerDiscount>,
        private readonly storeUpsaleService: StoreUpsaleService
    ) { super(sellerDiscountModel) }

    async getListDiscount(userId: string, query: SellerDisountQuery) {
        try {
            let { page, limit, search, filter, sort, status } = query
            let filterObject: DataFilter<Partial<SellerDiscount>> = {
                limit,
                page,
                condition: { sellerId: userId },
                population: []
            }
            if (filter) {
                filterObject.condition = { ...filterObject.condition, ...filter }
            }
            if (status) {
                filterObject.condition = { ...filterObject.condition, status }
            }
            if (search) {
                filterObject.condition['$or'] = [
                    { email: { '$regex': search, '$options': 'i' } },
                    { supportEmail: { '$regex': search, '$options': 'i' } },
                ]
            }
            if (sort) {
                filterObject.sort = sort
            }
            const list = await this.getListDocument(filterObject)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: list
            }
        } catch (error) {
            throw error
        }
    }

    async createDiscount(userId: string, createDiscountDto: CreateDiscountDto) {
        try {
            const discountIns = new this.sellerDiscountModel({
                sellerId: userId,
                ...createDiscountDto
            })
            await discountIns.save()
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: discountIns
            }
        } catch (error) {
            throw error
        }
    }

    async updateDiscount(userId: string, updateDiscountDto: UpdateDiscountDto) {
        try {
            const { discountId, status } = updateDiscountDto
            const updateDiscount = await this.sellerDiscountModel.findOneAndUpdate({ sellerId: userId, _id: discountId }, { ...updateDiscountDto }, { new: true })
            if (!updateDiscount) {
                throw new BaseApiException({
                    message: BaseNoti.DISCOUNT.DISCOUNT_NOT_EXISTS
                })
            }
            if (updateDiscount.status == false) {
                await this.storeUpsaleService.updateMany({ sellerDiscountId: discountId }, { status: false, $unset: { sellerDiscountId: true } })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: updateDiscount
            }
        } catch (error) {
            throw error
        }
    }

    async deleteDiscount(userId: string, discountId: string) {
        try {
            const found = await this.sellerDiscountModel.findOneAndUpdate({ sellerId: userId, id: discountId })
            if (!found) {
                throw new BaseApiException({
                    message: BaseNoti.DISCOUNT.DISCOUNT_NOT_EXISTS
                })
            }
            await Promise.all([
                this.sellerDiscountModel.findByIdAndDelete(discountId),
                this.storeUpsaleService.deleteManyUpsale({ sellerDiscountId: found._id })
            ])
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }
}
