import { Schema, Types, Document } from "mongoose";
import { schemaOptions } from "src/base/base.shema";

export const VariantOptionSchema: Schema = new Schema({
    productId: {
        type: Types.ObjectId,
        index: true,
        ref: 'Product'
    },
    name: String,
    values: [String]
}, { ...schemaOptions, collection: 'VariantOption' })


export class VariantOption extends Document {
    productId: string
    name: string
    values: string[]
}