import { Module } from '@nestjs/common';
import { VariantOptionService } from './variant-option.service';
import { VariantOption, VariantOptionSchema } from './variant-option.model';
import { MongooseModule } from '@nestjs/mongoose';
import { VariantModule } from 'src/variant/variant.module';
import { VariantSellerModule } from 'src/variant-seller/variant-seller.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: VariantOption.name, schema: VariantOptionSchema }]),
    VariantModule,
    VariantSellerModule
  ],
  providers: [VariantOptionService],
  exports: [VariantOptionService]
})
export class VariantOptionModule { }
