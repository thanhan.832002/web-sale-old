import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsArray, IsNotEmpty, IsOptional, ValidateNested } from "class-validator";
import { Product } from "src/product/product.model";
import { OneVariantDto } from "src/variant/variant.dto";


export class OneVariantOptionDto {
    @ApiProperty()
    @IsNotEmpty()
    name: string

    @ApiProperty({ type: [String] })
    @IsNotEmpty()
    values: string[]
}

class OneVariantOptionUpdateDto {
    @ApiProperty()
    @IsOptional()
    name: string

    @ApiProperty({ type: [String] })
    @IsOptional()
    values: string[]
}

export class UpdateListVariantOptionDto {
    @ApiProperty()
    @IsNotEmpty()
    productId: string

    @ApiProperty({ type: [OneVariantOptionUpdateDto] })
    @IsOptional()
    listVariantOption: OneVariantOptionUpdateDto[]

    @ApiProperty({ type: [OneVariantDto] })
    @ValidateNested()
    @Type(() => OneVariantDto)
    @IsArray()
    @IsOptional()
    listVariant: OneVariantDto[]
}