import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { VariantService } from 'src/variant/variant.service';
import { OneVariantOptionDto, UpdateListVariantOptionDto } from './variant-option.dto';
import { VariantOption } from './variant-option.model';
import * as Promise from 'bluebird'
import { VariantSellerService } from 'src/variant-seller/variant-seller.service';
import { ModuleRef } from '@nestjs/core';
import { ProductSellerService } from 'src/product-seller/product-seller.service';

@Injectable()
export class VariantOptionService implements OnModuleInit {
    private productSellerService: ProductSellerService
    constructor(
        @InjectModel(VariantOption.name) private readonly variantOptionModel: Model<VariantOption>,
        private readonly variantService: VariantService,
        private readonly variantSellerService: VariantSellerService,
        private moduleRef: ModuleRef
    ) { }
    onModuleInit() {
        this.productSellerService = this.moduleRef.get(ProductSellerService, { strict: false });
    }

    async createListVariantOption(listVariantOptionDto: OneVariantOptionDto[], productId: string) {
        try {
            const listVariantOption = listVariantOptionDto.map(variantOption => ({
                ...variantOption,
                productId
            }))
            const listcreated = await this.variantOptionModel.insertMany(listVariantOption)
            return listcreated
        } catch (error) {
            throw error
        }
    }

    async updateListVariantOption({ listVariantOption, product, listVariant }: any) {
        try {
            let listcreatedVariantOption = await Promise.map(listVariantOption, async (variantOption) => {
                const { name, values } = variantOption
                delete variantOption._id
                const updatedOption = await this.variantOptionModel.findOneAndUpdate({ name, productId: product._id }, { ...variantOption, productId: product._id }, { new: true, upsert: true })
                if (updatedOption) return updatedOption
            })
            //
            const listcreatedVariantOptionId = listcreatedVariantOption.map(option => option._id)

            const [listProductSellerId, deleteVariant] = await Promise.all([
                this.productSellerService.deleteAllVariantProductSeller(product._id),
                this.variantService.deleteAllVariantProduct({ productId: product._id, _id: { $nin: listcreatedVariantOptionId } })
            ])
            const listCreatedVariant = await this.variantService.createListVariant(listVariant, listcreatedVariantOption, product)
            //
            await Promise.map(listProductSellerId, async (productSellerId) => {
                const listVariantSellerId = await this.variantSellerService.createListVariantSeller(productSellerId, listCreatedVariant, listcreatedVariantOption)
                await this.productSellerService.updateProductSeller(productSellerId, { listVariantSellerId })
            })
            const listCreatedVariantId = listCreatedVariant.map(variant => variant._id)
            return { listCreatedVariantId, listcreatedVariantOption }
        } catch (error) {
            throw error
        }
    }

}
