import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { ExtractJwt, VerifiedCallback, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { ConfigService } from '@nestjs/config';
import BaseNoti from 'src/base/notify';
import { JwtPayload } from './auth.dto';
import { BaseApiException } from 'src/base/exception/base-api.exception';


@Injectable()
export class JwtService extends PassportStrategy(Strategy, 'jwt') {
    constructor(
        @Inject(forwardRef(() => AuthService))
        private accessTokenService: AuthService,
        private readonly configService: ConfigService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: configService.get<string>('JWT_SECRET'),
        });
    }
    async validate(payload: JwtPayload, done: VerifiedCallback) {
        const errorResponse = {
            message: BaseNoti.AUTH.INVALID_TOKEN,
            statusCode: 403
        }
        try {
            const { userId, role, sessionId } = payload
            const isHasToken: string = await this.accessTokenService.getAccessTokenFromRedis(payload)
            if (!isHasToken) {
                return done(new BaseApiException({
                    message: BaseNoti.AUTH.INVALID_TOKEN,
                    status: 403
                }))
            }
            const user = { userId, role, sessionId }
            return done(null, user, payload.iat);
        } catch (error) {
            throw error
        }
    }


}
