import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AccessToken, AccessTokenSchema } from './access-token.model';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtService } from './jwt.service';
import { SharedModule } from 'src/base/shared.module';
import { EmailModule } from 'src/email/email.module';
import { RateLimitCustomMidleWare } from 'src/base/rate-limit/rate-limit.middleware';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: AccessToken.name, schema: AccessTokenSchema }]),
    SharedModule,
    ConfigModule,
    EmailModule
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtService],
  exports: [AuthService, JwtService]
})
export class AuthModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(RateLimitCustomMidleWare).forRoutes(
        { path: '/auth/login', method: RequestMethod.POST }
      )
  }
}
