import { ApiProperty, PickType } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional } from "class-validator";
import { Role } from "src/base/enum/roles.enum";
import { User } from "src/user/user.model";


export class RegisterDto extends PickType(User, ['email', 'password']) { }
export class LoginDto extends PickType(User, ['email', 'password']) {
    @ApiProperty()
    @IsOptional()
    code: string
}


export class RenewTokenDto {
    @ApiProperty({ description: 'Refresh token được trả về khi đăng nhập' })
    @IsNotEmpty()
    refreshToken: string;

}

export class JwtPayload {
    userId: string;
    tokenType?: string;
    role?: Role;
    iat?: Date;
    sessionId?: string
}

export class JwtTokenDecrypted {
    userId: string;
    role: Role
}

export class VerifyMailDto {
    @ApiProperty()
    @IsNotEmpty()
    token: string
}