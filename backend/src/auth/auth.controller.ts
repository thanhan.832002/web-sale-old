import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { LoginDto, RegisterDto, RenewTokenDto, VerifyMailDto } from './auth.dto';
import { AuthService } from './auth.service';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService
    ) { }

    @Post('login')
    async login(@Body() signinDto: LoginDto) {
        return this.authService.login(signinDto)
    }

    @Post('renew-token')
    async renewToken(
        @Body() renewTokenDto: RenewTokenDto,
    ) {
        return this.authService.renewToken(renewTokenDto)
    }
}
