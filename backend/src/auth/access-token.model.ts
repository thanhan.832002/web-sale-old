import { Schema, Document } from "mongoose";
import { schemaOptions } from "src/base/base.shema";

export const AccessTokenSchema: Schema = new Schema({
    userId: {
        type: String,
        required: true
    },
    sessionId: String,
    token: {
        type: String,
        required: true
    }

}, { ...schemaOptions, ... { collection: 'AccessToken' } });
export class AccessToken extends Document {
    id: string

    userId: string

    sessionId: string

    token: string


}