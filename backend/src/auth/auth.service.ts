import { Injectable, OnModuleInit } from "@nestjs/common";
import { ModuleRef } from "@nestjs/core";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { User } from "src/user/user.model";
import { UserService } from "src/user/user.service";
import { AccessToken } from "./access-token.model";
import { JwtPayload, LoginDto, RegisterDto, RenewTokenDto, VerifyMailDto } from "./auth.dto";
import * as bcrypt from 'bcryptjs'
import { ConfigService } from "@nestjs/config";
import { JwtService } from "./jwt.service";
import { BaseService } from "src/base/base.service";
import { CacheService } from "src/base/caching/cache.service";
import { BaseApiException } from "src/base/exception/base-api.exception";
import { Token } from "src/base/interfaces/access-token.interface";
import BaseNoti from "src/base/notify";
import { sign } from 'jsonwebtoken';
import { v4 as uuidv4 } from 'uuid';
import { verify } from 'jsonwebtoken';
import * as moment from 'moment'
import { hashSync, genSaltSync } from 'bcrypt';
import { EmailService } from "src/email/email.service";
const { authenticator } = require('otplib')
@Injectable()
export class AuthService extends BaseService<AccessToken> implements OnModuleInit {
    private userService: UserService

    constructor(
        @InjectModel(AccessToken.name) public accessTokenModel: Model<AccessToken>,
        private readonly cacheService: CacheService,
        private readonly configService: ConfigService,
        private readonly emailService: EmailService,
        private moduleRef: ModuleRef
    ) {
        super(accessTokenModel)
    }

    secret = this.configService.get<string>('JWT_SECRET')

    onModuleInit() {
        this.userService = this.moduleRef.get(UserService, { strict: false });
    }

    async getAccessTokenFromRedis(payload: JwtPayload): Promise<string> {
        const { userId, sessionId } = payload
        const key = `access_token-${userId}-${sessionId}`
        return this.cacheService.get({ key })
    }

    // async register(registerDto: RegisterDto) {
    //     try {
    //         const { email, password } = registerDto
    //         const foundUser = await this.userService.findUser({ email })
    //         if (foundUser) {
    //             if (foundUser.verified == true || foundUser.verified == undefined) {
    //                 throw new BaseApiException({
    //                     message: BaseNoti.USER.USER_EXISTS
    //                 })
    //             } else {
    //                 const verifyToken = await this.signVerifyToken({ email, password })
    //                 await this.emailService.emailVerifyAccount(email, verifyToken)
    //             }
    //         } else {
    //             const verifyToken = await this.signVerifyToken({ email, password })
    //             await this.emailService.emailVerifyAccount(email, verifyToken)
    //             const user: Partial<User> = {
    //                 email,
    //                 password,
    //                 verified: false
    //             }
    //             await this.userService.createUser(user)
    //         }
    //         return {
    //             message: BaseNoti.GLOBAL.SUCCESS,
    //         }
    //     } catch (error) {
    //         throw error
    //     }
    // }

    async verifyMail(verifyMailDto: VerifyMailDto) {
        try {
            const { token } = verifyMailDto
            const data = verify(token, this.secret)
            // @ts-ignore
            const { email, password, exp } = data
            const now = moment().unix()
            if (!email || !password) {
                throw new BaseApiException({
                    message: BaseNoti.USER.VERIFY_EMAIL_FAIL
                })
            }
            if (exp < now) {
                throw new BaseApiException({
                    message: BaseNoti.USER.VERIFY_EMAIL_FAIL_EXP
                })
            }
            const foundUser = await this.userService.findUser({ email })
            if (foundUser) {
                if (foundUser.verified && foundUser.verified == true) {
                    throw new BaseApiException({
                        message: BaseNoti.USER.VERIFY_EMAIL_ALREADY
                    })
                }
                const salt = genSaltSync(10)
                const passwordHash = hashSync(password, salt)
                await this.userService.update({ _id: foundUser._id }, { verified: true, password: passwordHash })
            } else return
            return {
                message: BaseNoti.USER.VERIFY_EMAIL_SUCCESS,
                data: email
            }
        } catch (error) {
            throw error
        }
    }

    async login({ email, password, code }: LoginDto) {
        try {
            const user = await this.userService.findUser({ email })
            if (!user) throw new BaseApiException({ message: BaseNoti.USER.USER_NOT_EXISTS })
            if (!user.isActive) {
                throw new BaseApiException({ message: BaseNoti.USER.USER_NOT_ACTIVE, })
            }
            if (user.verified == false) {
                throw new BaseApiException({ message: BaseNoti.AUTH.NOT_VERIFY_MAIL })
            }
            const isMatch = await bcrypt.compare(password, user.password);
            if (!isMatch) throw new BaseApiException({ message: BaseNoti.AUTH.WRONG_PASSWORD })
            if (user && user.twofa && user.twofa.enable) {
                if (!code) {
                    throw new BaseApiException({
                        message: BaseNoti.AUTH.MISSING_2FA
                    })
                }
                const toStrCode = code.toString()
                const isValid = authenticator.verify({ token: toStrCode, secret: user.twofa.secret });
                if (!isValid) {
                    throw new BaseApiException({
                        message: BaseNoti.AUTH.INVALID_2FA
                    })
                }
            }
            const { accessToken, refreshToken } = await this.generateUserToken(user)
            const newUser = user.toObject()
            delete newUser.password
            let objectResponse = {
                user: newUser,
                tokens: {
                    accessToken,
                    refreshToken
                }
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: objectResponse
            }

        } catch (error) {
            throw error
        }
    }
    async generateUserToken(user: User) {
        try {
            const sessionId = await this.genarateDeviceId()
            const accessTokenPayload: JwtPayload = {
                userId: user._id,
                tokenType: 'accessToken',
                sessionId
            }
            const refreshTokenPayload: JwtPayload = {
                userId: user._id,
                tokenType: 'refreshToken',
                sessionId
            }
            if (user.role != undefined) {
                accessTokenPayload.role = user.role
                refreshTokenPayload.role = user.role
            }
            const [accessToken, refreshToken] = await Promise.all([
                this.signPayload(accessTokenPayload),
                this.signPayload(refreshTokenPayload)
            ])
            await Promise.all([
                this.saveAccessToken(accessToken),
                this.saveRefreshToken(refreshToken)
            ])
            return {
                accessToken: accessToken.token,
                refreshToken: refreshToken.token
            }
        } catch (error) {
            throw error
        }
    }

    async renewToken(renewTokenDto: RenewTokenDto) {
        try {
            const { refreshToken } = renewTokenDto
            const foundToken = await this.accessTokenModel.findOne({ token: refreshToken })
            if (!foundToken) {
                throw new BaseApiException({
                    message: BaseNoti.AUTH.INVALID_TOKEN,
                    status: 401
                })
            }
            const foundUser = await this.userService.findUser({ _id: foundToken.userId })
            if (!foundUser) {
                throw new BaseApiException({
                    message: BaseNoti.AUTH.USERNAME_NOT_EXISTS,
                    status: 401
                })
            }
            const sessionId = foundToken.sessionId
            const accessTokenPayload: JwtPayload = {
                userId: foundUser._id,
                tokenType: 'accessToken',
                sessionId
            }
            if (foundUser.role != undefined) {
                accessTokenPayload.role = foundUser.role
            }
            const accessToken = await this.signPayload(accessTokenPayload)
            await this.saveAccessToken(accessToken)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: accessToken
            }
        } catch (error) {
            //console.log('error :>> ', error);
            throw error
        }
    }

    async saveRefreshToken(refreshToken: Token): Promise<AccessToken> {
        try {
            const { userId, sessionId } = refreshToken
            const tokenFilter = { userId, sessionId }
            const isExits = await this.accessTokenModel.findOne(tokenFilter)
            if (!isExits) {
                const newToken = new this.accessTokenModel(refreshToken)
                return newToken.save()
            }
            return this.accessTokenModel.findOneAndUpdate(tokenFilter, { token: refreshToken.token })
        } catch (error) {
            throw error
        }
    }
    async saveAccessToken(accessTokenObject: Token): Promise<void> {
        try {
            const { token, userId, sessionId } = accessTokenObject
            const key = `access_token-${userId}-${sessionId}`
            const accessTokenExpiresIn = this.configService.get<string>('ACCESS_TOKEN_EXPIRES_IN')
            await this.cacheService.set({ key, value: token, expiresIn: accessTokenExpiresIn })
        } catch (error) {
            throw error
        }
    }

    async signPayload(payload: JwtPayload): Promise<Token> {

        let { tokenType, userId, role, sessionId } = payload
        const accessTokenExpiresIn = this.configService.get<string>('ACCESS_TOKEN_EXPIRES_IN')
        const refreshTokenExpiresIn = this.configService.get<string>('REFRESH_TOKEN_EXPIRES_IN')
        let expiresIn = tokenType == 'accessToken' ? accessTokenExpiresIn /* '10s' */ : refreshTokenExpiresIn
        let token = sign({ ...payload }, this.secret, { expiresIn: expiresIn })
        return {
            token,
            userId: payload.userId,
            sessionId
        }
    }

    async deleteTokenRedis(userId: string, sessionId: string) {
        try {
            const key = `access_token-${userId}-${sessionId}`
            await this.cacheService.remove({ key })
        } catch (error) {
            throw error
        }
    }

    async signVerifyToken(payload) {
        let token = sign({ ...payload }, this.secret, { expiresIn: '2h' })
        return token
    }

    async genarateDeviceId() {
        try {
            while (1) {
                let sessionId = uuidv4()
                const isExistsDeviceId = await this.accessTokenModel.exists({ sessionId })
                if (!isExistsDeviceId) return sessionId
            }
        } catch (error) {
            throw error
        }
    }

    async removeToken(userId: string) {
        try {
            const refreshTokens = await this.accessTokenModel.find({ userId })
            if (!refreshTokens || !refreshTokens.length) return
            await this.accessTokenModel.deleteMany({ userId })
            const key = refreshTokens.map(refreshToken => `access_token-${userId}-${refreshToken?.sessionId}`)
            await this.cacheService.remove({ key })
        } catch (error) {
            throw error
        }
    }
}
