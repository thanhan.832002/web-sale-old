import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Transform, Type } from "class-transformer";
import { IsArray, IsBoolean, IsNotEmpty, IsNumber, IsOptional, Min, Validate } from "class-validator";
import { IsObjectId } from "src/base/custom-validator";

export class OneVariantDto {
    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsNotEmpty()
    price: number

    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    comparePrice: number

    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    productCost: number

    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    fullfillmentCost: number

    @ApiProperty()
    @IsOptional()
    imageId: string

    @ApiProperty()
    @IsOptional()
    sku: string

    @ApiProperty({ type: [String] })
    @IsArray()
    @IsNotEmpty()
    listValue: string[]

    @ApiPropertyOptional()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsOptional()
    isSellable: boolean
}
export class OneVariantUpdate {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    variantId: string



    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    price: number

    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    comparePrice: number

    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    productCost: number

    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    fullfillmentCost: number

    @ApiProperty()
    @IsOptional()
    imageId: string

    @ApiProperty()
    @IsOptional()
    sku: string
}

export class UpdateListVariantDto {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    productId: string

    @ApiProperty({ type: [OneVariantUpdate] })
    @Type(() => OneVariantUpdate)
    @IsArray()
    @IsNotEmpty()
    listVariant: OneVariantUpdate[]
}