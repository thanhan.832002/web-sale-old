import { Schema, Types, Document } from "mongoose";
import { schemaOptions } from "src/base/base.shema";

export const VariantSchema: Schema = new Schema({
    productId: {
        type: Types.ObjectId,
        index: true,
        ref: 'Product'
    },
    price: Number,
    comparePrice: Number,
    productCost: Number,
    fullfillmentCost: Number,
    sku: String,
    imageId: {
        type: Types.ObjectId,
        ref: 'Gallery'
    },
    listVariantOptionId: {
        type: [Types.ObjectId],
        ref: 'VariantOption'
    },
    listValue: [String],
    isSellable: {
        type: Boolean,
        index: true,
        default: true
    }
}, { ...schemaOptions, collection: 'Variant' })

//@ts-ignore
VariantSchema.pre('save', function (this: Variant, next: any) {
    if (!this.productCost) {
        this.productCost = this.comparePrice | 0
    }
    return next()
})

export class Variant extends Document {
    productId: string
    price: number
    comparePrice: number
    productCost: number
    fullfillmentCost: number
    imageId: string
    sku: string
    listVariantOptionId: string[]
    listValue: string[]
    isSellable: boolean
}