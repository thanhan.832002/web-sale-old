import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Variant, VariantSchema } from './variant.model';
import { VariantService } from './variant.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Variant.name, schema: VariantSchema }]),
  ],
  providers: [VariantService],
  exports: [VariantService]
})
export class VariantModule { }
