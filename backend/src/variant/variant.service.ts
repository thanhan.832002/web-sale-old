import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Product } from 'src/product/product.model';
import { VariantOption } from 'src/variant-option/variant-option.model';
import { OneVariantDto } from './variant.dto';
import { Variant } from './variant.model';

@Injectable()
export class VariantService {
    constructor(
        @InjectModel(Variant.name) private readonly variantModel: Model<Variant>,
    ) { }

    async createListVariant(listVariantDto: OneVariantDto[], listcreatedVariantOption: VariantOption[], product: Partial<Product>): Promise<Variant[]> {
        try {
            const { listImageId } = product
            const listCreatedVariant = []
            for (let variantDto of listVariantDto) {
                const { listValue, imageId } = variantDto
                const listVariantOptionId = listValue.map(value => {
                    const foundVariantOption = listcreatedVariantOption.find(vp => vp.values.includes(value))
                    if (foundVariantOption) return foundVariantOption._id
                })
                if (!imageId) {
                    if (listImageId && listImageId.length) variantDto.imageId = listImageId[0]
                    else delete variantDto.imageId
                }
                const oneVariant = {
                    ...variantDto,
                    productId: product._id,
                    listVariantOptionId
                }
                const variantIns = new this.variantModel(oneVariant)
                await variantIns.save()
                listCreatedVariant.push(variantIns)
            }
            return listCreatedVariant
        } catch (error) {
            throw error
        }
    }

    async getListVariantProduct(productId: string) {
        try {
            const list = await this.variantModel.find({ productId }).populate([
                {
                    path: 'productId', select: 'coverId collectionId imageIds', populate: [
                        {
                            path: 'coverId',
                            select: 'imageUrl'
                        },
                        {
                            path: 'collectionId',
                        },
                        {
                            path: 'imageIds',
                            select: 'imageUrl'
                        },
                    ],
                },
                { path: 'imageId', select: 'imageUrl' }
            ])
            return list
        } catch (error) {
            throw error
        }
    }

    async updateListVariant(listVariantId: string[], updateDto: any) {
        try {
            await this.variantModel.updateMany({ _id: { $in: listVariantId } }, updateDto)
        } catch (error) {
            throw error
        }
    }

    async updateAllVariant(filter: any, updateDto: any) {
        try {
            await this.variantModel.updateMany(filter, updateDto)
        } catch (error) {
            throw error
        }
    }

    async deleteAllVariantProduct(filter: any) {
        try {
            await this.variantModel.deleteMany(filter)
        } catch (error) {
            throw error
        }
    }

    async findVariantId(productId: string, variantId: string) {
        try {
            const foundVariant = await this.variantModel.findOne({ productId, _id: variantId })
            return foundVariant
        } catch (error) {
            throw error
        }
    }

    async updateVariant(variantId: string, updateDto: any) {
        try {
            await this.variantModel.findByIdAndUpdate(variantId, updateDto)
        } catch (error) {
            throw error
        }
    }

    async findListVariant(filter: any) {
        try {
            const list = await this.variantModel.find(filter)
            return list
        } catch (error) {
            throw error
        }
    }
}
