import { Controller, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { SellerGuard } from 'src/base/guard/seller.guard';
import { SellerPaymentQuery } from './seller-payment.dto';
import { UserPaymentService } from './seller-payment.service';

@Controller('seller-payment')
@ApiTags('Seller Payment')
@ApiBearerAuth()
@UseGuards(SellerGuard)
@UseGuards(AuthGuard('jwt'))
export class SellerPaymentController {
    constructor(
        private readonly userPaymentService: UserPaymentService
    ) { }

    @Get('list')
    sellerGetListPayment(@GetUser() { userId }: JwtTokenDecrypted, @Query() sellerPaymentQuery: SellerPaymentQuery) {
        return this.userPaymentService.sellerGetListPayment(userId, sellerPaymentQuery)
    }

    @Post('toggle/:sellerPaymentId')
    toggleSellerPayment(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('sellerPaymentId') sellerPaymentId: string,
    ) {
        return this.userPaymentService.toggleSellerPayment(userId, sellerPaymentId)
    }

    @Put('toggle-paypal-card/:sellerPaymentId')
    toggleCardPaypal(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('sellerPaymentId') sellerPaymentId: string,
    ) {
        return this.userPaymentService.toggleCardPaypal(userId, sellerPaymentId)
    }

}
