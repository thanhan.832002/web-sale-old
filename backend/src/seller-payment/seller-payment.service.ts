import { Injectable, OnModuleInit } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model, trusted } from "mongoose";
import { BaseService } from "src/base/base.service";
import { BaseApiException } from "src/base/exception/base-api.exception";
import { BaseQuery } from "src/base/interfaces/base-query.interface";
import { DataFilter } from "src/base/interfaces/data.interface";
import BaseNoti from "src/base/notify";
import { AdminPaymentQuery, AttachPaymentDto, SellerPaymentQuery } from "./seller-payment.dto";
import { UserPayment } from "./seller-payment.model";
import * as Promise from 'bluebird'
import { StorePaymentService } from "src/store-payment/store-payment.service";
import { PaymentType } from "src/payment-method/payment.enum";
import { ModuleRef } from "@nestjs/core";
import { StoreService } from "src/store/store.service";
import { KeyRedisEnum } from "src/view/view.enum";
import { CacheService } from "src/base/caching/cache.service";

@Injectable()
export class UserPaymentService extends BaseService<UserPayment> implements OnModuleInit {
    private storeService: StoreService
    constructor(
        @InjectModel(UserPayment.name) private readonly userPaymentModel: Model<UserPayment>,
        private readonly storePaymentService: StorePaymentService,
        private readonly cacheService: CacheService,
        private moduleRef: ModuleRef
    ) { super(userPaymentModel) }

    onModuleInit() {
        this.storeService = this.moduleRef.get(StoreService, { strict: false });
    }

    async adminAttachPaymentSeller(attachPaymentDto: AttachPaymentDto) {
        try {
            const { type, listSellerId } = attachPaymentDto
            const listUserPayment = await Promise.map(listSellerId, async (sellerId) => {
                let foundPay = await this.userPaymentModel.findOne({ sellerId, type })
                if (!foundPay) {
                    const newPay = new this.userPaymentModel({ ...attachPaymentDto, sellerId })
                    const [pay, listAllStore] = await Promise.all([
                        newPay.save(),
                        this.storeService.findListStore({ sellerId })
                    ])
                    const createStorePaymentArray = listAllStore.map(store => ({
                        storeId: store._id,
                        sellerPaymentId: newPay._id,
                        status: true,
                        isOpenPaypalCard: newPay.type == PaymentType.paypal ? false : undefined
                    }))
                    await this.storePaymentService.createDefaultStorePayment(createStorePaymentArray)
                    foundPay = newPay
                    return foundPay
                } else {
                    foundPay = await this.userPaymentModel.findByIdAndUpdate(foundPay._id, { ...attachPaymentDto, sellerId }, { new: true })
                }
                await this.removeCachePolicy(sellerId)
            })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listUserPayment
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetListPayment({ sellerId }: AdminPaymentQuery) {
        try {
            let condition: { sellerId?: string } = { sellerId }
            const list = await this.userPaymentModel.find(condition).populate([
                { path: 'paymentId', select: 'name type mode enabled', populate: [{ path: 'policyId' }] }
            ])
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: list
            }
        } catch (error) {
            throw error
        }
    }

    async sellerGetListPayment(userId: string, { type }: SellerPaymentQuery) {
        try {
            let condition: { sellerId: string, type?: string } = { sellerId: userId }
            if (type) condition = { ...condition, type }
            const list = await this.userPaymentModel.find(condition).populate([
                { path: 'paymentId', select: 'type name enabled mode' }
            ])
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: list
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetListSellerPayment(paymentId: string, query: BaseQuery) {
        try {
            let { filter, limit, page, search, sort } = query
            const filterObject: DataFilter<UserPayment> = {
                limit,
                page,
                condition: { paymentId },
                population: [
                    { path: 'sellerId', select: '-password' },
                ],
                selectCols: 'createdAt sellerId status _id id type'
            }
            if (sort) {
                filterObject.sort = sort
            }
            if (filter) {
                filterObject.condition = { ...filterObject.condition, ...filter }
            }
            if (search) {
                filterObject.condition['$or'] = [
                    { name: { '$regex': search, '$options': 'i' } },
                    { email: { '$regex': search, '$options': 'i' } }
                ]
            }
            let listPayment: any = await this.getListDocument(filterObject)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listPayment
            }
        } catch (error) {
            throw error
        }
    }

    async adminDeletePaymentSeller(adminId: string, userPaymentId: string) {
        try {
            const sellerPayment = await this.findUserPaymentById(userPaymentId)
            await this.storePaymentService.deleteStorePayment({ sellerPaymentId: userPaymentId })
            const { sellerId } = sellerPayment
            await this.userPaymentModel.findByIdAndDelete(userPaymentId)
            await this.removeCachePolicy(sellerId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async deleteManyPayment(filter: any) {
        try {
            await this.userPaymentModel.deleteMany(filter)
        } catch (error) {
            throw error
        }
    }

    async countPayment(filter: any) {
        try {
            return this.userPaymentModel.countDocuments(filter) || 0
        } catch (error) {
            throw error
        }
    }

    async findUserPaymentById(userPaymentId: string) {
        try {
            const found = userPaymentId ? await this.userPaymentModel.findById(userPaymentId) : null
            if (!found) {
                throw new BaseApiException({
                    message: BaseNoti.USER_PAYMENT.USER_PAYMENT_NOT_EXISTS
                })
            }
            return found
        } catch (error) {
            throw error
        }
    }

    async findListUserPayment(filter: any): Promise<any> {
        try {
            return await this.userPaymentModel.find(filter).populate([{ path: 'paymentId', select: 'publicKey', populate: 'policyId' }]).lean()
        } catch (error) {
            throw error
        }
    }

    async toggleSellerPayment(userId: string, sellerPaymentId: string) {
        try {
            const foundSellerPayment: any = await this.userPaymentModel.findOne({
                sellerId: userId,
                _id: sellerPaymentId
            }).populate('paymentId')
            if (!foundSellerPayment) {
                throw new BaseApiException({
                    message: BaseNoti.USER_PAYMENT.USER_PAYMENT_NOT_EXISTS
                })
            }
            let paymentUpdated
            if (foundSellerPayment.paymentId.enabled == true) {
                paymentUpdated = await this.userPaymentModel.findByIdAndUpdate(sellerPaymentId, { status: !foundSellerPayment.status }, { new: true })
            } else {
                paymentUpdated = await this.userPaymentModel.findByIdAndUpdate(sellerPaymentId, { status: false }, { new: true })

            }
            if (paymentUpdated.status == false) {
                await this.storePaymentService.changeStatusSellerPayment([paymentUpdated._id], false)
            }
            await this.removeCachePolicy(userId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async toggleCardPaypal(userId: string, sellerPaymentId: string) {
        try {
            const foundSellerPayment: any = await this.userPaymentModel.findOne({
                sellerId: userId,
                type: PaymentType.paypal,
                _id: sellerPaymentId
            })
            if (!foundSellerPayment) {
                throw new BaseApiException({
                    message: BaseNoti.USER_PAYMENT.USER_PAYMENT_NOT_EXISTS
                })
            }
            if (foundSellerPayment.isOpenPaypalCard) {
                await this.storePaymentService.updateMany({ sellerPaymentId: foundSellerPayment._id }, { isOpenPaypalCard: false })
            }
            await this.userPaymentModel.findByIdAndUpdate(sellerPaymentId, { isOpenPaypalCard: !foundSellerPayment.isOpenPaypalCard }, { new: true })
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async createDefaultUserPayment(sellerId: string, storeId: string) {
        try {
            const listSellerPayment = await this.userPaymentModel.find({ sellerId })
            const listStorePaymentDto = listSellerPayment.map(sellerPayment => ({
                sellerPaymentId: sellerPayment._id,
                status: sellerPayment.status,
                storeId,
                paypalCard: sellerPayment.type == PaymentType.paypal ? false : undefined
            }))
            const listPayment = await this.storePaymentService.createDefaultStorePayment(
                [
                    ...listStorePaymentDto,
                ])
            return listPayment
        } catch (error) {
            throw error
        }
    }

    async turnOffAllSellerPayment(paymentId: string) {
        try {
            await this.userPaymentModel.updateMany({ paymentId }, { status: false })
            const listSellerPayment = await this.userPaymentModel.find({ paymentId })
            const listSellerPaymentId = listSellerPayment.map(payment => payment._id)
            await this.storePaymentService.changeStatusSellerPayment(listSellerPaymentId, false)
        } catch (error) {
            throw error
        }
    }

    async removeCachePolicy(sellerId) {
        try {
            let listKey = []
            const listStore = await this.storeService.findListStore({ sellerId })
            for (let store of listStore) {
                const { domain } = store
                listKey.push(`${KeyRedisEnum.policy}-${domain}`)
                listKey.push(`${KeyRedisEnum.listPayment}-${domain}`)
            }
            await this.cacheService.remove({ key: listKey })
        } catch (error) {

        }
    }
}
