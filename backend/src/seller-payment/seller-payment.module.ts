import { Module } from '@nestjs/common';
import { UserPaymentService } from './seller-payment.service';
import { UserPayment, UserPaymentSchema } from './seller-payment.model';
import { MongooseModule } from '@nestjs/mongoose';
import { UserPaymentAdminController } from './seller-payment-admin.controller';
import { SellerPaymentController } from './seller-payment.controller';
import { StorePaymentModule } from 'src/store-payment/store-payment.module';
import { SharedModule } from 'src/base/shared.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: UserPayment.name, schema: UserPaymentSchema }]),
    StorePaymentModule,
    SharedModule
  ],
  providers: [UserPaymentService],
  exports: [UserPaymentService],
  controllers: [UserPaymentAdminController, SellerPaymentController]
})
export class UserPaymentModule { }
