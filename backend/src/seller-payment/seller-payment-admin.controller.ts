import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdminGuard, } from 'src/base/guard/admin.guard';
import { AdminPaymentQuery } from './seller-payment.dto';
import { UserPaymentService } from './seller-payment.service';

@Controller('seller-payment-admin')
@ApiTags('User Payment Admin')
@ApiBearerAuth()
@UseGuards(AdminGuard)
@UseGuards(AuthGuard('jwt'))
export class UserPaymentAdminController {
    constructor(
        private readonly userPaymentService: UserPaymentService
    ) { }

    @Get('list')
    adminGetListPayment(@Query() adminPaymentQuery: AdminPaymentQuery) {
        return this.userPaymentService.adminGetListPayment(adminPaymentQuery)
    }

}
