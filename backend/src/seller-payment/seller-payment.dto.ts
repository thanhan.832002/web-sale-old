import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, Validate } from "class-validator";
import { IsListObjectId, IsObjectId } from "src/base/custom-validator";
import { PaymentType } from "src/payment-method/payment.enum";

export class SellerPaymentQuery {
    @ApiPropertyOptional()
    @IsOptional()
    type: PaymentType
}

export class AdminPaymentQuery {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    sellerId: string
}

export class AttachPaymentDto {
    type: PaymentType

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    paymentId: string

    @ApiProperty({ type: [String] })
    @Validate(IsListObjectId)
    @IsNotEmpty()
    listSellerId: string[]
}