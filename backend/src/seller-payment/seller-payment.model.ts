import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { PaymentType } from "src/payment-method/payment.enum"

export const UserPaymentSchema: Schema = new Schema({
    sellerId: {
        type: Types.ObjectId,
        index: true,
        ref: 'User'
    },
    paymentId: {
        type: Types.ObjectId,
        index: true,
        ref: 'Payment'
    },
    type: {
        type: String,
        enum: PaymentType
    },
    status: {
        type: Boolean,
        default: true
    },
    isOpenPaypalCard: {
        type: Boolean,
    }
}, { ...schemaOptions, collection: 'UserPayment' })


export class UserPayment extends Document {
    sellerId: string
    paymentId: string
    type: string
    status: boolean
    isOpenPaypalCard: boolean
}
