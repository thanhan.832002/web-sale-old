import { Module } from '@nestjs/common';
import { ProductReviewService } from './product-review.service';
import { ProductReviewController } from './product-review.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductReview, ProductReviewSchema } from './product-review.model';
import { ProductModule } from 'src/product/product.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: ProductReview.name, schema: ProductReviewSchema }]),
    ProductModule
  ],
  providers: [ProductReviewService],
  exports: [ProductReviewService],
  controllers: [ProductReviewController]
})
export class ProductReviewModule { }
