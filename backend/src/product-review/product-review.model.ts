import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsEnum, IsNotEmpty, IsOptional, Matches, Validate } from "class-validator";
import { Document, Schema, Types } from "mongoose";
import { schemaOptions } from "src/base/base.shema";
import { IsObjectId } from "src/base/custom-validator";

export const ProductReviewSchema: Schema = new Schema({
    email: String,
    phoneNumber: String,
    name: String,
    images: [String],
    rating: { type: Number, enum: [1, 2, 3, 4, 5] },
    content: String,
    title: String,
    productId: {
        type: Types.ObjectId,
        index: true,
        ref: 'Product'
    }
}, { ...schemaOptions, collection: 'ProductReview' })

export class ProductReview extends Document {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    productId: string

    @ApiProperty()
    @IsOptional()
    @Matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
    email: string

    @ApiProperty()
    @IsNotEmpty()
    name: string

    @ApiPropertyOptional()
    @IsOptional()
    phoneNumber: string

    @ApiProperty({
        type: [String]
    })
    @IsOptional()
    images: string[]

    @ApiProperty()
    @IsOptional()
    rating: number

    @ApiProperty()
    @IsNotEmpty()
    content: string

    @ApiProperty()
    @IsNotEmpty()
    title: string
}