import { ApiProperty } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsNotEmpty } from "class-validator";
import { CreateReviewDto } from "src/review/review.dto";

export class CreateProductReviewDto extends CreateReviewDto {

}