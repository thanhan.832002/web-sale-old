import { Body, Controller, Delete, Get, Param, Post, Put, UploadedFiles, UseGuards, UseInterceptors } from '@nestjs/common';
import { ProductReviewService } from './product-review.service';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { extname } from 'path';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { CreateProductReviewDto } from './product-review.dto';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { UpdateReviewDto } from 'src/review/review.dto';
const imageFileFilter = (req: any, file: any, callback: Function) => {
    const ext = extname(file.originalname);
    if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.webp') {
        return callback(null, false);
    }
    callback(null, true);
};

const multerOptionsImage: MulterOptions = {
    // storage: storageOptionsImages,
    fileFilter: imageFileFilter,
    limits: {
        fileSize: 10485760 //10MB
    }
};

@ApiTags('Product Review')
@ApiBearerAuth()
@UseGuards(AdminGuard)
@UseGuards(AuthGuard('jwt'))
@Controller('product-review')
export class ProductReviewController {
    constructor(
        private readonly productReviewService: ProductReviewService
    ) { }

    @Post('create-review/:productId')
    @UseInterceptors(AnyFilesInterceptor(multerOptionsImage))
    async createProductReview(
        @Param('productId') productId: string,
        @Body() createReviewDto: CreateProductReviewDto,
        @UploadedFiles() images: Array<Express.Multer.File>
    ) {
        return this.productReviewService.createProductReview(productId, createReviewDto, images)
    }

    @Delete('delete/:productId/:reviewId')
    sellerDeleteReview(
        @Param('productId') productId: string,
        @Param('reviewId') reviewId: string
    ) {
        return this.productReviewService.deleteReview(productId, reviewId)
    }

    @Put('edit/:productId/:reviewId')
    @UseInterceptors(AnyFilesInterceptor(multerOptionsImage))
    sellerEditReview(
        @Param('productId') productId: string,
        @Param('reviewId') reviewId: string,
        @Body() updateReviewDto: UpdateReviewDto,
        @UploadedFiles() images: Array<Express.Multer.File>
    ) {
        return this.productReviewService.editReview(productId, reviewId, updateReviewDto, images)
    }

    @Get('list/:productId')
    getListProductReview(
        @Param('productId') productId: string
    ) {
        return this.productReviewService.getListProductReview(productId)
    }
}
