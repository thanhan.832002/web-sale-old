import { Injectable } from '@nestjs/common';
import { ProductReview } from './product-review.model';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ProductService } from 'src/product/product.service';
import * as path from 'path';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';
const sharp = require('sharp');
import * as fs from 'fs';
import { UpdateReviewDto } from 'src/review/review.dto';
@Injectable()
export class ProductReviewService {
    constructor(
        @InjectModel(ProductReview.name) private productReviewModel: Model<ProductReview>,
        private readonly productService: ProductService
    ) { }

    generateFilename = (file: any) => {
        if (file.originalname.includes('.png')) file.originalname = file.originalname.replace('.png', '.webp')
        if (file.originalname.includes('.jpg')) file.originalname = file.originalname.replace('.jpg', '.webp')
        if (file.originalname.includes('.jpeg')) file.originalname = file.originalname.replace('.jpeg', '.webp')
        let filename = `${Date.now()}-${file.originalname}`
        filename = filename.replaceAll(/[%#]/g, "");
        return filename
    };

    async createProductReview(productId: string, productReview: Partial<ProductReview>, images?: Array<Express.Multer.File>) {
        try {
            await this.productService.findProductId(productId)
            productReview.productId = productId
            if (images && images.length) {
                for (let image of images) {
                    const fileName = this.generateFilename(image)
                    image.filename = fileName
                    sharp(image.buffer)
                        .resize(800)
                        .toFile(`./public/reviews/${fileName}`, (err, info) => { });
                }
                productReview.images = images.map(image => `reviews/${image.filename}`)
            }
            const newReview = new this.productReviewModel(productReview)
            await newReview.save()
            return newReview
        } catch (error) {
            throw error
        }
    }

    async editReview(productId: string, reviewId: string, updateReviewDto: UpdateReviewDto, images?: Array<Express.Multer.File>) {
        try {
            let update: any = { ...updateReviewDto }
            if (images?.length && !update.images) {
                for (let image of images) {
                    const fileName = this.generateFilename(image)
                    image.filename = fileName
                    sharp(image.buffer)
                        .resize(800)
                        .toFile(`./public/reviews/${fileName}`, (err, info) => { });
                }
                update.images = images.map(image => `reviews/${image.filename}`)
            }
            const foundReview = await this.productReviewModel.findOneAndUpdate({ productId, _id: reviewId }, update)
            if (!foundReview) {
                throw new BaseApiException({
                    message: BaseNoti.REVIEW.REVIEW_NOT_EXISTS
                })
            }
            // const check = await this
            if (images?.length && foundReview.images?.length && !update.images) {
                for (const image of foundReview.images) {
                    const publicPath = path.join(__dirname, '../../public')
                    try {
                        const filePath = path.join(publicPath, image)
                        // fs.unlinkSync(filePath)
                    } catch (error) { }
                }
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async deleteReview(productId: string, reviewId: string) {
        try {
            await this.productService.findProductId(productId)
            const { images } = await this.findReviewById(reviewId, productId)

            if (images && images.length) {
                for (const image of images) {
                    const publicPath = path.join(__dirname, '../../public')
                    try {
                        const filePath = path.join(publicPath, image)
                        fs.unlinkSync(filePath)
                    } catch (error) { }
                }
            }
            await this.productReviewModel.findByIdAndDelete(reviewId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async getListProductReview(productId: string) {
        try {
            const foundList = await this.productReviewModel.find({ productId })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: foundList
            }
        } catch (error) {
            throw error
        }
    }

    async findListReview(productId: string) {
        try {
            const foundList = await this.productReviewModel.find({ productId })
            return foundList
        } catch (error) {
            throw error
        }
    }

    async findReviewById(reviewId: string, productId?: string) {
        try {
            const foundReview = await this.productReviewModel.findById(reviewId)
            if (!foundReview || productId && foundReview.productId.toString() != productId) {
                throw new BaseApiException({
                    message: BaseNoti.REVIEW.REVIEW_NOT_EXISTS
                })
            }
            return foundReview
        } catch (error) {
            throw error
        }
    }

}
