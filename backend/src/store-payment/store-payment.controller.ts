import { Controller, Get, Param, Put, UseGuards } from '@nestjs/common';
import { StorePaymentService } from './store-payment.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SellerGuard } from 'src/base/guard/seller.guard';

@Controller('store-payment')
@ApiTags('Store Payment')
@UseGuards(SellerGuard)
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
export class StorePaymentController {
    constructor(
        private readonly storePaymentService: StorePaymentService
    ) { }

    @Put('toggle/:storePaymentId')
    toggleSellerPayment(
        @Param('storePaymentId') storePaymentId: string,
    ) {
        return this.storePaymentService.changeStatusStorePayment(storePaymentId)
    }

    @Put('toggle-paypal-card/:storePaymentId')
    toggleSellerPaymentPaypalCard(
        @Param('storePaymentId') storePaymentId: string,
    ) {
        return this.storePaymentService.changeStatusPaypalCard(storePaymentId)
    }
}
