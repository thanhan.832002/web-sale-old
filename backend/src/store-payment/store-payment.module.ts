import { Module } from '@nestjs/common';
import { StorePaymentService } from './store-payment.service';
import { StorePaymentController } from './store-payment.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { StorePayment, StorePaymentSchema } from './store-paymnet.model';
import { SharedModule } from 'src/base/shared.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: StorePayment.name, schema: StorePaymentSchema }]),
    SharedModule
  ],
  providers: [StorePaymentService],
  exports: [StorePaymentService],
  controllers: [StorePaymentController]
})
export class StorePaymentModule { }
