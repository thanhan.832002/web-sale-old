import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { StorePayment } from './store-paymnet.model';
import BaseNoti from 'src/base/notify';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import { PaymentType } from 'src/payment-method/payment.enum';
import { CacheService } from 'src/base/caching/cache.service';
import { KeyRedisEnum } from 'src/view/view.enum';
import { ModuleRef } from '@nestjs/core';
import { Store } from 'src/store/store.model';
import { HomepageService } from 'src/homepage/homepage.service';

@Injectable()
export class StorePaymentService implements OnModuleInit {
    private homepageService: HomepageService
    constructor(
        @InjectModel(StorePayment.name) private readonly storePaymentModel: Model<StorePayment>,
        private readonly cacheService: CacheService,
        private moduleRef: ModuleRef
    ) { }

    onModuleInit() {
        this.homepageService = this.moduleRef.get(HomepageService, { strict: false });
    }

    async changeStatusStorePayment(storePaymentId: string) {
        try {
            const foundPayment: any = await this.storePaymentModel.findById(storePaymentId).populate('sellerPaymentId')
            if (!foundPayment) {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.PAYMENT_NOT_EXISTS
                })
            }
            if (foundPayment.sellerPaymentId.status == true) {
                let updateField = foundPayment.sellerPaymentId.type == 'paypal' && foundPayment.status == true ? { status: !foundPayment.status, isOpenPaypalCard: false } : { status: !foundPayment.status }
                await this.storePaymentModel.findByIdAndUpdate(storePaymentId, updateField, { new: true })
            } else {
                await this.storePaymentModel.findByIdAndUpdate(storePaymentId, { status: false }, { new: true })
            }
            await this.removeCachePolicyNPayment(storePaymentId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async changeStatusPaypalCard(storePaymentId: string) {
        try {
            const foundPayment: any = await this.storePaymentModel.findById(storePaymentId).populate('sellerPaymentId')
            if (!foundPayment) {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.PAYMENT_NOT_EXISTS
                })
            }
            let updateField: any = {}
            if (foundPayment.sellerPaymentId.status == true) {
                if (foundPayment.status && foundPayment.sellerPaymentId.isOpenPaypalCard) {
                    updateField = { isOpenPaypalCard: !foundPayment.isOpenPaypalCard }
                } else {
                    updateField = { isOpenPaypalCard: false }
                }
            }
            else {
                updateField = { status: false, isOpenPaypalCard: false }
            }
            await this.storePaymentModel.findByIdAndUpdate(storePaymentId, updateField)
            await this.removeCachePolicyNPayment(storePaymentId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async changeStatusSellerPayment(listSellerPaymentId: string[], status: boolean) {
        try {
            await this.storePaymentModel.updateMany({ sellerPaymentId: { $in: listSellerPaymentId } }, { status })
        } catch (error) {
            throw error
        }
    }

    async findListStorePaymentBuyer(storeId: string) {
        try {
            const list = await this.storePaymentModel.find({ storeId, status: true }).populate([
                { path: 'sellerPaymentId', populate: { path: 'paymentId', select: '-secretKey -__v' } }
            ]).lean()
            return list
        } catch (error) {
            throw error
        }
    }

    async getStripeStore(storeId: string) {
        let list = await this.storePaymentModel.find({ storeId, status: true }).populate([
            { path: 'sellerPaymentId', populate: { path: 'paymentId', select: 'secretKey' } }
        ]).lean()
        //@ts-ignore
        list = list.filter(payment => payment?.sellerPaymentId?.type && payment?.sellerPaymentId?.type == 'stripe' && payment?.sellerPaymentId?.status)
        return list
    }

    async findListStorePayment(storeId: string) {
        try {
            const list = await this.storePaymentModel.find({ storeId }).populate([
                { path: 'sellerPaymentId', populate: { path: 'paymentId', select: 'name' } }

            ]).lean()
            return list
        } catch (error) {
            throw error
        }
    }

    async getPaypalStorePolicy(store: Store) {
        try {
            let list = await this.storePaymentModel.find({ storeId: store._id, status: true }).populate([
                { path: 'sellerPaymentId', populate: { path: 'paymentId', select: 'enabled policyId', populate: 'policyId' } },
                { path: 'storeId', select: 'supportEmail storeName' }
            ]).lean()
            //@ts-ignore
            list = list.filter(payment => payment?.sellerPaymentId?.type && payment?.sellerPaymentId?.type == PaymentType.paypal && payment?.sellerPaymentId?.status)
            let paypal
            if (list?.length) {
                paypal = list[0]
            }
            const { supportEmail, storeName } = store
            let policies
            if (paypal?.sellerPaymentId?.paymentId?.policyId) {
                const store = paypal?.storeId
                policies = paypal?.sellerPaymentId?.paymentId?.policyId
            } else {
                const domain = store.domain
                const indexdot = domain.indexOf('.')
                const main = domain.slice(indexdot + 1, domain.length)
                policies = await this.homepageService.getPolicy(main)
            }
            for (let policy in policies) {
                try {
                    if (policy !== '_id' && policy !== 'createdAt' && policy !== 'updatedAt' && policy !== '__v') {
                        policies[policy] = policies[policy].replace(/{{shop}}/g, storeName)
                        policies[policy] = policies[policy].replace(/{{email_support}}/g, supportEmail)
                    }
                } catch (error) { }
            }
            return policies
        } catch (error) {
            throw error
        }
    }

    async createDefaultStorePayment(listStorePaymentDto: Partial<StorePayment>[]) {
        try {
            const listIns = await this.storePaymentModel.insertMany(listStorePaymentDto)
            return await this.storePaymentModel.find({ _id: { $in: listIns.map(el => el._id) } }).populate([
                { path: 'sellerPaymentId', populate: { path: 'paymentId', select: 'name' } }
            ])
        } catch (error) {
            throw error
        }
    }

    async updateMany(filter: any, updateField: any) {
        try {
            await this.storePaymentModel.updateMany(filter, updateField)
        } catch (error) {
            throw error
        }
    }

    async removeCachePolicyNPayment(id: string) {
        try {
            const foundStore = await this.storePaymentModel.findById(id).populate({ path: 'storeId', select: 'domain' })
            //@ts-ignore
            const domain = foundStore?.storeId?.domain
            const listKey = [`${KeyRedisEnum.policy}-${domain}`, `${KeyRedisEnum.listPayment}-${domain}`, `${KeyRedisEnum.skstripe}-${domain}`]
            await this.cacheService.remove({ key: listKey })
        } catch (error) {

        }
    }

    async deleteStorePayment(filter: any) {
        try {
            await this.storePaymentModel.deleteMany(filter)
        } catch (error) {
            throw error
        }
    }
}
