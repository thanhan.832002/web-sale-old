import { ApiProperty } from "@nestjs/swagger"
import { Type } from "class-transformer"
import { IsNotEmpty, IsNumber, Validate, IsOptional } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsObjectId } from "src/base/custom-validator"

export const StorePaymentSchema: Schema = new Schema({
    storeId: {
        type: Types.ObjectId,
        ref: 'Store'
    },
    sellerPaymentId: {
        type: Types.ObjectId,
        ref: 'UserPayment'
    },
    status: {
        type: Boolean,
        default: false
    },
    isOpenPaypalCard: { type: Boolean }
}, { ...schemaOptions, collection: 'StorePayment' })


export class StorePayment extends Document {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    storeId: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    sellerPaymentId: string

    @ApiProperty()
    @IsOptional()
    status: boolean

    @ApiProperty()
    @IsOptional()
    isOpenPaypalCard: boolean
}
