import { Module } from '@nestjs/common';
import { OrderModule } from 'src/order/order.module';
import { PaymentModule } from 'src/payment-method/payment.module';
import { UserModule } from 'src/user/user.module';
import { StatsAdminController } from './stats-admin.controller';
import { StatsSellerController } from './stats-seller.controller';
import { StatsService } from './stats.service';
import { ViewModule } from 'src/view/view.module';
import { StoreModule } from 'src/store/store.module';
import { ProductModule } from 'src/product/product.module';
import { StatsMasterController } from './stats-master.controller';

@Module({
  imports: [
    PaymentModule,
    OrderModule,
    UserModule,
    ViewModule,
    StoreModule,
    ProductModule
  ],
  controllers: [StatsSellerController, StatsAdminController, StatsMasterController],
  providers: [StatsService]
})
export class StatsModule { }
