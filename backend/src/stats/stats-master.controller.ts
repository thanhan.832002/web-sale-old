import { Controller, UseGuards, Get, Query, Post } from "@nestjs/common";
import { StatsService } from "./stats.service";
import { MasterGuard } from "src/base/guard/master.guard";
import { AuthGuard } from "@nestjs/passport";
import { ApiTags, ApiBearerAuth } from "@nestjs/swagger";
import { NewDateQuery } from "./stats.dto";

@Controller('stats-master')
@ApiTags('Stats Master')
@ApiBearerAuth()
@UseGuards(MasterGuard)
@UseGuards(AuthGuard('jwt'))
export class StatsMasterController {
    constructor(
        private readonly statsService: StatsService
    ) { }

    @Get('by-time')
    async masterGetStatsByTime(
        @Query() query: NewDateQuery
    ) {
        return this.statsService.masterGetStatsBytime(query)
    }

    @Get('email')
    async masterGetStatsEmail(
        @Query() query: NewDateQuery
    ) {
        return this.statsService.masterGetStatsEmail(query)
    }

    @Post('resend-fail-mail')
    async masterResendEmail() {
        return this.statsService.masterResendEmail()
    }
}