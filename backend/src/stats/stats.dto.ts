import { ApiPropertyOptional } from "@nestjs/swagger"
import { Transform, Type } from "class-transformer"
import { IsNumber, IsObject, IsOptional, Validate } from "class-validator"
import { Types } from "mongoose"
import { IsListObjectId, IsObjectId } from "src/base/custom-validator"

export class NewDateQuery {
    @ApiPropertyOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsOptional()
    from?: string

    @ApiPropertyOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsOptional()
    to?: string

    @ApiPropertyOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsOptional()
    timezone?: string

    @ApiPropertyOptional()
    @IsOptional()
    @IsObject()
    filter?: any

    @ApiPropertyOptional()
    @IsOptional()
    @IsObject()
    sort?: object

    @ApiPropertyOptional()
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    limit?: number

    @ApiPropertyOptional()
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    page?: number

    @ApiPropertyOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsOptional()
    search?: string
}

export class NewViewQuery extends NewDateQuery {
    @ApiPropertyOptional()
    @IsOptional()
    storeId?: string | Types.ObjectId
}

export class SellerUtmQuery extends NewDateQuery {
    @ApiPropertyOptional()
    @IsOptional()
    utmMedium?: string

    @ApiPropertyOptional()
    @IsOptional()
    utmCampaign?: string
}