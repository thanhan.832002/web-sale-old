import { Controller, Get, Param, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { StatsService } from './stats.service';
import { AuthGuard } from '@nestjs/passport';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { NewDateQuery } from './stats.dto';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';

@Controller('stats-admin')
@ApiTags('Stats Admin')
@ApiBearerAuth()
@UseGuards(AdminGuard)
@UseGuards(AuthGuard('jwt'))
export class StatsAdminController {
    constructor(
        private readonly statsService: StatsService
    ) { }

    @Get('seller/:sellerId')
    adminGetStatsSeller(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('sellerId') sellerId: string) {
        return this.statsService.adminGetStatsSeller(userId, sellerId)
    }

    @Get('payment/:paymentId')
    adminGetPaymentStats(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('paymentId') paymentId: string,
        @Query() query: NewDateQuery) {
        return this.statsService.adminGetPaymentStats(userId, paymentId, query)
    }

    @Get('by-time')
    adminGetTotalStatsByTime(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: NewDateQuery) {
        return this.statsService.adminGetTotalStatsByTime(userId, query)
    }

    @Get('recent')
    adminGetRecent(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: NewDateQuery) {
        return this.statsService.adminGetRecent(userId, query)
    }

    @Get('analytics-utm')
    getAnalyticsUtm(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: NewDateQuery) {
        return this.statsService.adminGetAnalyticsDate(userId, query)
    }

    @Get('analytics-domain')
    getAnalyticsDomain(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: NewDateQuery) {
        return this.statsService.adminGetAnalyticsDomain(userId, query)
    }

    @Get('analytics-seller')
    getAnalyticsSeller(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: NewDateQuery) {
        return this.statsService.adminGetAnalyticsSeller(userId, query)
    }

    @Get('analytics-product')
    getAnalyticProduct(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: NewDateQuery) {
        return this.statsService.adminGetAnalyticsProduct(userId, query)
    }

    @Get('abandoned-date')
    async adminGetAbandonDate(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: NewDateQuery) {
        return this.statsService.adminGetAbandonDate(userId, query)
    }

    @Get('abandoned-domain')
    async adminGetAbandonDomain(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: NewDateQuery) {
        return this.statsService.adminGetAbandonDomain(userId, query)
    }

    @Get('abandoned-product')
    async adminGetAbandonProduct(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: NewDateQuery) {
        return this.statsService.adminGetAbandonProduct(userId, query)
    }

    @Get('abandoned-seller')
    async adminGetAbandonSeller(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: NewDateQuery) {
        return this.statsService.adminGetAbandonSeller(userId, query)
    }
}
