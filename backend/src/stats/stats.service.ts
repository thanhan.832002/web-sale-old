import { Injectable } from '@nestjs/common';
import { OrderService } from 'src/order/order.service';
import { UserService } from 'src/user/user.service';
import BaseNoti from 'src/base/notify';
import { PaymentService } from 'src/payment-method/payment.service';
import * as Promise from 'bluebird'
import { ViewService } from 'src/view/view.service';
import { StoreService } from 'src/store/store.service';
import { Types } from 'mongoose';
import { NewDateQuery, NewViewQuery, SellerUtmQuery } from './stats.dto';
import { ProductService } from 'src/product/product.service';
var momentTz = require("moment-timezone");
@Injectable()
export class StatsService {
    constructor(
        private readonly orderService: OrderService,
        private readonly paymentService: PaymentService,
        private readonly userService: UserService,
        private readonly viewService: ViewService,
        private readonly storeService: StoreService,
        private readonly productService: ProductService
    ) { }

    async masterGetStatsBytime({ from, to, timezone }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
            const stats = await this.viewService.masterGetTotalStatsByTime(timeFrom, timeTo)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: stats
            }
        } catch (error) {
            throw error
        }
    }

    async masterGetStatsEmail({ from, to, timezone }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
            const stats = await this.orderService.masterGetStatsEmail(timeFrom, timeTo)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: stats
            }
        } catch (error) {
            throw error
        }
    }

    async masterResendEmail() {
        try {
            return await this.orderService.masterResendEmail()
        } catch (error) {
            throw error
        }
    }

    async adminGetStatsSeller(adminId: string, sellerId: string) {
        try {
            const [foundUser, { total, purchase, productCost }] = await Promise.all([
                this.userService.findUserByAdmin(adminId, sellerId),
                this.viewService.sellerGetRevenueSeller(sellerId)
            ])
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: {
                    amount: Math.round((total) * 100) / 100,
                    profit: Math.round((total - productCost) * 100) / 100,
                    order: purchase,
                    productCost: Math.round((productCost) * 100) / 100
                }
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetPaymentStats(adminId: string, paymentId: string, query: NewDateQuery) {
        try {
            await this.paymentService.findPaymentByAdmin(adminId, paymentId)
            const { total, count } = await this.orderService.adminGetPaymentStats(paymentId, query)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: {
                    order: count,
                    amount: total
                }
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetTotalStatsByTime(adminId: string, { from, to, timezone }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
            const stats = await this.viewService.adminGetTotalStatsByTime(adminId, timeFrom, timeTo)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: stats
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetRecent(adminId: string, query: NewDateQuery) {
        try {
            let { page, limit } = query
            if (!page) page = 1
            if (!limit) limit = 25
            const listProduct = await this.orderService.adminGetRecent(adminId, query)
            const totalCount = listProduct.length
            const documents = listProduct.slice((page - 1) * limit, page * limit)
            const totalInList = documents.length
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: {
                    page: page,
                    total: totalCount,
                    totalInList,
                    data: documents
                }
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetAnalyticsDomain(adminId: string, { from, to, timezone }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).tz("US/Arizona").startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).tz("US/Arizona").endOf('days').tz('Atlantic/Azores').toDate()
            const listRevenue = await this.viewService.adminRevenueDomain(adminId, { from: timeFrom, to: timeTo })
            await Promise.map(listRevenue, async (report) => {
                const { storeId, revenue, revenueCancel } = report
                const [domain] = await Promise.all([
                    this.storeService.getDomainFromId(storeId),
                ])
                const revenueTotal = revenue + revenueCancel
                report.group = domain
                report.revenueTotal = this.orderService.roundTwoDigit(revenueTotal)
                report.revenue = this.orderService.roundTwoDigit(revenue)
            })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listRevenue && listRevenue.length && listRevenue[0] ? listRevenue : []
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetAbandonDomain(adminId: string, { from, to, timezone }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).tz("US/Arizona").startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).tz("US/Arizona").endOf('days').tz('Atlantic/Azores').toDate()
            const listRevenue = await this.viewService.adminGetAbandonDomain(adminId, { from: timeFrom, to: timeTo })
            await Promise.map(listRevenue, async (report) => {
                const { storeId, revenue } = report
                const domain = await this.storeService.getDomainFromId(storeId)
                report.group = domain
                report.revenue = this.orderService.roundTwoDigit(revenue)
            })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listRevenue && listRevenue.length && listRevenue[0] ? listRevenue : []
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetAnalyticsDate(adminId: string, { from, to, timezone }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).tz("US/Arizona").startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).tz("US/Arizona").endOf('days').tz('Atlantic/Azores').toDate()
            const listRevenue = await this.viewService.adminRevenueDate(adminId, { from: timeFrom, to: timeTo })
            await Promise.map(listRevenue, async (report) => {
                const { revenue, revenueCancel } = report
                const revenueTotal = report.revenue + revenueCancel
                report.revenueTotal = this.orderService.roundTwoDigit(revenueTotal)
                report.revenue = this.orderService.roundTwoDigit(revenue)
            })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listRevenue && listRevenue.length && listRevenue[0] ? listRevenue : []
            }
        } catch (error) {
            throw error
        }
    }


    async adminGetAbandonDate(adminId: string, { from, to, timezone }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).tz("US/Arizona").startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).tz("US/Arizona").endOf('days').tz('Atlantic/Azores').toDate()
            const listRevenue = await this.viewService.adminRevenueDateAbandoned(adminId, { from: timeFrom, to: timeTo })
            await Promise.map(listRevenue, async (report) => {
                const { revenue } = report
                report.revenueTotal = this.orderService.roundTwoDigit(revenue)
                report.revenue = this.orderService.roundTwoDigit(revenue)
            })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listRevenue && listRevenue.length && listRevenue[0] ? listRevenue : []
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetAnalyticsSeller(adminId: string, { from, to, timezone }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).tz("US/Arizona").startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).tz("US/Arizona").endOf('days').tz('Atlantic/Azores').toDate()
            const listRevenue = await this.viewService.adminRevenueSeller(adminId, { from: timeFrom, to: timeTo })
            await Promise.map(listRevenue, async (report) => {
                const { sellerId, revenue, revenueCancel } = report
                const [seller] = await Promise.all([
                    this.userService.findUserById(sellerId)
                ])
                const revenueTotal = report.revenue + revenueCancel
                report.group = seller.email
                report.revenueTotal = this.orderService.roundTwoDigit(revenueTotal)
                report.revenue = this.orderService.roundTwoDigit(revenue)
            })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listRevenue && listRevenue.length && listRevenue[0] ? listRevenue : []
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetAbandonSeller(adminId: string, { from, to, timezone }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).tz("US/Arizona").startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).tz("US/Arizona").endOf('days').tz('Atlantic/Azores').toDate()
            const listRevenue = await this.viewService.adminGetAbandonSeller(adminId, { from: timeFrom, to: timeTo })
            await Promise.map(listRevenue, async (report) => {
                const { sellerId, revenue, productCost } = report
                const [seller] = await Promise.all([
                    this.userService.findUserById(sellerId)
                ])
                report.group = seller.email
                report.revenue = this.orderService.roundTwoDigit(revenue)
                report.productCost = this.orderService.roundTwoDigit(productCost)
            })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listRevenue && listRevenue.length && listRevenue[0] ? listRevenue : []
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetAnalyticsProduct(adminId: string, { from, to, timezone }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).tz("US/Arizona").startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).tz("US/Arizona").endOf('days').tz('Atlantic/Azores').toDate()
            const listRevenue = await this.viewService.adminRevenueProduct(adminId, timeFrom, timeTo)
            await Promise.map(listRevenue, async (report) => {
                const { productId, revenue, revenueCancel } = report
                const [product, imageUrl] = await Promise.all([
                    this.productService.findOneProduct({ _id: productId }),
                    this.productService.getImageUrlFromProducId(productId)
                ])
                const revenueTotal = revenue + revenueCancel
                report.imageUrl = imageUrl
                report.group = product.productName
                report.revenueTotal = this.orderService.roundTwoDigit(revenueTotal)
                report.revenue = this.orderService.roundTwoDigit(revenue)
            })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listRevenue && listRevenue.length && listRevenue[0] ? listRevenue : []
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetAbandonProduct(adminId: string, { from, to, timezone }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).tz("US/Arizona").startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).tz("US/Arizona").endOf('days').tz('Atlantic/Azores').toDate()
            const listRevenue = await this.viewService.adminGetAbandonProduct(adminId, timeFrom, timeTo)
            await Promise.map(listRevenue, async (report) => {
                const { productId, revenue } = report
                const [product, imageUrl] = await Promise.all([
                    this.productService.findOneProduct({ _id: productId }),
                    this.productService.getImageUrlFromProducId(productId)
                ])
                report.imageUrl = imageUrl
                report.group = product.productName
                report.revenue = this.orderService.roundTwoDigit(revenue)
            })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listRevenue && listRevenue.length && listRevenue[0] ? listRevenue : []
            }
        } catch (error) {
            throw error
        }
    }

    async sellerGetTotalStatsByTime(userId: string, { timezone, from, to, storeId }: NewViewQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
            const stats = await this.viewService.sellerGetTotalStatsByTime(userId, timeFrom, timeTo, storeId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: stats
            }
        } catch (error) {
            throw error
        }
    }

    async sellerGetAnalyticsDomain(userId: string, query: NewDateQuery) {
        try {
            try {
                const listProduct = await this.orderService.sellerGetAnalyticsDomain(userId, query)
                return {
                    message: BaseNoti.GLOBAL.SUCCESS,
                    data: listProduct
                }
            } catch (error) {
                throw error
            }
        } catch (error) {
            throw error
        }
    }

    async sellerGetAnalyticsRecent(userId: string, query: NewDateQuery) {
        try {
            try {
                const listProduct = await this.orderService.sellerGetAnalyticsRecent(userId, query)
                return {
                    message: BaseNoti.GLOBAL.SUCCESS,
                    data: listProduct
                }
            } catch (error) {
                throw error
            }
        } catch (error) {
            throw error
        }
    }

    async sellerGetAnalyticsRealtime(userId: string, query: NewDateQuery) {
        try {
            try {
                let { page, limit } = query
                if (!page) page = 1
                if (!limit) limit = 25

                const listProduct = await this.orderService.sellerGetAnalyticsRealtime(userId, query)
                const totalCount = listProduct.length
                const documents = listProduct.slice((page - 1) * limit, page * limit)
                const totalInList = documents.length
                return {
                    message: BaseNoti.GLOBAL.SUCCESS,
                    data: {
                        page: page,
                        total: totalCount,
                        totalInList,
                        data: documents
                    }
                }
            } catch (error) {
                throw error
            }
        } catch (error) {
            throw error
        }
    }

    async sellerGetRevenueDomain(userId: string, query: NewDateQuery) {
        try {
            const data = await this.orderService.sellerGetRevenueDomain(userId, query)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data
            }
        } catch (error) {
            throw error
        }
    }

    async sellerGetRevenueSeller(userId: string, query: NewDateQuery) {
        try {
            let { from, to, timezone } = query
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
            const { productCost, total, purchase, totalCancel, view, addToCart, checkout, goCheckout } = await this.viewService.sellerGetRevenueSeller(userId, timeFrom, timeTo)
            const revenue = total - totalCancel
            let aov = purchase == 0 ? 0 : revenue / purchase
            let pcre = revenue == 0 ? 0 : productCost / revenue
            aov = Math.round(aov * 100) / 100
            pcre = Math.round(pcre * 100) / 100
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: [{
                    view, addToCart, checkout,
                    revenue,
                    goCheckout,
                    revenueTotal: total,
                    productCost,
                    purchase,
                    aov,
                    'pc/re': pcre
                }]
            }
        } catch (error) {
            throw error
        }
    }

    async sellerGetRevenueProduct(userId: string, query: NewDateQuery) {
        try {
            const data = await this.orderService.sellerGetRevenueProduct(userId, query)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data
            }
        } catch (error) {
            throw error
        }
    }

    async sellerGetRevenueHour(userId: string, query: NewDateQuery) {
        try {
            const data = await this.orderService.sellerGetRevenueHour(userId, query)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data
            }
        } catch (error) {
            throw error
        }
    }

    async sellerGetRevenueDate(userId: string, query: NewDateQuery) {
        try {
            let { from, to, timezone } = query
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).tz("US/Arizona").startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).tz("US/Arizona").endOf('days').tz('Atlantic/Azores').toDate()
            const listRevenue = await this.viewService.sellerRevenueDate(userId, { from: timeFrom, to: timeTo })
            const result = listRevenue.map((product) => {
                const { purchase, revenue, productCost, revenueCancel, date, view } = product
                product.revenueTotal = this.orderService.roundTwoDigit(revenue + revenueCancel)
                product.revenue = this.orderService.roundTwoDigit(revenue)
                const aov = purchase == 0 ? 0 : revenue / purchase
                const pcre = revenue == 0 ? 0 : productCost / revenue
                return {
                    ...product,
                    view: view | 0,
                    group: date,
                    productCost: Math.round(productCost * 100) / 100,
                    purchase: Math.round(purchase * 100) / 100,
                    aov: Math.round(aov * 100) / 100,
                    'pc/re': Math.round(pcre * 100) / 100
                }
            })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: result && result.length && result[0] ? result : []
            }

        } catch (error) {
            throw error
        }
    }

    async sellerGetRevenueMonth(userId: string, query: NewDateQuery) {
        try {
            let { from, to, timezone } = query
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).tz("US/Arizona").startOf('months').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).tz("US/Arizona").endOf('months').tz('Atlantic/Azores').toDate()
            const listRevenue = await this.viewService.sellerRevenueMonth(userId, { from: timeFrom, to: timeTo })
            await Promise.map(listRevenue, async (report) => {
                const { month, revenueCancel, createdAt } = report
                const revenueTotal = report.revenue + revenueCancel
                report.revenueTotal = revenueTotal
                report.month = month
            })
            const result = listRevenue.map((product) => {
                const { purchase, revenue, productCost, revenueTotal, month, view, addToCart, checkout, goCheckout } = product
                const aov = purchase == 0 ? 0 : revenue / purchase
                const pcre = revenue == 0 ? 0 : productCost / revenue
                return {
                    group: month,
                    view: view | 0,
                    addToCart, checkout, goCheckout,
                    productCost: Math.round(productCost * 100) / 100,
                    revenueTotal: Math.round(revenueTotal * 100) / 100,
                    revenue: Math.round(revenue * 100) / 100,
                    purchase: Math.round(purchase * 100) / 100,
                    aov: Math.round(aov * 100) / 100,
                    'pc/re': Math.round(pcre * 100) / 100
                }
            })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: result && result.length && result[0] ? result : []
            }

        } catch (error) {
            throw error
        }
    }

    async sellerUtm(userId: string, query: SellerUtmQuery) {
        try {
            const resultArray = await this.viewService.analyticUtm(query, userId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: resultArray
            }
        } catch (error) {
            throw error
        }
    }

}
