import { Controller, Get, Param, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { StatsService } from './stats.service';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { SellerGuard } from 'src/base/guard/seller.guard';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { NewDateQuery, NewViewQuery, SellerUtmQuery } from './stats.dto';

@Controller('stats-seller')
@ApiTags('Stats Seller')
@ApiBearerAuth()
@UseGuards(SellerGuard)
@UseGuards(AuthGuard('jwt'))
export class StatsSellerController {
    constructor(
        private readonly statsService: StatsService
    ) { }

    @Get('by-time')
    sellerGetTotalStatsByTime(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: NewViewQuery) {
        return this.statsService.sellerGetTotalStatsByTime(userId, query)
    }

    @Get('analytics-domain')
    sellerGetAnalyticsDomain(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: NewDateQuery) {
        return this.statsService.sellerGetAnalyticsDomain(userId, query)
    }

    @Get('analytics-recent')
    sellerGetAnalyticsRecent(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: NewDateQuery) {
        return this.statsService.sellerGetAnalyticsRecent(userId, query)
    }

    @Get('analytics-realtime')
    sellerGetAnalyticsRealtime(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: NewDateQuery) {
        return this.statsService.sellerGetAnalyticsRealtime(userId, query)
    }

    @Get('analytics-utm')
    sellerGetAnalyticsUtm(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: SellerUtmQuery) {
        return this.statsService.sellerUtm(userId, query)
    }

    @Get('revenue/domain')
    sellerGetRevenueDomain(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: NewDateQuery) {
        return this.statsService.sellerGetRevenueDomain(userId, query)
    }

    @Get('revenue/date')
    sellerGetRevenueDate(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: NewDateQuery) {
        return this.statsService.sellerGetRevenueDate(userId, query)
    }

    @Get('revenue/month')
    sellerGetRevenueMonth(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: NewDateQuery) {
        return this.statsService.sellerGetRevenueMonth(userId, query)
    }

    @Get('revenue/seller')
    sellerGetRevenueSeller(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: NewDateQuery) {
        return this.statsService.sellerGetRevenueSeller(userId, query)
    }

    @Get('revenue/product')
    sellerGetRevenueProduct(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: NewDateQuery) {
        return this.statsService.sellerGetRevenueProduct(userId, query)
    }

    @Get('revenue/hour')
    sellerGetRevenueHour(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: NewDateQuery) {
        return this.statsService.sellerGetRevenueHour(userId, query)
    }
}
