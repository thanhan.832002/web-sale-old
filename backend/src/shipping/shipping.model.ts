import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger"
import { Transform, Type } from "class-transformer"
import { ArrayMinSize, IsArray, IsBoolean, IsInt, IsNotEmpty, IsNumber, IsOptional, Min, Validate, ValidateNested } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsObjectId } from "src/base/custom-validator"

const ShippingPriceSchema: Schema = new Schema({
    name: String,
    price: Number,
    quantity: Number
})

export const ShippingSchema: Schema = new Schema({
    name: String,
    desc: String,
    listPrice: [ShippingPriceSchema],
    listCountry: [String],
    isRestOfWorld: {
        type: Boolean,
        index: true,
        default: false
    },
    adminId: {
        type: Types.ObjectId,
        index: true,
        ref: 'User'
    }
}, { ...schemaOptions, collection: 'Shipping' })

export class ShippingPriceDto {
    @ApiProperty()
    @IsNotEmpty()
    name: string

    @ApiProperty()
    @Min(0)
    @Type(() => Number)
    @IsNotEmpty()
    price: number

    @ApiProperty()
    @Min(1)
    @IsInt()
    @Type(() => Number)
    @IsNotEmpty()
    quantity: number
}

export class Shipping extends Document {
    @ApiProperty()
    @IsNotEmpty()
    name: string

    @ApiProperty()
    @IsOptional()
    desc: string

    @ApiProperty({ type: [ShippingPriceDto] })
    @ValidateNested()
    @Type(() => ShippingPriceDto)
    @ArrayMinSize(1)
    @IsArray()
    @IsNotEmpty()
    listPrice: ShippingPriceDto[]

    @ApiProperty()
    @IsArray()
    @IsOptional()
    listCountry: string[]

    @ApiProperty()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsNotEmpty()
    isRestOfWorld: boolean

    @ApiProperty()
    @IsNotEmpty()
    @Validate(IsObjectId)
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    adminId: string
}
