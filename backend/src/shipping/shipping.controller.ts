import { Controller, Get, Post, Put, Delete, Param, Body, Query, UseGuards, Ip } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { BuyerShippingQuery, CreateShippingDto, EditShippingDto, ListShippingQuery } from './shipping,dto';
import { ShippingService } from './shipping.service';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';

@Controller('shipping')
@ApiTags('Shipping')
export class ShippingController {
    constructor(
        private readonly shippingService: ShippingService) { }

    @Get('ip')
    getIpInfo(
        @Ip() ip: string
    ) {
        return this.shippingService.getIpInfo(ip)
    }


    @Get('list-shipping')
    getListShipping(
        @Query() query: BuyerShippingQuery
    ) {
        return this.shippingService.getListShipping(query)
    }

    @ApiBearerAuth()
    @UseGuards(AdminGuard)
    @UseGuards(AuthGuard('jwt'))
    @Get('shipping-detail/:shippingId')
    getOneShipping(@Param('shippingId') shippingId: string) {
        return this.shippingService.getOneShipping(shippingId)
    }

    @ApiBearerAuth()
    @UseGuards(AdminGuard)
    @UseGuards(AuthGuard('jwt'))
    @Get('all-shipping')
    adminGetListShipping(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: ListShippingQuery) {
        return this.shippingService.adminGetlistShipping(userId, query)
    }

    @ApiBearerAuth()
    @UseGuards(AdminGuard)
    @UseGuards(AuthGuard('jwt'))
    @Post('create')
    createShip(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() createShippingDto: CreateShippingDto) {
        return this.shippingService.newShipping(userId, createShippingDto)
    }

    @ApiBearerAuth()
    @UseGuards(AdminGuard)
    @UseGuards(AuthGuard('jwt'))
    @Put('edit/:shippingId')
    editShipping(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('shippingId') shippingId: string,
        @Body() editShippingDto: EditShippingDto) {
        return this.shippingService.editShipping(userId, shippingId, editShippingDto)
    }

    @ApiBearerAuth()
    @UseGuards(AdminGuard)
    @UseGuards(AuthGuard('jwt'))
    @Delete('delete/:shippingId')
    deleteShipping(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('shippingId') shippingId: string) {
        return this.shippingService.deleteShipping(userId, shippingId)
    }
}
