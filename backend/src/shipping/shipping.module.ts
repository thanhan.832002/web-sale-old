import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ShippingController } from './shipping.controller';
import { Shipping, ShippingSchema } from './shipping.model';
import { ShippingService } from './shipping.service';
import { SharedModule } from 'src/base/shared.module';
import { StoreShippingModule } from 'src/store-shipping/store-shipping.module';
import { StoreModule } from 'src/store/store.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Shipping.name, schema: ShippingSchema }]),
    SharedModule,
    StoreShippingModule,
    StoreModule
  ],
  controllers: [ShippingController],
  providers: [ShippingService],
  exports: [ShippingService]
})
export class ShippingModule { }
