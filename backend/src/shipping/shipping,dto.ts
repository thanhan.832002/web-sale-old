import { ApiProperty, ApiPropertyOptional, PickType } from "@nestjs/swagger";
import { ArrayMinSize, IsArray, IsBoolean, IsNotEmpty, IsOptional, Validate, ValidateNested } from "class-validator";
import { Shipping, ShippingPriceDto } from "./shipping.model";
import { Transform, Type } from "class-transformer";
import { BaseQuery } from "src/base/interfaces/base-query.interface";
import { IsObjectId } from "src/base/custom-validator";

export class CreateShippingDto extends PickType(Shipping, ['name', 'desc', 'listPrice', 'listCountry', 'isRestOfWorld']) { }

export class EditShippingDto {
    @ApiProperty()
    @IsOptional()
    name: string

    @ApiProperty()
    @IsOptional()
    desc: string

    @ApiProperty()
    @IsArray()
    @IsOptional()
    listCountry: string[]

    @ApiProperty({ type: [ShippingPriceDto] })
    @ValidateNested()
    @Type(() => ShippingPriceDto)
    @ArrayMinSize(1)
    @IsArray()
    @IsOptional()
    listPrice: ShippingPriceDto[]
}

export class ListShippingQuery extends BaseQuery {
    @ApiPropertyOptional()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsOptional()
    isRestOfWorld: boolean
}

export class BuyerShippingQuery {
    @ApiProperty()
    @IsNotEmpty()
    domain: string

    @ApiPropertyOptional()
    @IsOptional()
    country: string

    @ApiProperty()
    @IsNotEmpty()
    @Validate(IsObjectId)
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    id: string
}