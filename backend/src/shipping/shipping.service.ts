import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BaseService } from 'src/base/base.service';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { DataFilter } from 'src/base/interfaces/data.interface';
import BaseNoti from 'src/base/notify';
import { Promise } from "bluebird";
import { BuyerShippingQuery, CreateShippingDto, EditShippingDto, ListShippingQuery } from './shipping,dto';
import { Shipping, ShippingPriceDto } from './shipping.model';
import { Timeout } from '@nestjs/schedule';
import { CacheService } from 'src/base/caching/cache.service';
import { KeyRedisEnum } from 'src/view/view.enum';
import { StoreShippingService } from 'src/store-shipping/store-shipping.service';
import { StoreService } from 'src/store/store.service';
const axios = require('axios')

@Injectable()
export class ShippingService extends BaseService<Shipping>{
    constructor(
        @InjectModel(Shipping.name) private readonly shippingModel: Model<Shipping>,
        private readonly storeShippingService: StoreShippingService,
        private readonly storeService: StoreService,
        private readonly cacheService: CacheService
    ) { super(shippingModel) }

    async newShipping(adminId: string, createShippingDto: CreateShippingDto) {
        try {
            let { listCountry, listPrice, isRestOfWorld } = createShippingDto
            if ((listCountry && listCountry.length && isRestOfWorld) || ((!listCountry || !listCountry.length) && !isRestOfWorld)) {
                throw new BaseApiException({
                    message: BaseNoti.SHIPPING.INVALID_INPUT
                })
            }
            if (isRestOfWorld) {
                await this.checkIsRestOfWorldAlreadyExists()
            } else {
                await this.checkIsRestOfWorldAlreadyExistsNot()
            }

            if (listCountry && listCountry.length) await this.checkCountryAlreadyExists(listCountry)
            listPrice = this.checkValidListPrice(listPrice)
            const newShip = new this.shippingModel({ ...createShippingDto, adminId })
            await newShip.save()
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: newShip
            }
        } catch (error) {
            throw error
        }
    }

    async getOneShipping(shippingId: string) {
        try {
            // const cachedResponse = await this.cacheService.get({ key: `${KeyRedisEnum.shippingdetail}-${shippingId}` })
            // if (cachedResponse) return JSON.parse(cachedResponse)
            const foundShip = await this.shippingModel.findById(shippingId)
            if (!foundShip) {
                throw new BaseApiException({ message: BaseNoti.SHIPPING.SHIPPING_NOT_EXISTS })
            }
            const response = {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: foundShip
            }
            // await this.cacheService.set({ key: `${KeyRedisEnum.shippingdetail}-${shippingId}`, value: JSON.stringify(response), expiresIn: 24 * 60 * 60 })
            return response
        } catch (error) {

        }
    }

    checkValidListPrice(listPrice: ShippingPriceDto[]) {
        try {
            const foundOneProductPrice = listPrice.find(priceConfig => priceConfig.quantity == 1)
            if (!foundOneProductPrice) {
                throw new BaseApiException({
                    message: BaseNoti.SHIPPING.ONE_PRODUCT_PRICE_NOT_EXISTS
                })
            }
            let listQuantity = listPrice.map(price => price.quantity)
            listQuantity = [...new Set(listQuantity)]
            if (listQuantity.length != listPrice.length) {
                throw new BaseApiException({
                    message: BaseNoti.SHIPPING.INVALID_INPUT
                })
            }
            return listPrice.sort((a, b) => a.quantity - b.quantity)
        } catch (error) {
            throw error
        }
    }

    async checkIsRestOfWorldAlreadyExists() {
        try {
            const foundShipping = await this.shippingModel.exists({ isRestOfWorld: true })
            if (foundShipping) {
                throw new BaseApiException({
                    message: BaseNoti.SHIPPING.SHIPPING_EXISTS
                })
            }
        } catch (error) {
            throw error
        }
    }

    async checkIsRestOfWorldAlreadyExistsNot() {
        try {
            const foundShipping = await this.shippingModel.exists({ isRestOfWorld: true })
            if (!foundShipping) {
                throw new BaseApiException({
                    message: BaseNoti.SHIPPING.RESTOFWORLD_NOT_EXISTS
                })
            }
        } catch (error) {
            throw error
        }
    }

    async checkCountryAlreadyExists(listCountry: string[]) {
        try {
            await Promise.map(listCountry, async (country) => {
                const foundShipping = await this.shippingModel.exists({ listCountry: country })
                if (foundShipping) {
                    throw new BaseApiException({
                        message: BaseNoti.SHIPPING.COUNTRY_EXISTS
                    })
                }
            }, { concurrency: 25 })
        } catch (error) {
            throw error
        }
    }

    async editShipping(adminId: string, shippingId: string, editShippingDto: EditShippingDto) {
        try {
            let { name, desc, listPrice, listCountry } = editShippingDto
            const foundShip: Shipping = await this.shippingModel.findOne({ _id: shippingId, adminId })
            if (!foundShip) {
                throw new BaseApiException({ message: BaseNoti.SHIPPING.SHIPPING_NOT_EXISTS })
            }
            const updateFields: any = {}
            if (!foundShip.isRestOfWorld && listCountry && listCountry.length) {
                await this.checkCountryAlreadyExistsWhenEdit(listCountry, foundShip.id)
                updateFields.listCountry = listCountry
            }
            if (name) updateFields.name = name
            if (desc) updateFields.desc = desc
            if (listPrice) {
                updateFields.listPrice = this.checkValidListPrice(listPrice)
            }
            const updatedShipping = await this.shippingModel.findByIdAndUpdate(shippingId, updateFields, { new: true })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: updatedShipping
            }
        } catch (error) {
            throw error
        }
    }

    async checkCountryAlreadyExistsWhenEdit(listCountry: string[], shippingId: string) {
        try {
            await Promise.map(listCountry, async (country) => {
                const foundShipping = await this.shippingModel.exists({ listCountry: country, _id: { $ne: shippingId } })
                if (foundShipping) {
                    throw new BaseApiException({
                        message: BaseNoti.SHIPPING.COUNTRY_EXISTS
                    })
                }
            }, { concurrency: 25 })
        } catch (error) {
            throw error
        }
    }

    async deleteShipping(adminId: string, shippingId: string) {
        try {
            const foundShip = await this.shippingModel.findOneAndDelete({ _id: shippingId, adminId })
            if (!foundShip) {
                throw new BaseApiException({ message: BaseNoti.SHIPPING.SHIPPING_NOT_EXISTS })
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async getIpInfo(ip: string) {
        try {
            const url = `http://ip-api.com/json/${ip}?fields=status,query,country,countryCode`
            const { data } = await axios(url)
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data
            }
        } catch (error) {
            let message = error?.response?.data?.message || `Fail to fetch data from http://ip-api.com/json/${ip}?fields=status,query,country,countryCode`
            let status = error?.response?.status
            throw new BaseApiException({
                status,
                message
            })
        }
    }

    async getListShipping({ domain, country, id }: BuyerShippingQuery) {
        try {
            const cachedResponse = await this.cacheService.get({ key: `${KeyRedisEnum.listshipping}-${domain}-${country}` })
            if (cachedResponse) return JSON.parse(cachedResponse)
            let response
            const foundStore = await this.storeService.findStoreBuyer(domain)
            const foundStoreShipping = await this.storeShippingService.getShippingStore(foundStore._id)
            if (foundStoreShipping) {
                const listShip = foundStoreShipping.listPrice.map(price => ({ ...price, name: foundStoreShipping.name || '' }))
                response = {
                    message: BaseNoti.GLOBAL.SUCCESS,
                    data: listShip
                }
            } else {
                let foundShip = country ? await this.shippingModel.findOne({ adminId: id, listCountry: country, listPrice: { $exists: true } }).lean() : null
                if (!foundShip) {
                    foundShip = await this.shippingModel.findOne({ adminId: id, isRestOfWorld: true, listPrice: { $exists: true } }).lean()
                }
                const listShip = foundShip.listPrice.map(price => ({ ...price, name: foundShip.name || '' }))
                response = {
                    message: BaseNoti.GLOBAL.SUCCESS,
                    data: listShip
                }
            }
            return response
        } catch (error) {
            throw error
        }
    }

    async adminGetlistShipping(adminId: string, query: ListShippingQuery) {
        try {
            let { page, limit, search, filter, sort, isRestOfWorld } = query;
            let filterObject: DataFilter<Partial<Shipping>> = {
                limit,
                page,
                sort: { quantity: 1 },
                condition: { listPrice: { $exists: true }, adminId },
                population: [],
            };
            if (filter) {
                filterObject.condition = { ...filterObject.condition, ...filter };
            }
            if (isRestOfWorld !== undefined) {
                filterObject.condition = { ...filterObject.condition, isRestOfWorld };
            }
            if (search) {
                filterObject.condition['$or'] = [
                    { name: { $regex: search, $options: 'i' } },
                    { desc: { $regex: search, $options: 'i' } },
                ];
            }
            const listShip = await this.getListDocument(filterObject);
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listShip,
            };
        } catch (error) {
            throw error
        }
    }

    // async getShippingPrice(adminId: string, quantity: number, country?: string) {
    //     try {
    //         let foundShip = await this.shippingModel.findOne({ adminId, listPrice: { $exists: true }, listCountry: country })
    //         if (!foundShip) {
    //             foundShip = await this.shippingModel.findOne({ adminId, listPrice: { $exists: true }, isRestOfWorld: true })
    //         }
    //         let { listPrice } = foundShip
    //         const foundPrice = listPrice.sort((a, b) => { return b.quantity - a.quantity }).find(price => price.quantity <= quantity)
    //         return foundPrice.price || 0
    //     } catch (error) {
    //         throw error
    //     }
    // }

    async getShippingPriceNew(storeId: string, adminId: string, quantity: number, total: number, country?: string) {
        try {
            let foundShip = await this.storeShippingService.getShippingStore(storeId)
            if (!foundShip) {
                foundShip = country ? await this.shippingModel.findOne({ adminId, listPrice: { $exists: true }, listCountry: country }) : null
                if (!foundShip) {
                    foundShip = await this.shippingModel.findOne({ adminId, listPrice: { $exists: true }, isRestOfWorld: true })
                }
            }
            let { listPrice } = foundShip
            let price = 0
            const priceQuantity = listPrice.filter(price => price.quantity).sort((a, b) => { return b.quantity - a.quantity }).find(price => price.quantity <= quantity)
            const priceTotal = listPrice.filter(price => price.total).sort((a, b) => { return b.total - a.total }).find(price => price.total <= total)
            if (priceTotal?.price || priceTotal?.price == 0) price = priceTotal.price
            else if (priceQuantity?.price || priceQuantity?.price == 0) price = priceQuantity.price
            return price
        } catch (error) {
            throw error
        }
    }

    async createDefaultROW(adminId: string) {
        try {
            const foundShip = await this.shippingModel.findOne({ adminId })
            if (foundShip) return
            const newShipping = new this.shippingModel({
                adminId,
                name: 'REST OF WORLD',
                desc: 'Rest of world',
                isRestOfWorld: true,
                listPrice: {
                    name: 'Standard',
                    price: 0,
                    quantity: 1
                }
            })
            await newShipping.save()
            return newShipping
        } catch (error) {
            throw error
        }
    }

}
