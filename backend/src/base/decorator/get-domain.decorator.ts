import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { BaseApiException } from "../exception/base-api.exception";
import BaseNoti from "../notify";

export const GetDomain = createParamDecorator(
    (data: unknown, ctx: ExecutionContext) => {
        const request = ctx.switchToHttp().getRequest();
        let strDomain = request?.headers?.referer
        if (strDomain && strDomain.slice(-1) !== '/') strDomain += '/'
        let domain = getBw(strDomain, '://', '/')
        if (!domain) {
            // throw new BaseApiException({
            //     message: BaseNoti.DOMAIN.DOMAIN_IS_EMPTY
            // })
        }
        return domain;
    },
);
const getBw = (str: string, from: string, to: string) => {
    try {
        const splited = str.split(from)
        if (splited && splited.length && splited[1]) {
            str = splited[1]
            const splitedSecond = str.split(to)
            if (splitedSecond && splitedSecond.length) {
                return splitedSecond[0]
            }
        }
        return ''
    } catch (error) {
        return ''
    }
}