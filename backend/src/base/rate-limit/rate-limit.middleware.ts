import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as redis from 'redis';
import { RateLimiterRedis, RateLimiterMemory } from 'rate-limiter-flexible';
import { decode } from 'jsonwebtoken';

const rateLimiterMemory = new RateLimiterMemory({
    points: 1,
    duration: 1,
});
const rateLimiterRedis = new RateLimiterRedis({
    storeClient: redis.createClient({
        enable_offline_queue: false,
        host: 'localhost',
        port: 6379,
        auth_pass: 'bestbuyredispassword',
    }),
    points: 1,
    duration: 1,
    inmemoryBlockOnConsumed: 1,
    inmemoryBlockDuration: 1,
    insuranceLimiter: rateLimiterMemory
});
@Injectable()
export class RateLimitCustomMidleWare implements NestMiddleware {
    async use(req: Request, res: Response, next: NextFunction) {
        try {
            const { params } = req
            const key = await this.getKey(req)
            await rateLimiterRedis.consume(key, 1)
            next();
        } catch (error) {
            res.status(429).send({ message: 'TOO_MANY_REQUEST' });
        }
    }
    async getKey(req: Request) {
        const path = req.path.split('/').join('_')
        const key = req.ip;
        return `userID-${key}-${path}`
    }
}