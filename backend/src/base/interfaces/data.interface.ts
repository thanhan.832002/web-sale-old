
export class PaginationData<T>{

    page: number

    total: number

    totalInList: number

    data: T[]
}
export class DataFilter<T> {
    condition?: Partial<T> | Object
    limit?: number = 25
    page?: number = 1
    selectCols?: string
    sort?: any
    population?: PopulationQuery[]
}

export class PopulationQuery {
    path: string
    select?: string
    populate?: PopulationQuery[]
}