import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger"
import { Transform, Type } from "class-transformer"
import { IsNotEmpty, IsNumber, IsObject, IsOptional } from "class-validator"
import { UtmQuery } from "src/product-buyer/product-buyer.query"

export class BaseQuery {
    @ApiPropertyOptional()
    @IsOptional()
    @IsObject()
    filter?: any

    @ApiPropertyOptional()
    @IsOptional()
    @IsObject()
    sort?: object

    @ApiPropertyOptional()
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    limit?: number

    @ApiPropertyOptional()
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    page?: number

    @ApiPropertyOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsOptional()
    search?: string

    @ApiPropertyOptional()
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    dateFrom?: number

    @ApiPropertyOptional()
    @IsOptional()
    @IsNumber()
    @Type(() => Number)
    dateTo?: number
}
export class ViewUtmQuery extends UtmQuery {
    @ApiProperty()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsNotEmpty()
    sessionId: string
}

export class SlugQuery extends ViewUtmQuery {
    @ApiPropertyOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsOptional()
    slug: string
}

export class CouponQuery {
    @ApiPropertyOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsOptional()
    slug: string
}
export class DomainQuery extends ViewUtmQuery {
    @ApiPropertyOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    @IsOptional()
    domain: string
}