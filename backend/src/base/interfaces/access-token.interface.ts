
export class Token {
    readonly token: string
    readonly userId: string
    readonly sessionId: string
}