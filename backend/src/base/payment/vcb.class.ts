const axios = require('axios')
const request = require('request-promise')
const moment = require('moment-timezone')

const forge = require('node-forge');
const userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36";
const LOGIN_URL = "https://digiapp.vietcombank.com.vn/authen-service/v1/login";
const TRANS_HIS_URL = "https://digiapp.vietcombank.com.vn/bank-service/v1/transaction-history";


const publicKey = "LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUFpa3FRckl6WkprVXZIaXNqZnU1WkNOK1RMeS8vNDNDSWM1aEpFNzA5VElLM0hiY0M5dnVjMitQUEV0STZwZVNVR3FPbkZvWU93bDNpOHJSZFNhSzE3RzJSWk4wMU1JcVJJSi82YWM5SDRMMTFkdGZRdFI3S0hxRjdLRDBmajZ2VTRrYjUrMGN3UjNSdW1CdkRlTWxCT2FZRXBLd3VFWTlFR3F5OWJjYjVFaE5HYnh4TmZiVWFvZ3V0VndHNUMxZUtZSXR6YVlkNnRhbzNncTdzd05IN3A2VWRsdHJDcHhTd0ZFdmM3ZG91RTJzS3JQRHA4MDdaRzJkRnNsS3h4bVI0V0hESFdmSDBPcHpyQjVLS1dRTnl6WHhUQlhlbHFyV1pFQ0xSeXBOcTdQKzFDeWZnVFNkUTM1ZmRPN00xTW5pU0JUMVYzM0xkaFhvNzMvOXFENWU1VlFJREFRQUIKLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0t";
const clientPubKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCrt+fXUlvz4Cee1NdosfjzJeL9x/TpTzSIwqvFbefME0pcdIWJB9L9uzVYqFCDUJUij8a0yhxSrONVt5QeQWMUVE+HdhoF0ylVW6B9xs5Yw2NEP6tVgqvFVfHp4Z+IVwXfRWhuePRtrwu6NNalJhgpRDnwqL1EU8P3l2TMQ1Gg/QIDAQAB";
const clientPrivateKey = `-----BEGIN RSA PRIVATE KEY-----
MIICWwIBAAKBgQCrt+fXUlvz4Cee1NdosfjzJeL9x/TpTzSIwqvFbefME0pcdIWJ
B9L9uzVYqFCDUJUij8a0yhxSrONVt5QeQWMUVE+HdhoF0ylVW6B9xs5Yw2NEP6tV
gqvFVfHp4Z+IVwXfRWhuePRtrwu6NNalJhgpRDnwqL1EU8P3l2TMQ1Gg/QIDAQAB
AoGAcqSjakQYE+exS/85mTJIzLycoWYgAqvYnP2rBHJt8PdoC/XXOTc6DCAya3Bj
4GeqSZrHxOeypRe75vxbLR/mbw4HwzDonfejuzZLaXuAiiF6WBduxMoZJ7EAHvF0
W45zAiu26zM92Axasd5yqhVJIcNz3xu3uIunmDai0Os9g8ECQQD4/GKSreSBlkxV
wt8e2QAaqAdpQKgbQgf2xh9QFew7dPPZNDHBrAIcsb1dTjUSwtDeRWp/XtRIsdB2
tQBlTwEFAkEAsI5KG5qRxXV7tkQ46letuCPOraGFt9b6JmIle2/cJZdaGwu4YSbR
G2AGRFJEYZ94UTvC4nOC1AldliGrRwUBmQJAFCmD5fLU7TV5ivxTyxCxdHck2n4U
3tgzAuW6RWbxEUOAN5eFa/R820v9VJ/vpkxGU6l1XUkVkalsgyBXHt0N7QJATY9U
cufmHa5MmHdD4X5+7COeyZvpWCdyYKZA/QbY2RKyHKt7ZgJdXf9DAYR6UcByZI0y
QbSoxmkO2cMUsPA8EQJAe+AyBdczvG4fQzECZxCsl7qhMuRr4EN8gsJ4MFheA/hI
3E5+Zuwse+dvBorEKy2cCxFhNoHlq36kCBYQT34tEA==
-----END RSA PRIVATE KEY-----`;

export class VCB {
    username: string;
    password: string;
    bankNumber: string;
    captcha_id: string = 'b68806fc-dd40-a04d-c84f-e2b87c1688a9'
    captcha_result: number = 0;
    cookie: string = '';
    userInfo: object = {}
    clientIp = '';
    accessKey = '';
    sessionId = '';

    constructor(username: string, password: string, bankNumber: string) {
        this.username = username
        this.password = password
        this.bankNumber = bankNumber
    }

    // async listTransaction() {
    //     for (let i = 0; i < 5; i++) {
    //         try {
    //             await this.decodeCapchaLoop()
    //             await this.login()
    //             const trans = await this.getTransHis()
    //             return trans
    //         } catch (error) {
    //             // console.log('error :>> ', error);
    //             await this.wait(5000)
    //         } finally { }
    //     }

    // }



    // decodeCapchaLoop() {
    //     return new Promise(async (resolve, reject) => {
    //         for (let i = 0; i < 10; i++) {
    //             const captcha_result = await this.decodeCapcha()
    //             if (captcha_result) {
    //                 this.captcha_result = captcha_result
    //                 resolve(true)
    //             }
    //         }
    //         reject('captcha error')
    //     });

    // }

    async start() {
        try {
            const captcha_result = await this.decodeCapcha()
            this.captcha_result = captcha_result
            await this.login()
            const trans = await this.getTransHis()
            return trans
        } catch (error) {
            throw error
        }
    }

    async decodeCapcha() {
        try {
            const url = 'https://digiapp.vietcombank.com.vn/utility-service/v1/captcha/' + this.captcha_id
            let { data } = await axios({
                url,
                method: 'GET',
                responseType: 'arraybuffer',
            })
            data = Buffer.from(data).toString('base64');
            const urlDeocdeCaptcha = `https://solve.proxyv6.net/predict`
            const axiosOption = {
                method: 'post',
                url: urlDeocdeCaptcha,
                data: {
                    base64img: `data:image/jpeg;base64,${data}`
                },
            }
            const response = await axios(axiosOption)
            console.log('response.data :>> ', response.data);
            if (response.data) {
                return response.data
            }
            return 0
        } catch (error) {
            console.log('error :>> ', error);
            return 0
        }
    }
    async login() {
        let loginData = JSON.stringify(this.encryptRequest({
            "clientPubKey": clientPubKey,
            "DT": "Windows",
            "PM": "Chrome 104.0.0.0",
            "OV": "10",
            "lang": "vi",
            "user": this.username,
            "password": this.password,
            "captchaToken": this.captcha_id,
            "captchaValue": this.captcha_result,
            "checkAcctPkg": "1",
            "mid": 6
        }));
        let options = {
            'url': LOGIN_URL,
            method: "POST",
            headers: {
                "accept": "application/json",
                "content-type": "application/json",
                "Sec-Fetch-Dest": "empty",
                "Sec-Fetch-Mode": "cors",
                "Sec-Fetch-Site": "same-site",
                "user-agent": userAgent,
                "Authorization": "Bearer null",
                "X-Channel": "Web",
                "X-Request-ID": `${(new Date()).getTime()}`
            },
            body: loginData,
            encoding: "utf8",

        };
        let json = await request(options);
        let res = JSON.parse(this.decryptResponse(JSON.parse(json)));
        if (!res.userInfo) {
            throw new Error("LOGIN FAILED");
        }
        this.userInfo = res.userInfo;
        this.accessKey = res.accessKey;
        this.sessionId = res.sessionId;
        this.clientIp = res.clientIp;
    }

    async getTransHis() {
        try {
            let fromDate = moment().tz("Asia/Ho_Chi_Minh").subtract(1, "d").format("DD/MM/YYYY");
            let toDate = moment().tz("Asia/Ho_Chi_Minh").format("DD/MM/YYYY");
            const body = JSON.stringify(this.encryptRequest({
                "DT": "Windows",
                "PM": "Chrome 104.0.0.0",
                "OV": "10",
                "lang": "vi",
                "accountNo": this.bankNumber,
                "accountType": "D",
                "fromDate": fromDate,
                "toDate": toDate,
                "pageIndex": 0,
                "lengthInPage": 999999,
                "stmtDate": "",
                "stmtType": "",
                "mid": 14,
                "cif": this.userInfo['cif'],
                "user": this.username,
                "mobileId": this.userInfo['cusId'],
                "clientId": this.userInfo['clientId'],
                "sessionId": this.sessionId
            }));
            let options = {
                'url': TRANS_HIS_URL,
                method: "POST",
                headers: {
                    "accept": "application/json",
                    "content-type": "application/json",
                    "Sec-Fetch-Dest": "empty",
                    "Sec-Fetch-Mode": "cors",
                    "Sec-Fetch-Site": "same-site",
                    "user-agent": userAgent,
                    "Authorization": "Bearer null",
                    "X-Channel": "Web",
                    "X-Request-ID": `${(new Date()).getTime()}`
                },
                body,
                encoding: "utf8",

            };
            let json = await request(options);
            let res = JSON.parse(this.decryptResponse(JSON.parse(json)));
            if (res.code != "00") {
                throw new Error("getTransHis Error");
            }
            const data = res.transactions
            return data;
        } catch (e) {
        }
        return [];
    }
    encryptRequest(e) {
        let s = forge;
        const n = s.random.getBytesSync(32)
            , t = s.random.getBytesSync(16);
        e = Object.assign({
            clientPubKey: clientPubKey
        }, e);
        const i = s.cipher.createCipher("AES-CTR", n);
        i.start({
            iv: t
        }),
            i.update(s.util.createBuffer(s.util.encodeUtf8(JSON.stringify(e)))),
            i.finish();
        const r = Buffer.concat([Buffer.from(t, "binary"), Buffer.from(i.output.data, "binary")])
            , l = s.pki.publicKeyFromPem(s.util.decode64(publicKey)).encrypt(s.util.encode64(n));
        return {
            d: r.toString("base64"),
            k: s.util.encode64(l)
        }
    }
    decryptResponse(e) {
        let s = forge;
        const { k: n, d: t } = e
            , i = s.pki.privateKeyFromPem(clientPrivateKey)
            , r = s.util.decodeUtf8(i.decrypt(s.util.decode64(n)))
            , l = Buffer.from(t, "base64")
            , o = l.slice(0, 16)
            , u = l.slice(16)
            , c = s.cipher.createDecipher("AES-CTR", Buffer.from(r, "base64").toString("binary"));
        return c.start({
            iv: o.toString("binary")
        }),
            c.update(s.util.createBuffer(u)),
            c.finish(),
            s.util.decodeUtf8(c.output.data)
    }

    wait = (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms))
    }
}
