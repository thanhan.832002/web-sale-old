import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ResponseInterceptors implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        var that = this
        return next.handle().pipe(
            map(objectResponse => that.getResponseMessage(objectResponse, context))
        );
    }
    getResponseMessage(objectResponse: object, context: ExecutionContext) {
        const request = context.switchToHttp().getRequest<Request>();
        if (request.method === 'POST') {
            context.switchToHttp()
                .getResponse()
                .status(200);
        }
        return objectResponse
    }
}