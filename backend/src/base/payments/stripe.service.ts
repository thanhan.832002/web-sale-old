import { Icard } from "src/order/order-buyer.dto";
import Stripe from "stripe";
import axios from 'axios';
export class StripeService {
    stripe: Stripe
    secretKey: string;
    constructor(secretKey: string) {
        //@ts-ignore
        this.secretKey = secretKey
    }
    async fetchInternDetail(paymentIntentID: string): Promise<Partial<Stripe.PaymentIntent>> {
        try {
            let config = {
                method: 'get',
                maxBodyLength: Infinity,
                url: `https://api.stripe.com/v1/payment_intents/${paymentIntentID}`,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': `Bearer ${this.secretKey}`
                }
            };
            const { data } = await axios(config)
            return data
        } catch (error) {
            throw error
        }
    }
    async comfirmPayment(paymentIntentID: string): Promise<Partial<Stripe.PaymentIntent>> {
        try {
            let config = {
                method: 'post',
                maxBodyLength: Infinity,
                url: `https://api.stripe.com/v1/payment_intents/${paymentIntentID}/confirm`,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': `Bearer ${this.secretKey}`
                },
                data: {
                    // payment_method: 'pm_card_visa'
                }
            };
            const { data } = await axios(config)
            return data
        } catch (error) {
            throw error
        }
    }
    async updatePayment(paymentIntentID: string, description: string): Promise<Partial<Stripe.PaymentIntent>> {
        try {
            let config = {
                method: 'post',
                maxBodyLength: Infinity,
                url: `https://api.stripe.com/v1/payment_intents/${paymentIntentID}`,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': `Bearer ${this.secretKey}`
                },
                data: {
                    description
                }
            };
            const { data } = await axios(config)
            return data
        } catch (error) {
            throw error
        }
    }

    async fetchCharge(chargeId: string) {
        try {
            const stripeSd = require('stripe')(this.secretKey)
            const charge = await stripeSd.charges.retrieve(
                chargeId
            );
            return charge
        } catch (error) {
            throw error
        }
    }

}