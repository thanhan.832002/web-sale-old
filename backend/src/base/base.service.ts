import { Model, Document, Types } from "mongoose";
import { PopulationQuery, DataFilter, PaginationData } from "./interfaces/data.interface";

export class BaseService<T> {
    private _model: Model<Document>;
    constructor(schemaModel) {
        this._model = schemaModel;
    }
    async findOne({ filter, population, selectCols }: { filter: Partial<T> | Object; population?: PopulationQuery[]; selectCols?: string[] }): Promise<T> {
        try {
            let query = this._model.findOne(filter)
            if (population) {
                query.populate(population)
            }
            if (selectCols && selectCols.length) {
                query.select(selectCols)
            }
            // @ts-ignore
            return await query
        } catch (error) {
            throw error
        }
    }
    async getListDocument(filter: DataFilter<T>): Promise<PaginationData<T>> {
        try {
            let { condition, page, limit, selectCols, population, sort } = filter
            if (!sort || !Object.keys(sort).length) {
                sort = { createdAt: '-1' }
            }
            if (!limit) {
                limit = 25
            }
            if (!page) {
                page = 1
            }
            limit = limit != 0 ? limit : limit;
            let skip = (page != 0 && page != 1) ? (page - 1) * limit : page - 1;
            let totalCount = await this._model.countDocuments(condition)
            let query = this._model.find(condition)
            if (selectCols && selectCols.length) {
                query.select(selectCols)
            }
            if (population && population.length) {
                query.populate(population)
            }
            let documents = await query.sort(sort).skip(skip).limit(limit).select('-__v').exec()
            // @ts-ignore
            return this.createTableObject(page, documents, totalCount)
        } catch (error) {
            throw error
        }
    }

    createTableObject(page: number, documents: T[], totalCount: number): PaginationData<T> {

        var objectListData: PaginationData<T> = {
            page: page,
            total: totalCount,
            totalInList: documents.length,
            data: documents
        }
        return objectListData;
    }
}