import { CacheModule, Module } from '@nestjs/common';
import { BaseService } from './base.service';
import { CacheService } from './caching/cache.service';
import * as redisStore from 'cache-manager-redis-store';

@Module({
  providers: [CacheService, BaseService],
  imports: [
    CacheModule.registerAsync({
      useFactory: async () => ({
        store: redisStore,
        // host: '198.199.73.118',
        host: 'localhost',
        port: 6379,
        auth_pass: 'bestbuyredispassword',
      })
    }),
  ],
  exports: [CacheService, BaseService]
})
export class SharedModule { }