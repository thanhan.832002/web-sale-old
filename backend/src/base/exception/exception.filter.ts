import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common';
import { Response } from 'express';
import BaseNoti from '../notify';
import * as Sentry from '@sentry/node';
type DataResponse = {
    message: string;
    data?: object
}

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost) {
        console.log('exception :>> ', exception);
        const ctx = host.switchToHttp()
        const response = ctx.getResponse<Response>()
        let statusCode: number = 400
        let dataResponse: DataResponse = {
            message: "UNKNOW_ERROR"
        }
        if (exception instanceof HttpException) {
            statusCode = exception.getStatus()
            const { message, data } = (exception as any).response
            if (message) {
                dataResponse.message = message
            }
            if (data) dataResponse.data = data
            if (message && message == "Unauthorized") {
                dataResponse.message = BaseNoti.AUTH.TOKEN_EXPIRED
                statusCode = 403
            } else {
                dataResponse.message = message // Hoai test
            }
            if (statusCode) {
                if (message && message !== "Unauthorized") {
                    statusCode = statusCode
                }
            }
        }
        Sentry.captureException(exception);
        response.status(statusCode).json(dataResponse);
    }
}

