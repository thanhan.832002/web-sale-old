const qrcode = require('qrcode')
const { authenticator } = require('otplib')
export class TwofactorProcesser {
    username: string;
    secret: string
    serviceName?: string;

    constructor(username: string, secret: string) {
        this.serviceName = 'ZShop';
        this.username = username
        this.secret = secret
    }
    async qRCodeImage() {
        try {
            const otpAuth = this.generateOTPToken(this.username.split('@')[0], this.serviceName, this.secret)
            const qRCode = await this.generateQRCode(otpAuth)
            return qRCode
        } catch (error) {
            return ''
        }

    }

    generateOTPToken(username: string, serviceName: string, secret: string) {
        return authenticator.keyuri(username, serviceName, secret)
    }
    generateUniqueSecret() {
        return authenticator.generateSecret()
    }

    async generateQRCode(otpAuth) {
        try {
            const QRCodeImageUrl = await qrcode.toDataURL(otpAuth)
            return QRCodeImageUrl
        } catch (error) {
            return
        }
    }
}