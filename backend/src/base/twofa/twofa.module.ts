import { Module } from '@nestjs/common';
import { TwofactorProcesser } from './twofa.class';

@Module({
    exports: [TwofactorProcesser],
    providers: [TwofactorProcesser]
})
export class TwofaModule { }
