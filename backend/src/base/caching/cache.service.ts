import { Injectable, Inject, CACHE_MANAGER } from '@nestjs/common';
import { Cache } from 'cache-manager';
@Injectable()
export class CacheService {
    constructor(
        @Inject(CACHE_MANAGER) private cacheManager: Cache
    ) {
        // this.remove({key:"p:domain.com"})
    }

    async get({ key }): Promise<any> {
        const value = await this.cacheManager.get(key)
        return this.cacheManager.get(key)
    }

    async set({ key, value, expiresIn }) {
        //@ts-ignore
        return this.cacheManager.set(key, value, { ttl: expiresIn });
    }

    async setNoExpires({ key, value }) {
        return this.cacheManager.set(key, value);
    }

    remove({ key }): Promise<any> {
        return this.cacheManager.del(key)
    }
}