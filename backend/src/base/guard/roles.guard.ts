import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common"
import { Reflector } from "@nestjs/core"
import { Role } from "../enum/roles.enum"
import { BaseApiException } from "../exception/base-api.exception"
import BaseNoti from "../notify"

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(
        private readonly reflector: Reflector,
    ) { }

    async canActivate(context: ExecutionContext) {
        try {
            const roles = this.reflector.get<Role[]>('roles', context.getHandler())
            if (!roles) {
                return true
            }
            const request = context.switchToHttp().getRequest()
            const user = request.user
            const resultMatch = await this.matchRoles(roles, user.role)
            if (!resultMatch) {
                throw new BaseApiException({
                    message: BaseNoti.AUTH.AUTH_FORBIDDEN,
                    status: 409
                })
            }
            return resultMatch
        } catch (error) {
            throw error
        }

    }

    async matchRoles(roles: Role[], role: number) {
        try {
            return roles.includes(role)
        } catch (error) {
            return false
        }
    }
}