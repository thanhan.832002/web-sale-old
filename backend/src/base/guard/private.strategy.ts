import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request } from 'express';
import { ConfigService } from '@nestjs/config';
import { BaseApiException } from '../exception/base-api.exception';
import BaseNoti from '../notify';

@Injectable()
export class PrivateKeyGuard implements CanActivate {
    constructor(private configService: ConfigService) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const token = this.extractTokenFromHeader(request);
        const privateKey = this.configService.get('API_KEY')
        if (!token || token != privateKey) {
            throw new BaseApiException({
                status: 403,
                message: BaseNoti.AUTH.INVALID_KEY
            });
        }
        return true;
    }

    private extractTokenFromHeader(request: Request): string | undefined {
        const [type, token] = request.headers.authorization?.split(' ') ?? [];
        return type === 'Bearer' ? token : undefined;
    }
}