import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common"
import { ConfigService } from "@nestjs/config"
import { Role } from "../enum/roles.enum"
import { BaseApiException } from "../exception/base-api.exception"
import BaseNoti from "../notify"
export class KeyGuard implements CanActivate {
    constructor() { }

    async canActivate(context: ExecutionContext) {
        try {
            const keyApi = process.env.API_KEY
            const request = context.switchToHttp().getRequest()
            const header = request.headers
            const { secret } = header
            if (secret != keyApi) {
                throw new BaseApiException({
                    message: BaseNoti.AUTH.SECRET_NOT_VALID
                })
            }
            return true
        } catch (error) {
            throw error
        }

    }
}