import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common"
import { Role } from "../enum/roles.enum"
import { BaseApiException } from "../exception/base-api.exception"
import BaseNoti from "../notify"
export class SellerGuard implements CanActivate {
    constructor(
    ) { }

    async canActivate(context: ExecutionContext) {
        try {
            const request = context.switchToHttp().getRequest()
            const user = request.user
            if (!user || user.role != Role.seller) {
                throw new BaseApiException({
                    message: BaseNoti.USER.USER_NOT_SELLER,
                    status: 401
                })
            }
            return true
        } catch (error) {
            throw error
        }

    }
}