import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';
const ObjectId = require('mongoose').Types.ObjectId;

@ValidatorConstraint({ name: 'IsObjectId', async: false })
export class IsObjectId implements ValidatorConstraintInterface {
  validate(id: string, args: ValidationArguments) {
    if (ObjectId.isValid(id)) {
      if (String(new ObjectId(id)) === id) return true;
      return false;
    }
    return false;
  }
  defaultMessage(args: ValidationArguments) {
    return `${args.property} is not valid ObjectId`;
  }
}

@ValidatorConstraint({ name: 'IsListObjectId', async: false })
export class IsListObjectId implements ValidatorConstraintInterface {
  validate(listId: string[], args: ValidationArguments) {
    if (!listId) return false;
    for (let id of listId) {
      if (ObjectId.isValid(id)) {
        if (String(new ObjectId(id)) === id) continue;
        return false;
      }
      return false;
    }
    return true;
  }
  defaultMessage(args: ValidationArguments) {
    return `${args.property} is not valid list ObjectId`;
  }
}
