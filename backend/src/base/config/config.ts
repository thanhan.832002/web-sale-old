export default () => ({
    JWT_SECRET: process.env.JWT_SECRET,
    ACCESS_TOKEN_EXPIRES_IN: process.env.ACCESS_TOKEN_EXPIRES_IN,
    REFRESH_TOKEN_EXPIRES_IN: process.env.REFRESH_TOKEN_EXPIRES_IN,
    API_KEY: process.env.API_KEY,
    SENDGRID_API_KEY: process.env.SENDGRID_API_KEY,
    CSR: process.env.CSR,
    PRIVATE_KEY: process.env.PRIVATE_KEY
})