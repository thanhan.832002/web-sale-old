import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SellerGuard } from 'src/base/guard/seller.guard';
import { OrderService } from './order.service';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';
import { SellerOrderQuery } from './order.dto';

@Controller('order-seller')
@ApiTags('Order Seller')
@ApiBearerAuth()
@UseGuards(SellerGuard)
@UseGuards(AuthGuard('jwt'))
export class OrderSellerController {
    constructor(
        private readonly orderService: OrderService
    ) { }

    @Get('list')
    sellerGetListOrder(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: SellerOrderQuery) {
        return this.orderService.sellerGetListOrder(userId, query)
    }

    @Get('list-export')
    sellerGetListOrderExport(@GetUser() { userId }: JwtTokenDecrypted, @Query() query: SellerOrderQuery) {
        return this.orderService.sellerGetListOrderExport(userId, query)
    }
}
