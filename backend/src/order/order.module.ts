import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SelectedProductSellerModule } from 'src/selected-product-seller/selected-product-seller.module';
import { TrackingHistoryModule } from 'src/tracking-history/tracking-history.module';
import { UserPaymentModule } from 'src/seller-payment/seller-payment.module';
import { OrderController } from './order.controller';
import { Order, OrderSchema } from './order.model';
import { OrderService } from './order.service';
import { OrderAdminController } from './order-admin.controller';
import { StoreModule } from 'src/store/store.module';
import { UserModule } from 'src/user/user.module';
import { OrderSellerController } from './order-seller.controller';
import { AdminSettingModule } from 'src/admin-setting/admin-setting.module';
import { OrderBuyerService } from './order-buyer.service';
import { ProductSellerModule } from 'src/product-seller/product-seller.module';
import { VariantSellerModule } from 'src/variant-seller/variant-seller.module';
import { ShippingModule } from 'src/shipping/shipping.module';
import { CouponModule } from 'src/coupon/coupon.module';
import { PaymentModule } from 'src/payment-method/payment.module';
import { SendAbandonedModule } from 'src/do-send-abandoned/send-abandoned.module';
import { ViewModule } from 'src/view/view.module';
import { EmailModule } from 'src/email/email.module';
import { DomainModule } from 'src/domain/domain.module';
import { UtmModule } from 'src/utm/utm.module';
import { GalleryModule } from 'src/gallery/gallery.module';
import { ProductModule } from 'src/product/product.module';
import { RateLimitCustomMidleWare } from 'src/base/rate-limit/rate-limit.middleware';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Order.name, schema: OrderSchema }]),
    TrackingHistoryModule,
    UserPaymentModule,
    StoreModule,
    UserModule,
    AdminSettingModule,
    ProductSellerModule,
    VariantSellerModule,
    ShippingModule,
    StoreModule,
    SelectedProductSellerModule,
    CouponModule,
    UserPaymentModule,
    PaymentModule,
    SendAbandonedModule,
    ViewModule,
    EmailModule,
    DomainModule,
    ProductSellerModule,
    ProductModule,
    UtmModule,
    GalleryModule,
    UserModule
  ],
  controllers: [OrderController, OrderAdminController, OrderSellerController],
  providers: [OrderService, OrderBuyerService],
  exports: [OrderService, OrderBuyerService]
})
export class OrderModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(RateLimitCustomMidleWare).forRoutes(
        { path: '/order/paypal-checkout/:domain', method: RequestMethod.POST },
        { path: '/order/paypal-direct-checkout/:domain', method: RequestMethod.POST },
        { path: '/order/card-checkout/:domain', method: RequestMethod.POST },
        { path: '/order/card-comfirm-payment/:domain', method: RequestMethod.POST }
      )
  }
}
