import { InjectModel } from "@nestjs/mongoose";
import { Injectable } from "@nestjs/common";
import { Order } from "./order.model";
import { Model } from "mongoose";
import { CalculatePriceDto, CompletePaypalPaymentDto, CreatePaypalPaymentDto, CheckoutDto, PaypalDirectCheckoutDto, CardCheckoutDto, CardConfirmDto, OneItemDto } from "./order-buyer.dto";
import { SelectedProductSellerService } from "src/selected-product-seller/selected-product-seller.service";
import { ProductSellerService } from "src/product-seller/product-seller.service";
import { VariantSellerService } from "src/variant-seller/variant-seller.service";
import { UserPaymentService } from "src/seller-payment/seller-payment.service";
import { BaseApiException } from "src/base/exception/base-api.exception";
import { OrderStatusEnum, ShippingStatusEnum } from "./order.enum";
import { ShippingService } from "src/shipping/shipping.service";
import { PaymentService } from "src/payment-method/payment.service";
import { CouponService } from "src/coupon/coupon.service";
import { StoreService } from "src/store/store.service";
import { CouponTypeEnum, DiscountEnum } from "src/coupon/coupon.model";
import { PaymentType } from "src/payment-method/payment.enum";
import { OrderService } from "./order.service";
const paypal = require('paypal-rest-sdk');
import { CouponOrder, PaypalDto, StripeDto } from "./order.dto";
import BaseNoti from "src/base/notify";
import * as Promise from 'bluebird'
import * as moment from 'moment'
import { StripeService } from "src/base/payments/stripe.service";
import { SendAbandonedService } from "src/do-send-abandoned/send-abandoned.service";
import { StoreStatus } from "./../store/store.enum"
import { EmailService } from "src/email/email.service";
import { ViewService } from "src/view/view.service";
import Stripe from "stripe";
import { DomainService } from "src/domain/domain.service";
import { UtmService } from "src/utm/utm.service";
import { UtmQuery } from "src/product-buyer/product-buyer.query";
import { Store } from "src/store/store.model";
import { ProductService } from "src/product/product.service";
import { UserService } from "src/user/user.service";
import { ViewUtmQuery } from "src/base/interfaces/base-query.interface";
const axios = require('axios').default;
@Injectable()
export class OrderBuyerService {
    constructor(
        @InjectModel(Order.name) private readonly orderModel: Model<Order>,
        private readonly selectedProductService: SelectedProductSellerService,
        private readonly productSellerService: ProductSellerService,
        private readonly productService: ProductService,
        private readonly variantSellerService: VariantSellerService,
        private readonly sendAbandoedSesrvice: SendAbandonedService,
        private readonly userPaymentService: UserPaymentService,
        private readonly shippingService: ShippingService,
        private readonly paymentService: PaymentService,
        private readonly couponService: CouponService,
        private readonly domainService: DomainService,
        private readonly storeService: StoreService,
        private readonly orderService: OrderService,
        private readonly emailService: EmailService,
        private readonly viewService: ViewService,
        private readonly utmService: UtmService,
        private readonly userService: UserService
    ) { }

    async getMyOrder(domain: string, orderCode: string, email: string) {
        try {
            if (orderCode) orderCode = orderCode.trim()
            let foundOrder = null
            const listDomainNotAdmin = await this.domainService.findListDomain({ isAdmin: false })
            const index = listDomainNotAdmin.findIndex(domainAdmin => domainAdmin.domain == domain)
            if (index == -1) {
                const foundStore = await this.storeService.findStoreBuyer(domain)
                foundOrder = await this.orderModel
                    .findOne({ orderCode: orderCode, orderStatus: { $ne: OrderStatusEnum.draft }, storeId: foundStore._id }).select('-__v -oldTrackingNumber -searchName -sendMailConfirmed -sendMailFullfilled -isAbandon')
                    .populate([
                        {
                            path: 'listSelectedProductId', populate: [
                                { path: 'productSellerId', select: '-description -variantSellerId -comparePrice -sku -productCost -domain' },
                                // { path: 'variantSellerId' },
                            ]
                        }
                    ])
            } else {
                const foundDomain = listDomainNotAdmin[index]
                const listStore = await this.storeService.findListStore({ domainId: foundDomain._id })
                const listStoreId = listStore.map(store => store._id)
                foundOrder = await this.orderModel
                    .findOne({ orderCode: orderCode, orderStatus: { $ne: OrderStatusEnum.draft }, storeId: { $in: listStoreId } })
                    .populate([
                        {
                            path: 'listSelectedProductId', populate: [
                                { path: 'productSellerId', select: '-description' },
                                { path: 'variantSellerId' },
                            ]
                        }
                    ])
            }
            if (foundOrder) {
                const { shippingAddress } = foundOrder
                if (!shippingAddress.email || email != shippingAddress.email.toLowerCase()) {
                    throw new BaseApiException({
                        message: BaseNoti.ORDER.ORDER_NOT_VALID_EMAIL
                    })
                }
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: foundOrder || null
            }
        } catch (error) {
            throw error
        }
    }
    generateResponse = (intent: any) => {
        switch (intent.status) {
            case "requires_action":
                return {
                    requiresAction: true,
                    clientSecret: intent.client_secret,
                    paymentIntentID: intent.id,
                    success: false
                };
            case "requires_source_action":
                return {
                    requiresAction: true,
                    clientSecret: intent.client_secret,
                    paymentIntentID: intent.id,
                    success: false
                };
            case "requires_source":
                return {
                    error: "Your card was denied, please provide a new payment method",
                    success: false
                };
            case "succeeded":
                return {
                    requiresAction: false,
                    success: true
                };
        }
    }
    async cardCheckout(domain: string, cardCheckoutDto: CardCheckoutDto, utmQuery: ViewUtmQuery, ip: string, userAgent: string) {
        try {
            let { card, email, countryIp, paymentMethodID, isAbandon, billingAddress, cardHolderName } = cardCheckoutDto
            let { utm_campaign, utm_content, utm_medium, utm_source, sessionId } = utmQuery
            const utmCampaign = utm_campaign ? utm_campaign : ''
            const utmContent = utm_content ? utm_content : ''
            const utmMedium = utm_medium ? utm_medium : ''
            const utmSource = utm_source ? utm_source : ''
            const foundStore = await this.storeService.findStore({ domain, status: StoreStatus.active })
            const { sellerId, productSellerId, adminId } = foundStore
            let foundUtm = await this.utmService.findOne({ utmCampaign, utmContent, utmMedium, utmSource, adminId })
            if (!foundUtm) {
                foundUtm = await this.utmService.createUtm({ utmCampaign, utmContent, utmMedium, utmSource })
            }
            this.viewService.countCheckout(adminId, foundStore._id, 1, foundUtm._id, sessionId, ip)
            const foundPaymentStripe = await this.userPaymentService.findOne({ filter: { type: PaymentType.stripe, sellerId, status: true } })
            if (!foundPaymentStripe) {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.STRIPE_NOT_AVAILABLE
                })
            }
            const dataCalculatePrice = await this.calculatePrice(foundStore._id, adminId, { ...cardCheckoutDto, productSellerId, domain, sellerId }, countryIp)
            const productId = await this.productSellerService.findProductIdByProductSellerId(productSellerId)
            const { total, totalCompare, totalCost, totalPrice, discount, shipping, totalQuantity, listSelected, tip, foundCoupon } = dataCalculatePrice
            if (foundCoupon && foundCoupon.type && foundCoupon.type == CouponTypeEnum.abandoned) {
                isAbandon = true
            }
            const [listSelectedProductIns, orderCode] = await Promise.all([
                this.selectedProductService.createListSelectedProduct(listSelected),
                this.genarateOrderCode(adminId)
            ])
            let coupon: CouponOrder
            if (foundCoupon) {
                coupon = {
                    code: foundCoupon.code,
                    discount,
                    discountType: foundCoupon.discountType
                }
            }
            const listSelectedProductId = listSelectedProductIns.map(selectedProduct => selectedProduct._id)
            const newOrderDto: Partial<Order> = {
                isAbandon,
                listSelectedProductId,
                storeId: foundStore._id,
                sellerId,
                paymentId: foundPaymentStripe.paymentId,
                orderCode,
                subTotal: Math.round(totalPrice * 100) / 100,
                shipping,
                total: total + tip,
                tip,
                discount,
                coupon,
                totalCost,
                utmId: foundUtm._id,
                totalQuantity,
                productId,
                shippingAddress: { ...cardCheckoutDto },
                orderStatus: OrderStatusEnum.draft,
                billingAddress,
                //@ts-ignore
                stripe: {
                    paymentMethodID,
                    cardHolderName
                },
                processingDate: new Date(),
                searchName: cardCheckoutDto && cardCheckoutDto.lastName && cardCheckoutDto.firstName ? `${cardCheckoutDto.firstName} ${cardCheckoutDto.lastName}${cardCheckoutDto.lastName} ${cardCheckoutDto.firstName}` : undefined,
                fraudAnalysis: {
                    ip,
                    userAgent
                }
            }
            if (newOrderDto?.shippingAddress?.email) {
                newOrderDto.shippingAddress.email = newOrderDto.shippingAddress.email.toLowerCase()
            }
            const newOrder = new this.orderModel(newOrderDto)
            await Promise.all([
                newOrder.save(),
                this.selectedProductService.updateMany({ _id: { $in: listSelectedProductId } }, { orderId: newOrder._id }),
                this.sendAbandoedSesrvice.createSendAbandoned(email, newOrder.storeId, listSelectedProductId)
            ])
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: newOrder
            }
        } catch (error) {
            throw error
        }
    }

    async cardComfirmPayment(domain: string, cardCheckoutDto: CardConfirmDto, ip: string, userAgent: string) {
        try {
            let { paymentIntentID, paymentMethodID, orderCode } = cardCheckoutDto
            const foundStore = await this.storeService.findStore({ domain, status: StoreStatus.active })
            const { sellerId } = foundStore
            const foundPaymentStripe = await this.userPaymentService.findOne({ filter: { type: PaymentType.stripe, sellerId, status: true } })
            if (!foundPaymentStripe) {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.STRIPE_NOT_AVAILABLE
                })
            }
            const foundOrder = await this.orderModel.findOneAndUpdate({ orderCode }, { 'stripe.id': paymentIntentID }).populate({ path: 'sellerId', select: 'adminId' })
            if (!foundOrder) {
                throw new BaseApiException({
                    message: BaseNoti.ORDER.ORDER_NOT_EXISTS
                })
            }
            if (foundOrder.orderStatus !== OrderStatusEnum.draft) {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.PAYMENT_ALREADY_EXISTS
                })
            }
            const config = await this.getConfigStripe(foundPaymentStripe.paymentId);
            const payment = new StripeService(config.secretKey);
            const description = `orderID ${foundOrder.orderCode}`
            const { total, totalCost, totalQuantity, utmId, shippingAddress, tip } = foundOrder
            try {
                let stripeResult = await payment.fetchInternDetail(paymentIntentID)
                let { status } = stripeResult
                if (status !== 'succeeded') {
                    if (stripeResult.last_payment_error?.code) {
                        let messageErr = stripeResult.last_payment_error?.message
                        if (stripeResult.last_payment_error?.code == 'payment_intent_authentication_failure') {
                            messageErr = 'Failed authentication'
                        }
                        throw new BaseApiException({
                            message: messageErr
                        })
                    }
                    stripeResult = await payment.comfirmPayment(paymentIntentID)
                    status = stripeResult.status
                }
                if (status !== 'succeeded') {
                    throw new BaseApiException({ message: BaseNoti.PAYMENT.PAYMENT_NOT_VALID })
                }
                const chargeId = typeof stripeResult.latest_charge != 'string' ? stripeResult.latest_charge.id : stripeResult.latest_charge
                const [charge, updatePay] = await Promise.all([
                    payment.fetchCharge(chargeId),
                    payment.updatePayment(paymentIntentID, description)
                ])
                const { outcome } = charge
                //@ts-ignore
                await this.orderService.adminAddTimeline(foundOrder.sellerId.adminId, foundOrder._id, { content: `Confirmation email was sent to: ${shippingAddress.email}`, time: moment().unix() })
                let sendMail = true
                try {
                    //@ts-ignore
                    await this.emailService.emailConfirmOrder(foundOrder.sellerId.adminId, foundOrder)
                    if (foundOrder.billingAddress) {
                        //@ts-ignore
                        await this.emailService.emailChangeShipping(foundOrder.sellerId.adminId, foundOrder)
                    }
                } catch (error) {
                    sendMail = false
                }
                await Promise.all([
                    this.orderModel.findByIdAndUpdate(foundOrder._id,
                        {
                            stripe: {
                                paymentMethodID: foundOrder.stripe?.paymentMethodID || stripeResult.payment_method,
                                cardHolderName: foundOrder.stripe.cardHolderName,
                                id: stripeResult.id,
                                amount: stripeResult.amount,
                                amount_captured: stripeResult.amount_received,
                                amount_refunded: 0,
                                application_fee: 0,
                                application_fee_amount: stripeResult.application_fee_amount,
                            },
                            shippingStatus: ShippingStatusEnum.paid,
                            orderStatus: OrderStatusEnum.processing,
                            processingDate: new Date(),
                            sendMailConfirmed: sendMail,
                            fraudAnalysis: {
                                ip,
                                userAgent,
                                riskLevel: outcome.risk_level,
                                riskScore: outcome.risk_score
                            }
                        }),
                    this.sendAbandoedSesrvice.deleteSendAbandoned(shippingAddress.email, foundOrder.storeId),
                    this.domainService.checkDomainSold(foundStore.domainId)
                ])
                //@ts-ignore
                const nothasView = await this.viewService.updateAnalyticOrder(foundOrder.isAbandon, foundOrder.createdAt, foundOrder.sellerId._id, foundOrder.sellerId.adminId, foundOrder.storeId, { total, cost: totalCost, quantity: totalQuantity, tip: tip | 0 }, utmId)
                if (nothasView) {
                    try {
                        const foundStore = await this.storeService.findOne({ filter: { _id: foundOrder.storeId }, selectCols: ['productSellerId'] })
                        await this.viewService.findByIdAndUpdate(nothasView._id, {
                            //@ts-ignore
                            adminId: foundOrder.sellerId.adminId,
                            //@ts-ignore
                            productId: foundOrder.productId,
                            productSellerId: foundStore.productSellerId
                        })
                    } catch (error) {
                    }
                }

                return {
                    message: BaseNoti.GLOBAL.SUCCESS,
                    data: {
                        orderCode: foundOrder.orderCode,
                        total,
                        email: shippingAddress.email,
                        phoneNumber: shippingAddress.phoneNumber
                    }
                }
            } catch (error) {
                if (error?.response?.data?.error?.payment_intent?.last_payment_error?.message) {
                    throw new BaseApiException({
                        message: error?.response?.data?.error?.payment_intent?.last_payment_error?.message
                    })
                }
                if (error?.response?.data?.message) {
                    throw new BaseApiException({
                        message: error.response.data.message
                    })
                }
                throw error
            }
        } catch (error) {
            throw error
        }
    }
    async paypalDirectCheckout(ip: string, userAgent: string, domain: string, { listSelectedProduct, utm_campaign, utm_content, utm_medium, utm_source, countryIp, isAbandon, sessionId }: PaypalDirectCheckoutDto) {
        try {
            if (!ip) {
                throw new BaseApiException({
                    message: BaseNoti.IP.NOT_DETECTED_IP
                })
            }
            listSelectedProduct = listSelectedProduct.filter(item => item.quantity)
            // const countryIpBe = await this.getCountryFromIp(ip)
            // if (countryIp != countryIpBe) {
            //     throw new BaseApiException({
            //         message: BaseNoti.IP.COUNTRY_NOT_MATCH_IP
            //     })
            // }
            const utmCampaign = utm_campaign ? utm_campaign : ''
            const utmContent = utm_content ? utm_content : ''
            const utmMedium = utm_medium ? utm_medium : ''
            const utmSource = utm_source ? utm_source : ''
            const foundStore = await this.storeService.findStore({ domain, status: StoreStatus.active })

            const { sellerId, productSellerId, adminId } = foundStore
            let foundUtm = await this.utmService.findOne({ utmCampaign, utmContent, utmMedium, utmSource })
            if (!foundUtm) {
                foundUtm = await this.utmService.createUtm({ utmCampaign, utmContent, utmMedium, utmSource })
            }
            const productId = await this.productSellerService.findProductIdByProductSellerId(productSellerId)
            this.viewService.countCheckout(adminId, foundStore._id, 1, foundUtm._id, sessionId, ip)
            this.viewService.countGoCheckout(adminId, foundStore._id, 1, { utm_campaign, utm_content, utm_medium, utm_source, sessionId }, ip)
            const foundPaymentPaypal = await this.userPaymentService.findOne({ filter: { type: PaymentType.paypal, sellerId, status: true } })
            if (!foundPaymentPaypal) {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.PAYPAL_NOT_AVAILABLE
                })
            }
            const dataCalculatePrice = await this.calculatePriceDirectPaypal(adminId, foundStore, { listSelectedProduct: listSelectedProduct, productSellerId, domain, sellerId }, countryIp)
            let { total, totalCompare, totalCost, totalPrice, discount, shipping, totalQuantity, listSelected, foundCoupon } = dataCalculatePrice
            if (foundCoupon && foundCoupon.type && foundCoupon.type == CouponTypeEnum.abandoned) {
                isAbandon = true
            }
            discount = Math.round(discount * 100) / 100

            total = Math.round((totalPrice - discount + shipping) * 100) / 100
            const [listSelectedProductIns, orderCode] = await Promise.all([
                this.selectedProductService.createListSelectedProduct(listSelected),
                this.genarateOrderCode(adminId)
            ])
            let coupon: CouponOrder
            if (foundCoupon) {
                coupon = {
                    code: foundCoupon.code,
                    discount,
                    discountType: foundCoupon.discountType
                }
            }
            const listSelectedProductId = listSelectedProductIns.map(selectedProduct => selectedProduct._id)
            const newOrderDto: Partial<Order> = {
                listSelectedProductId,
                storeId: foundStore._id,
                sellerId,
                paymentId: foundPaymentPaypal.paymentId,
                orderCode,
                subTotal: totalPrice,
                shipping,
                total,
                discount,
                coupon,
                utmId: foundUtm._id,
                totalCost,
                totalQuantity,
                productId,
                orderStatus: OrderStatusEnum.draft,
                isAbandon,
                fraudAnalysis: {
                    ip, userAgent
                }
            }
            const newOrder = new this.orderModel(newOrderDto)
            await Promise.all([
                newOrder.save(),
                this.selectedProductService.updateMany({ _id: { $in: listSelectedProductId } }, { orderId: newOrder._id }),
            ])
            return this.createPaypalOrder({
                order: newOrder,
                paymentMethodId: foundPaymentPaypal.paymentId.toString()
            })
        } catch (error) {
            throw error
        }
    }

    async paypalCheckout(ip: string, userAgent: string, domain: string, paypalCheckoutDto: CheckoutDto, utmQuery: ViewUtmQuery) {
        try {
            let { countryIp, email, isAbandon, billingAddress } = paypalCheckoutDto
            if (!paypalCheckoutDto.country) {
                if (!ip) {
                    throw new BaseApiException({
                        message: BaseNoti.IP.NOT_DETECTED_IP
                    })
                }
                // const countryIpBe = await this.getCountryFromIp(ip)
                // if (countryIp != countryIpBe) {
                //     throw new BaseApiException({
                //         message: BaseNoti.IP.COUNTRY_NOT_MATCH_IP
                //     })
                // }
            }
            let { utm_campaign, utm_content, utm_medium, utm_source, sessionId } = utmQuery
            console.log('utmQuery :>> ', utmQuery);
            const utmCampaign = utm_campaign ? utm_campaign : ''
            const utmContent = utm_content ? utm_content : ''
            const utmMedium = utm_medium ? utm_medium : ''
            const utmSource = utm_source ? utm_source : ''
            const foundStore = await this.storeService.findStore({ domain, status: StoreStatus.active })
            const { sellerId, productSellerId, adminId } = foundStore
            let foundUtm = await this.utmService.findOne({ utmCampaign, utmContent, utmMedium, utmSource })
            if (!foundUtm) {
                foundUtm = await this.utmService.createUtm({ utmCampaign, utmContent, utmMedium, utmSource })
            }
            const productId = await this.productSellerService.findProductIdByProductSellerId(productSellerId)
            this.viewService.countCheckout(adminId, foundStore._id, 1, foundUtm._id, sessionId, ip)
            const foundPaymentPaypal = await this.userPaymentService.findOne({ filter: { type: PaymentType.paypal, sellerId, status: true } })
            if (!foundPaymentPaypal) {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.PAYPAL_NOT_AVAILABLE
                })
            }
            const dataCalculatePrice = await this.calculatePrice(foundStore._id, adminId, { ...paypalCheckoutDto, productSellerId, domain, sellerId }, countryIp)
            const { total, totalCompare, totalCost, totalPrice, discount, shipping, totalQuantity, listSelected, tip, foundCoupon } = dataCalculatePrice
            if (foundCoupon && foundCoupon.type && foundCoupon.type == CouponTypeEnum.abandoned) {
                isAbandon = true
            }
            const [listSelectedProductIns, orderCode] = await Promise.all([
                this.selectedProductService.createListSelectedProduct(listSelected),
                this.genarateOrderCode(adminId)
            ])
            let coupon: CouponOrder
            if (foundCoupon) {
                coupon = {
                    code: foundCoupon.code,
                    discount,
                    discountType: foundCoupon.discountType
                }
            }
            const listSelectedProductId = listSelectedProductIns.map(selectedProduct => selectedProduct._id)
            const newOrderDto: Partial<Order> = {
                listSelectedProductId,
                storeId: foundStore._id,
                sellerId,
                paymentId: foundPaymentPaypal.paymentId,
                utmId: foundUtm._id,
                orderCode,
                subTotal: totalPrice,
                shipping,
                total: total + tip,
                tip,
                discount,
                coupon,
                totalCost,
                totalQuantity,
                productId,
                shippingAddress: { ...paypalCheckoutDto },
                billingAddress,
                orderStatus: OrderStatusEnum.draft,
                isAbandon,
                searchName: paypalCheckoutDto && paypalCheckoutDto.lastName && paypalCheckoutDto.firstName ? `${paypalCheckoutDto.firstName} ${paypalCheckoutDto.lastName}${paypalCheckoutDto.lastName} ${paypalCheckoutDto.firstName}` : undefined,
                fraudAnalysis: {
                    ip, userAgent
                }
            }
            if (newOrderDto?.shippingAddress?.email) {
                newOrderDto.shippingAddress.email = newOrderDto.shippingAddress.email.toLowerCase()
            }
            const newOrder = new this.orderModel(newOrderDto)
            await Promise.all([
                newOrder.save(),
                this.selectedProductService.updateMany({ _id: { $in: listSelectedProductId } }, { orderId: newOrder._id }),
            ])
            if (email) await this.sendAbandoedSesrvice.createSendAbandoned(email, newOrder.storeId, listSelectedProductId)
            return this.createPaypalOrder({
                order: newOrder,
                paymentMethodId: foundPaymentPaypal.paymentId
            })
        } catch (error) {
            throw error
        }
    }

    async calculatePrice(storeId: string, adminId: string, { listSelectedProduct, productSellerId, couponCode, domain, sellerId, tip }: CalculatePriceDto, country: string) {
        try {
            listSelectedProduct = listSelectedProduct.filter(item => item.quantity)
            let totalQuantity = 0
            let totalCompare = 0
            let totalPrice = 0
            let totalCost = 0
            let discount = 0
            const foundProductSeller = await this.productSellerService.findProductCheckout(productSellerId)
            const listSelected = await Promise.map(listSelectedProduct, async (selectedProduct: OneItemDto) => {
                const { variantSellerId, quantity } = selectedProduct
                const variant: any = await this.variantSellerService.findVariantCheckout(variantSellerId)
                const { price, comparePrice, productCost, imageId, listValue, sku, variantId, listOption } = variant
                totalCompare = comparePrice * quantity + totalCompare
                totalPrice = price * quantity + totalPrice
                totalCost = productCost * quantity + totalCost
                totalQuantity += quantity
                return {
                    domain,
                    productName: foundProductSeller.productName,
                    productSellerId,
                    variantSellerId,
                    variantId,
                    sku: sku,
                    imageUrl: imageId.imageUrl,
                    listValue: listValue,
                    quantity,
                    price,
                    comparePrice,
                    productCost,
                    totalCost: productCost * quantity,
                    listOption
                }
            }, { concurrency: 5 })
            let total = totalPrice
            let shipping = await this.shippingService.getShippingPriceNew(storeId, adminId, totalQuantity, total, country)
            const foundCoupon = couponCode ? await this.couponService.getCouponByCode(sellerId, couponCode, total, totalQuantity, shipping) : undefined
            if (foundCoupon) {
                if (foundCoupon.discountType == DiscountEnum.percent) {
                    discount = total * foundCoupon.discount / 100
                } else if (foundCoupon.discountType == DiscountEnum.value) {
                    discount = total - foundCoupon.discount < 0 ? total : foundCoupon.discount
                } else {
                    discount = shipping
                }
                total = total - discount
            }
            tip = tip || 0
            discount = Math.round(discount * 100) / 100
            total = Math.round((totalPrice - discount + shipping) * 100) / 100
            return { total, totalCompare, totalCost, totalPrice, shipping, listSelected, totalQuantity, discount, tip, foundCoupon }
        } catch (error) {
            throw error
        }
    }

    async calculatePriceDirectPaypal(adminId: string, store: Store, { listSelectedProduct, productSellerId, domain, sellerId }: CalculatePriceDto, country: string) {
        try {
            let totalQuantity = 0
            let totalCompare = 0
            let totalPrice = 0
            let totalCost = 0
            let discount = 0
            const foundProductSeller = await this.productSellerService.findProductCheckout(productSellerId)
            const listSelected = await Promise.map(listSelectedProduct, async (selectedProduct) => {
                const { variantSellerId, quantity } = selectedProduct
                const variant: any = await this.variantSellerService.findVariantCheckout(variantSellerId)
                const { price, comparePrice, productCost, imageId, listValue, sku, variantId, listOption } = variant
                totalCompare = comparePrice * quantity + totalCompare
                totalPrice = price * quantity + totalPrice
                totalCost = productCost * quantity + totalCost
                totalQuantity += quantity
                return {
                    domain,
                    productName: foundProductSeller.productName,
                    productSellerId,
                    variantSellerId,
                    variantId,
                    sku: sku,
                    imageUrl: imageId.imageUrl,
                    listValue: listValue,
                    quantity,
                    price,
                    comparePrice,
                    productCost,
                    totalCost: productCost * quantity,
                    listOption
                }
            })
            let total = totalPrice
            let shipping = await this.shippingService.getShippingPriceNew(store._id, adminId, totalQuantity, total, country)
            const foundCoupon = await this.couponService.getAutoApply(store, { total, quantity: totalQuantity, shipping })
            if (foundCoupon) {
                if (foundCoupon.discountType == DiscountEnum.percent) {
                    discount = total * foundCoupon.discount / 100
                } else if (foundCoupon.discountType == DiscountEnum.value) {
                    discount = total - foundCoupon.discount < 0 ? total : foundCoupon.discount
                } else {
                    discount = shipping
                }
                total = total - discount
            }
            discount = Math.round(discount * 100) / 100
            total = Math.round((totalPrice - discount + shipping) * 100) / 100
            return { total, totalCompare, totalCost, totalPrice, shipping, listSelected, totalQuantity, discount, foundCoupon }
        } catch (error) {
            throw error
        }
    }

    // async deleteOrderFail(orderCode: string) {
    //     try {
    //         const foundOrder: any = await this.orderModel.findOne({ orderCode, orderStatus: OrderStatusEnum.draft }).populate({ path: 'listSelectedProductId', select: 'variantSellerId' })
    //         if (!foundOrder) {

    //             return
    //         }
    //         const { listSelectedProductId, storeId } = foundOrder
    //         await Promise.all([
    //             this.orderModel.findByIdAndDelete(foundOrder._id),
    //             this.selectedProductService.deleteManySelectedProduct(listSelectedProductId),
    //             this.sendAbandoedSesrvice.createSendAbandoned(foundOrder?.shippingAddress?.email, storeId, foundOrder.listSelectedProductId[0].variantSellerId)
    //         ])
    //     } catch (error) {

    //     }
    // }

    async completePaypalPayment(completePaypalPaymentDto: CompletePaypalPaymentDto) {
        try {
            let total = 0
            let fee = 0
            let orderCode = ''
            let { paymentID, payerID, paymentMethodId } = completePaypalPaymentDto
            const payment: any = await this.exectutePayment(paymentID, payerID, paymentMethodId)
            if (payment && payment.state && payment.state == 'approved') {
                if (payment && payment.transactions && payment.transactions.length && payment.transactions[0].custom) {
                    orderCode = payment.transactions[0].custom.split('ordercode_')[1]
                    total = payment.transactions[0].amount.total
                    if (payment?.transactions[0]?.related_resources[0]?.sale?.transaction_fee?.value) {
                        fee = payment.transactions[0].related_resources[0].sale.transaction_fee.value
                    }
                } else {
                    throw new BaseApiException({
                        message: BaseNoti.GLOBAL.UNKNOW_ERROR
                    })
                }
                const order = await this.orderService.findOne({
                    filter: { orderCode }, population: [{
                        path: 'sellerId', select: 'adminId'
                    }]
                })
                const isNotValidPayment = await this.orderModel.findOne({ filter: { paypal: { $exists: true } }, 'paypal.paymentID': paymentID })
                if (isNotValidPayment) {
                    throw new BaseApiException({
                        message: BaseNoti.PAYMENT.PAYMENT_NOT_VALID
                    })
                }
                const { email, country_code, shipping_address, first_name, last_name, phone, billing_address } = payment?.payer?.payer_info
                let { _id, shippingAddress, storeId, sellerId } = order
                let billingAddress
                if (payment?.payer?.payer_info) {
                    shippingAddress = {
                        email: email,
                        firstName: first_name,
                        lastName: last_name,
                        address: shipping_address?.line1 || "",
                        zipCode: shipping_address?.postal_code || "",
                        city: shipping_address?.city || "",
                        state: shipping_address?.state || "",
                        country: country_code,
                        phoneNumber: phone
                    }
                    if (billing_address && Object.keys(billing_address).length) {
                        billingAddress = {
                            email: email,
                            firstName: first_name,
                            lastName: last_name,
                            address: billing_address?.line1 || "",
                            zipCode: billing_address?.postal_code || "",
                            city: billing_address?.city || "",
                            state: billing_address?.state || "",
                            country: country_code,
                            phoneNumber: phone
                        }
                    }
                }
                let paypalInfo: PaypalDto = {
                    paymentID,
                    payerID,
                    amount: Number(total),
                    fee: Number(fee),
                    remain: Math.round((total - fee) * 100) / 100,
                    transaction_id: payment?.transactions[0]?.related_resources[0]?.sale?.id || ''
                }
                const newOrder = await this.orderModel.findByIdAndUpdate(_id, {
                    paypal: paypalInfo,
                    shippingStatus: ShippingStatusEnum.paid,
                    orderStatus: OrderStatusEnum.processing,
                    processingDate: new Date(),
                    shippingAddress,
                    billingAddress,
                    searchName: `${shippingAddress.firstName} ${shippingAddress.lastName}${shippingAddress.lastName} ${shippingAddress.firstName}`,
                }, { new: true })
                const foundStore = await this.storeService.findOne({ filter: { _id: storeId }, selectCols: ['productSellerId', 'domainId'] })
                let sendMailConfirmed = true
                try {
                    //@ts-ignore
                    await this.emailService.emailConfirmOrder(order.sellerId.adminId, newOrder)
                    if (order.billingAddress) {
                        //@ts-ignore
                        await this.emailService.emailChangeShipping(order.sellerId.adminId, order)
                    }
                } catch (error) {
                    console.log('error :>> ', error);
                    sendMailConfirmed = false
                }
                await Promise.all([
                    this.orderModel.findByIdAndUpdate(_id, { sendMailConfirmed }),
                    //@ts-ignore
                    this.orderService.adminAddTimeline(order.sellerId.adminId, newOrder._id, { content: `Confirmation email was sent to: ${shippingAddress.email}`, time: moment().unix() }),
                    this.sendAbandoedSesrvice.deleteSendAbandoned(shippingAddress.email, storeId),
                    this.domainService.checkDomainSold(foundStore.domainId)
                ])
                //@ts-ignore
                const nothasView = await this.viewService.updateAnalyticOrder(order.isAbandon, order.createdAt, order.sellerId._id, order.sellerId.adminId, newOrder.storeId, { total: newOrder.total, cost: newOrder.totalCost, quantity: newOrder.totalQuantity, tip: newOrder.tip | 0 }, newOrder.utmId)
                if (nothasView) {
                    try {

                        this.viewService.findByIdAndUpdate(nothasView._id, {
                            //@ts-ignore
                            adminId: order.sellerId.adminId,
                            productId: order.productId,
                            productSellerId: foundStore.productSellerId
                        })
                    } catch (error) {

                    }
                }
                return {
                    message: BaseNoti.GLOBAL.SUCCESS,
                    data: {
                        orderCode,
                        email: shippingAddress.email,
                        phoneNumber: shippingAddress.phoneNumber,
                        total
                    }
                }
            } else {
                throw new BaseApiException({
                    message: BaseNoti.PAYMENT.PAYMENT_INVALID
                })
            }
        } catch (error) {
            throw error
        }
    }

    async createPaypalOrder({ order, paymentMethodId }: CreatePaypalPaymentDto) {
        try {
            const config = await this.getConfigPalpal(paymentMethodId)
            let { orderCode, total } = order
            total = Math.round(total * 100) / 100
            paypal.configure(config);
            const payment = {
                "intent": "sale",
                "payer": {
                    "payment_method": "paypal",
                    // payer_info: {
                    //     email: '',
                    //     billing_address: {
                    //         line1: '35 đường số 3',
                    //         line2: 'City Land Park Hill',
                    //         city: 'HCM',
                    //         country_code: 'VN',
                    //         postal_code: '70000',
                    //         state: '',
                    //         phone: '+84812512597'
                    //     }
                    // }
                },
                "redirect_urls": {
                    "return_url": "http://localhost:2000/cart/paypal-success",
                    "cancel_url": `https://api.opensitex.com/order/delete-order?orderCode=${orderCode}`
                },
                "transactions": [{
                    "amount": {
                        "total": total,
                        "currency": "USD"
                    },
                    "description": `Pay for order ${orderCode}`,
                    "custom": `ordercode_${orderCode}`
                }]
            }
            const transaction: any = await this.createPay(payment)
            // console.log('transaction :>> ', JSON.stringify(transaction));
            const approvalUrl = transaction.links.find(link => link.rel == 'approval_url')
            const href = approvalUrl.href
            const token = href.split('token=')[1]
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: {
                    id: token,
                    orderCode,
                    paymentMethodId
                }
            }
        } catch (error) {
            throw error
        }
    }

    createPay = (payment) => {
        return new Promise((resolve, reject) => {
            paypal.payment.create(payment, function (err, payment) {
                if (err) {
                    console.log('err :>> ', err);
                    reject(err);
                }
                else {
                    resolve(payment);
                }
            });
        });
    }

    exectutePayment(paymentID: string, payerID: string, paymentMethodId: string) {

        return new Promise(async (resolve, reject) => {
            try {
                const config = await this.getConfigPalpal(paymentMethodId)
                paypal.configure(config);
                const execute_payment_json = {
                    "payer_id": payerID,
                };
                paypal.payment.execute(paymentID, execute_payment_json, async (error, payment) => {
                    // console.log('error, payment :>> ', error, payment);
                    if (error) {
                        throw error;
                    } else {
                        resolve(payment)
                    }
                });
            } catch (error) {
                throw error
            }
        });
    }

    async getConfigPalpal(paymentId: string) {
        try {
            const { mode, publicKey, secretKey } = await this.paymentService.getConfigPayment(paymentId)
            return {
                'mode': mode,
                'client_id': publicKey,
                'client_secret': secretKey
            }

        } catch (error) {
            throw error
        }
    }

    async getConfigStripe(paymentId) {
        try {
            const { mode, publicKey, secretKey } = await this.paymentService.getConfigPayment(paymentId)
            return {
                secretKey,
                publicKey
            }
        } catch (error) {
            throw error
        }
    }

    async genarateOrderCode(adminId: string): Promise<String> {
        try {
            const prefix = await this.userService.getOrderPrefix(adminId)
            const now = moment().unix()
            let orderCode: string
            while (1) {
                orderCode = prefix ? `${prefix}_${now}_${this.randomString(10)}` : `${now}_${this.randomString(10)}`
                const isExists = await this.orderModel.exists({ orderCode })
                if (!isExists) {
                    return orderCode
                }
            }
        } catch (error) {
            throw error
        }
    }

    randomString = (length) => {
        const alphabet = '0123456789ABCDEFGHIJKLMNOPKRSTUVWXYZ'
        let code = '';
        for (let i = 0; i < length; i++) {
            let index = Math.floor(Math.random() * alphabet.length);
            let randomchar = alphabet[index]
            code = code + randomchar;
        }
        return code;
    };

}