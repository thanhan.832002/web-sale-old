import { Controller, Post, Body, Delete, Param, UseGuards, Query, Get, Ip } from '@nestjs/common';
import { OrderBuyerService } from './order-buyer.service';
import { CardCheckoutDto, CardConfirmDto, CheckoutDto, CompletePaypalPaymentDto, PaypalDirectCheckoutDto } from './order-buyer.dto';
import { ApiTags } from '@nestjs/swagger';
import { KeyGuard } from 'src/base/guard/key.guard';
import { GetDomain } from 'src/base/decorator/get-domain.decorator';
import { SlugQuery, ViewUtmQuery } from 'src/base/interfaces/base-query.interface';
import { UtmQuery } from 'src/product-buyer/product-buyer.query';
import { TrackingOrderQuery } from './order.dto';
import { UserAgent } from 'src/base/decorator/user-agent.decorator';

@ApiTags('Order')
@Controller('order')
export class OrderController {
    constructor(
        private readonly orderBuyerService: OrderBuyerService
    ) { }

    @Post('paypal-checkout/:domain')
    paypalCheckout(
        @Ip() ip: string,
        @UserAgent() userAgent: string,
        @Param('domain') domain: string,
        @Body() paypalCheckoutDto: CheckoutDto,
        @Query() utmQuery: ViewUtmQuery
    ) {
        return this.orderBuyerService.paypalCheckout(ip, userAgent, domain, paypalCheckoutDto, utmQuery)
    }

    @Post('paypal-direct-checkout/:domain')
    paypalDirectCheckout(
        @Ip() ip: string,
        @UserAgent() userAgent: string,
        @Param('domain') domain: string,
        @Body() paypalDirectCheckoutDto: PaypalDirectCheckoutDto) {
        return this.orderBuyerService.paypalDirectCheckout(ip, userAgent, domain, paypalDirectCheckoutDto)
    }

    @Post('paypal-complete-payment')
    completePaypalPayment(@Body() completePaypalPaymentDto: CompletePaypalPaymentDto) {
        return this.orderBuyerService.completePaypalPayment(completePaypalPaymentDto)
    }

    @Post('card-checkout/:domain')
    cardCheckout(
        @Param('domain') domain: string,
        @Body() cardCheckoutDto: CardCheckoutDto,
        @Ip() ip: string,
        @UserAgent() userAgent: string,
        @Query() utmQuery: ViewUtmQuery
    ) {
        return this.orderBuyerService.cardCheckout(domain, cardCheckoutDto, utmQuery, ip, userAgent)
    }
    @Post('card-comfirm-payment/:domain')
    cardComfirmPayment(
        @Param('domain') domain: string,
        @Body() cardCheckoutDto: CardConfirmDto,
        @Ip() ip: string,
        @UserAgent() userAgent: string
    ) {
        return this.orderBuyerService.cardComfirmPayment(domain, cardCheckoutDto, ip, userAgent)
    }
    @UseGuards(KeyGuard)
    @Get('/:orderCode')
    getMyOrder(
        @GetDomain() domain: string,
        @Param('orderCode') orderCode: string,
        @Query() { slug, email }: TrackingOrderQuery
    ) {
        if (slug) domain = slug + '.' + domain
        return this.orderBuyerService.getMyOrder(domain, orderCode, email)
    }

    @UseGuards(KeyGuard)
    @Delete('delete')
    deleteFailOrder(@Query() { orderCode }: any) {
        // return this.orderBuyerService.deleteOrderFail(orderCode)
    }
}
