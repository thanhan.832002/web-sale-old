import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdminGuard } from 'src/base/guard/admin.guard';
import { BaseQuery } from 'src/base/interfaces/base-query.interface';
import { TrackingHistoryService } from 'src/tracking-history/tracking-history.service';
import { AddTimelineDto, AddTrackingDto, ChanceTrackingDto, OrderAdminQuery, TimelineIdQuery, UpdateOrderDto } from './order.dto';
import { OrderService } from './order.service';
import { ChangeShipping } from './order.dto';
import { NewDateQuery } from 'src/stats/stats.dto';
import { GetUser } from 'src/base/decorator/get-user.decorator';
import { JwtTokenDecrypted } from 'src/auth/auth.dto';

@Controller('order-admin')
@ApiTags('Order Admin')
@ApiBearerAuth()
@UseGuards(AdminGuard)
@UseGuards(AuthGuard('jwt'))
export class OrderAdminController {
    constructor(
        private readonly orderService: OrderService,
        private readonly trackingHistoryService: TrackingHistoryService

    ) { }

    @Post('update')
    adminUpdateOrder(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() updateDto: UpdateOrderDto) {
        return this.orderService.adminUpdateOrder(userId, updateDto)
    }

    @Get('list')
    adminGetListOrder(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: OrderAdminQuery) {
        return this.orderService.adminGetListOrder(userId, query)
    }

    @Get('list-export')
    adminGetListExportOrder(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: OrderAdminQuery) {
        return this.orderService.adminGetListExportOrder(userId, query)
    }

    @Get('detail/:orderId')
    adminGetOrder(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('orderId') orderId: string) {
        return this.orderService.adminGetOrder(userId, orderId)
    }

    @Post('add-timeline/:orderId')
    adminAddTimeline(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('orderId') orderId: string,
        @Body() addTimelineDto: AddTimelineDto) {
        return this.orderService.adminAddTimeline(userId, orderId, addTimelineDto)
    }

    @Delete('delete-timeline/:orderId')
    adminDeleteTimeline(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('orderId') orderId: string,
        @Query() { timelineId }: TimelineIdQuery) {
        return this.orderService.adminDeleteTimeline(userId, orderId, timelineId)
    }

    @Post('add-tracking')
    adminAddTracking(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() addTrackingDto: AddTrackingDto) {
        return this.orderService.adminAddTracking(userId, addTrackingDto)
    }

    @Put('change-shipping/:orderId')
    adminChangeShipping(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Param('orderId') orderId: string,
        @Body() changeShippingDto: ChangeShipping
    ) {
        return this.orderService.adminChangeShipping(userId, orderId, changeShippingDto)
    }

    @Post('change-tracking')
    adminChangeTracking(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Body() changeTrackingDto: ChanceTrackingDto
    ) {
        return this.orderService.adminChangeTracking(userId, changeTrackingDto)
    }

    @Get('tracking-history')
    getHistoryUploadTracking(
        @GetUser() { userId }: JwtTokenDecrypted,
        @Query() query: BaseQuery) {
        return this.trackingHistoryService.getHistoryUploadTracking(userId, query)
    }

}
