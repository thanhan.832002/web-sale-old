export enum OrderStatusEnum {
  draft = 'draft',
  processing = 'processing',
  fulfilled = 'fulfilled',
  done = 'done',
  canceled = 'canceled',
}

export enum ShippingStatusEnum {
  paid = 1,
  available,
  online,
  'delivered&guarantee',
}
