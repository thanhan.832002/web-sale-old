import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsNotEmpty,
  IsObject,
  IsOptional,
  Validate,
} from 'class-validator';
import { Schema, Document, Types } from 'mongoose';
import { schemaOptions } from 'src/base/base.shema';
import { IsListObjectId, IsObjectId } from 'src/base/custom-validator';
import {
  CouponOrder,
  FraudAnalysis,
  PaypalDto,
  ShippingAddressDto,
  StripeDto,
  TimeLineDto,
} from './order.dto';
import { OrderStatusEnum } from './order.enum';
import { Transform } from 'class-transformer';
import { DiscountEnum } from 'src/coupon/coupon.model';

const TimeLineOrderSchema = new Schema(
  {
    content: String,
  },
  { ...schemaOptions },
);

const PaypalSchema = new Schema({
  paymentID: String,
  payerID: String,
  amount: Number,
  fee: Number,
  remain: Number,
  transaction_id: String,
});

const StripeSchema = new Schema({
  id: String,
  paymentMethodID: String,
  amount: Number,
  amount_captured: Number,
  amount_refunded: Number,
  application_fee: String,
  application_fee_amount: Number,
  cardHolderName: String,
});

const ShippingAddressSchema = new Schema({
  email: {
    type: String,
  },
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },

  address: {
    type: String,
  },
  apartment: {
    type: String,
  },
  companyName: {
    type: String,
  },
  zipCode: {
    type: String,
  },
  city: {
    type: String,
  },
  state: {
    type: String,
  },
  country: {
    type: String,
  },
  phoneNumber: {
    type: String,
  },
});

export const CouponOrderSchema: Schema = new Schema({
  code: String,
  discountType: {
    type: Number,
    enum: DiscountEnum,
  },
  discount: Number,
});

export const FraudAnalysisSchema: Schema = new Schema({
  riskLevel: String,
  riskScore: Number,
  ip: String,
  userAgent: String,
});

export const OrderSchema: Schema = new Schema(
  {
    listSelectedProductId: {
      type: [Types.ObjectId],
      ref: 'SelectedProductSeller',
    },
    storeId: {
      type: Types.ObjectId,
      ref: 'Store',
      index: true,
    },
    sellerId: {
      type: Types.ObjectId,
      ref: 'User',
      index: true,
    },
    productId: {
      type: Types.ObjectId,
      ref: 'User',
      index: true,
    },
    paymentId: {
      type: Types.ObjectId,
      indexe: true,
      ref: 'Payment',
    },
    utmId: {
      type: Types.ObjectId,
      ref: 'Utm',
    },
    orderCode: {
      type: String,
      index: true,
    },
    subTotal: Number,
    discount: Number,
    coupon: CouponOrderSchema,
    shipping: Number,
    total: Number,
    tip: { type: Number, default: 0 },
    totalCost: Number,
    totalQuantity: Number,
    timeLine: [TimeLineOrderSchema],
    processingDate: Date,
    fulfilledAt: Date,
    trackingNumber: String,
    oldTrackingNumber: [String],
    stripe: StripeSchema,
    paypal: PaypalSchema,
    shippingAddress: ShippingAddressSchema,
    billingAddress: ShippingAddressSchema,
    orderStatus: {
      type: String,
      index: true,
      enum: OrderStatusEnum,
    },
    searchName: {
      type: String,
    },
    sendMailConfirmed: {
      type: Boolean,
    },
    sendMailFullfilled: {
      type: Boolean,
    },
    isAbandon: Boolean,
    recheck: {
      type: Boolean,
      default: false,
    },
    fraudAnalysis: {
      type: FraudAnalysisSchema,
    },
  },
  { ...schemaOptions, collection: 'Order' },
);

export class Order extends Document {
  @ApiProperty()
  @Validate(IsListObjectId)
  @IsNotEmpty()
  listSelectedProductId: string[];

  @ApiProperty()
  @Validate(IsObjectId)
  @IsNotEmpty()
  storeId: string;

  @ApiProperty()
  @Validate(IsObjectId)
  @IsNotEmpty()
  sellerId: string;

  @ApiProperty()
  @Validate(IsObjectId)
  @IsNotEmpty()
  productId: string;

  @ApiProperty()
  @Validate(IsObjectId)
  @IsNotEmpty()
  paymentId: string;

  @ApiProperty()
  @Validate(IsObjectId)
  @IsNotEmpty()
  utmId: string;

  @ApiProperty()
  @IsNotEmpty()
  orderCode: string;

  @ApiProperty()
  @IsNotEmpty()
  subTotal: number;

  @ApiProperty()
  @IsNotEmpty()
  discount: number;

  @ApiProperty()
  @IsOptional()
  coupon: CouponOrder;

  @ApiProperty()
  @IsNotEmpty()
  shipping: number;

  @ApiProperty()
  @IsNotEmpty()
  total: number;

  @ApiProperty()
  @IsNotEmpty()
  totalCost: number;

  @ApiProperty()
  @IsNotEmpty()
  totalQuantity: number;

  @ApiProperty()
  @IsOptional()
  tip: number;

  @ApiProperty()
  @IsNotEmpty()
  timeLine: TimeLineDto[];

  @ApiProperty()
  @IsNotEmpty()
  processingDate: Date;

  @ApiProperty()
  @IsNotEmpty()
  fulfilledAt: Date;

  @ApiProperty()
  @IsNotEmpty()
  trackingNumber: string;

  @ApiProperty()
  @IsNotEmpty()
  oldTrackingNumber: string[];

  @ApiProperty()
  @IsNotEmpty()
  stripe: StripeDto;

  @ApiProperty()
  @IsNotEmpty()
  paypal: PaypalDto;

  @ApiProperty()
  @IsNotEmpty()
  shippingAddress: ShippingAddressDto;

  @ApiProperty()
  @IsOptional()
  billingAddress: ShippingAddressDto;

  @ApiProperty()
  @IsNotEmpty()
  orderStatus: OrderStatusEnum;

  searchName: string;

  createdAt: Date;

  @ApiProperty()
  @IsOptional()
  carrierCode: string;
  @ApiProperty()
  @IsBoolean()
  @Transform(
    ({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1,
  )
  @IsOptional()
  isAbandon: boolean;

  @ApiProperty()
  @IsBoolean()
  @Transform(
    ({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1,
  )
  @IsOptional()
  sendMailConfirmed: boolean;

  @ApiProperty()
  @IsBoolean()
  @Transform(
    ({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1,
  )
  @IsOptional()
  sendMailFullfilled: boolean;

  fraudAnalysis: FraudAnalysis;
}
