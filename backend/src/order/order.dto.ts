import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import {
  Matches,
  IsOptional,
  IsNotEmpty,
  IsArray,
  Validate,
  IsBoolean,
} from 'class-validator';
import { OrderStatusEnum, ShippingStatusEnum } from './order.enum';
import { IsObjectId } from 'src/base/custom-validator';
import { NewDateQuery } from 'src/stats/stats.dto';
import { DiscountEnum } from 'src/coupon/coupon.model';

export class TrackingOrderQuery {
  @ApiPropertyOptional()
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  @IsOptional()
  slug: string;

  @Matches(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  )
  @ApiProperty()
  @Transform(({ value }) => {
    if (value) return value.trim().toLowerCase();
  })
  @IsNotEmpty()
  email: string;
}
export class ShippingAddressDto {
  @Matches(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  )
  @ApiProperty()
  @Transform(({ value }) => {
    if (value) return value.trim().toLowerCase();
  })
  @IsOptional()
  email: string;

  @ApiPropertyOptional()
  @IsOptional()
  firstName?: string;

  @ApiPropertyOptional()
  @IsOptional()
  lastName: string;

  @ApiPropertyOptional()
  @IsOptional()
  address: string;

  @ApiPropertyOptional()
  @IsOptional()
  apartment?: string;

  @ApiPropertyOptional()
  @IsOptional()
  companyName?: string;

  @ApiPropertyOptional()
  @IsOptional()
  zipCode: string;

  @ApiPropertyOptional()
  @IsOptional()
  city: string;

  @ApiPropertyOptional()
  @IsOptional()
  state: string;

  @ApiPropertyOptional()
  @IsOptional()
  country: string;

  @ApiProperty()
  @IsOptional()
  phoneNumber: string;
}

export class CouponOrder {
  code: string;
  discountType: DiscountEnum;
  discount: Number;
}

export class TimelineIdQuery {
  @ApiProperty()
  @IsNotEmpty()
  timelineId: string;
}

export class AddTimelineDto {
  @ApiProperty()
  @IsNotEmpty()
  content: string;

  @ApiProperty()
  @IsNotEmpty()
  time: number;
}

export class FraudAnalysis {
  riskLevel?: string;
  riskScore?: number;
  ip: string;
  userAgent: string;
}

export class ChangeShipping {
  @ApiProperty({ type: ShippingAddressDto })
  @IsNotEmpty()
  shippingAddress: ShippingAddressDto;
}
export class UpdateOrderDto {
  @ApiProperty({ type: [String] })
  @IsNotEmpty()
  @IsArray()
  listOrderId: string[];

  @ApiPropertyOptional()
  @IsOptional()
  orderStatus: OrderStatusEnum;

  @ApiPropertyOptional()
  @IsOptional()
  shippingStatus: ShippingStatusEnum;
}
export class AddTracking {
  @ApiProperty()
  @IsNotEmpty()
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  orderCode: string;

  @ApiProperty()
  @IsNotEmpty()
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  trackingNumber: string;

  @ApiProperty()
  @IsNotEmpty()
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  carrierCode: string;
}

export class AddTrackingDto {
  @ApiProperty({ type: [AddTracking] })
  @IsNotEmpty()
  // @IsArray()
  trackings: AddTracking[];
}
export class ChangeTracking {
  @ApiProperty()
  @IsNotEmpty()
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  orderCode: string;

  @ApiPropertyOptional()
  @IsOptional()
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  oldTrackingNumber: string;

  @ApiProperty()
  @IsNotEmpty()
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  trackingNumber: string;

  @ApiProperty()
  @IsNotEmpty()
  @Transform(({ value }) => {
    if (value) return value.trim();
  })
  carrierCode: string;
}
export class ChanceTrackingDto {
  @ApiProperty({ type: [ChangeTracking] })
  @IsNotEmpty()
  // @IsArray()
  trackings: ChangeTracking[];
}

export class TimeLineDto {
  content: string;
  time: number;
}

export class SellerOrderQuery extends NewDateQuery {
  @ApiPropertyOptional()
  @Validate(IsObjectId)
  @IsOptional()
  storeId: string;

  @ApiPropertyOptional()
  @IsOptional()
  orderStatus: OrderStatusEnum;

  @ApiPropertyOptional()
  @IsOptional()
  domain: string;

  @ApiPropertyOptional()
  @IsOptional()
  orderCode: string;

  @ApiPropertyOptional()
  @IsOptional()
  email: string;

  @ApiPropertyOptional()
  @Type(() => Number)
  @IsOptional()
  priceFrom: number;

  @ApiPropertyOptional()
  @Type(() => Number)
  @IsOptional()
  priceTo: number;
}

export class OrderAdminQuery extends NewDateQuery {
  @ApiPropertyOptional()
  @IsOptional()
  searchBy: string;

  @ApiPropertyOptional()
  @Validate(IsObjectId)
  @IsOptional()
  storeId: string;

  @ApiPropertyOptional()
  @Validate(IsObjectId)
  @IsOptional()
  productId: string;

  @ApiPropertyOptional()
  @Validate(IsObjectId)
  @IsOptional()
  sellerId: string;

  @ApiPropertyOptional()
  @IsBoolean()
  @Transform(
    ({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1,
  )
  @IsOptional()
  isAbandon: boolean;
}

export class PaypalDto {
  paymentID: string;
  payerID: string;
  amount: number;
  fee?: number;
  remain?: number;
  transaction_id: string;
}

export class StripeDto {
  cardHolderName: string;
  id: string;
  paymentMethodID: string;
  amount: number;
  amount_captured: number;
  amount_refunded: number;
  application_fee: string;
  application_fee_amount: number;
}
