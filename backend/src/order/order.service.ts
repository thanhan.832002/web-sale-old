import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { BaseService } from 'src/base/base.service';
import { DataFilter } from 'src/base/interfaces/data.interface';
import BaseNoti from 'src/base/notify';
import { OrderStatusEnum, ShippingStatusEnum } from './order.enum';
import { Order } from './order.model';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import { AddTimelineDto, AddTracking, AddTrackingDto, ChanceTrackingDto, ChangeShipping, OrderAdminQuery, PaypalDto, SellerOrderQuery, StripeDto, UpdateOrderDto } from './order.dto';
import * as Promise from 'bluebird'
import { TrackingHistoryService } from 'src/tracking-history/tracking-history.service';
import { UploadTrackingHistoryDto } from 'src/tracking-history/tracking-history.model';
import { AdminSettingService } from 'src/admin-setting/admin-setting.service';
import { StoreService } from 'src/store/store.service';
import { ViewService } from 'src/view/view.service';
var momentTz = require("moment-timezone");
import * as moment from 'moment';
import { EmailService } from 'src/email/email.service';
import { NewDateQuery } from 'src/stats/stats.dto';
import { Timeout } from '@nestjs/schedule';
import { SeventeenTrack } from 'src/17track/17track.class';
import { ProductSellerService } from 'src/product-seller/product-seller.service';
import { PaymentService } from 'src/payment-method/payment.service';
import axios from 'axios';
import { UserService } from 'src/user/user.service';
const qs = require('qs')
import * as _ from 'lodash'
import { Role } from 'src/base/enum/roles.enum';
@Injectable()
export class OrderService extends BaseService<Order>{
    constructor(
        @InjectModel(Order.name) public orderModel: Model<Order>,
        private readonly trackingHistoryService: TrackingHistoryService,
        private readonly productSellerService: ProductSellerService,
        private readonly adminSettingService: AdminSettingService,
        private readonly paymentService: PaymentService,
        private readonly storeService: StoreService,
        private readonly viewService: ViewService,
        private readonly emailService: EmailService,
        private readonly userService: UserService
    ) { super(orderModel) }

    async sellerGetListOrder(userId: string, query: SellerOrderQuery) {
        try {
            let { page, limit, search, filter, orderStatus, priceFrom, priceTo, domain, orderCode, email, from, to, timezone, storeId } = query;
            if (!timezone) timezone = 'US/Arizona'
            let created
            if (from) {
                from = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
                created = { $gte: from }
            }
            if (to) {
                to = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
                created = { ...created, $lte: to }
            }
            let filterObject: DataFilter<Partial<Order>> = {
                limit,
                page,
                condition: { orderStatus: { $nin: [OrderStatusEnum.draft] }, sellerId: new Types.ObjectId(userId) },
                population: [
                    {
                        path: 'storeId', select: 'domain'
                    },
                    {
                        path: 'listSelectedProductId', select: '-orderId -domain',
                    },
                ],
                selectCols: 'orderCode id orderStatus paypal stripe total subTotal shipping discount storeId createdAt shippingAddress trackingNumber'
            };
            if (storeId) {
                filterObject.condition = {
                    ...filterObject.condition,
                    storeId: new Types.ObjectId(storeId)
                }
            }
            if (orderStatus) {
                filterObject.condition = {
                    ...filterObject.condition,
                    orderStatus
                }
            }
            if (orderCode) {
                filterObject.condition = {
                    ...filterObject.condition,
                    orderCode: { '$regex': orderCode, '$options': 'i' },
                }
            }
            if (email) {
                filterObject.condition = {
                    ...filterObject.condition,
                    'shippingAddress.email': { '$regex': email, '$options': 'i' }
                }
            }
            if (domain) {
                const foundStore = await this.storeService.findListStore({ domain: { '$regex': domain, '$options': 'i' } })
                const listStoreId = foundStore.length ? foundStore.map(user => user._id) : []
                filterObject.condition = {
                    ...filterObject.condition,
                    storeId: { $in: listStoreId },
                }
            }
            if (created) {
                filterObject.condition = {
                    ...filterObject.condition,
                    createdAt: created,
                }
            }
            let price
            if (priceFrom) {
                price = { $gte: priceFrom }
            }
            if (priceTo) {
                price = { ...price, $lte: priceTo }
            }
            if (price) {
                filterObject.condition = {
                    ...filterObject.condition,
                    total: price,
                }
            }
            const listOrder = await this.getListDocument(filterObject);
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listOrder
            }
        } catch (error) {
            throw error
        }
    }

    async sellerGetListOrderExport(userId: string, query: SellerOrderQuery) {
        try {
            let { search, filter, orderStatus, priceFrom, priceTo, domain, orderCode, email, from, to, timezone, storeId } = query;
            if (!timezone) timezone = 'US/Arizona'
            let created
            if (from) {
                from = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
                created = { $gte: from }
            }
            if (to) {
                to = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
                created = { ...created, $lte: to }
            }
            let condition: any = { orderStatus: { $nin: [OrderStatusEnum.draft] }, sellerId: new Types.ObjectId(userId) }
            if (storeId) {
                condition = {
                    ...condition,
                    storeId: new Types.ObjectId(storeId)
                }
            }
            if (orderStatus) {
                condition = {
                    ...condition,
                    orderStatus
                }
            }
            if (orderCode) {
                condition = {
                    ...condition,
                    orderCode: { '$regex': orderCode, '$options': 'i' },
                }
            }
            if (email) {
                condition = {
                    ...condition,
                    'shippingAddress.email': { '$regex': email, '$options': 'i' }
                }
            }
            if (domain) {
                const foundStore = await this.storeService.findListStore({ domain: { '$regex': domain, '$options': 'i' } })
                const listStoreId = foundStore.length ? foundStore.map(user => user._id) : []
                condition = {
                    ...condition,
                    storeId: { $in: listStoreId },
                }
            }
            if (created) {
                condition = {
                    ...condition,
                    createdAt: created,
                }
            }
            let price
            if (priceFrom) {
                price = { $gte: priceFrom }
            }
            if (priceTo) {
                price = { ...price, $lte: priceTo }
            }
            if (price) {
                condition = {
                    ...condition,
                    total: price,
                }
            }
            const listOrder = await this.orderModel.find(condition).select('-oldTrackingNumber -utmId -timeLine').populate([
                { path: 'listSelectedProductId' },
                { path: 'storeId', select: 'domain' },
                { path: 'sellerId', select: 'email' },
                { path: 'paymentId', select: 'name' }
            ])
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listOrder
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetListOrder(adminId: string, query: OrderAdminQuery) {
        try {
            const listSellerId = await this.userService.getListSellerIdByAdminId(adminId)
            let { page, limit, search, filter, from, to, timezone, searchBy, storeId, sellerId, productId, isAbandon } = query;
            if (!timezone) timezone = 'US/Arizona'
            let filterObject: DataFilter<Partial<Order>> = {
                limit,
                page,
                condition: { orderStatus: { $nin: [OrderStatusEnum.draft] }, sellerId: { $in: listSellerId } },
                population: [
                    {
                        path: 'listSelectedProductId',
                    },
                    {
                        path: 'storeId', select: 'domain'
                    },
                    {
                        path: 'sellerId', select: 'email'
                    },
                    {
                        path: 'paymentId', select: 'name'
                    }
                ],
                selectCols: '-oldTrackingNumber -utmId -timeLine'
            };
            if (isAbandon) {
                filterObject.condition = { ...filterObject.condition, isAbandon }
            }
            if (storeId) {
                filterObject.condition = { ...filterObject.condition, storeId }
            }
            if (productId) {
                filterObject.condition = { ...filterObject.condition, productId }
            }
            if (sellerId) {
                filterObject.condition = { ...filterObject.condition, sellerId }
            }
            let filterOther: any = {}
            if (from) {
                from = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
            }
            if (to) {
                to = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
            }
            const created = from && to ? {
                'createdAt': {
                    '$gte': from,
                    '$lte': to
                }
            } : undefined
            if (created) {
                filterObject.condition = { ...filterObject.condition, ...created }
            }
            if (filter) {
                filterObject.condition = { ...filterObject.condition, ...filter, ...created };
                if (filter.other) {
                    let { searchOrder, searchItem, processingDate, createdDate, sku, shippingAddress, billingAddress, trackingNumber } = filter.other
                    if (searchOrder) {
                        filterOther.orderCode = { $regex: searchOrder, $options: 'i' }
                    }
                    if (searchItem) {
                        filterOther['cart.listSelectedProduct.productId.name'] = { $regex: searchItem, $options: 'i' }
                    }
                    if (shippingAddress) {
                        filterOther['shippingAddress.address'] = { $regex: shippingAddress, $options: 'i' }
                    }
                    if (billingAddress) {
                        filterOther['billingAddress.address'] = { $regex: billingAddress, $options: 'i' }
                    }
                    if (trackingNumber) {
                        filterOther.trackingNumber = { $regex: trackingNumber, $options: 'i' }
                    }
                    if (sku) {
                        filterOther['cart.listSelectedProduct.productId.sku'] = { $regex: sku, $options: 'i' }
                    }
                    if (processingDate && processingDate.from && processingDate.to) {
                        let from = momentTz(processingDate.from).tz(timezone).startOf("day").utc().toDate();
                        let to = momentTz(processingDate.to).tz(timezone).endOf("day").utc().toDate();
                        filterOther.processingDate = { $gte: from, $lte: to }
                    }
                    if (createdDate && createdDate.from && createdDate.to) {
                        let from = momentTz(createdDate.from).tz(timezone).startOf("day").utc().toDate();
                        let to = momentTz(createdDate.to).tz(timezone).endOf("day").utc().toDate();
                        filterOther.createdAt = { $gte: from, $lte: to }
                    }
                    //@ts-ignore
                    delete filterObject.condition.other
                }
                delete filter.other
                if (filter.orderStatus) {
                    if (filter.orderStatus.length) {
                        filter.orderStatus = { $in: filter.orderStatus }
                    } else delete filter.orderStatus
                }
                if (filter.shippingStatus) {
                    if (filter.shippingStatus.length) {
                        filter.shippingStatus = { '$in': filter.shippingStatus.map(shippingStatus => Number(shippingStatus)) }
                    } else delete filter.shippingStatus
                }
                if (filter.sort) {
                    filterObject.sort = filter.sort
                } else {
                    filterObject.sort = { _id: -1 }
                }
                filterObject.condition = { ...filterObject.condition, ...filter };
            }
            if (search) {
                search = search.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&")
                if (search.split('+').length === 2 && !search.split('+')[0]) {
                    search = search.split('+')[1]
                }
                if (searchBy == 'orderCode') {
                    filterObject.condition['$or'] = [
                        { orderCode: { $regex: new RegExp(search) } }
                    ]
                } else if (searchBy == 'transactionId') {
                    filterObject.condition['$or'] = [
                        { 'paypal.paymentID': { $regex: new RegExp(search) } },
                        { 'stripe.id': { $regex: new RegExp(search) } }
                    ]
                }
                else if (searchBy == 'trackingNumber') {
                    filterObject.condition['$or'] = [
                        { trackingNumber: { $regex: new RegExp(search) } },
                    ]
                }
                else if (searchBy == 'customerName') {
                    filterObject.condition['$or'] = [
                        { searchName: { $regex: new RegExp(search) } },
                    ]
                }
                else {
                    filterObject.condition['$or'] = [
                        { 'shippingAddress.email': { $regex: new RegExp(search) } },
                        { 'shippingAddress.phoneNumber': { $regex: new RegExp(search) } }
                    ]
                }
            }
            filterObject.condition = { ...filterObject.condition, ...filterOther }
            const listOrder = await this.getListDocument(filterObject);
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listOrder
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetListExportOrder(adminId: string, query: OrderAdminQuery) {
        try {
            const listSellerId = await this.userService.getListSellerIdByAdminId(adminId)
            let { search, filter, from, to, timezone, searchBy, storeId, sellerId, productId, isAbandon } = query;
            if (!timezone) timezone = 'US/Arizona'
            let condition: any = { orderStatus: { $nin: [OrderStatusEnum.draft] }, sellerId: { $in: listSellerId } }
            let filterOther: any = {}
            if (isAbandon) {
                condition = { ...condition, isAbandon }
            }
            if (storeId) {
                condition = { ...condition, storeId }
            }
            if (productId) {
                condition = { ...condition, productId }
            }
            if (sellerId) {
                condition = { ...condition, sellerId }
            }
            if (from) {
                from = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
            }
            if (to) {
                to = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
            }
            const created = from && to ? {
                'createdAt': {
                    '$gte': from,
                    '$lte': to
                }
            } : undefined
            if (created) {
                condition = { ...condition, ...created }
            }
            if (filter) {
                condition = { ...condition, ...filter, ...created };
                if (filter.other) {
                    let { searchOrder, searchItem, processingDate, createdDate, sku, shippingAddress, billingAddress, trackingNumber } = filter.other
                    if (searchOrder) {
                        filterOther.orderCode = { $regex: searchOrder, $options: 'i' }
                    }
                    if (searchItem) {
                        filterOther['cart.listSelectedProduct.productId.name'] = { $regex: searchItem, $options: 'i' }
                    }
                    if (shippingAddress) {
                        filterOther['shippingAddress.address'] = { $regex: shippingAddress, $options: 'i' }
                    }
                    if (billingAddress) {
                        filterOther['billingAddress.address'] = { $regex: billingAddress, $options: 'i' }
                    }
                    if (trackingNumber) {
                        filterOther.trackingNumber = { $regex: trackingNumber, $options: 'i' }
                    }
                    if (sku) {
                        filterOther['cart.listSelectedProduct.productId.sku'] = { $regex: sku, $options: 'i' }
                    }
                    if (processingDate && processingDate.from && processingDate.to) {
                        let from = momentTz(processingDate.from).tz(timezone).startOf("day").utc().toDate();
                        let to = momentTz(processingDate.to).tz(timezone).endOf("day").utc().toDate();
                        filterOther.processingDate = { $gte: from, $lte: to }
                    }
                    if (createdDate && createdDate.from && createdDate.to) {
                        let from = momentTz(createdDate.from).tz(timezone).startOf("day").utc().toDate();
                        let to = momentTz(createdDate.to).tz(timezone).endOf("day").utc().toDate();
                        filterOther.createdAt = { $gte: from, $lte: to }
                    }
                    //@ts-ignore
                    delete condition.other
                }
                delete filter.other
                if (filter.orderStatus) {
                    if (filter.orderStatus.length) {
                        filter.orderStatus = { $in: filter.orderStatus }
                    } else delete filter.orderStatus
                }
                if (filter.shippingStatus) {
                    if (filter.shippingStatus.length) {
                        filter.shippingStatus = { '$in': filter.shippingStatus.map(shippingStatus => Number(shippingStatus)) }
                    } else delete filter.shippingStatus
                }
                // if (filter.sort) {
                //     filterObject.sort = filter.sort
                // } else {
                //     filterObject.sort = { _id: -1 }
                // }
                condition = { ...condition, ...filter };
            }
            if (search) {
                search = search.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&")
                if (search.split('+').length === 2 && !search.split('+')[0]) {
                    search = search.split('+')[1]
                }
                if (searchBy == 'orderCode') {
                    condition['$or'] = [
                        { orderCode: { $regex: new RegExp(search, "i") } }
                    ]
                } else if (searchBy == 'transactionId') {
                    condition['$or'] = [
                        { 'paypal.paymentID': { $regex: new RegExp(search, "i") } },
                        { 'stripe.id': { $regex: new RegExp(search, "i") } }
                    ]
                }
                else if (searchBy == 'trackingNumber') {
                    condition['$or'] = [
                        { trackingNumber: { $regex: new RegExp(search, "i") } },
                    ]
                }
                else if (searchBy == 'customerName') {
                    condition['$or'] = [
                        { searchName: { $regex: new RegExp(search, "i") } },
                    ]
                }
                else {
                    condition['$or'] = [
                        { 'shippingAddress.email': { $regex: new RegExp(search, "i") } },
                        { 'shippingAddress.phoneNumber': { $regex: new RegExp(search, "i") } }
                    ]
                }
            }
            condition = { ...condition, ...filterOther }
            const listOrder = await this.orderModel.find(condition).populate([
                {
                    path: 'listSelectedProductId',
                },
                {
                    path: 'storeId', select: 'domain'
                },
                {
                    path: 'sellerId', select: 'email'
                },
                {
                    path: 'paymentId', select: 'name'
                }
            ]).sort({ _id: -1 })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: listOrder
            }
        } catch (error) {
            throw error
        }
    }

    async adminGetOrder(adminId: string, orderId: string) {
        try {
            let [foundOrder, listSellerId] = await Promise.all([
                this.orderModel.findById(orderId).populate([
                    { path: 'listSelectedProductId' },
                    { path: 'storeId', select: 'domain' },
                    { path: 'paymentId', select: 'name' },
                ]).lean(),
                this.userService.getListSellerIdByAdminId(adminId)
            ])
            if (!foundOrder || !listSellerId.includes(foundOrder.sellerId.toString())) {
                throw new BaseApiException({ message: BaseNoti.ORDER.ORDER_NOT_EXISTS })
            }
            const relatedOrder = await this.orderModel.countDocuments({ 'shippingAddress.email': foundOrder.shippingAddress.email, orderStatus: { $ne: OrderStatusEnum.draft } })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: { ...foundOrder, relatedOrder }
            }
        } catch (error) {
            throw error
        }
    }

    async adminUpdateOrder(adminId: string, updateDto: UpdateOrderDto) {
        try {
            const timezone = await this.viewService.getTimezoneRedis(adminId)
            const { orderStatus, shippingStatus, listOrderId } = updateDto
            if (!orderStatus) delete updateDto.orderStatus
            if (!shippingStatus) delete updateDto.shippingStatus
            const listOrder = await this.orderModel.find({ _id: { $in: listOrderId } }).select('orderStatus shippingStatus createdAt')
            await Promise.map(listOrder, async (order) => {
                const orderUpdated = await this.orderModel.findByIdAndUpdate(order._id, updateDto, { new: true }).select('orderStatus shippingStatus createdAt storeId utmId total')
                const { createdAt, storeId, utmId, total, tip } = orderUpdated
                const day = momentTz(createdAt).tz(timezone).date()
                const month = momentTz(createdAt).tz(timezone).month() + 1
                const year = momentTz(createdAt).tz(timezone).year()
                if (order.orderStatus == OrderStatusEnum.canceled && orderUpdated.orderStatus != OrderStatusEnum.canceled) {
                    this.viewService.viewModel.findOneAndUpdate({ day, month, year, storeId, utmId }, { $inc: { totalCancel: -total } })
                }
                if (orderStatus && order.orderStatus != orderUpdated.orderStatus && orderStatus === OrderStatusEnum.canceled) {
                    await Promise.all([
                        this.emailService.emailCancelOrder(adminId, order),
                        this.adminAddTimeline(adminId, order._id, { content: `Customer cancel order`, time: moment().unix() })
                    ])
                } else if (orderStatus && orderStatus === OrderStatusEnum.fulfilled) {
                    await this.adminAddTimeline(adminId, order._id, { content: `Order was completed`, time: moment().unix() })
                }
            }, { concurrency: 25 })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async adminChangeShipping(adminId: string, orderId: string, { shippingAddress }: ChangeShipping) {
        try {
            const foundOrder = await this.orderModel.findById(orderId)
            const firstName = shippingAddress.firstName ? shippingAddress.firstName : foundOrder.shippingAddress.firstName
            const lastName = shippingAddress.lastName ? shippingAddress.lastName : foundOrder.shippingAddress.lastName
            const searchName = `${firstName} ${lastName}${lastName} ${firstName}`
            const updateAddress: any = { shippingAddress: { ...shippingAddress }, searchName }
            if (!foundOrder) {
                throw new BaseApiException({
                    message: BaseNoti.ORDER.ORDER_NOT_EXISTS
                })
            }
            const now = moment().unix()
            const updatedOrder = await this.orderModel.findByIdAndUpdate(orderId, updateAddress, { new: true })
            await Promise.all([
                this.adminAddTimeline(adminId, orderId, { content: `Customer's infomation was changed`, time: now }),
                this.emailService.emailChangeShipping(adminId, updatedOrder)
            ])
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: updatedOrder
            }
        } catch (error) {
            throw error
        }
    }

    async adminAddTimeline(adminId: string, orderId: string, { content, time }: AddTimelineDto) {
        try {
            let [foundOrder, listSellerId] = await Promise.all([
                this.orderModel.findById(orderId),
                this.userService.getListSellerIdByAdminId(adminId)
            ])
            if (!foundOrder || !listSellerId.includes(foundOrder.sellerId.toString())) {
                throw new BaseApiException({ message: BaseNoti.ORDER.ORDER_NOT_EXISTS })
            }
            await this.orderModel.findByIdAndUpdate(orderId, { $push: { timeLine: { content, time } } }, { new: true })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async addTimeline(adminId: string, orderId: string, { content, time }: AddTimelineDto) {
        try {
            await this.orderModel.findByIdAndUpdate(orderId, { $push: { timeLine: { content, time } } }, { new: true })
        } catch (error) {
            throw error
        }
    }

    async adminDeleteTimeline(adminId: string, orderId: string, timelineId: string) {
        try {
            let [foundOrder, listSellerId] = await Promise.all([
                this.orderModel.findById(orderId),
                this.userService.getListSellerIdByAdminId(adminId)
            ])
            if (!foundOrder || !listSellerId.includes(foundOrder.sellerId.toString())) {
                throw new BaseApiException({ message: BaseNoti.ORDER.ORDER_NOT_EXISTS })
            }
            await this.orderModel.findByIdAndUpdate(orderId, { $pull: { timeLine: { _id: timelineId } } }, { new: true })
            return {
                message: BaseNoti.GLOBAL.SUCCESS,
            }
        } catch (error) {
            throw error
        }
    }

    async ProcessBatchTracking(adminId: string, trackings: AddTracking[]) {
        try {
            let listFailedMail = []
            const listOrderCode = trackings.map(tracking => tracking.orderCode.trim())
            const listOrder = await this.orderModel.find({ orderCode: { $in: listOrderCode } })
            const listOrderCodeIn = listOrder.map(order => order.orderCode)
            const listFailedOrderCode = listOrderCode.filter(x => !listOrderCodeIn.includes(x));
            const listFailed = trackings.filter(tracking => listFailedOrderCode.includes(tracking.orderCode.trim()))
            const listOrderAdd: any = listOrder.map(order => {
                const { orderCode } = order
                const tracking = trackings.find(tracking => tracking.orderCode == orderCode)
                return { ...order.toObject(), trackingNumber: tracking?.trackingNumber, addedTracking: order.trackingNumber ? true : false }
            })
            //@ts-ignore
            const { resultPaypal, resultStripe } = await this.handleAddtrackingPayment(listOrderAdd)
            let { listSuccessPaypal, listErrPaypal } = resultPaypal
            let { listSuccessStripe, listErrStripe } = resultStripe
            //Handler After Suceess:
            const listOrderSuccess = listOrderAdd.filter(order => {
                const { stripe, paypal } = order
                if (paypal) return listSuccessPaypal.includes(order.paypal.transaction_id)
                if (stripe) return listSuccessStripe.includes(order.stripe.id)
            })
            await Promise.map(listOrderSuccess, async (order) => {
                const now = moment().unix()
                const { trackingNumber, carrierCode, addedTracking } = order
                let sendMailFullfilled = true
                try {
                    await this.emailService.emailAddTrackingOrder(adminId, order)
                } catch (error) {
                    sendMailFullfilled = false
                    listFailedMail.push(order._id.toString())
                }
                if (!addedTracking) {
                    await Promise.all([
                        this.orderModel.findByIdAndUpdate(order._id, { trackingNumber, carrierCode, orderStatus: OrderStatusEnum.fulfilled, shippingStatus: ShippingStatusEnum.available, fulfilledAt: new Date(), sendMailFullfilled }),
                        this.addTimeline(adminId, order._id, { content: 'Added Tracking Number', time: now }),
                        this.addTimeline(adminId, order._id, { content: `Tracking information email was sent: ${order.shippingAddress.email}`, time: now })
                    ])
                } else {
                    await Promise.all([
                        this.orderModel.findByIdAndUpdate(order._id, { trackingNumber }),
                        this.addTimeline(adminId, order._id, { content: 'Changed Tracking Number', time: now }),
                        this.addTimeline(adminId, order._id, { content: `Tracking information email was sent: ${order.shippingAddress.email}`, time: now })
                    ])
                }
            }, { concurrency: 25 })
            listErrPaypal = listErrPaypal.map(error => {
                const { transaction_id } = error
                const foundOrder = listOrderAdd.find(order => order?.paypal?.transaction_id && order.paypal.transaction_id == transaction_id)
                return { ...error, orderCode: foundOrder.orderCode }
            })
            listErrStripe = listErrStripe.map(error => {
                const { id } = error
                const foundOrder = listOrderAdd.find(order => order?.stripe?.id && order.stripe.id == id)
                return { ...error, orderCode: foundOrder.orderCode }
            })
            return {
                success: listOrderSuccess.length,
                failed: listFailed.length,
                listFailed,
                thirdErr: 0,
                listErrPaypal,
                listErrStripe,
                listFailedMail
            }
        } catch (error) {
            throw error
        }
    }
    async adminAddTracking(adminId: string, { trackings }: AddTrackingDto) {
        const uniqTrackings = _.uniqBy(trackings, e => e.orderCode)
        let result = {
            total: trackings.length,
            success: 0,
            failed: 0,
            thirdErr: 0,
            listErrPaypal: [],
            listErrStripe: [],
            listFailed: [],
            listFailedMail: []
        }
        try {
            const chunks = _.chunk(uniqTrackings, 200);
            await Promise.map(chunks, async trackingChunk => {
                try {
                    const data = await this.ProcessBatchTracking(adminId, trackingChunk)
                    result.success += data.success
                    result.failed += data.failed
                    result.thirdErr += data.thirdErr
                    result.listErrPaypal.push(...data.listErrPaypal)
                    result.listErrStripe.push(...data.listErrStripe)
                    result.listFailed.push(...data.listFailed)
                    result.listFailedMail.push(...data.listFailedMail)
                } catch (error) {
                    console.log('error :>> ', error);
                }
            }, { concurrency: 2 })
        } catch (error) {
            console.log('error :>> ', error);
        }
        const saveHistoryDto: UploadTrackingHistoryDto = {
            trackings,
            success: result.success | 0,
            failed: result.listFailed.length | 0,
            listFailed: result.listFailed,
            listErrPaypal: result.listErrPaypal.length ? JSON.stringify(result.listErrPaypal) : undefined,
            listErrStripe: result.listErrStripe.length ? JSON.stringify(result.listErrStripe) : undefined,
            listFailedMail: result.listFailedMail,
            thirdPartyError: result.thirdErr | 0,
            total: trackings.length,
            duplicate: trackings.length - uniqTrackings.length,
            type: 'add',
            adminId
        }
        await this.trackingHistoryService.saveHistoryUploadTracking(saveHistoryDto)
        return {
            message: BaseNoti.GLOBAL.SUCCESS,
            data: {
                ...result,
                duplicate: trackings.length - uniqTrackings.length
            }
        }
    }

    async ProcessingBatchChangeTracking(adminId: string, trackings: AddTracking[]) {
        try {
            let listFailed = []
            let listBelong = []
            let list3rdErr = []
            let listTrackingNumberSuccess = []
            const listUpdateTracking = await Promise.map(listBelong, async (tracking) => {
                const { orderCode, trackingNumber, carrierCode, oldTrackingNumber } = tracking
                let foundOrder: Order
                foundOrder = await this.orderModel.findOne({ orderCode, trackingNumber: oldTrackingNumber, orderStatus: OrderStatusEnum.fulfilled })
                if (foundOrder) {
                    const now = moment().unix()
                    const [order] = await Promise.all([
                        this.orderModel.findByIdAndUpdate(foundOrder._id, { trackingNumber, carrierCode, $push: { oldTrackingNumber: foundOrder.trackingNumber } }, { new: true }),
                        this.adminAddTimeline(adminId, foundOrder._id, { content: 'Changed Tracking Number', time: now }),
                        this.adminAddTimeline(adminId, foundOrder._id, { content: `Tracking information email was sent: ${foundOrder.shippingAddress.email}`, time: now })
                    ])
                    this.emailService.emailAddTrackingOrder(adminId, order)
                    foundOrder = order
                } else {
                    listFailed.push(tracking)
                }
                return { ...foundOrder.toObject(), carrierCode, trackingNumber, oldTrackingNumber }
            }, { concurrency: 25 })
            this.handleUpdatetrackingPayment(listUpdateTracking)
            //@ts-ignore
            // const saveHistoryDto: UploadTrackingHistoryDto = {
            //     trackings,
            //     success: listBelong.length | 0,
            //     failed: listFailed.length | 0,
            //     thirdPartyError: list3rdErr.length | 0,
            //     total: trackings.length,
            //     type: 'change',
            //     adminId
            // }
            // await this.trackingHistoryService.saveHistoryUploadTracking(saveHistoryDto)
            return {
                total: trackings.length,
                success: listTrackingNumberSuccess.length,
                failed: listFailed.length,
                thirdErr: list3rdErr.length
            }
        } catch (error) {
            throw error
        }
    }
    async adminChangeTracking(adminId: string, { trackings }: ChanceTrackingDto) {
        let result = {
            total: trackings.length,
            success: 0,
            failed: 0,
            thirdErr: 0
        }
        try {
            const chunks = _.chunk(trackings, 200);
            await Promise.map(chunks, async trackingChunk => {
                try {
                    const data = await this.ProcessingBatchChangeTracking(adminId, trackingChunk)
                    result.success += data.success
                    result.failed += data.failed
                    result.thirdErr += data.thirdErr
                } catch (error) {
                    console.log('error :>> ', error);
                }
            }, { concurrency: 2 })
        } catch (error) {
            console.log('error :>> ', error);
        }
        //@ts-ignore
        const saveHistoryDto: UploadTrackingHistoryDto = {
            trackings,
            success: result.success | 0,
            failed: result.failed | 0,
            thirdPartyError: result.thirdErr | 0,
            total: trackings.length,
            type: 'add',
            adminId
        }
        await this.trackingHistoryService.saveHistoryUploadTracking(saveHistoryDto)
        return {
            message: BaseNoti.GLOBAL.SUCCESS,
            data: {
                ...result
            }
        }
    }

    // async registerTrackingNumber(listBelong, historyId: string) {
    //     try {
    //         const secretkey = await this.adminSettingService.getSeventeenTrackKey()
    //         if (!secretkey) return
    //         const seventeenTrack = new SeventeenTrack(secretkey)
    //         const { failed } = await seventeenTrack.registerTracking(listBelong)
    //         await this.trackingHistoryService.findByIdAndUpdate(historyId, { thirdPartyError: failed })
    //     } catch (error) {

    //     }
    // }

    async masterGetStatsEmail(from, to) {
        try {
            const [confirmSuccess, confirmFail, trackingHistoryFounds] = await Promise.all([
                this.orderModel.countDocuments({ sendMailConfirmed: true, createdAt: { $gte: from, $lte: to } }),
                this.orderModel.countDocuments({ sendMailConfirmed: false, createdAt: { $gte: from, $lte: to } }),
                this.trackingHistoryService.uploadTrackingModel.find({ createdAt: { $gte: from, $lte: to } }).select('trackings listFailedMail')
            ])
            let listAllOrder = []
            let listFailedFullfill = []
            for (let tracking of trackingHistoryFounds) {
                const { trackings, listFailedMail } = tracking
                const listOrderCode = trackings.map(track => track.orderCode)
                listAllOrder.push(...listOrderCode)
                listFailedFullfill.push(...listFailedMail)
            }
            listAllOrder = [...new Set(listAllOrder)];
            return {
                confirmSuccess,
                confirmFail,
                fulfillSuccess: listAllOrder.length - listFailedFullfill.length,
                fulfillFail: listFailedFullfill.length
            }
        } catch (error) {

            throw error
        }
    }

    async masterResendEmail() {
        try {
            await Promise.all([
                this.sendFailConfirm(),
                this.sendFailFullfill()
            ])
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async sendFailFullfill() {
        try {
            const listfail = await this.trackingHistoryService.uploadTrackingModel.find({ 'listFailedMail.1': { $exists: true } })
            for (let tracking of listfail) {
                const { listFailedMail, adminId } = tracking
                let listSuccess = []
                const listOrder = await this.orderModel.find({ _id: { $in: listFailedMail } })
                let listChunk = _.chunk(listOrder, 50);
                for (let chunk of listChunk) {
                    await Promise.map(chunk, async (order) => {
                        try {
                            await this.emailService.emailConfirmOrder(adminId, order)
                            listSuccess.push(order._id.toString())
                        } catch (error) {
                        }
                    }, { concurrency: 25 })
                }
                if (listSuccess.length) {
                    await this.trackingHistoryService.uploadTrackingModel.findByIdAndUpdate(tracking._id, { $pullAll: { listFailedMail: listSuccess } })
                }
                await this.wait(500)
            }
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async sendFailConfirm() {
        try {
            const admin = await this.userService.findUser({ role: Role.admin })
            let listConfirmDone = []
            const listFail = await this.orderModel.find({ sendMailConfirmed: false })
            let listChunk = _.chunk(listFail, 50);
            for (let chunk of listChunk) {
                await Promise.map(chunk, async (order) => {
                    try {
                        await this.emailService.emailConfirmOrder(admin._id, order)
                        listConfirmDone.push(order._id)
                    } catch (error) { }

                }, { concurrency: 25 })
                await this.wait(500)
            }
            await this.orderModel.updateMany({ _id: { $in: listConfirmDone } }, { sendMailConfirmed: true })
        } catch (error) {
            throw error
        }
    }

    wait(ms) {
        return new Promise((resolve, reject) => {
            setTimeout(() => { resolve(true) }, ms)
        })
    }

    async adminGetPaymentStats(paymentId: string, { from, to, timezone }: NewDateQuery) {
        try {
            const info = await this.orderModel.aggregate([
                {
                    $match: {
                        paymentId: new Types.ObjectId(paymentId),
                        orderStatus: { $nin: [OrderStatusEnum.draft, OrderStatusEnum.canceled] }
                    }
                },
                {
                    $group: {
                        _id: 'a',
                        total: { $sum: "$total" },
                        count: { $sum: 1 }
                    }
                }
            ])
            let result = { total: 0, count: 0 }
            if (info && info.length && info[0] && info[0].total) {
                result.total = Math.round((info[0].total) * 100) / 100
            }
            if (info && info.length && info[0] && info[0].count) {
                result.count = info[0].count
            }
            return result
        } catch (error) {
            throw error
        }
    }

    async adminGetRecent(adminId: string, { from, to, timezone }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()

            let result = await this.viewService.analyticRecentAdmin(adminId, timeFrom, timeTo)
            await Promise.map(result, async (productSeller: any) => {
                let { productSellerId, storeId, revenue, productCost } = productSeller
                if (!revenue) revenue = 0
                if (!productCost) productCost = 0
                productSeller.revenue = this.roundTwoDigit(revenue)
                productSeller.productCost = this.roundTwoDigit(productCost)
                const [domain, imageUrl] = await Promise.all([
                    this.storeService.getDomainFromId(storeId),
                    this.productSellerService.getCoverImageFromId(productSellerId),
                ])
                productSeller.domain = domain
                productSeller.imageUrl = imageUrl
            }, { concurrency: 20 })
            result = result.map((product) => {
                const { purchase, revenue, viewContent, productCost } = product
                const conversionRate = viewContent != 0 ? purchase / viewContent * 100 : 0
                const aov = purchase == 0 ? 0 : revenue / purchase
                const pcre = revenue == 0 ? 0 : productCost / revenue
                return { ...product, conversionRate, purchase, aov, 'pc/re': pcre, viewContent }
            }, { concurrency: 25 })
            return result
        } catch (error) {
            throw error
        }
    }

    async adminGetAnalyticsDomain(storeId: string, { timeFrom, timeTo }: any) {
        try {
            let productCost = 0
            let revenue = 0
            let purchase = 0
            const info = await this.orderModel.aggregate([
                {
                    $match: {
                        createdAt: { $gte: timeFrom, $lte: timeTo },
                        orderStatus: { $nin: [OrderStatusEnum.draft, OrderStatusEnum.canceled] },
                        storeId: new Types.ObjectId(storeId)
                    },
                },
                {
                    $group: {
                        _id: 'a',
                        productCost: { $sum: "$totalCost" },
                        revenue: { $sum: "$total" },
                        purchase: { $sum: 1 },
                    }
                }
            ])
            if (info && info.length && info[0] && info[0].productCost) productCost = info[0].productCost
            if (info && info.length && info[0] && info[0].revenue) revenue = info[0].revenue
            if (info && info.length && info[0] && info[0].purchase) purchase = info[0].purchase
            return { productCost, revenue, purchase }
        } catch (error) {
            throw error
        }
    }

    async sellerGetAnalyticsRealtime(userId: string, { from, to, timezone, search }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
            let listProductSeller = await this.viewService.analyticDomain(userId, { from: timeFrom, to: timeTo })
            await Promise.map(listProductSeller, async (productSeller: any) => {
                let { productSellerId, storeId, productCost, revenue, aov } = productSeller
                if (!revenue) revenue = 0
                if (!productCost) productCost = 0
                const domain = await this.storeService.getDomainFromId(storeId)
                productSeller.domain = domain
                productSeller.productCost = Math.round(productCost * 100) / 100
                productSeller.revenue = Math.round(revenue * 100) / 100
                productSeller.aov = Math.round(aov * 100) / 100

            }, { concurrency: 20 })

            listProductSeller = listProductSeller.map((product) => {
                const { purchase, revenue, viewContent, productCost } = product
                const conversionRate = viewContent != 0 ? purchase / viewContent * 100 : 0
                const aov = purchase == 0 ? 0 : revenue / purchase
                const pcre = revenue == 0 ? 0 : productCost / revenue
                return { ...product, conversionRate, purchase, aov, 'pc/re': pcre }
            })
            return listProductSeller
        } catch (error) {
            throw error
        }
    }

    async sellerGetAnalyticsDomain(userId: string, { from, to, timezone, search }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
            let listProductSeller = await this.viewService.analyticDomain(userId, { from: timeFrom, to: timeTo })
            await Promise.map(listProductSeller, async (productSeller: any) => {
                const { productSellerId, storeId } = productSeller
                const [domain, imageUrl] = await Promise.all([
                    this.storeService.getDomainFromId(storeId),
                    this.productSellerService.getCoverImageFromId(productSellerId),
                ])
                productSeller.domain = domain
                productSeller.imageUrl = imageUrl
            }, { concurrency: 20 })
            listProductSeller = listProductSeller.map((product) => {
                let { purchase, revenue, viewContent, productCost } = product
                if (!revenue) revenue = 0
                if (!productCost) productCost = 0
                revenue = this.roundTwoDigit(revenue)
                productCost = this.roundTwoDigit(productCost)
                const conversionRate = viewContent != 0 ? purchase / viewContent * 100 : 0
                const aov = purchase == 0 ? 0 : revenue / purchase
                const pcre = revenue == 0 ? 0 : productCost / revenue
                return { ...product, conversionRate, purchase, aov, 'pc/re': pcre, revenue, productCost }
            })
            return listProductSeller
        } catch (error) {
            throw error
        }
    }

    async sellerGetAnalyticsRecent(userId: string, { from, to, timezone, search }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
            let listProductSeller = await this.viewService.sellerAnalyticRecent(userId, { from: timeFrom, to: timeTo })
            await Promise.map(listProductSeller, async (productSeller: any) => {
                const { productSellerId, storeId } = productSeller
                const [domain, imageUrl] = await Promise.all([
                    this.storeService.getDomainFromId(storeId),
                    this.productSellerService.getCoverImageFromId(productSellerId),
                ])
                productSeller.domain = domain
                productSeller.imageUrl = imageUrl
            }, { concurrency: 20 })
            listProductSeller = listProductSeller.map((product) => {
                let { purchase, revenue, viewContent, productCost } = product
                if (!revenue) revenue = 0
                if (!productCost) productCost = 0
                revenue = this.roundTwoDigit(revenue)
                productCost = this.roundTwoDigit(productCost)
                const conversionRate = viewContent != 0 ? purchase / viewContent * 100 : 0
                const aov = purchase == 0 ? 0 : revenue / purchase
                const pcre = revenue == 0 ? 0 : productCost / revenue
                return { ...product, conversionRate, purchase, aov, 'pc/re': pcre, revenue, productCost }
            })
            return listProductSeller
        } catch (error) {
            throw error
        }
    }

    async sellerGetRevenueDomain(userId: string, { from, to, timezone, search }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
            const listRevenue = await this.viewService.sellerRevenueDomain(userId, { from: timeFrom, to: timeTo })
            await Promise.map(listRevenue, async (report) => {
                let { revenueCancel, storeId, revenue, productCost } = report
                if (!revenue) revenue = 0
                if (!productCost) productCost = 0
                if (!revenueCancel) revenueCancel = 0
                const domain = await this.storeService.getDomainFromId(storeId)
                const revenueTotal = report.revenue + revenueCancel
                report.domain = domain
                report.revenueTotal = this.roundTwoDigit(revenueTotal)
                report.revenue = this.roundTwoDigit(revenue)
            })
            const result = listRevenue.map((product) => {
                const { purchase, revenue, productCost, revenueTotal, domain } = product
                const aov = purchase == 0 ? 0 : revenue / purchase
                const pcre = revenue == 0 ? 0 : productCost / revenue
                return {
                    ...product,
                    group: domain,
                    productCost: Math.round(productCost * 100) / 100,
                    purchase: Math.round(purchase * 100) / 100,
                    aov: Math.round(aov * 100) / 100,
                    'pc/re': Math.round(pcre * 100) / 100
                }
            })
            return result
        } catch (error) {
            throw error
        }
    }

    async sellerGetRevenueProduct(userId: string, { from, to, timezone }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const timeFrom = momentTz(from).tz(timezone).startOf('days').tz('Atlantic/Azores').toDate()
            const timeTo = momentTz(to).tz(timezone).endOf('days').tz('Atlantic/Azores').toDate()
            const listRevenue = await this.viewService.sellerRevenueProduct(timeFrom, timeTo, userId)
            await Promise.map(listRevenue, async (report) => {
                const { productId, storeId, revenue, revenueCancel } = report
                const [productName] = await Promise.all([
                    this.productSellerService.getProducname(productId)
                ])
                const revenueTotal = revenue + revenueCancel
                report.productName = productName
                report.revenueTotal = this.roundTwoDigit(revenueTotal)
            })
            const result = listRevenue.map((product) => {
                const { purchase, revenue, productCost, revenueTotal, productName, view, addToCart, checkout, goCheckout } = product
                const aov = purchase == 0 ? 0 : revenue / purchase
                const pcre = revenue == 0 ? 0 : productCost / revenue
                return {
                    group: productName,
                    goCheckout,
                    productCost: Math.round(productCost * 100) / 100,
                    revenueTotal: Math.round(revenueTotal * 100) / 100,
                    revenue: Math.round(revenue * 100) / 100,
                    purchase: Math.round(purchase * 100) / 100,
                    aov: Math.round(aov * 100) / 100,
                    'pc/re': Math.round(pcre * 100) / 100,
                    view, addToCart, checkout
                }
            })
            return result
        } catch (error) {
            throw error
        }
    }

    async sellerGetRevenueHour(userId: string, { from, to, timezone }: NewDateQuery) {
        try {
            if (!timezone) timezone = 'US/Arizona'
            const listOrderId = await this.orderModel.find({
                createdAt: { $gte: from, $lte: to },
                orderStatus: { $nin: [OrderStatusEnum.draft] },
                sellerId: new Types.ObjectId(userId)
            }).select('totalQuantity totalCost total createdAt orderStatus').lean()
            let resultArray = []
            for (let order of listOrderId) {
                let { totalQuantity, totalCost, total, createdAt, orderStatus, tip } = order
                if (!tip) tip = 0
                const group = momentTz(createdAt).tz(timezone).hour()
                const findGroupIndex = resultArray.findIndex(result => result.group == group)
                if (findGroupIndex > -1) {
                    resultArray[findGroupIndex] = {
                        group,
                        quantity: resultArray[findGroupIndex].quantity + totalQuantity,
                        productCost: totalCost + resultArray[findGroupIndex].productCost,
                        purchase: resultArray[findGroupIndex].purchase + 1,
                        revenue: orderStatus == OrderStatusEnum.canceled ?
                            resultArray[findGroupIndex].revenue - tip :
                            resultArray[findGroupIndex].revenue + total - tip,
                        revenueTotal: resultArray[findGroupIndex].revenueTotal + total
                    }
                } else {
                    resultArray.push({
                        group,
                        quantity: totalQuantity,
                        purchase: 1,
                        productCost: totalCost,
                        revenueTotal: total - tip,
                        revenue: orderStatus !== OrderStatusEnum.canceled ? total : 0
                    })
                }
            }
            resultArray = resultArray.map((product) => {
                const { purchase, revenue, productCost, revenueTotal } = product
                const aov = purchase == 0 ? 0 : revenue / purchase
                const pcre = revenue == 0 ? 0 : productCost / revenue
                return {
                    ...product,
                    productCost: Math.round(productCost * 100) / 100,
                    revenueTotal: Math.round(revenueTotal * 100) / 100,
                    revenue: Math.round(revenue * 100) / 100,
                    aov: Math.round(aov * 100) / 100,
                    'pc/re': Math.round(pcre * 100) / 100
                }
            }).sort((a, b) => (a.group - b.group))
            return resultArray
        } catch (error) {
            throw error
        }
    }

    async revenueCancelByTime({ from, to }: Partial<NewDateQuery>, filter: any) {
        try {
            const info = await this.orderModel.aggregate(
                [
                    {
                        $match: {
                            ...filter,
                            createdAt: { $gte: from, $lte: to },
                            orderStatus: OrderStatusEnum.canceled
                        }
                    },
                    {
                        $group:
                        {
                            _id: 'a',
                            total: { $sum: "$total" },
                        },
                    }
                ]
            )
            const total = info && info.length && info[0]?.total ? info[0].total : 0
            return total
        } catch (error) {
            throw error
        }
    }

    async handleAddtrackingPaymentNew(listOrderTrackingPaypal: Order[], listOrderTrackingStripe: Order[]) {
        try {
            let listHandlePaypal = []
            let listHandleStripe = []
            for (let order of listOrderTrackingPaypal) {
                if (!order) continue
                const { paymentId, paypal, stripe, trackingNumber, shippingAddress, carrierCode } = order
                if (!trackingNumber) return
                if (stripe) {
                    const { id } = stripe
                    const { firstName, lastName, city, country, apartment, address, zipCode, state, phoneNumber } = shippingAddress
                    const shipping = {
                        name: `${firstName} ${lastName}`,
                        address: {
                            city, country, line1: address, line2: apartment, postal_code: zipCode, state
                        },
                        phone: phoneNumber,
                        tracking_number: trackingNumber
                    }
                    const foundPaymentIdx = listHandleStripe.findIndex(payment => payment.paymentId.toString() == paymentId.toString())
                    if (foundPaymentIdx > -1) {
                        listHandleStripe[foundPaymentIdx].listTracking.push({ id, shipping })
                    } else {
                        listHandleStripe.push({
                            paymentId,
                            listTracking: [
                                { id, shipping }
                            ]
                        })
                    }
                }
            }
            for (let order of listOrderTrackingStripe) {
                if (!order) continue
                const { paymentId, paypal, stripe, trackingNumber, shippingAddress, carrierCode } = order
                if (!trackingNumber) return
                if (stripe) {
                    const { id } = stripe
                    const { firstName, lastName, city, country, apartment, address, zipCode, state, phoneNumber } = shippingAddress
                    const shipping = {
                        name: `${firstName} ${lastName}`,
                        address: {
                            city, country, line1: address, line2: apartment, postal_code: zipCode, state
                        },
                        phone: phoneNumber,
                        tracking_number: trackingNumber
                    }
                    const foundPaymentIdx = listHandleStripe.findIndex(payment => payment.paymentId.toString() == paymentId.toString())
                    if (foundPaymentIdx > -1) {
                        listHandleStripe[foundPaymentIdx].listTracking.push({ id, shipping })
                    } else {
                        listHandleStripe.push({
                            paymentId,
                            listTracking: [
                                { id, shipping }
                            ]
                        })
                    }
                }
            }
            const [resultPaypal, resultStripe] = await Promise.all([
                this.addTrackingInfoPaypal(listHandlePaypal),
                this.adddTrackingInfoStripe(listHandleStripe)
            ])
            return { resultPaypal, resultStripe }
        } catch (error) {
            throw error
        }
    }

    async handleAddtrackingPayment(listOrderTrackingPaypal: Order[]) {
        try {
            let listHandlePaypal = []
            let listHandleStripe = []
            for (let order of listOrderTrackingPaypal) {
                const { paymentId, paypal, stripe, trackingNumber, shippingAddress, carrierCode } = order
                if (!trackingNumber) return
                if (paypal) {
                    const { transaction_id } = paypal
                    const foundPaymentIdx = listHandlePaypal.findIndex(payment => payment.paymentId.toString() == paymentId.toString())
                    if (foundPaymentIdx > -1) {
                        listHandlePaypal[foundPaymentIdx].listTracking.push({ transaction_id, trackingNumber, carrierCode })
                    } else {
                        listHandlePaypal.push({
                            paymentId,
                            listTracking: [
                                { transaction_id, trackingNumber, carrierCode }
                            ]
                        })
                    }
                } else if (stripe) {
                    const { id } = stripe
                    const { firstName, lastName, city, country, apartment, address, zipCode, state, phoneNumber } = shippingAddress
                    const shipping = {
                        name: `${firstName} ${lastName}`,
                        address: {
                            city, country, line1: address, line2: apartment, postal_code: zipCode, state
                        },
                        phone: phoneNumber,
                        tracking_number: trackingNumber
                    }
                    const foundPaymentIdx = listHandleStripe.findIndex(payment => payment.paymentId.toString() == paymentId.toString())
                    if (foundPaymentIdx > -1) {
                        listHandleStripe[foundPaymentIdx].listTracking.push({ id, shipping })
                    } else {
                        listHandleStripe.push({
                            paymentId,
                            listTracking: [
                                { id, shipping }
                            ]
                        })
                    }
                }
            }
            const [resultPaypal, resultStripe] = await Promise.all([
                this.addTrackingInfoPaypal(listHandlePaypal),
                this.adddTrackingInfoStripe(listHandleStripe)
            ])
            return { resultPaypal, resultStripe }
        } catch (error) {
            throw error
        }
    }

    async handleUpdatetrackingPayment(listOrderTracking: any[]) {
        try {
            let listHandlePaypal = []
            let listHandleStripe = []
            for (let order of listOrderTracking) {
                if (!order) continue
                const { paymentId, paypal, stripe, trackingNumber, shippingAddress, carrierCode, oldTrackingNumber } = order
                if (!trackingNumber) return
                if (paypal) {
                    const { transaction_id } = paypal
                    const foundPaymentIdx = listHandlePaypal.findIndex(payment => payment.paymentId.toString() == paymentId.toString())
                    if (foundPaymentIdx > -1) {
                        listHandlePaypal[foundPaymentIdx].listTracking.push({ transaction_id, trackingNumber, carrierCode, oldTrackingNumber })
                    } else {
                        listHandlePaypal.push({
                            paymentId,
                            listTracking: [
                                { transaction_id, trackingNumber, carrierCode, oldTrackingNumber }
                            ]
                        })
                    }
                } else if (stripe) {
                    const { id } = stripe
                    const { firstName, lastName, city, country, apartment, address, zipCode, state, phoneNumber } = shippingAddress
                    const shipping = {
                        name: `${firstName} ${lastName}`,
                        address: {
                            city, country, line1: address, line2: apartment, postal_code: zipCode, state
                        },
                        phone: phoneNumber,
                        tracking_number: trackingNumber
                    }
                    const foundPaymentIdx = listHandleStripe.findIndex(payment => payment.paymentId.toString() == paymentId.toString())
                    if (foundPaymentIdx > -1) {
                        listHandleStripe[foundPaymentIdx].listTracking.push({ id, shipping })
                    } else {
                        listHandleStripe.push({
                            paymentId,
                            listTracking: [
                                { id, shipping }
                            ]
                        })
                    }
                }
            }
            await Promise.all([
                this.updateTrackingInfoPaypal(listHandlePaypal),
                this.adddTrackingInfoStripe(listHandleStripe)
            ])
        } catch (error) {
            throw error
        }
    }

    async adddTrackingInfoStripe(listAddStripe) {
        try {
            let listSuccessStripe = []
            let listErrStripe = []
            await Promise.map(listAddStripe, async (stripe) => {
                const { paymentId, listTracking } = stripe
                const { publicKey, secretKey } = await this.paymentService.getConfigPayment(paymentId)
                const { listSuccess, listErr } = await this.requestAddTrackingStripe(secretKey, listTracking)
                listSuccessStripe.push(...listSuccess)
                listErrStripe.push(...listErr)
            })
            return { listSuccessStripe, listErrStripe }
        } catch (error) {
            throw error
        }
    }

    async addTrackingInfoPaypal(listAddPaypal) {
        try {
            let listSuccessPaypal = []
            let listErrPaypal = []

            await Promise.map(listAddPaypal, async (paypal) => {
                const { paymentId, listTracking } = paypal
                const { publicKey, secretKey } = await this.paymentService.getConfigPayment(paymentId)
                const accessToken = await this.getPaypalAccessToken(publicKey, secretKey)
                if (!accessToken) return
                const listAddTracing = listTracking.map(tracking => {
                    const { transaction_id, trackingNumber, carrierCode } = tracking
                    return {
                        transaction_id,
                        tracking_number: trackingNumber,
                        carrier: carrierCode || "OTHER",
                        status: "SHIPPED"
                    }
                })
                const { listSuccess, listErr } = await this.requestAddTrackingPaypal(accessToken, listAddTracing)
                listSuccessPaypal.push(...listSuccess)
                listErrPaypal.push(...listErr)
            }, { concurrency: 25 })
            return {
                listSuccessPaypal, listErrPaypal
            }
        } catch (error) {
            console.log('error :>> ', error);
            throw error
        }
    }

    async updateTrackingInfoPaypal(listAddPaypal) {
        try {
            await Promise.map(listAddPaypal, async (paypal) => {
                const { paymentId, listTracking } = paypal
                const { publicKey, secretKey } = await this.paymentService.getConfigPayment(paymentId)
                const accessToken = await this.getPaypalAccessToken(publicKey, secretKey)
                if (!accessToken) return
                const listAddTracing = listTracking.map(tracking => {
                    const { transaction_id, trackingNumber, carrierCode, oldTrackingNumber } = tracking
                    return {
                        transaction_id,
                        tracking_number: trackingNumber,
                        carrier: carrierCode || "OTHER",
                        status: "SHIPPED",
                        oldTrackingNumber
                    }
                })
                await this.requestUpdateTrackingPaypal(accessToken, listAddTracing)
            }, { concurrency: 25 })
        } catch (error) {
            throw error
        }
    }

    async getPaypalAccessToken(publicKey: string, secretKey: string) {
        try {
            const basicToken = this.generateBasicAuthToken(publicKey, secretKey)
            let body = qs.stringify({
                'grant_type': 'client_credentials'
            });

            let config = {
                method: 'post',
                maxBodyLength: Infinity,
                // url: 'https://api-m.sandbox.paypal.com/v1/oauth2/token', //sandbox
                url: 'https://api.paypal.com/v1/oauth2/token', //live
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': basicToken
                },
                data: body
            };
            const { data } = await axios(config)
            return data?.access_token || ''
        } catch (error) {
            throw error
        }
    }

    generateBasicAuthToken(username, password) {
        const combined = `${username}:${password}`;
        // Encode as base64
        const encoded = Buffer.from(combined).toString('base64');
        return `Basic ${encoded}`;
    }

    async requestAddTrackingPaypal(accessToken, listTracking) {
        try {
            let listSuccess = []
            let listErr = []
            const chunks = _.chunk(listTracking, 20);
            await Promise.map(chunks, async (trackings) => {
                let body = JSON.stringify({
                    "trackers": trackings
                });
                let config = {
                    method: 'post',
                    maxBodyLength: Infinity,
                    url: 'https://api.paypal.com/v1/shipping/trackers-batch',
                    // url: 'https://api-m.sandbox.paypal.com/v1/shipping/trackers-batch',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${accessToken}`
                    },
                    data: body
                };
                const { data } = await axios.request(config)
                const { tracker_identifiers, errors } = data

                const listSuccessChunk = tracker_identifiers.map(tran => tran.transaction_id)
                listSuccess.push(...listSuccessChunk)
                const listErrChunk = errors.map(err => {
                    try {
                        const message = err?.message;
                        const transaction_id = err?.details[0]?.value
                        return {
                            message,
                            transaction_id
                        }
                    } catch (error) {
                        return {
                            transaction_id: "ERROR_ID"
                        }
                    }
                })
                listErr.push(...listErrChunk)
            })
            return {
                listSuccess, listErr
            }
        } catch (error) {
            console.log('error :>> ', error);
            throw error
        }
    }

    async requestUpdateTrackingPaypal(accessToken, listTracking) {
        try {
            for (let tracking of listTracking) {
                const { transaction_id, tracking_number, oldTrackingNumber } = tracking
                let config = {
                    method: 'put',
                    maxBodyLength: Infinity,
                    url: `https://api.paypal.com/v1/shipping/trackers/${transaction_id}-${oldTrackingNumber}`,
                    // url: `https://api-m.sandbox.paypal.com/v1/shipping/trackers/${transaction_id}-${oldTrackingNumber}`,
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${accessToken}`
                    },
                    data: { tracking_number, status: 'SHIPPED' }
                };
                await axios.request(config)
            }
        } catch (error) {
            throw error
        }
    }

    async requestAddTrackingStripe(secretKey: string, listTracking) {
        try {
            let listSuccess = []
            let listErr = []
            await Promise.map(listTracking, async (tracking) => {
                const { id, shipping } = tracking
                if (!id || !shipping || !shipping.address) return
                let config = {
                    method: 'post',
                    maxBodyLength: Infinity,
                    url: `https://api.stripe.com/v1/payment_intents/${id}`,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization': `Bearer ${secretKey}`
                    },
                    data: {
                        shipping
                    }
                };
                try {
                    const { data } = await axios(config)
                    listSuccess.push(tracking.id)
                } catch (error) {
                    const message = error?.response?.data?.error?.message || "ERROR_MESSAGE"
                    listErr.push({
                        message, id
                    })

                }
            }, { concurrency: 25 })
            return {
                listSuccess, listErr
            }
        } catch (error) {
            throw error
        }
    }

    roundTwoDigit(number: number) {
        return Math.round(number * 100) / 100
    }
}
