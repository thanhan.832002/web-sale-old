import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { ShippingAddressDto } from "./order.dto";
import { IsBoolean, IsInt, IsNotEmpty, IsNumber, IsOptional, Min, Validate } from "class-validator";
import { IsObjectId } from "src/base/custom-validator";
import { Transform, Type } from "class-transformer";
import { Order } from "./order.model";
import { UtmQuery } from "src/product-buyer/product-buyer.query";
import { ViewUtmQuery } from "src/base/interfaces/base-query.interface";

export interface Icard {
    cardNumber: string;
    exp_month: string;
    exp_year: string;
    cvv: string;
    cardHolderName: string
}

export class OneItemDto {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    variantSellerId: string

    @ApiProperty()
    @IsNumber()
    @Min(1)
    @Type(() => Number)
    @IsNotEmpty()
    quantity: number
}

export class CheckoutDto extends ShippingAddressDto {
    @ApiProperty({ type: [OneItemDto] })
    @IsNotEmpty()
    listSelectedProduct: OneItemDto[]

    @ApiPropertyOptional()
    @IsOptional()
    @Transform(({ value }) => {
        if (value) return value.trim()
    })
    couponCode: string

    @ApiProperty()
    @IsNotEmpty()
    countryIp: string

    @ApiProperty()
    @Min(0)
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    tip: number

    @ApiProperty()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsOptional()
    isAbandon: boolean

    @ApiProperty({ type: ShippingAddressDto })
    @IsOptional()
    billingAddress: ShippingAddressDto
}

export class CardCheckoutDto extends CheckoutDto {
    @ApiProperty()
    @IsNotEmpty()
    card: Icard

    @ApiProperty()
    @IsOptional()
    paymentMethodID: string

    @ApiProperty()
    @IsOptional()
    cardHolderName: string
}

export class CardConfirmDto {
    @ApiProperty()
    @IsOptional()
    paymentMethodID: string

    @ApiProperty()
    @IsOptional()
    orderCode: string

    @ApiProperty()
    @IsNotEmpty()
    paymentIntentID: string
}

export class PaypalDirectCheckoutDto extends ViewUtmQuery {
    @ApiProperty({ type: [OneItemDto] })
    @IsNotEmpty()
    listSelectedProduct: OneItemDto[]

    @ApiProperty()
    @IsNotEmpty()
    countryIp: string

    @ApiProperty()
    @IsBoolean()
    @Transform(({ value }: any) => [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1)
    @IsOptional()
    isAbandon: boolean
}

export class CalculatePriceDto {
    sellerId: string
    productSellerId: string
    listSelectedProduct: OneItemDto[]
    couponCode?: string
    domain: string
    tip?: number
}

export class CreatePaypalPaymentDto {
    paymentMethodId: string
    order: Order
}
export class CompletePaypalPaymentDto {
    @ApiProperty()
    @IsNotEmpty()
    paymentMethodId: string

    @ApiProperty()
    @IsNotEmpty()
    paymentID: string

    @ApiProperty()
    @IsNotEmpty()
    payerID: string
}