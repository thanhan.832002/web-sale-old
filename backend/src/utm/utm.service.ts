import { Injectable } from '@nestjs/common';
import { Utm } from './utm.model';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class UtmService {
    constructor(
        @InjectModel(Utm.name) public utmModel: Model<Utm>,
    ) { }

    async createUtm(createUtm: Partial<Utm>) {
        try {
            const newUtm = new this.utmModel(createUtm)
            await newUtm.save()
            return newUtm
        } catch (error) {
            throw error
        }
    }

    async findOne(filter) {
        try {
            const found = await this.utmModel.findOne(filter)
            return found
        } catch (error) {
            throw error
        }
    }

    async getListUtmId(filter: any) {
        try {
            const list = await this.utmModel.find(filter)
            return list.map(utm => utm._id) || []
        } catch (error) {
            throw error
        }
    }

    async getListUtm(filter: any) {
        try {
            const list = await this.utmModel.find(filter).select('id utmSource utmMedium utmContent utmCampaign')
            return list
        } catch (error) {
            throw error
        }
    }
}
