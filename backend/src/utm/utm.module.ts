import { Module } from '@nestjs/common';
import { UtmService } from './utm.service';
import { Utm, UtmSchema } from './utm.model';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Utm.name, schema: UtmSchema }])
  ],
  providers: [UtmService],
  exports: [UtmService]
})
export class UtmModule { }
