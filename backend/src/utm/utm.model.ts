import { Schema, Document } from 'mongoose';

export const UtmSchema: Schema = new Schema({
    utmSource: String,
    utmMedium: String,
    utmContent: String,
    utmCampaign: String
}, { collection: 'Utm' })

export class Utm extends Document {
    utmSource: string

    utmMedium: string

    utmContent: string

    utmCampaign: string
}