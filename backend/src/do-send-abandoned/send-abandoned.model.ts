import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"

export const SendAbandonedSchema: Schema = new Schema({
    email: String,
    wasSent: {
        type: Number,
        index: true,
        default: 0,
        enum: [0, 1, 2, 3]
    },
    // listCouponId: {
    //     type: [Types.ObjectId],
    //     ref: 'Coupon'
    // },
    // couponId: {
    //     type: Types.ObjectId,
    //     ref: 'Coupon'
    // },
    storeId: {
        type: Types.ObjectId,
        ref: 'Store'
    },
    listSelectedProductId: {
        type: [Types.ObjectId],
        ref: 'SelectedProductSeller'
    },
    createdTime: Number
}, { ...schemaOptions, collection: 'SendAbandoned' })


export class SendAbandoned extends Document {
    email: string
    wasSent: number
    // listCouponId: string[]
    storeId: string
    listSelectedProductId: string[]
    createdTime: number
}