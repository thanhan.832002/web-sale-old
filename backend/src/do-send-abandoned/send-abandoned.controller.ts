import { Body, Controller, Get, Param, Post } from "@nestjs/common";
import { SendAbandonedService } from "./send-abandoned.service";
import { CreateAbandonedDto } from "./send-abandoned.dto";
import { ApiTags } from "@nestjs/swagger";

@ApiTags('Abandoned')
@Controller('abandoned')
export class SendAbandonedController {
    constructor(
        private readonly sendAbandonedService: SendAbandonedService
    ) { }

    @Post('create/:domain')
    async createAbandoned(
        @Param('domain') domain: string,
        @Body() createAbandonedDto: CreateAbandonedDto
    ) {
        return this.sendAbandonedService.createAbandonedMail(domain, createAbandonedDto)
    }

    @Get('cart/:abandonedId')
    async getCartAbandoned(
        @Param('abandonedId') abandonedId: string
    ) {
        return this.sendAbandonedService.getCartAbandoned(abandonedId)
    }
}