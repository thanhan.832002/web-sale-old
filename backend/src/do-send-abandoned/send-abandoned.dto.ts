import { ApiProperty } from "@nestjs/swagger"
import { Transform, Type } from "class-transformer"
import { Validate, IsNotEmpty, Matches, IsArray, ValidateNested } from "class-validator"
import { IsObjectId } from "src/base/custom-validator"

export class SelectedProductAbandoned {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    variantSellerId: string

    @ApiProperty()
    @IsNotEmpty()
    quantity: number
}


export class CreateAbandonedDto {
    @ApiProperty()
    @Matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
    @Transform(({ value }) => {
        if (value) return value.trim().toLowerCase()
    })
    @IsNotEmpty()
    email: string

    @ApiProperty({ type: [SelectedProductAbandoned] })
    @ValidateNested()
    @Type(() => SelectedProductAbandoned)
    @IsArray()
    @IsNotEmpty()
    listSelectedProduct: SelectedProductAbandoned[]
}