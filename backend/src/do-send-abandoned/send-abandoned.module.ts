import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SendAbandoned, SendAbandonedSchema } from './send-abandoned.model';
import { SendAbandonedService } from './send-abandoned.service';
import { AbandonedCheckoutModule } from 'src/abandoned-checkout/abandoned-checkout.module';
import { CouponModule } from 'src/coupon/coupon.module';
import { EmailModule } from 'src/email/email.module';
import { SendAbandonedController } from './send-abandoned.controller';
import { SharedModule } from 'src/base/shared.module';
import { StoreModule } from 'src/store/store.module';
import { SelectedProductSellerModule } from 'src/selected-product-seller/selected-product-seller.module';
import { ProductSellerModule } from 'src/product-seller/product-seller.module';
import { VariantSellerModule } from 'src/variant-seller/variant-seller.module';
import { RateLimitCustomMidleWare } from 'src/base/rate-limit/rate-limit.middleware';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: SendAbandoned.name, schema: SendAbandonedSchema }]),
        AbandonedCheckoutModule,
        CouponModule,
        EmailModule,
        StoreModule,
        SelectedProductSellerModule,
        ProductSellerModule,
        VariantSellerModule
    ],
    providers: [SendAbandonedService],
    exports: [SendAbandonedService],
    controllers: [SendAbandonedController]
})
export class SendAbandonedModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(RateLimitCustomMidleWare).forRoutes(
                { path: '/abandoned/create/:domain', method: RequestMethod.POST }
            )
    }
}
