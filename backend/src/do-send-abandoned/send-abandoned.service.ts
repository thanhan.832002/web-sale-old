import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SendAbandoned } from "./send-abandoned.model";
import { AbandonedCheckoutService } from "src/abandoned-checkout/abandoned-checkout.service";
import { CouponTypeEnum, DiscountEnum } from "src/coupon/coupon.model";
import { CouponService } from "src/coupon/coupon.service";
var momentTz = require("moment-timezone");
import * as moment from 'moment'
import * as Promise from 'bluebird'
import { EmailService } from "src/email/email.service";
import { StoreService } from "src/store/store.service";
import BaseNoti from "src/base/notify";
import { CacheService } from "src/base/caching/cache.service";
import { KeyRedisEnum } from "src/view/view.enum";
import { CreateAbandonedDto } from "./send-abandoned.dto";
import { SelectedProductSellerService } from "src/selected-product-seller/selected-product-seller.service";
import { ProductSellerService } from "src/product-seller/product-seller.service";
import { VariantSellerService } from "src/variant-seller/variant-seller.service";
import { OneItemDto } from "src/order/order-buyer.dto";
import { VariantSeller } from "src/variant-seller/variant-seller.model";
import * as _ from 'lodash'
import { Timeout } from "@nestjs/schedule";
@Injectable()
export class SendAbandonedService {
    constructor(
        @InjectModel(SendAbandoned.name) private readonly sendAbandonedModel: Model<SendAbandoned>,
        private readonly abandonedCheckoutService: AbandonedCheckoutService,
        private readonly couponService: CouponService,
        private readonly emailService: EmailService,
        private readonly storeService: StoreService,
        private readonly selectedProductSellerService: SelectedProductSellerService,
        private readonly productSellerService: ProductSellerService,
        private readonly variantSellerService: VariantSellerService,

    ) { }

    async createAbandonedMail(domain: string, { email, listSelectedProduct }: CreateAbandonedDto) {
        try {
            email = email.toLowerCase()
            const foundStore = await this.storeService.findStoreBuyer(domain)
            const foundProductSeller = await this.productSellerService.findOne({ _id: foundStore.productSellerId })
            const listSelected = await Promise.map(listSelectedProduct, async (selectedProduct: OneItemDto) => {
                const { variantSellerId, quantity } = selectedProduct
                const variant: VariantSeller = await this.variantSellerService.findVariantCheckout(variantSellerId)
                const { price, comparePrice, productCost, imageId, listValue, sku, listOption, variantId } = variant
                return {
                    domain,
                    productName: foundProductSeller.productName,
                    productSellerId: foundProductSeller._id,
                    variantSellerId,
                    variantId,
                    sku: sku,
                    //@ts-ignore
                    imageUrl: imageId.imageUrl,
                    listValue: listValue,
                    quantity,
                    price,
                    comparePrice,
                    productCost,
                    listOption
                }
            }, { concurrency: 5 })
            const listSelectedProductIns = await this.selectedProductSellerService.createListSelectedProduct(listSelected)
            const listSelectedProductId = listSelectedProductIns.map(item => item._id)
            await this.sendAbandonedModel.findOneAndDelete({ email, storeId: foundStore._id })
            await this.createSendAbandoned(email, foundStore._id, listSelectedProductId)
            return {
                message: BaseNoti.GLOBAL.SUCCESS
            }
        } catch (error) {
            throw error
        }
    }

    async createSendAbandoned(email: string, storeId: string, listSelectedProductId: string[]) {
        try {
            email = email.toLowerCase()
            const createdTime = moment().unix()
            await this.deleteSendAbandoned(email, storeId)
            const newSend = new this.sendAbandonedModel({ email, storeId, listSelectedProductId, createdTime })
            await newSend.save()
            return newSend
        } catch (error) {
            throw error
        }
    }

    async deleteSendAbandoned(email: string, storeId: string) {
        try {
            const aban = await this.sendAbandonedModel.findOneAndDelete({ email, storeId })
            // if (aban?.listCouponId?.length) {
            //     await this.couponService.deleteMany(aban.listCouponId)
            // }
        } catch (error) {
            throw error
        }
    }

    async getCartAbandoned(abandonedId: string) {
        try {
            const foundAban = await this.sendAbandonedModel.findById(abandonedId).populate('listSelectedProductId')
            let response = {
                message: BaseNoti.GLOBAL.SUCCESS,
                data: []
            }
            if (foundAban?.listSelectedProductId && foundAban.listSelectedProductId.length) {
                response.data = foundAban.listSelectedProductId
            }
            return response
        } catch (error) {
            throw error
        }
    }

    async sendFirstMail(adminId: string) {
        try {
            const foundAbandonedSetting = await this.abandonedCheckoutService.findOne({ time: 1, adminId })
            if (!foundAbandonedSetting || !foundAbandonedSetting.discount || !foundAbandonedSetting.timestamp || !foundAbandonedSetting.emailTemplate || !foundAbandonedSetting.emailSubject) return
            if (!foundAbandonedSetting) return
            const { timestamp, discount, couponId } = foundAbandonedSetting
            if (!timestamp || !discount || !couponId) return
            const foundCoupon = await this.couponService.findOne({ filter: { _id: couponId } })
            if (!foundCoupon || !foundCoupon.code || foundCoupon.discount == undefined || foundCoupon.discount == null || !foundCoupon.discountType) return
            let timeCount = moment().subtract(timestamp, 'seconds').unix()
            const listStore = await this.storeService.findListStore({ adminId });
            const listStoreId = listStore.map(store => store._id)
            const listDoSend: SendAbandoned[] = await this.sendAbandonedModel.find({
                wasSent: 0,
                createdTime: { $lte: timeCount },
                storeId: { $in: listStoreId }
            })
            let listChunk = _.chunk(listDoSend, 50);
            for (let chunk of listChunk) {
                await Promise.map(chunk, async (doSend: SendAbandoned) => {
                    //Send Mail:
                    await Promise.all([
                        this.emailService.sendMailAbandoned1(adminId, doSend, foundCoupon, foundAbandonedSetting),
                        this.sendAbandonedModel.findByIdAndUpdate(doSend._id, { wasSent: 1 })
                    ])
                }, { concurrency: 25 })
                await this.wait(500)
            }
        } catch (error) {
            console.log('error :>> ', error)
            throw error
        }
    }

    async sendSecondMail(adminId: string) {
        try {

            const foundAbandonedSetting = await this.abandonedCheckoutService.findOne({ time: 2, adminId })
            if (!foundAbandonedSetting || !foundAbandonedSetting.discount || !foundAbandonedSetting.timestamp || !foundAbandonedSetting.emailTemplate || !foundAbandonedSetting.emailSubject) return
            if (!foundAbandonedSetting) return
            const { timestamp, discount, couponId } = foundAbandonedSetting
            if (!timestamp || !discount || !couponId) return
            const foundCoupon = await this.couponService.findOne({ filter: { _id: couponId } })
            if (!foundCoupon || !foundCoupon.code || foundCoupon.discount == undefined || foundCoupon.discount == null || !foundCoupon.discountType) return
            let timeCount = moment().subtract(timestamp, 'seconds').unix()
            const listStore = await this.storeService.findListStore({ adminId })
            const listStoreId = listStore.map(store => store._id)
            const listDoSend: SendAbandoned[] = await this.sendAbandonedModel.find({
                wasSent: 1,
                createdTime: { $lte: timeCount },
                storeId: { $in: listStoreId }
            })

            let listChunk = _.chunk(listDoSend, 50);
            for (let chunk of listChunk) {
                await Promise.map(chunk, async (doSend) => {
                    //Send mail:
                    await Promise.all([
                        this.emailService.sendMailAbandoned2(adminId, doSend, foundCoupon, foundAbandonedSetting),
                        this.sendAbandonedModel.findByIdAndUpdate(doSend._id, { wasSent: 2 })
                    ])
                }, { concurrency: 25 })
                await this.wait(500)
            }
        } catch (error) {
            throw error
        }
    }

    async sendThirdMail(adminId: string) {
        try {
            const foundAbandonedSetting = await this.abandonedCheckoutService.findOne({ time: 3, adminId })
            if (!foundAbandonedSetting || !foundAbandonedSetting.discount || !foundAbandonedSetting.timestamp || !foundAbandonedSetting.emailTemplate || !foundAbandonedSetting.emailSubject) return
            if (!foundAbandonedSetting) return
            const { timestamp, discount, couponId } = foundAbandonedSetting
            if (!timestamp || !discount || !couponId) return
            const foundCoupon = await this.couponService.findOne({ filter: { _id: couponId } })
            if (!foundCoupon || !foundCoupon.code || foundCoupon.discount == undefined || foundCoupon.discount == null || !foundCoupon.discountType) return
            const timeCount = moment().subtract(timestamp, 'seconds').unix()
            const listStore = await this.storeService.findListStore({ adminId })
            const listStoreId = listStore.map(store => store._id)
            const listDoSend: SendAbandoned[] = await this.sendAbandonedModel.find({
                wasSent: 2,
                createdTime: { $lte: timeCount },
                storeId: { $in: listStoreId }
            })
            const listChunk = _.chunk(listDoSend, 50);
            for (let chunk of listChunk) {
                await Promise.map(chunk, async (doSend) => {
                    //Send mail:
                    await Promise.all([
                        this.emailService.sendMailAbandoned3(adminId, doSend, foundCoupon, foundAbandonedSetting),
                        this.sendAbandonedModel.findByIdAndUpdate(doSend._id, { wasSent: 3 })
                    ])
                }, { concurrency: 25 })
                await this.wait(500)
            }
        } catch (error) {
            throw error
        }
    }

    wait(ms) {
        return new Promise((resolve, reject) => {
            setTimeout(() => { resolve(true) }, ms)
        })
    }
}