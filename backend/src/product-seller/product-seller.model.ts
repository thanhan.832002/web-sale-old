import { ApiProperty } from "@nestjs/swagger"
import { Type } from "class-transformer"
import { IsNotEmpty, IsNumber, Validate } from "class-validator"
import { Schema, Types, Document } from "mongoose"
import { schemaOptions } from "src/base/base.shema"
import { IsListObjectId, IsObjectId } from "src/base/custom-validator"
import { Gallery } from "src/gallery/gallery.model"
import { Variant } from "src/variant/variant.model"

export const ProductSellerSchema: Schema = new Schema({
    productId: {
        type: Types.ObjectId,
        ref: 'Product',
        index: true
    },
    sellerId: {
        type: Types.ObjectId,
        index: true,
        ref: 'User'
    },
    // storeId: {
    //     type: Types.ObjectId,
    //     ref: 'Store'
    // },
    productName: {
        type: String,
        defaut: true,
        index: true
    },
    listVariantSellerId: {
        type: [Types.ObjectId],
        ref: 'VariantSeller'
    },
    description: {
        type: String
    },
    coverId: {
        type: Types.ObjectId,
        ref: 'Gallery',
        index: true
    },
    listImageId: {
        type: [Types.ObjectId],
        ref: 'Gallery'
    },
    language: {
        type: String,
        default: 'EN'
    }
}, { ...schemaOptions, collection: 'ProductSeller' })


export class ProductSeller extends Document {
    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    productId: string

    @ApiProperty()
    @Validate(IsObjectId)
    @IsNotEmpty()
    sellerId: string


    // @ApiProperty()
    // @Validate(IsObjectId)
    // @IsNotEmpty()
    // storeId: string

    @ApiProperty()
    @Validate(IsListObjectId)
    @IsNotEmpty()
    listImageId: string[] | Gallery[]

    @ApiProperty()
    @Validate(IsListObjectId)
    @IsNotEmpty()
    listVariantSellerId: string[] | Variant[]

    @ApiProperty()
    @IsNotEmpty()
    description: string

    @ApiProperty()
    @IsNotEmpty()
    productName: string

    @ApiProperty()
    @IsNotEmpty()
    language: string
}
