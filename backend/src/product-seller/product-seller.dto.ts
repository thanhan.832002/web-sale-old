import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsNotEmpty, Validate } from "class-validator";
import { IsListObjectId } from "src/base/custom-validator";

export class UpdateMediaImageDto {
    @ApiProperty()
    @Validate(IsListObjectId)
    @IsArray()
    @IsNotEmpty()
    listNewImageId: string[]
}

export class SortMediaImageDto {
    @ApiProperty()
    @Validate(IsListObjectId)
    @IsArray()
    @IsNotEmpty()
    listNewImageId: string[]
}

export class DeleteMediaImageDto {
    @ApiProperty()
    @Validate(IsListObjectId)
    @IsArray()
    @IsNotEmpty()
    listDeletedImageId: string[]
}