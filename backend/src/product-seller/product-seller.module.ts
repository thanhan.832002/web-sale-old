import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductModule } from 'src/product/product.module';
import { VariantSellerModule } from 'src/variant-seller/variant-seller.module';
import { ProductSeller, ProductSellerSchema } from './product-seller.model';
import { ProductSellerService } from './product-seller.service';
import { GalleryModule } from 'src/gallery/gallery.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: ProductSeller.name, schema: ProductSellerSchema }]),
    ProductModule,
    VariantSellerModule,
    GalleryModule
  ],
  providers: [ProductSellerService],
  exports: [ProductSellerService]
})
export class ProductSellerModule { }
