import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BaseApiException } from 'src/base/exception/base-api.exception';
import BaseNoti from 'src/base/notify';
import { ProductService } from 'src/product/product.service';
import { VariantSeller } from 'src/variant-seller/variant-seller.model';
import { VariantSellerService } from 'src/variant-seller/variant-seller.service';
import { Variant } from 'src/variant/variant.model';
import { ProductSeller } from './product-seller.model';
import { VariantOptionService } from 'src/variant-option/variant-option.service';
import { ModuleRef } from '@nestjs/core';
import * as Promise from 'bluebird'
import { GalleryService } from 'src/gallery/gallery.service';
@Injectable()
export class ProductSellerService implements OnModuleInit {
    private variantOptionService: VariantOptionService

    constructor(
        @InjectModel(ProductSeller.name) private readonly productSellerModel: Model<ProductSeller>,
        private readonly productService: ProductService,
        private readonly variantSellerService: VariantSellerService,
        private readonly galleryService: GalleryService,
        private moduleRef: ModuleRef
    ) { }

    onModuleInit() {
        this.variantOptionService = this.moduleRef.get(VariantOptionService, { strict: false });
    }


    async createProductSeller(productId: string) {
        try {
            const foundProductToCreateStore = await this.productService.findProductCanCreateProductSeller(productId)
            const listVariant = foundProductToCreateStore.listVariant as Variant[]
            const productSellerObj = { ...foundProductToCreateStore.toObject() }
            const { listVariantOptionId } = productSellerObj
            const listOption = listVariantOptionId
            delete productSellerObj._id
            const productSellerIns: ProductSeller = new this.productSellerModel({ ...productSellerObj, productId })
            if (listVariant?.length) {
                const listVariantSellerId = await this.variantSellerService.createListVariantSeller(productSellerIns._id, listVariant, listOption)
                productSellerIns.listVariantSellerId = listVariantSellerId
            }
            await productSellerIns.save()
            return productSellerIns
        } catch (error) {
            throw error
        }
    }

    async updateProductSeller(productSellerId: string, updatProductSeller: any) {
        try {
            const { listImageId } = updatProductSeller
            if (listImageId && listImageId.length) {
                updatProductSeller.coverId = listImageId[0]
            }
            const updatedProductSeller = await this.productSellerModel.findByIdAndUpdate(productSellerId, updatProductSeller, { new: true }).populate('listImageId')
            return updatedProductSeller
        } catch (error) {
            throw error
        }
    }

    async findProductSeller(productSellerId: string): Promise<ProductSeller> {
        try {
            let foundProductSeller: any = await this.productSellerModel.findById(productSellerId).populate([
                {
                    path: 'listVariantSellerId',
                    populate: [{ path: 'imageId', select: 'imageUrl' }]
                },
                { path: 'listImageId' },
                { path: 'productId', select: 'slug' }
            ])
            // const listVariant = foundProductSeller.toObject().listVariantSellerId.filter(variant => {
            //     return variant.isSellable
            // })
            let productSeller = { ...foundProductSeller.toObject() }
            // productSeller.listVariantSellerId = listVariant
            return productSeller
        } catch (error) {
            throw error
        }
    }


    async findProductSellerHomepage(productSellerId: string): Promise<ProductSeller> {
        try {
            const foundProductSeller = await this.productSellerModel.findById(productSellerId).select('-__v -description').populate([
                {
                    path: 'listVariantSellerId',
                    populate: [{ path: 'imageId' }]
                },
                { path: 'listImageId' },
                { path: 'productId', select: 'slug' }
            ])
            return foundProductSeller
        } catch (error) {
            throw error
        }
    }

    async checkVariantSellerIsValid(productSellerId: string, variantSellerId: string) {
        try {
            const isExists = await this.productSellerModel.exists({
                _id: productSellerId,
                listVariantSellerId: variantSellerId
            })
            if (!isExists) {
                throw new BaseApiException({
                    message: BaseNoti.VARIANT.VARIANT_NOT_EXISTS
                })
            }
        } catch (error) {
            throw error
        }
    }

    async checkListVariantSellerIsValid(productSellerId: string, listVariantId: string[]) {
        try {
            let listValid = []
            for (const variantId of listVariantId) {
                const isExists = await this.productSellerModel.exists({
                    _id: productSellerId,
                    listVariantSellerId: variantId
                })
                if (isExists) listValid.push(variantId)
            }
            return listValid
        } catch (error) {
            throw error
        }
    }

    async findProductCheckout(productSellerId: string) {
        try {
            const found = await this.productSellerModel.findById(productSellerId).select('productName slug id ')
            if (!found) {
                throw new BaseApiException({
                    message: BaseNoti.PRODUCT.PRODUCT_NOT_EXISTS
                })
            }
            return found
        } catch (error) {
            throw error
        }
    }

    async deleteAllVariantProductSeller(productId: string) {
        try {
            const listProductSeller = await this.productSellerModel.find({ productId }).select('id')
            await Promise.map(listProductSeller, async (productSeller) => {
                await this.variantSellerService.deleteMany({ productSellerId: productSeller._id })
            })
            return listProductSeller.map(productSeller => productSeller._id)
        } catch (error) {
            throw error
        }
    }

    async findMany(filter: any) {
        try {
            const list = await this.productSellerModel.find(filter)
            return list
        } catch (error) {
            throw error
        }
    }

    async findProductIdByProductSellerId(productSellerId: string): Promise<string> {
        try {
            const found = await this.productSellerModel.findById(productSellerId).select('productId id ')
            if (!found || !found.productId) return
            return found.productId.toString()
        } catch (error) {
            throw error
        }
    }

    async findListProductSellerIdByProductId(productId: string) {
        try {
            const listAll = await this.productSellerModel.find({ productId }).select('id')
            return listAll.map(productSeller => productSeller._id)
        } catch (error) {
            return []
        }
    }

    async getCoverImageFromId(productSellerId: string) {
        try {
            const foundProductSeller: any = await this.productSellerModel.findById(productSellerId).select('listImageId').populate({ path: 'listImageId', select: 'imageUrl' }).lean()
            if (foundProductSeller && foundProductSeller.listImageId && foundProductSeller.listImageId.length) {
                return foundProductSeller.listImageId[0].imageUrl
            }
        } catch (error) {
            throw error
        }
    }

    async getCoverAndProductName(productSellerId: string) {
        try {
            let imageUrl = ''
            let productName = ''
            const foundProductSeller: any = await this.productSellerModel.findById(productSellerId).select('listImageId productName').populate({ path: 'listImageId', select: 'imageUrl' }).lean()
            if (foundProductSeller && foundProductSeller.listImageId && foundProductSeller.listImageId.length) {
                imageUrl = foundProductSeller.listImageId[0].imageUrl
            }
            if (foundProductSeller?.productName) productName = foundProductSeller.productName
            return {
                imageUrl,
                productName
            }
        } catch (error) {
            throw error
        }
    }

    async getProducname(productId: string) {
        try {
            const foundProduct = await this.productService.findOne({
                filter: { _id: productId },
                selectCols: ['productName']
            })
            return foundProduct?.productName || ''
        } catch (error) {

        }
    }

    async getProductSellername(productSellerId: string) {
        try {
            const foundProduct = await this.productSellerModel.findById(productSellerId)
            return foundProduct?.productName || ''
        } catch (error) {

        }
    }

    async findOne(filter: any) {
        try {
            return await this.productSellerModel.findOne(filter)
        } catch (error) {
            throw error
        }
    }

}
