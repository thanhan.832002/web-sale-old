module.exports = {
    apps: [{
        name: "websalePreBe",
        script: "./dist/main.js",
        env: {
            NODE_ENV: "development",
        },
        env_production: {
            NODE_ENV: "production",
        },
        instances: "6"
    }]
}