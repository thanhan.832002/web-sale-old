import colors from "vuetify/es5/util/colors";

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: "%s",
    title: "Web Sale Premium",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/images/logo-not-text.png" },
    ],
    script: [
      {
        src: "/js/jquery.min.js",
      },
      {
        src: "https://unpkg.com/@dotlottie/player-component@latest/dist/dotlottie-player.mjs",
        type: "module",
      },
    ],
    // link: [
    //   {
    //     href: "https://cdn.jsdelivr.net/npm/@mdi/font@7.2.96/css/materialdesignicons.min.css",
    //   },
    // ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "~/assets/css/tailwind.main.css",
    "~/assets/css/materials.scss",
    "~/assets/css/main.css",
    "~/assets/css/style.css",
    "~/assets/css/custom.scss",
  ],

  ssr: false,

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "~/plugins/axios",
    "~/plugins/api",
    "~/plugins/notify",
    "~/plugins/localforage.client",
    "~/plugins/nuxt-client-init.client",
    "~/plugins/utils",
    "~/plugins/vue-debounce",
    "~/plugins/vue-json-excel",
    "~/plugins/apexchart",
    "~/plugins/vue-tiny",
    "~/plugins/vue-draggable",
  ],
  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    "@nuxt/typescript-build",
    // https://go.nuxtjs.dev/vuetify
    "@nuxtjs/vuetify",
    "@nuxtjs/localforage",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/axios", "vue-toastification/nuxt", "nuxt-clipboard2"],

  server: {
    // host: '103.118.28.103',
    port: 2001,
  },

  axios: {
    baseURL:
      process.env.NODE_ENV === "development"
        ? "http://localhost:2000"
        : "https://api.opensitex.com",
  },
  // http://192.168.1.154:2000
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    defaultAssets: false,
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    postcss: {
      postcssOptions: {
        plugins: {
          "postcss-import": false,
          tailwindcss: {},
          autoprefixer: {},
        },
      },
    },
  },

  loading: {
    color: "#3e7fb8",
    height: "2px",
    // css: false
  },
};
