import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const couponApi = ($axios: NuxtAxiosInstance) => ({
    create: (payload: any) => {
        return $axios.$post(`${resources.Coupon}/create`, payload)
    },
    edit: (couponId: string, payload: any) => {
        return $axios.$put(`${resources.Coupon}/edit/${couponId}`, payload)
    },
    delete: (couponId: string) => {
        return $axios.$delete(`${resources.Coupon}/delete/${couponId}`)
    },
    deleteList: (listCouponId: string[]) => {
        return $axios.$post(`${resources.Coupon}/delete-list`, { listCouponId })
    },
    list: (query: any) => {
        return $axios.$get(`${resources.Coupon}/list-coupon?${objectToParam(query)}`)
    }
})

export default couponApi
export type CouponApiType = ReturnType<typeof couponApi>