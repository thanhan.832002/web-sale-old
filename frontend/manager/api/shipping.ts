import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const shippingApi = ($axios: NuxtAxiosInstance) => ({
  shippingDetail: (shippingId: string) => {
    return $axios.$get(`${resources.Shipping}/shipping-detail/${shippingId}`);
  },

  listShippingUser: (query: any) => {
    return $axios.$get(
      `${resources.Shipping}/list-shipping?${objectToParam(query)}`
    );
  },
  allShippingAdmin: (query?: any) => {
    return $axios.$get(
      `${resources.Shipping}/all-shipping?${objectToParam(query)}`
    );
  },
  create: (payload: any) => {
    return $axios.$post(`${resources.Shipping}/create`, payload);
  },

  edit: (shippingId: string, payload: any) => {
    return $axios.$put(`${resources.Shipping}/edit/${shippingId}`, payload);
  },

  delete: (shippingId: string) => {
    return $axios.$delete(`${resources.Shipping}/delete/${shippingId}`);
  },
});

export default shippingApi;
export type ShippingApiType = ReturnType<typeof shippingApi>;
