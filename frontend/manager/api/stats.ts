import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { IQuery } from "~/interfaces/common";

const statsApi = ($axios: NuxtAxiosInstance) => ({
  getByTime: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/by-time?${objectToParam(query)}`
    );
  },
  getOrder: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/order-daily?${objectToParam(query)}`
    );
  },
  getRecent: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/recent?${objectToParam(query)}`
    );
  },
  getProfit: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/profit-daily?${objectToParam(query)}`
    );
  },
  getCustomer: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/customer-daily?${objectToParam(query)}`
    );
  },
  getItemSold: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/item-sold-daily?${objectToParam(query)}`
    );
  },
  getReportRevenue: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/report-revenue?${objectToParam(query)}`
    );
  },
  getReportRevenueProduct: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/report-revenue-product?${objectToParam(query)}`
    );
  },

  // Analytic
  getListUtm: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/analytics-utm?${objectToParam(query)}`
    );
  },
  getListDomain: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/analytics-domain?${objectToParam(query)}`
    );
  },
  getListProduct: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/analytics-product?${objectToParam(query)}`
    );
  },
  getListSeller: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/analytics-seller?${objectToParam(query)}`
    );
  },

  // Abandoned
  getListUtmAbandon: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/abandoned-date?${objectToParam(query)}`
    );
  },
  getListDomainAbandon: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/abandoned-domain?${objectToParam(query)}`
    );
  },
  getListProductAbandon: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/abandoned-product?${objectToParam(query)}`
    );
  },
  getListSellerAbandon: (query: any) => {
    return $axios.$get(
      `${resources.StatsAdmin}/abandoned-seller?${objectToParam(query)}`
    );
  },

  // seller
  statsAdminSeller: (sellerId: string) => {
    return $axios.$get(`${resources.StatsAdmin}/seller/${sellerId}`);
  },
  getRealTimeAnalytics: (query: any) => {
    return $axios.$get(
      `${resources.StatsSeller}/analytics-realtime?${objectToParam(query)}`
    );
  },
  statsAnalyticsDomain: (query: any) => {
    return $axios.$get(
      `${resources.StatsSeller}/analytics-domain?${objectToParam(query)}`
    );
  },
  statsAnalyticsRecent: (query: any) => {
    return $axios.$get(
      `${resources.StatsSeller}/analytics-recent?${objectToParam(query)}`
    );
  },
  statsAnalyticsUtm: (query: any) => {
    return $axios.$get(
      `${resources.StatsSeller}/analytics-utm?${objectToParam(query)}`
    );
  },
  statsByTime: (query: any) => {
    return $axios.$get(
      `${resources.StatsSeller}/by-time?${objectToParam(query)}`
    );
  },
  statsByTimeMaser: (query: any) => {
    return $axios.$get(`stats-master/by-time?${objectToParam(query)}`);
  },
  statsStoreByTime: (query: any) => {
    return $axios.$get(
      `${resources.StatsSeller}/by-time?${objectToParam(query)}`
    );
  },
  chartSeller: (query: any) => {
    return $axios.$get(
      `${resources.StatsSeller}/chart?${objectToParam(query)}`
    );
  },

  statsBestSeller: (query: IQuery) => {
    return $axios.$get(
      `${resources.StatsSeller}/best-store?${objectToParam(query)}`
    );
  },
  revenueDomain: (query: any) => {
    return $axios.$get(
      `${resources.StatsSeller}/revenue/domain?${objectToParam(query)}`
    );
  },
  revenueDate: (query: any) => {
    return $axios.$get(
      `${resources.StatsSeller}/revenue/date?${objectToParam(query)}`
    );
  },
  revenueMonth: (query: any) => {
    return $axios.$get(
      `${resources.StatsSeller}/revenue/month?${objectToParam(query)}`
    );
  },
  revenueProduct: (query: any) => {
    return $axios.$get(
      `${resources.StatsSeller}/revenue/product?${objectToParam(query)}`
    );
  },
  revenueHour: (query: any) => {
    return $axios.$get(
      `${resources.StatsSeller}/revenue/hour?${objectToParam(query)}`
    );
  },
  revenueSeller: (query: any) => {
    return $axios.$get(
      `${resources.StatsSeller}/revenue/seller?${objectToParam(query)}`
    );
  },
});

export default statsApi;
export type StatsApiType = ReturnType<typeof statsApi>;
