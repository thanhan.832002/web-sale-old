import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const collectionApi = ($axios: NuxtAxiosInstance) => ({
    create: (payload: any) => {
        return $axios.$post(`${resources.Category}/create`, payload)
    },

    getListAll: () => {
        return $axios.$get(`${resources.Category}/admin/list-all-collection`)
    },

    getListAdmin: (query: any) => {
        return $axios.$get(`${resources.Category}/list?${objectToParam(query)}`)
    },


    edit: (collectionId: string, payload: any) => {
        return $axios.$put(`${resources.Category}/update/${collectionId}`, payload)
    },

    editList: (body: any) => {
        return $axios.$put(`${resources.Category}/update-list`, body)
    },

    delete: (collectionId: string) => {
        return $axios.$delete(`${resources.Category}/delete/${collectionId}`)
    },

    getDetailAdmin: (collectionSlug: string) => {
        return $axios.$get(`${resources.Category}/detail/${collectionSlug}`)
    }

})

export default collectionApi
export type CollectionApiType = ReturnType<typeof collectionApi>