import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const productSellerApi = ($axios: NuxtAxiosInstance) => ({
    list: (query?: any) => {
        return $axios.$get(`${resources.productSeller}/list?${objectToParam(query)}`)
    },
})

export default productSellerApi
export type ProductSellerApiType = ReturnType<typeof productSellerApi>