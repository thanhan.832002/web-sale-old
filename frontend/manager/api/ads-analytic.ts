import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const adsAnalyticApi = ($axios: NuxtAxiosInstance) => ({
    add: (body: any) => {
        return $axios.$post(`${resources.adsAnalytic}/add`, {
            listUpdate: body
        })
    },
    list: () => {
        return $axios.$get(`${resources.adsAnalytic}`)
    }
})

export default adsAnalyticApi
export type AdsAnalyticApiType = ReturnType<typeof adsAnalyticApi>