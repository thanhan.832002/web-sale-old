import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { IQuery } from "~/interfaces/common";

const sellerOrder = ($axios: NuxtAxiosInstance) => ({
    list: (query: IQuery) => {
        return $axios.$get(`${resources.OrderSeller}/list?${objectToParam(query)}`)
    }, 
    exportList: (query: IQuery) => {
        return $axios.$get(`${resources.OrderSeller}/list-export?${objectToParam(query)}`)
    }
})

export default sellerOrder
export type SellerOrderType = ReturnType<typeof sellerOrder>