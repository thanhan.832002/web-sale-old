import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { IQuery } from "~/interfaces/common";

const storeAdmin = ($axios: NuxtAxiosInstance) => ({
    listSellerStore: (query: any) => {
        return $axios.$get(`${resources.storeAdmin}/list?${objectToParam(query)}`)
    },
    changeStatus: (body: any) => {
        return $axios.$put(`${resources.storeAdmin}/change-status`, body)
    }
})

export default storeAdmin
export type StoreAdminType = ReturnType<typeof storeAdmin>