import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { IQuery } from "~/interfaces/common";

const userPaymentAdmin = ($axios: NuxtAxiosInstance) => ({
    listSeller: (paymentId: string, query: IQuery) => {
        return $axios.$get(`${resources.userPaymentAdmin}/list-seller/${paymentId}`)
    },
    attach: (body: any) => {
        return $axios.$put(`${resources.userPaymentAdmin}/attach`, body)
    },

    // seller
    listPaymentSeller: (sellerId: string) => {
        return $axios.$get(`${resources.userPaymentAdmin}/list?sellerId=${sellerId}`)
    }
})

export default userPaymentAdmin
export type UserPaymentAdminType = ReturnType<typeof userPaymentAdmin>