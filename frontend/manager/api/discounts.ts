import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { resources } from "~/interfaces/api";
import { objectToParam } from "~/common/utils";
import { IItemListDiscount, ISellerDiscount } from "~/interfaces/discount";
const discountsApi = ($axios: NuxtAxiosInstance) => ({
    list: (storeId: string, query: any) => {
        return $axios.$get(`${resources.SellerDiscount}/list/${storeId}?${objectToParam(query)}`);
    },

    addDiscount: (body: IItemListDiscount) => {
        return $axios.$post(`${resources.SellerDiscount}/create`, body)
    },

    detail: (storeId: string) => {
        return $axios.$get(`${resources.SellerDiscount}/store/${storeId}`);
    },

    storeCreate: (body: ISellerDiscount) => {
        return $axios.$post(`${resources.SellerDiscount}/store/create`, body)
    },

    storeUpdate: (body: ISellerDiscount, storeId: string) => {
        return $axios.$put(`${resources.SellerDiscount}/store/update/${storeId}`, body)
    },

    edit: (payload: any) => {
        return $axios.$put(`${resources.SellerDiscount}/update`,
            payload
        )
    },
    delete: (discountId: string) => {
        return $axios.$delete(`${resources.SellerDiscount}/delete/${discountId}`)
    },
})

export default discountsApi
export type DiscountsApiType = ReturnType<typeof discountsApi>