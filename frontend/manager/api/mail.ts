import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { IQuery } from "~/interfaces/common";

const adminSendMail = ($axios: NuxtAxiosInstance) => ({
  sendOrderConfirm: (email: string) => {
    return $axios.$post(`${resources.AdminSendMail}/order-confirm`, {
      email,
    });
  },
  sendOrderCancel: (email: string) => {
    return $axios.$post(`${resources.AdminSendMail}/order-cancel`, {
      email,
    });
  },
  sendOrderTracking: (email: string) => {
    return $axios.$post(`${resources.AdminSendMail}/order-tracking`, {
      email,
    });
  },
  sendOrderChangeShipping: (email: string) => {
    return $axios.$post(`${resources.AdminSendMail}/change-shipping`, {
      email,
    });
  },
  abandonedFirst: (email: string) => {
    return $axios.$post(`${resources.AdminSendMail}/abandoned-first`, {
      email,
    });
  },
  abandonedSecond: (email: string) => {
    return $axios.$post(`${resources.AdminSendMail}/abandoned-second`, {
      email,
    });
  },
  abandonedThird: (email: string) => {
    return $axios.$post(`${resources.AdminSendMail}/abandoned-third`, {
      email,
    });
  },
});

export default adminSendMail;
export type AdminSendMailType = ReturnType<typeof adminSendMail>;
