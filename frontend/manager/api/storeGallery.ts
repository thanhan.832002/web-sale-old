import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { IAuth } from "~/interfaces/auth";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { IChangeGalleryTag, ICreateUpdateTag } from "~/interfaces/tag";

const storeGalleryApi = ($axios: NuxtAxiosInstance) => ({
  getList: (storeId: string) => {
    return $axios.$get(`${resources.storeGallery}/list/${storeId}`);
  },
  delete: (storeId: string, galleryId: string) => {
    return $axios.$delete(
      `${resources.storeGallery}/delete/${storeId}/${galleryId}`
    );
  },
});

export default storeGalleryApi;
export type StoreGalleryApiType = ReturnType<typeof storeGalleryApi>;
