import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { IQuery } from "~/interfaces/common";

const userMaster = ($axios: NuxtAxiosInstance) => ({
  listAdmin: (query: IQuery) => {
    return $axios.$get(
      `${resources.userMaster}/list-admin?${objectToParam(query)}`
    );
  },
  addPrefix: (body: any) => {
    return $axios.$put(`${resources.userMaster}/add-prefix`, body);
  },
  createAdmin: (body: any) => {
    return $axios.$post(`${resources.userMaster}/create-admin`, body);
  },
  changeStatus: (body: any) => {
    return $axios.$put(`${resources.userMaster}/change-status`, body);
  },
  email: (query: IQuery) => {
    return $axios.$get(
      `${resources.StatsMaster}/email?${objectToParam(query)}`
    );
  },
  resendMail: () => {
    return $axios.$post(`${resources.StatsMaster}/resend-fail-mail`);
  },
});

export default userMaster;
export type UserMasterType = ReturnType<typeof userMaster>;
