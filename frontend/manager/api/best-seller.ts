import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const bestSellerApi = ($axios: NuxtAxiosInstance) => ({
    list: () => {
        return $axios.$get(`${resources.bestSeller}/list`)
    },
    addBestSeller: (listProductId: string[]) => {
        return $axios.$post(`${resources.bestSeller}/add`, {
            listProductId
        })
    },
    changePosition: (status: number, productId: string) => {
        return $axios.$put(`${resources.bestSeller}/change-position/${productId}`, {
            status
        })
    },
    delete: (productId: string) => {
        return $axios.$delete(`${resources.bestSeller}/delete/${productId}`)
    }
})

export default bestSellerApi
export type BestSellerApiType = ReturnType<typeof bestSellerApi>