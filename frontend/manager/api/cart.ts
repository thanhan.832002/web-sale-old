import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { resources } from "~/interfaces/api";
import { objectToParam } from "~/common/utils";
import { api } from "~/common/constaint";

const cartApi = ($axios: NuxtAxiosInstance) => ({
    create: () => {
        return $axios.$post(`${api}/${resources.Cart}/create`)
    },
    getMyCart: (cartId: string) => {
        return $axios.$get(`${api}/${resources.Cart}/my-cart?cartId=${cartId}`)
    },
    addToCart: (cartId: string, payload: any) => {
        return $axios.$post(`${api}/${resources.Cart}/add-to-cart?cartId=${cartId}`, payload)
    },
    addAllToCart: (cartId: string, payload: any) => {
        return $axios.$post(`${api}/${resources.Cart}/add-all-to-cart?cartId=${cartId}`, payload)
    },
    addComboToCart: (cartId: string, payload: any) => {
        return $axios.$post(`${api}/${resources.Cart}/add-combo-to-cart?cartId=${cartId}`, payload)
    },
    updateCart: (cartId: string, payload: any) => {
        return $axios.$put(`${api}/${resources.Cart}/update-cart?cartId=${cartId}`, payload)
    },
    updateCombocart: (cartId: string, payload: any) => {
        return $axios.$put(`${api}/${resources.Cart}/update-combo-cart?cartId=${cartId}`, payload)
    },
    deleteCart: (cartId: string, payload: any) => {
        return $axios.$post(`${api}/${resources.Cart}/delete-product?cartId=${cartId}`, payload)
    },
    deleteComboCart: (cartId: string, comboId: string) => {
        return $axios.$post(`${api}/${resources.Cart}/delete-combo-cart?cartId=${cartId}`, {
            comboId
        })
    },

    checkout: (payload: any, domain: string) => {
        return $axios.$post(`${api}/${resources.Order}/paypal-checkout/${domain}`, payload)
    },
    checkoutCard: (payload: any, domain: string) => {
        return $axios.$post(`${api}/${resources.Order}/card-checkout/${domain}`, payload)
    },
    paypalCreateOrder: (payload: any, domain: string) => {
        return $axios.$post(`${api}/${resources.Order}/paypal-create-order/${domain}`, payload)
    },
    paypalCreateOrderDirect: (payload: any, domain: string) => {
        return $axios.$post(`${api}/${resources.Order}/paypal-direct-checkout/${domain}`, payload)
    },
    completePaypalPayment: (payload: any) => {
        return $axios.$post(`${api}/${resources.Order}/paypal-complete-payment`, payload)
    },
    stripeCreateorder: (body: any, domain: string) => {
        return $axios.$post(`${api}/${resources.Order}/stripe-create-order/${domain}`, body)
    },
    stripeCompletePayment: (orderCode: string, domain: string) => {
        return $axios.$post(`${api}/${resources.Order}/stripe-complete-payment/${domain}`, {
            orderCode
        })
    }
})

export default cartApi
export type CartApiType = ReturnType<typeof cartApi>