import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const settingApi = ($axios: NuxtAxiosInstance) => ({
    getSetting: () => {
        return $axios.$get(`${resources.AdminSetting}/get`)
    },
    updateSetting: (payload: any) => {
        return $axios.$put(`${resources.AdminSetting}/update`, payload)
    },

    //Home Page Setting
    detailHomePage: (domainId: string) => {
        return $axios.$get(`${resources.HomePageSetting}/detail/${domainId}`)
    },
    updateDetailHomePage: (domainId: string, payload: any) => {
        return $axios.$put(`${resources.HomePageSetting}/update/${domainId}`, payload)
    }
})

export default settingApi
export type SettingApiType = ReturnType<typeof settingApi>