import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const domainSellerApi = ($axios: NuxtAxiosInstance) => ({
  list: (query: any) => {
    return $axios.$get(
      `${resources.domainSeller}/list?${objectToParam(query)}`
    );
  },
});

export default domainSellerApi;
export type DomainSellerApiType = ReturnType<typeof domainSellerApi>;
