import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const productApi = ($axios: NuxtAxiosInstance) => ({
    create: (payload: any) => {
        return $axios.$post(`${resources.Product}/create-product`, payload)
    },
    getListAll: () => {
        return $axios.$get(`${resources.Product}/admin/list-all-product`)
    },
    getListAdmin: (query: any) => {
        return $axios.$get(`${resources.Product}/admin/list-product?${objectToParam(query)}`)
    },

    edit: (productId: string, payload: any) => {
        return $axios.$post(`${resources.Product}/edit-product/${productId}`, payload)
    },

    delete: (productId: string) => {
        return $axios.$delete(`${resources.Product}/delete-product/${productId}`)
    },

    deleteList: (listProductId: string[]) => {
        return $axios.$post(`${resources.Product}/delete-list-product`, { listProductId })
    },

    getDetail: (productId: string) => {
        return $axios.$get(`${resources.Product}/product-detail/${productId}`)
    },
    getDetailAdmin: (productId: string) => {
        return $axios.$get(`${resources.Product}/product-detail/${productId}`)
    },
    getListReview: (query: any) => {
        return $axios.$get(`${resources.Product}/review/admin/list?${objectToParam(query)}`)
    },
    approvedReview: (reviewId: string, approved: boolean) => {
        return $axios.$put(`${resources.Product}/review/admin/approved/${reviewId}`, { approved })
    },
    updateReview: (reviewId: string, productId: string, review: any) => {
        return $axios.$put(`${resources.productReview}/edit/${reviewId}/${productId}`, review)
    },
    deleteReview: (reviewId: string) => {
        return $axios.$delete(`${resources.Product}/review/admin/delete/${reviewId}`)
    },

    updateVariantOption: (body: any) => {
        return $axios.$put(`${resources.VariantOption}/update`, body)
    },
    updateVariant: (body: any) => {
        return $axios.$put(`${resources.Variant}/update-list`, body)
    }

})

export default productApi
export type ProductApiType = ReturnType<typeof productApi>