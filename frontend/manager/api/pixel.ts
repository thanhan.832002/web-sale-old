import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const pixelApi = ($axios: NuxtAxiosInstance) => ({
    put: (storeId: any, payload: any) => {
        return $axios.$put(`${resources.pixel}/update/${storeId}`, payload)
    },
    get: (storeId: any) => {
        return $axios.$get(`${resources.pixel}/detail/${storeId}`)
    },
})

export default pixelApi
export type PixelApiType = ReturnType<typeof pixelApi>