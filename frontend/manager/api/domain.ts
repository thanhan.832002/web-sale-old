import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { api } from "~/common/constaint";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { IDomain } from "~/interfaces/domain";

const domainApi = ($axios: NuxtAxiosInstance) => ({
    list: (query: any) => {
        return $axios.$get(`${api}/${resources.domain}/list?${objectToParam(query)}`);
    },
    getPointDomainInfo: (domainId: string) => {
        return $axios.$get(`${api}/${resources.domain}/point-domain-info/${domainId}`)
    },
    getSslInfo: (domainId: string) => {
        return $axios.$get(`${api}/${resources.domain}/ssl-info/${domainId}`)
    },
    verifySsl: (domainId: string) => {
        return $axios.$get(`${api}/${resources.domain}/verify-ssl/${domainId}`)
    },
    create: (body: IDomain) => {
        return $axios.post(`${api}/${resources.domain}/create`, body)
    },
    verifyEmail: (domainId: string) => {
        return $axios.post(`${api}/${resources.domain}/verify-email-domain`, { domainId })
    },
    addSupportEmail: (body: IDomain) => {
        return $axios.put(`${api}/${resources.domain}/update-email`, body)
    },
    update: (body: IDomain) => {
        return $axios.put(`${api}/${resources.domain}/update`, body)
    },
    verifyDomain: (domainId: string) => {
        return $axios.put(`${api}/${resources.domain}/check-point-domain/${domainId}`)
    },
    resendVerifyEmail: (domainId: string) => {
        return $axios.put(`${api}/${resources.domain}/resend-verify-email/${domainId}`)
    },
    delete: (domainId: string) => {
        return $axios.delete(`${api}/${resources.domain}/delete/${domainId}`)
    }
});

export default domainApi;
export type DomainApiType = ReturnType<typeof domainApi>;
