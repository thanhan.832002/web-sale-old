import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { resources } from "~/interfaces/api";

const promotionApi = ($axios: NuxtAxiosInstance) => ({
    get: () => {
        return $axios.$get(`${resources.Promotion}/promotion-detail`)
    },
    update: (payload: any) => {
        return $axios.$post(`${resources.Promotion}/update-promotion`, payload)
    }
})

export default promotionApi
export type PromotionApiType = ReturnType<typeof promotionApi>