import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const comboApi = ($axios: NuxtAxiosInstance) => ({
    getList: (query: any) => {
        return $axios.$get(`${resources.Combo}/list-combo?${objectToParam(query)}`)
    },
    getListAdmin: (query: any) => {
        return $axios.$get(`${resources.Combo}/admin/list-combo?${objectToParam(query)}`)
    },
    create: (payload: any) => {
        return $axios.$post(`${resources.Combo}/create-combo`, payload)
    },
    edit: (comboId: string, payload: any) => {
        return $axios.$put(`${resources.Combo}/edit-combo/${comboId}`, payload)
    },
    delete: (comboId: string) => {
        return $axios.$delete(`${resources.Combo}/delete-combo/${comboId}`)
    },
    deleteList: (listComboId: string[]) => {
        return $axios.$post(`${resources.Combo}/delete-list-combo`, { listComboId })
    }
})

export default comboApi
export type ComboApiType = ReturnType<typeof comboApi>