import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const orderApi = ($axios: NuxtAxiosInstance) => ({
  getMyOrder: (orderCode: string) => {
    return $axios.$get(`${resources.Order}/my-order/${orderCode}`);
  },
  getAdminMyOrder: (orderId: string) => {
    return $axios.$get(`${resources.OrderAdmin}/admin/get-order/${orderId}`);
  },
  updateOrder: (body: any) => {
    return $axios.$post(`${resources.OrderAdmin}/update`, body);
  },
  updateCustomerInfo: (orderId: string, body: any) => {
    return $axios.$put(`${resources.OrderAdmin}/change-shipping/${orderId}`, {
      shippingAddress: body,
    });
  },
  getListOrder: (query: any) => {
    return $axios.$get(`${resources.OrderAdmin}/list?${objectToParam(query)}`);
  },
  getListExportOrder: (query: any) => {
    return $axios.$get(
      `${resources.OrderAdmin}/list-export?${objectToParam(query)}`
    );
  },
  adminGetOrder: (orderId: string) => {
    return $axios.$get(`${resources.OrderAdmin}/detail/${orderId}`);
  },
  adminCreateTimeLine: (orderId: string, payload: any) => {
    return $axios.$post(
      `${resources.OrderAdmin}/add-timeline/${orderId}`,
      payload
    );
  },
  adminDeleteTimeLine: (orderId: string, timelineId: any) => {
    return $axios.$delete(
      `${resources.OrderAdmin}/delete-timeline/${orderId}?timelineId=${timelineId}`
    );
  },
  adminAddTracking: (body: any) => {
    return $axios.$post(`${resources.OrderAdmin}/add-tracking`, {
      trackings: body,
    });
  },
  adminChangeTracking: (body: any) => {
    return $axios.$post(`${resources.OrderAdmin}/change-tracking`, {
      trackings: body,
    });
  },
  adminGetListTracking: (query: any) => {
    return $axios.$get(
      `${resources.OrderAdmin}/tracking-history?${objectToParam(query)}`
    );
  },
});

export default orderApi;
export type OrderApiType = ReturnType<typeof orderApi>;
