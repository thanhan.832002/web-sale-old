import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { IQuery } from "~/interfaces/common";

const userAdmin = ($axios: NuxtAxiosInstance) => ({
    listAdmin: (query: IQuery) => {
        return $axios.$get(`${resources.userAdmin}/list-admin?${objectToParam(query)}`)
    },
    listSeller: (query: IQuery) => {
        return $axios.$get(`${resources.userAdmin}/list-seller?${objectToParam(query)}`)
    },
    activeUser: (userId: string) => {
        return $axios.$put(`${resources.userAdmin}/active-user`, {
            userId
        })
    },
    deactiveUser: (userId: string) => {
        return $axios.$put(`${resources.userAdmin}/deactive-user`, {
            userId
        })
    },
    createSeller: (body: any) => {
        return $axios.$post(`${resources.userAdmin}/create-seller`, body)
    },

    //seller
    sellerDetail: (sellerId: string) => {
        return $axios.$get(`${resources.userAdmin}/detail-seller/${sellerId}`)
    },
})

export default userAdmin
export type UserAdminType = ReturnType<typeof userAdmin>