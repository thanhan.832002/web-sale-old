import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { resources } from "~/interfaces/api";

const variantApi = ($axios: NuxtAxiosInstance) => ({
    create: (payload: any) => {
        return $axios.$post(`${resources.Variant}/create`, payload)
    },
    edit: (variantId: string, payload: any) => {
        return $axios.$put(`${resources.Variant}/edit-variant/${variantId}`)
    },
    delete: (variantId: string) => {
        return $axios.$delete(`${resources.Variant}/delete-variant/${variantId}`)
    },
    list: (productId: string) => {
        return $axios.$get(`${resources.Variant}/list-variant-by-product/${productId}`)
    },
})

export default variantApi
export type VariantApiType = ReturnType<typeof variantApi>