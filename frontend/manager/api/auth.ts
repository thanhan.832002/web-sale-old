import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { IAuth } from "~/interfaces/auth";

const authApi = ($axios: NuxtAxiosInstance) => ({
    login: (payload: IAuth) => {
        return $axios.$post(`/auth/login`, payload)
    },

    register: (payload: IAuth) => {
        return $axios.$post(`/auth/register`, payload)
    },

    renewToken: (refreshToken: string) => {
        return $axios.$post(`/auth/renew-token`, refreshToken)
    }
})

export default authApi
export type AuthApiType = ReturnType<typeof authApi>