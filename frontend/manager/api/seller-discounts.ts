import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { resources } from "~/interfaces/api";
import { objectToParam } from "~/common/utils";
const discountsApi = ($axios: NuxtAxiosInstance) => ({
    list: (query: any) => {
        return $axios.$get(`${resources.discounts}/list?${objectToParam(query)}`);
    },
    addDiscount: (body: any) => {
        return $axios.$post(`${resources.discounts}/create`, body)
    },
    edit: (payload: any) => {
        return $axios.$put(`${resources.discounts}/update`,
            payload
        )
    },
    delete: (tagId: string) => {
        return $axios.$delete(`${resources.discounts}/delete/${tagId}`)
    },
})

export default discountsApi
export type DiscountsApiType = ReturnType<typeof discountsApi>