import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { IQuery } from "~/interfaces/common";

const paymentApi = ($axios: NuxtAxiosInstance) => ({
    list: (query: IQuery) => {
        return $axios.$get(`${resources.Payment}/list?${objectToParam(query)}`)
    },

    create: (payload: any) => {
        return $axios.$post(`${resources.Payment}/create`, payload)
    },
    update: (paymentId: string, payload: any) => {
        return $axios.$put(`${resources.Payment}/edit/${paymentId}`, payload)
    },
    delete: (paymentId: string) => {
        return $axios.$delete(`${resources.Payment}/delete/${paymentId}`)
    },
    detail: (paymentId: string) => {
        return $axios.$get(`${resources.Payment}/detail/${paymentId}`)
    },
    stats: (paymentId: string) => {
        return $axios.$get(`${resources.Payment}/stats/${paymentId}`)
    },
    statsAdmin: (paymentId: string) => {
        return $axios.$get(`stats-admin/payment/${paymentId}`)
    },
    lisAdmintSeller: (paymentId: string, query: IQuery) => {
        return $axios.$get(`${resources.Payment}/list-seller/${paymentId}?${objectToParam(query)}`)
    },

    attach: (body: any) => {
        return $axios.$put(`${resources.Payment}/attach`, body)
    },

    deleteUserPayment: (userPaymentId: string) => {
        return $axios.$delete(`${resources.Payment}/delete-seller-payment/${userPaymentId}`)
    },


    userListPayment: () => {
        return $axios.$get(`${resources.Payment}/list-payment`)
    },
    adminListPayment: () => {
        return $axios.$get(`${resources.Payment}/admin/list-payment`)
    },
    updatePayment: (body: any) => {
        return $axios.$post(`${resources.Payment}/admin/update-payment`, body)
    },

    // Seller-Payment

    sellerListPayment: (type?: string) => {
        return $axios.$get(`${resources.SellerPayment}/list`)
    },
    sellerTogglePayment: (sellerPaymentId: string) => {
        return $axios.$post(`${resources.SellerPayment}/toggle/${sellerPaymentId}`)
    },

    sellerSettingTogglePaypalCard: (sellerPaymentId: string) => {
        return $axios.$put(`${resources.SellerPayment}/toggle-paypal-card/${sellerPaymentId}`)
    },

})

export default paymentApi
export type PaymentApiType = ReturnType<typeof paymentApi>