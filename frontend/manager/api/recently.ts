import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const recentlyApi = ($axios: NuxtAxiosInstance) => ({
    get: () => {
        return $axios.$get(`${resources.recentlyView}/status`)
    },
    edit: (status: boolean) => {
        return $axios.$put(`${resources.recentlyView}/edit`, { status })
    }
})

export default recentlyApi
export type RecentlyApiType = ReturnType<typeof recentlyApi>