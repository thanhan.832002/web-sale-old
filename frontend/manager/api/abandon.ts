import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const abandonApi = ($axios: NuxtAxiosInstance) => ({
  get: () => {
    return $axios.$get(`${resources.abandon}/list`);
  },
  edit: (body: any) => {
    return $axios.$put(`${resources.abandon}/edit`, body);
  },
});

export default abandonApi;
export type AbandonApiType = ReturnType<typeof abandonApi>;
