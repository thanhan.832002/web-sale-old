import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { IAuth } from "~/interfaces/auth";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { IChangeGalleryTag, ICreateUpdateTag } from "~/interfaces/tag";

const galleryApi = ($axios: NuxtAxiosInstance) => ({
  getList: (tagId: string) => {
    return $axios.$get(`${resources.Gallery}/list?tagId=${tagId}`);
  },
  update: (payload: IChangeGalleryTag) => {
    return $axios.$put(`${resources.Gallery}/change-gallery-tag`, payload);
  },
  delete: (body: any) => {
    return $axios.$put(`${resources.Gallery}/delete`, body);
  },

  //Tag
  getListTag: (query: any) => {
    return $axios.$get(`${resources.Tag}/list?${objectToParam(query)}`);
  },
  createTag: (name: string) => {
    return $axios.$post(`${resources.Tag}/create`, {
      name,
    });
  },
  updateTag: (tagId: string, name: string) => {
    return $axios.$put(`${resources.Tag}/edit/${tagId}`, {
      name,
    });
  },
  deleteTag: (tagId: string) => {
    return $axios.$delete(`${resources.Tag}/delete/${tagId}`);
  },
});

export default galleryApi;
export type GalleryApiType = ReturnType<typeof galleryApi>;
