import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { resources } from "~/interfaces/api";
import { objectToParam } from "~/common/utils";
const adminDiscountsApi = ($axios: NuxtAxiosInstance) => ({
    list: (query: any) => {
        return $axios.$get(`${resources.AdminDiscount}/list?${objectToParam(query)}`);
    },
    addDiscount: (body: any) => {
        return $axios.$post(`${resources.AdminDiscount}/create`, body)
    },
    getDetail: (discountId: string) => {
        return $axios.$get(`${resources.AdminDiscount}/detail/${discountId}`);
    },
    edit: (payload: any) => {
        return $axios.$put(`${resources.AdminDiscount}/update`,
            payload
        )
    },
    delete: (tagId: string) => {
        return $axios.$delete(`${resources.AdminDiscount}/delete/${tagId}`)
    },
})

export default adminDiscountsApi
export type AdminDiscountsApiType = ReturnType<typeof adminDiscountsApi>