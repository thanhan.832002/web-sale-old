import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const userApi = ($axios: NuxtAxiosInstance) => ({
    getMe: () => {
        return $axios.$get(`${resources.User}/me`)
    },
    updateInfo: (body: any) => {
        return $axios.$put(`${resources.User}/update`, body)
    },
    getAllAdmin: (query: any) => {
        return $axios.$get(`${resources.User}/all-admin?${objectToParam(query)}`)
    },
    forgotPassword: (email: string, domain: string) => {
        return $axios.$put(`${resources.User}/forgot-password`, {
            email,
            domain
        })
    },
    changePassword: (payload: any) => {
        return $axios.$put(`${resources.User}/change-password`, payload)
    },
    createNewPassword: (body: any) => {
        return $axios.$put(`${resources.User}/create-new-password`, body)
    },
    masterGetAllUser: (query: any) => {
        return $axios.$get(`${resources.User}/master/all-user?${objectToParam(query)}`)
    },
    masterActiveUser: (userId: string, isActive: boolean) => {
        return $axios.$post(`${resources.User}/master/active/${userId}`, { isActive })
    },

    turnOn2Fa: (password: string) => {
        return $axios.$post(`${resources.User}/turn-on-2fa`, { password })
    },
    verify2Fa: (code: string, password: string) => {
        return $axios.$post(`${resources.User}/verify-2fa`, { code, password })
    },
    turnOff2Fa: (password: string) => {
        return $axios.$post(`${resources.User}/turn-off-2fa`, { password })
    },
})

export default userApi
export type UserApiType = ReturnType<typeof userApi>