import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { IShipping, ISellerStoreCheckDomain, IStoreProductDisplay, IStoreUpsale, ISellerStoreItemShipping } from "~/interfaces/store";
import axios from "~/plugins/axios";

const storeApi = ($axios: NuxtAxiosInstance) => ({
    create: (body: any) => {
        return $axios.$post(`${resources.store}/create`, body)
    },
    list: (query?: any) => {
        return $axios.$get(`${resources.store}/list?${objectToParam(query)}`)
    },
    detail: (storeId: any) => {
        return $axios.$get(`${resources.store}/detail/${storeId}`)
    },
    updateOverView: (storeId: string, payload: any) => {
        return $axios.$put(`${resources.store}/update-overview/${storeId}`,
            payload
        )
    },
    updateVariant: (storeId: string, variantSellerId: string, payload: any) => {
        return $axios.$put(`${resources.store}/update-variant/${storeId}/${variantSellerId}`,
            payload
        )
    },

    changeStatus: (body: any) => {
        return $axios.$put(`${resources.store}/change-status`, body)
    },

    postMedia: (storeId: string, body: any) => {
        return $axios.$post(`${resources.store}/media/${storeId}/add-image`, body)
    },
    delete: (storeId: string, body: any) => {
        return $axios.$put(`${resources.store}/media/${storeId}/delete-image`, body)
    },
    sortImage: (storeId: string, body: any) => {
        return $axios.$put(`${resources.store}/media/${storeId}/sort-image`, body)
    },
    variant: (body: any, storeId: string) => {
        return $axios.$put(`${resources.store}/update-list-variant/${storeId}`, { listVariant: body })
    },

    // Seller Store
    sellerStoreListPayment: (storeId: string) => {
        return $axios.$get(`${resources.store}/list-payment/${storeId}`)
    },
    sellerStoreTogglePayment: (storePaymentId: string) => {
        return $axios.$put(`${resources.storePayment}/toggle/${storePaymentId}`)
    },
    sellerStoreTogglePaypalCard: (storePaymentId: string) => {
        return $axios.$put(`${resources.storePayment}/toggle-paypal-card/${storePaymentId}`)
    },
    sellerStoreProductDisplay: (storeId: string, body: IStoreProductDisplay) => {
        return $axios.$put(`${resources.store}/product-display/${storeId}/update`, body)
    },
    sellerStoreCheckDomain: (query: ISellerStoreCheckDomain) => {
        return $axios.$get(`${resources.store}/check-domain?${objectToParam(query)}`)
    },

    // Seller Store Upsale
    listStoreUpsale: (storeId: string) => {
        return $axios.$get(`${resources.storeUpsale}/${storeId}`)
    },
    updateStoreUpsale: (storeId: string, payload: IStoreUpsale) => {
        return $axios.$put(`${resources.storeUpsale}/update/${storeId}`, payload)
    },


    // Seller Store Shipping
    detailStoreShipping: (storeId: string) => {
        return $axios.$get(`${resources.storeShipping}/detail/${storeId}`)
    },
    updateStoreShipping: (storeId: string, body: IShipping<ISellerStoreItemShipping>) => {
        return $axios.$put(`${resources.storeShipping}/edit/${storeId}`, body)
    }
})

export default storeApi
export type StoreApiType = ReturnType<typeof storeApi>