import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const reviewApi = ($axios: NuxtAxiosInstance) => ({
  get: (storeId: any) => {
    return $axios.$get(`${resources.review}/list/${storeId}`);
  },
  delete: (storeId: any, reviewId: any) => {
    return $axios.$delete(`${resources.review}/delete/${storeId}/${reviewId}`);
  },
  approve: (storeId: any, reviewId: any, body: any) => {
    return $axios.$put(
      `${resources.review}/approved/${storeId}/${reviewId}`,
      body
    );
  },
  edit: (storeId: any, reviewId: any, body: any) => {
    return $axios.$put(`${resources.review}/edit/${storeId}/${reviewId}`, body);
  },
  post: (storeId: any, body: any) => {
    return $axios.$post(`${resources.review}/create/${storeId}`, body);
  },

  AdminGet: (productId: any) => {
    return $axios.$get(`${resources.productReview}/list/${productId}`);
  },
  AdminDelete: (productId: any, reviewId: any) => {
    return $axios.$delete(
      `${resources.productReview}/delete/${productId}/${reviewId}`
    );
  },
  AdminPost: (productId: any, body: any) => {
    return $axios.$post(
      `${resources.productReview}/create-review/${productId}`,
      body
    );
  },
});

export default reviewApi;
export type ReviewApiType = ReturnType<typeof reviewApi>;
