import { filter } from "lodash";
import { redirectPage } from "~/utils/apiHanlers";

export default async function (ctx) {
  const { store, redirect, app, route } = ctx
  let isLogin = store.getters["user/isLogin"];
  const timezone = store.state.setting?.siteInfo?.timezone || ""
  const role = store.state.user.role;
  const path = route.path.split("/").filter((path) => path)[0];
  if (!isLogin) {
    return redirect("/auth/login");
  } 
  if (role === 1 && path === "seller") {
    redirect("/admin/dashboard");
  } else if (role === 2 && path === "admin") {
    redirect("/seller/dashboard");
  } else if (role === 0 && (path === "admin" || path === "seller")) {
    redirect("/master/manage-user");
  } else if(role !== 0 && path === 'master'){
    if(role === 1){
    redirect("/admin/dashboard");
    }
    if(role === 2){
    redirect("/seller/dashboard");
    }
  }
  if(isLogin && !timezone){
   await store.dispatch('user/onGetSettingPage', {ctx})
  }
}
