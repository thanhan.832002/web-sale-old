import { redirectPage } from "~/utils/apiHanlers";

export default async function ({ store, redirect, app }) {
  let isLogin = store.getters["user/isLogin"];
  let role = store.state.user.role;
  if (isLogin) {
    if (role === 1) {
      redirect("/admin/dashboard");
    }
    if (role === 2) {
      redirect("/seller/dashboard");
    } else {
      redirect("/master/manage-user");
    }
  }
}

// export const notAuthenticated = (isLogin) => {
//     if (isLogin) {
//         return redirectPage(`/admin/dashboard`);
//     }
// }
