export interface IItemMail {
  adminId: string;
  couponId: string;
  createdAt: string;
  discount: number;
  emailSubject: string;
  emailTemplate: string;
  time: number;
  timestamp: number;
  updatedAt: string;
  __v: number;
  _id: string;
}
