export interface IQuery {
  page: number;
  limit: number;
  search: string;
  from?: string;
  to?: string;
  timezone?: string;
}

export interface IPayment {
  name: string;
  type: string;
  mode?: string;
  enabled: boolean;
  publicKey: string;
  secretKey: string;
  policy?: {
    refund: string;
    privacy: string;
    shipping: string;
    terms: string;
    faqs: string;
    aboutUs: string;
    dmca: string;
    cancelAndOrderChange: string;
    paymentMethod: string;
  };
}
