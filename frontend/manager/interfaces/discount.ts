export interface IItemListDiscount {
    discountType: string | number
    discount: number
    minAmount: number
    minQuantity: number
    status: boolean | string,
    code?: string
    dateFrom?: string;
    dateTo?: string;
}

export interface ISellerDiscount extends IItemListDiscount {
    storeId: string
    couponId?: string | null
    multipleReq?: IItemListDiscount[]
}

export interface IItemListUpsale {
    quantity: number
    content: number
    discount: number
}