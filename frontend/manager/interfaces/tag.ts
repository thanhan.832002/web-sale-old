export interface ICreateUpdateTag {
    name: string,
    desc: string
}

export interface IChangeGalleryTag {
    tagId: string,
    listGalleryId: string[]
}