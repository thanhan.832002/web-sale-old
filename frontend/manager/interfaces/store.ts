export interface IStoreProductDisplay {
    peopleViewing: number,
    purchased: number,
    lastMinute: number
}

export interface IStoreUpsale {
    sellerDiscountId: string,
    listStoreId: string[],
    status: boolean
}

export interface ISellerStoreCheckDomain {
    domain: string
    storeId?: string
}

export interface ISellerStoreItemShipping {
    price: number
    quantity: number
    total: number
}

export interface IShipping<T> {
    name: string,
    listPrice: T
}