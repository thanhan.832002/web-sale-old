export interface IDomain {
    domainId?: string
    domain?: string,
    enabled?: boolean,
    isVerifiedMail?: boolean
    supportEmail?: string
}

export interface IResponseDomain {
    createdAt: string
    domain: string
    enabled: boolean
    id: string
    supportEmail: string
    updatedAt: string
    _id: string
    isVerifiedMail: boolean
}

export interface ISSLInfo {
    type: string
    cname_validation_p1: string
    cname_validation_p2: string
}

export interface IDomainInfo {
    name: string
    content: string
    type: string
}

export enum IActionDomain {
    ADD_SUPPORTEMAIL = "add-support-email",
    EDIT_DOMAIN = "edit-domain",
    EDIT_EMAIL = "edit-email",
    ACTIVE_DOMAIN = "active-domain",
    VERIFY_EMAIL = "verify-email",
    RESEND_EMAIL = "resend-email",
    DELETE_DOMAIN = "delete-domain",
    ENABLE_SSL = "enable-ssl"
}