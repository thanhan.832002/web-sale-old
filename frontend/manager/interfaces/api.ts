import { AuthApiType } from "~/api/auth";
import { HomeApiType } from "~/api/home";
import { GalleryApiType } from "~/api/gallery";
import { CollectionApiType } from "~/api/collection";
import { ProductApiType } from "~/api/product";
import { VariantApiType } from "~/api/variant";
import { PromotionApiType } from "~/api/promotion";
import { ComboApiType } from "~/api/combo";
import { ShippingApiType } from "~/api/shipping";
import { CouponApiType } from "~/api/coupon";
import { OrderApiType } from "~/api/order";
import { UserApiType } from "~/api/user";
import { UserMasterType } from "~/api/userMaster";
import { SettingApiType } from "~/api/settings";
import { StatsApiType } from "~/api/stats";
import { PaymentApiType } from "~/api/payment";
import { AdsAnalyticApiType } from "~/api/ads-analytic";
import { BestSellerApiType } from "~/api/best-seller";
import { RecentlyApiType } from "~/api/recently";
import { AbandonApiType } from "~/api/abandon";
import { UserPaymentAdminType } from "~/api/user-payment-admin";
import { UserAdminType } from "~/api/user-admin";
import { StoreAdminType } from "~/api/store-admin";

import { ProductSellerApiType } from "~/api/product-seller";
import { StoreApiType } from "~/api/store";
import { StoreGalleryApiType } from "~/api/storeGallery";
import { PixelApiType } from "~/api/pixel";
import { ReviewApiType } from "~/api/review";
import { SellerOrderType } from "~/api/sellerOrder";
import { CartApiType } from "~/api/cart";
import { DomainSellerApiType } from "~/api/domain-seller";
import { AdminSendMailType } from "~/api/mail";
import { DomainApiType } from "~/api/domain";
import { DiscountsApiType } from "~/api/discounts";
import { AdminDiscountsApiType } from "~/api/admin-discount";
export interface IApiPlugin {
  userMaster: UserMasterType;
  discounts: DiscountsApiType;
  AdminDiscount: AdminDiscountsApiType;
  domainSeller: DomainSellerApiType;
  domain: DomainApiType;
  pixel: PixelApiType;
  home: HomeApiType;
  auth: AuthApiType;
  gallery: GalleryApiType;
  collection: CollectionApiType;
  product: ProductApiType;
  variant: VariantApiType;
  promotion: PromotionApiType;
  combo: ComboApiType;
  shipping: ShippingApiType;
  coupon: CouponApiType;
  order: OrderApiType;
  payment: PaymentApiType;
  userPaymentAdmin: UserPaymentAdminType;
  userAdmin: UserAdminType;
  user: UserApiType;
  setting: SettingApiType;
  stats: StatsApiType;
  adsAnalytic: AdsAnalyticApiType;
  bestSeller: BestSellerApiType;
  recently: RecentlyApiType;
  abandon: AbandonApiType;
  storeAdmin: StoreAdminType;
  productSeller: ProductSellerApiType;
  store: StoreApiType;
  storeGallery: StoreGalleryApiType;
  sellerOrder: SellerOrderType;
  review: ReviewApiType;
  cart: CartApiType;
  adminSendMail: AdminSendMailType;
}

export enum resources {
  userMaster = "user-master",
  StoreBuyer = "store-buyer",
  PaymentBuyer = "payment-buyer",
  discounts = "seller-discount",
  AdminDiscount = "discount",
  SellerDiscount = "discount",
  domainSeller = "domain-seller",
  domain = "domain",
  review = "review-seller",
  productReview = "product-review",
  pixel = "pixel",
  storeGallery = "store-gallery",
  Tag = "tag",
  Gallery = "gallery",
  Collection = "collection",
  Category = "category",
  Product = "product",
  Variant = "variant",
  VariantOption = "variant-option",
  Promotion = "promotion",
  Combo = "combo",
  Shipping = "shipping",
  Coupon = "coupon",
  Order = "order",
  OrderAdmin = "order-admin",
  OrderSeller = "order-seller",
  Payment = "payment",
  SellerPayment = "seller-payment",
  User = "user",
  Setting = "setting",
  AdminSetting = "admin-setting",
  HomePageSetting = "homepage-setting",
  Stats = "stats",
  StatsAdmin = "stats-admin",
  StatsMaster = "stats-master",
  StatsSeller = "stats-seller",
  adsAnalytic = "analytics",
  bestSeller = "best-seller",
  recentlyView = "recently-view",
  abandon = "abandoned-checkout",
  userPaymentAdmin = "seller-payment-admin",
  userAdmin = "user-admin",
  storeAdmin = "store-admin",
  productSeller = "product-seller",
  store = "store",
  storeUpsale = "store-upsale",
  storePayment = "store-payment",
  storeShipping = "store-shipping",
  Cart = "cart",
  AdminSendMail = "test-email",
}
