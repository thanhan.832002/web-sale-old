export interface IShippingListPrice {
  name: string;
  price: number;
  quantity: number;
  _id?: string
}

export interface ICountry {
  name: string;
  code: string;
}

export interface IShipping {
  name: string;
  desc: string;
  listPrice: IShippingListPrice[];
  listCountry: string[];
}
