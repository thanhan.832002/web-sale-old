export interface IItemListTrackingExport {
    message: string,
    orderNumber: string,
    transaction_id: string,
    type: string
}