export const accessTokenKey = "accessToken";
export const refreshTokenKey = "refreshToken";
export const userInfoKey = "userInfo";

// export const api = 'http://localhost:2000'
// export const api = "http://192.168.1.154:2000";
export const api =
  process.env.NODE_ENV === "development"
    ? "http://localhost:2000"
    : "https://api.opensitex.com";
export const apiLocal = api;
export const api_key_tiny_editor =
  "cckdt047mysl0hut717e97kcjnr4cyynjx7m0i8xwx5osgw3";

export const regexDomain =
  /^[a-zA-Z0-9][a-zA-Z0-9-.]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/;

export const regexEmail =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
