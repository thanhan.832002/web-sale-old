export const parseTxtText = (data) => {
    if (data && data.mail_cname) {
        const length = data?.dkim1?.host.length + 5 || 0
        for (let i = 0; i < length - data.mail_cname.host.length; i++) {
            data.mail_cname.host += ' '
        }
        const txt = `;; CNAME Records\n${data?.dkim1?.host + '. 1' || ""}	IN	CNAME	${data?.dkim1?.data || ""}\n${data?.dkim2?.host + '. 1' || ""}	IN	CNAME	${data?.dkim2?.data || ""}\n${data?.mail_cname?.host + '. 1' || ""}	IN	CNAME	${data?.mail_cname?.data || ""}\n`
        return txt
    }
}