export const listTemplate = [
  {
    name: "order confirmation",
    template: `<html><head>
        <title></title>
          <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,600,600i,700,800|Raleway:400,700,900,700&amp;display=swap" rel="stylesheet" type="text/css">
      </head>
      <body>
        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width:100%;max-width:600px" align="center">
          <tbody>
            <tr>
              <td style="padding:0px 0px 0px 0px;color:#000000;text-align:left" bgcolor="#FFFFFF" width="100%" align="left">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="display:none!important;opacity:0;color:transparent;height:0;width:0">
                  <tbody>
                    <tr>
                      <td>
                        <p>Thank you for your purchase</p>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td height="100%" valign="top">
                        <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin-bottom:30px">
                          <tbody>
                            <tr>
                            
                              <td style="background:#f7f7f7;padding:10px 0 10px 0;color:#ffffff" align="center">
                                <img style="height:50px" src="https://cdn.wtecdn.net/files/40b9ec540571696b74f026dca2545b55/ExSecretLogo.png">
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td height="100%" valign="top">
                        <div style="font-family:inherit">
                          <h1 style="font-size:18px;margin:0;font-family:sans-serif">
                            Hi Maria,
                          </h1>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit" height="100%" valign="top" bgcolor="">
                        <div>
                          <div style="font-family:inherit;text-align:inherit"><span style="color:#000000;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:normal;letter-spacing:normal;text-align:left;text-indent:0px;text-transform:none;word-spacing:0px;text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;font-family:arial,helvetica,sans-serif;font-size:15px">Thank you for shopping with us! Please find your order details below.</span></div>
                          <br>
                          <div style="font-family:inherit;text-align:inherit"><span style="color:#000000;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:left;text-indent:0px;text-transform:none;word-spacing:0px;background-color:rgb(255,255,255);text-decoration-style:initial;text-decoration-color:initial;float:none;display:inline;font-family:arial,helvetica,sans-serif;font-size:15px">Please kindly note that all orders are processed after 6 hours of order confirmation receipt. Once processed, no further changes could be made.</span></div>                    
                          <br>
                          <div style="font-family:inherit;text-align:inherit"><span style="color:#000000;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;text-align:left;text-indent:0px;text-transform:none;word-spacing:0px;background-color:rgb(255,255,255);text-decoration-style:initial;text-decoration-color:initial;float:none;display:inline;font-family:arial,helvetica,sans-serif;font-size:15px">If you're not satisfied about anything from our service. Please don't hesitate to email us at  support@#. We'll do our best to help you have a good buying experience.</span>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td height="100%" valign="top">
                        <div style="display:flex">
                          <h3 style="font-size:15px;font-weight:bold;text-transform:uppercase;width:100%;font-family:sans-serif">
                            ORDER #0123
                          </h3>
                        </div>
                        <hr style="border-width:1px 0 0 0;border-style:solid;border-color:#dddddd">
                        <br>
                        <table border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
                          <tbody>
                            
                            <tr>
                              <td style="vertical-align:middle" align="left" width="17%"><a style="display:block" title="" href="#" target="_blank"> <img style="display:block;font-family:Arial;width:80px" src="https://cdn.wtecdn.net/files/22b6c2e2f361744e5639db02faf332b4/set3_gray.jpg" alt="" width="80" border="0"> </a></td>
                              <td style="padding:0 20px 0 20px;vertical-align:top" align="left" width="83%">
                                <p style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif"><a style="text-decoration:none" title="ErgoStool" href="#" rel="noopener" target="_blank">ExSecret - Ultimate Lift Stretch Full-Figure Seamless Lace Cut-Out Bra</a></p>
                                <p style="font-size:15px;line-height:26px;color:#555;margin:0;font-family:sans-serif">80% Off Set 3 (Black &amp; Beige &amp; Gray)/2XL</p>
                                <p style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif"><span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$64.95</span> &nbsp;x&nbsp; <span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">1</span></p>
                              </td>
                              <td style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">$64.95</td>
                            </tr>
                            
                            <tr>
                              <td style="vertical-align:middle" align="left" width="17%"><a style="display:block" title="" href="#" target="_blank"> <img style="display:block;font-family:Arial;width:80px" src="https://cdn.wtecdn.net/files/22b6c2e2f361744e5639db02faf332b4/set3_gray.jpg" alt="" width="80" border="0"> </a></td>
                              <td style="padding:0 20px 0 20px;vertical-align:top" align="left" width="83%">
                                <p style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif"><a style="text-decoration:none" title="ErgoStool" href="#" rel="noopener" target="_blank">ExSecret - Ultimate Lift Stretch Full-Figure Seamless Lace Cut-Out Bra</a></p>
                                <p style="font-size:15px;line-height:26px;color:#555;margin:0;font-family:sans-serif">80% Off Set 3 (Black &amp; Beige &amp; Gray)/2XL</p>
                                <p style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif"><span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$64.95</span> &nbsp;x&nbsp; <span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">1</span></p>
                              </td>
                              <td style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">$64.95</td>
                            </tr>
                            
                            <tr>
                              <td style="vertical-align:middle" align="left" width="17%"><a style="display:block" title="" href="#" target="_blank"> <img style="display:block;font-family:Arial;width:80px" src="https://cdn.wtecdn.net/files/22b6c2e2f361744e5639db02faf332b4/set3_gray.jpg" alt="" width="80" border="0"> </a></td>
                              <td style="padding:0 20px 0 20px;vertical-align:top" align="left" width="83%">
                                <p style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif"><a style="text-decoration:none" title="ErgoStool" href="#" rel="noopener" target="_blank">ExSecret - Ultimate Lift Stretch Full-Figure Seamless Lace Cut-Out Bra</a></p>
                                <p style="font-size:15px;line-height:26px;color:#555;margin:0;font-family:sans-serif">80% Off Set 3 (Black &amp; Beige &amp; Gray)/2XL</p>
                                <p style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif"><span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$64.95</span> &nbsp;x&nbsp; <span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">1</span></p>
                              </td>
                              <td style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">$64.95</td>
                            </tr>
                            
                            <tr>
                              <td style="vertical-align:middle" align="left" width="17%"><a style="display:block" title="" href="#" target="_blank"> <img style="display:block;font-family:Arial;width:80px" src="https://cdn.wtecdn.net/files/22b6c2e2f361744e5639db02faf332b4/set3_gray.jpg" alt="" width="80" border="0"> </a></td>
                              <td style="padding:0 20px 0 20px;vertical-align:top" align="left" width="83%">
                                <p style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif"><a style="text-decoration:none" title="ErgoStool" href="#" rel="noopener" target="_blank">ExSecret - Ultimate Lift Stretch Full-Figure Seamless Lace Cut-Out Bra</a></p>
                                <p style="font-size:15px;line-height:26px;color:#555;margin:0;font-family:sans-serif">80% Off Set 3 (Black &amp; Beige &amp; Gray)/2XL</p>
                                <p style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif"><span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$64.95</span> &nbsp;x&nbsp; <span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">1</span></p>
                              </td>
                              <td style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">$64.95</td>
                            </tr>
                            
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td height="100%" valign="top">
                        <br>
                        <table border="0" width="60%" cellspacing="0" cellpadding="0" align="right">
                          <tbody>
                            <tr>
                              <td colspan="2">
                                <hr style="border-width:1px 0 0 0;border-style:solid;border-color:#dddddd">
                              </td>
                            </tr>
                            <tr>
                              <td style="padding:0 20px 0 20px;vertical-align:top" align="left" width="86%">
                                <p style="font-size:15px;line-height:26px;color:#222;font-weight:300;margin:0">Subtotal</p>
                              </td>
                              <td style="font-size:15px;line-height:26px;color:#222;margin:0;font-weight:600;text-align:right">$129.90</td>
                            </tr>
                            <tr>
                              <td style="padding:0 20px 0 20px;vertical-align:top" align="left" width="86%">
                                <p style="font-size:15px;line-height:26px;color:#222;font-weight:300;margin:0">
                                  
                                    Shipping (Free)
                                  
                                </p>
                              </td>
                              <td style="font-size:15px;line-height:26px;color:#222;margin:0;font-weight:600;text-align:right">$0.00</td>
                            </tr>
                            <tr>
                              <td style="padding:0 20px 0 20px;vertical-align:top" align="left" width="86%">
                                <p style="font-size:15px;line-height:26px;color:#222;font-weight:300;margin:0">Discount (FIN)</p>
                              </td>
                              <td style="font-size:15px;line-height:26px;color:#222;margin:0;font-weight:600;text-align:right">- $0.00</td>
                            </tr>
                            
                            <tr>
                              <td colspan="2">
                                <hr style="border-width:1px 0 0 0;border-style:solid;border-color:#dddddd">
                              </td>
                            </tr>
                            <tr>
                              <td style="padding:0 20px 0 20px;vertical-align:top" align="left" width="86%">
                                <p style="font-size:15px;line-height:26px;color:#222;font-weight:400;margin:0">Total Paid</p>
                              </td>
                              <td style="font-size:24px;line-height:26px;color:#222;margin:0;font-weight:600;text-align:right">$129.90</td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td height="100%" valign="top">
                        <br>
                        <hr style="border-width:1px 0 0 0;border-style:solid;border-color:#dddddd">
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="padding:0px 0px 0px 0px" bgcolor="#FFFFFF">
                  <tbody>
                    <tr style="display: flex;">
                      <td height="100%" valign="top" style="flex: 1;display: flex; margin-right: 5px;">
                        <table style="width:100%;border-spacing:0;border-collapse:collapse;margin:0px 0px 0px 0px" cellpadding="0" cellspacing="0" align="right" border="0" bgcolor="">
                          <tbody>
                            <tr>
                              <td style="padding:0px;margin:0px;border-spacing:0">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                                  <tbody>
                                    <tr>
                                      <td height="100%" valign="top">
                                        <h5 style="font-size:15px;font-weight:bold;text-transform:uppercase;margin:14px 0 10px;font-family:sans-serif">SHIPPING ADDRESS</h5>
                                        <p style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">Elona Maria</p>
                                        <p style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif"><a href="mailto:elonan@example.com" rel="noopener" target="_blank">elonan@example.com</a></p>
                                        <p style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">Main St, San Jose, California 95131, United States</p>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td height="100%" valign="top" style="flex: 1;display: flex; margin-right: 5px;">
                        <table style="width:100%;border-spacing:0;border-collapse:collapse;margin:0px 0px 0px 10px" cellpadding="0" cellspacing="0" align="right" border="0" bgcolor="">
                          <tbody>
                            <tr>
                              <td style="padding:0px;margin:0px;border-spacing:0">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                                  <tbody>
                                    <tr>
                                      <td height="100%" valign="top">
                                        <h5 style="font-size:15px;font-weight:bold;text-transform:uppercase;margin:14px 0 10px;font-family:sans-serif">BILLING ADDRESS</h5>
                                        <p style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">Jenifer N</p>
                                        <p style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif"><a href="mailto:elonan.san@example.com" rel="noopener" target="_blank">elonan.san@example.com</a></p>
                                        <p style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">Main St, San Jose, California 95131, Canada</p>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      
    <div class="ddict_div" style="top: 103px; left: 304.516px;"><p style="text-align: center;"><img src="chrome-extension://bpggmmljdiliancllaapiggllnkbjocb/img/spin.gif"></p></div></body></html>`,
  },
  {
    name: "order change billing",
    template: `<html><head>
        <title></title>
          <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,600,600i,700,800|Raleway:400,700,900,700&amp;display=swap" rel="stylesheet" type="text/css">
      </head>
      <body>
        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width:100%;max-width:600px" align="center">
          <tbody>
            <tr>
              <td style="padding:0px 0px 0px 0px;color:#000000;text-align:left" bgcolor="#FFFFFF" width="100%" align="left">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="display:none!important;opacity:0;color:transparent;height:0;width:0">
                  <tbody>
                    <tr>
                      <td>
                        <p>Thank you for your purchase</p>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td height="100%" valign="top">
                        <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin-bottom:30px">
                          <tbody>
                            <tr>
                            
                              <td style="background:#f7f7f7;padding:10px 0 10px 0;color:#ffffff" align="center">
                                <img style="height:50px" src="https://cdn.wtecdn.net/files/40b9ec540571696b74f026dca2545b55/ExSecretLogo.png">
                              </td>
                              
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td height="100%" valign="top">
                        <div style="font-family:inherit">
                          <h1 style="font-size:18px;margin:0;font-family:sans-serif">
                            Hi Maria,
                          </h1>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit" height="100%" valign="top" bgcolor="">
                        <div>
                          <div style="font-family:inherit;text-align:inherit"><span style="color:#000000;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:normal;letter-spacing:normal;text-align:left;text-indent:0px;text-transform:none;white-space:pre-wrap;word-spacing:0px;text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;font-family:arial,helvetica,sans-serif;font-size:15px">Thanks for shopping with <a href="#" style="font-family:sans-serif" target="_blank">user2023</a>. We just received confirmation that your order billing was changed today. Here is new billing information:</span></div>
                          </div>
                        
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td height="100%" valign="top">
                        <div style="display:flex">
                          <h3 style="font-size:15px;font-weight:bold;text-transform:uppercase;width:100%;font-family:sans-serif">
                            ORDER #0123
                          </h3>
                        </div>
                        <hr style="border-width:1px 0 0 0;border-style:solid;border-color:#dddddd">
                        <br>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="padding:0px 0px 0px 0px" bgcolor="#FFFFFF">
                  <tbody>
                    <tr style="display: flex;">
                      <td height="100%" valign="top" style="flex: 1;display: flex; margin-right: 5px;">
                        <table style="width:100%;border-spacing:0;border-collapse:collapse;margin:0px 0px 0px 0px" cellpadding="0" cellspacing="0" align="right" border="0" bgcolor="">
                          <tbody>
                            <tr>
                              <td style="padding:0px;margin:0px;border-spacing:0">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                                  <tbody>
                                    <tr>
                                      <td height="100%" valign="top">
                                        <h5 style="font-size:15px;font-weight:bold;text-transform:uppercase;margin:14px 0 10px;font-family:sans-serif">SHIPPING ADDRESS</h5>
                                        <p style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">Elona Maria</p>
                                        <p style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif"><a href="mailto:elonan@example.com" rel="noopener" target="_blank">elonan@example.com</a></p>
                                        <p style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">Main St, San Jose, California 95131, United States</p>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td height="100%" valign="top" style="flex: 1;display: flex; margin-left: 5px;">
                        <table style="width:100%;border-spacing:0;border-collapse:collapse;margin:0px 0px 0px 10px" cellpadding="0" cellspacing="0" align="right" border="0" bgcolor="">
                          <tbody>
                            <tr>
                              <td style="padding:0px;margin:0px;border-spacing:0">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                                  <tbody>
                                    <tr>
                                      <td height="100%" valign="top">
                                        <h5 style="font-size:15px;font-weight:bold;text-transform:uppercase;margin:14px 0 10px;font-family:sans-serif">BILLING ADDRESS</h5>
                                        <p style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">Jenifer N</p>
                                        <p style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif"><a href="mailto:elonan.san@example.com" rel="noopener" target="_blank">elonan.san@example.com</a></p>
                                        <p style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">Main St, San Jose, California 95131, Canada</p>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit" height="100%" valign="top" bgcolor="">
                        <div>
                          <div style="font-family:inherit;text-align:inherit"><span style="font-size:15px">Best,<br>
                            The <a href="#" target="_blank">bralette2020</a> team</span>
                          </div>
                          <!--<div style="font-family:inherit;text-align:inherit"><span style="font-size:15px"><br>-->
                          <!--  </span><span style="font-size:15px"><em>*This email is sent from an account we use for sending messages only. If you need to contact us, please don't reply to this email, use </em></span><a href="#" target="_blank"><span style="font-size:15px"><u><em>our contact form</em></u></span></a><span style="font-size:15px"><em> instead.</em></span>-->
                          <!--</div>-->
                          <div></div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
    </body></html>`,
  },
  {
    name: "order cancel",
    template: `<html><head>
        <title></title>
          <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,600,600i,700,800|Raleway:400,700,900,700&amp;display=swap" rel="stylesheet" type="text/css">
      </head>
      <body>
        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width:100%;max-width:600px" align="center">
          <tbody>
            <tr>
              <td style="padding:0px 0px 0px 0px;color:#000000;text-align:left" bgcolor="#FFFFFF" width="100%" align="left">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="display:none!important;opacity:0;color:transparent;height:0;width:0">
                  <tbody>
                    <tr>
                      <td>
                        <p>Thank you for your purchase</p>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td height="100%" valign="top">
                        <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin-bottom:30px">
                          <tbody>
                            <tr>
                            
                              <td style="background:#f7f7f7;padding:10px 0 10px 0;color:#ffffff" align="center">
                                <img style="height:50px" src="https://cdn.wtecdn.net/files/40b9ec540571696b74f026dca2545b55/ExSecretLogo.png">
                              </td>
                              
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td height="100%" valign="top">
                        <div style="font-family:inherit">
                          <h1 style="font-size:18px;margin:0;font-family:sans-serif">
                            Hi Maria,
                          </h1>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit" height="100%" valign="top" bgcolor="">
                        <div style="display: flex; justify-content: start">
                            <div style="font-family:inherit;text-align:inherit">
                            <span style="color:#000000;font-style:normal;font-variant-ligatures:normal; font-variant-caps:normal;font-weight:normal;letter-spacing:normal;text-align:left;text-indent:0px;text-transform:none;word-spacing:0px;text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial; font-family:arial,helvetica,sans-serif;font-size:15px">Order <span style="font-weight: bold">1_1681530296</span> was canceled at your request and your payment has been voided.
                           </span>
                           <br/>
                           <span style="font-size: 15px; color:#000000;font-style:normal;font-variant-ligatures:normal; font-variant-caps:normal;font-weight:normal;letter-spacing:normal;text-align:left;">
                           Total amount refunded: <span style="font-weight: bold">$47.99 USD.</span> </span
                            </div>
                          
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td height="100%" valign="top">
                        <div style="display:flex">
                          <h3 style="font-size:15px;font-weight:bold;text-transform:uppercase;width:100%;font-family:sans-serif">
                            ORDER #0123
                          </h3>
                        </div>
                        <hr style="border-width:1px 0 0 0;border-style:solid;border-color:#dddddd">
                        <br>
                        <table border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
                          <tbody>
                            
                          <tr>
                          <td style="vertical-align:middle" align="left" width="17%"><a style="display:block" title="" href="#" target="_blank"> <img style="display:block;font-family:Arial;width:80px" src="https://cdn.wtecdn.net/files/22b6c2e2f361744e5639db02faf332b4/set3_gray.jpg" alt="" width="80" border="0"> </a></td>
                          <td style="padding:0 20px 0 20px;vertical-align:top" align="left" width="83%">
                            <p style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif"><a style="text-decoration:none" title="ErgoStool" href="#" rel="noopener" target="_blank">ExSecret - Ultimate Lift Stretch Full-Figure Seamless Lace Cut-Out Bra</a></p>
                            <p style="font-size:15px;line-height:26px;color:#555;margin:0;font-family:sans-serif">80% Off Set 3 (Black &amp; Beige &amp; Gray)/2XL</p>
                            <div style="display: flex; items-content: center; justify-content: space-between">
                            <p style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif"> &nbsp;x&nbsp; <span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$49 x 1</span></p>
                            <p style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif"> &nbsp;x&nbsp; <span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$49</span></p>
                            </div>
                          </td>
                        </tr>

                        <tr style="margin-top: 12px">
                              <td style="vertical-align:middle" align="left" width="17%"><a style="display:block" title="" href="#" target="_blank"> <img style="display:block;font-family:Arial;width:80px" src="https://cdn.wtecdn.net/files/22b6c2e2f361744e5639db02faf332b4/set3_gray.jpg" alt="" width="80" border="0"> </a></td>
                              <td style="padding:0 20px 0 20px;vertical-align:top" align="left" width="83%">
                                <p style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif"><a style="text-decoration:none" title="ErgoStool" href="#" rel="noopener" target="_blank">ExSecret - Ultimate Lift Stretch Full-Figure Seamless Lace Cut-Out Bra</a></p>
                                <p style="font-size:15px;line-height:26px;color:#555;margin:0;font-family:sans-serif">80% Off Set 3 (Black &amp; Beige &amp; Gray)/2XL</p>
                                <div style="display: flex; items-content: center; justify-content: space-between">
                                <p style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif"> &nbsp;x&nbsp; <span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$49 x 1</span></p>
                                <p style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif"> &nbsp;x&nbsp; <span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$49</span></p>
                                </div>
                              </td>
                            </tr>

                            <tr style="margin-top: 12px">
                              <td style="vertical-align:middle" align="left" width="17%"><a style="display:block" title="" href="#" target="_blank"> <img style="display:block;font-family:Arial;width:80px" src="https://cdn.wtecdn.net/files/22b6c2e2f361744e5639db02faf332b4/set3_gray.jpg" alt="" width="80" border="0"> </a></td>
                              <td style="padding:0 20px 0 20px;vertical-align:top" align="left" width="83%">
                                <p style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif"><a style="text-decoration:none" title="ErgoStool" href="#" rel="noopener" target="_blank">ExSecret - Ultimate Lift Stretch Full-Figure Seamless Lace Cut-Out Bra</a></p>
                                <p style="font-size:15px;line-height:26px;color:#555;margin:0;font-family:sans-serif">80% Off Set 3 (Black &amp; Beige &amp; Gray)/2XL</p>
                                <div style="display: flex; items-content: center; justify-content: space-between">
                                <p style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif"> &nbsp;x&nbsp; <span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$49 x 1</span></p>
                                <p style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif"> &nbsp;x&nbsp; <span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$49</span></p>
                                </div>
                              </td>
                            </tr>
                            
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <br>
                <hr style="border-width:1px 0 0 0;border-style:solid;border-color:#dddddd">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                  <tbody>
                    <tr>
                      <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit" height="100%" valign="top" bgcolor="">
                        <div>
                          <div style="font-family:inherit;text-align:inherit"><span style="font-size:15px">Best,<br>
                            The <a href="wecella.com" target="_blank">bralette2020</a> team</span>
                          </div>
                          <!--<div style="font-family:inherit;text-align:inherit"><span style="font-size:15px"><br>-->
                          <!--  </span><span style="font-size:15px"><em>*This email is sent from an account we use for sending messages only. If you need to contact us, please don't reply to this email, use </em></span><a href="wecella.com" target="_blank"><span style="font-size:15px"><u><em>our contact form</em></u></span></a><span style="font-size:15px"><em> instead.</em></span>-->
                          <!--</div>-->
                          <div></div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      
    </body></html>`,
  },
  {
    name: "order fulfilled",
    template: `<html>

        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Email Confirm Order</title>
            <link
                href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,600,600i,700,800|Raleway:400,700,900,700&amp;display=swap"
                rel="stylesheet" type="text/css" />
            <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
        </head>
        
        <body>
            <div id="app">
                <div style="background:#f7f7f7;padding:30px 0 30px 0;color:#ffffff; width:100%;max-width:600px; margin: auto;">
                    <div style="max-width: 360px; max-height: 120px; margin: auto">
                        <div style="display: flex;">
                            <img style="height:50px" src="{{logo}}">
                            <p
                                style="background:#f7f7f7;padding:10px 0 10px 0; color: #303030;font-family: 'Roboto';font-size: 24px;font-weight: 900;line-height: 37px;margin: 0px 0px 0px 8px; text-align: center;">
                            </p>
                        </div>
                    </div>
                </div>
                <table width="100%" cellpadding="0" cellspacing="0" border="0"
                    style="width:100%;max-width:600px; margin-top: 40px;" align="center">
                    <tbody>
                        <tr>
                            <td style="padding:0px 0px 0px 0px;color:#000000;text-align:left" bgcolor="#FFFFFF" width="100%"
                                align="left">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                    style="display:none!important;opacity:0;color:transparent;height:0;width:0">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <p>Thank you for your purchase</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                                    <tbody>
                                        <tr>
                                            <td height="100%" valign="top">
                                                <div style="font-family:inherit">
                                                    <h1 style="font-size:18px;margin:0;font-family:sans-serif">
                                                        Hi Maria,
                                                    </h1>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                                    <tbody>
                                        <tr>
                                            <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit"
                                                height="100%" valign="top" bgcolor="">
                                                <div>
                                                    <div style="font-family:inherit;text-align:inherit"><span
                                                            style="color:#000000;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:normal;letter-spacing:normal;text-align:left;text-indent:0px;text-transform:none;word-spacing:0px;text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;font-family:arial,helvetica,sans-serif;font-size:15px">
                                                            Great news - your order has successfully been handed to our delivery
                                                            partner and prepared to be shipped to you as soon as possible! If
                                                            you order contains multiple shipments, we will email you additional
                                                            tracking information the moment it becomes available.</span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                                    <tbody>
                                        <tr>
                                            <td height="100%" valign="top">
                                                <div style="display:flex; align-items: center; justify-items: space-between;">
                                                    <h3
                                                        style="font-size:15px;font-weight:bold;text-transform:uppercase;width:100%;font-family:sans-serif">
                                                        ORDER
                                                        <div
                                                            style="font-size:15px;font-weight:bold;text-transform:uppercase;width:100%;font-family:sans-serif">
                                                            1_1681530296
                                                        </div>
                                                    </h3>
                                                    <h3
                                                        style="font-size:15px;font-weight:bold;text-transform:uppercase;width:100%;font-family:sans-serif; text-align: right;">
                                                        TRACKING NUMBERS
                                                        <div>
                                                            <a href={{trackingUrl}} target="_blank">1231231231231</a>
                                                        </div>
                                                    </h3>
                                                </div>
                                                <hr style="border-width:1px 0 0 0;border-style:solid;border-color:#dddddd">
                                                <br>
                                                <table border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
                                                <tbody>
                                                  <tr>
                                                    <td style="vertical-align:middle" align="left" width="17%"><a style="display:block" title="" href="#" target="_blank"> <img style="display:block;font-family:Arial;width:80px" src="https://cdn.wtecdn.net/files/22b6c2e2f361744e5639db02faf332b4/set3_gray.jpg" alt="" width="80" border="0"> </a></td>
                                                    <td style="padding:0 20px 0 20px;vertical-align:top" align="left" width="83%">
                                                      <p style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif"><a style="text-decoration:none" title="ErgoStool" href="#" rel="noopener" target="_blank">ExSecret - Ultimate Lift Stretch Full-Figure Seamless Lace Cut-Out Bra</a></p>
                                                      <p style="font-size:15px;line-height:26px;color:#555;margin:0;font-family:sans-serif">80% Off Set 3 (Black &amp; Beige &amp; Gray)/2XL</p>
                                                      <p style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif"><span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$64.95</span> &nbsp;x&nbsp; <span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">1</span></p>
                                                    </td>
                                                    <td style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">$64.95</td>
                                                  </tr>
                                                  <tr>
                                                    <td style="vertical-align:middle" align="left" width="17%"><a style="display:block" title="" href="#" target="_blank"> <img style="display:block;font-family:Arial;width:80px" src="https://cdn.wtecdn.net/files/22b6c2e2f361744e5639db02faf332b4/set3_gray.jpg" alt="" width="80" border="0"> </a></td>
                                                    <td style="padding:0 20px 0 20px;vertical-align:top" align="left" width="83%">
                                                      <p style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif"><a style="text-decoration:none" title="ErgoStool" href="#" rel="noopener" target="_blank">ExSecret - Ultimate Lift Stretch Full-Figure Seamless Lace Cut-Out Bra</a></p>
                                                      <p style="font-size:15px;line-height:26px;color:#555;margin:0;font-family:sans-serif">80% Off Set 3 (Black &amp; Beige &amp; Gray)/2XL</p>
                                                      <p style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif"><span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$64.95</span> &nbsp;x&nbsp; <span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">1</span></p>
                                                    </td>
                                                    <td style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">$64.95</td>
                                                  </tr>
                                                  <tr>
                                                    <td style="vertical-align:middle" align="left" width="17%"><a style="display:block" title="" href="#" target="_blank"> <img style="display:block;font-family:Arial;width:80px" src="https://cdn.wtecdn.net/files/22b6c2e2f361744e5639db02faf332b4/set3_gray.jpg" alt="" width="80" border="0"> </a></td>
                                                    <td style="padding:0 20px 0 20px;vertical-align:top" align="left" width="83%">
                                                      <p style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif"><a style="text-decoration:none" title="ErgoStool" href="#" rel="noopener" target="_blank">ExSecret - Ultimate Lift Stretch Full-Figure Seamless Lace Cut-Out Bra</a></p>
                                                      <p style="font-size:15px;line-height:26px;color:#555;margin:0;font-family:sans-serif">80% Off Set 3 (Black &amp; Beige &amp; Gray)/2XL</p>
                                                      <p style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif"><span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">$64.95</span> &nbsp;x&nbsp; <span style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">1</span></p>
                                                    </td>
                                                    <td style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">$64.95</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                                    <tbody>
                                        <tr>
                                            <td height="100%" valign="top">
                                                <br>
                                                <hr style="border-width:1px 0 0 0;border-style:solid;border-color:#dddddd">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%"
                                    style="padding:0px 0px 0px 0px" bgcolor="#FFFFFF">
                                    <tbody>
                                        <tr style="display: flex;">
                                            <td height="100%" valign="top" style="flex: 1;display: flex; margin-right: 5px;">
                                                <table
                                                    style="width:100%;border-spacing:0;border-collapse:collapse;margin:0px 0px 0px 0px"
                                                    cellpadding="0" cellspacing="0" align="right" border="0" bgcolor="">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding:0px;margin:0px;border-spacing:0">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                                                    style="table-layout:fixed">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td height="100%" valign="top">
                                                                                <h5
                                                                                    style="font-size:15px;font-weight:bold;text-transform:uppercase;margin:14px 0 10px;font-family:sans-serif">
                                                                                    SHIPPING ADDRESS</h5>
                                                                                <p
                                                                                    style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">
                                                                                    John Doe</p>
                                                                                <p
                                                                                    style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">
                                                                                    <a :href="mailto:Johndoe@gmail.com"
                                                                                        rel="noopener"
                                                                                        target="_blank">Johndoe@gmail.com</a>
                                                                                </p>
                                                                                <p
                                                                                    style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">
                                                                                    1 Main St, San Jose, CA, US
                                                                                </p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td height="100%" valign="top" style="flex: 1;display: flex; margin-right: 5px;">
                                                <table
                                                    style="width:100%;border-spacing:0;border-collapse:collapse;margin:0px 0px 0px 10px"
                                                    cellpadding="0" cellspacing="0" align="right" border="0" bgcolor="">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding:0px;margin:0px;border-spacing:0">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                                                    style="table-layout:fixed">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td height="100%" valign="top">
                                                                                <h5
                                                                                    style="font-size:15px;font-weight:bold;text-transform:uppercase;margin:14px 0 10px;font-family:sans-serif">
                                                                                    BILLING ADDRESS
                                                                                </h5>
                                                                                <p
                                                                                    style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">
                                                                                    John Doe</p>
                                                                                <p
                                                                                    style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">
                                                                                    Johndoe@gmail.com
                                                                                </p>
                                                                                <p
                                                                                    style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">
                                                                                    1 Main St, San Jose, CA, US</p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </body>
        
        </html>`,
  },
  {
    name: "confirm shinpping",
    template: `<html>

   <head>
       <meta charset="UTF-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <title>Email Confirm Order</title>
       <link
           href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,600,600i,700,800|Raleway:400,700,900,700&amp;display=swap"
           rel="stylesheet" type="text/css" />
       <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
   </head>
   
   <body>
       <div id="app">
           <div style="background:#f7f7f7;padding:30px 0 30px 0;color:#ffffff; width:100%;max-width:600px; margin: auto;">
               <div style="max-width: 360px; max-height: 120px; margin: auto">
                   <div style="display: flex;">
                       <img style="height:50px" src="{{logo}}">
                       <p
                           style="background:#f7f7f7;padding:10px 0 10px 0; color: #303030;font-family: 'Roboto';font-size: 24px;font-weight: 900;line-height: 37px;margin: 0px 0px 0px 8px; text-align: center;">
                       </p>
                   </div>
               </div>
           </div>
           <table width="100%" cellpadding="0" cellspacing="0" border="0"
               style="width:100%;max-width:600px; margin-top: 40px;" align="center">
               <tbody>
                   <tr>
                       <td style="padding:0px 0px 0px 0px;color:#000000;text-align:left" bgcolor="#FFFFFF" width="100%"
                           align="left">
                           <table border="0" cellpadding="0" cellspacing="0" width="100%"
                               style="display:none!important;opacity:0;color:transparent;height:0;width:0">
                               <tbody>
                                   <tr>
                                       <td>
                                           <p>Thank you for your purchase</p>
                                       </td>
                                   </tr>
                               </tbody>
                           </table>
                           <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                               <tbody>
                                   <tr>
                                       <td height="100%" valign="top">
                                           <div style="font-family:inherit">
                                               <h1 style="font-size:18px;margin:0;font-family:sans-serif">
                                                   Hi Maria,
                                               </h1>
                                           </div>
                                       </td>
                                   </tr>
                               </tbody>
                           </table>
                           <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                               <tbody>
                                   <tr>
                                       <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit"
                                           height="100%" valign="top" bgcolor="">
                                           <div>
                                               <div style="font-family:inherit;text-align:inherit"><span
                                                       style="color:#000000;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:normal;letter-spacing:normal;text-align:left;text-indent:0px;text-transform:none;word-spacing:0px;text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;font-family:arial,helvetica,sans-serif;font-size:15px">
                                                       Great news - your order has successfully been handed to our delivery
                                                       partner and prepared to be shipped to you as soon as possible! If
                                                       you order contains multiple shipments, we will email you additional
                                                       tracking information the moment it becomes available.</span>
                                               </div>
                                           </div>
                                       </td>
                                   </tr>
                               </tbody>
                           </table>
                           <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                               <tbody>
                                   <tr>
                                       <td height="100%" valign="top">
                                           <div style="display:flex; align-items: center; justify-items: space-between;">
                                               <h3
                                                   style="font-size:15px;font-weight:bold;text-transform:uppercase;width:100%;font-family:sans-serif">
                                                   ORDER
                                                   <div
                                                       style="font-size:15px;font-weight:bold;text-transform:uppercase;width:100%;font-family:sans-serif">
                                                       1_1681530296
                                                   </div>
                                               </h3>
                                               <h3
                                                   style="font-size:15px;font-weight:bold;text-transform:uppercase;width:100%;font-family:sans-serif; text-align: right;">
                                                   TRACKING NUMBERS
                                                   <div>
                                                       <a href={{trackingUrl}} target="_blank">1231231231231</a>
                                                   </div>
                                               </h3>
                                           </div>
                                           <hr style="border-width:1px 0 0 0;border-style:solid;border-color:#dddddd">
                                           <br>
                                           <table border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
                                               <tbody>
                                                   <tr>
   
                                                       <td style="padding:0 20px 0 0px;vertical-align:top" align="left"
                                                           width="83%">
                                                           <p
                                                               style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif">
                                                               <a style="text-decoration:none" title="ErgoStool" href="#"
                                                                   rel="noopener" target="_blank">ExSecret - Ultimate Lift
                                                                   Stretch Full-Figure Seamless Lace Cut-Out Bra</a>
                                                           </p>
                                                           <p
                                                               style="font-size:15px;line-height:26px;color:#555;margin:0;font-family:sans-serif">
                                                               80% Off Set 3 (Black &amp; Beige &amp; Gray)/2XL</p>
                                                           <p
                                                               style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">
                                                               <span
                                                                   style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">1</span>
                                                           </p>
                                                       </td>
                                                       <td
                                                           style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">
   
                                                           <a href="kenh14.vn">
                                                               YT1231231312312321
                                                           </a>
                                                       </td>
                                                   </tr>
                                                   <tr>
   
                                                       <td style="padding:0 20px 0 0px;vertical-align:top" align="left"
                                                           width="83%">
                                                           <p
                                                               style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif">
                                                               <a style="text-decoration:none" title="ErgoStool" href="#"
                                                                   rel="noopener" target="_blank">ExSecret - Ultimate Lift
                                                                   Stretch Full-Figure Seamless Lace Cut-Out Bra</a>
                                                           </p>
                                                           <p
                                                               style="font-size:15px;line-height:26px;color:#555;margin:0;font-family:sans-serif">
                                                               80% Off Set 3 (Black &amp; Beige &amp; Gray)/2XL</p>
                                                           <p
                                                               style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">
                                                               <span
                                                                   style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">1</span>
                                                           </p>
                                                       </td>
                                                       <td
                                                           style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">
   
                                                           <a href="kenh14.vn">
                                                               YT1231231312312321
                                                           </a>
                                                       </td>
                                                   </tr>
                                                   <tr>
   
                                                       <td style="padding:0 20px 0 0px;vertical-align:top" align="left"
                                                           width="83%">
                                                           <p
                                                               style="font-size:15px;line-height:26px;color:#222;font-weight:600;margin:0;font-family:sans-serif">
                                                               <a style="text-decoration:none" title="ErgoStool" href="#"
                                                                   rel="noopener" target="_blank">ExSecret - Ultimate Lift
                                                                   Stretch Full-Figure Seamless Lace Cut-Out Bra</a>
                                                           </p>
                                                           <p
                                                               style="font-size:15px;line-height:26px;color:#555;margin:0;font-family:sans-serif">
                                                               80% Off Set 3 (Black &amp; Beige &amp; Gray)/2XL</p>
                                                           <p
                                                               style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">
                                                               <span
                                                                   style="font-size:15px;line-height:26px;color:#222;margin:0;font-family:sans-serif">1</span>
                                                           </p>
                                                       </td>
                                                       <td
                                                           style="font-size:15px;line-height:26px;color:#222;vertical-align:bottom;font-weight:600;font-family:sans-serif">
   
                                                           <a href="kenh14.vn">
                                                               YT1231231312312321
                                                           </a>
                                                       </td>
                                                   </tr>
   
   
                                               </tbody>
                                           </table>
                                       </td>
                                   </tr>
                               </tbody>
                           </table>
                           <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
                               <tbody>
                                   <tr>
                                       <td height="100%" valign="top">
                                           <br>
                                           <hr style="border-width:1px 0 0 0;border-style:solid;border-color:#dddddd">
                                       </td>
                                   </tr>
                               </tbody>
                           </table>
                           <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%"
                               style="padding:0px 0px 0px 0px" bgcolor="#FFFFFF">
                               <tbody>
                                   <tr style="display: flex;">
                                       <td height="100%" valign="top" style="flex: 1;display: flex; margin-right: 5px;">
                                           <table
                                               style="width:100%;border-spacing:0;border-collapse:collapse;margin:0px 0px 0px 0px"
                                               cellpadding="0" cellspacing="0" align="right" border="0" bgcolor="">
                                               <tbody>
                                                   <tr>
                                                       <td style="padding:0px;margin:0px;border-spacing:0">
                                                           <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                                               style="table-layout:fixed">
                                                               <tbody>
                                                                   <tr>
                                                                       <td height="100%" valign="top">
                                                                           <h5
                                                                               style="font-size:15px;font-weight:bold;text-transform:uppercase;margin:14px 0 10px;font-family:sans-serif">
                                                                               SHIPPING ADDRESS</h5>
                                                                           <p
                                                                               style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">
                                                                               John Doe</p>
                                                                           <p
                                                                               style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">
                                                                               <a :href="mailto:Johndoe@gmail.com"
                                                                                   rel="noopener"
                                                                                   target="_blank">Johndoe@gmail.com</a>
                                                                           </p>
                                                                           <p
                                                                               style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">
                                                                               1 Main St, San Jose, CA, US
                                                                           </p>
                                                                       </td>
                                                                   </tr>
                                                               </tbody>
                                                           </table>
                                                       </td>
                                                   </tr>
                                               </tbody>
                                           </table>
                                       </td>
                                       <td height="100%" valign="top" style="flex: 1;display: flex; margin-right: 5px;">
                                           <table
                                               style="width:100%;border-spacing:0;border-collapse:collapse;margin:0px 0px 0px 10px"
                                               cellpadding="0" cellspacing="0" align="right" border="0" bgcolor="">
                                               <tbody>
                                                   <tr>
                                                       <td style="padding:0px;margin:0px;border-spacing:0">
                                                           <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                                               style="table-layout:fixed">
                                                               <tbody>
                                                                   <tr>
                                                                       <td height="100%" valign="top">
                                                                           <h5
                                                                               style="font-size:15px;font-weight:bold;text-transform:uppercase;margin:14px 0 10px;font-family:sans-serif">
                                                                               BILLING ADDRESS
                                                                           </h5>
                                                                           <p
                                                                               style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">
                                                                               John Doe</p>
                                                                           <p
                                                                               style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">
                                                                               Johndoe@gmail.com
                                                                           </p>
                                                                           <p
                                                                               style="font-size:15px;color:#222;margin:0;line-height:26px;font-family:sans-serif">
                                                                               1 Main St, San Jose, CA, US</p>
                                                                       </td>
                                                                   </tr>
                                                               </tbody>
                                                           </table>
                                                       </td>
                                                   </tr>
                                               </tbody>
                                           </table>
                                       </td>
                                   </tr>
   
                               </tbody>
                           </table>
                       </td>
                   </tr>
                   <tr style="margin-top: 20px;">
                       <td width="520">
                           <div style="font-family:inherit;text-align:inherit; line-height: 30px;"><span
                                   style="font-size:15px">Best,<br>
                                   The <a href="{{domain}}" target="_blank">{{storeName}}</a>
                                   team</span>
                           </div>
                       </td>
                   </tr>
               </tbody>
           </table>
       </div>
   </body>
   
   </html>`,
  },
];
