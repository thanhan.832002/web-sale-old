import param from "./param";
export const objectToParam = (obj: object) => param(obj);
// const jwt = require('jsonwebtoken')
const numeral = require("numeral");
import { $localForage } from "~/plugins/localforage.client";
import { Context } from "@nuxt/types";
import { api } from "./constaint";
var momentz = require("moment-timezone");
var moment = require("moment");

export type IUtils = {
  formatCurrency: (price: number) => string;
  formatCurrencyFloat: (price: number) => string;
  calcDollar: (price: number) => string;
  parserFromTime: (settings: any, dateStart: string) => number;
  parserToTime: (settings: any, dateEnd: string) => number;
  parseDomain: (domain: string) => String;
  formatDate: (createdAt: string, settings: any) => string;
  formatDateCreatedAt: (createdAt: string) => string;
  sortList: (list: any, item: string, type: string) => any;
  sleep: (milliseconds: number) => any;
  deepEqual: (obj1: any, obj2: any) => boolean
  findDuplicates: (array: any, fieldName: string) => string[]
};
// export const isTokenExpired = async (token: string) => {
//     let { exp } = jwt.decode(token)
//     const now = Date.now().valueOf() / 1000
//     return now > exp
// }

export const utils = (ctx: Context) => ({
  formatCurrency: (price: number) => {
    return `${price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  },

  formatCurrencyFloat: (price: any) => {
    if (isNaN(price)) return 0;
    const priceFixed = parseFloat(price.toFixed(2));
    return `${priceFixed.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  },

  calcDollar: (price: number) => {
    const dollar = (price / 23000).toFixed(2);
    return `~${dollar}$`;
  },

  formatDateCreatedAt: (createdAt: string) => {
    return moment(createdAt).format("HH:mm DD/MM/YYYY");
  },

  formatDate: (createdAt: string, settings?: any) => {
    const timezone = settings?.timezone || "+7";
    const unix = moment(createdAt).unix() * 1000 + timezone * 60 * 60 * 1000;
    return moment(unix).utc().format("HH:mm DD/MM/YYYY");
  },

  parseDomain: (domain: string) => {
    return `${api.replace("http://", `http://${domain}.`)}`;
  },

  parserFromTime: (settings: any, dateStart: string) => {
    if (settings.timezone === "+1") {
      const timezone = momentz
        .tz(dateStart, "YYYY-MM-DD", "Atlantic/Cape_Verde")
        .utc()
        .unix();
      return timezone;
    }
    if (settings.timezone === "-8") {
      const timezone = momentz
        .tz(dateStart, "YYYY-MM-DD", "America/Chicago")
        .utc()
        .unix();
      return timezone;
    }
    if (settings.timezone === "-9") {
      const timezone = momentz
        .tz(dateStart, "YYYY-MM-DD", "America/Anchorage")
        .utc()
        .unix();
      return timezone;
    }
    if (settings.timezone === "-7") {
      const timezone = momentz
        .tz(dateStart, "YYYY-MM-DD", "US/Arizona")
        .utc()
        .unix();
      return timezone;
    }
    if (settings.timezone === "+0") {
      const timezone = momentz.tz(dateStart, "YYYY-MM-DD", "GB").utc().unix();
      return timezone;
    }
    if (settings.timezone === "+8") {
      const timezone = momentz
        .tz(dateStart, "YYYY-MM-DD", "Asia/Singapore")
        .utc()
        .unix();
      return timezone;
    }
    if (settings.timezone === "+7") {
      const timezone = momentz
        .tz(dateStart, "YYYY-MM-DD", "Asia/Ho_Chi_Minh")
        .utc()
        .unix();
      return timezone;
    }
  },

  parserToTime: (settings: any, dateEnd: string) => {
    if (settings.timezone === "+1") {
      const timezone = momentz
        .tz(dateEnd, "YYYY-MM-DD", "Atlantic/Cape_Verde")
        .endOf("day")
        .utc()
        .unix();
      return timezone;
    }
    if (settings.timezone === "-8") {
      const timezone = momentz
        .tz(dateEnd, "YYYY-MM-DD", "America/Chicago")
        .endOf("day")
        .utc()
        .unix();
      return timezone;
    }
    if (settings.timezone === "-9") {
      const timezone = momentz
        .tz(dateEnd, "YYYY-MM-DD", "America/Anchorage")
        .endOf("day")
        .utc()
        .unix();
      return timezone;
    }
    if (settings.timezone === "-7") {
      const timezone = momentz
        .tz(dateEnd, "YYYY-MM-DD", "US/Arizona")
        .endOf("day")
        .utc()
        .unix();
      return timezone;
    }
    if (settings.timezone === "+0") {
      const timezone = momentz
        .tz(dateEnd, "YYYY-MM-DD", "GB")
        .endOf("day")
        .utc()
        .unix();
      return timezone;
    }
    if (settings.timezone === "+8") {
      const timezone = momentz
        .tz(dateEnd, "YYYY-MM-DD", "Asia/Singapore")
        .endOf("day")
        .utc()
        .unix();
      return timezone;
    }
    if (settings.timezone === "+7") {
      const timezone = momentz
        .tz(dateEnd, "YYYY-MM-DD", "Asia/Ho_Chi_Minh")
        .endOf("day")
        .utc()
        .unix();
      return timezone;
    }
  },

  sortList: (list: any, item: string, type: string) => {
    if (type === "lowToHigh") {
      return list.sort((a: any, b: any) => a[item] - b[item]);
    } else {
      return list.sort((a: any, b: any) => b[item] - a[item]);
    }
  },

  sleep(milliseconds: number) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(true);
      }, milliseconds);
    });
  },

  deepEqual: (obj1: any, obj2: any) => {
    return JSON.stringify(obj1) === JSON.stringify(obj2);
  },

  findDuplicates(array: any, fieldName: string) {
    const seen = new Set();
    const duplicates = new Set();

    for (const obj of array) {
      const value = obj[fieldName];

      if (seen.has(value)) {
        duplicates.add(value);
      } else {
        seen.add(value);
      }
    }

    return Array.from(duplicates);
  }
});
