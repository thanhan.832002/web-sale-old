import Vue from "vue";
import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";
import { IAuth, ITokensResponse, IUser } from "~/interfaces/auth";
import {
  accessTokenKey,
  refreshTokenKey,
  userInfoKey,
} from "~/common/constaint";
import { apiErrorHandler, redirectPage } from "~/utils/apiHanlers";
import { IApiPlugin } from "~/interfaces/api";
import collectionApi from "~/api/collection";
import { $axios } from "~/utils/axios";
import { getTimeZone, getUnixFrom, getUnixTo } from "~/utils/timeZone";
var momentz = require("moment-timezone");
var moment = require("moment");
@Module({
  name: "setting",
  namespaced: true,
  stateFactory: true,
})
export default class AdminSetting extends VuexModule {
  siteInfo: any = {};
  from = "";
  to = "";
  timezone = "";
  timeActive = "";

  dateArr = [
    moment().subtract(7, "days").format("YYYY-MM-DD"),
    moment().format("YYYY-MM-DD"),
  ];

  @Mutation
  setDateArr(value: string[]) {
    this.dateArr = value;
  }

  @Mutation
  setTimeActive(value: string) {
    this.timeActive = value;
  }

  @Mutation
  setTimezone(value: string) {
    this.timezone = value;
  }

  @Mutation
  setFilterByTime({ time }: { time: string }) {
    const timezone = this.siteInfo.timezone;
    if (!this.timezone || !time) return;
    this.timezone = getTimeZone(timezone) || "US/Arizona";

    let dateStart = "";
    let dateEnd = "";

    let from = "";
    let to = "";
    this.timeActive = time;
    if (time === "Today") {
      dateStart = momentz
        .tz(this.timezone)
        .startOf("days")
        .format("YYYY-MM-DD");
      dateEnd = momentz.tz(this.timezone).startOf("days").format("YYYY-MM-DD");
      from = getUnixFrom(timezone, dateStart);
      to = getUnixTo(timezone, dateEnd);
    }
    if (time === "This year") {
      dateStart = momentz
        .tz(this.timezone)
        .startOf("year")
        .format("YYYY-MM-DD");
      dateEnd = momentz.tz(this.timezone).endOf("week").format("YYYY-MM-DD");
      from = getUnixFrom(timezone, dateStart);
      to = getUnixTo(timezone, dateEnd);
    }
    if (time === "This week") {
      dateStart = momentz
        .tz(this.timezone)
        .startOf("week")
        .format("YYYY-MM-DD");
      dateEnd = momentz.tz(this.timezone).endOf("week").format("YYYY-MM-DD");
      from = getUnixFrom(timezone, dateStart);
      to = getUnixTo(timezone, dateEnd);
    }

    if (time === "Last week") {
      dateStart = momentz
        .tz(this.timezone)
        .subtract(1, "weeks")
        .startOf("week")
        .format("YYYY-MM-DD");
      dateEnd = momentz
        .tz(this.timezone)
        .subtract(1, "weeks")
        .endOf("week")
        .format("YYYY-MM-DD");
      from = getUnixFrom(timezone, dateStart);
      to = getUnixTo(timezone, dateEnd);
    }
    if (time === "This month") {
      dateStart = momentz
        .tz(this.timezone)
        .startOf("month")
        .format("YYYY-MM-DD");
      dateEnd = momentz.tz(this.timezone).endOf("month").format("YYYY-MM-DD");
      from = getUnixFrom(timezone, dateStart);
      to = getUnixTo(timezone, dateEnd);
    }

    if (time === "Last month") {
      dateStart = momentz
        .tz(this.timezone)
        .subtract(1, "months")
        .startOf("month")
        .format("YYYY-MM-DD");
      dateEnd = momentz
        .tz(this.timezone)
        .subtract(1, "months")
        .endOf("month")
        .format("YYYY-MM-DD");
      from = getUnixFrom(timezone, dateStart);
      to = getUnixTo(timezone, dateEnd);
    }

    if (time === "Yesterday") {
      dateStart = momentz
        .tz(this.timezone)
        .subtract(1, "days")
        .startOf("day")
        .format("YYYY-MM-DD");
      dateEnd = momentz
        .tz(this.timezone)
        .subtract(1, "days")
        .endOf("day")
        .format("YYYY-MM-DD");
      from = getUnixFrom(timezone, dateStart);
      to = getUnixTo(timezone, dateEnd);
    }

    if (time === "Last 7 days") {
      dateStart = momentz
        .tz(this.timezone)
        .subtract(7, "days")
        .startOf("day")
        .format("YYYY-MM-DD");
      dateEnd = momentz.tz(this.timezone).endOf("day").format("YYYY-MM-DD");
      from = getUnixFrom(timezone, dateStart);
      to = getUnixTo(timezone, dateEnd);
    }

    if (time === "Last 14 days") {
      dateStart = momentz
        .tz(this.timezone)
        .subtract(14, "days")
        .startOf("day")
        .format("YYYY-MM-DD");
      dateEnd = momentz.tz(this.timezone).endOf("day").format("YYYY-MM-DD");
      from = getUnixFrom(timezone, dateStart);
      to = getUnixTo(timezone, dateEnd);
    }
    this.from = from;
    this.to = to;
    this.dateArr = [dateStart, dateEnd];
  }

  @Mutation
  setTime({
    dateStart,
    dateEnd,
    timeFrom,
    timeTo,
  }: {
    dateStart: string;
    dateEnd: string;
    timeFrom?: string;
    timeTo?: string;
  }) {
    this.timezone = getTimeZone(this.siteInfo.timezone) || "US/Arizona";

    const isCheckTimeHigher = moment(dateStart).diff(moment(dateEnd), "days");
    if (isCheckTimeHigher > 0) {
      this.from =
        getUnixTo(this.siteInfo.timezone, dateEnd, timeTo || "") || "";
      this.to =
        getUnixFrom(this.siteInfo.timezone, dateStart, timeFrom || "") || "";
      this.dateArr = [dateEnd, dateStart];
      return;
    }
    this.from =
      getUnixFrom(this.siteInfo.timezone, dateStart, timeFrom || "") || "";
    this.to = getUnixTo(this.siteInfo.timezone, dateEnd, timeTo || "") || "";
  }

  @Mutation
  setSiteInfo(siteInfo: any) {
    this.siteInfo = siteInfo;
  }

  @Action({ rawError: true })
  onGetSetting({ ctx }: { ctx: Vue }) {
    try {
      const { $api, $store } = ctx;
    } catch (error) {}
  }
}
