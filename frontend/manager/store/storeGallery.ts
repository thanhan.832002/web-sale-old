import Vue from "vue";
import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";

import { api } from "~/common/constaint";
import { apiErrorHandler } from "~/utils/apiHanlers";

import { resources } from "~/interfaces/api";
@Module({
  name: "storeGallery",
  namespaced: true,
  stateFactory: true,
})
class StoreGalleryModule extends VuexModule {
  config = {
    page: 1,
    limit: 100,
    search: "",
  };

  isUploadImage = false;
  progress = 0;
  loadingUploadImage = false;
  loading = false;
  loadingTableTag = false;
  loadingTable = false;
  listTag = [];
  listGallery = [];
  listCoverId: any = [];
  storeId: string = "";

  @Mutation
  setListTag(listTag: any) {
    this.listTag = listTag;
  }

  @Mutation
  setListGallery(listGallery: any) {
    this.listGallery = listGallery;
  }

  @Mutation
  setValueBoolane({ status, type }: { status: boolean; type: string }) {
    if (type === "loading") {
      this.loading = status;
    } else if (type === "loadingTableTag") {
      this.loadingTableTag = status;
    } else if (type === "loadingTable") {
      this.loadingTable = status;
    } else if (type === "isUploadImage") {
      this.isUploadImage = status;
    }
  }
  @Mutation
  setProgress(progress: number) {
    this.progress = progress;
  }

  @Mutation
  setStoreId(storeId: string) {
    if (storeId) {
      this.storeId = storeId;
    }
  }
  @Mutation
  setListCoverId(listCoverId: any) {
    this.listCoverId = listCoverId;
  }
  @Action({ rawError: true })
  async onGetListTag({ ctx }: { ctx: Vue }) {
    try {
      const { $api, $store } = ctx;
      ctx.$store.commit("storeGallery/setValueBoolane", {
        status: true,
        type: "loadingTableTag",
      });
      const response = await $api.gallery.getListTag(this.config);
      if (response) {
        const listTag = [{ name: "All", _id: "" }, ...response.data.data];
        $store.commit("storeGallery/setListTag", listTag);
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    } finally {
      ctx.$store.commit("storeGallery/setValueBoolane", {
        status: false,
        type: "loadingTableTag",
      });
    }
  }

  @Action({ rawError: true })
  async onCreateTag({ ctx, name }: { ctx: Vue; name: any }) {
    try {
      const { $api, $store, $pushNotification } = ctx;
      $store.commit("storeGallery/setValueBoolane", {
        status: true,
        type: "loading",
      });
      await $api.gallery.createTag(name);
      $pushNotification.success("Create tag successfully");
      await $store.dispatch("storeGallery/onGetListTag", { ctx });
    } catch (error) {
      apiErrorHandler(error, ctx);
    } finally {
      ctx.$store.commit("storeGallery/setValueBoolane", {
        status: false,
        type: "loading",
      });
    }
  }

  @Action({ rawError: true })
  async onUpdateTag({ ctx, payload }: { ctx: Vue; payload: any }) {
    try {
      const { $api, $store, $pushNotification } = ctx;
      await $api.gallery.updateTag(payload.storeId, payload.name);
      $pushNotification.success("Update tag successfully");
      await $store.dispatch("storeGallery/onGetListTag", { ctx });
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async onDeleteTag({ ctx, storeId }: { ctx: Vue; storeId: string }) {
    try {
      const { $api, $store, $pushNotification } = ctx;
      await $api.gallery.deleteTag(storeId);
      $pushNotification.success("Delete tag successfully");
      await $store.dispatch("storeGallery/onGetListTag", { ctx });
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async onUploadImageGallery({
    ctx,
    payload,
    storeId,
  }: {
    ctx: Vue;
    payload: any;
    storeId: any;
  }) {
    try {
      const { $api, $store, $pushNotification, $axios } = ctx;

      await $axios.post(
        `${api}/${resources.storeGallery}/create/${storeId}`,
        payload
      );
      // $store.commit('storeGallery/setProgress', 0)
      $store.dispatch("storeGallery/onGetListImgCategory", { ctx, storeId });
    } catch (error: any) {
      console.log("error :>> ", error);
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async onGetListImgCategory({ ctx, storeId }: { ctx: Vue; storeId: string }) {
    try {
      const { $api, $store, $pushNotification } = ctx;
      const response = await $api.storeGallery.getList(storeId);
      if (response) {
        const listGallery = response.data;
        $store.commit("storeGallery/setListGallery", listGallery);
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }
}

export default StoreGalleryModule;
