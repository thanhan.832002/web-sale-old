import Vue from "vue";
import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";

import { apiErrorHandler } from "~/utils/apiHanlers";

@Module({
  name: "cart",
  namespaced: true,
  stateFactory: true,
})
class CartModule extends VuexModule {
  publicKeyPaypal = "";
  publicKeyStripe = "";
  listPayment = [];
  policy = [];

  cartInfo: any = {}; // Thông tin cart đã tạo
  cartId = "";
  cart = {}; // Cart giỏ hàng
  listCart = [];
  cartInfoAdded: any = {}; // Thông tin cart vừa thêm vào giỏ hàng

  loading = false;
  loadingTable = false;

  @Mutation
  setCart(cart: any) {
    this.cart = cart;
  }

  @Mutation
  setCartInfo(cartInfo: any) {
    if (cartInfo) {
      this.cartInfo = cartInfo;
      this.cartId = cartInfo._id;
    }
  }

  @Mutation
  setCartInfoAdded(cartInfoAdded: any) {
    if (cartInfoAdded) {
      this.cartInfoAdded = cartInfoAdded;
    }
  }

  @Mutation
  setListCart(listCart: any) {
    this.listCart = listCart;
  }

  @Mutation
  setValueBoolane({ value, type }: { value: boolean; type: string }) {
    if (type === "loadingTable") {
      this.loadingTable = value;
    } else if (type === "loading") {
      this.loading = value;
    }
  }

  @Mutation
  setPublicKey(listPayment: any) {
    if (listPayment && listPayment.length) {
      const findIndexPaypal = listPayment.findIndex(
        (payment: any) => payment.type === "paypal"
      );
      if (findIndexPaypal !== -1) {
        this.publicKeyPaypal =
          listPayment[findIndexPaypal]?.paymentId?.publicKey || "";
      }
      const findIndexStripe = listPayment.findIndex(
        (payment: any) => payment.type === "stripe"
      );
      if (findIndexStripe !== -1) {
        this.publicKeyStripe =
          listPayment[findIndexStripe]?.paymentId?.publicKey || "";
      }
      this.policy = listPayment[0]?.paymentId?.policyId || {};
      this.listPayment = listPayment;
    }
  }

  @Mutation
  setListPayment(listPayment: any) {
    if (listPayment && listPayment.length) {
      const findIndexPaypal = listPayment.findIndex(
        (payment: any) => payment.type === "paypal"
      );
      if (findIndexPaypal !== -1) {
        this.publicKeyPaypal =
          listPayment[findIndexPaypal]?.paymentId?.publicKey || "";
      }
      const findIndexStripe = listPayment.findIndex(
        (payment: any) => payment.type === "stripe"
      );
      if (findIndexStripe !== -1) {
        this.publicKeyStripe =
          listPayment[findIndexStripe]?.paymentId?.publicKey || "";
      }
      this.policy = listPayment[0]?.paymentId?.policyId || {};
    }
    this.listPayment = listPayment;
  }

  @Action({ rawError: true })
  async createCart({ ctx }: { ctx: Vue }) {
    try {
      const { $store, $api } = ctx;
      const response = await $api.cart.create();
      if (response.data) {
        $store.commit("cart/setCartInfo", response.data);
        localStorage.setItem("cartInfo", JSON.stringify(response.data));
        ctx.$cookies.set("cartInfo", response.data);
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async getListCart({ ctx, cartId }: { ctx: Vue; cartId: string }) {
    try {
      const { $store, $api } = ctx;
      $store.commit("cart/setValueBoolane", {
        value: true,
        type: "loadingTable",
      });
      const response = await $api.cart.getMyCart(cartId);
      if (response?.data) {
        $store.commit("cart/setCart", response.data);
      }
    } catch (error) {
      ctx.$store.dispatch("cart/createCart", { ctx });
      // apiErrorHandler(error, ctx)
    } finally {
      ctx.$store.commit("cart/setValueBoolane", {
        value: false,
        type: "loadingTable",
      });
    }
  }

  @Action({ rawError: true })
  async addToCart({
    ctx,
    cartId,
    payload,
  }: {
    ctx: Vue;
    cartId: string;
    payload: any;
  }) {
    try {
      const { $store, $api } = ctx;
      $store.commit("cart/setValueBoolane", { value: true, type: "loading" });
      const response = await $api.cart.addToCart(cartId, payload);
    } catch (error) {
      apiErrorHandler(error, ctx);
    } finally {
      ctx.$store.commit("cart/setValueBoolane", {
        value: false,
        type: "loading",
      });
    }
  }

  @Action({ rawError: true })
  async updateCart({
    ctx,
    cartId,
    payload,
  }: {
    ctx: Vue;
    cartId: string;
    payload: any;
  }) {
    try {
      const { $store, $api } = ctx;
      await $api.cart.updateCart(cartId, payload);
      $store.dispatch("cart/getListCart", { ctx, cartId });
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async updateComboCart({
    ctx,
    cartId,
    payload,
  }: {
    ctx: Vue;
    cartId: string;
    payload: any;
  }) {
    try {
      const { $store, $api } = ctx;
      await $api.cart.updateCombocart(cartId, payload);
      $store.dispatch("cart/getListCart", { ctx, cartId });
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async deleteCart({
    ctx,
    cartId,
    payload,
  }: {
    ctx: Vue;
    cartId: string;
    payload: any;
  }) {
    try {
      const { $store, $api } = ctx;
      await $api.cart.deleteCart(cartId, payload);
      $store.dispatch("cart/getListCart", { ctx, cartId });
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }
  @Action({ rawError: true })
  async deleteComboCart({
    ctx,
    cartId,
    comboId,
  }: {
    ctx: Vue;
    cartId: string;
    comboId: string;
  }) {
    try {
      const { $store, $api } = ctx;
      await $api.cart.deleteComboCart(cartId, comboId);
      $store.dispatch("cart/getListCart", { ctx, cartId });
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }
}

export default CartModule;
