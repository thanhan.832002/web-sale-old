import Vue from "vue";
import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";

import { apiErrorHandler } from "~/utils/apiHanlers";

import * as _ from "lodash";
@Module({
  name: "adminOrder",
  namespaced: true,
  stateFactory: true,
})
class AdminOrderModule extends VuexModule {
  listOrder: any = [];
  listAllOrder: any = [];
  listAllExportOrder: any = [];
  listFilter: any = {
    orderStatus: [],
    shippingStatus: [],
    other: {},
  };

  config = {
    page: 1,
    limit: 25,
    search: "",
  };
  totalPage: number = 1;
  page = 1;
  total = 0
  searchBy: string = "emailPhone";
  listIds: string[] = [];
  loadingTable = false;

  @Mutation
  setListAllExportOrder(listAllExportOrder: any) {
    this.listAllExportOrder = listAllExportOrder;
  }

  @Mutation
  setLoadingTable(value: boolean) {
    this.loadingTable = value;
  }
  @Mutation
  setSetConfig() {
    this.listOrder = [];
    this.listAllOrder = [];
    this.listAllExportOrder = [];
    this.listFilter = {
      orderStatus: [],
      shippingStatus: [],
      other: {},
    };

    this.config = {
      page: 1,
      limit: 25,
      search: "",
    };
    this.totalPage = 1;
    this.total = 0
    this.page = 1;
    this.searchBy = "emailPhone";
    this.listIds = [];
  }

  @Mutation
  setSearchBy(search: string) {
    this.searchBy = search;
  }

  @Mutation
  setListIds(value: string[]) {
    this.listIds = value;
  }

  @Mutation
  setPage(page: number) {
    this.config.page = page;
  }

  @Mutation
  setLimit(limit: number) {
    this.config.limit = limit;
  }

  @Mutation
  setSearch(search: string) {
    this.config.search = search;
  }

  @Mutation
  setListFilter({ filter, status }: { filter: number[]; status: string }) {
    if (status === "orderStatus") {
      this.listFilter.orderStatus = filter;
    } else if (status === "shippingStatus") {
      this.listFilter.shippingStatus = filter;
    }
  }

  @Mutation
  setListFilterOther(value: any) {
    // this.listFilter.other = {}
    this.listFilter.other = value;
  }

  @Mutation
  setListOrder(listOrder: string) {
    this.listOrder = listOrder;
  }

  @Mutation
  setListAllOrder(listOrder: any) {
    this.listAllOrder.push(...listOrder);
    this.listAllOrder = _.uniqBy(this.listAllOrder, "_id");
  }

  @Mutation
  setTotalPage(totalPage: number) {
    this.totalPage = totalPage;
  }

  @Mutation
  setTotal(total: number) {
    this.total = total
  }

  @Mutation
  setPageRedirect(page: number) {
    this.page = page;
  }

  @Mutation
  setFilterStatus({ value, type }: { value: number; type: string }) {
    if (type === "orderStatus") {
      this.listFilter.orderStatus.splice(value, 1);
    } else if (type === "shippingStatus") {
      this.listFilter.shippingStatus.splice(value, 1);
    }
  }

  @Action
  async onGetListOrder({ ctx }: { ctx: Vue }) {
    const { $store, $api } = ctx;
    try {
      $store.commit("adminOrder/setTotal", 0)
      $store.commit("adminOrder/setLoadingTable", true);
      const timezone = $store.state.setting.timezone;
      const response = await $api.order.getListOrder({
        ...this.config,
        filter: this.listFilter,
        searchBy: this.searchBy,
        timezone,
      });

      const { total } = response.data;
      if (response?.data?.data) {
        $store.commit("adminOrder/setListOrder", response.data.data);
        $store.commit("adminOrder/setListAllOrder", response.data.data);
        $store.commit(
          "adminOrder/setTotalPage",
          Math.ceil(total / this.config.limit)
        );
        $store.commit("adminOrder/setTotal", total)
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    } finally {
      $store.commit("adminOrder/setLoadingTable", false);
    }
  }

  @Action
  async onGetAllList({ ctx }: { ctx: Vue }) {
    const { $store, $api } = ctx;
    try {
      const timezone = $store.state.setting.timezone;
      const response = await $api.order.getListExportOrder({
        ...this.config,
        filter: this.listFilter,
        searchBy: this.searchBy,
        timezone,
      });
      if (response.data) {
        $store.commit("adminOrder/setListAllExportOrder", response.data);
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }
}

export default AdminOrderModule;
