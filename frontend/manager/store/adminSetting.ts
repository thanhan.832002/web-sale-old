import Vue from "vue";
import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";

import { apiErrorHandler } from "~/utils/apiHanlers";
import collectionApi from "~/api/collection";

@Module({
  name: "adminSetting",
  namespaced: true,
  stateFactory: true,
})
class AdminSettingModule extends VuexModule {
  listAbandon = [];
  loadingTable = false;

  @Mutation
  setListAbandon(list: any) {
    this.listAbandon = list;
  }

  @Mutation
  setLoadingTable(value: boolean) {
    this.loadingTable = value;
  }

  @Action({ rawError: true })
  async onGetListAbandon({ ctx }: { ctx: Vue }) {
    const { $api, $store } = ctx;
    try {
      $store.commit("adminProduct/setLoadingFullPage", true);
      const response = await $api.abandon.get();
      if (response?.data) {
        $store.commit("adminSetting/setListAbandon", response.data);
        return response.data;
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    } finally {
      $store.commit("adminProduct/setLoadingFullPage", false);
    }
  }
}

export default AdminSettingModule;
