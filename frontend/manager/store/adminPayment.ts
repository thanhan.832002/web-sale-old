import Vue from "vue";
import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";
import { apiErrorHandler, redirectPage } from "~/utils/apiHanlers";
import { IPayment, IQuery } from "~/interfaces/common";
@Module({
  name: "adminPayment",
  namespaced: true,
  stateFactory: true,
})
class AdminPaymentModule extends VuexModule {
  totalPage = 0;
  total = 0;

  listPayment = [];
  listAdminSeller = [
    {
      name: "Olivia Rhye",
      email: "olivia@untitledui.com",
      amount: 2788,
      date: "12:01, Jan 6, 2022",
    },
    {
      name: "Olivia Rhye",
      email: "olivia@untitledui.com",
      amount: 2788,
      date: "12:01, Jan 6, 2022",
    },
    {
      name: "Olivia Rhye",
      email: "olivia@untitledui.com",
      amount: 2788,
      date: "12:01, Jan 6, 2022",
    },
    {
      name: "Olivia Rhye",
      email: "olivia@untitledui.com",
      amount: 2788,
      date: "12:01, Jan 6, 2022",
    },
  ];

  loadingTable = false;
  loadingTableSeller = false;
  loading = false;

  dialogAddPayment = false;
  dialogDuplicatePayment = false;

  @Mutation
  setListPayment({
    listPayment,
    totalPage,
  }: {
    listPayment: any;
    totalPage: number;
  }) {
    this.listPayment = listPayment;
    this.totalPage = totalPage;
  }

  @Mutation
  setListAdminSeller({
    listAdminSeller,
    totalPage,
    total,
  }: {
    listAdminSeller: any;
    totalPage: number;
    total: number;
  }) {
    this.listAdminSeller = listAdminSeller;
    this.totalPage = totalPage;
    this.total = total;
  }

  @Mutation
  setDialogAddPayment(value: boolean) {
    this.dialogAddPayment = value;
  }

  @Mutation
  setDialogDuplicatePayment(value: boolean) {
    this.dialogDuplicatePayment = value;
  }

  @Mutation
  setLoading({ value, type }: { value: boolean; type: string }) {
    switch (type) {
      case "loadingTable":
        this.loadingTable = value;
        break;
      case "loading":
        this.loading = value;
        break;
      default:
        this.loadingTable = false;
        this.loading = false;
    }
  }

  @Action({ rawError: true })
  async onGetListPayment({ ctx, query }: { ctx: Vue; query: IQuery }) {
    const { $api, $store } = ctx;
    try {
      $store.commit("adminPayment/setLoading", {
        value: true,
        type: "loadingTable",
      });
      const response = await $api.payment.list(query);
      if (response?.data?.data) {
        const { total } = response.data;
        const totalPage = Math.ceil(total / query.limit);
        $store.commit("adminPayment/setListPayment", {
          listPayment: response.data.data,
          totalPage,
        });
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    } finally {
      $store.commit("adminPayment/setLoading", {
        value: false,
        type: "loadingTable",
      });
    }
  }

  @Action({ rawError: true })
  async onAddPayment({
    ctx,
    payload,
    query,
    type,
  }: {
    ctx: Vue;
    payload: IPayment;
    query: IQuery;
    type: string;
  }) {
    const { $api, $store, $pushNotification } = ctx;
    try {
      $store.commit("adminPayment/setLoading", {
        value: true,
        type: "loading",
      });
      await $api.payment.create(payload);
      $store.commit("adminPayment/onGetListPayment", { ctx, query });
      $store.commit("adminPayment/setDialogAddPayment", false);
      $store.commit("adminPayment/setDialogDuplicatePayment", false);
      if (type === "create") {
        $pushNotification.success("Create payment successfully");
      } else {
        $pushNotification.success("Duplicate payment successfully");
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    } finally {
      $store.commit("adminPayment/setLoading", {
        value: false,
        type: "loading",
      });
    }
  }

  @Action({ rawError: true })
  async onGetListAdminSeller({
    ctx,
    query,
    paymentId,
  }: {
    ctx: Vue;
    query: IQuery;
    paymentId: string;
  }) {
    const { $api, $store } = ctx;
    try {
      $store.commit("adminPayment/setLoading", {
        value: true,
        type: "loadingTable",
      });
      const response = await $api.payment.lisAdmintSeller(paymentId, query);
      if (response?.data?.data) {
        const { total } = response.data;
        const totalPage = Math.ceil(total / query.limit);
        $store.commit("adminPayment/setListAdminSeller", {
          listAdminSeller: response.data.data,
          totalPage,
          total,
        });
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    } finally {
      $store.commit("adminPayment/setLoading", {
        value: false,
        type: "loadingTable",
      });
    }
  }
}

export default AdminPaymentModule;
