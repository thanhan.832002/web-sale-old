import { Module, VuexModule, Mutation } from "vuex-module-decorators";

import _ from "lodash";
@Module({
  name: "product",
  stateFactory: true,
  namespaced: true,
})
class ProductModule extends VuexModule {
  language = {};
  dialogAddToCart = false;
  dialogAdded = false;
  dialogAddedBestSeller = false;
  dialogAddedRecently = false;
  dialogAddedProduct = false;

  loadingTable = false;
  loading = false;
  loadingPaypal = false;

  product = {};
  productId = "";

  listRelatedProduct = [];
  listReviewProduct = [];

  isCorrect = false;

  @Mutation
  checkIsCorrectPassword() {
    this.isCorrect = true;
  }

  @Mutation
  setLanguage(language: any) {
    this.language = language;
  }

  @Mutation
  setLoadingPaypal(loadingPaypal: boolean) {
    this.loadingPaypal = loadingPaypal;
  }

  @Mutation
  setValueBoolane({ status, type }: { status: boolean; type: string }) {
    if (type === "loading") {
      this.loading = status;
    } else if (type === "loadingTable") {
      this.loadingTable = status;
    }
  }

  @Mutation
  setDialogAddToCart(value: boolean, type: string) {
    this.dialogAddToCart = value;
  }

  @Mutation
  setProduct(product: any) {
    this.product = product;
    this.productId = product._id;
    // product.relatedProduct.forEach((product: any) => {
    //     if (product.variant.length) {
    //         product.variantId = product.variant[0]._id;
    //     }
    // });
  }

  @Mutation
  setDialogAdded({ value, type }: { value: boolean; type: string }) {
    if (type === "product") {
      this.dialogAddedProduct = value;
    } else if (type === "recently") {
      this.dialogAddedRecently = value;
    } else if (type === "bestSeller") {
      this.dialogAddedBestSeller = value;
    }
  }

  @Mutation
  setListRelatedProduct(listRelatedProduct: any) {
    this.listRelatedProduct = listRelatedProduct;
  }

  @Mutation
  setListReviewProduct(listReviewProduct: any) {
    this.listReviewProduct = listReviewProduct;
  }
}

export default ProductModule;
