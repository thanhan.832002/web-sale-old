import Vue from "vue";
import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";

import { apiErrorHandler } from "~/utils/apiHanlers";

import { IQuery } from "~/interfaces/common";
@Module({
  name: "adminUsers",
  namespaced: true,
  stateFactory: true,
})
class AdminUsersModule extends VuexModule {
  listAdminSeller = [];
  totalPage = 0;

  sellerId = "";
  listStore = [];
  listPayment = [];

  @Mutation
  setSellerId(sellerId: string) {
    this.sellerId = sellerId;
  }

  @Mutation
  setListStore({
    listStore,
    totalPage,
  }: {
    listStore: any;
    totalPage: number;
  }) {
    this.listStore = listStore;
    this.totalPage = totalPage;
  }

  @Mutation
  setListPayment({
    listPayment,
    totalPage,
  }: {
    listPayment: any;
    totalPage: number;
  }) {
    this.listPayment = listPayment;
    this.totalPage = totalPage;
  }

  @Mutation
  setListAdminSeller({
    listAdminSeller,
    totalPage,
  }: {
    listAdminSeller: any;
    totalPage: number;
  }) {
    this.listAdminSeller = listAdminSeller;
    this.totalPage = totalPage;
  }

  @Action({ rawError: true })
  async onGetListAdminSeller({ ctx, query }: { ctx: Vue; query: IQuery }) {
    const { $api, $store } = ctx;
    try {
      const response = await $api.userAdmin.listSeller(query);
      if (response?.data?.data) {
        const { total } = response.data;
        const totalPage = Math.ceil(total / query.limit);
        $store.commit("adminUsers/setListAdminSeller", {
          listAdminSeller: response.data.data,
          totalPage,
        });
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async onGetStoreSeller({ ctx, query }: { ctx: Vue; query: IQuery }) {
    const { $api, $store } = ctx;
    try {
      const response = await $api.storeAdmin.listSellerStore(query);
      if (response?.data?.data) {
        response.data.data.forEach((store: any) => {
          store["isActive"] = store.status === "active" ? true : false;
        });
        const { total } = response.data;
        const totalPage = Math.ceil(total / query.limit);
        $store.commit("adminUsers/setListStore", {
          listStore: response.data.data,
          totalPage,
        });
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async onGetListPayment({ ctx, sellerId }: { ctx: Vue; sellerId: string }) {
    const { $api, $store } = ctx;
    try {
      const response = await $api.userPaymentAdmin.listPaymentSeller(sellerId);
      if (response?.data) {
        $store.commit("adminUsers/setListPayment", {
          listPayment: response.data,
        });
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }
}

export default AdminUsersModule;
