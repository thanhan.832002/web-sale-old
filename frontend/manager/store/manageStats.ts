import { Module, VuexModule, Mutation } from "vuex-module-decorators";

import _ from "lodash";

const dashboardAdmin = [
  {
    type: "revenue",
    value: 193946.4,
    text: "revenue",
  },
  {
    type: "orders",
    value: 5840,
    text: "Orders",
  },
  {
    type: "productCost",
    value: 56186.63,
    text: "Cost",
  },
  {
    type: "conversionRate",
    value: 3.32,
    text: "Conversion Rate",
  },
  {
    type: "profit",
    value: 13776000.4,
    text: "Profit",
  },
  {
    type: "aov",
    value: 56186.4,
    text: "Aov",
  },
  {
    type: "PCRE",
    value: 28,
    text: "Pc/Re",
  },
  {
    type: "tip",
    value: 0,
    text: "Tip",
  },
];

const dashboardSeller = [
  {
    type: "revenue",
    value: 0,
    text: "Profit",
  },
  {
    type: "orders",
    value: 0,
    text: "Order",
  },
  {
    type: "productCost",
    value: 0,
    text: "Cost",
  },
  {
    type: "conversionRate",
    value: 0,
    text: "CR",
  },
  {
    type: "aov",
    value: 0,
    text: "AOV",
  },
  {
    type: "PCRE",
    value: 0,
    text: "PC/RE",
  },
];

const analyticRevenueSeller = [
  {
    type: "revenue",
    value: 0,
    text: "Revenue",
  },
  {
    type: "orders",
    value: 0,
    text: "PO",
  },
  {
    type: "productCost",
    value: 0,
    text: "Cost",
  },
  {
    type: "aov",
    value: 0,
    text: "AOV",
  },
  {
    type: "conversionRate",
    value: 0,
    text: "CR",
  },
];

const storeSeller = [
  {
    type: "orders",
    value: 0,
    text: "Order",
  },
  {
    type: "revenue",
    value: 0,
    text: "Sales",
  },
  {
    type: "productCost",
    value: 0,
    text: "Product Cost",
  },
  {
    type: "conversionRate",
    value: 0,
    text: "Conversion Rate",
  },
];

@Module({
  name: "manageStats",
  stateFactory: true,
  namespaced: true,
})
class ManageStatsModule extends VuexModule {
  stats: any = null;

  @Mutation
  setStatsEachPage(type: string) {
    switch (type) {
      case "admin-dashboard":
        this.stats = dashboardAdmin;
        break;
      case "seller-dashboard":
        this.stats = dashboardSeller;
        break;
      case "analytic-revenue-seller":
        this.stats = analyticRevenueSeller;
        break;
      case "store-seller":
        this.stats = storeSeller;
        break;
      default:
        this.stats = dashboardAdmin;
        break;
    }
  }

  @Mutation
  setStats(value: any) {
    this.stats = value;
  }

  @Mutation
  parseStats(data: any) {
    this.stats.forEach((item: any) => {
      if (data[item.type] || item.type === "PCRE" || item.type === "profit") {
        if (item.type !== "PCRE" && item.type !== "profit") {
          return (item.value = data[item.type]);
        } else if (item.type === "profit") {
          return (item.value = (data["revenue"] - data["productCost"]) * 100);
        } else {
          if (data["productCost"] / data["revenue"]) {
            return (item.value = (data["productCost"] / data["revenue"]) * 100);
          }
        }
      }
      return (item.value = 0);
    });
  }
}

export default ManageStatsModule;
