import { ActionTree } from "vuex";
import { getModule } from "vuex-module-decorators";
import { Context } from "@nuxt/types/app";
import {
  accessTokenKey,
  refreshTokenKey,
  userInfoKey,
} from "~/common/constaint";
import UserModule from "./user";
import AdminOrderModule from "./adminOrder";
import SellerStoreModule from "./sellerStore";
import AdminPaymentModule from "./adminPayment";
import AdminUsersModule from "./adminUsers";
import MasterUsersModule from "./masterUsers";
import AdminSettingModule from "./adminSetting";
export const state = () => ({});
export type RootState = ReturnType<typeof state>;

export const actions: ActionTree<RootState, RootState> = {
  async nuxtClientInit({ commit }, { store, $localForage, app }: Context) {
    getModule(UserModule, store);
    getModule(SellerStoreModule, store);
    getModule(AdminOrderModule, store);
    getModule(AdminPaymentModule, store);
    getModule(AdminUsersModule, store);
    getModule(MasterUsersModule, store);
    getModule(AdminSettingModule, store);
    try {
      const proms = [
        $localForage.getItem(userInfoKey),
        $localForage.getItem(accessTokenKey),
        $localForage.getItem(refreshTokenKey),
      ];
      const [user, accessToken, refreshToken] = await Promise.all(proms);
      if (user && accessToken && refreshToken) {
        app.$axios.setHeader("Authorization", `Bearer ${accessToken}`);
        //@ts-ignore
        store.commit(`user/Authorization`, { user, accessToken, refreshToken });
      }
    } catch (error) {
      ////console.log(error)
    }
  },
};
