import Vue from "vue";
import { Module, VuexModule, Mutation } from "vuex-module-decorators";

@Module({
  name: "home",
  stateFactory: true,
  namespaced: true,
})
class HomeModule extends VuexModule {
  wheels = 2;
  listData = [];
  listRecent = [];
  listRealtime = [];
  listRevenueDomain = [];
  listRevenueDate = [];
  listRevenueSeller = [];
  listRevenueProduct = [];
  listRevenueHour = [];
  listRevenueMonth = [];
  config = {
    dateFrom: 0,
    dateTo: 0,
  };
  revenueTab = 0;
  loadingRevenueTable = false;
  tab = 0;
  email = {};

  @Mutation
  setEmail(value: any) {
    this.email = value;
  }

  @Mutation
  setTab(value: number) {
    this.tab = value;
  }

  @Mutation
  setLoadingRevenueTable(value: any) {
    this.loadingRevenueTable = value;
  }

  @Mutation
  setRevenueTab(value: any) {
    this.revenueTab = value;
  }

  @Mutation
  setListRevenueDomain(value: any) {
    this.listRevenueDomain = value;
  }
  @Mutation
  setListRevenueDate(value: any) {
    this.listRevenueDate = value;
  }
  @Mutation
  setListRevenueSeller(value: any) {
    this.listRevenueSeller = value;
  }
  @Mutation
  setListRevenueProduct(value: any) {
    this.listRevenueProduct = value;
  }
  @Mutation
  setListRevenueHour(value: any) {
    this.listRevenueHour = value;
  }
  @Mutation
  setListRevenueMonth(value: any) {
    this.listRevenueMonth = value;
  }
  @Mutation
  setConfig(value: any) {
    this.config = value;
  }
  @Mutation
  incrWheels(extra: number) {
    this.wheels += extra;
  }
  @Mutation
  setListData(value: any) {
    this.listData = value;
  }
  @Mutation
  setListRecent(value: any) {
    this.listRecent = value;
  }

  @Mutation
  setListRealtime(value: any) {
    this.listRealtime = value;
  }

  @Mutation
  onSort({
    key,
    tab,
    sortType,
    ctx,
  }: {
    key: string;
    tab: number;
    sortType: string;
    ctx: Vue;
  }) {
    let list: any = [];
    const listRecent = this.listRecent || [];
    const listRealtime = this.listRealtime || [];
    const listRevenueDomain = this.listRevenueDomain || [];
    const listRevenueDate = this.listRevenueDate || [];
    const listRevenueSeller = this.listRevenueSeller || [];
    const listRevenueProduct = this.listRevenueProduct || [];
    const listRevenueHour = this.listRevenueHour || [];
    const listRevenueMonth = this.listRevenueMonth || [];

    if (tab === 0) {
      list = listRecent;
    } else if (tab === 1) {
      list = listRealtime;
    } else {
      if (this.revenueTab == 0) {
        list = listRevenueDomain;
      }
      if (this.revenueTab == 1) {
        list = listRevenueDate;
      }
      if (this.revenueTab == 2) {
        list = listRevenueSeller;
      }
      if (this.revenueTab == 3) {
        list = listRevenueProduct;
      }
      if (this.revenueTab == 4) {
        list = listRevenueHour;
      }
      if (this.revenueTab == 5) {
        list = listRevenueMonth;
      }
    }

    list = ctx.$utils.sortList(list, key, sortType);
  }

  get axles() {
    return this.wheels / 2;
  }
}

export default HomeModule;
