import Vue from "vue";
import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";
import { IAuth, IUser } from "~/interfaces/auth";
import {
  accessTokenKey,
  api,
  refreshTokenKey,
  userInfoKey,
} from "~/common/constaint";
import { apiErrorHandler, redirectPage } from "~/utils/apiHanlers";
import { $localForage } from "~/plugins/localforage.client";
import { Store } from "vuex";

@Module({
  name: "user",
  namespaced: true,
  stateFactory: true,
})
class UserModule extends VuexModule {
  accessToken: String | undefined;
  refreshToken: String | undefined;
  userInfo: any | undefined;

  role: any = 1;

  isRegisterSuccess = false;
  isTwofa = false

  get isLogin() {
    return !!this.accessToken;
  }

  @Mutation
  setIsTwofa(value: boolean) {
    this.isTwofa = value
  }

  @Mutation
  setRegisterSuccess(value: boolean) {
    this.isRegisterSuccess = value;
  }

  @Mutation
  setUser(user: any) {
    this.userInfo = user;
    if (user?.twofa) {
      this.isTwofa = user.twofa.enable
    }
  }

  @Mutation
  async Authorization({
    accessToken,
    refreshToken,
    user,
  }: {
    accessToken: string;
    refreshToken: string;
    user: IUser;
  }) {
    this.accessToken = accessToken;
    this.userInfo = user;
    this.refreshToken = refreshToken;
    this.role = user.role;
  }

  @Mutation
  userLogout() {
    try {
      this.accessToken = "";
      this.refreshToken = "";
      this.userInfo = null;
    } catch (error) { }
  }
  @Mutation
  async setToken({
    accessToken,
    refreshToken,
  }: {
    accessToken: string;
    refreshToken: string;
  }) {
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
  }
  @Mutation
  setAccessToken(accessToken: string) {
    this.accessToken = accessToken;
  }

  @Action
  async login({ ctx, payload }: { ctx: Vue; payload: IAuth }) {
    try {
      const { $api, $store, $axios } = ctx;
      const response = await $api.auth.login(payload);
      let {
        data: {
          //@ts-ignore
          tokens: { accessToken, refreshToken },
          user = null,
        } = {},
      } = response;
      $store.commit("user/Authorization", { accessToken, refreshToken, user });
      $axios.setHeader("Authorization", `Bearer ${accessToken}`);
      const proms = [
        ctx.$localForage.setItem(accessTokenKey, accessToken),
        ctx.$localForage.setItem(refreshTokenKey, refreshToken),
        ctx.$localForage.setItem(userInfoKey, user),
      ];
      await Promise.all(proms);
      localStorage.setItem("accessToken", accessToken);
      localStorage.setItem("refreshToken", refreshToken);
      ctx.$pushNotification.success("Login successfully");
      ctx.$store.dispatch("user/onGetMe", { ctx });
      // redirectPage(`/admin/dashboard`);
      if (response?.data?.user?.role === 1) {
        return redirectPage(`/admin/dashboard`);
      } else if (response?.data?.user?.role === 2) {
        return redirectPage(`/seller/dashboard`);
      } else {
        return redirectPage(`/master/manage-user`);
      }
    } catch (error: any) {
      const message = error?.response?.data?.message || ""
      if (message === "MISSING_2FA") {
        this.setIsTwofa(true)
      }
      console.log('error :>> ', error);
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async register({ ctx, payload }: { ctx: Vue; payload: any }) {
    try {
      const { $api, $store } = ctx;
      const response = await $api.auth.register(payload);
      $store.commit("user/setRegisterSuccess", true);
      ctx.$pushNotification.success("Create account successfully");
      // ctx.$router.push('/auth/login')
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async renewAccessToken({ store }: { store: Store<any> }) {
    try {
      //@ts-ignore
      const { $axios, $api, $localForage } = this.store;
      const refreshToken = await $localForage.getItem(refreshTokenKey);
      if (!refreshToken) {
        return this.logout({ store });
      }
      const response = await $axios.$post("/auth/renew-token", {
        refreshToken,
      });
      let accessToken;
      if (response?.data?.token) {
        accessToken = response.data.token;
      } else {
        throw new Error();
      }
      store.commit("user/setAccessToken", accessToken);
      $axios.setHeader("Authorization", `Bearer ${accessToken}`);
      await $localForage.setItem(accessTokenKey, accessToken);
      await localStorage.setItem(accessTokenKey, accessToken);
      return accessToken;
    } catch (error) {
      this.logout({ store });
      throw error;
    }
  }
  @Action({ rawError: true })
  async onGetMe({ ctx }: { ctx: Vue }) {
    try {
      const { $api, $store } = ctx;
      const response = await $api.user.getMe();
      if (response?.data) {
        const user = response.data;
        $store.commit("user/setUser", user);
        await $localForage.setItem(userInfoKey, user);
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
      throw error;
    }
  }

  @Action({ rawError: true })
  async onGetSettingPage({ ctx }: { ctx: Vue }) {
    try {
      //@ts-ignore
      const { store, $api, app } = ctx;
      const response = await $api.setting.getSetting();
      if (response?.data) {
        const { logo } = response.data;
        if (logo) {
          app.head.link[0].href = `${api}/${logo}`;
        }
        app.head.title = response.data.webTitle || "Web Sale Premium";
        store.commit("setting/setSiteInfo", response.data);
        store.commit("setting/setTimezone", response.data.timezone);
      }
    } catch (error) {
      console.log("error :>> ", error);
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async logout({ store }: { store: Store<any> }) {
    try {
      await $localForage.setItem(accessTokenKey, null);
      await $localForage.setItem(refreshTokenKey, null);
      await $localForage.setItem(userInfoKey, null);
      localStorage.setItem("accessToken", "");
      localStorage.setItem("refreshToken", "");
      localStorage.setItem("passProduct", "");
      localStorage.setItem("isPasswordAdmin", "");
      store.commit("user/userLogout");
      redirectPage("/auth/login");
    } catch (error) {
      console.log("error :>> ", error);
    }
  }
}

export default UserModule;
