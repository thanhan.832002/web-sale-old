import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { apiErrorHandler } from '~/utils/apiHanlers';

@Module({
    name: 'promotion',
    stateFactory: true,
    namespaced: true,
})
class PromotionModule extends VuexModule {

    promotion: any = {}
    promotionStauts = 0

    @Mutation
    setPromotion(payload: any) {
        this.promotion = payload
        this.promotionStauts = payload.status
    }

    @Action({ rawError: true })
    async getPromotionDetail({ ctx }: { ctx: Vue }) {
        try {
            //@ts-ignore
            const { $store } = ctx
            const response = await ctx.$api.promotion.get()
            if (response.data) {
                ctx.$store.commit('promotion/setPromotion', response.data)
            }
        } catch (error) {
            apiErrorHandler(error, ctx)
        }
    }

}

export default PromotionModule