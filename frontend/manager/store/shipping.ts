import { Module, VuexModule, Mutation } from "vuex-module-decorators";

@Module({
  name: "shipping",
  stateFactory: true,
  namespaced: true,
})
class ShippingModule extends VuexModule {
  shippingName = "true";
}

export default ShippingModule;
