import Vue from "vue";
import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";
import { clientLanguageKey } from "@/utils/common";
import { $localForage } from "~/plugins/localforage.client";
import { apiErrorHandler } from "~/utils/apiHanlers";
import { clone, cloneDeep } from "lodash";
import { api, regexEmail } from "~/common/constaint";
import { IStoreProductDisplay, IStoreUpsale } from "~/interfaces/store";
import { IItemListDiscount, ISellerDiscount } from "~/interfaces/discount";
import { getTimeZone } from "~/utils/timeZone";
var moment = require("moment");
var momentz = require("moment-timezone");
function deepEqual(obj1: any, obj2: any) {
  return JSON.stringify(obj1) === JSON.stringify(obj2);
}
@Module({
  name: "sellerStore",
  stateFactory: true,
  namespaced: true,
})
export default class SellerProductCategoryStore extends VuexModule {
  storeList = [];
  store: any = {
    product: {},
    store: {
      subDomain: "",
      mainDomain: "",
    },
    storeSetting: {},
  };
  cloneStore: any = {
    product: {},
    store: {
      subDomain: "",
      mainDomain: "",
    },
    storeSetting: {},
  };

  isChanged = false;
  productList = [];
  imageSrc = "";

  listStoreId: string[] = [];
  status = false;
  sellerDiscountId = "";

  pixel: any = {
    twitterPixel: {
      addToCartId: "",
      addedPaymentInfoId: "",
      checkoutInitiatedId: "",
      contentViewId: "",
      pageViewId: "",
      purchaseId: "",
      twitterPixelId: "",
    },
    googleAds: {
      googleAnalyticId: "",
      googleTagManager: "",
    },
    facebookPixel: { contentId: "", facebookPixelId: "" },
    tiktokPixel: {
      tikTokPixelId: "",
    },
    snapPixel: {
      snapPixelId: "",
    },
  };

  clonePixel: any = {
    twitterPixel: {
      addToCartId: "",
      addedPaymentInfoId: "",
      checkoutInitiatedId: "",
      contentViewId: "",
      pageViewId: "",
      purchaseId: "",
      twitterPixelId: "",
    },
    googleAds: {
      googleAnalyticId: "",
      googleTagManager: "",
    },
    facebookPixel: { contentId: "", facebookPixelId: "" },
    tiktokPixel: {
      tikTokPixelId: "",
    },
    snapPixel: {
      snapPixelId: "",
    },
  };

  storeId = "";
  coverId = "";
  imageUrl = "";

  orderDetail = {};
  drawer = false;
  listProductAdmin = false;
  loadingTableStore = false;
  isNotChangeStore = true;
  isNotChangePixel = true;
  isNotChangeListImage = true;
  isNotChangeMinimumRequire = true;

  discount = {
    code: "",
    discount: 0,
    status: "true",
    discountType: "1",
    minAmount: 0,
    minQuantity: 0,
  };

  cloneDiscount = {};

  checkboxMinimum = "1";
  cloneCheckboxMinimum = "1";
  isNotChangeCheckboxMinimum = true;
  minimumRequire = 0;
  cloneMinimumRequire = 0;
  discountId = "";
  listMinimumRequire: IItemListDiscount[] = [];
  cloneListMinimumRequire: any = [];
  isShowDateEnd = false;
  cloneIsShowDateEnd = false;
  isNotChangeShowDateEnd = true;
  discountCheck = {
    code: "",
    discount: 0,
    status: "true",
    discountType: "1",
    minAmount: 0,
    minQuantity: 0,
  };

  dateStart = "";
  cloneDateStart = "";
  dateEnd = "";

  cloneDateEnd = "";
  timePickerStart = "";
  cloneTimePickerStart = "";
  timePickerEnd = "";
  cloneTimePickerEnd = "";

  loading = false;
  isNotChangeDiscount = true;
  isNotChangeListMinimum = true;
  isNotChangeDateStart = true;
  isNotChangeDateEnd = true;
  isNotChangeTimePickerStart = true;
  isNotChangeTimePickerEnd = true;

  @Mutation
  setIsNotChangeDateStart(value: boolean) {
    this.isNotChangeDateStart = value;
  }

  @Mutation
  setIsNotChangeDateEnd(value: boolean) {
    this.isNotChangeDateEnd = value;
  }

  @Mutation
  setIsNotChangeTimePickerStart(value: boolean) {
    this.isNotChangeTimePickerStart = value;
  }

  @Mutation
  setIsNotChangeTimePickerEnd(value: boolean) {
    this.isNotChangeTimePickerEnd = value;
  }

  @Mutation
  setIsNotChangeListMinimum(value: boolean) {
    this.isNotChangeListMinimum = value;
  }
  @Mutation
  setLoading(value: boolean) {
    this.loading = value;
  }

  @Mutation
  setIsChanged(value: boolean) {
    this.isChanged = value;
  }

  @Mutation
  setTimePickerStart(value: string) {
    this.timePickerStart = value;
  }

  @Mutation
  setTimePickerEnd(value: string) {
    this.timePickerEnd = value;
  }

  @Mutation
  setDateStart(value: string) {
    this.dateStart = value;
  }
  @Mutation
  setCloneDateStart() {
    this.cloneDateStart = cloneDeep(this.dateStart);
  }
  @Mutation
  setCloneDateEnd() {
    this.cloneDateStart = cloneDeep(this.dateEnd);
  }

  @Mutation
  setCloneTimePickerStart() {
    this.cloneTimePickerStart = cloneDeep(this.timePickerStart);
  }

  @Mutation
  setCloneTimePickerEnd() {
    this.cloneTimePickerEnd = cloneDeep(this.timePickerEnd);
  }

  @Mutation
  setDateEnd(value: string) {
    this.dateEnd = value;
  }

  @Mutation
  setDiscountCheck(value: any) {
    this.discountCheck = value;
  }

  @Mutation
  setIsShowDateEnd(value: boolean) {
    this.isShowDateEnd = value;
  }

  @Mutation
  setCloneIsShowDateEnd() {
    this.cloneIsShowDateEnd = cloneDeep(this.isShowDateEnd);
  }

  @Mutation
  setIsNotChangeShowDateEnd(value: boolean) {
    this.isNotChangeShowDateEnd = value;
  }

  @Mutation
  setCheckboxMinimum(value: string) {
    this.checkboxMinimum = value;
  }

  @Mutation
  setCloneCheckboxMinimum() {
    this.cloneCheckboxMinimum = cloneDeep(this.checkboxMinimum);
  }

  @Mutation
  setIsNotChangeCheckboxMinimum(value: boolean) {
    this.isNotChangeCheckboxMinimum = value;
  }
  @Mutation
  setMinimumRequire(value: number) {
    this.minimumRequire = value;
  }

  @Mutation
  setCloneMinimumRequire() {
    this.cloneMinimumRequire = cloneDeep(this.minimumRequire);
  }

  @Mutation
  setDiscountId(value: string) {
    this.discountId = value;
  }
  @Mutation
  setListMinimumRequire(value: any) {
    this.listMinimumRequire = value;
  }

  @Mutation
  setCloneListMinimumRequire() {
    this.cloneListMinimumRequire = cloneDeep(this.listMinimumRequire);
  }

  @Mutation
  setDiscount(value: any) {
    this.discount = { ...value };
  }

  @Mutation
  setCloneDiscount(result: any) {
    this.cloneDiscount = cloneDeep(result);
  }

  @Mutation
  setIsNotChangeListImage(value: boolean) {
    this.isNotChangeListImage = value;
  }

  @Mutation
  setIsNotChangePixel(value: boolean) {
    this.isNotChangePixel = value;
  }

  @Mutation
  setIsNotChangeStore(value: boolean) {
    this.isNotChangeStore = value;
  }

  @Mutation
  setIsNotChangeDiscount(value: boolean) {
    this.isNotChangeDiscount = value;
  }

  @Mutation
  setIsNotChangeMinimumRequire(value: boolean) {
    this.isNotChangeMinimumRequire = value;
  }

  @Mutation
  setLoadingTableStore(value: boolean) {
    this.loadingTableStore = value;
  }

  @Mutation
  setListProductAdmin(value: boolean) {
    this.listProductAdmin = value;
  }

  @Mutation
  setSubDomain(value: any) {
    this.store.store.subDomain = value;
  }
  @Mutation
  setMainDomain(value: any) {
    this.store.store.mainDomain = value;
  }
  @Mutation
  setListStoreId(list: string[]) {
    this.listStoreId = list;
  }

  @Mutation
  setStatusUpsale(status: boolean) {
    this.status = status;
  }

  @Mutation
  setSellerDiscountId(id: string) {
    this.sellerDiscountId = id;
  }

  @Mutation
  setOrderDetail(value: any) {
    this.orderDetail = value;
  }
  @Mutation
  setDrawer(value: any) {
    this.drawer = value;
  }
  @Mutation
  setCoverId({ coverId, imageUrl }: { coverId: string; imageUrl: string }) {
    this.coverId = coverId;
    this.imageUrl = imageUrl;
  }

  @Mutation
  setStoreId(value: any) {
    this.storeId = value;
  }

  @Mutation
  setPixel(value: any) {
    if (value) {
      this.pixel = value;
    }
  }

  @Mutation
  setGoogleAds(value: any) {
    this.pixel.googleAds = { ...this.pixel.googleAds, ...value };
    this.isNotChangePixel = deepEqual(this.pixel, this.clonePixel);
  }

  @Mutation
  setTiktokPixel(value: any) {
    this.pixel.tiktokPixel = { ...this.pixel.tiktokPixel, ...value };
    this.isNotChangePixel = deepEqual(this.pixel, this.clonePixel);
  }

  @Mutation
  setSnapPixel(value: any) {
    this.pixel.snapPixel = { ...this.pixel.snapPixel, ...value };
    this.isNotChangePixel = deepEqual(this.pixel, this.clonePixel);
  }

  @Mutation
  setTwitterPixel(value: any) {
    this.pixel.twitterPixel = { ...this.pixel.twitterPixel, ...value };
    this.isNotChangePixel = deepEqual(this.pixel, this.clonePixel);
  }

  @Mutation
  setFacebookPixel(value: any) {
    this.pixel.facebookPixel = { ...this.pixel.facebookPixel, ...value };
    this.isNotChangePixel = deepEqual(this.pixel, this.clonePixel);
  }

  @Mutation
  setStoreList(value: any) {
    this.storeList = value;
  }
  @Mutation
  setProductList(value: any) {
    this.productList = value;
  }

  @Mutation
  setStoreStore(value: any) {
    this.store.store = { ...this.store.store, ...value };
    this.isNotChangeStore = deepEqual(this.store, this.cloneStore);
  }

  @Mutation
  setStoreProduct(value: any) {
    this.store.product = { ...this.store.product, ...value };
    this.isNotChangeStore = deepEqual(this.store, this.cloneStore);
  }

  @Mutation
  setStoreSetting(value: any) {
    this.store.storeSetting = { ...this.store.storeSetting, ...value };
    this.isNotChangeStore = deepEqual(this.store, this.cloneStore);
  }

  @Mutation
  setStore(value: any) {
    this.store = value;
  }

  @Mutation
  setCloneStore() {
    this.cloneStore = cloneDeep(this.store);
  }

  @Mutation
  setClonePixel() {
    this.clonePixel = cloneDeep(this.pixel);
  }

  @Action({ rawError: true })
  async onGetDiscount({
    ctx,
    dontLoading,
  }: {
    ctx: Vue;
    dontLoading?: boolean;
  }) {
    const { $store, $api } = ctx;
    try {
      const storeId = $store.state.sellerStore?.storeId || "";
      const discountStore = $store.state.sellerStore?.discount || {};
      $store.commit("sellerStore/setDiscount", {});
      if (!storeId) return;
      if (!dontLoading) {
        $store.commit("sellerStore/setLoadingTableStore", true);
      }
      const response = await $api.discounts.detail(storeId);
      if (response?.data) {
        const {
          code,
          dateFrom,
          dateTo,
          discount,
          discountType,
          minAmount,
          minQuantity,
          multipleReq,
          status,
        } = response.data;

        if (minAmount === 0 && minQuantity === 0) {
          $store.commit("sellerStore/setCheckboxMinimum", "1");
          $store.commit("sellerStore/setCloneCheckboxMinimum");
          $store.commit("sellerStore/setMinimumRequire", 0);
          $store.commit("sellerStore/setCloneMinimumRequire");
        }
        if (minAmount > 0) {
          $store.commit("sellerStore/setCheckboxMinimum", "2");
          $store.commit("sellerStore/setCloneCheckboxMinimum");
          $store.commit("sellerStore/setMinimumRequire", minAmount);
          $store.commit("sellerStore/setCloneMinimumRequire");
        }
        if (minQuantity > 0) {
          $store.commit("sellerStore/setCheckboxMinimum", "3");
          $store.commit("sellerStore/setCloneCheckboxMinimum");
          $store.commit("sellerStore/setMinimumRequire", minQuantity);
          $store.commit("sellerStore/setCloneMinimumRequire");
        }
        $store.commit("sellerStore/setDiscount", {
          code,
          discount,
          discountType: `${discountType}`,
          minAmount,
          minQuantity,
          status: `${status}`,
        });

        $store.commit("sellerStore/setCloneDiscount", {
          code,
          discount,
          discountType: `${discountType}`,
          minAmount,
          minQuantity,
          status: `${status}`,
        });

        $store.commit("sellerStore/setDiscountCheck", discountStore);

        $store.commit("sellerStore/setListMinimumRequire", multipleReq);

        $store.commit("sellerStore/setCloneListMinimumRequire", multipleReq);

        const siteInfo = $store.state.setting.siteInfo;
        const timezone = getTimeZone(siteInfo.timezone);
        if (dateFrom) {
          $store.commit(
            "sellerStore/setDateStart",
            momentz.unix(dateFrom).tz(timezone).format("YYYY-MM-DD")
          );
          $store.commit("sellerStore/setCloneDateStart");
          $store.commit(
            "sellerStore/setTimePickerStart",
            momentz.unix(dateFrom).tz(timezone).format("HH:mm")
          );
          $store.commit("sellerStore/setCloneTimePickerStart");
        } else {
          $store.commit(
            "sellerStore/setDateStart",
            moment().endOf("day").format("YYYY-MM-DD")
          );
          $store.commit("sellerStore/setCloneDateStart");
          $store.commit(
            "sellerStore/setTimePickerStart",
            moment().format("HH:mm")
          );
          $store.commit("sellerStore/setCloneTimePickerStart");
        }

        if (dateTo) {
          $store.commit("sellerStore/setIsShowDateEnd", true);
          $store.commit("sellerStore/setCloneIsShowDateEnd");
          $store.commit(
            "sellerStore/setDateEnd",
            momentz.unix(dateTo).tz(timezone).format("YYYY-MM-DD")
          );
          $store.commit("sellerStore/setCloneDateEnd");
          $store.commit(
            "sellerStore/setTimePickerEnd",
            momentz.unix(dateTo).tz(timezone).format("HH:mm")
          );
          $store.commit("sellerStore/setCloneTimePickerEnd");
        }
        $store.commit("sellerStore/setDiscountId", response?.data._id);
      }
      return response.data;
    } catch (error) {
      console.log("error :>> ", error);
      apiErrorHandler(error, ctx);
    } finally {
      $store.commit("sellerStore/setLoadingTableStore", false);
    }
  }

  @Action({ rawError: true })
  async onSubmitDiscount({ ctx }: { ctx: Vue }) {
    const { $store, $api } = ctx;
    try {
      const storeId = $store.state.sellerStore?.storeId || "";
      const discountStore = $store.state.sellerStore?.discount || {};
      const checkboxMinimum = $store.state.sellerStore?.checkboxMinimum || "";
      const minimumRequire = $store.state.sellerStore?.minimumRequire || 0;
      const discountId = $store.state.sellerStore?.discountId || "";
      const isShowDateEnd = $store.state.sellerStore?.isShowDateEnd || false;
      const listMinimumRequire =
        $store.state.sellerStore?.listMinimumRequire || [];

      if (!discountStore.code) {
        return ctx.$pushNotification.error("Please enter discount code");
      } else if (
        discountStore.discountType !== "3" &&
        !discountStore.discount
      ) {
        return ctx.$pushNotification.error("Please enter discount value");
      } else if (
        discountStore.discountType === "1" &&
        (discountStore.discount < 0 || discountStore.discount > 100)
      ) {
        return ctx.$pushNotification.error(
          "Discount value cannot be less than 0 and greater than 100"
        );
      }
      let body: ISellerDiscount = {
        ...discountStore,
        minAmount: checkboxMinimum === "2" ? minimumRequire : 0,
        minQuantity: checkboxMinimum === "3" ? minimumRequire : 0,
        couponId: discountId || null,
        status: eval(discountStore.status),
        storeId: storeId,
        discountType: parseInt(discountStore.discountType),
        dateFrom: moment($store.state.setting.from).unix(),
        multipleReq: listMinimumRequire,
      };
      if (isShowDateEnd) {
        body.dateTo = moment($store.state.setting.to).unix() || 0;
      }
      if (discountId) {
        await $store.dispatch("sellerStore/onUpdateDiscount", {
          ctx,
          body: body,
        });
      } else {
        await $store.dispatch("sellerStore/onCreateDiscount", {
          ctx,
          body: body,
        });
      }
      $store.dispatch("sellerStore/onGetDiscount", {
        ctx: ctx,
      });
    } catch (error) {
      apiErrorHandler(error, ctx);
    } finally {
      $store.commit("sellerStore/setIsChanged", false);
      $store.commit("sellerStore/setLoading", false);
    }
  }

  @Action({ rawError: true })
  async onUpdateDiscount({ ctx, body }: { ctx: Vue; body: ISellerDiscount }) {
    try {
      const { $store, $api } = ctx;
      const storeId = $store.state.sellerStore?.storeId || "";
      await $api.discounts.storeUpdate(body, storeId);
      // ctx.$pushNotification.success("Update discount successfully");
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async onCreateDiscount({ ctx, body }: { ctx: Vue; body: ISellerDiscount }) {
    try {
      const { $store, $api } = ctx;
      await $api.discounts.storeCreate(body);
      ctx.$pushNotification.success("Create discount successfully");
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async onGetPixel({
    ctx,
    storeId,
  }: {
    ctx: Vue;
    storeId: string;
    payload: any;
  }) {
    try {
      const { $store, $api } = ctx;
      const response = await $api.pixel.get(storeId);
      if (response?.data) {
        $store.commit("sellerStore/setPixel", response.data);
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async onGetListProductSeller({ ctx }: { ctx: Vue; payload: any }) {
    try {
      const { $store, $api } = ctx;
      const response = await $api.productSeller.list();
      if (response?.data) {
        $store.commit("sellerStore/setProductList", response.data.data);
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async onGetListStore({ ctx }: { ctx: Vue }) {
    const { $store, $api } = ctx;
    try {
      $store.commit("sellerStore/setLoadingTableStore", true);
      const response = await $api.store.list();
      if (response?.data) {
        $store.commit("sellerStore/setStoreList", response.data.data);
      }
    } catch (error) {
      console.log("error :>> ", error);
      apiErrorHandler(error, ctx);
    } finally {
      $store.commit("sellerStore/setLoadingTableStore", false);
    }
  }

  @Action({ rawError: true })
  async onGetDetailStore({ ctx }: { ctx: Vue }) {
    try {
      const { $store, $api } = ctx;
      const response = await $api.store.detail(
        $store.state.sellerStore.storeId
      );
      if (response?.data) {
        if (response.data?.store?.domain) {
          const subDomain = response.data.store.domain.split(".")[0];
          response.data.store.subDomain = subDomain;
          const mainDomain = response.data.store.domain.slice(
            subDomain.length + 1
          );
          response.data.store.mainDomain = mainDomain;
        }
        $store.commit("sellerStore/setStore", response.data);
        $store.commit("sellerStore/setCloneStore", response.data);

      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }
  @Action({ rawError: true })
  async onUpdatePixel({ ctx }: { ctx: Vue }) {
    try {
      const { $store, $api, $pushNotification } = ctx;
      const pixel = cloneDeep($store.state.sellerStore.pixel);
      const store = cloneDeep($store.state.sellerStore.store);
      const payload = {
        id: store.store._id,
        googleAds: pixel.googleAds,
        facebookPixel: pixel.facebookPixel,
        tiktokPixel: pixel.tiktokPixel,
        snapPixel: pixel.snapPixel,
        twitterPixel: pixel.twitterPixel,
      };
      await $api.pixel.put(payload.id, payload);
      await $store.dispatch("sellerStore/onGetPixel", {
        ctx,
        storeId: store.store._id,
      });
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async onSortImage({ ctx }: { ctx: Vue }) {
    try {
      const { $store, $api, $pushNotification } = ctx;
      const store = cloneDeep($store.state.sellerStore.store);
      const listShowImage =
        $store.state.sellerStore?.store.product?.listImageId;
      const payload = {
        id: store.store._id,
        listNewImageId: listShowImage.map((item: any) => item._id),
      };
      await $api.store.sortImage(payload.id, payload);
    } catch (error) {
      console.log("error :>> ", error);
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async onGetAllChanged({ ctx }: { ctx: Vue }) {
    const { $store, $api } = ctx;
    try {
      const store = cloneDeep($store.state.sellerStore.store);
      const pixel = cloneDeep($store.state.sellerStore.pixel);

      const listVariant = store.product.listVariantSellerId.map(
        (item: any) => ({
          price: item.price,
          comparePrice: item.comparePrice,
          imageId: item.imageId._id,
          variantId: item._id,
        })
      );
      const productDisplay = {
        peopleViewing: store.store.productDisplay?.peopleViewing,
        purchased: store.store.productDisplay?.purchased,
        lastMinute: store.store.productDisplay?.lastMinute,
      };
      for (let variant of listVariant) {
        variant.comparePrice = parseFloat(
          variant.comparePrice.toString().replace(",", ".")
        );
        variant.price = parseFloat(variant.price.toString().replace(",", "."));
      }
      const payload = {
        announcement: store.store.announcement,
        id: store.store._id,
        buyMore: store.store.buyMore,
        colorVariant: store.store.colorVariant,
        templateType: store.store.templateType,
        saveButton: store.store.saveButton,
        storeName: store.store.storeName,
        productName: store.product.productName,
        slug: store.store.subDomain,
        supportEmail: store.store.supportEmail,
        companyInfo: store.storeSetting.companyInfo,
        phoneNumber: store.storeSetting.phoneNumber,
        description: store.product.description,
        isOpenAbandoned: store.store.isOpenAbandoned,
        logo: store.storeSetting?.logo?.id,
        domain:
          store.store.subDomain && store.store.mainDomain
            ? store.store.subDomain + "." + store.store.mainDomain
            : undefined,
        isOnHomePage: store.store.isOnHomePage,
        listVariant,
        googleAds: pixel.googleAds,
        facebookPixel: pixel.facebookPixel,
        tiktokPixel: pixel.tiktokPixel,
        snapPixel: pixel.snapPixel,
        twitterPixel: pixel.twitterPixel,
        productDisplay,
        sellerDiscountId: store.sellerDiscountId,
        listStoreId: store.listStoreId,
        status: store.status,
      };
      await $api.store.updateOverView(store.store._id, payload);
      $store.commit("sellerStore/setIsNotChangeStore", true);
      $store.commit("sellerStore/setIsNotChangePixel", true);
    } catch (error) {
      apiErrorHandler(error, ctx);
    } finally {
    }
  }
}
