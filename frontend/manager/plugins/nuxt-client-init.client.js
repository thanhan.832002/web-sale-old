import { apiErrorHandler } from "~/utils/apiHanlers";
import { getModule } from "vuex-module-decorators";
import AdminSetting from "~/store/setting";
import SellerProductCategoryStore from "~/store/sellerStore";
import User from "~/store/user";
import { api } from "@/common/constaint";

export default async (ctx) => {
  try {
    const { $api, store, route } = ctx;
    const setting = getModule(AdminSetting, store);
    getModule(SellerProductCategoryStore, store);
    await ctx.store.dispatch("nuxtClientInit", ctx);
    const accessToken = ctx.store.state.user.accessToken;
    if (
      route.path === "/auth/login" ||
      route.path === "/auth/register" ||
      (route.path === "/" && !accessToken)
    )
      return;
    await store.dispatch("user/onGetSettingPage", { ctx });
  } catch (error) {
    apiErrorHandler(error, ctx);
  }
};
