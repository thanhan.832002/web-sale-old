import Vue from 'vue'
import apexchart from 'vue-apexcharts'
Vue.use(apexchart)

Vue.component('apexchart', apexchart)