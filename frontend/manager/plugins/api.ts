import { Plugin } from "@nuxt/types";
import authApi from "~/api/auth";
import homeApi from "~/api/home";
import galleryApi from "~/api/gallery";
import collectionApi from "~/api/collection";
import productApi from "~/api/product";
import variantApi from "~/api/variant";
import promotionApi from "~/api/promotion";
import comboApi from "~/api/combo";
import { NuxtAxiosInstance } from "@nuxtjs/axios";
import shippingApi from "~/api/shipping";
import couponApi from "~/api/coupon";
import orderApi from "~/api/order";
import userApi from "~/api/user";
import settingApi from "~/api/settings";
import statsApi from "~/api/stats";
import paymentApi from "~/api/payment";
import adsAnalyticApi from "~/api/ads-analytic";
import bestSellerApi from "~/api/best-seller";
import recentlyApi from "~/api/recently";
import abandonApi from "~/api/abandon";
import userPaymentAdmin from "~/api/user-payment-admin";
import userAdmin from "~/api/user-admin";
import storeAdmin from "~/api/store-admin";
import productSellerApi from "~/api/product-seller";
import storeApi from "~/api/store";
import storeGalleryApi from "~/api/storeGallery";
import sellerOrder from "~/api/sellerOrder";
import pixelApi from "~/api/pixel";
import reviewApi from "~/api/review";
import cartApi from "~/api/cart";
import domainSellerApi from "~/api/domain-seller";
import adminSendMail from "~/api/mail";
import domainApi from "~/api/domain";
import discountsApi from "~/api/discounts";
import adminDiscountsApi from "~/api/admin-discount";
import userMaster from "~/api/userMaster";
const apiPlugin: Plugin = (context, inject) => {
  const factories = {
    userMaster: userMaster(context.$axios),
    discounts: discountsApi(context.$axios),
    AdminDiscount: adminDiscountsApi(context.$axios),
    domainSeller: domainSellerApi(context.$axios),
    domain: domainApi(context.$axios),
    home: homeApi(context.$axios),
    auth: authApi(context.$axios),
    gallery: galleryApi(context.$axios),
    collection: collectionApi(context.$axios),
    product: productApi(context.$axios),
    variant: variantApi(context.$axios),
    promotion: promotionApi(context.$axios),
    combo: comboApi(context.$axios),
    shipping: shippingApi(context.$axios),
    coupon: couponApi(context.$axios),
    order: orderApi(context.$axios),
    sellerOrder: sellerOrder(context.$axios),
    user: userApi(context.$axios),
    setting: settingApi(context.$axios),
    stats: statsApi(context.$axios),
    payment: paymentApi(context.$axios),
    adsAnalytic: adsAnalyticApi(context.$axios),
    bestSeller: bestSellerApi(context.$axios),
    recently: recentlyApi(context.$axios),
    abandon: abandonApi(context.$axios),
    userPaymentAdmin: userPaymentAdmin(context.$axios),
    userAdmin: userAdmin(context.$axios),
    storeAdmin: storeAdmin(context.$axios),
    productSeller: productSellerApi(context.$axios),
    store: storeApi(context.$axios),
    storeGallery: storeGalleryApi(context.$axios),
    pixel: pixelApi(context.$axios),
    review: reviewApi(context.$axios),
    cart: cartApi(context.$axios),
    adminSendMail: adminSendMail(context.$axios),
  };
  inject("api", factories);
};

export default apiPlugin;
