
import { Context, Plugin } from '@nuxt/types'

import { IUtils, utils } from '~/common/utils'

declare module 'vue/types/vue' {
    interface Vue {
        $utils: IUtils
    }
}
declare module '@nuxt/types' {
    interface NuxtAppOptions {
        $utils: IUtils
    }
    // nuxtContext.$utils
    interface Context {
        $utils: IUtils
    }
}


const ultilPlugin: Plugin = (ctx: Context, inject) => {
    inject('utils', utils(ctx))
}

export default ultilPlugin