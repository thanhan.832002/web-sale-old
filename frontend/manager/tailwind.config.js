/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,vue,ts,html}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  theme: {
    extend: {
      colors: {
        // bg
        "bg-main": "#FFFFFF",
        "bg-surface": "#F9FAFB",
        "bg-hover": "#F3F4F6",
        "bg-active": "#E5E7EB",
        "bg-reversed": "#111827",
        "bg-placeholder": "#E5E7EB",
        "bg-overlay": "#888c93",
        "bg-success": "#F0FDF4",
        "bg-warning": "#FDF6B2",
        "bg-error": "#FDECEC",
        "bg-disable": "#D1D5DB",
        "bg-green-shade": "#DEF7EC",

        // button
        button: "#3B82F6",
        "button-hover": "#1C64F2",
        "button-shade": "#E1EFFE",
        "button-shade-hover": "#C3DDFD",

        // text
        "content-main": "#1F2937",
        "content-brand": "#3F83F8",
        "content-on-color": "#FFFFFF",
        "content-subtitle": "#6B7280",
        "content-disable": "#9CA3AF",
        "content-success": "#10B981",
        "content-destructive": "#F05252",
        "content-warning": "#E3A008",
        "content-primary": "#2D3234",
        "content-badge-basic-plan": "#0E9F6E",

        // border
        "border-main": "#F1F5F9",
        "border-highlight": "#E5E7EB",
        "border-brand": "#3F83F8",
        "border-destructive": "#F05252",
        "border-success": "#10B981",
      },
    },
  },
  plugins: [],
};
