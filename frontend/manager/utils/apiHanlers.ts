import Vue from "vue";
//@ts-ignore
import enMessage from "../locales/en.json";

export const apiErrorHandler = (error: any, ctx: Vue) => {
  try {
    if (error?.response?.data?.message) {
      const message = error.response.data.message;
      if (message === "UNKNOW_ERROR") {
        return ctx.$pushNotification.error(`An error has occurred`);
      }
      if (message === "TOKEN_EXPIRED") {
        return
      }
      return ctx.$pushNotification.error(enMessage.responseMessage[message] || message);
    } else {
      return ctx.$pushNotification.error(`An error has occurred`);
    }
  } catch (error) {
    return ctx.$pushNotification.error(`An error has occurred`);
  }
};

export const redirectPage = (href: string) => {
  window.location.href = `${window.location.origin}${href}`;
};
