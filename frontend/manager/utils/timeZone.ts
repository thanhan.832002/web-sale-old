import { clientLanguageKey } from "./common";

var momentz = require("moment-timezone");
var moment = require("moment");

export const getUnixFrom = (
  timezone: string,
  startDate: string,
  timeFrom?: string
) => {
  let chooseDate = `${startDate}T${timeFrom ? timeFrom + ":00" : "00:00:00"}${
    //@ts-ignore
    timezone[0] + "0" + timezone.slice(1)
  }:00`;
  return chooseDate;
};

export const getUnixTo = (
  timezone: string,
  startEnd: string,
  timeTo?: string
) => {
  let chooseDate = `${startEnd}T${timeTo ? timeTo + ":59" : "23:59:59"}${
    timezone[0] + "0" + timezone.slice(1)
  }:00`;
  return chooseDate;
};

export const getTimeZone = (timezone: string) => {
  if (timezone === "+1") {
    return "Atlantic/Cape_Verde";
  }
  if (timezone === "-8") {
    return "America/Anchorage";
  }
  if (timezone === "-9") {
    return "America/Adak";
  }
  if (timezone === "+0") {
    return "GB";
  }
  if (timezone === "+8") {
    return "Asia/Singapore";
  }
  if (timezone === "+7") {
    return "Asia/Ho_Chi_Minh";
  }
  return "US/Arizona"; // default -7
};
