import { Store } from "vuex"
import { IApiPlugin } from "@/interfaces/api"
import { NuxtAxiosInstance } from "@nuxtjs/axios"
import ToastInterface from "vue-toastification/dist/types/src/ts/interface"
import { PushNotificationType } from "~/plugins/notify"
import { NuxtCookies } from "cookie-universal-nuxt/types"

declare module 'vue/types/vue' {
    interface Vue {
        $api: IApiPlugin,
        $store: Store<any>
        $toast: ReturnType<typeof ToastInterface>
        $pushNotification: PushNotificationType
        $localForage: LocalForage
        $cookies: NuxtCookies,
    }
}

declare module '@nuxt/types' {
    interface Context {
        $api: IApiPlugin
        $store: Store<any>
        $toast: ReturnType<typeof ToastInterface>
        $pushNotification: PushNotificationType
        $localForage: LocalForage
        $cookies: NuxtCookies,

    }
}

declare module 'vuex/types/index' {
    // this.$myInjectedFunction inside Vuex stores
    interface Store<S> {
        $api: IApiPlugin
        $store: Store<any>
        $toast: ReturnType<typeof ToastInterface>
        $pushNotification: PushNotificationType
        $localForage: LocalForage
        $cookies: NuxtCookies,
    }
}