// import colors from "vuetify/es5/util/colors";
export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: "%s",
    title: " ",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Store" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [
      // /bg-white.png
      // { rel: "icon", type: "image/x-icon", href: "/logous.png" },
      {
        rel: "preconnect",
        href: "https://cdn.jsdelivr.net/",
        crossorigin: true,
      },
      {
        rel: "preconnect",
        href: "https://www.paypal.com/",
        crossorigin: true,
      },
      {
        rel: "preconnect",
        href: "https://connect.facebook.net/",
        crossorigin: true,
      },
      {
        rel: "preconnect",
        href: "https://sc-static.net/",
        crossorigin: true,
      },
      {
        rel: "preconnect",
        href: "https://www.googletagmanager.com/",
        crossorigin: true,
      },
      {
        rel: "preconnect",
        href: "https://www.google-analytics.com/",
        crossorigin: true,
      },
    ],
    script: [
      { src: "https://js.stripe.com/v3", async: true },
      // { src: "https://accounts.google.com/gsi/client" },
    ],
  },

  ssr: false,

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "~/assets/css/main.css",
    "~/assets/css/style.scss",
    "~/assets/css/materials.scss",
  ],
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    // "~/plugins/i18n.client",
    "~/plugins/axios",
    "~/plugins/api",
    "~/plugins/notify",
    "~/plugins/localforage.client",
    "~/plugins/utils",
    "~/plugins/vue-debounce",
    "~/plugins/vue-json-excel",
    "~/plugins/vueSlickCarousel.client",
    { src: "~/plugins/gtag.client.js", ssr: false },
    // { src: "~/plugins/fb-pixel.ts", ssr: false },
    { src: "~/plugins/tiktok-pixel.ts", ssr: false },
    { src: "~/plugins/snap-pixel.ts", ssr: false },
    { src: "~/plugins/vue-smooth-scroll.ts", ssr: false },
    { src: "~/plugins/nuxt-client-init.client", ssr: false },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,
  rootId: "__zshop",
  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    "@nuxt/typescript-build",
    // https://go.nuxtjs.dev/vuetify
    "@nuxtjs/vuetify",
    "@nuxtjs/localforage",
    "@nuxt/image",
    "@nuxt/components",
  ],
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "@nuxtjs/axios",
    // '@nuxtjs/i18n',
    "vue-toastification/nuxt",
    "nuxt-facebook-pixel-module",
    "nuxt-snapchat-tracker-module",
    "@nuxtjs/sentry",
    [
      "nuxt-compress",
      {
        gzip: {
          cache: true,
        },
        brotli: {
          threshold: 10240,
        },
      },
    ],
  ],
  facebook: {
    pixelId: "DEFAULT_PIXEL_ID",
  },

  server: {
    // host: process.env.NODE_ENV == "development" ? null : '103.118.28.103',
    port: 2002,
  },

  axios: {
    baseURL:
      process.env.NODE_ENV == "development"
        ? "http://192.168.1.154:2000"
        : "https://api.opensitex.com",
  },

  // serverMiddleware: [
  //   { path: '/auth/login', handler: '~/middleware/authenticated.js' },
  //   { path: '/admin/dashboard', handler: '~/middleware/notAuthenticated.js' }
  // ],

  sentry: {
    dsn:
      process.env.NODE_ENV === "development"
        ? "https://31acc86ebc8a3dbcb8106f40a40023b8@o4506245734596608.ingest.sentry.io/4506257837195264"
        : "https://dc5daeab9f5d8d8220cfdb4c35bac358@o4506245734596608.ingest.sentry.io/4506245737152512",
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    defaultAssets: false,
    theme: {
      dark: false,
      themes: {
        dark: {
          // primary: colors.blue.darken2,
          // accent: colors.grey.darken3,
          // secondary: colors.amber.darken3,
          // info: colors.teal.lighten1,
          // warning: colors.amber.base,
          // error: colors.deepOrange.accent4,
          // success: colors.green.accent3,
        },
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    // babel: {
    //   plugins: [
    //     ["@babel/plugin-proposal-private-property-in-object", { loose: true }],
    //   ],
    // },
    publicPath: "https://static.opensitex.com/client",
    postcss: false,
    standalone: true,
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          exclude: /(node_modules)/,
        });
      }

      // Your custom Webpack configuration here
      // For example, add a Webpack plugin:
      // config.plugins.push(new YourWebpackPlugin());
    },
  },

  optimization: {
    optimization: {
      splitChunks: {
        maxSize: 10000,
        pages: true,
        vendor: true,
        commons: true,
        runtime: true,
        layouts: true,
        cacheGroups: {
          styles: {
            name: "styles",
            test: /\.(css|vue)$/,
            chunks: "all",
            enforce: true,
            maxSize: 100000,
          },
        },
      },
    },
  },
  filenames: {
    css: ({ isDev }) => "[contenthash].css",
  },
  extractCSS: true,
  extend(config) {
    config.resolve.alias["vue"] = "vue/dist/vue.common";
    config.resolve.symlinks = false;
  },
};
