import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { api } from "~/common/constaint";
import { IAuth } from "~/interfaces/auth";

const authApi = ($axios: NuxtAxiosInstance) => ({
    login: (payload: IAuth) => {
        return $axios.$post(`${api}/auth/login`, payload)
    },

    register: (payload: IAuth) => {
        return $axios.$post(`${api}/auth/register`, payload)
    },

    renewToken: (refreshToken: string) => {
        return $axios.$post(`${api}/auth/renew-token`, refreshToken)
    }
})

export default authApi
export type AuthApiType = ReturnType<typeof authApi>