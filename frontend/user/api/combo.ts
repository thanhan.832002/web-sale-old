import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { api } from "~/common/constaint";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const comboApi = ($axios: NuxtAxiosInstance) => ({
    getList: (query: any) => {
        return $axios.$get(`${api}/${resources.Combo}/list-combo?${objectToParam(query)}`)
    },
    getListAdmin: (query: any) => {
        return $axios.$get(`${api}/${resources.Combo}/admin/list-combo?${objectToParam(query)}`)
    },
    getComboByProduct: (productId: string) => {
        return $axios.$get(`${api}/${resources.Combo}/get-combo-by-product/${productId}`)
    },
    create: (payload: any) => {
        return $axios.$post(`${api}/${resources.Combo}/create-combo`, payload)
    },
    edit: (comboId: string, payload: any) => {
        return $axios.$put(`${api}/${resources.Combo}/edit-combo/${comboId}`, payload)
    },
    delete: (comboId: string) => {
        return $axios.$delete(`${api}/${resources.Combo}/delete-combo/${comboId}`)
    },
    deleteList: (listComboId: string[]) => {
        return $axios.$post(`${api}/${resources.Combo}/delete-list-combo`, { listComboId })
    }
})

export default comboApi
export type ComboApiType = ReturnType<typeof comboApi>