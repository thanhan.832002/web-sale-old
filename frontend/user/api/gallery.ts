import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { IAuth } from "~/interfaces/auth";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { IChangeGalleryTag, ICreateUpdateTag } from "~/interfaces/tag";
import { api } from "~/common/constaint";

const galleryApi = ($axios: NuxtAxiosInstance) => ({
    getList: (tagId: string) => {
        return $axios.$get(`${api}/${resources.Gallery}/list?tagId=${tagId}`)
    },
    update: (payload: IChangeGalleryTag) => {
        return $axios.$put(`${api}/${resources.Gallery}/change-gallery-tag`, payload)
    },
    delete: (galleryId: string) => {
        return $axios.$delete(`${api}/${resources.Gallery}/delete/${galleryId}`)
    },

    //Tag
    getListTag: (query: any) => {
        return $axios.$get(`${api}/${resources.Tag}/list?${objectToParam(query)}`)
    },
    createTag: (name: string) => {
        return $axios.$post(`${api}/${resources.Tag}/create`, {
            name
        })
    },
    updateTag: (tagId: string, name: string) => {
        return $axios.$put(`${api}/${resources.Tag}/edit/${tagId}`, {
            name
        })
    },
    deleteTag: (tagId: string) => {
        return $axios.$delete(`${api}/${resources.Tag}/delete/${tagId}`)
    },
})

export default galleryApi
export type GalleryApiType = ReturnType<typeof galleryApi>