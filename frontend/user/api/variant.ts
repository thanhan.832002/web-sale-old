import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { api } from "~/common/constaint";
import { resources } from "~/interfaces/api";

const variantApi = ($axios: NuxtAxiosInstance) => ({
    create: (payload: any) => {
        return $axios.$post(`${api}/${resources.Variant}/create`, payload)
    },
    edit: (variantId: string, payload: any) => {
        return $axios.$put(`${api}/${resources.Variant}/edit-variant/${variantId}`)
    },
    delete: (variantId: string) => {
        return $axios.$delete(`${api}/${resources.Variant}/delete-variant/${variantId}`)
    },
    list: (productId: string) => {
        return $axios.$get(`${api}/${resources.Variant}/list-variant-by-product/${productId}`)
    }
})

export default variantApi
export type VariantApiType = ReturnType<typeof variantApi>