import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { api } from "~/common/constaint";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const productApi = ($axios: NuxtAxiosInstance) => ({
  create: (payload: any) => {
    return $axios.$post(`${api}/${resources.Product}/create-product`, payload);
  },

  getListAdmin: (query: any) => {
    return $axios.$get(
      `${api}/${resources.Product}/admin/list-product?${objectToParam(query)}`
    );
  },

  getListProduct: (query: any) => {
    return $axios.$get(
      `${api}/${resources.Product}/list-product?${objectToParam(query)}`
    );
  },

  getListProductByCollection: (query: any) => {
    return $axios.$get(
      `${api}/${resources.Product}/list-product-by-collection?${objectToParam(
        query
      )}`
    );
  },

  getRelatedProduct: (productSlug: string) => {
    return $axios.$get(
      `${api}/${resources.Product}/related-product/${productSlug}`
    );
  },

  getRecentlyViewed: (listProductId: string[]) => {
    return $axios.$post(`${api}/${resources.Product}/recently-viewed`, {
      listProductId,
    });
  },

  getDetailProduct: (
    sessionId: string,
    storeId: string,
    adminId: string,
    query?: any
  ) => {
    return $axios.$get(
      `${api}/${resources.ProductBuyer}/detail-product/?${objectToParam({
        ...query,
        sessionId,
        adminId,
        storeId,
      })}`
    );
  },

  countCheckout: (sessionId: string,
    storeId: string,
    adminId: string,
    query?: any) => {
    return $axios.$get(
      `${api}/${resources.ProductBuyer}/checkout/?${objectToParam({
        ...query,
        sessionId,
        adminId,
        storeId,
      })}`
    );
  },

  getBestSeller: (slug?: string) => {
    return $axios.$get(`${api}/${resources.Product}/best-seller?slug=${slug}`);
  },

  edit: (productId: string, payload: any) => {
    return $axios.$post(
      `${api}/${resources.Product}/edit-product/${productId}`,
      payload
    );
  },

  delete: (productId: string) => {
    return $axios.$delete(
      `${api}/${resources.Product}/delete-product/${productId}`
    );
  },

  deleteList: (listProductId: string[]) => {
    return $axios.$post(`${api}/${resources.Product}/delete-list-product`, {
      listProductId,
    });
  },

  getDetail: (productId: string) => {
    return $axios.$get(
      `${api}/${resources.Product}/product-detail/${productId}`
    );
  },

  getDetailProductDomain: (domain: string, query?: any) => {
    return $axios.$get(
      `${api}/${resources.ProductBuyer}/detail/${domain}?${objectToParam(
        query
      )}`
    );
  },

  productReview: (body: any) => {
    return $axios.$post(`${api}/${resources.Product}/review/create`, body);
  },
  replyReview: (body: any) => {
    return $axios.$post(`${api}/${resources.Product}/review/create`, body);
  },
  listProductReview: (productSlug: string) => {
    return $axios.$get(`${api}/${resources.Product}/review/${productSlug}`);
  },

  getListUpsale: (domain: string) => {
    return $axios.$get(`${api}/${resources.StoreBuyer}/upsale/${domain}`);
  },
});

export default productApi;
export type ProductApiType = ReturnType<typeof productApi>;
