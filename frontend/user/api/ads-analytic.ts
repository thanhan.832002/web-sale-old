import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const adsAnalyticApi = ($axios: NuxtAxiosInstance) => ({
  list: (domain: string) => {
    return $axios.$get(`${resources.pixel}/domain/${domain}`);
  },
});

export default adsAnalyticApi;
export type AdsAnalyticApiType = ReturnType<typeof adsAnalyticApi>;
