import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const paymentApi = ($axios: NuxtAxiosInstance) => ({
  userListPayment: (domain: string) => {
    return $axios.$get(`${resources.PaymentBuyer}/list/${domain}`);
  },
  userSkStripe: (domain: string) => {
    return $axios.$get(`${resources.PaymentBuyer}/sk-stripe/${domain}`);
  },
  userListPolicy: (domain: string) => {
    return $axios.$get(`${resources.PaymentBuyer}/policy/${domain}`);
  },
  adminListPayment: () => {
    return $axios.$get(`${resources.Payment}/admin/list-payment`);
  },
  updatePayment: (body: any) => {
    return $axios.$post(`${resources.Payment}/admin/update-payment`, body);
  },
});

export default paymentApi;
export type PaymentApiType = ReturnType<typeof paymentApi>;
