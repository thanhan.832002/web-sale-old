import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { api } from "~/common/constaint";
import { resources } from "~/interfaces/api";

const promotionApi = ($axios: NuxtAxiosInstance) => ({
    getPromotion: () => {
        return $axios.$get(`${api}/${resources.Promotion}/promotion-detail`)
    },
    update: (payload: any) => {
        return $axios.$post(`${api}/${resources.Promotion}/update-promotion`, payload)
    }
})

export default promotionApi
export type PromotionApiType = ReturnType<typeof promotionApi>