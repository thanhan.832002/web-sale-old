import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { api } from "~/common/constaint";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const settingApi = ($axios: NuxtAxiosInstance) => ({
    getSetting: (domain: string) => {
        return $axios.$get(`${api}/${resources.StoreBuyer}/setting/${domain}`)
    },
    getSettingProduct: (domain: string) => {
        return $axios.$get(`${api}/${resources.StoreBuyer}/setting-product/${domain}`)
    },
    updateSetting: (payload: any) => {
        return $axios.$post(`${api}/${resources.Setting}/update-setting`, payload)
    },
    getSettingHomePage: (domain: string) => {
        return $axios.$get(`${api}/${resources.HomePage}/${domain}`)
    }
})

export default settingApi
export type SettingApiType = ReturnType<typeof settingApi>