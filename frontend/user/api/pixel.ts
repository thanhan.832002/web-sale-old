import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { api } from "~/common/constaint";
import { resources } from "~/interfaces/api";

const pixelApi = ($axios: NuxtAxiosInstance) => ({
  getPixel:(domain: string) =>  $axios.$get(`${api}/${resources.Pixel}/domain/${domain}`)
})

export default pixelApi
export type PixelApiType = ReturnType<typeof pixelApi>