import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { api } from "~/common/constaint";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { IQueryAutoApply } from "~/interfaces/discount";

const couponApi = ($axios: NuxtAxiosInstance) => ({
    create: (payload: any) => {
        return $axios.$post(`${api}/${resources.Coupon}/create`, payload)
    },
    edit: (couponId: string, payload: any) => {
        return $axios.$put(`${api}/${resources.Coupon}/edit/${couponId}`, payload)
    },
    delete: (couponId: string) => {
        return $axios.$delete(`${api}/${resources.Coupon}/delete/${couponId}`)
    },
    deleteList: (listCouponId: string[]) => {
        return $axios.$post(`${api}/${resources.Coupon}/delete-list`, { listCouponId })
    },
    list: (query: any) => {
        return $axios.$get(`${api}/${resources.Coupon}/list-coupon?${objectToParam(query)}`)
    },
    autoApply: (query: IQueryAutoApply) => {
        return $axios.$get(`${api}/${resources.Coupon}/auto-apply?${objectToParam(query)}`)
    },
    getCouponBycode: (code: string, slug?: string) => {
        return $axios.$get(`${api}/${resources.Coupon}/code/${code}?slug=${slug || ""}`)
    },

    getListDiscount: (storeId: string) => {
        // ${storeId}
        return $axios.$get(`${api}/${resources.Coupon}/64a8ea47be44669a795c5f1a`);
    }
})

export default couponApi
export type CouponApiType = ReturnType<typeof couponApi>