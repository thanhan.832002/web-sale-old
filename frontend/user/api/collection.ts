import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { api } from "~/common/constaint";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const collectionApi = ($axios: NuxtAxiosInstance) => ({
    create: (payload: any) => {
        return $axios.$post(`${api}/${resources.Collection}/create-collection`, payload)
    },

    getList: (query: any) => {
        return $axios.$get(`${api}/${resources.Collection}/list-collection?${objectToParam(query)}`)
    },

    getListAdmin: (query: any) => {
        return $axios.$get(`${api}/${resources.Collection}/admin/list-collection?${objectToParam(query)}`)
    },

    getListChild: (query: any) => {
        return $axios.$get(`${api}/${resources.Collection}/list-child-collection?${objectToParam(query)}`)
    },

    getListAdminChild: (query: any) => {
        return $axios.$get(`${api}/${resources.Collection}/admin/list-child-collection?${objectToParam(query)}`)
    },

    edit: (collectionId: string, payload: any) => {
        return $axios.$post(`${api}/${resources.Collection}/edit-collection/${collectionId}`, payload)
    },

    delete: (collectionId: string) => {
        return $axios.$delete(`${api}/${resources.Collection}/delete-collection/${collectionId}`)
    },

    deleteList: (listCollectionId: string[]) => {
        return $axios.$post(`${api}/${resources.Collection}/delete-list-collection`, { listCollectionId })
    },

    getDetailAdmin: (collectionSlug: string) => {
        return $axios.$get(`${api}/${resources.Collection}/admin/collection-detail/${collectionSlug}`)
    }

})

export default collectionApi
export type CollectionApiType = ReturnType<typeof collectionApi>