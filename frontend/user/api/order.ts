import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { api } from "~/common/constaint";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";
import { ICardConfirmPayment, IConfirmPayment } from "~/interfaces/stripe";

const orderApi = ($axios: NuxtAxiosInstance) => ({
    getMyOrder: (email: string, orderCode: string, slug?: string) => {
        return $axios.$get(`${api}/${resources.Order}/${orderCode.replace("#", "")}?email=${email}&slug=${slug || ""}`)
    },
    cancelOrder: (orderCode: string) => {
        return $axios.$put(`${api}/${resources.Order}/cancel-order/${orderCode}`)
    },
    deleteOrder: (orderCode: string) => {
        return $axios.$delete(`${api}/${resources.Order}/delete?orderCode=${orderCode}`)
    },
    cardCheckout: (body: IConfirmPayment, domain: string, query: any, sessionId: string) => {
        return $axios.$post(`${api}/${resources.Order}/card-checkout/${domain}?${objectToParam({ ...query, sessionId })}`, body)
    },
    confirmPayment: (body: ICardConfirmPayment, domain: string) => {
        return $axios.$post(`${api}/${resources.Order}/card-comfirm-payment/${domain}`, body)
    },
})

export default orderApi
export type OrderApiType = ReturnType<typeof orderApi>