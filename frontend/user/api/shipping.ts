import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { api } from "~/common/constaint";
import { objectToParam } from "~/common/utils";
import { resources } from "~/interfaces/api";

const shippingApi = ($axios: NuxtAxiosInstance) => ({
    shippingDetail: (shippingId: string) => {
        return $axios.$get(`${api}/${resources.Shipping}/shipping-detail/${shippingId}`)
    },

    listShipping: (domain: string, country: string, id: string) => {
        return $axios.$get(`${api}/${resources.Shipping}/list-shipping?domain=${domain}&country=${country}&id=${id}`)
    },

    create: (payload: any) => {
        return $axios.$post(`${api}/${resources.Shipping}/create`, payload)
    },

    edit: (shippingId: string, payload: any) => {
        return $axios.$put(`${api}/${resources.Shipping}/edit/${shippingId}`, payload)
    },

    delete: (shippingId: string) => {
        return $axios.$delete(`${api}/${resources.Shipping}/delete/${shippingId}`)
    },

    getIp: () => {
        return $axios.$get(`${api}/${resources.Shipping}/ip`)
    }
})

export default shippingApi
export type ShippingApiType = ReturnType<typeof shippingApi>