import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { resources } from "~/interfaces/api";
import { objectToParam } from "~/common/utils";
import { api } from "~/common/constaint";
import { ICreateAbandoned } from "~/interfaces/checkout";

const cartApi = ($axios: NuxtAxiosInstance) => ({
  create: () => {
    return $axios.$post(`${api}/${resources.Cart}/create`);
  },
  getMyCart: (cartId: string) => {
    return $axios.$get(`${api}/${resources.Cart}/my-cart?cartId=${cartId}`);
  },
  addToCart: (cartId: string, payload: any) => {
    return $axios.$post(
      `${api}/${resources.Cart}/add-to-cart?cartId=${cartId}`,
      payload
    );
  },
  viewAddToCart: (query: any, sessionId: string, domain?: string,) => {
    return $axios.$post(
      `${api}/${resources.View}/count-add-to-cart?${objectToParam({ ...query, domain: domain || "", sessionId })}`
    );
  },
  addAllToCart: (cartId: string, payload: any) => {
    return $axios.$post(
      `${api}/${resources.Cart}/add-all-to-cart?cartId=${cartId}`,
      payload
    );
  },
  addComboToCart: (cartId: string, payload: any) => {
    return $axios.$post(
      `${api}/${resources.Cart}/add-combo-to-cart?cartId=${cartId}`,
      payload
    );
  },
  updateCart: (cartId: string, payload: any) => {
    return $axios.$put(
      `${api}/${resources.Cart}/update-cart?cartId=${cartId}`,
      payload
    );
  },
  updateCombocart: (cartId: string, payload: any) => {
    return $axios.$put(
      `${api}/${resources.Cart}/update-combo-cart?cartId=${cartId}`,
      payload
    );
  },
  deleteCart: (cartId: string, payload: any) => {
    return $axios.$post(
      `${api}/${resources.Cart}/delete-product?cartId=${cartId}`,
      payload
    );
  },
  deleteComboCart: (cartId: string, comboId: string) => {
    return $axios.$post(
      `${api}/${resources.Cart}/delete-combo-cart?cartId=${cartId}`,
      {
        comboId,
      }
    );
  },

  checkout: (payload: any, domain: string, query: any, sessionId: string) => {
    return $axios.$post(
      `${api}/${resources.Order}/paypal-checkout/${domain}?${objectToParam({ ...query, sessionId })}`,
      payload
    );
  },
  checkoutCard: (payload: any, domain: string, query: any) => {
    return $axios.$post(
      `${api}/${resources.Order}/card-checkout/${domain}?${objectToParam(query)}`,
      payload
    );
  },
  comfirmPaymentCard: (payload: any, domain: string, query: any) => {
    return $axios.$post(
      `${api}/${resources.Order}/card-comfirm-payment/${domain}?${objectToParam(query)}`,
      payload
    );
  },
  paypalCreateOrder: (payload: any, domain: string) => {
    return $axios.$post(
      `${api}/${resources.Order}/paypal-create-order/${domain}`,
      payload
    );
  },
  paypalCreateOrderDirect: (body: any, domain: string, query: any) => {
    return $axios.$post(
      `${api}/${resources.Order}/paypal-direct-checkout/${domain}?${objectToParam({ ...query })}`,
      body
    );
  },
  completePaypalPayment: (payload: any) => {
    return $axios.$post(
      `${api}/${resources.Order}/paypal-complete-payment`,
      payload
    );
  },
  stripeCreateorder: (body: any, domain: string) => {
    return $axios.$post(
      `${api}/${resources.Order}/stripe-create-order/${domain}`,
      body
    );
  },
  stripeCompletePayment: (orderCode: string, domain: string) => {
    return $axios.$post(
      `${api}/${resources.Order}/stripe-complete-payment/${domain}`,
      {
        orderCode,
      }
    );
  },

  getCartAbandoned: (abandonedId: string) => {
    return $axios.$get(`${api}/${resources.Abandoned}/cart/${abandonedId}`);
  },

  createAbandoned: (body: ICreateAbandoned, domain: string) => {
    return $axios.$post(`${api}/${resources.Abandoned}/create/${domain}`, body);
  }
});

export default cartApi;
export type CartApiType = ReturnType<typeof cartApi>;
