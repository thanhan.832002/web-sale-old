import { redirectPage } from "~/utils/apiHanlers"

// export default async function ({ store, redirect, app }) {
//     let isLogin = store.getters['user/isLogin']
//     if (isLogin) {
//         app.router.push('/admin/dashboard')
//     }
// }

export const notAuthenticated = (isLogin) => {
    if (isLogin) {
        return redirectPage(`/admin/dashboard`);
    }
}