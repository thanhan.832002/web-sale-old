import { redirectPage } from "~/utils/apiHanlers"

// export default async function ({ store, redirect, app }) {
//     let isLogin = store.getters['user/isLogin']
//     if (!isLogin) {
//         // app.router.push('/auth/login')
//     }
// }


export const authenticated = (isLogin) => {
    if (!isLogin) {
        return redirectPage(`/auth/login`);
    }
}