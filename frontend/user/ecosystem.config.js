module.exports = {
    apps: [{
      name: "websalePreUser",
      script: "./app.js",
      instances: 3,
      env: {
        NODE_ENV: "development",
      },
      env_production: {
        NODE_ENV: "production",
      },
      "node_args": ["--max-old-space-size=500"]

    }]
  }