import Vue from "vue";

export const TwitterTracking = (twq: any, ctx: Vue, type: string, data?: any) => {
    switch (type) {
        case "PageView":
            PageView(twq)
            break
        case "ContentView":
            ContentView(twq)
            break
        case "AddToCart":
            AddToCart(twq)
            break
        case "InitiateCheckout":
            CheckoutInitiated(twq)
            break
        case "AddedPaymentInfo":
            AddedPaymentInfo(twq)
            break
        case "Purchase":
            Purchase(twq, data)
            break
        default:
            PageView(twq)
            break
    }
}

function PageView(twq: any) {
    twq("track", 'PageView')
}

function ContentView(twq: any) {
    twq("track", 'ContentView')
}

function AddToCart(twq: any) {
    twq("track", 'AddToCart')
}

function CheckoutInitiated(twq: any) {
    twq("track", 'InitiateCheckout')
}

function AddedPaymentInfo(twq: any) {
    twq("track", 'AddedPaymentInfo')
}

function Purchase(twq: any, data?: any) {
    twq("track", 'Purchase', {
        currency: "USD",
        value: data.amount || data.priceVariant || 1,
    })
}

export const handleTwitter = (ctx: Vue) => {
    const twitter = ctx.$store.state?.setting?.twitter || undefined;
    if (twitter && twitter.platformId) {
        const {
            twitterPixelId,
            pageViewId,
            contentViewId,
            addToCartId,
            checkoutInitiatedId,
            addedPaymentInfoId,
            purchaseId,
        } = twitter.platformId;
        let twitterPixelIds = [], pageViewIds = [], contentViewIds = [], addToCartIds = [], checkoutInitiatedIds = [], addedPaymentInfoIds = [], purchaseIds = []
        if (twitterPixelId) {
            twitterPixelIds = twitterPixelId
                .split("\n")
                .map((id: string) => id.trim())
                .filter((id: string) => id);
        }
        if (pageViewId) {
            pageViewIds = pageViewId
                .split("\n")
                .map((id: string) => id.trim())
                .filter((id: string) => id);
        }
        if (contentViewId) {
            contentViewIds = contentViewId
                .split("\n")
                .map((id: string) => id.trim())
                .filter((id: string) => id);
        }
        if (addToCartId) {
            addToCartIds = addToCartId
                .split("\n")
                .map((id: string) => id.trim())
                .filter((id: string) => id);
        }
        if (checkoutInitiatedId) {
            checkoutInitiatedIds = checkoutInitiatedId
                .split("\n")
                .map((id: string) => id.trim())
                .filter((id: string) => id);
        }
        if (addedPaymentInfoId) {
            addedPaymentInfoIds = addedPaymentInfoId
                .split("\n")
                .map((id: string) => id.trim())
                .filter((id: string) => id);
        }
        if (purchaseId) {
            purchaseIds = purchaseId
                .split("\n")
                .map((id: string) => id.trim())
                .filter((id: string) => id);
        }

        for (let id of pageViewIds) {
            //@ts-ignore
            if (twq) {
                //@ts-ignore
                twq("init", id);
            }
        }

        for (let id of contentViewIds) {
            //@ts-ignore
            if (twq) {
                //@ts-ignore
                twq("init", id);
            }
        }

        for (let id of addToCartIds) {
            //@ts-ignore
            if (twq) {
                //@ts-ignore
                twq("init", id);
            }
        }

        for (let id of checkoutInitiatedIds) {
            //@ts-ignore
            if (twq) {
                //@ts-ignore
                twq("init", id);
            }
        }

        for (let id of addedPaymentInfoIds) {
            //@ts-ignore
            if (twq) {
                //@ts-ignore
                twq("init", id);
            }
        }

        for (let id of purchaseIds) {
            //@ts-ignore
            if (twq) {
                //@ts-ignore
                twq("init", id);
            }
        }
    }
}