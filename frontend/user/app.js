const express = require("express");
const path = require("path");
const app = express();
const fs = require("fs");
const axios = require("axios");
const useragent = require("express-useragent");
app.use(useragent.express());

async function fetchMeta(domain) {
  try {
    // domain = `bodysuit.toptrend1day.com`
    const url = `https://api.opensitex.com/product-buyer/meta/${domain}`;
    const { data } = await axios.get(url);
    return `<head>
        <meta property="og:title" content="${data.data.productName}" />
        <meta property="og:image" content="https://api.opensitex.com/${data.data.imageUrl}" />
        <meta property="og:image:height" content="300" />
        <meta property="og:image:width" content="300" />`;
  } catch (error) {
    return "<head>";
  }
}
app.get("/", async function (req, res) {
  try {
    const userAgent = req.useragent;
    const isBot = userAgent.isBot;
    if (!isBot) return res.render(path.join(__dirname + "/dist/index.html"));
    const domain = req.hostname;
    const metaHtml = await fetchMeta(domain);
    let indexFile = path.join(__dirname + "/dist/index.html");
    const content = fs
      .readFileSync(indexFile, "utf-8")
      .replace("<head>", metaHtml);
    return res.send(content);
  } catch (error) {
    return res.render(path.join(__dirname + "/dist/index.html"));
  }
});

app.get("/:slug*", async function (req, res) {
  try {
    const { slug } = req.params;
    const userAgent = req.useragent;
    const isBot = userAgent.isBot;
    if (!isBot) return res.render(path.join(__dirname + "/dist/index.html"));
    const domain = slug ? `${slug}.${req.hostname}` : req.hostname;
    const metaHtml = await fetchMeta(domain);
    let indexFile = path.join(__dirname + "/dist/index.html");
    const content = fs
      .readFileSync(indexFile, "utf-8")
      .replace("<head>", metaHtml);
    return res.send(content);
  } catch (error) {
    console.log("error :>> ", error);
    return res.render(path.join(__dirname + "/dist/index.html"));
  }
});

const staticfilemiddleware = express.static(path.join(__dirname + "/dist"));

app.use(staticfilemiddleware);

app.set("views", __dirname + "/public");
app.engine("html", require("ejs").renderFile);
app.set("view engine", "html");

var server = app.listen(process.env.port || 2002, function () {
  var port = server.address().port;
  console.log("app now running on port", port);
});
