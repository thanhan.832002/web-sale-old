import Vue from "vue";
import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";
import { IAuth, ITokensResponse, IUser } from "~/interfaces/auth";
import {
  accessTokenKey,
  refreshTokenKey,
  userInfoKey,
} from "~/common/constaint";
import { apiErrorHandler, redirectPage } from "~/utils/apiHanlers";
import { IApiPlugin } from "~/interfaces/api";
import collectionApi from "~/api/collection";
import { $axios } from "~/utils/axios";
@Module({
  name: "setting",
  namespaced: true,
  stateFactory: true,
})
export default class AdminSetting extends VuexModule {
  siteInfo = {};
  settingHomePage = {};
  logo = "";

  tiktok = {};
  facebook = {};
  google = {};
  snap = {};
  twitter = {};

  isHeaderHomePage = false;

  listPolicyPayment = [];

  get fbPixel() {
    return this.facebook;
  }

  get snapPixel() {
    return this.snap;
  }

  @Mutation
  setHeaderHomePage(value: boolean) {
    this.isHeaderHomePage = value;
  }

  @Mutation
  setSettingHomePage(setting: any) {
    this.settingHomePage = setting;
  }

  @Mutation
  setSiteInfo(siteInfo: any) {
    this.siteInfo = siteInfo;
  }

  @Mutation
  setLogo(logo: string) {
    this.logo = logo;
  }

  @Mutation
  setListPolicyPayment(listPolicyPayment: any) {
    this.listPolicyPayment = listPolicyPayment;
  }

  @Mutation
  setAdsAnalytic(listAdsAnalytic: any) {
    if (listAdsAnalytic && listAdsAnalytic.length) {
      this.google = listAdsAnalytic[0];
      this.tiktok = listAdsAnalytic[1];
      this.facebook = listAdsAnalytic[2];
      this.snap = listAdsAnalytic[3];
      this.twitter = listAdsAnalytic[4];
    }
  }

  @Action({ rawError: true })
  onGetSetting({ ctx }: { ctx: Vue }) {
    try {
      const { $api, $store } = ctx;
    } catch (error) { }
  }
}
