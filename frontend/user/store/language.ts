
// import Vue from 'vue';
// import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
// import { clientLanguageKey } from '@/ultils/common/index';
// import { $i18n } from '~/plugins/i18n.client';
// import { $localForage } from '~/plugins/localforage.client';

// @Module
// export default class LanguageStore extends VuexModule {
//     locale: string = "vi";
//     locales: string[] = ["vi", "en"]
//     @Mutation
//     setLocale(locale: string) {
//         this.locale = locale
//         $i18n.locale = locale
//     }
//     @Action({ rawError: true })
//     async swichLocale({ ctx, locale }: { ctx: Vue, locale: string }) {
//         //@ts-ignore
//         await ctx.$localForage.setItem(clientLanguageKey, locale)
//         this.context.commit("setLocale", locale)
//     }
// }