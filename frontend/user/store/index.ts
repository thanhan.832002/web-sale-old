
import { ActionTree } from 'vuex'
import { getModule } from 'vuex-module-decorators'
import { Context } from '@nuxt/types/app'
import { accessTokenKey, refreshTokenKey, userInfoKey } from '~/common/constaint'
import UserModule from './user'

export const state = () => ({

})
export type RootState = ReturnType<typeof state>


export const actions: ActionTree<RootState, RootState> = {

    async nuxtClientInit({ commit }, { store, $localForage, app }: Context) {

    },
}

