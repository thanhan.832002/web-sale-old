import { Module, VuexModule, Mutation } from 'vuex-module-decorators'

@Module({
    name: 'home',
    stateFactory: true,
    namespaced: true,
})
class HomeModule extends VuexModule {
    siteInfo = {}
    statusRecentlyView = false
    listCollection = []
    listRecentlyViewed = []
    listBestSeller = []

    @Mutation
    setSiteInfo(siteInfo: any) {
        this.siteInfo = siteInfo
    }

    @Mutation
    setList({ listCollection, listRecentlyViewed, listBestSeller }: { listCollection: any, listRecentlyViewed: any, listBestSeller: any }) {
        this.listCollection = listCollection
        this.listRecentlyViewed = listRecentlyViewed
        this.listBestSeller = listBestSeller
    }

    @Mutation
    setStatusRecentlyView(status: boolean) {
        this.statusRecentlyView = status
    }
}

export default HomeModule