import Vue from 'vue'
import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { IAuth, ITokensResponse, IUser } from '~/interfaces/auth'
import { accessTokenKey, refreshTokenKey, userInfoKey } from '~/common/constaint';
import { apiErrorHandler, redirectPage } from '~/utils/apiHanlers';
import { IApiPlugin } from "~/interfaces/api"
import collectionApi from '~/api/collection';
import { $axios } from '~/utils/axios';
@Module({
    name: 'adminCollection',
    namespaced: true,
    stateFactory: true
})
class AdminCollectionModule extends VuexModule {
    drawer = true

    listProduct = []
    listCollection = []

    loadingTable = false
    loading = false

    coverId = ""
    imageUrl = ""

    collectionId = ""

    @Mutation
    setCollectionId(id: string) {
        this.collectionId = id
    }

    @Mutation
    setValueBoolane({ status, type }: { status: boolean, type: string }) {
        if (type === 'loading') {
            this.loading = status
        } else if (type === 'loadingTable') {
            this.loadingTable = status
        }
    }

    @Mutation
    setCoverId({ coverId, imageUrl }: { coverId: string, imageUrl: string }) {
        this.coverId = coverId
        this.imageUrl = imageUrl
    }

    @Mutation
    setListCollection(listCollection: any) {
        this.listCollection = listCollection
    }


    @Action({ rawError: true })
    async onGetListCollection({ ctx, payload }: { ctx: Vue, payload: any }) {
        try {
            const { $store, $api } = ctx
            $store.commit('adminCollection/setValueBoolane', { status: true, type: "loadingTable" })
            const response = await collectionApi(ctx.$axios).getListAdmin(payload)
            if (response?.data?.data) {
                $store.commit('adminCollection/setListCollection', response.data.data)
            }
            return response
        } catch (error) {
            apiErrorHandler(error, ctx)
        } finally {
            ctx.$store.commit('adminCollection/setValueBoolane', { status: false, type: "loadingTable" })
        }
    }

    @Action({ rawError: true })
    async onGetListCollectionChild({ ctx, payload }: { ctx: Vue, payload: any }) {
        try {
            const { $store, $api } = ctx
            $store.commit('adminCollection/setValueBoolane', { status: true, type: "loadingTable" })
            const response = await collectionApi(ctx.$axios).getListAdminChild(payload)
            if (response?.data?.data) {
                $store.commit('adminCollection/setListCollection', response.data.data)
            }
            return response
        } catch (error) {
            apiErrorHandler(error, ctx)
        } finally {
            ctx.$store.commit('adminCollection/setValueBoolane', { status: false, type: "loadingTable" })
        }
    }
}

export default AdminCollectionModule