import Vue from "vue";
import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";
import { IAuth, ITokensResponse, IUser } from "~/interfaces/auth";
import {
  accessTokenKey,
  api,
  refreshTokenKey,
  userInfoKey,
} from "~/common/constaint";
import { apiErrorHandler, redirectPage } from "~/utils/apiHanlers";
import { IApiPlugin, resources } from "~/interfaces/api";
import collectionApi from "~/api/collection";
import { $axios } from "~/utils/axios";
import { trackEventAddToCart } from "~/utils/trackEvent";
import { onGetListCart } from "~/utils/localStorage";
import { getSessionId, randomCharacter, setSessionId } from "~/utils/sessionId";

@Module({
  name: "cart",
  namespaced: true,
  stateFactory: true,
})
class CartModule extends VuexModule {
  publicKeyPaypal = "";
  publicKeyStripe = "";
  publicKeyStripeCheckout = "";
  listPayment = [];
  policy = [];

  cartInfo: any = {}; // Thông tin cart đã tạo
  cartId = "";
  cart = {}; // Cart giỏ hàng
  listCart = [];
  cartInfoAdded: any = {}; // Thông tin cart vừa thêm vào giỏ hàng

  drawerProduct = false;

  loading = false;
  loadingTable = false;

  @Mutation
  setPolicy(policy: any) {
    this.policy = policy;
  }

  @Mutation
  setDrawerProduct(value: boolean) {
    this.drawerProduct = value;
  }

  @Mutation
  setCart(cart: any) {
    this.cart = cart;
    console.log("this.cart :>> ", this.cart);
  }

  @Mutation
  setCartInfo(cartInfo: any) {
    if (cartInfo) {
      this.cartInfo = cartInfo;
      this.cartId = cartInfo._id;
    }
  }

  @Mutation
  setCartInfoAdded(cartInfoAdded: any) {
    if (cartInfoAdded) {
      this.cartInfoAdded = cartInfoAdded;
    }
  }

  @Mutation
  setListCart(listCart: any) {
    this.listCart = listCart;
  }

  @Mutation
  setValueBoolane({ value, type }: { value: boolean; type: string }) {
    if (type === "loadingTable") {
      this.loadingTable = value;
    } else if (type === "loading") {
      this.loading = value;
    }
  }

  @Mutation
  setPublicKey(listPayment: any) {
    if (listPayment && listPayment.length) {
      const findIndexPaypal = listPayment.findIndex(
        (payment: any) => payment.type === "paypal"
      );
      if (findIndexPaypal !== -1) {
        this.publicKeyPaypal =
          listPayment[findIndexPaypal]?.paymentId?.publicKey || "";
        this.policy = listPayment[findIndexPaypal]?.paymentId?.policyId || [];
      }
      const findIndexStripe = listPayment.findIndex(
        (payment: any) => payment.type === "stripe"
      );
      if (findIndexStripe !== -1) {
        this.publicKeyStripe =
          listPayment[findIndexStripe]?.paymentId?.publicKey || "";
      }
      this.listPayment = listPayment;
    }
  }

  @Mutation
  setPublicKeyStripe(publicKey: string) {
    this.publicKeyStripeCheckout = publicKey;
  }

  @Mutation
  setListPayment(listPayment: any) {
    // console.log('listPayment :>> ', listPayment);
    if (listPayment && listPayment.length) {
      const findIndexPaypal = listPayment.findIndex(
        (payment: any) => payment.type === "paypal"
      );
      if (findIndexPaypal !== -1) {
        this.publicKeyPaypal =
          listPayment[findIndexPaypal]?.paymentId?.publicKey || "";
        this.policy = listPayment[findIndexPaypal]?.paymentId?.policyId || [];
      }
      const findIndexStripe = listPayment.findIndex(
        (payment: any) => payment.type === "stripe"
      );
      if (findIndexStripe !== -1) {
        this.publicKeyStripe =
          listPayment[findIndexStripe]?.paymentId?.publicKey || "";
      }
    }
    this.listPayment = listPayment;
  }

  @Action({ rawError: true })
  async getPolicy({ ctx }: { ctx: Vue }) {
    const { $api, $store, $axios } = ctx;
    try {
      const domain = $store.state.product.domain || "";
      $store.commit("cart/setValueBoolane", {
        value: true,
        type: "loadingTable",
      });
      const isHomePage = $store.state?.product?.isHomePage || false
      if (isHomePage) {
        const response = await $axios.$get(`${api}/${resources.HomePage}/policy/${domain}`)
        if (response?.data) {
          const logoImage = `${response?.data?.logoId?.imageUrl
            ? `${api}/${response?.data?.logoId?.imageUrl}`
            : "/logous.png"
            }`;
          $store.commit("cart/setPolicy", response.data);
          $store.commit("setting/setLogo", logoImage);
          $store.commit(
            "cart/setPolicy",
            response?.data?.paymentId?.policyId || []
          );
        }
      } else {
        const response = await $api.payment.userListPolicy(domain);
        if (response?.data) {
          $store.commit("cart/setPolicy", response.data);
        }
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    } finally {
      $store.commit("cart/setValueBoolane", {
        value: false,
        type: "loadingTable",
      });
    }
  }

  @Action({ rawError: true })
  async getListCart({ ctx, cartId }: { ctx: Vue; cartId: string }) {
    try {
      const { $store, $api } = ctx;
      $store.commit("cart/setValueBoolane", {
        value: true,
        type: "loadingTable",
      });
      const response = await $api.cart.getMyCart(cartId);
      if (response?.data) {
        $store.commit("cart/setCart", response.data);
      }
    } catch (error) {
      ctx.$store.dispatch("cart/createCart", { ctx });
      // apiErrorHandler(error, ctx)
    } finally {
      ctx.$store.commit("cart/setValueBoolane", {
        value: false,
        type: "loadingTable",
      });
    }
  }

  // @Action({ rawError: true })
  // async addToCart({
  //   ctx,
  //   cartId,
  //   payload,
  // }: {
  //   ctx: Vue;
  //   cartId: string;
  //   payload: any;
  // }) {
  //   try {
  //     const { $store, $api } = ctx;
  //     $store.commit("cart/setValueBoolane", { value: true, type: "loading" });
  //     const response = await $api.cart.addToCart(cartId, payload);
  //   } catch (error) {
  //     apiErrorHandler(error, ctx);
  //   } finally {
  //     ctx.$store.commit("cart/setValueBoolane", {
  //       value: false,
  //       type: "loading",
  //     });
  //   }
  // }

  @Action({ rawError: true })
  async onAddToCart({ ctx, variantId, product, quantityProduct, domain, isBuyItNow }: { ctx: Vue, variantId: string, product: any, quantityProduct: number, domain: string, isBuyItNow: boolean }) {
    try {
      const { $axios, $store, $api, $gtag, $route } = ctx
      $axios.setHeader(
        "secret",
        "RtV30HXkxzgdV3SX83XK4UuR7k2z9mtZ0Kxo1DYkHpt4CLF9gPM4u7ysa0cWIWwXqJUCSVut7gIouorFSrbVuejy5HghYdr9TQLm"
      );

      let sessionId = getSessionId();
      if (!sessionId) {
        const charRandom = randomCharacter().toLocaleLowerCase();
        const unixTimeStamp = Math.floor(new Date().getTime() / 1000);
        sessionId = `${charRandom}_${unixTimeStamp}`;
        setSessionId(sessionId);
      }

      const query = $store.state.sitePage.utm || {};
      if (!isBuyItNow) {
        await $api.cart.viewAddToCart(query, sessionId, domain);
      }
      let cart: any = onGetListCart()
      const findIdxProductExist = cart.findIndex(
        (variant: any) => variant._id === variantId || variant.variantSellerId === variantId
      );
      if (findIdxProductExist !== -1) {
        cart[findIdxProductExist].quantity += quantityProduct;
      } else {
        if (product?.listVariantSellerId?.length) {
          const findVariant = product.listVariantSellerId.find(
            (variant: any) => variant._id === variantId || variant.variantSellerId === variantId
          );
          let listVariant = {};
          if (findVariant) {
            for (let i = 0; i < findVariant.listOption.length; i++) {
              let item: any = {};
              item[`${findVariant.listOption[i].name}`] =
                findVariant.listValue[i];
              listVariant = { ...listVariant, ...item };
            }
            let payload: any = {
              ...findVariant,
              quantity: quantityProduct,
              productName: product.productName,
              listVariant,
            };
            cart.push(payload);
          }
        }
      }
      localStorage.setItem("cart", JSON.stringify(cart));
      $store.commit("cart/setListCart", cart);
      if (!isBuyItNow) trackEventAddToCart(ctx, variantId);
      $gtag.event("click_add_to_cart", {
        event_category: "click add to cart",
        event_label: "click add to cart",
      });
      // if (!value) {
      // 	this.$pushNotification.success(
      // 		`${this.$language.addToCartSuccessfully}`
      // 	);
      // }
      // }
    } catch (error: any) {
      const message =
        error?.response?.data?.message || error?.message || error || "";
      ctx.$sentry.captureException(message || "An error occurred");
      apiErrorHandler(error, ctx)
    }
  }



  @Action({ rawError: true })
  async updateCart({
    ctx,
    cartId,
    payload,
  }: {
    ctx: Vue;
    cartId: string;
    payload: any;
  }) {
    try {
      const { $store, $api } = ctx;
      await $api.cart.updateCart(cartId, payload);
      $store.dispatch("cart/getListCart", { ctx, cartId });
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async updateComboCart({
    ctx,
    cartId,
    payload,
  }: {
    ctx: Vue;
    cartId: string;
    payload: any;
  }) {
    try {
      const { $store, $api } = ctx;
      await $api.cart.updateCombocart(cartId, payload);
      $store.dispatch("cart/getListCart", { ctx, cartId });
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async deleteCart({
    ctx,
    cartId,
    payload,
  }: {
    ctx: Vue;
    cartId: string;
    payload: any;
  }) {
    try {
      const { $store, $api } = ctx;
      await $api.cart.deleteCart(cartId, payload);
      $store.dispatch("cart/getListCart", { ctx, cartId });
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }
  @Action({ rawError: true })
  async deleteComboCart({
    ctx,
    cartId,
    comboId,
  }: {
    ctx: Vue;
    cartId: string;
    comboId: string;
  }) {
    try {
      const { $store, $api } = ctx;
      await $api.cart.deleteComboCart(cartId, comboId);
      $store.dispatch("cart/getListCart", { ctx, cartId });
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }
}

export default CartModule;
