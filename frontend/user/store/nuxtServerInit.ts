import { ActionTree } from "vuex";
import { getModule } from "vuex-module-decorators";
import { Context } from "@nuxt/types/app";

export const state = () => ({});
export type RootState = ReturnType<typeof state>;

export const actions: ActionTree<RootState, RootState> = {
  async nuxtServerInit({ commit }, { store, app, req }: Context) {
    console.log("req :>> ", req);
  },
};
