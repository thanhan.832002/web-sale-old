import Vue from 'vue'
import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { IAuth, ITokensResponse, IUser } from '~/interfaces/auth'
import { accessTokenKey, refreshTokenKey, userInfoKey } from '~/common/constaint';
import { apiErrorHandler, redirectPage } from '~/utils/apiHanlers';
import { $localForage } from '~/plugins/localforage.client';
import { Store } from "vuex"
import { $axios } from '~/utils/axios';
import authApi from '~/api/auth';
@Module({
    name: 'user',
    namespaced: true,
    stateFactory: true
})
class UserModule extends VuexModule {
    accessToken: String | undefined
    refreshToken: String | undefined
    userInfo: any | undefined

    get isLogin() {
        return !!this.accessToken;
    }

    @Mutation
    setUser(user: any) {
        this.userInfo = user
    }

    @Mutation
    async Authorization({ accessToken, refreshToken, user }: { accessToken: string, refreshToken: string, user: IUser }) {
        this.accessToken = accessToken
        this.userInfo = user
        this.refreshToken = refreshToken
    }

    @Mutation
    userLogout() {
        try {
            this.accessToken = ""
            this.refreshToken = ""
            this.userInfo = null
        } catch (error) {
            ////console.log(error)

        }
    }

    @Mutation
    setAccessToken(accessToken: string) {
        this.accessToken = accessToken
    }

    @Action
    async login({ ctx, payload }: { ctx: Vue, payload: IAuth }) {
        try {
            const { $api, $store } = ctx
            const response = await $api.auth.login(payload)
            let {
                data: {
                    //@ts-ignore
                    tokens: { accessToken, refreshToken },
                    user = null
                } = {},
            } = response;
            $store.commit('user/Authorization', { accessToken, refreshToken, user })
            await ctx.$localForage.setItem(accessTokenKey, accessToken)
            await ctx.$localForage.setItem(refreshTokenKey, refreshToken)
            await ctx.$localForage.setItem(userInfoKey, user)
            localStorage.setItem("accessToken", accessToken);
            localStorage.setItem("refreshToken", refreshToken);
            ctx.$pushNotification.success('Login successfully');
            redirectPage(`/admin/dashboard`);
        } catch (error) {
            apiErrorHandler(error, ctx)
        }
    }

    @Action({ rawError: true })
    async register({ ctx, payload }: { ctx: Vue, payload: any }) {
        try {
            const { $api, $store } = ctx
            const response = await $api.auth.register(payload)
            ctx.$pushNotification.success('Create account successfully');
            redirectPage(`/auth/login`);
        } catch (error) {
            apiErrorHandler(error, ctx)
        }
    }

    @Action({ rawError: true })
    async renewAccessToken({ store }: { store: Store<any> }) {
        try {
            //@ts-ignore
            const { $axios, $api, $localForage } = this.store
            const refreshToken = await $localForage.getItem(refreshTokenKey);
            if (!refreshToken) {
                return this.logout({ store })
            }
            const response = await $axios.$post('/auth/renew-token', { refreshToken })
            let accessToken
            if (response?.data?.token) {
                accessToken = response.data.token
            } else {
                throw new Error()
            }
            //console.log('accessTokenKey :>> ', accessToken);
            store.commit('user/setAccessToken', accessToken)
            $axios.setHeader("Authorization", `Bearer ${accessToken}`);
            await $localForage.setItem(accessTokenKey, accessToken)
            await localStorage.setItem(accessTokenKey, accessToken)
            return accessToken
        } catch (error) {
            this.logout({ store })
            throw error
        }
    }

    @Action({ rawError: true })
    async logout({ store }: { store: Store<any> }) {
        try {
            await $localForage.setItem(accessTokenKey, null)
            await $localForage.setItem(refreshTokenKey, null)
            await $localForage.setItem(userInfoKey, null)
            localStorage.setItem("accessToken", "");
            localStorage.setItem("refreshToken", "");
            store.commit('user/userLogout')
        } catch (error) {
            ////console.log(error)
        }
    }
}

export default UserModule