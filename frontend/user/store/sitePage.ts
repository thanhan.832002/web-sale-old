import Vue from "vue";
import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";
import { IAuth, ITokensResponse, IUser } from "~/interfaces/auth";
import {
  accessTokenKey,
  api,
  apiLocal,
  refreshTokenKey,
  userInfoKey,
} from "~/common/constaint";
import { apiErrorHandler, redirectPage } from "~/utils/apiHanlers";
import { IApiPlugin, resources } from "~/interfaces/api";
import collectionApi from "~/api/collection";
import { $axios } from "~/utils/axios";
import { Context } from "@nuxt/types";

interface IUtm {
  utm_source: string
  utm_medium: string
  utm_content: string
  utm_campaign: string
}

@Module({
  name: "sitePage",
  namespaced: true,
  stateFactory: true,
})
export default class sitePage extends VuexModule {
  myIp: any = {
    country: "Global",
    countryCode: "global",
    status: "success"
  }
  ip = ""

  utm: any = {}

  imageOg = ""

  lazyloading = false
  loadingFullPage = false

  @Mutation
  setLoadingFullPage(loading: boolean) {
    this.loadingFullPage = loading
  }

  @Mutation
  setLazyLoading(value: boolean) {
    this.lazyloading = value
  }

  @Mutation
  setMyIp(myIp: any) {
    if (myIp && myIp.country) {
      this.myIp = {
        countryCode: myIp.countryCode,
        country: myIp.country
      }
    }
  }

  @Mutation
  setIp(ip: string) {
    if (ip) {
      this.ip = ip
    }
  }

  @Mutation
  setImageOg(imageOg: any) {
    this.imageOg = imageOg
  }

  @Mutation
  setUtm(utm: any) {
    for (let elem in utm) {
      if (utm[elem]) {
        this.utm[`${elem}`] = utm[elem]
        console.log('this.utm :>> ', this.utm);
      }
    }
  }

  // @Mutation
  // setUserAgent(userAgent: any) {
  //   this.userAgent = userAgent
  // }

  // @Mutation
  // setHeader(headers: any) {
  //   this.headers = headers
  // }

  // @Mutation
  // setXRealIp(XRealIp: string) {
  //   this.XRealIp = XRealIp
  // }

  // @Mutation
  // setDataXRealIp(dataXRealIp: any) {
  //   this.dataXRealIp = dataXRealIp
  // }

  @Action({ rawError: true })
  async getSitePage({
    req,
    $axios,
    store,
    params,
    app,
  }: {
    req: any;
    $axios: any;
    store: any;
    params: any;
    app: Vue;
  }) {
    try {
      const { slug } = params;
      // let domain = "t-2whiteningtoothpaste.toptrend1day.com";
      // let domain = "toptrend1day.com";
      let domain = store.state.product.domain || "";
      if (slug) {
        domain = `${slug}.${domain}`;
      }
      const proms = [
        $axios.$get(`${apiLocal()}/${resources.HomePage}/list-domain`),
      ];
      const [responseListDomain] = await Promise.all(proms);
      let isHomePage = false;
      if (responseListDomain?.data?.length) {
        const findDomain = responseListDomain.data.findIndex(
          (item: any) => item === domain
        );
        if (findDomain >= 0) {
          isHomePage = true;
        }
      }
      // Get setting home page/ detail product
      if (isHomePage) {
        const promsHomePage = [
          $axios.$get(`${apiLocal()}/${resources.HomePage}/policy/${domain}`),
          $axios.$get(`${apiLocal()}/${resources.HomePage}/${domain}`), // Seting Home Page
        ];
        const [responsePolicy, responseSetting] = await Promise.all(
          promsHomePage
        );
        const logoImage = `${responsePolicy?.data?.logoId?.imageUrl
          ? `${api}/${responsePolicy?.data?.logoId?.imageUrl}`
          : "/logous.png"
          }`;
        const logo = responseSetting?.data?.logoId || "";
        const settingInfo = {
          storeName: responseSetting.data.homepageName,
          phoneNumber: responseSetting.data.phone,
          supportEmail: responseSetting.data.supportEmail,
          companyInfo: responseSetting.data.address,
          logo: logo?.imageUrl || "",
        };
        store.commit("setting/setSiteInfo", settingInfo);
        store.commit("setting/setLogo", logoImage);
        store.commit(
          "cart/setPolicy",
          responsePolicy?.data?.paymentId?.policyId || []
        );
        store.commit("setting/setSettingHomePage", responseSetting?.data || {});
      } else {
        const promsDetailProduct = [
          $axios.$get(
            `${apiLocal()}/${resources.StoreBuyer}/setting/${domain}`
          ),
          $axios.$get(`${apiLocal()}/${resources.PaymentBuyer}/list/${domain}`),
        ];
        const [responseSetting, responseListPayment] = await Promise.all(
          promsDetailProduct
        );
        console.log("responseSetting.data :>> ", responseSetting.data);
        const { logo } = responseSetting.data;
        const settingInfo = {
          ...responseSetting.data,
          logo: logo?.imageUrl || "logous.png",
        };
        if (logo) {
          //@ts-ignore
          app.head.link[0].href = `${api}/${logo?.imageUrl || "/logous.png"}`;
        }
        //@ts-ignore
        app.head.title = responseSetting.data.webTitle || "Store";
        store.commit("setting/setSiteInfo", settingInfo);
        store.commit("cart/setListPayment", responseListPayment.data);
        store.commit("cart/setPublicKey", responseListPayment.data);
      }
      store.commit("product/setDomain", domain);
      store.commit("product/setIsHomePage", isHomePage);
      // await ctx.store.dispatch("nuxtServerInit/nuxtServerInit", ctx);
    } catch (error) {
    }
  }
}
