import Vue from 'vue'
import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { apiErrorHandler } from '~/utils/apiHanlers'

@Module({
    name: 'checkout',
    stateFactory: true,
    namespaced: true,
})
class CheckoutModule extends VuexModule {

}

export default CheckoutModule