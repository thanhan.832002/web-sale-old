import Vue from 'vue'
import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { IAuth, ITokensResponse, IUser } from '~/interfaces/auth'
import { accessTokenKey, refreshTokenKey, userInfoKey } from '~/common/constaint';
import { apiErrorHandler, redirectPage } from '~/utils/apiHanlers';
import axios from "axios"
import { api } from '~/common/constaint';
import { resources } from '~/interfaces/api';
@Module({
    name: 'adminGallery',
    namespaced: true,
    stateFactory: true
})
class AdminGalleryModule extends VuexModule {
    config = {
        page: 1,
        limit: 100,
        search: ""
    };

    isUploadImage = false
    progress = 0
    loadingUploadImage = false
    loading = false
    loadingTableTag = false
    loadingTable = false
    listTag = []
    listGallery = []
    listCoverId: any = []
    tagId: string = ""

    @Mutation
    setListTag(listTag: any) {
        this.listTag = listTag
    }

    @Mutation
    setListGallery(listGallery: any) {
        this.listGallery = listGallery
    }

    @Mutation
    setValueBoolane({ status, type }: { status: boolean, type: string }) {
        if (type === 'loading') {
            this.loading = status
        } else if (type === 'loadingTableTag') {
            this.loadingTableTag = status
        } else if (type === 'loadingTable') {
            this.loadingTable = status
        } else if (type === 'isUploadImage') {
            this.isUploadImage = status
        }
    }
    @Mutation
    setProgress(progress: number) {
        this.progress = progress
    }

    @Mutation
    setTagId(tagId: string) {
        if (tagId) {
            this.tagId = tagId
        }
    }
    @Mutation
    setListCoverId(listCoverId: any) {
        this.listCoverId = listCoverId
    }
    @Action({ rawError: true })
    async onGetListTag({ ctx }: { ctx: Vue }) {
        try {
            const { $api, $store } = ctx
            ctx.$store.commit('adminGallery/setValueBoolane', { status: true, type: 'loadingTableTag' })
            const response = await $api.gallery.getListTag(this.config)
            if (response) {
                const listTag = [{ name: "All", _id: "" }, ...response.data.data]
                $store.commit('adminGallery/setListTag', listTag)
            }
        } catch (error) {
            apiErrorHandler(error, ctx)
        } finally {
            ctx.$store.commit('adminGallery/setValueBoolane', { status: false, type: 'loadingTableTag' })
        }
    }

    @Action({ rawError: true })
    async onCreateTag({ ctx, name }: { ctx: Vue, name: any }) {
        try {
            const { $api, $store, $pushNotification } = ctx
            $store.commit('adminGallery/setValueBoolane', { status: true, type: 'loading' })
            await $api.gallery.createTag(name)
            $pushNotification.success("Create tag successfully")
            await $store.dispatch("adminGallery/onGetListTag", { ctx })
        } catch (error) {
            apiErrorHandler(error, ctx)
        } finally {
            ctx.$store.commit('adminGallery/setValueBoolane', { status: false, type: 'loading' })
        }
    }

    @Action({ rawError: true })
    async onUpdateTag({ ctx, payload }: { ctx: Vue, payload: any }) {
        try {
            const { $api, $store, $pushNotification } = ctx
            await $api.gallery.updateTag(payload.tagId, payload.name)
            $pushNotification.success("Update tag successfully")
            await $store.dispatch("adminGallery/onGetListTag", { ctx })
        } catch (error) {
            apiErrorHandler(error, ctx)
        }
    }

    @Action({ rawError: true })
    async onDeleteTag({ ctx, tagId }: { ctx: Vue, tagId: string }) {
        try {
            const { $api, $store, $pushNotification } = ctx
            await $api.gallery.deleteTag(tagId)
            $pushNotification.success("Delete tag successfully")
            await $store.dispatch("adminGallery/onGetListTag", { ctx })
        } catch (error) {
            apiErrorHandler(error, ctx)
        }
    }

    @Action({ rawError: true })
    async onUploadImageGallery({ ctx, payload }: { ctx: Vue, payload: any }) {
        try {
            const { $api, $store, $pushNotification } = ctx
            await ctx.$store.dispatch('user/renewAccessToken', { store: $store })
            const accessToken = await ctx.$localForage.getItem(accessTokenKey)
            const config = {
                method: "POST",
                url: `${api}/${resources.Gallery}/create`,
                headers: {
                    "Content-Type": "multipart/form-data",
                    Authorization: `Bearer ${accessToken}`
                },
                data: payload,
                onUploadProgress: (progressEvent: any) => {
                    //@ts-ignore
                    const progress = parseInt(Math.round((progressEvent.loaded * 100) / progressEvent.total));
                    // Update state here
                    $store.commit('adminGallery/setProgress', progress)
                },
            };
            await axios(config)
            $store.commit('adminGallery/setProgress', 0)
            $store.dispatch('adminGallery/onGetListImgCaterogy', { ctx, tagId: this.tagId })
        } catch (error: any) {
            apiErrorHandler(error, ctx)
        }
    }

    @Action({ rawError: true })
    async onGetListImgCaterogy({ ctx, tagId }: { ctx: Vue, tagId: string }) {
        try {
            const { $api, $store, $pushNotification } = ctx
            const response = await $api.gallery.getList(tagId)
            if (response) {
                const listGallery = response.data
                $store.commit('adminGallery/setListGallery', listGallery)
            }
        } catch (error) {
            apiErrorHandler(error, ctx)
        }
    }

}

export default AdminGalleryModule