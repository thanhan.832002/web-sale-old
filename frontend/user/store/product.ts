import Vue from "vue";
import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";
import { apiErrorHandler } from "~/utils/apiHanlers";
import { apiLocal, domainLocal } from "~/common/constaint";
//@ts-ignore
import language from "~/locales/language-product.json";
import checkCart from "~/utils/checkCart";
import { IBodyAutoApply } from "~/interfaces/product";

@Module({
  name: "product",
  stateFactory: true,
  namespaced: true,
})
class ProductModule extends VuexModule {
  language = {};
  dialogAddToCart = false;
  dialogAdded = false;
  dialogAddedBestSeller = false;
  dialogAddedRecently = false;
  dialogAddedProduct = false;

  isDonePaypalHome = false;

  loadingTable = false;
  loading = false;
  loadingPaypal = false;

  product = {};
  productId = "";
  productName = "";

  listRelatedProduct = [];
  listReviewProduct = [];

  domain = "";
  slug = ""
  isCheckSlug = false

  priceVariant = 0;
  priceProduct = 0;
  priceDiscount = 0
  listImage = [];

  listDiscount = [];

  upsale: any = {};
  isHomePage = false;

  listShipping: any = [];
  nameShipping: string = "";
  findGreatThanShipping: any = {};

  @Mutation
  setIsDonePaypalHome(value: boolean) {
    this.isDonePaypalHome = value;
  }

  @Mutation
  setIsHomePage(isHomePage: boolean) {
    this.isHomePage = isHomePage || false;
  }

  @Mutation
  setUpsale(upsale: any) {
    if (upsale && upsale.sellerDiscountId) {
      upsale.minutes = Math.floor(upsale.sellerDiscountId.exp / 60);
      upsale.second = upsale.sellerDiscountId.exp % 60;
    }
    this.upsale = upsale;
  }

  @Mutation
  setMinutes(minutes: number) {
    this.upsale.minutes = minutes || 0;
  }

  @Mutation
  setSecond(second: number) {
    this.upsale.second = second || 0;
  }

  @Mutation
  setListImage(listImage: any) {
    this.listImage = listImage;
  }

  @Mutation
  setPriceVariant(priceVariant: number) {
    this.priceVariant = priceVariant || 0;
  }

  @Mutation
  setPriceProduct(priceProduct: number) {
    this.priceProduct = priceProduct || 0;
  }

  @Mutation
  setProductName(productName: string) {
    this.productName = productName;
  }

  @Mutation
  setLanguage(language: any) {
    this.language = language;
  }

  @Mutation
  setDomain(domain: any) {
    this.domain = domain;
  }

  @Mutation
  setSlug(slug: string) {
    this.slug = slug
  }

  @Mutation
  setIsCheckSlug(value: boolean) {
    this.isCheckSlug = value
  }

  @Mutation
  setLoadingPaypal(loadingPaypal: boolean) {
    this.loadingPaypal = loadingPaypal;
  }

  @Mutation
  setValueBoolane({ status, type }: { status: boolean; type: string }) {
    if (type === "loading") {
      this.loading = status;
    } else if (type === "loadingTable") {
      this.loadingTable = status;
    }
  }

  @Mutation
  setDialogAddToCart(value: boolean, type: string) {
    this.dialogAddToCart = value;
  }

  @Mutation
  setProduct(product: any) {
    this.product = product;
    this.productId = product._id;
    // product.relatedProduct.forEach((product: any) => {
    //     if (product.variant.length) {
    //         product.variantId = product.variant[0]._id;
    //     }
    // });
  }

  @Mutation
  setListDiscount(listDiscount: any) {
    this.listDiscount = listDiscount;
  }

  @Mutation
  setListShipping(listShipping: any) {
    this.listShipping = listShipping;
  }

  @Mutation
  getPriceShipping({ listCart, totalP }: { listCart: any; totalP: number }) {
    if (listCart && listCart.length) {
      let total = 0;
      let totalPrice = Math.round(totalP * 100) / 100;
      for (let product of listCart) {
        total += product.quantity;
      }

      let findGreatThanShipping: any = {};
      let listShippingFilter = this.listShipping.filter(
        (shipping: any) =>
          shipping.quantity <= total || shipping.total <= totalPrice
      );

      if (listShippingFilter.length) {
        this.nameShipping = listShippingFilter[0]?.name || "Shipping";
        let findPriceGood: any = {}
        let findQuantityGood: any = {}
        for (let shipping of listShippingFilter) {
          if (shipping.quantity <= total) {
            findQuantityGood = shipping
          }
          if (shipping.total <= totalPrice) {
            findPriceGood = shipping
          }
        }
        if (findQuantityGood && findQuantityGood._id) {
          findGreatThanShipping = findQuantityGood
        }
        if (findPriceGood && findPriceGood._id) {
          findGreatThanShipping = findPriceGood
        }
      }
      this.findGreatThanShipping = findGreatThanShipping
    }
  }

  @Mutation
  setDialogAdded({ value, type }: { value: boolean; type: string }) {
    if (type === "product") {
      this.dialogAddedProduct = value;
    } else if (type === "recently") {
      this.dialogAddedRecently = value;
    } else if (type === "bestSeller") {
      this.dialogAddedBestSeller = value;
    }
    // console.log('this.dialogAddedProduct :>> ', this.dialogAddedProduct);
    // console.log('this.dialogAddedRecently :>> ', this.dialogAddedRecently);
    // console.log('this.dialogAddedBestSeller :>> ', this.dialogAddedBestSeller);
  }

  @Mutation
  setListRelatedProduct(listRelatedProduct: any) {
    this.listRelatedProduct = listRelatedProduct;
  }

  @Mutation
  setListReviewProduct(listReviewProduct: any) {
    this.listReviewProduct = listReviewProduct;
  }

  @Mutation
  setPriceDiscount(price: number) {
    this.priceDiscount = price
  }

  @Action({ rawError: true })
  async onApplyCoupon({ ctx, body }: { ctx: Vue, body: IBodyAutoApply }) {
    const { $api, $store } = ctx
    try {
      let priceCoupon = 0
      const response = await $api.coupon.autoApply({ ...body, slug: this.slug })
      if (response?.data) {
        if (response.data.discountType === 1) {
          priceCoupon = (body.total * response.data.discount) / 100;
        } else if (response.data.discountType === 2) {
          priceCoupon =
            response.data.discount > body.total
              ? body.total
              : response.data.discount;
        } else {
          priceCoupon = this.findGreatThanShipping.price
        }
      }
      $store.commit("product/setPriceDiscount", priceCoupon)
    } catch (error) {
      apiErrorHandler(error, ctx)
    }
  }

  @Action({ rawError: true })
  async getListUpsale({ ctx }: { ctx: Vue }) {
    const { $api, $store } = ctx;
    try {
      const domain = $store.state.product.domain || domainLocal;
      const response = await $api.product.getListUpsale(domain);
      if (response.data) {
        $store.commit("product/setUpsale", response?.data || {});
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async getListDiscount({ ctx, storeId }: { ctx: Vue; storeId: string }) {
    try {
      const { $api, $store } = ctx;
      const response = await $api.coupon.getListDiscount(storeId);
      console.log("response :>> ", response);
    } catch (error) {
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async getDetailProduct({ ctx }: { ctx: Vue }) {
    try {
      const { $api, $store } = ctx;
      const domain = $store.state.product.domain || domainLocal;
      const query = $store.state.sitePage.utm || {};
      const response = await $api.product.getDetailProductDomain(domain, query);
      let listOption: any = [];

      if (response?.data) {
        listOption = response?.data?.listVariantSellerId[0]?.listOption || [];
        if (listOption.length === 1) {
          response.data.listVariantSellerId =
            response.data.listVariantSellerId.filter(
              (item: any) => item.isSellable
            );
        }
        const { listVariantSellerId } = response.data;
        let listAllValue: any = [];

        listVariantSellerId[0].listValue.forEach((value: any, i: number) => {
          listAllValue[i] = [];
          for (let variant of listVariantSellerId) {
            listAllValue[i].push(variant.listValue[i]);
          }
        });
        listAllValue = listAllValue.map((valueArr: any) => [
          ...new Set(valueArr),
        ]);
        listAllValue.forEach((value: any, i: number) => {
          listOption[i].values = value;
        });
        $store.commit(
          "sitePage/setLazyLoading",
          response.data.listVariantSellerId[0]?.isSellable || false
        );
      }
      // set image first in link
      if (response?.data?.listVariantSellerId?.length) {
        const variantSeller = response.data.listVariantSellerId[0];
        if (variantSeller?.imageId?.imageUrl) {
          const linkImg = `${apiLocal()}/${variantSeller.imageId.imageUrl}`;
          $store.commit("sitePage/setImageOg", linkImg);
        }
      }

      response.data.description = response.data.description.replaceAll(
        'class="blur-up lazyload-placeholder lazyloaded ls-is-cached"',
        'class="blur-up lazyload-placeholder lazyloaded ls-is-cached" loading="lazy"'
      );
      response.data.description = response.data.description.replaceAll(
        'width="',
        'sizes="sm:100vw md:50vw lg:400px" max-width="'
      );
      const product = {
        ...response.data,
        listVariantOptionId:
          listOption.map((variant: any) => ({
            selected: 0,
            name: variant.name,
            values: variant.values,
            _id: variant._id,
            id: variant.id,
          })) || [],
        imageIds: response?.data?.listImageId || [],
      };
      if (response?.data?.listVariantSellerId?.length) {
        response.data.priceProductSrr =
          response.data.listVariantSellerId[0].price;
      }
      let productLanguage = {};
      if (response?.data?.language) {
        const { language: languageResponse } = response.data;
        productLanguage = language[languageResponse] || language["EN"];
      }

      let listImageId = [...product.listImageId];
      for (let item of response.data.listVariantSellerId) {
        if (item.imageId) {
          listImageId.push(item.imageId);
        }
      }
      listImageId = ctx.$utils.uniqBy(listImageId, "imageUrl");
      $store.commit("product/setProduct", product);
      $store.commit("product/setLanguage", productLanguage);
      $store.commit("product/setListImage", listImageId);
      checkCart(ctx, product);
    } catch (error) {
      ctx.$router.push("/error");
      apiErrorHandler(error, ctx);
    }
  }

  @Action({ rawError: true })
  async getListRelatedProduct({
    ctx,
    productSlug,
  }: {
    ctx: Vue;
    productSlug: string;
  }) {
    try {
      const { $store, $api } = ctx;
      $store.commit("product/setValueBoolane", {
        status: true,
        type: "loadingTable",
      });
      const response = await $api.product.getRelatedProduct(productSlug);
      if (response?.data?.data) {
        for (let product of response.data.data) {
          if (product && product.variant && product.variant.length) {
            product.variantId = product.variant[0]._id;
          }
        }
        const listRelatedProduct = response.data.data;
        $store.commit("product/setListRelatedProduct", listRelatedProduct);
      }
    } catch (error) {
      apiErrorHandler(error, ctx);
    } finally {
      ctx.$store.commit("product/setValueBoolane", {
        status: false,
        type: "loadingTable",
      });
    }
  }
}

export default ProductModule;
