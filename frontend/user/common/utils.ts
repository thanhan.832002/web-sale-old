import { api } from './constaint';
import param from './param'
export const objectToParam = (obj: object) => param(obj)
import { Context } from '@nuxt/types';


function isObject(item: any) {
    return (typeof item === 'object' && !Array.isArray(item) && item !== null);
}
export type IUtils = {
    formatCurrency: (price: number) => string
    formatCurrencyFloat: (price: any) => string
    calcDollar: (price: number) => string
    cloneDeep: (obj: any) => any
    uniq: (array: any) => any
    uniqBy: (array: any, key: string) => any
    sleep: (milliseconds: number) => any
    convertImg: (url: string) => string
    compare: (obj1: any, obj2: any) => boolean
}
// export const isTokenExpired = async (token: string) => {
//     let { exp } = jwt.decode(token)
//     const now = Date.now().valueOf() / 1000
//     return now > exp
// }

export const utils = (ctx: Context) => ({
    formatCurrency: (price: number) => {
        return `${price && price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`
    },

    formatCurrencyFloat: (price: any) => {
        if (isNaN(price)) return 0
        const priceFixed = parseFloat(price)
        return `${priceFixed.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`
    },

    calcDollar: (price: number) => {
        const dollar = (price / 23000).toFixed(2)
        return `~${dollar}$`
    },

    cloneDeep(obj: any): any {
        if (Array.isArray(obj)) {
            const newArr = [];
            for (let i = 0; i < obj.length; i++) {
                newArr[i] = this.cloneDeep(obj[i]);
            }
            return newArr;
        } else if (isObject(obj)) {
            const newObj: any = {};
            for (let key in obj) {
                newObj[key] = this.cloneDeep(obj[key]);
            }
            return newObj;
        } else {
            return obj;
        }
    },

    uniq(array: any) {
        return Array.from(new Set(array));
    },

    uniqBy(array: any, key: string) {
        const seen = new Set();
        return array.filter((item: any) => {
            const k = key ? item[key] : item;
            return seen.has(k) ? false : seen.add(k);
        });
    },

    sleep(milliseconds: number) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(true);
            }, milliseconds);
        });
    },
    convertImg: (url: string) => {
        return `${api}/${url}`
    },
    compare: (obj1: any, obj2: any) => {
        return JSON.stringify(obj1) === JSON.stringify(obj2)
    }
})
