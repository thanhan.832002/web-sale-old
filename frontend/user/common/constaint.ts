export const accessTokenKey = "accessToken";
export const refreshTokenKey = "refreshToken";
export const userInfoKey = "userInfo";

export const api =
  process.env.NODE_ENV == "development"
    ? "https://api.opensitex.com"
    : "https://api.opensitex.com";
export const apiLocal = () => {
  if (process.env.NODE_ENV == "development") return api;
  if (process.client) {
    return api;
  } else {
    return api;
  }
};

export const domainLocal = "";

export const regexEmail =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
