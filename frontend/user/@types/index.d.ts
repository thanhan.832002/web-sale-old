import { Store } from "vuex"
import { IApiPlugin } from "@/interfaces/api"
import ToastInterface from "vue-toastification/dist/types/src/ts/interface"
import { PushNotificationType } from "~/plugins/notify"
import { IUtils } from "~/common/utils"

declare module 'vue/types/vue' {
    interface Vue {
        $api: IApiPlugin,
        $store: Store<any>
        $toast: ReturnType<typeof ToastInterface>
        $pushNotification: PushNotificationType
        $localForage: LocalForage
        $utils: IUtils
    }
}

declare module '@nuxt/types' {
    interface Context {
        $api: IApiPlugin
        $store: Store<any>
        $toast: ReturnType<typeof ToastInterface>
        $pushNotification: PushNotificationType
        $localForage: LocalForage
        $utils: IUtils
    }
}

declare module 'vuex/types/index' {
    interface Store<S> {
        $api: IApiPlugin
        $store: Store<any>
        $toast: ReturnType<typeof ToastInterface>
        $pushNotification: PushNotificationType
        $localForage: LocalForage
        $utils: IUtils
    }
}