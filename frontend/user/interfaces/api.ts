import { AuthApiType } from "~/api/auth";
import { HomeApiType } from "~/api/home";
import { GalleryApiType } from "~/api/gallery";
import { CollectionApiType } from "~/api/collection";
import { ProductApiType } from "~/api/product";
import { VariantApiType } from "~/api/variant";
import { CartApiType } from "~/api/cart";
import { PromotionApiType } from "~/api/promotion";
import { ComboApiType } from "~/api/combo";
import { ShippingApiType } from "~/api/shipping";
import { CouponApiType } from "~/api/coupon";
import { SettingApiType } from "~/api/settings";
import { OrderApiType } from "~/api/order";
import { PaymentApiType } from "~/api/payment";
import { AdsAnalyticApiType } from "~/api/ads-analytic";
import { RecentlyApiType } from "~/api/recently";
import { PixelApiType } from "~/api/pixel";


export interface IApiPlugin {
    home: HomeApiType
    auth: AuthApiType
    gallery: GalleryApiType
    collection: CollectionApiType
    product: ProductApiType
    variant: VariantApiType
    cart: CartApiType
    promotion: PromotionApiType
    pixel: PixelApiType
    combo: ComboApiType
    shipping: ShippingApiType
    coupon: CouponApiType
    setting: SettingApiType
    order: OrderApiType
    payment: PaymentApiType
    adsAnalytic: AdsAnalyticApiType
    recently: RecentlyApiType
}

export enum resources {
    Tag = 'tag',
    Gallery = 'gallery',
    Collection = 'collection',
    Product = 'product',
    ProductBuyer = "product-buyer",
    Variant = "variant",
    Cart = "cart",
    Abandoned = "abandoned",
    View = "view",
    Promotion = 'promotion',
    Pixel = "pixel",
    Combo = 'combo',
    Shipping = 'shipping',
    Coupon = 'coupon',
    Setting = 'setting',
    HomePage = "homepage",
    StoreBuyer = "store-buyer",
    Discount = "discount",
    PaymentBuyer = "payment-buyer",
    Order = 'order',
    Payment = "payment",
    adsAnalytic = "analytics",
    pixel = "pixel",
    recentlyView = 'recently-view'
}