export interface ICreateAbandoned {
    email: string
    listSelectedProduct: IItemListSelectedProduct[]
}

export interface IItemListSelectedProduct {
    variantSellerId: string
    quantity: number
}

export interface IItemTrackingOrderSucces {
    quantity: number
    total: number
    productName: string
    listSelectedProduct: any
}