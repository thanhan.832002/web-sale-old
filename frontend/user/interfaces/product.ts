export interface IBodyAutoApply {
    total: number,
    quantity: number,
    shipping: number,
}