export interface IQueryAutoApply {
    total: number
    quantity: number
    shipping: number
    slug?: string
}