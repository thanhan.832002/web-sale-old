export interface IItemListShipping {
    name: string
    price: number
    quantity: number
    total: number
    _id: string
}