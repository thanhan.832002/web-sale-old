export interface IConfirmPayment {
    "email": string,
    "firstName": string,
    "lastName": string,
    "address": string,
    "apartment": string,
    "companyName": string,
    "zipCode": string,
    "city": string,
    "state": string,
    "country": string,
    "phoneNumber": string,
    "listSelectedProduct": IItemlistSelectedProduct[],
    "couponCode": string,
    "countryIp": string,
    "card": any,
    "paymentIntentID": string
}

export interface ICardConfirmPayment {
    "paymentMethodID"?: string
    orderCode: string
    "paymentIntentID": string
    isAbandon?: boolean
}

export interface IItemlistSelectedProduct {
    "variantSellerId": string,
    "quantity": number
}