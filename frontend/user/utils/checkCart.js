export default function checkCart(ctx, product) {
  try {
    const { $store } = ctx;
    const listCart = JSON.parse(localStorage.getItem("cart")) || [];
    let cart = [];
    if (product?.listVariantSellerId?.length) {
      cart = listCart.filter((cart) => {
        const findVariant = product.listVariantSellerId.find(
          (variant) =>
            (variant._id === cart._id ||
              cart.variantSellerId === variant._id) &&
            variant.price === cart.price &&
            variant.comparePrice === cart.comparePrice &&
            variant.listValue.join(" / ") === cart.listValue.join(" / ") &&
            cart.quantity &&
            product.productName === cart.productName
        );
        return findVariant ? true : false;
      });
    }
    localStorage.setItem("cart", JSON.stringify(cart));
    $store.commit("cart/setListCart", cart);
  } catch (error) {}
}
