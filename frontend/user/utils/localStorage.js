export const onGetListCart = () =>
  JSON.parse(localStorage.getItem("cart")) || [];

export const onGetDate = () => localStorage.getItem("date");

export const onSetItemLocalStorage = (item, value) =>
  localStorage.setItem(item, value);

export const onSetCheckoutInfo = (text, value) =>
  sessionStorage.setItem(text, JSON.stringify(value));

export const onGetCheckoutInfo = (text) => sessionStorage.getItem(text);

export const onClearSession = (text) => sessionStorage.removeItem(text);
