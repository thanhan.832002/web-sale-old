export default (gtagId) => {
    if (gtagId) {
        const script = document.createElement('script')
        script.src = `https://www.googletagmanager.com/gtag/js?id=${gtagId}`
        script.async = true
        document.getElementsByTagName('head')[0].appendChild(script)
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', `${gtagId}`);
    }
};