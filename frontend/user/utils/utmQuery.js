export default function (ctx) {
  const { $store, $route } = ctx;
  const { utm_source, utm_medium, utm_content, utm_campaign } = $route.query;
  if (utm_source || utm_medium || utm_content || utm_campaign) {
    $store.commit("sitePage/setUtm", {
      utm_source,
      utm_medium,
      utm_content,
      utm_campaign,
    });
  }
}
