import axios from "axios";
//@ts-ignore
import AllCountries from "@/countries/AllCountries.json"
import { IApiPlugin } from "~/interfaces/api";

interface IItemCountry {
    name: string
    code: string
}
export default async (store: any, $api: IApiPlugin) => {
    try {
        let ip = ""
        const response = await axios.get("https://lumtest.com/myip.json")
        if (!response || !response.data) return

        ip = response.data.ip || ""
        store.commit("sitePage/setIp", ip);

        const findCountry = AllCountries.find((country: IItemCountry) => country.code === response.data.country)
        if (!findCountry) return

        const body = {
            country: findCountry.name,
            countryCode: findCountry.code,
            status: "success"
        }

        store.commit("sitePage/setMyIp", body)
    } catch (error) {
        console.log('error 1 :>> ', error);
    }
}

// async function getIp(store: any) {
// }