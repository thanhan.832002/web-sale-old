import { AxiosError } from "axios";
import Vue from "vue";
import { $i18n } from "~/plugins/i18n.client"
//@ts-ignore
import enMessage from "../locales/en.json"


export const apiErrorHandler = (error: any, ctx: Vue) => {
    try {
        if (error?.response?.data?.message) {
            const message = error.response.data.message;
            return ctx.$pushNotification.error(enMessage.responseMessage[message]);
        }
    } catch (error) {
        return ctx.$pushNotification.error(`An error has occurred`);
    }
}

export const redirectPage = (href: string) => {
    window.location.href = `${window.location.origin}${href}`
}