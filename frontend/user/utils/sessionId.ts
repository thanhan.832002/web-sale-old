export const getSessionId = () => localStorage.getItem("sessionId")
export const setSessionId = (value: string) => localStorage.setItem("sessionId", value)
export const randomCharacter = () => {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < 5) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
        counter += 1;
    }
    return result;
}