//@ts-nocheck
import { Context } from "@nuxt/types";
import { TwitterTracking } from "~/assets/js/twitter";
import { IItemTrackingOrderSucces } from "~/interfaces/checkout";

export const initFbPixel = (facebookPixelId) => {
  if (facebookPixelId) {
    const script = document.createElement("script");
    script.innerHTML = `!function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');`;
    document.head.appendChild(script);

    const listFbPixel = facebookPixelId
      .split("\n")
      .map((id) => id.trim())
      .filter((id) => id);
    if (fbq) {
      for (let pixel of listFbPixel) {
        fbq("init", `${pixel}`);
        fbq("track", "PageView");
        fbq("track", "ViewContent");
        const noscript = document.createElement("noscript");
        noscript.innerHTML = `<img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=${pixel}&ev=PageView&noscript=1"
    />`;
        document.head.appendChild(noscript);
      }
    }
  }
};

export const trackEventAddToCart = (ctx: Vue, variantId: string) => {
  try {
    if (fbq) {
      fbq("track", "AddToCart", {});
    }

    if (ttq) {
      ttq.track("AddToCart", {
        content_id: variantId
      });
    }

    if (snaptr) {
      snaptr("track", "ADD_CART");
    }

    if (twq) {
      TwitterTracking(twq, ctx, "AddToCart");
    }
  } catch (error) { }
};

export const trackEventCheckoutInitiated = (ctx: Vue) => {
  try {
    if (fbq) {
      fbq("track", "InitiateCheckout", {});
    }

    if (ttq) {
      ttq.track("InitiateCheckout", {
        content_id: "InitiateCheckout"
      });
    }

    if (twq) {
      TwitterTracking(twq, ctx, "InitiateCheckout");
    }
  } catch (error) { }
};

export const trackEventAddPaymentInfo = (ctx: Vue, bodyTrack?: any) => {
  try {
    if (fbq) {
      fbq("track", "AddPaymentInfo", {
        content_type: bodyTrack.productName || "",
        currency: "USD",
        value: bodyTrack.value || 1,
      });
    }

    if (ttq) {
      ttq.track("AddPaymentInfo");
    }

    if (twq) {
      TwitterTracking(twq, ctx, "AddPaymentInfo");
    }
  } catch (error) { }
};

export const trackEventPurchased = (ctx: Vue, order: IItemTrackingOrderSucces) => {
  try {
    if (fbq) {
      fbq("track", "Purchase", {
        currency: "USD",
        value: order.total || 1,
        content_name: order.productName || ""
      }, {
        eventID: order.orderCode || ""
      });
      // fbq("track", "CompletePayment", {
      //   currency: "USD",
      //   value: order.total || 1,
      //   content_name: order.productName || "",
      // });
    }
    if (ttq) {
      // ttq.track("Purchase", {
      //   currency: "USD",
      //   value: order.total || 1,
      //   contents: order.listSelectedProduct.map((product) => ({
      //     content_id: product.orderCode || "",
      //     quantity: product.quantity || 1,
      //     price: product.price || 1,
      //   })),
      //   content_id: `${product.orderCode || '1'}`,
      // });
      ttq.identify({
        email: `${order.email || 'email@example.com'}`,
        // phone_number: `${order.phoneNumber || '+15551234567'}`,
      })
      ttq.track("CompletePayment", {
        currency: "USD",
        value: order.total || 1,
        contents: order.listSelectedProduct.map((product) => ({
          content_id: product.orderCode || "1",
          quantity: product.quantity || 1,
          price: product.price || 1,
        })),
        content_id: `${order.orderCode || '1'}`,
      });
    }

    if (snaptr) {
      snaptr("track", "PURCHASE");
    }
    if (twq) {
      TwitterTracking(twq, ctx, "AddedPaymentInfo");
      TwitterTracking(twq, ctx, "CompletePayment");
    }
  } catch (error) { }
};
