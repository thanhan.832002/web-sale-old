
import { Context, Plugin } from '@nuxt/types'

import { IUtils, utils } from '~/common/utils'

declare module 'vue/types/vue' {
    interface Vue {
        $ultils: IUtils
    }
}
declare module '@nuxt/types' {
    interface NuxtAppOptions {
        $ultils: IUtils
    }
    // nuxtContext.$ultils
    interface Context {
        $ultils: IUtils
    }
}


const ultilPlugin: Plugin = (ctx: Context, inject) => {
    inject('utils', utils(ctx))
}

export default ultilPlugin