import Vue from "vue/types";
import gtagTiktokPixel from "~/utils/tiktok-pixel";

export default function (ctx: any) {
  const { store } = ctx;
  const tiktokPlatformId = store.state.setting.tiktok.platformId;
  if (tiktokPlatformId) {
    const tiktokPixelIds = tiktokPlatformId
      .split("\n")
      .map((id: string) => id.trim())
      .filter((id: string) => id);
    if (tiktokPixelIds && tiktokPixelIds.length) {
      for (let id of tiktokPixelIds) {
        gtagTiktokPixel(id);
      }
    }
  }
}
