import { NuxtAxiosInstance } from "@nuxtjs/axios"
import { redirectPage } from "~/utils/apiHanlers"
export default function ({ $axios, redirect, store }: { $axios: NuxtAxiosInstance, redirect: any, store: any }) {
    // $axios.onRequest(config => {
    //     try {
    //         const {
    //             localStorage: {
    //                 accessToken
    //             } } = store.state
    //         const newConfig = { ...config }
    //         if (accessToken === null) return newConfig
    //         newConfig.headers.Authorization = accessToken ? 'Bearer ' + accessToken : ''
    //         return newConfig
    //     } catch (error) {
    //         return config
    //     }
    // })

    $axios.onError(async (error: any) => {
        const code = parseInt(error.response && error.response.status)
        if (code == 401) {
            await store.dispatch('auth/logout')
            redirectPage('/auth/login')
            throw error
        } else if (code == 403) {
            try {
                const newToken = await store.dispatch('user/renewAccessToken', { store })
                if (newToken) {
                    const originalRequest = error.config;
                    originalRequest.headers.Authorization = `Bearer ${newToken}`
                    return $axios(originalRequest);
                } else {
                    await store.dispatch('user/logout')
                    return redirectPage('/auth/login')
                }
            } catch (error) {
            }
        } else if (code == 409) {
            return redirectPage('/admin/dashboard')
        }
        throw error
    })
}