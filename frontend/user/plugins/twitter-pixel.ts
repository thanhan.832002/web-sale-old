export default function ({ store }: { store: any }) {
    const twitter = store.state.setting.twitter
    if (twitter?.platformId) {
        const { twitterPixelId, pageViewId, contentViewId, addToCartId, checkoutInitiatedId, addedPaymentInfoId, purchaseId } = twitter.platformId
        console.log('twitterPixelId :>> ', twitterPixelId);
        if (twitterPixelId) {
            const twitterPixelIds = twitterPixelId.split("\n")
                .map((id: string) => id.trim())
                .filter((id: string) => id);
            for (let id of twitterPixelIds) {
                //@ts-ignore
                twq("init", id)
            }
        }
        if (pageViewId) {
            const pageViewIds = pageViewId.split("\n")
                .map((id: string) => id.trim())
                .filter((id: string) => id);
            for (let id of pageViewIds) {
                //@ts-ignore
                twq("init", id)
            }
        }
        if (contentViewId) {
            const contentViewIds = contentViewId.split("\n")
                .map((id: string) => id.trim())
                .filter((id: string) => id);
            for (let id of contentViewIds) {
                //@ts-ignore
                twq("init", id)
            }
        }
        if (addToCartId) {
            const addToCartIds = addToCartId.split("\n")
                .map((id: string) => id.trim())
                .filter((id: string) => id);
            for (let id of addToCartIds) {
                //@ts-ignore
                twq("init", id)
            }
        }
        if (checkoutInitiatedId) {
            const checkoutInitiatedIds = checkoutInitiatedId.split("\n")
                .map((id: string) => id.trim())
                .filter((id: string) => id);
            for (let id of checkoutInitiatedIds) {
                //@ts-ignore
                twq("init", id)
            }
        }

        if (addedPaymentInfoId) {
            const addedPaymentInfoIds = addedPaymentInfoId.split("\n")
                .map((id: string) => id.trim())
                .filter((id: string) => id);
            for (let id of addedPaymentInfoIds) {
                //@ts-ignore
                twq("init", id)
            }
        }

        if (purchaseId) {
            const purchaseIds = purchaseId.split("\n")
                .map((id: string) => id.trim())
                .filter((id: string) => id);
            for (let id of purchaseIds) {
                //@ts-ignore
                twq("init", id)
            }
        }
    }
}