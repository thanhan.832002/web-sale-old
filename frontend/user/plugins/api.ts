import { Plugin } from '@nuxt/types'
import authApi from '~/api/auth'
import homeApi from '~/api/home'
import galleryApi from '~/api/gallery'
import collectionApi from '~/api/collection'
import productApi from '~/api/product'
import variantApi from '~/api/variant'
import cartApi from '~/api/cart'
import promotionApi from '~/api/promotion'
import comboApi from '~/api/combo'
import shippingApi from '~/api/shipping'
import couponApi from '~/api/coupon'
import settingApi from '~/api/settings'
import orderApi from '~/api/order'
import paymentApi from '~/api/payment'
import adsAnalyticApi from '~/api/ads-analytic'
import recentlyApi from '~/api/recently'
import { NuxtAxiosInstance } from '@nuxtjs/axios'
import pixelApi from '~/api/pixel'

const apiPlugin: Plugin = (context, inject) => {
    const factories = {
        home: homeApi(context.$axios),
        auth: authApi(context.$axios),
        gallery: galleryApi(context.$axios),
        collection: collectionApi(context.$axios),
        product: productApi(context.$axios),
        variant: variantApi(context.$axios),
        cart: cartApi(context.$axios),
        promotion: promotionApi(context.$axios),
        pixel: pixelApi(context.$axios),
        combo: comboApi(context.$axios),
        shipping: shippingApi(context.$axios),
        coupon: couponApi(context.$axios),
        setting: settingApi(context.$axios),
        order: orderApi(context.$axios),
        payment: paymentApi(context.$axios),
        adsAnalytic: adsAnalyticApi(context.$axios),
        recently: recentlyApi(context.$axios),
    }
    inject('api', factories)
}

export default apiPlugin