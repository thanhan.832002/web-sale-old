import { apiErrorHandler } from "~/utils/apiHanlers";
import { getModule } from "vuex-module-decorators";
import AdminSetting from "~/store/setting";
import CartModule from "~/store/cart";
import PromotionModule from "~/store/promotion";
import { api, apiLocal } from "@/common/constaint";
import utmQuery from "~/utils/utmQuery";
import { resources } from "~/interfaces/api";
import { initFbPixel } from "@/utils/trackEvent";

export default async (ctx) => {
  await ctx.store.dispatch("nuxtClientInit", ctx);
  try {
    const { $api, store, route, $axios } = ctx;
    const { slug } = route.params;
    let isCheckSlug = false;
    const setting = getModule(AdminSetting, store);
    getModule(CartModule, store);

    let domain = "";
    if (!slug) {
      domain = window.location.host || "";
    } else {
      domain = `${slug}.${window.location.host}`;
    }

    // if (process.env.NODE_ENV === "development") {
    //   if (!slug) {
    //     domain = window.location.host || "";
    //   } else {
    //     domain = `${slug}.${window.location.host}`;
    //   }
    // } else {
    //   let hostnameSplit = window.location.hostname.split(".") || [];
    //   if (hostnameSplit.length > 2) {
    //     domain = window.location.hostname || "";
    //   } else {
    //     domain = `${slug}.${window.location.hostname}`;
    //   }
    // }

    if (
      slug !== "checkout" &&
      slug !== "policies" &&
      slug !== "pages" &&
      slug !== "order-success"
    )
      isCheckSlug = true;

    const responseCheckDomain = await $axios.$get(
      `${resources.HomePage}/check-domain/${domain}`
    );
    const isHomePage = responseCheckDomain?.data || false;
    if (isHomePage) {
      const response = await $axios.$get(
        `${apiLocal()}/${resources.HomePage}/${domain}`
      );
      const logo = response?.data?.logoId || "";
      const settingInfo = {
        storeName: response.data.homepageName,
        phoneNumber: response.data.phone,
        supportEmail: response.data.supportEmail,
        companyInfo: response.data.address,
        logo: logo?.imageUrl || "",
      };
      console.log("settingInfo :>> ", settingInfo);
      store.commit("product/setIsHomePage", isHomePage);
      store.commit("setting/setHeaderHomePage", true);
      store.commit(
        "setting/setLogo",
        `${
          response?.data?.logoId?.imageUrl
            ? `${apiLocal()}/${response?.data.logoId.imageUrl}`
            : "/logous.png"
        }`
      );
      store.commit("setting/setSettingHomePage", response?.data || {});
      setting.setSiteInfo(settingInfo);
    } else {
      utmQuery({ $route: route, $store: store });
      const proms = [
        $axios.$get(`${api}/${resources.StoreBuyer}/setting/${domain}`),
        $axios.$get(`${api}/${resources.pixel}/domain/${domain}`),
      ];

      const [responseSetting, responsePixel] = await Promise.all(proms);
      if (responseSetting?.data) {
        const { logo } = responseSetting.data;
        const settingInfo = {
          ...responseSetting.data,
          logo: responseSetting.data?.logo?.imageUrl || "",
        };
        if (logo) {
          ctx.app.head.link[0].href = `${api}/${
            logo?.imageUrl || "/logous.png"
          }`;
        }
        ctx.app.head.title = responseSetting.data.webTitle || "Store";
        setting.setSiteInfo(settingInfo);
      }
      if (responsePixel?.data) {
        if (responsePixel.data?.facebookPixel?.facebookPixelId) {
          const facebookPixelId =
            responsePixel.data?.facebookPixel?.facebookPixelId;
          initFbPixel(facebookPixelId);
        }
        const listAdsAnalytic = [
          {
            name: "google",
            platformId: responsePixel?.data?.googleAds?.googleAnalyticId || "",
            googleTagManager:
              responsePixel?.data?.googleAds?.googleTagManager || "",
          },
          {
            name: "tiktok",
            platformId: responsePixel?.data?.tiktokPixel?.tikTokPixelId || "",
          },
          {
            name: "facebook",
            platformId:
              responsePixel?.data?.facebookPixel?.facebookPixelId || "",
            contentId: responsePixel?.data?.facebookPixel?.contentId || "",
          },
          {
            name: "snapchat",
            platformId: responsePixel?.data?.snapPixel?.snapPixelId || "",
          },
          {
            name: "twitter",
            platformId: responsePixel?.data?.twitterPixel || {},
          },
        ];
        store.commit("setting/setAdsAnalytic", listAdsAnalytic);
      }
      // $api.setting
      //   .getSetting(domain)
      //   .then((result) => {
      //     if (result?.data) {
      //       const { logo } = result.data;
      //       const settingInfo = {
      //         ...result.data,
      //         logo: result.data?.logo?.imageUrl || "",
      //       };
      //       if (logo) {
      //         ctx.app.head.link[0].href = `${api}/${
      //           logo?.imageUrl || "/logous.png"
      //         }`;
      //       }
      //       ctx.app.head.title = result.data.webTitle || "Store";
      //       setting.setSiteInfo(settingInfo);
      //     }
      //   })
      //   .catch((err) => {
      //     ctx.app.router.push("/error");
      //     throw err;
      //   });
    }
    store.commit("product/setDomain", domain);
    store.commit("product/setSlug", slug);
    store.commit("product/setIsCheckSlug", isCheckSlug);
  } catch (error) {
    ctx.app.router.push("/error");
    // apiErrorHandler(error, ctx)
  }
};
